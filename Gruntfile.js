// Salaxy Web library (salaxy-lib-ng1)
// Grunt dev and build configuration v2.0
// OP 22.11.2017

module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    conf: {
      gruntServerPort: 9000,
      dist: "web/dist/v03-rc",
    },
    less: {
      development: {
        options: {
          sourceMap: true,
          sourceMapFileInline: false,
          sourceMapRootpath: "/",
        },
        files: {
          "dist/npm/@salaxy/ng1/css/bootstrap.css": "src/less/_bootstrap.less",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.css": "src/less/_salaxy-lib-ng1-bootstrap.less",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-all.css": "src/less/_salaxy-lib-ng1-all.less",
          // TODO: Consider moving salaxy-rpt.css to the other 2 CSS files.
          "dist/npm/@salaxy/ng1/css/salaxy-rpt.css": "src/less/_salaxy-rpt.less",
          "dist/npm/@salaxy/ng1/css/personal.css": "src/less/salaxy-component/personal/style.less"
        }
      }
    },
    postcss: {
      minified: {
        options: {
          map: false,
          processors: [
            require('pixrem')(),
            require('autoprefixer')(),
            require('cssnano')()
          ]
        },
        files: {
          "dist/npm/@salaxy/ng1/css/bootstrap.min.css": "dist/npm/@salaxy/ng1/css/bootstrap.css",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.min.css": "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.css",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-all.min.css": "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-all.css",
          "dist/npm/@salaxy/ng1/css/salaxy-rpt.min.css": "dist/npm/@salaxy/ng1/css/salaxy-rpt.css",
          "dist/npm/@salaxy/ng1/css/personal.min.css": "dist/npm/@salaxy/ng1/css/personal.css"
        }
      },
      dev: {
        options: {
          map: {
            inline: false,
          },
          processors: [
            require('pixrem')(),
            require('autoprefixer')(),
          ]
        },
        files: {
          "dist/npm/@salaxy/ng1/css/bootstrap.css": "dist/npm/@salaxy/ng1/css/bootstrap.css",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.css": "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.css",
          "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-all.css": "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-all.css",
          "dist/npm/@salaxy/ng1/css/salaxy-rpt.css": "dist/npm/@salaxy/ng1/css/salaxy-rpt.css",
          "dist/npm/@salaxy/ng1/css/personal.css": "dist/npm/@salaxy/ng1/css/personal.css"
        }
      },
      anonCalc: {
        options: {
          map: false,
          processors: [
            require('pixrem')(),
            require('autoprefixer')(),
            require('cssnano')()
          ]
        },
        files: {
          "dist/npm/@salaxy/ng1/css/version/anon/bootstrap.min.css": "dist/npm/@salaxy/ng1/css/bootstrap.css",
          "dist/npm/@salaxy/ng1/css/version/anon/salaxy-lib-ng1-bootstrap.min.css": "dist/npm/@salaxy/ng1/css/salaxy-lib-ng1-bootstrap.css",
          "dist/npm/@salaxy/ng1/css/version/anon/salaxy-rpt.min.css": "dist/npm/@salaxy/ng1/css/salaxy-rpt.css"
        }
      },

    },
    ngtemplates: {
      bootstrap: {
        cwd: 'src/templates',
        src: '**/*.html',
        dest: 'src/ts/templates/bootstrap.ts',
        options: {
          module: 'salaxy.ng1.templates.bootstrap',
          prefix: 'salaxy-components',
          htmlmin: { collapseWhitespace: true },
          bootstrap: function(module, script) {
            return '/* eslint-disable */\nimport * as angular from "angular";\n/** Bootstrap templates compiled to JavaScript\n@ignore */\nexport const SalaxyNg1BootstrapTemplatesModule = angular.module("salaxy.ng1.templates.bootstrap", []).run(["$templateCache", function ($templateCache) { ' + script + ' }])';
          }
        }
      }
    },
    ts: {
      es6: {
        tsconfig: {
          tsconfig: 'src/ts/tsconfig.json',
          passThrough: true
        }
      },
      testPaymentChannel: {
        tsconfig: {
          tsconfig: 'web/payment-channels/salaxy/test/tsconfig.json',
          passThrough: true
        }
      },
      app: {
        tsconfig: {
          tsconfig: 'web/app/tsconfig.json',
          passThrough: true
        }
      },
      ng1Demo: {
        tsconfig: {
          tsconfig: 'web/ng1/tsconfig.json',
          passThrough: true
        }
      },
    },
    browserify: {
      ng1: {
        src: ["dist/npm/@salaxy/ng1/js/index.js"],
        dest: 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1.js',
        options: {
          alias: {
            '@salaxy/ng1': "./dist/npm/@salaxy/ng1/js/index.js",
          },
          external: ["@salaxy/core", "@salaxy/reports"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
      testPaymentChannel: {
        src: ["web/payment-channels/salaxy/test/dist/index.js"],
        dest: 'web/payment-channels/salaxy/test/dist/paymentChannel.js',
        options: {
          external: ["@salaxy/ng1", "@salaxy/core", "@salaxy/reports"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
      app: {
        src: ["web/app/scripts/build/index.js"],
        dest: 'web/app/scripts/Salaxy.demoApp.js',
        options: {
          external: ["@salaxy/ng1", "@salaxy/core", "@salaxy/reports"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
      ng1Demo: {
        src: ["web/ng1/scripts/build/index.js"],
        dest: 'web/ng1/scripts/ng1Demo.js',
        options: {
          external: ["@salaxy/ng1", "@salaxy/core", "@salaxy/reports"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      }
      
    },
    clean: {
      npm: {
        src: ['dist/npm/@salaxy/ng1']
      }
    },
    eslint: {
      options: {
        configFile: "src/ts/.eslintrc.js",
        extensions: [".ts"],
      },
      target: ['src/ts']
    },
    concat: {
      dependencies: {
        options: {
          banner: "'use strict';\n\n/***** SALAXY DEPENDENCIES, See https://developers.salaxy.com for details *****/\n\n",
          process: function(src, filepath) {
            if (filepath.indexOf("angular-barcode.js") > 0) {
              src = src.replace("//# sourceMappingURL=angular-barcode.js.map", "");
            }
            return '// Source: ' + filepath + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          }
        },
        src: [
          // Angular modules
          'node_modules/angular/angular.js',
          'node_modules/angular-i18n/angular-locale_fi.js',             // Currency and number filters, perhaps date.
          'node_modules/angular-route/angular-route.js',                // Angular routing (routeProvider)
          'node_modules/angular-sanitize/angular-sanitize.js',          // $sce
          'node_modules/angular-translate/dist/angular-translate.js',   // Translations: To be removed, but is currently in use.
          'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',// UI bootstrap components with templates
          'node_modules/angular-barcode/dist/angular-barcode.js',       // Barcode in invoices
          'node_modules/ng-file-upload/dist/ng-file-upload-all.min.js', // Upload
          'node_modules/chart.js/dist/Chart.js',                        // Chart.js used through in angular-chart (+annotation plugin)
          'node_modules/angular-chart.js/dist/angular-chart.js',        // ...should be rewritten to use Chart.js directly (angular-chart uses old Chart.js)
          'node_modules/chartjs-plugin-annotation/chartjs-plugin-annotation.js',
          // Other imported modules
          'node_modules/marked/lib/marked.js',          // Imported in translations, but is it really used in texts?
          'node_modules/jszip/dist/jszip.js',           // Imported in report generation
          'node_modules/file-saver/dist/FileSaver.js',  // Excel and PDF download UI when file created in client.
          'node_modules/rrule/dist/es5/rrule.js',       // Becoming used in calendar events.
        ],
        dest: 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-dependencies.js',
        nonull: true
      },
      all: {
        options: {
          banner: "'use strict';\n\n/***** SALAXY FULL LIBRARY (salaxy-core and external dependencies), See https://developers.salaxy.com for details *****/\n\n",
          process: function(src, filepath) {
            return '// Source: ' + filepath + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          }
        },
        src: [
          'dist/npm/@salaxy/ng1/js/salaxy-core-all.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-reports.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-dependencies.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1.js',
        ],
        dest: 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-all.js',
        nonull: true
      },
      minAll: {
        options: {
          banner: "'use strict';\n\n/***** SALAXY FULL LIBRARY (salaxy-core and external dependencies), See https://developers.salaxy.com for details *****/\n\n",
        },
        src: [
          'dist/npm/@salaxy/ng1/js/salaxy-core-all.min.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-reports.min.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-dependencies.min.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1.min.js',
        ],
        dest: 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-all.min.js',
        nonull: true
      },
      anonCalc: {
        src: ['dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-all.min.js', 'env/demo/settings.js', 'src/anon/anon-calc.js'],
        dest: 'dist/npm/@salaxy/ng1/js/version/anon/calculator.js',
      }
    },
    terser: {
      all: {
        files: {
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1.min.js': 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1.js',
          'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-dependencies.min.js': 'dist/npm/@salaxy/ng1/js/salaxy-lib-ng1-dependencies.js',
        },
      }
    },
    copy: {
      core: {
        files: [
          { expand: true, cwd: 'node_modules/@salaxy/core', src: 'salaxy-core-all.*', dest: 'dist/npm/@salaxy/ng1/js' },
        ],
      },
      reports: {
        files: [
          { expand: true, cwd: 'node_modules/@salaxy/reports', src: 'salaxy-lib-reports.*js', dest: 'dist/npm/@salaxy/ng1/js' },
          { expand: true, cwd: 'node_modules/@salaxy/reports', src: 'salaxy-lib-reports.*ts', dest: 'dist/npm/@salaxy/ng1/js' },
        ],
      },
      npm: {
        files: [
          { expand: true, src: ['src/npm/*'], dest: 'dist/npm/@salaxy/ng1', flatten: true, filter: 'isFile' },
          { expand: true, cwd: 'src/less', src: '**', dest: 'dist/npm/@salaxy/ng1/less' },
        ]
      },
      web: {
        files: [
          { expand: true, src: ['dist/npm/@salaxy/ng1/js/salaxy-*'], dest: '<%= conf.dist %>/js', flatten: true, filter: 'isFile' },
          { expand: true, src: ['dist/npm/@salaxy/ng1/css/*'], dest: '<%= conf.dist %>/css', flatten: true, filter: 'isFile' },
          { expand: true, cwd: 'src/less', src: '**', dest: 'web/less-preview' },
        ]
      },
      docs: {
        files: [
          { expand: true, cwd: 'src/templates', src: '**', dest: 'web/docs/templates/' },
        ],
      },
    },
    typedoc: {
      ng1: {
        options: {
          tsconfig: "src/ts/tsconfig.json",
          json: 'web/docs/ts/@salaxy/ng1.json',
          name: 'Salaxy Angular.js docs (@salaxy/ng1)',
          exclude: "**/*/@salaxy/reports/**/*.ts",
        },
        src: ['src/ts/index.ts']
      },
      core: {
        options: {
          tsconfig: "../salaxy-lib-core/src/@salaxy/core/tsconfig.json",
          json: 'web/docs/ts/@salaxy/core.json',
          name: 'Salaxy Core JavaScript docs',
        },
        src: ['../salaxy-lib-core/src/@salaxy/core/index.ts']
      },
      core_vs: {
        options: {
          tsconfig: "../git/salaxy-lib-core/src/@salaxy/core/tsconfig.json",
          json: 'web/docs/ts/@salaxy/core.json',
          name: 'Salaxy Core JavaScript docs',
        },
        src: ['../git/salaxy-lib-core/src/@salaxy/core/index.ts']
      },
    },
    connect: {
      all: {
        options: {
          port: '<%= conf.gruntServerPort%>',
          base: 'web',
          hostname: "localhost",
          livereload: true
        }
      },
    },
    watch: {
      options: {
        livereload: 35730,
      },
      web: {
        files: ['web/app/**/*.*', 'web/ng1/**/*.*', 'web/personal/**/*.*', 'web/core/**/*.*'],
      },
      styles: {
        files: ['src/less/**/*.less'],
        tasks: ['less', 'postcss', 'copy:npm', 'copy:web'],
      },
      ts: {
        files: ['src/templates/**/*.html', 'src/ts/**/*.ts', 'src/ts/i18n/*.json'],
        tasks: ['ngtemplates', 'ts:es6', 'browserify:ng1', 'concat:dependencies', 'concat:all', 'copy:web'],
      },
      allPaymentChannels: {
        files: ['web/payment-channels/**/*.ts'],
        tasks: ['testPaymentChannel'],
      },
      app: {
        files: ['web/app/**/*.ts'],
        tasks: ['app'],
      },
      ng1Demo: {
        files: ['web/ng1/**/*.ts'],
        tasks: ['ng1Demo'],
      },
    },
    open: {
      all: {
        path: 'http://<%= connect.all.options.hostname %>:<%= connect.all.options.port %>'
      }
    },
    http: {
      api_test: {
        options: { url: 'https://test-secure.salaxy.com/docs/swagger/v02' },
        dest: 'web/docs/swagger/salaxy-02.json'
      },
      api_demo: {
        options: { url: 'https://test-api.salaxy.com/docs/swagger/v02' },
        dest: 'web/docs/swagger/salaxy-02.json'
      },
      api_local: {
        options: { url: 'http://localhost:82/docs/swagger/v02' },
        dest: 'web/docs/swagger/salaxy-02.json'
      },
      schema_test: {
        options: { url: 'https://test-secure.salaxy.com/docs/schema/v02' },
        dest: 'web/docs/swagger/schema.json'
      },
      schema_demo: {
        options: { url: 'https://test-api.salaxy.com/docs/schema/v02' },
        dest: 'web/docs/swagger/schema.json'
      },
      schema_local: {
        options: { url: 'http://localhost:82/docs/schema/v02' },
        dest: 'web/docs/swagger/schema.json'
      }
    }
  });

  // loadNpmTasks is needed for non-standard namings, typical cases are handled by jit-grunt
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-http');
  grunt.loadNpmTasks('@lodder/grunt-postcss');

  // ===========================================================================
  // BUILD JOBS 		  ========================================================
  // ===========================================================================

  var tasksBase = ['clean:npm', 'copy:core', 'copy:reports', 'less', 'postcss', 'ngtemplates', 'ts', 'browserify', 'concat:dependencies', 'concat:all'];
  var tasksCopy = ['copy:npm', 'copy:web', 'copy:docs'];

  grunt.registerTask('noDocsWarning', function() {
    grunt.log.writeln('WARNING: Lint, docs generation and minification removed from the default task. Run "GRUNT build" and "npm run lint" before Push.');
  });
  grunt.registerTask('noLintWarning', function() {
    grunt.log.writeln('WARNING: Run "npm run lint" BEFORE PUSH (scroll up if errors).');
  });

  // Default build in development
  grunt.registerTask('default', tasksBase.concat(tasksCopy, ['noDocsWarning']));

  // TypeScript build only including templates
  grunt.registerTask('code', ['ngtemplates', 'ts:es6', 'browserify:ng1', 'concat:dependencies', 'concat:all', 'copy:web']);

  // Full (longer) build that should be run before push and when you switch branches or start new
  grunt.registerTask('build', tasksBase.concat(['terser', 'concat:minAll', 'typedoc:ng1', 'typedoc:core'], tasksCopy, ['noLintWarning']));

  // Basic build that is run in the VS DevOps server
  grunt.registerTask('build-devops', tasksBase.concat(['terser', 'concat:minAll'], tasksCopy));
 
  // Full build that is run in the VS DevOps server for developers.salaxy.com
  grunt.registerTask('build-devops-developers', tasksBase.concat(['terser', 'concat:minAll', 'typedoc:ng1', 'typedoc:core_vs'], tasksCopy));

  // Starts the server (run the build first)
  grunt.registerTask('server', ['connect', 'open', 'watch']);

  grunt.registerTask('css', ['less', 'postcss']);

  // Fetching the API data
  grunt.registerTask('fetch-test-api', ['http:api_test', 'http:schema_test']);
  grunt.registerTask('fetch-demo-api', ['http:api_demo', 'http:schema_demo']);
  grunt.registerTask('fetch-local-api', ['http:api_local', 'http:schema_local']);

  /** task for bundling the js and minifying css for anon calculator. Grunt build is required before running this task */
  grunt.registerTask('anonCalc', ['concat:anonCalc', 'postcss:anonCalc']);

  // The main web application
  grunt.registerTask('app', ['ts:app', 'browserify:app']); 
  // NG1 Components demo site: Examples, documenatation and How-to guides
  grunt.registerTask('ng1Demo', ['ts:ng1Demo', 'browserify:ng1Demo']); 

  // Test payment channel
  grunt.registerTask('testPaymentChannel', ['ts:testPaymentChannel', 'browserify:testPaymentChannel']);
  
  /** The Parse Bootstrap Less task is only required when there are new LESS variables. */
  grunt.registerTask('parseBootstrapLess', function() {
    var LessVariablesToArrayParser = require("./src/less/tools/less-variables-to-array-parser");
    var parser = new LessVariablesToArrayParser(__dirname);
    console.log("Parsing bootstrap-less/variables.less");
    parser.parseFile("src/less/bootstrap-less/variables.less", "bootstrapLessVariables")
    console.log("Parsing salaxy-component/variable-defaults.less");
    parser.parseFile("src/less/salaxy-component/variable-defaults.less", "salaxyLessVariables")
    console.log('Created bootstrapLessVariables.ts and salaxyLessVariables.ts from LESS-files.');
  });
};