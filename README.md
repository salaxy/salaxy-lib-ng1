SALAXY MAIN WEB LIBRARY (salaxy-lib-ng1)
===============================================

<img class="img-responsive" src="https://developers.salaxy.com/img/content/architecture/architecture-ng1.png" width="600">

This project contains the main Salaxy library that we have been running 
in production since March 2016 on our Palkkaus.fi web site.
It contains the sources for JavaScript libraries and CSS-styles and related images, fonts and other libraries.

For now, the project also contains the https://developers.salaxy.com developer web site
and part of the plain JavaScript libraries are also developed and documented in this project. 
We are in process of moving the JavaScript / TypeScript libraries to https://gitlab.com/salaxy/salaxy-lib-core.
The developer web site may be moved later.

Getting started - the quick way
-------------------------------

The easiest way to get started is to follow our Getting started instructions
and examples in [developers.salaxy.com](https://developers.salaxy.com/#/ng1/index). 

Getting started - full control
------------------------------

If you already have GIT, Node and GRUNT installed:

1. Clone repository `git clone -b WWW https://gitlab.com/salaxy/salaxy-lib-ng1`
   - This clones the stable WWW branch
   - For real development, you probably want to clone the Master `git clone https://gitlab.com/salaxy/salaxy-lib-ng1`
2. Enter project directory `cd salaxy-lib-ng1`
3. Install dependencies via NPM: `npm install --ignore-scripts`
4. Build project via Grunt: `grunt build`
5. Start the project web site in dev web server: `grunt server`
  - Server is configured to run in port 9000. If that is already in use, you can change the port in gruntfile.js
6. Have fun in exploring and developing with the code.
7. Tell us what you think [contact@salaxy.com](mailto:contact@salaxy.com) or if you have any problems [Issue tracker](https://gitlab.com/salaxy/salaxy-lib-ng1/issues)

Additional steps in MASTER branch (development)
-----------------------------------------------

**WWW** branch should typically be linked to Core libraries that are
published in NPM. Therefore it is typically best to use that.

**Master** branch may be linked to the latest libraries in GIT and there may be build
errors if you have the versions published in NPM. To link to the latest libraries,
you should:

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-lib-core`
2. NPN install and build the project
   - See instructions in the readme files and docs folder in the project if necessary.
4. Link the libraries form the build target folder to your local NPM
   - `cd /src/salaxy-lib-core/dist/npm/@salaxy/core` => `npm link`
   - `cd /src/salaxy-lib-core/dist/npm/@salaxy/reports` => `npm link`
5. Link the libraries in the root of this project:
   - `cd /src/salaxy-lib-ng1`
   - `npm link @salaxy/core`
   - `npm link @salaxy/reports`

To install the required software (GIT, Node and GRUNT)
-----------------------------------------------
	
1.	Install the latest version of Node.js from https://nodejs.org/.
	Use the default installation options (LTS version).
2. 	Install GRUNT build system by opening command prompt (in any folder), and executing command: 
	`npm install -g grunt-cli`. 
	This will make the GRUNT build system globally available on your workstation. 
3.  Install GIT from here: https://git-scm.com/downloads
    For windows users, if you plan to become serious with GIT, we higly recommend
	[GitExtensions](https://gitextensions.github.io/)
    

Troubleshooting: 

-	If you see error: `Fatal error: Unable to find local grunt`, 
	make sure you've executed step 3 in the correct folder, where Gruntfile.js resides.
-   Ask us to help you: [contact@salaxy.com](mailto:contact@salaxy.com)

Editing the files
------------------------------

At the moment, this project is edited exclusively by Salaxy product development team. 
For small fixes, just
[create an Issue](https://gitlab.com/salaxy/salaxy-lib-ng1/issues)
and/or e-mail to Olli Perttilä at Palkkaus.fi or [contact@salaxy.com](mailto:contact@salaxy.com).
For larger improvements, fixes and suggestions it is possible to request a merge request. 
However, it is probably better to first contact us at Palkkaus.fi or [contact@salaxy.com](mailto:contact@salaxy.com).


