TODO: Coding guidelines for NG1-project
=======================================

TODO: This is still work in progress. Working on the issue while fixing ESLint issues.

ESLint
------

Basic idea is to follow the ESLint suggestions. They are there for reasons.
Before pushing, make sure that ESLint passes.
**Changes to ESLint rules always require permission / code review of a senior developer**

Naturally there are sometimes exceptions to ESLint rules. When you encounter them,
mark the lines using:

- `eslint-disable-line`
- `eslint-disable-next-line`
- If you add `/* eslint-disable */`, be sure to also add `/* eslint-enable */` after disabling
  is no longer needed. Prefer the single-line versions.

**Take note of all disable instances** and discuss them in code review / send to senior for review.

Examples of justified exceptions to ESLint rules:

- Sometimes large sets of data should be added to the end-of-file (for readability) even
  though the rule sais private fields (en even public fields) should be at the beginning of the file.
  Please note though that we have a separate folder for code-generation if there is really a lot of data
  and if it is generated and updated from external source. Also, end-user texts should be in i18n folders.
- Sometimes e.g. large text blocks make lines be wider than the rule suggests.
  Again, please note that end-user texts should be in i18n files.

Imports
-------

ESLint rules should force that imports are in alphabetic order. However it makes sense to group them
categorically based on how "external" they are. Because of this, we group the imports to 3 categories
using empty lines:

1. External libraries
2. Salaxy libraries (@salaxy/core, @salaxy/reports)
3. Current project (@salaxy/ng1)
   - Note that if you are using NG1 in a web site (even e.g. Salaxy.ng1 demo site),
     then NG1 is a Salaxy library and that web site is current project.

I.e. the imports could be like this:

```TypeScript
import * as angular from "angular"; // External

import { Arrays, Avatar, EnumerationsLogic, Objects } from "@salaxy/core"; // Salaxy library

import { InputController } from "./InputController"; // This project, alphabetical order
import { InputEnumOption } from "./InputEnumOption";
```

Documentation
-------------

The libraries (e.g. @salaxy/ng1, @salaxy/core) **must** be documented using JSDoc. 
The motivation for this is:

- These are libraries that may be used by external developers
  - We generate documentation of the libraries components, controllers etc.
    to our web site
- We are a startup: It is foreseeable that at some point there will be a
  merger or aquisition to another company. At that point, we want to have
  our code clearly documented so that it passes Technical Due Diligence.
- Good documentation also helps new recruits and maintenance in the future

Documentation is absolutely critical for product code, but it is recommended
for customer projects and customizations.

Documentation must be the explanation from **you as the developer** to another developer 
**looking at the code 2-5 years from now** trying to make changes or understand what is going on:

- It should give an overview of the purpose of the member
  - Purpose of a class or interface
  - What a function does
  - Description of a property, field or parameter
- Exact details can be inspected from source code.

It is **wrong to auto-generate** documentation: It is a message from one human to another.
Also, do not add documentation like "Documentation TODO". Your code is not ready and it should not
be pushed to Git before the documentation is there.

Documentation **must be done for all public members:**

- Exported classes and interfaces
- Properties, fields etc. of exported classes
- Public functions and all their parameters

Documentation **is not required** for:

- Internal classes and interfaces
- Constructors in NG1-projects
  - Justification: Constructors should be used for dependency injection only
    and this should be self-explanatory.
- Private properties, fields, and functions etc.
- Generated code
  - Except that it should be clear from where the code is generated.

ESLinter should be able to check that the documentation is done.
However as of writing (11.2.2021) there are som issues in this
(e.g. class property documentation may be missing without warning).
