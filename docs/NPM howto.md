Release check-list
------------------

Publish the NPM package:

- If this is the first time you publish on this workstation, add a adduser:
  - `npm adduser`, UID: salaxy, PWD: check in salaxy-vault, E-mail: contact@salaxy.com
- Update the version numbers
  - `/src/salaxy-lib-ng1/src/npm/package.json`
  - If you update core, also update reference in `/package.json`
- Make a build
  - `cd /src/salaxy-lib-ng1`
  - `grunt build`
  - `npm run lint`
- If you are making large changes, test the package at this point. See "Testing the package locally" below
- Publish JS
  - `cd /src/salaxy-lib-ng1/dist/npm/@salaxy/ng1`
  - `npm publish --access public` (version: x.y.z)   
    ...or `npm publish --access public --tag beta` for beta versions (version: x.y.z-beta.n)
  - `cd ../../../..`
- Commit + Push to GIT
   - Release must in be in WWW-branch
   - Preferably, both WWW and Master on same level. At least, merge WWW to Master.
   - Add Tag for version number, e.g. "2.10.20"

Testing release from NPM:
  
- `cd /src/foobar`
- `npm uninstall --save @salaxy/ng1`
- `npm install --save @salaxy/ng1@latest`

Testing the package locally (when big changes)

- `cd /src/salaxy-lib-ng1`
- `grunt`
- `cd /src/salaxy-lib-ng1/dist/npm/@salaxy/ng1`
- `npm link`
- `cd /src/foobar`
- `npm link @salaxy/ng1`
