var salaxy;
(function(salaxy) {
    /** 
     * Environment specific configuration for Salaxy API's and JavaScript in general
     */
    salaxy.config = {
        wwwServer: 'https://test.palkkaus.fi', //'https://demo-www.palkkaus.fi'
        apiServer: 'https://test-api.salaxy.com', //'https://demo-secure.salaxy.com',
        isTestData: true,
        useCookie: true
    };
})(salaxy || (salaxy = {}));