var salaxy;
(function(salaxy) {
    /** 
     * Environment specific configuration for Salaxy API's and JavaScript in general
     */
    salaxy.config = {
        wwwServer: 'http://localhost:81',
        apiServer: 'http://localhost:82',
        isTestData: true,
        useCookie: true
    };
})(salaxy || (salaxy = {}));