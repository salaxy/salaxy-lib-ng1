var salaxy;
(function(salaxy) {
    /** 
     * Environment specific configuration for Salaxy API's and JavaScript in general
     */
    salaxy.config = {
        wwwServer: 'https://test-www.palkkaus.fi',
        apiServer: 'https://test-secure.salaxy.com',
        isTestData: true,
        useCookie: true
    };
})(salaxy || (salaxy = {}));