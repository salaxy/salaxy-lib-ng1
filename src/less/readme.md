Dynamic LESS-compilation
------------------------

As of July 2019 the `src/less` folder is independent from external references: 
everything is in that folder an it does not require node_modules.
This change was done so that you can use dynamic LESS-rendering in browser
by simply copying this folder.

Custom styles compilation with LESS
-----------------------------------

You can compile project specific css-files using the included less-files.
For example:

```
@import "node_modules/@salaxy/ng1/less/_bootstrap.less";
@import "variables/variables-bootstrap.less";
```

produces bootstrap.css

and

```
@import "node_modules/@salaxy/ng1/less/_salaxy-lib-ng1-bootstrap.less";
@import "variables/variables-bootstrap.less";
@import "variables/variables-salaxy.less";
```

produces salaxy-lib-ng1-bootstrap.less

with overridden variables.

Sample grunt tasks:

```
        less: {
            development: {
                options: {
                    sourceMap: true,
                    sourceMapFileInline: false,
                    sourceMapRootpath: "/",
                },
                files: {
                    
                    "dist/css/bootstrap.css": "src/less/_bootstrap.less",
                    "dist/css/salaxy-lib-ng1-bootstrap.css": "src/less/_salaxy-lib-ng1-bootstrap.less",
                }
            }
        },

        postcss: {
            minified: {
                options: {
                    map: false,
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')(),
                        require('cssnano')()
                    ]
                },
                files: {
                    "dist/css/bootstrap.min.css": "dist/css/bootstrap.css",
                    "dist/css/salaxy-lib-ng1-bootstrap.min.css": "dist/css/salaxy-lib-ng1-bootstrap.css"
                }
            },
            dev: {
                options: {
                    map: {
                        inline: false,
                    },
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')(),
                    ]
                },
                files: {
                    "dist/css/bootstrap.css": "dist/css/bootstrap.css",
                    "dist/css/salaxy-lib-ng1-bootstrap.css": "dist/css/salaxy-lib-ng1-bootstrap.css"
                }
            },

        },

```

