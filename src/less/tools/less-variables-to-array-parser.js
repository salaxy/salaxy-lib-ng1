/*!
 * Helper for using bs-lessdoc-parser to parse LESS file variables with their documentation as TypeScript array.
 */

'use strict';

var fs = require('fs');
var path = require('path');
var BsLessdocParser = require('./bs-lessdoc-parser');

/** 
 * Creates a new parser
 * @param rootDir Root directory for the project.
 */
function LessVariablesToArrayParser(rootDir) {
  this.rootDir = rootDir;
}

/** 
 * Parses a LESS variables file with comments and renders it as TypeScript const array.
 * The file will be written as `src/ts/codegen/${name}.ts`.
 * 
 * @param source Source less file path. Starting from the project root.
 * @param name Name for the TypeScript constant. Also the TypeScript file.
 */
LessVariablesToArrayParser.prototype.parseFile = function (source, name) { 
  var sourcePath = path.join(this.rootDir, source);
  var targetPath = path.join(this.rootDir, `src/ts/codegen/${name}.ts`);
  var sourceFile = fs.readFileSync(sourcePath, { encoding: 'utf8' });
  var parser = new BsLessdocParser(sourceFile);
  var parserResult = JSON.stringify(parser.parseFile(), null, 2);
  var result = `/* eslint-disable */
  /** 
   * Less variables for ${source}
   * @ignore
   */
  export const ${name} = ${parserResult};`;
  fs.writeFileSync(targetPath, result, { encoding: 'utf8' });
};

module.exports = LessVariablesToArrayParser;