AngularJS libraries for Salaxy platform
=======================================

This package contains the main Salaxy library that we have been running 
in production since March 2016 on our Palkkaus.fi web site.

Usage:

- Run `npm install --save @salaxy/ng1@latest`
- Reference or copy files from `node_modules/@salaxy/ng1`

| File                                | Description                                                               |
| ----------------------------------- | ------------------------------------------------------------------------- |
| index.js                            | ES6 module compilation                                                    |
| salaxy-lib-ng1.js                   | Browser friendly (browserify) compilation                                 | 
| salaxy-lib-ng1.min.js               | Browser friendly (browserify) compilation (min)                           | 
| salaxy-lib-ng1-dependencies.js      | Required dependencies bundle                                              | 
| salaxy-lib-ng1-dependencies.min.js  | Required dependencies bundle (min)                                        | 
| salaxy-lib-ng1-all.js               | Browser friendly (browserify) compilation with dependencies bundle        | 
| salaxy-lib-ng1-all.min.js           | Browser friendly (browserify) compilation with dependencies bundle (min)  | 


For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com 