﻿Salaxy library for AngularJS, full reference
============================================

Full reference is automatically generated documentation from source TypeScript code using TypeDoc.
It can be used to look at advanced technical issues and problems as it contains full TypeDoc documenation
without any filtering or formatting.

For overview of how the code works, please go to 
<a href="https://developers.salaxy.com/ng1" target="_top">
  https://developers.salaxy.com/</a>.

The overview groups the referencce documentation to:

- Components
- Controllers
- Directives
- Filters
- Services

...which will typically make more sense from AngularJS point-of-view than looking at the raw TypeDoc documentation.

For detailed information, you may also consider checking the library
<a href="https://gitlab.com/salaxy/salaxy-lib-ng1/-/tree/master/src" target="_top">
  source code in GitLab</a>. Especially 
<a href="https://code.visualstudio.com/" target="_top">VSCode</a>
will make it easy to navigate the code with its excellent support of TypeScript.
  