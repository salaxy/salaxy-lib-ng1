import { IQService } from "angular";

import { Ajax, Configs, Cookies, Token } from "@salaxy/core";

/**
 * The $http access to the server methods: GET, POST and DELETE
 * with different return types and authentication / error events.
 */
export class AjaxNg1 implements Ajax {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$http", "$q"];

    /** Alert service for ajax. */
    public static alertService: any;

   /**
    * By default (true) the token is set to salaxy-token -cookie.
    * Disable cookies with this flag.
    */
    public useCookie = true;

    /**
     * By default credentials are not used in http-calls.
     * Enable credentials with this flag.
     */
    public useCredentials = false;

    /**
     * The server address - root of the server
     * This is settable field. Will probably be changed to a configuration object in the final version.
     */
    public serverAddress = "https://test-api.salaxy.com";

    private token: string;

    /**
     * Creates a new instance of AjaxNg1
     *
     * @param $http - Angular http service
     */
    constructor(private $http: angular.IHttpService, private $q: IQService) {

      const config = Configs.current;
      if (config) {
        // apiServer
        if (config.apiServer) {
          this.serverAddress = config.apiServer;
        }

        // useCredentials
        if (config.useCredentials != null) {
          this.useCredentials = config.useCredentials;
        }

        // useCookie
        if (config.useCookie != null) {
          this.useCookie = config.useCookie;
        }
      }
    }

    /**
     * Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api'.
     * Default version, as of writing 2.0
     */
    public getApiAddress(): string {
        return this.serverAddress + "/v02/api";
    }

    /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
    public getServerAddress(): string {
        return this.serverAddress;
    }

    /** Gets a JSON-message from server using the API
     *
     * @param method The API method is the url path after the api version segments (e.g. '/v02/api')
     * and starts with a forward slash, e.g. '/calculator/new'.
     *
     * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
     */
    public getJSON(method: string): Promise<any> {

        const request: any = {}; // angular.IRequestConfig

        const token: string = this.getCurrentToken();
        if (token) {
            request.headers = { Authorization: "Bearer " + token };
        }

        request.url = this.getUrl(method);
        request.method = "GET";
        request.responseType = "json";
        request.withCredentials = (token) ? false : this.useCredentials;

        return this.$http(request).then(
            (response) => response.data,
            (error: any) => {
                return this.handleError(error);
            }) as any;
    }

    /**
     * Gets a HTML-message from server using the API
     *
     * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
     * and starts with a forward slash, e.g. '/calculator/new'.
     *
     * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
     */
    public getHTML(method: string): Promise<string> {

        const request: any = {}; // angular.IRequestConfig

        const token: string = this.getCurrentToken();
        if (token) {
            request.headers = { Authorization: "Bearer " + token };
        }

        request.url = this.getUrl(method);
        request.method = "GET";
        request.responseType = "text";
        request.withCredentials = (token) ? false : this.useCredentials;

        return (this.$http(request).then(
            (response) => {
                return response.data;
            },
            (error: any) => {
                return this.handleError(error);
            }) as any
        );
    }

    /**
     * POSTS data to server and receives back a JSON-message.
     *
     * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
     * and starts with a forward slash, e.g. '/calculator/new'.
     * @param data - The data that is posted to the server.
     *
     * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
     */
    public postJSON(method: string, data: any): Promise<any> {

        const request: any = {}; // angular.IRequestConfig

        const token: string = this.getCurrentToken();
        if (token) {
            request.headers = { Authorization: "Bearer " + token };
        }

        request.url = this.getUrl(method);
        request.method = "POST";
        request.data = data;
        request.responseType = "json";
        request.withCredentials = (token) ? false : this.useCredentials;

        return (this.$http(request).then(
            (response) => response.data,
            (error: any) => {
                return this.handleError(error);
            }) as any
        );
    }

    /**
     * POSTS data to server and receives back HTML.
     *
     * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
     * and starts with a forward slash, e.g. '/calculator/new'.
     * @param data - The data that is posted to the server.
     *
     * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
     */
    public postHTML(method: string, data: any): Promise<string> {

        const request: any = {}; // angular.IRequestConfig

        const token: string = this.getCurrentToken();
        if (token) {
            request.headers = { Authorization: "Bearer " + token };
        }

        request.url = this.getUrl(method);
        request.method = "POST";
        request.data = data;
        request.responseType = "text";
        request.withCredentials = (token) ? false : this.useCredentials;

        return (this.$http(request).then(
            (response) => response.data,
            (error: any) => {
                return this.handleError(error);
            }) as any
        );
    }

    /**
     * Sends a DELETE-message to server using the API
     *
     * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
     * and starts with a forward slash, e.g. '/calculator/new'.
     *
     * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
     */
    public remove(method: string): Promise<any> {

        const request: any = {}; // angular.IRequestConfig

        const token: string = this.getCurrentToken();
        if (token) {
            request.headers = { Authorization: "Bearer " + token };
        }

        request.url = this.getUrl(method);
        request.method = "DELETE";
        request.responseType = "json";
        request.withCredentials = (token) ? false : this.useCredentials;

        return (this.$http(request).then(
            (response) => {
                return response.data;
            },
            (error: any) => {
                return this.handleError(error);
            }) as any
        );
    }

    /**
     * Gets the current token.
     * Will check the salaxy-token cookie if the token is persisted there
     */
    public getCurrentToken(): string {
        if (!this.token && this.useCookie) {
            this.token = new Cookies().get("salaxy-token") || "";
        }
        return this.token;
    }

    /** Gets the status of the current token. */
    public getTokenStatus(): "noToken" | "ok" | "expired" | "invalid" {
      const token = this.getCurrentToken();
      return Token.validate(token);
    }

    /**
     * Sets the current token. Persists it to cookie until the browser window
     *
     * @param token - the authentication token to persist.
     */
    public setCurrentToken(token: string): void {
        if (this.useCookie) {
            new Cookies().setCookie("salaxy-token", token || "");
        }
        this.token = token;
    }

    /** If missing, append the API server address to the given url method string */
    private getUrl(method: string): string {
      if (!method || method.trim() === "") {
        return null;
      }
      if (method.toLowerCase().startsWith("http")) {
        return method;
      }
      if (method.toLowerCase().startsWith("/v")) {
        return this.getServerAddress() + method;
      }
      return this.getApiAddress() + method;
    }

    private handleError(errorThrown: any): Promise<any> {
        let msg: string;
        if (errorThrown) {
            if (errorThrown.data && errorThrown.data.messageHtml) {
                msg = `<b>${errorThrown.data.error}:</b><br/>${errorThrown.data.messageHtml}`;
            } else if (errorThrown.data && errorThrown.data.message) {
                msg = errorThrown.data.message;
            } else {
                msg = `HTTP error: ${errorThrown.statusText} (${errorThrown.status})`;
            }
        } else {
            msg = "Unexpected error in HTTP request";
        }
        if (AjaxNg1.alertService) {
            if (AjaxNg1.alertService.catchError) {
                return AjaxNg1.alertService.catchError(errorThrown);
            } else {
                AjaxNg1.alertService.addError(msg);
            }
        }
        return this.$q.reject(errorThrown) as any;
    }
}
