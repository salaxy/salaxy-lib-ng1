/** 
 * Less variables for src/less/bootstrap-less/variables.less
 * @ignore
 */
export const bootstrapLessVariables = [
  {
    "heading": "Colors",
    "id": "colors",
    "customizable": true,
    "docstring": {
      "html": "Gray and brand colors for use across Bootstrap."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@gray-base",
            "defaultValue": "#000",
            "docstring": null
          },
          {
            "name": "@gray-darker",
            "defaultValue": "lighten(@gray-base, 13.5%)",
            "docstring": null
          },
          {
            "name": "@gray-dark",
            "defaultValue": "lighten(@gray-base, 20%)",
            "docstring": null
          },
          {
            "name": "@gray",
            "defaultValue": "lighten(@gray-base, 33.5%)",
            "docstring": null
          },
          {
            "name": "@gray-light",
            "defaultValue": "lighten(@gray-base, 46.7%)",
            "docstring": null
          },
          {
            "name": "@gray-lighter",
            "defaultValue": "lighten(@gray-base, 93.5%)",
            "docstring": null
          },
          {
            "name": "@brand-primary",
            "defaultValue": "darken(#428bca, 6.5%)",
            "docstring": null
          },
          {
            "name": "@brand-success",
            "defaultValue": "#5cb85c",
            "docstring": null
          },
          {
            "name": "@brand-info",
            "defaultValue": "#5bc0de",
            "docstring": null
          },
          {
            "name": "@brand-warning",
            "defaultValue": "#f0ad4e",
            "docstring": null
          },
          {
            "name": "@brand-danger",
            "defaultValue": "#d9534f",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Scaffolding",
    "id": "scaffolding",
    "customizable": true,
    "docstring": {
      "html": "Settings for some of the most global styles."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@body-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Background color for <code>&lt;body&gt;</code>."
            }
          },
          {
            "name": "@text-color",
            "defaultValue": "@gray-dark",
            "docstring": {
              "html": "Global text color on <code>&lt;body&gt;</code>."
            }
          },
          {
            "name": "@link-color",
            "defaultValue": "@brand-primary",
            "docstring": {
              "html": "Global textual link color."
            }
          },
          {
            "name": "@link-hover-color",
            "defaultValue": "darken(@link-color, 15%)",
            "docstring": {
              "html": "Link hover color set via <code>darken()</code> function."
            }
          },
          {
            "name": "@link-hover-decoration",
            "defaultValue": "underline",
            "docstring": {
              "html": "Link hover decoration."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Typography",
    "id": "typography",
    "customizable": true,
    "docstring": {
      "html": "Font, line-height, and color for body text, headings, and more."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@font-family-sans-serif",
            "defaultValue": "\"Helvetica Neue\", Helvetica, Arial, sans-serif",
            "docstring": null
          },
          {
            "name": "@font-family-serif",
            "defaultValue": "Georgia, \"Times New Roman\", Times, serif",
            "docstring": null
          },
          {
            "name": "@font-family-monospace",
            "defaultValue": "Menlo, Monaco, Consolas, \"Courier New\", monospace",
            "docstring": {
              "html": "Default monospace fonts for <code>&lt;code&gt;</code>, <code>&lt;kbd&gt;</code>, and <code>&lt;pre&gt;</code>."
            }
          },
          {
            "name": "@font-family-base",
            "defaultValue": "@font-family-sans-serif",
            "docstring": null
          },
          {
            "name": "@font-size-base",
            "defaultValue": "14px",
            "docstring": null
          },
          {
            "name": "@font-size-large",
            "defaultValue": "ceil((@font-size-base * 1.25))",
            "docstring": null
          },
          {
            "name": "@font-size-small",
            "defaultValue": "ceil((@font-size-base * .85))",
            "docstring": null
          },
          {
            "name": "@font-size-h1",
            "defaultValue": "floor((@font-size-base * 2.6))",
            "docstring": null
          },
          {
            "name": "@font-size-h2",
            "defaultValue": "floor((@font-size-base * 2.15))",
            "docstring": null
          },
          {
            "name": "@font-size-h3",
            "defaultValue": "ceil((@font-size-base * 1.7))",
            "docstring": null
          },
          {
            "name": "@font-size-h4",
            "defaultValue": "ceil((@font-size-base * 1.25))",
            "docstring": null
          },
          {
            "name": "@font-size-h5",
            "defaultValue": "@font-size-base",
            "docstring": null
          },
          {
            "name": "@font-size-h6",
            "defaultValue": "ceil((@font-size-base * .85))",
            "docstring": null
          },
          {
            "name": "@line-height-base",
            "defaultValue": "1.428571429",
            "docstring": {
              "html": "Unit-less <code>line-height</code> for use in components like buttons."
            }
          },
          {
            "name": "@line-height-computed",
            "defaultValue": "floor((@font-size-base * @line-height-base))",
            "docstring": {
              "html": "Computed &quot;line-height&quot; (<code>font-size</code> * <code>line-height</code>) for use with <code>margin</code>, <code>padding</code>, etc."
            }
          },
          {
            "name": "@headings-font-family",
            "defaultValue": "inherit",
            "docstring": {
              "html": "By default, this inherits from the <code>&lt;body&gt;</code>."
            }
          },
          {
            "name": "@headings-font-weight",
            "defaultValue": "500",
            "docstring": null
          },
          {
            "name": "@headings-line-height",
            "defaultValue": "1.1",
            "docstring": null
          },
          {
            "name": "@headings-color",
            "defaultValue": "inherit",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Iconography",
    "id": "iconography",
    "customizable": true,
    "docstring": {
      "html": "Specify custom location and filename of the included Glyphicons icon font. Useful for those including Bootstrap via Bower."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@icon-font-path",
            "defaultValue": "\"../fonts/\"",
            "docstring": {
              "html": "Load fonts from this directory."
            }
          },
          {
            "name": "@icon-font-name",
            "defaultValue": "\"glyphicons-halflings-regular\"",
            "docstring": {
              "html": "File name for all font files."
            }
          },
          {
            "name": "@icon-font-svg-id",
            "defaultValue": "\"glyphicons_halflingsregular\"",
            "docstring": {
              "html": "Element ID within SVG icon file."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Components",
    "id": "components",
    "customizable": true,
    "docstring": {
      "html": "Define common padding and border radius sizes and more. Values based on 14px text and 1.428 line-height (~20px to start)."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@padding-base-vertical",
            "defaultValue": "6px",
            "docstring": null
          },
          {
            "name": "@padding-base-horizontal",
            "defaultValue": "12px",
            "docstring": null
          },
          {
            "name": "@padding-large-vertical",
            "defaultValue": "10px",
            "docstring": null
          },
          {
            "name": "@padding-large-horizontal",
            "defaultValue": "16px",
            "docstring": null
          },
          {
            "name": "@padding-small-vertical",
            "defaultValue": "5px",
            "docstring": null
          },
          {
            "name": "@padding-small-horizontal",
            "defaultValue": "10px",
            "docstring": null
          },
          {
            "name": "@padding-xs-vertical",
            "defaultValue": "1px",
            "docstring": null
          },
          {
            "name": "@padding-xs-horizontal",
            "defaultValue": "5px",
            "docstring": null
          },
          {
            "name": "@line-height-large",
            "defaultValue": "1.3333333",
            "docstring": null
          },
          {
            "name": "@line-height-small",
            "defaultValue": "1.5",
            "docstring": null
          },
          {
            "name": "@border-radius-base",
            "defaultValue": "4px",
            "docstring": null
          },
          {
            "name": "@border-radius-large",
            "defaultValue": "6px",
            "docstring": null
          },
          {
            "name": "@border-radius-small",
            "defaultValue": "3px",
            "docstring": null
          },
          {
            "name": "@component-active-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Global color for active items (e.g., navs or dropdowns)."
            }
          },
          {
            "name": "@component-active-bg",
            "defaultValue": "@brand-primary",
            "docstring": {
              "html": "Global background color for active items (e.g., navs or dropdowns)."
            }
          },
          {
            "name": "@caret-width-base",
            "defaultValue": "4px",
            "docstring": {
              "html": "Width of the <code>border</code> for generating carets that indicate dropdowns."
            }
          },
          {
            "name": "@caret-width-large",
            "defaultValue": "5px",
            "docstring": {
              "html": "Carets increase slightly in size for larger components."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Tables",
    "id": "tables",
    "customizable": true,
    "docstring": {
      "html": "Customizes the <code>.table</code> component with basic values, each used across all table variations."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@table-cell-padding",
            "defaultValue": "8px",
            "docstring": {
              "html": "Padding for <code>&lt;th&gt;</code>s and <code>&lt;td&gt;</code>s."
            }
          },
          {
            "name": "@table-condensed-cell-padding",
            "defaultValue": "5px",
            "docstring": {
              "html": "Padding for cells in <code>.table-condensed</code>."
            }
          },
          {
            "name": "@table-bg",
            "defaultValue": "transparent",
            "docstring": {
              "html": "Default background color used for all tables."
            }
          },
          {
            "name": "@table-bg-accent",
            "defaultValue": "#f9f9f9",
            "docstring": {
              "html": "Background color used for <code>.table-striped</code>."
            }
          },
          {
            "name": "@table-bg-hover",
            "defaultValue": "#f5f5f5",
            "docstring": {
              "html": "Background color used for <code>.table-hover</code>."
            }
          },
          {
            "name": "@table-bg-active",
            "defaultValue": "@table-bg-hover",
            "docstring": null
          },
          {
            "name": "@table-border-color",
            "defaultValue": "#ddd",
            "docstring": {
              "html": "Border color for table and cell borders."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Buttons",
    "id": "buttons",
    "customizable": true,
    "docstring": {
      "html": "For each of Bootstrap's buttons, define text, background and border color."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@btn-font-weight",
            "defaultValue": "normal",
            "docstring": null
          },
          {
            "name": "@btn-default-color",
            "defaultValue": "#333",
            "docstring": null
          },
          {
            "name": "@btn-default-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-default-border",
            "defaultValue": "#ccc",
            "docstring": null
          },
          {
            "name": "@btn-primary-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-primary-bg",
            "defaultValue": "@brand-primary",
            "docstring": null
          },
          {
            "name": "@btn-primary-border",
            "defaultValue": "darken(@btn-primary-bg, 5%)",
            "docstring": null
          },
          {
            "name": "@btn-success-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-success-bg",
            "defaultValue": "@brand-success",
            "docstring": null
          },
          {
            "name": "@btn-success-border",
            "defaultValue": "darken(@btn-success-bg, 5%)",
            "docstring": null
          },
          {
            "name": "@btn-info-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-info-bg",
            "defaultValue": "@brand-info",
            "docstring": null
          },
          {
            "name": "@btn-info-border",
            "defaultValue": "darken(@btn-info-bg, 5%)",
            "docstring": null
          },
          {
            "name": "@btn-warning-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-warning-bg",
            "defaultValue": "@brand-warning",
            "docstring": null
          },
          {
            "name": "@btn-warning-border",
            "defaultValue": "darken(@btn-warning-bg, 5%)",
            "docstring": null
          },
          {
            "name": "@btn-danger-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@btn-danger-bg",
            "defaultValue": "@brand-danger",
            "docstring": null
          },
          {
            "name": "@btn-danger-border",
            "defaultValue": "darken(@btn-danger-bg, 5%)",
            "docstring": null
          },
          {
            "name": "@btn-link-disabled-color",
            "defaultValue": "@gray-light",
            "docstring": null
          },
          {
            "name": "@btn-border-radius-base",
            "defaultValue": "@border-radius-base",
            "docstring": null
          },
          {
            "name": "@btn-border-radius-large",
            "defaultValue": "@border-radius-large",
            "docstring": null
          },
          {
            "name": "@btn-border-radius-small",
            "defaultValue": "@border-radius-small",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Forms",
    "id": "forms",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@input-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "<code>&lt;input&gt;</code> background color"
            }
          },
          {
            "name": "@input-bg-disabled",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "<code>&lt;input disabled&gt;</code> background color"
            }
          },
          {
            "name": "@input-color",
            "defaultValue": "@gray",
            "docstring": {
              "html": "Text color for <code>&lt;input&gt;</code>s"
            }
          },
          {
            "name": "@input-border",
            "defaultValue": "#ccc",
            "docstring": {
              "html": "<code>&lt;input&gt;</code> border color"
            }
          },
          {
            "name": "@input-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": {
              "html": "Default <code>.form-control</code> border radius"
            }
          },
          {
            "name": "@input-border-radius-large",
            "defaultValue": "@border-radius-large",
            "docstring": {
              "html": "Large <code>.form-control</code> border radius"
            }
          },
          {
            "name": "@input-border-radius-small",
            "defaultValue": "@border-radius-small",
            "docstring": {
              "html": "Small <code>.form-control</code> border radius"
            }
          },
          {
            "name": "@input-border-focus",
            "defaultValue": "#66afe9",
            "docstring": {
              "html": "Border color for inputs on focus"
            }
          },
          {
            "name": "@input-color-placeholder",
            "defaultValue": "#999",
            "docstring": {
              "html": "Placeholder text color"
            }
          },
          {
            "name": "@input-height-base",
            "defaultValue": "(@line-height-computed + (@padding-base-vertical * 2) + 2)",
            "docstring": {
              "html": "Default <code>.form-control</code> height"
            }
          },
          {
            "name": "@input-height-large",
            "defaultValue": "(ceil(@font-size-large * @line-height-large) + (@padding-large-vertical * 2) + 2)",
            "docstring": {
              "html": "Large <code>.form-control</code> height"
            }
          },
          {
            "name": "@input-height-small",
            "defaultValue": "(floor(@font-size-small * @line-height-small) + (@padding-small-vertical * 2) + 2)",
            "docstring": {
              "html": "Small <code>.form-control</code> height"
            }
          },
          {
            "name": "@form-group-margin-bottom",
            "defaultValue": "15px",
            "docstring": {
              "html": "<code>.form-group</code> margin"
            }
          },
          {
            "name": "@legend-color",
            "defaultValue": "@gray-dark",
            "docstring": null
          },
          {
            "name": "@legend-border-color",
            "defaultValue": "#e5e5e5",
            "docstring": null
          },
          {
            "name": "@input-group-addon-bg",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "Background color for textual input addons"
            }
          },
          {
            "name": "@input-group-addon-border-color",
            "defaultValue": "@input-border",
            "docstring": {
              "html": "Border color for textual input addons"
            }
          },
          {
            "name": "@cursor-disabled",
            "defaultValue": "not-allowed",
            "docstring": {
              "html": "Disabled cursor for form controls and buttons."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Dropdowns",
    "id": "dropdowns",
    "customizable": true,
    "docstring": {
      "html": "Dropdown menu container and contents."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@dropdown-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Background for the dropdown menu."
            }
          },
          {
            "name": "@dropdown-border",
            "defaultValue": "rgba(0, 0, 0, .15)",
            "docstring": {
              "html": "Dropdown menu <code>border-color</code>."
            }
          },
          {
            "name": "@dropdown-fallback-border",
            "defaultValue": "#ccc",
            "docstring": {
              "html": "Dropdown menu <code>border-color</code> <strong>for IE8</strong>."
            }
          },
          {
            "name": "@dropdown-divider-bg",
            "defaultValue": "#e5e5e5",
            "docstring": {
              "html": "Divider color for between dropdown items."
            }
          },
          {
            "name": "@dropdown-link-color",
            "defaultValue": "@gray-dark",
            "docstring": {
              "html": "Dropdown link text color."
            }
          },
          {
            "name": "@dropdown-link-hover-color",
            "defaultValue": "darken(@gray-dark, 5%)",
            "docstring": {
              "html": "Hover color for dropdown links."
            }
          },
          {
            "name": "@dropdown-link-hover-bg",
            "defaultValue": "#f5f5f5",
            "docstring": {
              "html": "Hover background for dropdown links."
            }
          },
          {
            "name": "@dropdown-link-active-color",
            "defaultValue": "@component-active-color",
            "docstring": {
              "html": "Active dropdown menu item text color."
            }
          },
          {
            "name": "@dropdown-link-active-bg",
            "defaultValue": "@component-active-bg",
            "docstring": {
              "html": "Active dropdown menu item background color."
            }
          },
          {
            "name": "@dropdown-link-disabled-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Disabled dropdown menu item background color."
            }
          },
          {
            "name": "@dropdown-header-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Text color for headers within dropdown menus."
            }
          },
          {
            "name": "@dropdown-caret-color",
            "defaultValue": "#000",
            "docstring": {
              "html": "Deprecated <code>@dropdown-caret-color</code> as of v3.1.0"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Z-index master list",
    "id": "z-index-master-list",
    "customizable": false,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@zindex-navbar",
            "defaultValue": "1000",
            "docstring": null
          },
          {
            "name": "@zindex-dropdown",
            "defaultValue": "1000",
            "docstring": null
          },
          {
            "name": "@zindex-popover",
            "defaultValue": "1060",
            "docstring": null
          },
          {
            "name": "@zindex-tooltip",
            "defaultValue": "1070",
            "docstring": null
          },
          {
            "name": "@zindex-navbar-fixed",
            "defaultValue": "1030",
            "docstring": null
          },
          {
            "name": "@zindex-modal-background",
            "defaultValue": "1040",
            "docstring": null
          },
          {
            "name": "@zindex-modal",
            "defaultValue": "1050",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Media queries breakpoints",
    "id": "media-queries-breakpoints",
    "customizable": true,
    "docstring": {
      "html": "Define the breakpoints at which your layout will change, adapting to different screen sizes."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@screen-xs",
            "defaultValue": "480px",
            "docstring": {
              "html": "Deprecated <code>@screen-xs</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-xs-min",
            "defaultValue": "@screen-xs",
            "docstring": {
              "html": "Deprecated <code>@screen-xs-min</code> as of v3.2.0"
            }
          },
          {
            "name": "@screen-phone",
            "defaultValue": "@screen-xs-min",
            "docstring": {
              "html": "Deprecated <code>@screen-phone</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-sm",
            "defaultValue": "768px",
            "docstring": {
              "html": "Deprecated <code>@screen-sm</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-sm-min",
            "defaultValue": "@screen-sm",
            "docstring": null
          },
          {
            "name": "@screen-tablet",
            "defaultValue": "@screen-sm-min",
            "docstring": {
              "html": "Deprecated <code>@screen-tablet</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-md",
            "defaultValue": "992px",
            "docstring": {
              "html": "Deprecated <code>@screen-md</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-md-min",
            "defaultValue": "@screen-md",
            "docstring": null
          },
          {
            "name": "@screen-desktop",
            "defaultValue": "@screen-md-min",
            "docstring": {
              "html": "Deprecated <code>@screen-desktop</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-lg",
            "defaultValue": "1200px",
            "docstring": {
              "html": "Deprecated <code>@screen-lg</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-lg-min",
            "defaultValue": "@screen-lg",
            "docstring": null
          },
          {
            "name": "@screen-lg-desktop",
            "defaultValue": "@screen-lg-min",
            "docstring": {
              "html": "Deprecated <code>@screen-lg-desktop</code> as of v3.0.1"
            }
          },
          {
            "name": "@screen-xs-max",
            "defaultValue": "(@screen-sm-min - 1)",
            "docstring": null
          },
          {
            "name": "@screen-sm-max",
            "defaultValue": "(@screen-md-min - 1)",
            "docstring": null
          },
          {
            "name": "@screen-md-max",
            "defaultValue": "(@screen-lg-min - 1)",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Grid system",
    "id": "grid-system",
    "customizable": true,
    "docstring": {
      "html": "Define your custom responsive grid."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@grid-columns",
            "defaultValue": "12",
            "docstring": {
              "html": "Number of columns in the grid."
            }
          },
          {
            "name": "@grid-gutter-width",
            "defaultValue": "30px",
            "docstring": {
              "html": "Padding between columns. Gets divided in half for the left and right."
            }
          },
          {
            "name": "@grid-float-breakpoint",
            "defaultValue": "@screen-sm-min",
            "docstring": {
              "html": "Point at which the navbar becomes uncollapsed."
            }
          },
          {
            "name": "@grid-float-breakpoint-max",
            "defaultValue": "(@grid-float-breakpoint - 1)",
            "docstring": {
              "html": "Point at which the navbar begins collapsing."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Container sizes",
    "id": "container-sizes",
    "customizable": true,
    "docstring": {
      "html": "Define the maximum width of <code>.container</code> for different screen sizes."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@container-tablet",
            "defaultValue": "(720px + @grid-gutter-width)",
            "docstring": null
          },
          {
            "name": "@container-sm",
            "defaultValue": "@container-tablet",
            "docstring": {
              "html": "For <code>@screen-sm-min</code> and up."
            }
          },
          {
            "name": "@container-desktop",
            "defaultValue": "(940px + @grid-gutter-width)",
            "docstring": null
          },
          {
            "name": "@container-md",
            "defaultValue": "@container-desktop",
            "docstring": {
              "html": "For <code>@screen-md-min</code> and up."
            }
          },
          {
            "name": "@container-large-desktop",
            "defaultValue": "(1140px + @grid-gutter-width)",
            "docstring": null
          },
          {
            "name": "@container-lg",
            "defaultValue": "@container-large-desktop",
            "docstring": {
              "html": "For <code>@screen-lg-min</code> and up."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Navbar",
    "id": "navbar",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@navbar-height",
            "defaultValue": "50px",
            "docstring": null
          },
          {
            "name": "@navbar-margin-bottom",
            "defaultValue": "@line-height-computed",
            "docstring": null
          },
          {
            "name": "@navbar-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": null
          },
          {
            "name": "@navbar-padding-horizontal",
            "defaultValue": "floor((@grid-gutter-width / 2))",
            "docstring": null
          },
          {
            "name": "@navbar-padding-vertical",
            "defaultValue": "((@navbar-height - @line-height-computed) / 2)",
            "docstring": null
          },
          {
            "name": "@navbar-collapse-max-height",
            "defaultValue": "340px",
            "docstring": null
          },
          {
            "name": "@navbar-default-color",
            "defaultValue": "#777",
            "docstring": null
          },
          {
            "name": "@navbar-default-bg",
            "defaultValue": "#f8f8f8",
            "docstring": null
          },
          {
            "name": "@navbar-default-border",
            "defaultValue": "darken(@navbar-default-bg, 6.5%)",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-color",
            "defaultValue": "#777",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-hover-color",
            "defaultValue": "#333",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-hover-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-active-color",
            "defaultValue": "#555",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-active-bg",
            "defaultValue": "darken(@navbar-default-bg, 6.5%)",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-disabled-color",
            "defaultValue": "#ccc",
            "docstring": null
          },
          {
            "name": "@navbar-default-link-disabled-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-default-brand-color",
            "defaultValue": "@navbar-default-link-color",
            "docstring": null
          },
          {
            "name": "@navbar-default-brand-hover-color",
            "defaultValue": "darken(@navbar-default-brand-color, 10%)",
            "docstring": null
          },
          {
            "name": "@navbar-default-brand-hover-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-default-toggle-hover-bg",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@navbar-default-toggle-icon-bar-bg",
            "defaultValue": "#888",
            "docstring": null
          },
          {
            "name": "@navbar-default-toggle-border-color",
            "defaultValue": "#ddd",
            "docstring": null
          }
        ]
      },
      {
        "heading": "Inverted navbar",
        "id": "inverted-navbar",
        "variables": [
          {
            "name": "@navbar-inverse-color",
            "defaultValue": "lighten(@gray-light, 15%)",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-bg",
            "defaultValue": "#222",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-border",
            "defaultValue": "darken(@navbar-inverse-bg, 10%)",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-color",
            "defaultValue": "lighten(@gray-light, 15%)",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-hover-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-hover-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-active-color",
            "defaultValue": "@navbar-inverse-link-hover-color",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-active-bg",
            "defaultValue": "darken(@navbar-inverse-bg, 10%)",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-disabled-color",
            "defaultValue": "#444",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-link-disabled-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-brand-color",
            "defaultValue": "@navbar-inverse-link-color",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-brand-hover-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-brand-hover-bg",
            "defaultValue": "transparent",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-toggle-hover-bg",
            "defaultValue": "#333",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-toggle-icon-bar-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@navbar-inverse-toggle-border-color",
            "defaultValue": "#333",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Navs",
    "id": "navs",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "Shared nav styles",
        "id": "shared-nav-styles",
        "variables": [
          {
            "name": "@nav-link-padding",
            "defaultValue": "10px 15px",
            "docstring": null
          },
          {
            "name": "@nav-link-hover-bg",
            "defaultValue": "@gray-lighter",
            "docstring": null
          },
          {
            "name": "@nav-disabled-link-color",
            "defaultValue": "@gray-light",
            "docstring": null
          },
          {
            "name": "@nav-disabled-link-hover-color",
            "defaultValue": "@gray-light",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Tabs",
    "id": "tabs",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@nav-tabs-border-color",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@nav-tabs-link-hover-border-color",
            "defaultValue": "@gray-lighter",
            "docstring": null
          },
          {
            "name": "@nav-tabs-active-link-hover-bg",
            "defaultValue": "@body-bg",
            "docstring": null
          },
          {
            "name": "@nav-tabs-active-link-hover-color",
            "defaultValue": "@gray",
            "docstring": null
          },
          {
            "name": "@nav-tabs-active-link-hover-border-color",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@nav-tabs-justified-link-border-color",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@nav-tabs-justified-active-link-border-color",
            "defaultValue": "@body-bg",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Pills",
    "id": "pills",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@nav-pills-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": null
          },
          {
            "name": "@nav-pills-active-link-hover-bg",
            "defaultValue": "@component-active-bg",
            "docstring": null
          },
          {
            "name": "@nav-pills-active-link-hover-color",
            "defaultValue": "@component-active-color",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Pagination",
    "id": "pagination",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@pagination-color",
            "defaultValue": "@link-color",
            "docstring": null
          },
          {
            "name": "@pagination-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@pagination-border",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@pagination-hover-color",
            "defaultValue": "@link-hover-color",
            "docstring": null
          },
          {
            "name": "@pagination-hover-bg",
            "defaultValue": "@gray-lighter",
            "docstring": null
          },
          {
            "name": "@pagination-hover-border",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@pagination-active-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@pagination-active-bg",
            "defaultValue": "@brand-primary",
            "docstring": null
          },
          {
            "name": "@pagination-active-border",
            "defaultValue": "@brand-primary",
            "docstring": null
          },
          {
            "name": "@pagination-disabled-color",
            "defaultValue": "@gray-light",
            "docstring": null
          },
          {
            "name": "@pagination-disabled-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@pagination-disabled-border",
            "defaultValue": "#ddd",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Pager",
    "id": "pager",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@pager-bg",
            "defaultValue": "@pagination-bg",
            "docstring": null
          },
          {
            "name": "@pager-border",
            "defaultValue": "@pagination-border",
            "docstring": null
          },
          {
            "name": "@pager-border-radius",
            "defaultValue": "15px",
            "docstring": null
          },
          {
            "name": "@pager-hover-bg",
            "defaultValue": "@pagination-hover-bg",
            "docstring": null
          },
          {
            "name": "@pager-active-bg",
            "defaultValue": "@pagination-active-bg",
            "docstring": null
          },
          {
            "name": "@pager-active-color",
            "defaultValue": "@pagination-active-color",
            "docstring": null
          },
          {
            "name": "@pager-disabled-color",
            "defaultValue": "@pagination-disabled-color",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Jumbotron",
    "id": "jumbotron",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@jumbotron-padding",
            "defaultValue": "30px",
            "docstring": null
          },
          {
            "name": "@jumbotron-color",
            "defaultValue": "inherit",
            "docstring": null
          },
          {
            "name": "@jumbotron-bg",
            "defaultValue": "@gray-lighter",
            "docstring": null
          },
          {
            "name": "@jumbotron-heading-color",
            "defaultValue": "inherit",
            "docstring": null
          },
          {
            "name": "@jumbotron-font-size",
            "defaultValue": "ceil((@font-size-base * 1.5))",
            "docstring": null
          },
          {
            "name": "@jumbotron-heading-font-size",
            "defaultValue": "ceil((@font-size-base * 4.5))",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Form states and alerts",
    "id": "form-states-and-alerts",
    "customizable": true,
    "docstring": {
      "html": "Define colors for form feedback states and, by default, alerts."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@state-success-text",
            "defaultValue": "#3c763d",
            "docstring": null
          },
          {
            "name": "@state-success-bg",
            "defaultValue": "#dff0d8",
            "docstring": null
          },
          {
            "name": "@state-success-border",
            "defaultValue": "darken(spin(@state-success-bg, -10), 5%)",
            "docstring": null
          },
          {
            "name": "@state-info-text",
            "defaultValue": "#31708f",
            "docstring": null
          },
          {
            "name": "@state-info-bg",
            "defaultValue": "#d9edf7",
            "docstring": null
          },
          {
            "name": "@state-info-border",
            "defaultValue": "darken(spin(@state-info-bg, -10), 7%)",
            "docstring": null
          },
          {
            "name": "@state-warning-text",
            "defaultValue": "#8a6d3b",
            "docstring": null
          },
          {
            "name": "@state-warning-bg",
            "defaultValue": "#fcf8e3",
            "docstring": null
          },
          {
            "name": "@state-warning-border",
            "defaultValue": "darken(spin(@state-warning-bg, -10), 5%)",
            "docstring": null
          },
          {
            "name": "@state-danger-text",
            "defaultValue": "#a94442",
            "docstring": null
          },
          {
            "name": "@state-danger-bg",
            "defaultValue": "#f2dede",
            "docstring": null
          },
          {
            "name": "@state-danger-border",
            "defaultValue": "darken(spin(@state-danger-bg, -10), 5%)",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Tooltips",
    "id": "tooltips",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@tooltip-max-width",
            "defaultValue": "200px",
            "docstring": {
              "html": "Tooltip max width"
            }
          },
          {
            "name": "@tooltip-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Tooltip text color"
            }
          },
          {
            "name": "@tooltip-bg",
            "defaultValue": "#000",
            "docstring": {
              "html": "Tooltip background color"
            }
          },
          {
            "name": "@tooltip-opacity",
            "defaultValue": ".9",
            "docstring": null
          },
          {
            "name": "@tooltip-arrow-width",
            "defaultValue": "5px",
            "docstring": {
              "html": "Tooltip arrow width"
            }
          },
          {
            "name": "@tooltip-arrow-color",
            "defaultValue": "@tooltip-bg",
            "docstring": {
              "html": "Tooltip arrow color"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Popovers",
    "id": "popovers",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@popover-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Popover body background color"
            }
          },
          {
            "name": "@popover-max-width",
            "defaultValue": "276px",
            "docstring": {
              "html": "Popover maximum width"
            }
          },
          {
            "name": "@popover-border-color",
            "defaultValue": "rgba(0, 0, 0, .2)",
            "docstring": {
              "html": "Popover border color"
            }
          },
          {
            "name": "@popover-fallback-border-color",
            "defaultValue": "#ccc",
            "docstring": {
              "html": "Popover fallback border color"
            }
          },
          {
            "name": "@popover-title-bg",
            "defaultValue": "darken(@popover-bg, 3%)",
            "docstring": {
              "html": "Popover title background color"
            }
          },
          {
            "name": "@popover-arrow-width",
            "defaultValue": "10px",
            "docstring": {
              "html": "Popover arrow width"
            }
          },
          {
            "name": "@popover-arrow-color",
            "defaultValue": "@popover-bg",
            "docstring": {
              "html": "Popover arrow color"
            }
          },
          {
            "name": "@popover-arrow-outer-width",
            "defaultValue": "(@popover-arrow-width + 1)",
            "docstring": {
              "html": "Popover outer arrow width"
            }
          },
          {
            "name": "@popover-arrow-outer-color",
            "defaultValue": "fadein(@popover-border-color, 5%)",
            "docstring": {
              "html": "Popover outer arrow color"
            }
          },
          {
            "name": "@popover-arrow-outer-fallback-color",
            "defaultValue": "darken(@popover-fallback-border-color, 20%)",
            "docstring": {
              "html": "Popover outer arrow fallback color"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Labels",
    "id": "labels",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@label-default-bg",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Default label background color"
            }
          },
          {
            "name": "@label-primary-bg",
            "defaultValue": "@brand-primary",
            "docstring": {
              "html": "Primary label background color"
            }
          },
          {
            "name": "@label-success-bg",
            "defaultValue": "@brand-success",
            "docstring": {
              "html": "Success label background color"
            }
          },
          {
            "name": "@label-info-bg",
            "defaultValue": "@brand-info",
            "docstring": {
              "html": "Info label background color"
            }
          },
          {
            "name": "@label-warning-bg",
            "defaultValue": "@brand-warning",
            "docstring": {
              "html": "Warning label background color"
            }
          },
          {
            "name": "@label-danger-bg",
            "defaultValue": "@brand-danger",
            "docstring": {
              "html": "Danger label background color"
            }
          },
          {
            "name": "@label-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Default label text color"
            }
          },
          {
            "name": "@label-link-hover-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Default text color of a linked label"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Modals",
    "id": "modals",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@modal-inner-padding",
            "defaultValue": "15px",
            "docstring": {
              "html": "Padding applied to the modal body"
            }
          },
          {
            "name": "@modal-title-padding",
            "defaultValue": "15px",
            "docstring": {
              "html": "Padding applied to the modal title"
            }
          },
          {
            "name": "@modal-title-line-height",
            "defaultValue": "@line-height-base",
            "docstring": {
              "html": "Modal title line-height"
            }
          },
          {
            "name": "@modal-content-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Background color of modal content area"
            }
          },
          {
            "name": "@modal-content-border-color",
            "defaultValue": "rgba(0, 0, 0, .2)",
            "docstring": {
              "html": "Modal content border color"
            }
          },
          {
            "name": "@modal-content-fallback-border-color",
            "defaultValue": "#999",
            "docstring": {
              "html": "Modal content border color <strong>for IE8</strong>"
            }
          },
          {
            "name": "@modal-backdrop-bg",
            "defaultValue": "#000",
            "docstring": {
              "html": "Modal backdrop background color"
            }
          },
          {
            "name": "@modal-backdrop-opacity",
            "defaultValue": ".5",
            "docstring": {
              "html": "Modal backdrop opacity"
            }
          },
          {
            "name": "@modal-header-border-color",
            "defaultValue": "#e5e5e5",
            "docstring": {
              "html": "Modal header border color"
            }
          },
          {
            "name": "@modal-footer-border-color",
            "defaultValue": "@modal-header-border-color",
            "docstring": {
              "html": "Modal footer border color"
            }
          },
          {
            "name": "@modal-lg",
            "defaultValue": "900px",
            "docstring": null
          },
          {
            "name": "@modal-md",
            "defaultValue": "600px",
            "docstring": null
          },
          {
            "name": "@modal-sm",
            "defaultValue": "300px",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Alerts",
    "id": "alerts",
    "customizable": true,
    "docstring": {
      "html": "Define alert colors, border radius, and padding."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@alert-padding",
            "defaultValue": "15px",
            "docstring": null
          },
          {
            "name": "@alert-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": null
          },
          {
            "name": "@alert-link-font-weight",
            "defaultValue": "bold",
            "docstring": null
          },
          {
            "name": "@alert-success-bg",
            "defaultValue": "@state-success-bg",
            "docstring": null
          },
          {
            "name": "@alert-success-text",
            "defaultValue": "@state-success-text",
            "docstring": null
          },
          {
            "name": "@alert-success-border",
            "defaultValue": "@state-success-border",
            "docstring": null
          },
          {
            "name": "@alert-info-bg",
            "defaultValue": "@state-info-bg",
            "docstring": null
          },
          {
            "name": "@alert-info-text",
            "defaultValue": "@state-info-text",
            "docstring": null
          },
          {
            "name": "@alert-info-border",
            "defaultValue": "@state-info-border",
            "docstring": null
          },
          {
            "name": "@alert-warning-bg",
            "defaultValue": "@state-warning-bg",
            "docstring": null
          },
          {
            "name": "@alert-warning-text",
            "defaultValue": "@state-warning-text",
            "docstring": null
          },
          {
            "name": "@alert-warning-border",
            "defaultValue": "@state-warning-border",
            "docstring": null
          },
          {
            "name": "@alert-danger-bg",
            "defaultValue": "@state-danger-bg",
            "docstring": null
          },
          {
            "name": "@alert-danger-text",
            "defaultValue": "@state-danger-text",
            "docstring": null
          },
          {
            "name": "@alert-danger-border",
            "defaultValue": "@state-danger-border",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Progress bars",
    "id": "progress-bars",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@progress-bg",
            "defaultValue": "#f5f5f5",
            "docstring": {
              "html": "Background color of the whole progress component"
            }
          },
          {
            "name": "@progress-bar-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Progress bar text color"
            }
          },
          {
            "name": "@progress-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": {
              "html": "Variable for setting rounded corners on progress bar."
            }
          },
          {
            "name": "@progress-bar-bg",
            "defaultValue": "@brand-primary",
            "docstring": {
              "html": "Default progress bar color"
            }
          },
          {
            "name": "@progress-bar-success-bg",
            "defaultValue": "@brand-success",
            "docstring": {
              "html": "Success progress bar color"
            }
          },
          {
            "name": "@progress-bar-warning-bg",
            "defaultValue": "@brand-warning",
            "docstring": {
              "html": "Warning progress bar color"
            }
          },
          {
            "name": "@progress-bar-danger-bg",
            "defaultValue": "@brand-danger",
            "docstring": {
              "html": "Danger progress bar color"
            }
          },
          {
            "name": "@progress-bar-info-bg",
            "defaultValue": "@brand-info",
            "docstring": {
              "html": "Info progress bar color"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "List group",
    "id": "list-group",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@list-group-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Background color on <code>.list-group-item</code>"
            }
          },
          {
            "name": "@list-group-border",
            "defaultValue": "#ddd",
            "docstring": {
              "html": "<code>.list-group-item</code> border color"
            }
          },
          {
            "name": "@list-group-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": {
              "html": "List group border radius"
            }
          },
          {
            "name": "@list-group-hover-bg",
            "defaultValue": "#f5f5f5",
            "docstring": {
              "html": "Background color of single list items on hover"
            }
          },
          {
            "name": "@list-group-active-color",
            "defaultValue": "@component-active-color",
            "docstring": {
              "html": "Text color of active list items"
            }
          },
          {
            "name": "@list-group-active-bg",
            "defaultValue": "@component-active-bg",
            "docstring": {
              "html": "Background color of active list items"
            }
          },
          {
            "name": "@list-group-active-border",
            "defaultValue": "@list-group-active-bg",
            "docstring": {
              "html": "Border color of active list elements"
            }
          },
          {
            "name": "@list-group-active-text-color",
            "defaultValue": "lighten(@list-group-active-bg, 40%)",
            "docstring": {
              "html": "Text color for content within active list items"
            }
          },
          {
            "name": "@list-group-disabled-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Text color of disabled list items"
            }
          },
          {
            "name": "@list-group-disabled-bg",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "Background color of disabled list items"
            }
          },
          {
            "name": "@list-group-disabled-text-color",
            "defaultValue": "@list-group-disabled-color",
            "docstring": {
              "html": "Text color for content within disabled list items"
            }
          },
          {
            "name": "@list-group-link-color",
            "defaultValue": "#555",
            "docstring": null
          },
          {
            "name": "@list-group-link-hover-color",
            "defaultValue": "@list-group-link-color",
            "docstring": null
          },
          {
            "name": "@list-group-link-heading-color",
            "defaultValue": "#333",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Panels",
    "id": "panels",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@panel-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@panel-body-padding",
            "defaultValue": "15px",
            "docstring": null
          },
          {
            "name": "@panel-heading-padding",
            "defaultValue": "10px 15px",
            "docstring": null
          },
          {
            "name": "@panel-footer-padding",
            "defaultValue": "@panel-heading-padding",
            "docstring": null
          },
          {
            "name": "@panel-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": null
          },
          {
            "name": "@panel-inner-border",
            "defaultValue": "#ddd",
            "docstring": {
              "html": "Border color for elements within panels"
            }
          },
          {
            "name": "@panel-footer-bg",
            "defaultValue": "#f5f5f5",
            "docstring": null
          },
          {
            "name": "@panel-default-text",
            "defaultValue": "@gray-dark",
            "docstring": null
          },
          {
            "name": "@panel-default-border",
            "defaultValue": "#ddd",
            "docstring": null
          },
          {
            "name": "@panel-default-heading-bg",
            "defaultValue": "#f5f5f5",
            "docstring": null
          },
          {
            "name": "@panel-primary-text",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@panel-primary-border",
            "defaultValue": "@brand-primary",
            "docstring": null
          },
          {
            "name": "@panel-primary-heading-bg",
            "defaultValue": "@brand-primary",
            "docstring": null
          },
          {
            "name": "@panel-success-text",
            "defaultValue": "@state-success-text",
            "docstring": null
          },
          {
            "name": "@panel-success-border",
            "defaultValue": "@state-success-border",
            "docstring": null
          },
          {
            "name": "@panel-success-heading-bg",
            "defaultValue": "@state-success-bg",
            "docstring": null
          },
          {
            "name": "@panel-info-text",
            "defaultValue": "@state-info-text",
            "docstring": null
          },
          {
            "name": "@panel-info-border",
            "defaultValue": "@state-info-border",
            "docstring": null
          },
          {
            "name": "@panel-info-heading-bg",
            "defaultValue": "@state-info-bg",
            "docstring": null
          },
          {
            "name": "@panel-warning-text",
            "defaultValue": "@state-warning-text",
            "docstring": null
          },
          {
            "name": "@panel-warning-border",
            "defaultValue": "@state-warning-border",
            "docstring": null
          },
          {
            "name": "@panel-warning-heading-bg",
            "defaultValue": "@state-warning-bg",
            "docstring": null
          },
          {
            "name": "@panel-danger-text",
            "defaultValue": "@state-danger-text",
            "docstring": null
          },
          {
            "name": "@panel-danger-border",
            "defaultValue": "@state-danger-border",
            "docstring": null
          },
          {
            "name": "@panel-danger-heading-bg",
            "defaultValue": "@state-danger-bg",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Thumbnails",
    "id": "thumbnails",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@thumbnail-padding",
            "defaultValue": "4px",
            "docstring": {
              "html": "Padding around the thumbnail image"
            }
          },
          {
            "name": "@thumbnail-bg",
            "defaultValue": "@body-bg",
            "docstring": {
              "html": "Thumbnail background color"
            }
          },
          {
            "name": "@thumbnail-border",
            "defaultValue": "#ddd",
            "docstring": {
              "html": "Thumbnail border color"
            }
          },
          {
            "name": "@thumbnail-border-radius",
            "defaultValue": "@border-radius-base",
            "docstring": {
              "html": "Thumbnail border radius"
            }
          },
          {
            "name": "@thumbnail-caption-color",
            "defaultValue": "@text-color",
            "docstring": {
              "html": "Custom text color for thumbnail captions"
            }
          },
          {
            "name": "@thumbnail-caption-padding",
            "defaultValue": "9px",
            "docstring": {
              "html": "Padding around the thumbnail caption"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Wells",
    "id": "wells",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@well-bg",
            "defaultValue": "#f5f5f5",
            "docstring": null
          },
          {
            "name": "@well-border",
            "defaultValue": "darken(@well-bg, 7%)",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Badges",
    "id": "badges",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@badge-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@badge-link-hover-color",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Linked badge text color on hover"
            }
          },
          {
            "name": "@badge-bg",
            "defaultValue": "@gray-light",
            "docstring": null
          },
          {
            "name": "@badge-active-color",
            "defaultValue": "@link-color",
            "docstring": {
              "html": "Badge text color in active nav link"
            }
          },
          {
            "name": "@badge-active-bg",
            "defaultValue": "#fff",
            "docstring": {
              "html": "Badge background color in active nav link"
            }
          },
          {
            "name": "@badge-font-weight",
            "defaultValue": "bold",
            "docstring": null
          },
          {
            "name": "@badge-line-height",
            "defaultValue": "1",
            "docstring": null
          },
          {
            "name": "@badge-border-radius",
            "defaultValue": "10px",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Breadcrumbs",
    "id": "breadcrumbs",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@breadcrumb-padding-vertical",
            "defaultValue": "8px",
            "docstring": null
          },
          {
            "name": "@breadcrumb-padding-horizontal",
            "defaultValue": "15px",
            "docstring": null
          },
          {
            "name": "@breadcrumb-bg",
            "defaultValue": "#f5f5f5",
            "docstring": {
              "html": "Breadcrumb background color"
            }
          },
          {
            "name": "@breadcrumb-color",
            "defaultValue": "#ccc",
            "docstring": {
              "html": "Breadcrumb text color"
            }
          },
          {
            "name": "@breadcrumb-active-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Text color of current page in the breadcrumb"
            }
          },
          {
            "name": "@breadcrumb-separator",
            "defaultValue": "\"/\"",
            "docstring": {
              "html": "Textual separator for between breadcrumb elements"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Carousel",
    "id": "carousel",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@carousel-text-shadow",
            "defaultValue": "0 1px 2px rgba(0, 0, 0, .6)",
            "docstring": null
          },
          {
            "name": "@carousel-control-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@carousel-control-width",
            "defaultValue": "15%",
            "docstring": null
          },
          {
            "name": "@carousel-control-opacity",
            "defaultValue": ".5",
            "docstring": null
          },
          {
            "name": "@carousel-control-font-size",
            "defaultValue": "20px",
            "docstring": null
          },
          {
            "name": "@carousel-indicator-active-bg",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@carousel-indicator-border-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@carousel-caption-color",
            "defaultValue": "#fff",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Close",
    "id": "close",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@close-font-weight",
            "defaultValue": "bold",
            "docstring": null
          },
          {
            "name": "@close-color",
            "defaultValue": "#000",
            "docstring": null
          },
          {
            "name": "@close-text-shadow",
            "defaultValue": "0 1px 0 #fff",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Code",
    "id": "code",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@code-color",
            "defaultValue": "#c7254e",
            "docstring": null
          },
          {
            "name": "@code-bg",
            "defaultValue": "#f9f2f4",
            "docstring": null
          },
          {
            "name": "@kbd-color",
            "defaultValue": "#fff",
            "docstring": null
          },
          {
            "name": "@kbd-bg",
            "defaultValue": "#333",
            "docstring": null
          },
          {
            "name": "@pre-bg",
            "defaultValue": "#f5f5f5",
            "docstring": null
          },
          {
            "name": "@pre-color",
            "defaultValue": "@gray-dark",
            "docstring": null
          },
          {
            "name": "@pre-border-color",
            "defaultValue": "#ccc",
            "docstring": null
          },
          {
            "name": "@pre-scrollable-max-height",
            "defaultValue": "340px",
            "docstring": null
          }
        ]
      }
    ]
  },
  {
    "heading": "Type",
    "id": "type",
    "customizable": true,
    "docstring": null,
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@component-offset-horizontal",
            "defaultValue": "180px",
            "docstring": {
              "html": "Horizontal offset for forms and lists."
            }
          },
          {
            "name": "@text-muted",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Text muted color"
            }
          },
          {
            "name": "@abbr-border-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Abbreviations and acronyms border color"
            }
          },
          {
            "name": "@headings-small-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Headings small color"
            }
          },
          {
            "name": "@blockquote-small-color",
            "defaultValue": "@gray-light",
            "docstring": {
              "html": "Blockquote small color"
            }
          },
          {
            "name": "@blockquote-font-size",
            "defaultValue": "(@font-size-base * 1.25)",
            "docstring": {
              "html": "Blockquote font size"
            }
          },
          {
            "name": "@blockquote-border-color",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "Blockquote border color"
            }
          },
          {
            "name": "@page-header-border-color",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "Page header border color"
            }
          },
          {
            "name": "@dl-horizontal-offset",
            "defaultValue": "@component-offset-horizontal",
            "docstring": {
              "html": "Width of horizontal description list titles"
            }
          },
          {
            "name": "@dl-horizontal-breakpoint",
            "defaultValue": "@grid-float-breakpoint",
            "docstring": {
              "html": "Point at which .dl-horizontal becomes horizontal"
            }
          },
          {
            "name": "@hr-border",
            "defaultValue": "@gray-lighter",
            "docstring": {
              "html": "Horizontal line color."
            }
          }
        ]
      }
    ]
  }
];