/** 
 * Less variables for src/less/salaxy-component/variable-defaults.less
 * @ignore
 */
export const salaxyLessVariables = [
  {
    "heading": "Branding",
    "id": "branding",
    "customizable": true,
    "docstring": {
      "html": "Site branding elements: Logo fonts etc."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@salaxy-navi-logo",
            "defaultValue": "\"/img/navi-logo.png\"",
            "docstring": {
              "html": "Color of the Salary in Calculator"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Colors",
    "id": "colors",
    "customizable": true,
    "docstring": {
      "html": "Brand colors for Salary payment concepts."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@salaxy-salary",
            "defaultValue": "@brand-success",
            "docstring": {
              "html": "Color of the Salary in Calculator"
            }
          },
          {
            "name": "@salaxy-person",
            "defaultValue": "@brand-info",
            "docstring": {
              "html": "Color of the Person (Worker selection) in Calculator"
            }
          },
          {
            "name": "@salaxy-work",
            "defaultValue": "@brand-warning",
            "docstring": {
              "html": "Work is currently the &quot;worktime&quot; tab that shows holidays, absences and later also hours / timesheets."
            }
          },
          {
            "name": "@salaxy-expenses",
            "defaultValue": "@brand-danger",
            "docstring": {
              "html": "Expenses is the last tab in the UI."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Modal",
    "id": "modal",
    "customizable": true,
    "docstring": {
      "html": "Colors for modals."
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@salaxy-header-bg",
            "defaultValue": "@brand-primary",
            "docstring": {
              "html": "Modal header background this is also the default for other headers"
            }
          },
          {
            "name": "@salaxy-header-border",
            "defaultValue": "@salaxy-expenses",
            "docstring": {
              "html": "Modal header bottom border"
            }
          },
          {
            "name": "@salaxy-header-text",
            "defaultValue": "white",
            "docstring": {
              "html": "Modal title text color"
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Grid extensions",
    "id": "grid-extensions",
    "customizable": true,
    "docstring": {
      "html": "Extensions to Bootstrap 3 grid system"
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@salaxy-screen-xxl-min",
            "defaultValue": "1900px",
            "docstring": {
              "html": "XXL screen minimum width. XXL screen is very wide screeen: Accounting professionals, Admin etc."
            }
          }
        ]
      }
    ]
  },
  {
    "heading": "Navi",
    "id": "navi",
    "customizable": true,
    "docstring": {
      "html": "Navi heights, widths and colors"
    },
    "subsections": [
      {
        "heading": "",
        "id": "",
        "variables": [
          {
            "name": "@salaxy-navi-header-height",
            "defaultValue": "75px",
            "docstring": {
              "html": "Header height"
            }
          },
          {
            "name": "@salaxy-navi-sidebar-width",
            "defaultValue": "250px",
            "docstring": {
              "html": "Width of the sidebar (left navi)"
            }
          },
          {
            "name": "@salaxy-navi-header-bg",
            "defaultValue": "@salaxy-header-bg",
            "docstring": {
              "html": "Header background color"
            }
          },
          {
            "name": "@salaxy-navi-logo-bg",
            "defaultValue": "@salaxy-header-bg",
            "docstring": {
              "html": "Logo area background color"
            }
          },
          {
            "name": "@salaxy-navi-sidebar-bg",
            "defaultValue": "white",
            "docstring": {
              "html": "Left navi (sidebar) background color"
            }
          },
          {
            "name": "@poweredby-visibility",
            "defaultValue": "hidden",
            "docstring": null
          }
        ]
      }
    ]
  }
];