import * as angular from "angular";

/** Abstract Base class for salaxy components */
export abstract class ComponentBase implements angular.IComponentOptions {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public abstract bindings: any;

    /**
     * Controller has the actual implementation of the component.
     * Same Controller may be reused between different components with different views and binding.
     */
    public abstract controller;

    /** The default template for the component. */
    public abstract defaultTemplate;

    /**
     * Set the template-url attribute to specify your custom template.
     * @ignore
     */
    public templateUrl = ["$element", "$attrs" , ($element: JQLite, $attrs: angular.IAttributes) => {
        if ($attrs.templateUrl) {
            return $attrs.templateUrl;
        }
        return this.defaultTemplate;
    }];
}
