import * as angular from "angular";

import { ComponentsRegistration } from "../components/_ComponentsRegistration";
import { ControllersRegistration } from "../controllers/_ControllersRegistration";
import { DirectivesRegistration } from "../directives/_DirectivesRegistration";
import { FiltersRegistration } from "../filters/_FiltersRegistration";
import { PromisePolyfill, RouteHelperProvider } from "../helpers";
import { LoaderInterceptor } from "../interceptors";
import { ServicesRegistration } from "../services/_ServicesRegistration";

/**
 * Components contain the user interface element (the view) on top of the controller logic.
 * This is the Angular2 compatible alternative to Directives introduced in Angular 1.5.
 *
 * Registers the salaxy.ng1.components.all module that contains the entire
 * Palkkaus.fi Angular stack
 * @example
 * angular.module("myAngularApplication", ["salaxy.ng1.components.all"])
 * @ignore
 */
export const SalaxyNg1ComponentsModule = angular.module("salaxy.ng1.components.all", [
    "salaxy.ng1.templates.bootstrap",
    "ui.bootstrap",
    "ngSanitize",
    "chart.js",
    "ngFileUpload",
    "pascalprecht.translate",
    "angular-barcode",
])
    // Registrations
    .provider("RouteHelper", RouteHelperProvider)
    .service(ServicesRegistration.getCoreServices())
    .service(ServicesRegistration.getServices())
    .controller(ControllersRegistration.getControllers())
    .directive(DirectivesRegistration.getDirectives())
    .component(ComponentsRegistration.getComponents())
    .filter(FiltersRegistration.getFilters())
    .constant("data", { isNotDefined: true })
    // Interceptors
    .config(["$httpProvider", ($httpProvider: ng.IHttpProvider) => {
       $httpProvider.interceptors.push(LoaderInterceptor.factory());
     }])

    // Misc configurations
    .config(["$provide", "$translateProvider", ($provide: any, $translateProvider: angular.translate.ITranslateProvider) => {
        // Currently there is an issue with the sanitize mode, it will double encode UTF-8 characters or special characters.
        // use the null strategy, until this is resolved.
        $translateProvider.useSanitizeValueStrategy(null);
        $provide.value("$translateProvider", $translateProvider);
    }])
    // supporting the default empty hash-prefix after upgrade from 1.5 -> 1.6
    .config(["$locationProvider", ($locationProvider) => {
      $locationProvider.hashPrefix("");
    }])
    .run(PromisePolyfill)
    ;
