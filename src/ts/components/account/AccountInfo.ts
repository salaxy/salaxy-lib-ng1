import { SessionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Simple presentation of Salaxy account info: employer and/or worker data
 *
 * @example
 * ```html
 * <salaxy-account-info></salaxy-account-info>
 * ```
 */
export class AccountInfo extends ComponentBase {

    /** The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to controller implementation
     */
    public bindings = {};

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AccountInfo.html";
}
