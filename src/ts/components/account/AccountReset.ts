import { AccountResetController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Methods for editing test account
 *
 * @example
 * ```html
 * <salaxy-account-reset></salaxy-account-reset>
 * ```
 */
export class AccountReset extends ComponentBase {

    /** The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to controller implementation
     */
    public bindings = {};

    /** Uses the SessionController */
    public controller = AccountResetController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AccountReset.html";
}
