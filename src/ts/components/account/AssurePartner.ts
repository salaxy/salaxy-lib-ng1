import { PartnerController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Component that assures that customer has specified a partner.
 *
 * @example
 * ```html
 * <salaxy-assure-partner><main>This content is shown if partner selected!</main></salaxy-assure-partner>
 * ```
 */
export class AssurePartner extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {};

    /**
     * The "main" element contents is shown if the partner has been selected. Defaults to current partner info.
     * The "header" element is shown before the "main" Defaults to "Service provided by" + an actions drop-down.
     */
    public transclude = {
        main: "?main",
        header: "?header",
    };

    /** Uses the AccountAuthorizationController */
    public controller = PartnerController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AssurePartner.html";
}
