import { AccountAuthorizationController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a card for granting authorization
 *
 * @example
 * ```html
 * <div ng-controller="AccountAuthorizationController as authCtrl">
 *   <div class="row">
 *     <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" ng-repeat="site in authCtrl.partnerIntegrationSites">
 *       <salaxy-auth-card auth-id="site.id"></salaxy-auth-card>
 *     </div>
 *   </div>
 * </div>
 * ```
 */
export class AuthCard extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** Id that specifies what auth card to be shown */
        authId: "<",
        /** If set to true, the switch is not shown on the card. */
        hideSwitch: "@",
    };

    /** Uses the AccountAuthorizationController */
    public controller = AccountAuthorizationController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AuthCard.html";
}
