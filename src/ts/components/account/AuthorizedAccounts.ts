import { AccountAuthorizationController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of all authorized accounts which can act on behalf of this account.
 *
 * @example
 * ```html
 * <salaxy-authorized-accounts></salaxy-authorized-accounts>
 * ```
 */
export class AuthorizedAccounts extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = AccountAuthorizationController.crudBindings;

    /**
     * Header is mainly meant for buttons (Usually at least "Add new").
     * These are positioned to the right side of the table header (thead).
     */
    public transclude = {
        header: "?header",
    };

    /** Uses the AccountAuthorizationController */
    public controller = AccountAuthorizationController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AuthorizedAccounts.html";
}
