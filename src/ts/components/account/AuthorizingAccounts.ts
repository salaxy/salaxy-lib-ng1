import { AccountAuthorizationController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of all authorizing accounts on behalf of which this account can act.
 *
 * @example
 * ```html
 * <salaxy-authorizing-accounts></salaxy-authorizing-accounts>
 * ```
 */
export class AuthorizingAccounts extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
    };

    /**
     * Header is mainly meant for buttons (Usually at least "Add new").
     * These are positioned to the right side of the table header (thead).
     */
    public transclude = {
        header: "?header",
    };

    /** Uses the AccountAuthorizationController */
    public controller = AccountAuthorizationController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AuthorizingAccounts.html";
}
