import { CertificateController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of all certificates which has been generated for this account.
 *
 * @example
 * ```html
 * <salaxy-certificates></salaxy-certificates>
 * ```
 */
export class Certificates extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = CertificateController.crudBindings;

    /**
     * Header is mainly meant for buttons (Usually at least "Add new").
     * These are positioned to the right side of the table header (thead).
     */
    public transclude = {
        header: "?header",
    };

    /** Uses the CertificateController */
    public controller = CertificateController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/Certificates.html";
}
