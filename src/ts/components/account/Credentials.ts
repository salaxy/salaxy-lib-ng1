import { CredentialController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of all login credentials which can access this account.
 *
 * @example
 * ```html
 * <salaxy-credentials></salaxy-credentials>
 * ```
 */
export class Credentials extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      showEdit: "<",
    };

    /**
     * Header is mainly meant for buttons (Usually at least "Add new").
     * These are positioned to the right side of the table header (thead).
     */
    public transclude = {
        header: "?header",
    };

    /** Uses the CredentialController */
    public controller = CredentialController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/Credentials.html";
}
