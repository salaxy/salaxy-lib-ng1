import { SessionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a Login / Logout -button.
 * When anonymous, the button shows the login button.
 * When loggen in shows: Logout and links to Palkkaus Account and User Account pages.
 *
 * @example
 * ```html
 * <salaxy-login-button redirect-url="'/my-page'"></salaxy-login-button>
 * ```
 */
export class LoginButton extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
        /** URL to which the user should be redirected. When you specify string, use quotes. */
        redirectUrl: "<",

        /** If specified, sets the login text. Default is "SALAXY.UI_TERMS.login"  */
        loginText: "@",

        /** If specified, sets the button class. Default is "btn-default". */
        btnClass: "@",
    };

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/LoginButton.html";

}
