import { SigningController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the signature for the current account for signing.
 *
 * @example
 * ```html
 * <salaxy-signing ng-ref="$ctrl.$signing"></salaxy-signing>
 * <div ng-if="$ctrl.$signing.isSigningOk">
 *  Signature is OK => Show the rest of the page
 * </div>
 * ```
 */
export class Signing extends ComponentBase {

    /** The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to controller implementation
     */
    public bindings = {
      /** If true, shows the component even if the Signing is already OK. */
      showAlways: "<",
    };

    /** Uses the SessionController */
    public controller = SigningController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/Signing.html";
}
