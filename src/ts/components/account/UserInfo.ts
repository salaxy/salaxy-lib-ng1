import { SessionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Simple presentation of Salaxy user info: employer and/or worker data
 *
 * @example
 * ```html
 * <salaxy-user-info></salaxy-user-info>
 * ```
 */
export class UserInfo extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
    };

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/UserInfo.html";

}
