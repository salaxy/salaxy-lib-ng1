import { Calculator2019Controller } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * The **salaxy.ng1.components.Calc** is the generic Angular-based salary calculator implementation.
 * It shows the basic features required for a salary calculator user interfaces:
 *
 * 1. Setting the Worker for the calculation
 * 2. Defining the Worktime information: Period, Absences and holidays.
 * 3. Defining the Salary and the benefits
 * 4. Defining Expenses, benefits, household subsidies etc.
 * 5. Showing the results as charts and reports.
 *
 * Controllers are defined in **salaxy.ng1.controllers**. Most of the functionality comes from Calculator2019Controller.
 * The views can be modified separately for each component using the templateUrl. See documentation for details.
 *
 * @example
 * ```html
 * <salaxy-calc model="'new'"></salaxy-calc>
 * ```
 */
export class Calc extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = Calculator2019Controller.crudBindings;

    /** Uses the Calculator2019Controller */
    public controller = Calculator2019Controller;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/Calc.html";

}
