import { CalcChartController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Angular-chart based chart.
 *
 * @example
 * ```html
 * <salaxy-calc-chart role="worker" calc="$ctrl.currentCalc"></salaxy-calc-chart>
 * ```
 */
export class CalcChart extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */

  public bindings = {

    /** Calculation for which the chart is rendered.  */
    calc: "<",
    /** Role for which the chart is rendered. Currently, 'worker' and 'employer' are supported. Employer is the default. */
    role: "@",
    /** If true, the Y axis of this chart is scaled so that if worker and employer charts are side to side, their Y axis are the same */
    scaleYAxis: "<",
    /** Type of the chart - "pie" and "bar" (default) are currently supported. */
    chartType: "<",
    /** - colors: Custom colors for the chart as a string containing hex values of the colors, separated by comma */
    colors: "@",
  };

  /** Uses the CalcChartController */
  public controller = CalcChartController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/CalcChart.html";
}
