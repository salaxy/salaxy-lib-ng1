import { ODataQueryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows lists of calculations
 *
 * @example
 * ```html
 * <salaxy-calc-dashboard></salaxy-calc-dashboard>
 * ```
 */

export class CalcDashboard extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */

  public bindings = {
    /**
     * Option to hide new calc button.
     * Set this true if the site has action buttons configured elsewhere.
     */
    hideButton: "<",

    /** If readOnly is true, edit, copy etc. buttons from the new list view are hidden if readOnly property is supported */
    readOnly: "<",

  };

  /** Uses the ODataQueryController */
  public controller = ODataQueryController;
  // TODO: Consider should this be ODataQueryController or ApiCrudObjectController.

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/CalcDashboard.html";

}
