import { ODataQueryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of calculations
 *
 * @example
 * ```html
 * <salaxy-calc-list category="draft" limit-to="5"></salaxy-calc-list>
 * ```
 */
export class CalcList extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
       /** Filters list by given field:value */
      filter: "@",

      /** Max count of calculations to show in the list. */
      limitTo: "<",

      /** Status category for the list. Possible categories: draft, paid. Default is paid */
      category: "@",

      /**
       * List of statuses to include in the list.
       * WARNING: This may be depricated in v03: Use category and/or $filter instead.
       */
      statusList: "<",

      /**
       * Function that is called when user selects an item in the list.
       * Note that the event is called only in the selections (single and multi). Not when there is a link to details view in a link list.
       * Function has the following locals: value: true/false, item: the last selected/unselected item, allItems: Array of all currently selected items.
       * @example <salaxy-worker-list mode="select" on-list-select="$ctrl.updateFromEmployment(item.id)"></salaxy-worker-list>
       */
      onListSelect: "&",

      /** Selected items for the case where the list is used as a select list (as opposed to link list). */
      selectedItems: "<",

      /** Type of the view. Currently supports "default", "panel" and "select" */
      mode: "@",

      /** OData query options */
      options: "<",

      /** URL to the OData service. */
      url: "@",

      /** Data passed from the parent to the component / view */
      data: "<",

       /** If readOnly is true, edit, copy etc. buttons from the new list view are hidden if readOnly property is supported */
      readOnly: "<",

      /**
       * CRUD controller that implements Delete and potentially other methods about the object.
       * If you wish to use this in the view use "as $controllerName" syntax
       * @example
       * <salaxy-odata-table crud-controller="CalculationCrudController as $crud">
       */
      crudController: "@",

    };

    /** Uses the ODataQueryController */
    public controller = ODataQueryController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcList.html";

}
