import { Calculator2019Controller } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows an introduction text (instructions) for the user when user starts a new calculation.
 *
 * @example
 * ```html
 * <salaxy-calc-new-intro model="$ctrl.current"></salaxy-calc-new-intro>
 * ```
 */
export class CalcNewIntro extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = Calculator2019Controller.crudBindings;

    /** Uses the Calculator2019Controller */
    public controller = Calculator2019Controller;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcNewIntro.html";

}
