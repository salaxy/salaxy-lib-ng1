import { CalcReportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the report for the given calculation.
 *
 * @example
 * ```html
 * <salaxy-calc-report calc="$ctrl.currentCalc" report-type="'salarySlip'" template-name="'salarySlipV2'"></salaxy-calc-report>
 * ```
 */
export class CalcReport extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** The calculation for the report. */
      calc: "<",
      /** Calculation report type (for example: salarySlip, employerReport or paymentReport) */
      reportType: "<",
      /** Template for the report (for example: salarySlipV2, employerReportV2, paymentReportV2 ) */
      templateName: "<",
      /** If true, uses the current style of the site. The default value is false. */
      applySiteStyles: "<",

    };

    /** Uses the CalcReportController */
    public controller = CalcReportController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcReport.html";

}
