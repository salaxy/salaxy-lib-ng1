import { CalcReportsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the salary reports for the given calculations.
 *
 * @example
 * ```html
 * <salaxy-calc-reports calcs="$ctrl.calcs" report-type="'salarySlip'></salaxy-calc-reports>
 * ```
 */
export class CalcReports extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** The calculations for the report. */
      calcs: "<",
      /** Current report type. */
      reportType: "<",
    };

    /** Uses the CalcReportsController */
    public controller = CalcReportsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcReports.html";

}
