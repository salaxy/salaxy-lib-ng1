import { Calculator2019Controller } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the results view for the Calculator
 *
 * @example
 * ```html
 * <salaxy-calc-results model="$ctrl.current"></salaxy-calc-results>
 * ```
 */
export class CalcResults extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
    public bindings = Calculator2019Controller.crudBindings;

    /** Uses the Calculator2019Controller */
    public controller = Calculator2019Controller;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcResults.html";

}
