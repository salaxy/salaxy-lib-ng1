import { CalcRowTypeController } from "../../controllers";

import { ComponentBase } from "../_ComponentBase";

/**
 * Select component for Calculations row types.
 *
 * @example
 * ```html
 * <salaxy-calc-row-type name="rowType" model="row" label="Basic datepicker"></salaxy-calc-row-type>
 * ```
 */
export class CalcRowType extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

      /** Type of the input element. Options are typeahead and list (select, search-list and radio may be supported later). */
      type: "@",

      /** If set, filters the rows based on categories (plus rowTypes if set) */
      categories: "<",

      /** If set, shows only these types (plus categories if set) */
      rowTypes: "<",

      /** Name and identifier of the typeahead input */
      name: "@",

      /**
       * Placeholder text in the typeahead input.
       * Default is 'SALAXY.UI_TERMS.select'.
       */
      placeholder: "@",

      /** The calculation row that this component modifies. */
      model: "<",

      /**
       * Function that is called after the row type and potentially kind and message have been changed.
       * @example <salaxy-calc-row-type on-row-changed="$ctrl.commitNewRow()"></salaxy-calc-row-type>
       */
      onRowChanged: "&",

      /** If true, the list will show all the children under each parent that is shown */
      showChildren: "<",

    };

    /** Uses the InputCalcRowTypeController */
    public controller = CalcRowTypeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcRowType.html";
}
