import { ApiCrudObjectControllerBindings, CalcSharingReceiptActionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the sharing receipt action control for the given calculation id.
 *
 * @example
 * ```html
 * <salaxy-calc-sharing-receipt-action model="$ctrl" type="'approve'"></salaxy-calc-sharing-receipt-action>
 * ```
 */
export class CalcSharingReceiptAction extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {
    /** Sharing action (approve or reject) */
    public type = "<";
    /** Disables the control. */
    public disabled = "<";
    /** Optional style class to use for button. Defaults to 'btn-block btn-sm' */
    public buttonClass = "@";
    /** Shows only icon in button. Used in compact views, i.e. panels */
    public iconOnly = "<";
  }());

  /** Uses the CalcSharingReceiptActionController */
  public controller = CalcSharingReceiptActionController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/CalcSharingReceiptAction.html";

}
