import { Calculator2019Controller } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows information about the Worker / employment relation within a calculation.
 *
 * @example
 * ```html
 * <salaxy-calc-worker-info model="$ctrl.myWorkerOrCalc"></salaxy-calc-worker-info>
 * ```
 */
export class CalcWorkerInfo extends ComponentBase {

  // TODO: If the component is removed from payment pages, consider having it simply as an include within calculations
  // (and not as a component) OR consider having a calc-component with several different layouts.

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = Calculator2019Controller.crudBindings;

  /** Uses the Calculator2019Controller */
  public controller = Calculator2019Controller;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/CalcWorkerInfo.html";

}
