import { CalcWorktimeController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * UI for setting the Worktime properties within the calculator
 * The process starts by selecting the period based on which relevatn worktime
 * data is fetched: Holidays, Absences and later Hours.
 * Calculation rows are then added based on this worktime.
 *
 * @example
 * ```html
 * <salaxy-calc-worktime calc="$ctrl.currentCalc"></salaxy-calc-worktime>
 * ```
 */
export class CalcWorktime extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** The calculation that the component edits (shows in read-only mode). */
      calc: "<",
    };

    /** Uses the CalcWorktimeController */
    public controller = CalcWorktimeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/CalcWorktime.html";

}
