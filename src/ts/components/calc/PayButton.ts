import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a Payment -button or Payment Channel selection dropdown or both for the Calculation or Payroll
 *
 * @example
 * ```html
 * <salaxy-pay-button model="$ctrl.current"></salaxy-pay-button>
 * ```
 */
export class PayButton extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

      /**
       * The current business item that is being paid: Either Calculation or PayrollDetails.
       * Currently only supports direct reference. May later support 'url' etc. like CRUD controllers.
       */
      model: "<",

      /** Expression for ng-disabled of the input. */
      disabled: "<",

      /**
       * Additional class / classes for button. Can also be used to override default btn-primary class styles.
       * (i.e. <salaxy-pay-button button-class="btn-lg uppercase btn-success">)
       */
      buttonClass: "@",

      /**
       * Text for the payment button.
       * This is typically customized in custom payment scenarios.
       */
      label: "@",

      /** Mode is either "pay-button" (default), "channel-select" or "button-and-channel" */
      mode: "@",

      /** Called when the payment channel has been changed. */
      onPaymentChannelChange: "&",
    };

    /**
     * Uses the PaymentController
     * This is on purpose not typed. This is because we want the injection to be active for possible customer
     * specific overrides.
     */
    public controller = "PaymentController";

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/PayButton.html";
}
