﻿
import { ApiCrudObjectControllerBindings, PayrollCrudController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
 * Typical usecase is that a a company has e.g. a monthly salary list that is paid
 * at the end of month. For next month, a copy is then made from the latest list and
 * the copy is potentially modified with the changes of that particular month.
 * Payroll can also be started from scratch either by just writing salaries from
 * e.g. an e-mail or by uploading an Excel sheet.
 *
 * @example
 * ```html
 * <salaxy-payroll-edit></salaxy-payroll-edit>
 * ```
 */
export class PayrollEdit extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {
    /**
     * Mode of the payroll. Supports "service" or default.
     * In "service" mode the payroll has less funtionality and payment is disabled.
     */
    public mode = "<";
  }());

  /** Uses the PayrollCrudController */
  public controller = PayrollCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/payroll/PayrollEdit.html";
}
