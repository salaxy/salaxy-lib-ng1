﻿import { ApiCrudObjectControllerBindings, PayrollCrudController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
 * Typical usecase is that a a company has e.g. a monthly salary list that is paid
 * at the end of month. For next month, a copy is then made from the latest list and
 * the copy is potentially modified with the changes of that particular month.
 * Payroll can also be started from scratch either by just writing salaries from
 * e.g. an e-mail or by uploading an Excel sheet.
 *
 * @example
 * ```html
 * <salaxy-payroll-list></salaxy-payroll-list>
 * ```
 */
export class PayrollList extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */

  public bindings = (new class extends ApiCrudObjectControllerBindings {
   /**
    * Option to hide New payroll button.
    * Set this true if the site has action buttons configured elsewhere.
    */
    public hideButton = "<";

   /**
    * Mode of the payroll. Supports "service" or default.
    * In "service" mode the payroll has less funtionality and payment is disabled.
    */
    public mode = "<";
  }());


  /**
   * Header is mainly meant for buttons (Usually at least "Add new").
   * These are positioned to the right side of the table header (thead).
   */
  public transclude = {
    header: "?header",
  };

  /** Uses the PayrollCrudController */
  public controller = PayrollCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/payroll/PayrollList.html";
}
