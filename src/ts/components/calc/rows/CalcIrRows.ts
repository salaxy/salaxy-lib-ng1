import { CalcIrRowsController, Calculator2019Controller } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Editor for Incomes Register (Tulorekisteri) rows from Calculation.
 *
 * @example
 * ```html
 * <salaxy-calc-ir-rows model="'url'"></salaxy-calc-ir-rows>
 * ```
 */
export class CalcIrRows extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = Calculator2019Controller.crudBindings;

  /** Uses the CalcIrRowsController */
  public controller = CalcIrRowsController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/calc/rows/CalcIrRows.html";

}
