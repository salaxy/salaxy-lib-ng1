import { CalcRows2019Controller } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Shows a grid-type rows editor for addding calculation rows: Salaries, expenses, benefits etc.
 *
 * @example
 * ```html
 * <salaxy-calc-rows-2019-editor categories="['expenses']" parent="$ctrl.currentCalc" mode="edit"
 *     title="Kulukorvaukset"></salaxy-calc-rows-2019-editor>
 * ```
 */
export class CalcRows2019Editor extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = CalcRows2019Controller.bindings;

    /** Uses the CalcRows2019Controller */
    public controller = CalcRows2019Controller;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/calc/rows/CalcRows2019Editor.html";

}
