import { ContactInfoController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the customer service contact info and reference to zendesk's help-widget.
 * Options to show short content and hide contact info.
 *
 *
 * @example
 * ```html
 * <salaxy-contact-info></salaxy-contact-info>
 * ```
 */
export class ContactInfo extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

    /** If true, short version of the content is shown */
    showShortContent: "=",

    /** If true, contact info is hidden */
    hideContactInfo: "=",
  };
  /** Uses the AlertController */
  public controller = ContactInfoController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/communications/ContactInfo.html";
}
