import { ApiCrudObjectControllerBindings } from "../../controllers/bases";
import { MessageThreadCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Edit and create new user interface for message thread.
 *
 * @example
 * ```html
 * <salaxy-message-thread></salaxy-message-thread>
 * ```
 */
export class MessageThread extends ComponentBase {

  /**
   * Allows injecting HTML within the component.
   */
  public transclude = {
    /** A "header" element can be used to add HTML on top of the message thread (where the header is now). */
    header: "?header",
    /** A "header-new" element overrides the default text for new message thread. */
    headerNew: "?headerNew",
  };

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {

    /** Role of the current user: typically "owner" (default) or "otherParty" */
    public role = "@";

    /** Salaxy ID (IBAN format) for the other party (besides the message thread owner) */
    public otherParty = "<";

    /**
     * If set, shows a drop-down for these options using the enum component.
     * Typically, use object with string key-value pairs.
     */
    public otherPartyOptions = "<";

    /**
     * If set, the controller will mark a message thread as open once it has been loaded.
     * If the value is true, the message is marked as read immediately.
     * If the value is number, the controller waits the given amount of seconds before marking the item as read.
     */
    public markAsRead = "<";

  }());

  /** Uses the EmailMessageController */
  public controller = MessageThreadCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/communications/MessageThread.html";

}
