import { WelcomeController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a detail a Welcome message depending on the user profile.
 * Typically the first component on the front page.
 *
 * @example
 * ```html
 * <salaxy-welcome></salaxy-welcome>
 * ```
 */
export class Welcome extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /**
         * If set to true, the settings are handled by proxy:
         * we do not ask the user to change the settings in
         * the Welcome screen (mainly Pension or Insurance).
         */
        settingsByProxy: "<",
    };

    /** Uses the WelcomeController */
    public controller = WelcomeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/communications/Welcome.html";

}
