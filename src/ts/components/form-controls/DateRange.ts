import { Objects } from "@salaxy/core";

import { DateRangeController, InputBase } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Provides a user interface for picking up a date range
 * and optionally specifying also the number of working days within that range.
 *
 * @example
 * ```html
 * <salaxy-date-range name="myDateRange" ng-model="temp" label="Sample date range"></salaxy-date-range>
 * ```
 */
export class DateRange extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

      /** Options for ui.bootstrap.datepicker */
      datepickerOptions: "<",

      /**
       * Label for the scondary input (Default: SALAXY.NG1.DateRange.labelDaysCount).
       * Translation is attempted.
       */
      labelDaysCount: "@",

      /**
       * Minimum available date.
       * Bindable and ISO string version of the datepicker-options.minDate.
       */
      minDate: "<",

      /**
       * Maximum available date.
       * Bindable and ISO string version of the datepicker-options.maxDate.
       */
      maxDate: "<",

      /**
       * The selection mode is either:
       *
       * - "range" for (default) calendar control with start and end.
       * - "multiple" for selecting multiple individual dates from a calendar control
       * - "calc" for period selection that is optimized for salary calculation (months, 2 weeks, 1/2 months etc.)
       */
      mode: "@",

      /**
       * Fires an event when the model is changing: Any of the values specific to the model are changing: start, end or daysCount.
       * This should typically used instead of ng-change because ng-change will only fire if the object reference changes.
       * On-change fires when dates or day count changes and this is typically what you are looking for.
       * @example <salaxy-date-range ng-model="$ctrl.dateRange" on-change="$ctrl.dateRangeChange(value)"></salaxy-date-range>
       */
      onChange: "&",

    });

    /** Uses the DateRangeController */
    public controller = DateRangeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/DateRange.html";
}
