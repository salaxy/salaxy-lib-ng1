import { DatepickerController, InputBase } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

import { Objects } from "@salaxy/core";

/**
 * Modified ui.bootstrap.datepicker.
 * The component can be bound to a string typed variable. The standard ui.bootstrap.datepicker can be bound successfully only to a Date variable.
 *
 * @example
 * ```html
 * <salaxy-datepicker name="myDatepicker" ng-model="temp" label="Basic datepicker"></salaxy-datepicker>
 * ```
 */
export class Datepicker extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /** options for ui.bootstrap.datepicker */
        datepickerOptions: "<",

        /**
         * Preset algorithms for enabled/disabled dates.
         * Currently, only "salary" is supported for salary dates, but more might be added later.
         * The function is added to datepickerOptions in init, so if you set datePickerOptions after that, you may need to implement this on your own.
         */
        dateDisabled: "@",

        /** Payment channel. This affects which dates are disabled in the salary date selection. */
        paymentChannel: "<",

        /**
         * Minimum available date.
         * Bindable and ISO string version of the datepicker-options.minDate.
         * Currently not supported together with dateDisabled filters.
         */
         minDate: "<",

         /**
          * Maximum available date.
          * Bindable and ISO string version of the datepicker-options.maxDate.
          * Currently not supported together with dateDisabled filters.
          */
         maxDate: "<",

    });

    /** Uses the DatepickerController */
    public controller = DatepickerController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/Datepicker.html";
}
