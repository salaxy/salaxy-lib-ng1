import { DatepickerPopupController, InputBase } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

import { Objects } from "@salaxy/core";

/**
 * Modified ui.bootstrap.datepickerPopup.
 * The component can be bound to a string typed variable. The standard ui.bootstrap.datepickerPopup can be bound successfully only to a Date variable.
 *
 * @example
 * ```html
 *
 * <salaxy-datepicker-popup name="myDatepickerPopup" ng-model="temp" label="Basic datepicker popup" label-placement="top"></salaxy-datepicker-popup>
 * ```
 */
export class DatepickerPopup extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /** options for ui.bootstrap.datepicker */
        datepickerOptions: "<",

        /** popup placement:  */
        popupPlacement: "@",

        /** If 'top', the label is placed on it's own row before input */
        labelPlacement: "@",

        /** Format for the date, default is d.M.yyyy */
        format: "@",

        /**
         * If set to true, sets datepicker-append-to-body to false.
         * By default we set it to true in Salaxy framework,
         * but you may want to set it back to false in e.g. Modal dialogs.
         */
        appendInline: "@",

        /**
         * Preset algorithms for enabled/disabled dates.
         * Currently, only "salary" is supported for salary dates, but more might be added later.
         * The function is added to datepickerOptions in init, so if you set datePickerOptions after that, you may need to implement this on your own.
         */
        dateDisabled: "@",

        /** Payment channel. This affects which dates are disabled in the salary date selection. */
        paymentChannel: "<",

        /**
         * Minimum available date.
         * Bindable and ISO string version of the datepicker-options.minDate.
         * Currently not supported together with dateDisabled filters.
         */
        minDate: "<",

        /**
         * Maximum available date.
         * Bindable and ISO string version of the datepicker-options.maxDate.
         * Currently not supported together with dateDisabled filters.
         */
        maxDate: "<",

    });

    /** Uses the DatepickerController */
    public controller = DatepickerPopupController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/DatepickerPopup.html";
}
