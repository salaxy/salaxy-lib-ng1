import { FormGroupController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Helper for rendering the HTML for FormGroup:
 * This component renders only the label - input html.
 * It does not do any of the real form-control logic like ng-model, validations etc. (see salaxy-input for that).
 * The "input" part of the form group may be a non form control - e.g. just a text.
 * Also the label may be hidden.
 *
 * @example
 * ```html
 * <salaxy-form-group label="Some label"><p>Just text here</p></salaxy-form-group>
 * ```
 */
export class FormGroup extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
    /** Name of the input - also used as id. */
    name: "@",

    /** Label for the control */
    label: "@",

    /**
     * @deprecated Use "require" instead:
     * This attribute overlaps with required/ng-required directive, which produces unexpected results.
     */
    required: "@",

    /** If true the field is required (form validation) */
    require: "<",

    /**
     * Positioning of the label of form-control.
     * Supported values are "horizontal" (default), "no-label", "plain", "basic" and "empty-label".
     * See FormGourpLabelType for details.
     */
    labelType: "@",

    /**
     * Label columns expressed as Bootstrap grid columns.
     * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
     * Other label-types do not have column classes at the moment.
     */
    labelCols: "@",
  };

  /** Contents of the tag is the input element. */
  public transclude = true;

  /** Uses the InputController */
  public controller = FormGroupController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/form-controls/FormGroup.html";
}
