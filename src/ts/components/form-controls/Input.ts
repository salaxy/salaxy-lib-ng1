import { InputBase, InputController } from "../../controllers";

import { Objects } from "@salaxy/core";

import { ComponentBase } from "../_ComponentBase";

/**
 * Shows an input control with label, validation error, info etc.
 *
 * Basic label-input control for forms.
 *
 * @example
 * ```html
 * <salaxy-input name="myInput" ng-model="temp" label="Basic example"></salaxy-input>
 * ```
 */
export class Input extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
        model: "ngModel",

        form: "?^^form",
    };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /** Minimum length for the text value, default is 0 (form validation) */
        minlength: "@",
        /** Maximum length for the text value, default is 1024 (form validation) */
        maxlength: "@",
        /** Regular expression pattern for validation (form validation) */
        pattern: "@",
        /** Type for the input. Defaults to text. Supports currently text and password. */
        type: "@",

     });

    /** Uses the InputController */
    public controller = InputController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/Input.html";
}
