
import { InputBase, InputBooleanController } from "../../controllers";

import { Objects } from "@salaxy/core";

import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a boolean input control with label, validation error, info etc.
 *
 * @example
 * ```html
 * <salaxy-input-boolean name="myInput" ng-model="temp" label="Basic example" type="radio"></salaxy-input>
 * ```
 */

export class InputBoolean extends ComponentBase {

  /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
  public require = {
    model: "ngModel",

    form: "?^^form",
};

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = Objects.extend(InputBase.crudBindings, {

     /** Type of the input element. Options are checkbox, radio, select and switch */
     type: "@",

     /** Text to show as a label for input with value FALSE
      * Supported by types radio and select
      */
     labelFalse: "@",

     /** Text to show as a label for input with value TRUE
      * Supported by types radio, checkbox and select
      */
     labelTrue: "@",

     /**
      * TODO: Is this needed when there's support for 'empty-label' ?
      * BS class for offsetting the input (no-label)
      */
     offsetCols: "@",

     /** If true, the radio buttons are aligned horizontally side-by-side */
     horizontal: "<",

  });

  /** Uses the InputEnumController */
  public controller = InputBooleanController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/form-controls/InputBoolean.html";
}
