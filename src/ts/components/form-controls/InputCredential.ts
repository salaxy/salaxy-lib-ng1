import { InputCredentialController } from "../../controllers";

import { InputEnum } from "./InputEnum";

/**
 * Select credential.
 *
 * @example
 * ```html
 * <salaxy-input-credential type="select"  name="Credential"  label="Credential"></salaxy-input-credential>
 * <salaxy-input-credential type="radio"  name="Credential" label="Credential" ></salaxy-input-credential>
 * <salaxy-input-credential type="typeahead"  name="Credential" label="Company"></salaxy-input-credential>
 * ```
 */
export class InputCredential extends InputEnum {

    constructor() {
      super();
      /** Uses the InputEnumController */
      this.controller = InputCredentialController as any;
    }
}
