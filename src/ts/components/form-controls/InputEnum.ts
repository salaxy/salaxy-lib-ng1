import { InputBase, InputEnumController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

import { Objects } from "@salaxy/core";

/**
 * Select component for enumerations.
 *
 * @example
 * ```html
 * <salaxy-input-enum type="select"  options="{FiOy:'Osakeyhtiö', FiTm:'Toiminimi'}"  name="CompanyType" enum="CompanyType" label="Company" invalid-enums="none,other" required></salaxy-input-enum>
 * <salaxy-input-enum type="radio"  name="CompanyType" enum="CompanyType" label="Company" hidden-options="unknown"></salaxy-input-enum>
 * <salaxy-input-enum type="typeahead"  name="CompanyType" enum="CompanyType" label="Company" hidden-options="unknown"></salaxy-input-enum>
 * ```
 */
export class InputEnum extends ComponentBase {

  /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
  public require = {
    model: "ngModel",

    form: "?^^form",
  };

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = Objects.extend(InputBase.crudBindings, {
    /**
     * Binds to an enumeration defined by the Salaxy API.
     * Set the name of the enumeration.
     */
    enum: "@",

    /** TODO: not working well with Typeahead-element
     * Options of the select control as a key-value object.
     */
    options: "<",

    /** Type of the input element. Options are typeahead, select and radio */
    type: "@",

    /**
     * These values are visible only if they are selected in the data.
     * I.e. after something else is selected, hidden value cannot be selected back.
     * Use for not-selected values ("Please choose...") when you do not want selection reversed
     * or legacy data that is not selectable, but may still exist on the server.
     */
    hiddenOptions: "@",

    /**
     * Array or comma separated string to filter the option values to just the given ones.
     * Also sets the order to this order, so works for ordering a given set of values.
     * Note that hiddenOptions is applied first, so if you want e.g. "undefined" to appear if selected,
     * you may add it here and it behaves as expected (undefined is still hidden if a value is selevted).
     */
    filter: "@",

    /**
     * Comma serapated list of values that should not be considered as valid in the UI.
     * Default is ["unknown"] and is only used when input is "required" and input type is "select".
     * Use in situations where an unknown/other/none value is set in API but should not be available as an option.
     */
    invalidEnums: "@",

    /** If true, the control is not caching values. */
    disableCache: "@",

    /** Function that is used in the type=list layout to render the avatar */
    avatarFunc: "<",

  });

  /** Uses the InputEnumController */
  public controller = InputEnumController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/form-controls/InputEnum.html";
}
