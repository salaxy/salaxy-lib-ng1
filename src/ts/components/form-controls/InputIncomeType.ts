import { InputBase, InputIncomeTypeController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

import { Objects } from "@salaxy/core";

/**
 * Visual selection for income types (and deductibles) resulting to number code in Incomes register.
 *
 * @example
 * ```html
 * <salaxy-input-income-type name="code" ng-model="row.code" label="income type"></salaxy-input-income-type>
 * ```
 */
export class InputIncomeType extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /** Type of the input element is either typehead or list. Default is typeahead. */
        type: "@",
        /** List of income type codes to exclude from the selection list. */
        hiddenCodes: "<",
    });

    /** Uses the InputIncomeTypeController */
    public controller = InputIncomeTypeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/InputIncomeType.html";
}
