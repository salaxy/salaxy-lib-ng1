import { Objects } from "@salaxy/core";

import { InputBase, InputNumberController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Input control for numbers.
 * @example
 * ```html
 * <salaxy-input-number name="count" ng-model="row.count" label="Number of items"></salaxy-input-number>
 * ```
 */
export class InputNumber extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

      /**
       * Expression for Unit for the number.
       * If set, shows a visual clue of the unit.
       * For 'percent' editor value is multiplied by 100.
       */
      unit: "@",

      /** Minimum number*/
      minimum: "@",

      /** Maximum number*/
      maximum: "@",

      /** Multiple of (step) number*/
      multipleOf: "@",

      /**
       * If true, the input edits negative value: The value is multiplied by -1.
       * E.g. "3" days ago => -3.
       */
      negative: "<",
    });

    /** Uses the InputCalcRowTypeController */
    public controller = InputNumberController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/InputNumber.html";
}
