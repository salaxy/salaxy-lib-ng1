import { InputBase, InputOccupationTypeController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

import { Objects } from "@salaxy/core";

/**
 * Select component for Occupation types.
 *
 * @example
 * ```html
 * <salaxy-input-occupation-type default-list="household" name="occupationType" ng-model="row.occupation" label="occupationType"></salaxy-input-occupation-type>
 * <salaxy-input-occupation-type default-list="21490,21510,21532" name="occupationType" ng-model="row.occupation" label="occupationType"></salaxy-input-occupation-type>
 * ```
 */
export class InputOccupationType extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
      model: "ngModel",

      form: "?^^form",
  };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /**
         * One or comma-separated list of occupation IDs, or a known keyword to define which occupations are shown
         * Supported keywords: 'household' and 'company'
         * If empty, shows all
         */
        defaultList: "@",

    });

    /** Uses the InputOccupationTypeController */
    public controller = InputOccupationTypeController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/InputOccupationType.html";
}
