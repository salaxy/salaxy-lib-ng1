import { InputBase, InputController } from "../../controllers";

import { Objects } from "@salaxy/core";

import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a textarea control with label, validation error, info etc.
 *
 * @example
 * ```html
 * <salaxy-textarea name="myInput" ng-model="temp" rows="5" label="Basic example"></salaxy-textarea>
 * ```
 */
export class Textarea extends ComponentBase {

    /** ng-model is required, form tag is optionally used for validation and disabled/readonly */
    public require = {
        model: "ngModel",

        form: "?^^form",
    };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(InputBase.crudBindings, {

        /** Minimum length for the text value, default is 0 (form validation) */
        minlength: "@",
        /** Maximum length for the text value, default is 1024 (form validation) */
        maxlength: "@",
        /** Regular expression pattern for validation (form validation) */
        pattern: "@",

        /** The rows property of the textarea. Default is 3. */
        rows: "<",

     });

    /** Uses the InputController */
    public controller = InputController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/Textarea.html";
}
