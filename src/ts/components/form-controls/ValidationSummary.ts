import { ValidationSummaryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a validation summary user interface.
 * Currently, this component is used for showing the status of server-side validation,
 * but the idea is that later implementation may enable it for using also with client-side validation.
 *
 * @example
 * ```html
 * <salaxy-validation-summary api-validation="$ctrl.validationData">
 *       <span>Everything is OK!</span>
 * </salaxy-validation-summary>
 * ```
 */
export class ValidationSummary extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** The server-side API-validation object that is displayed by this component. */
        apiValidation: "<",

        /** HTML that should be shown when the bound validation data is null. */
        loadingHtml: "@",
    };

    /** Inner HTML is shown in case object is valid. */
    public transclude = true;

    /** Uses the ValidationSummaryController */
    public controller = ValidationSummaryController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/ValidationSummary.html";
}
