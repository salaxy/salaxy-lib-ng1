import { ActionButtonController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Helper component to generate buttons with known actions such as creating a new worker, new calculation etc.
 *
 * @example
 * ```html
 *  <salaxy-action-button label="SALAXY.NG1.WorkerDetailsComponent.newCalculation" action="'newCalcForWorker'" options="{'id': $ctrl.currentId }" button-class="btn-default pull-right-xs"></salaxy-action-button>
 * ```
 */

export class ActionButton extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

    /** Text for the button */
    label: "@",

    /** Additional Bootstrap or custom style classes. Bootstrap I.e. btn-danger, btn-sm, my-btn. Defaults to btn-primary */
    buttonClass: "@",

    /** Disabled but visible */
    disabled: "<",

    /** Options for button. */
    options: "<",

    /**
     * Action to be executed.
     * newCalc: starts a new blank calculation.
     * newCalcForWorker: starts a new calculation for the current worker.
     * newPayroll: starts a new blank payroll.
     * newWorker: opens workerWizard for creating a new worker.
     * newMessage: starts a new message thread with partner.
     * payment: Not supported at the moment. opens the payment dialog for the current calculation.
     * newTaxcard: Links to taxcards page. Currently supported only when role is Worker.
     */
    action: "<",

  };

  /** Uses the NaviController */
  public controller = ActionButtonController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/helpers/ActionButton.html";
}
