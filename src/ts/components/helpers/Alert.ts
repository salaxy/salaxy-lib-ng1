import { AlertController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows an alert box with styling.
 *
 * Based on Bootstrap alert, but has the following additional functionality:
 *
 * - primary style (in addition to success, info, warning and danger) for banner type of advertising
 * - Icon for better visuality
 * - Optional "Read more" area
 *
 * @example
 * ```html
 * <salaxy-alert type="warning" text="This is a Warning"></salaxy-alert>
 * ```
 */
export class Alert extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

        /**
         * Possibility to speicfy a font-awesome icon.
         * Setting "none", will show no icon.
         * If not set, it is determined by type.
         */
        icon: "@",

        /** Type of the alert is the Bootstrap style: Note that also "primary" and "default" are supported. */
        type: "@",

        /**
         * Alert main content as simple text.
         * You can alternatively provide html as main element.
         */
        text: "@",
    };

    /**
     * Transclusion slots
     */
    public transclude = {
        /**
         * The main content of the alert.
         * You can alternatively provide a simple text in "text" property.
         */
        main: "?main",

        /**
         * Optional Details part of the alert.
         * Will automatically show "Read more" button.
         */
        aside: "?aside",
    };

    /** Uses the AlertController */
    public controller = AlertController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Alert.html";
}
