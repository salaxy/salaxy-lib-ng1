import { AvatarController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the Palkkaus Avatar image: This is either an image or a font-icon with a user specific color/initilas combination.
 *
 * @example
 * ```html
 * <salaxy-avatar avatar="$ctrl.myAvatar"></salaxy-avatar>
 * ```
 */
export class Avatar extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** The Avatar object that should be rendered */
        avatar: "=",
    };

    /** Uses the AvatarController */
    public controller = AvatarController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Avatar.html";

    /** Rendered HTML replaces the original element */
    public replace: true;
}
