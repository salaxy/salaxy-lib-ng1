import { CalendarController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a monthly calendar where one month is one row and you can show ranges and special days (only partial support).
 * Future implementations will support categories / swimlanes of periods (e.g. holidays from several persons)
 * and have better support for special days: different types of markers, pop-up etc.
 *
 * @example
 * ```html
 * <salaxy-calendar start="'2019-01-5'" end="$ctrl.getToday()" periods="$ctrl.getPeriods()"></salaxy-chart>
 * ```
 */
export class Calendar extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

        /**
         * Start date for the calendar as ISO date yyyy-MM-dd. Renders from the beginning of this month.
         */
        start: "<",

        /**
         * End date of the calendar as ISO date yyyy-MM-dd. Renders until the end of this month.
         */
        end: "<",

        /**
         * Today date for the calendar as ISO date yyyy-MM-dd.
         * Can be set as dately object (e.g. ISO string, JS Date or key string "today")
         */
        today: "<",

        /** The data that is plotted on the calendar chart. */
        data: "<",

        /** Optional data mapper function that is used in mapping the data to CalendarSeries array. */
        mapper: "<",

        /** TODO: Remove */
        periods: "<",

        /** TODO: Remove */
        days: "<",

        /** Type of chart: "align-weekdays", "align-left" or horizontal. */
        mode: "@",

        /**
         * Function that is called when user selects an item in the calendar.
         * Function can have the following locals: type: "event" | "day", date: string, day: Full calendar day with all the day info,
         * calEvent: The calendar event if type is "event", series: Series that the event belongs to or the series that is clicked (not always available).
         * @example <salaxy-calendar on-list-select="$ctrl.myCustomSelectFunc(type, date, calEvent)"></salaxy-calendar>
         */
        onListSelect: "&",

        /** Possibility of specifying a list controller that can be used for item editing. */
        listController: "<",
    };

    /** Uses the CalendarController */
    public controller = CalendarController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Calendar.html";
}
