import { ChartController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a angular-chart.js with Salaxy defaults, caching, colors etc.
 * Basic use is to define a type and getChart() function that returns object with data and labels (plus other options and proiperties as neeed).
 * Caching watches for changes in type and data property returned by getChart()
 * so if you make changes to other properties after first rendering, make sure to call $ctrl.refresh().
 * However, the getChart() method is called only every 1 second.
 *
 * @example
 * ```html
 * <salaxy-chart type="pie" on-get-chart="$ctrl.getChart()"></salaxy-chart>
 * ```
 */
export class Chart extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

        /**
         * Function that returns an object with data, options and all the other properties for the chart.
         * The method call is cached first for 1 second and then data is bound to Angular UI only if the data property changes (to avoid flickering etc.)
         * If you change the other properties without changing the data, call the refresh() method to force cache refresh.
         */
        onGetChart: "&",

        /** Type of chart: "line" (default), "bar", "doughnut", "radar", "pie", "polarArea", "horizontalBar" or "bubble" */
        type: "@",
    };

    /** Uses the ChartController */
    public controller = ChartController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Chart.html";
}
