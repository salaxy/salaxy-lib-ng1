import { JsonFormatterController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a JavaScript object JSON as formatted HTML.
 * Open attribute accepts a number that indicated how many levels JSON should be open.
 * Based on http://azimi.me/json-formatter/demo/demo.html
 *
 * @example
 * ```html
 * <salaxy-json-formatter open="1" json="$ctrl.current"></salaxy-json-formatter>
 * <salaxy-json-formatter key="'Debugger'" json="$ctrl.sitemap" open="1" type="dialog"></salaxy-json-formatter>
 * ```
 */
export class JsonFormatter extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** The JavaScript object that should be displayed. */
      json: "<",
      /** If true json is compared to compare-to value. */
      compare: "<",
      /** The original JavaScript to which to show DIFF (show changes in data). */
      compareTo: "<",
      /** When displaying children, sets the property / key name for json */
      key: "<",
      /** Number that indicated how many levels JSON should be open */
      open: "<",
      /**
       * Set to "dialog" if you want to show a button that opens the JSON in dialog.
       * Otherwise, an inline tree view is shown.
       */
      type: "@",
    };

    /** Uses the JsonFormatterController */
    public controller = JsonFormatterController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/JsonFormatter.html";

    /** Rendered HTML replaces the original element */
    public replace: true;
}
