import { NaviController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Helper component to generate navigation components and views:
 * Top- and side-menus, paths and controls that show the current title.
 * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
 *
 *
 * NOTE: This is just an optional helper to make creating simple demo sites easier.
 * There is no need to use NaviService, NaviController or components in your custom site!
 * You can use something completely different.
 *
 * @example
 * ```html
 * <salaxy-navi-sitemap></salaxy-navi-sitemap>
 * ```
 */

export class NaviSitemap extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
        /**
         * - mode: "default" or "accordion"
         * - default: shows full two levels of the tree. Typically used as sitemap in the content area.
         * - accordion: shows first level of the navi tree and second level only if is in the current path.
         * Typically used in left menu navigation.
         * - 3-levels: Accordion with 3 levels.
         */
        mode: "@",
    };

    /** Uses the NaviController */
    public controller = NaviController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/NaviSitemap.html";
}
