import { NaviController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Helper component to easily create a web site that is responsive and usable.
 * The starter contains:
 *
 * - Customizable Header with title
 * - Left menu with logo, authentication info / login button and sitemap
 * - Mobile version with simple header: Title + Hamburger button that shows the menu, which is hidden by default.
 * - Loader message / animation
 * - Content area as ng-view
 * - Alert container
 *
 *
 * NOTE: This is just an optional helper to make creating simple (demo) sites easier.
 * There is no need to use NaviStarterLayout, NaviService, NaviController or components in your custom site!
 * You can use something completely different.
 *
 * @example
 * ```html
 * <salaxy-navi-starter-layout>
 *   <header ng-controller="NaviController as navi">
 *     Your html here: {{ navi.title }}
 *   </header>
 *   <main ng-controller="NaviController as navi">
 *   	<div ng-class="navi.current.isFullWidth ? 'container-fluid' : 'container'" ng-view autoscroll="true"></div>
 *   </main>
 * </salaxy-navi-starter-layout>
 * ```
 */
export class NaviStarterLayout extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
    /**
     * - mode: "default" or "accordion"
     * - default: shows full two levels of the tree. Typically used as sitemap in the content area.
     * - accordion: shows first level of the navi tree and second level only if is in the current path.
     * Typically used in left menu navigation.
     */
    mode: "@",
  };

  /**
   * Component may contain header and main tags to
   * override the default rendering of the header (non-mobile) and main content area respectively.
   */
  public transclude = {
    /** Header part of the page. Note that mobile header is defined separately. */
    header: "?header",
    /** Main content part of the page */
    main: "?main",
    /** The main navigation: By default: left sidebar. */
    nav: "?nav",
  };

  /** Uses the NaviController */
  public controller = NaviController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/helpers/NaviStarterLayout.html";

}
