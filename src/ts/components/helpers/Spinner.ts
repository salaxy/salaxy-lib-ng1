import { AvatarController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Spinner as a UI component
 *
 * @example
 * ```html
 * <salaxy-spinner salaxy-if-role="init" full-screen="true" heading="Loading..." text="Please wait."></salaxy-spinner>
 * ```
 */
export class Spinner extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** If true, the spinner is shown full screen. */
        fullScreen: "=",

        /**
         * Heading is the larger text under the spinner.
         * The text is translated.
         */
        heading: "@",

        /**
         * Small text - use pre for line breaks.
         * The text is translated.
         */
        text: "@",
    };

    /** Uses the AvatarController */
    public controller = AvatarController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Spinner.html";
}
