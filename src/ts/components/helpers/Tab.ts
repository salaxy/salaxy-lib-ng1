import { TabController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Component for a single tab pane in the tabs control.
 * Shows a navigation header and a tab pane.
 * Contains salaxy-tab-heading element for the tab header.
 * Contains salaxy-tab-content element for the tab pane content.
 * The disable attribute can be used to disable the tab.
 * The index attribute can be used to name the tab. The index
 * is used in setting the active tab in the salaxy-tabs element using the active attribute.
 *
 * @example
 * ```html
 * <salaxy-tabs active="active">
 *   <salaxy-tab>
 *      <salaxy-tab-heading>Tab number one</salaxy-tab-heading>
 *      <salaxy-tab-content>
 *          <p>Tab one content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 *  <salaxy-tab index="'kolme'">
 *      <salaxy-tab-heading><i>Tab number two</i></salaxy-tab-heading>
 *      <salaxy-tab-content>
 *          <p>Tab two content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 *  <salaxy-tab disable="true" heading="Tab text number three">
 *      <salaxy-tab-content>
 *          <p>Tab three content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 * </salaxy-tabs>
 * ```
 */
export class Tab extends ComponentBase   {

    /** Require salaxy-tabs */
    public require = {
        tabsCtrl: "^salaxyTabs",
    };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
     public bindings = {
        /** Index/name for the tab. */
         index: "<",
        /** Disable tab attribute. */
         disable: "<",
        /** Text heading. */
        heading: "@",
        /** Selection event handler */
        onSelect: "&select",
     };

     /** Uses the TabController */
     public controller = TabController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Tab.html";

    /** Transclusion */
    public transclude = {
        content: "?salaxyTabContent",
        heading: "?salaxyTabHeading",
    };
}
