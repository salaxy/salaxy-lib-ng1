import { TabsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**

 * Component for a tabs control.
 * Shows navigation headers and tab panes.
 * Contains one or more salaxy-tab elements, each for one tab.
 * The active-attribute shows/sets the current active tab.
 *
 * @example
 * ```html
 * <salaxy-tabs active="active">
 *   <salaxy-tab>
 *      <salaxy-tab-heading>Tab number one</salaxy-tab-heading>
 *      <salaxy-tab-content>
 *          <p>Tab one content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 *  <salaxy-tab index="'kolme'">
 *      <salaxy-tab-heading><i>Tab number two</i></salaxy-tab-heading>
 *      <salaxy-tab-content>
 *          <p>Tab two content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 *  <salaxy-tab disable="true" heading="Tab text number three">
 *      <salaxy-tab-content>
 *          <p>Tab three content</p>
 *      </salaxy-tab-content>
 *  </salaxy-tab>
 * </salaxy-tabs>
 * ```
 */
export class Tabs extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
     public bindings = {
        /** Expression for active tab in the tabset */
        active: "=?",
     };

    /** Uses the TabsController */
    public controller = TabsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Tabs.html";

    /** Transclusion */
    public transclude = true;

}
