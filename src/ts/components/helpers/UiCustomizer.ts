import { UiCustomizerController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * User interface for customizing the user interface with LESS variables
 * and later possibly also other properties.
 *
 * @example
 * ```html
 * <salaxy-ui-customizer></salaxy-ui-customizer>
 * ```
 */
export class UiCustomizer extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
    };

    /** Uses the UiCustomizerController */
    public controller = UiCustomizerController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/UiCustomizer.html";
}
