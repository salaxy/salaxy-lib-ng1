export * from "./index_components";
export * from "./_ComponentBase";
export * from "./_ComponentsRegistration";
export { SalaxyNg1ComponentsModule } from "./_NgComponents";
