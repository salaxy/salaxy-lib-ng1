import { CreditTransferController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Credit Transfer Form for Invoice visualization.
 *
 * @example
 * ```html
 * <salaxy-credit-transfer invoice="$ctrl.current">
 *   <message>My <strong>custom HTML</strong> message</message>
 * </salaxy-credit-transfer>
 * ```
 */
export class CreditTransfer extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

    /** Invoice to display in the credit transfer. */
    invoice: "<",

    /** Invoice preview to display in the credit transfer. */
    invoicePreview: "<",

    /**
     * Display mode is either
     *
     * - "official": The official printable credit transfer (Finanssialan keskusliitto)
     * - "barcode-copy": The copy-paste input + button (in a div) for copy pasting the the barcode into a web bank
     * - "default": Both of the above separted by an explaining text.
     */
    mode: "@",
  };

  /**
   * Transclusion slots
   */
  public transclude = {
    /**
     * HTML to include in the message section
     */
    message: "?message",
  };

  /** Uses the CreateCompanyAccountWizardController */
  public controller = CreditTransferController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/invoices/CreditTransfer.html";

}
