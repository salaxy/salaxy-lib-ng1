import { ApiCrudObjectControllerBindings, InvoiceCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Edit / actions user interface for a single invoice.
 *
 * @example
 * ```html
 * <salaxy-invoice-edit model="'url'"></salaxy-invoice-edit>
 * ```
 */
export class InvoiceEdit extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = new ApiCrudObjectControllerBindings();

    /** Uses the InvoiceCrudController */
    public controller = InvoiceCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/invoices/InvoiceEdit.html";

}
