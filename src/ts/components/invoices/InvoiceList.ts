import { InvoiceCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of salary reports.
 *
 * @example
 * ```html
 * <salaxy-invoice-list mode="panel"></salaxy-salary-report-list>
 * ```
 */
export class InvoiceList extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /**
       * list view 'mode'.
       * If undefined, defaults to full list.
       * Use panel for dashboards etc.
       */
      mode: "@",

     };

    /** Uses the InvoiceCrudController */
    public controller = InvoiceCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/invoices/InvoiceList.html";

}
