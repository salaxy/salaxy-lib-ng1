import { ApiCrudObjectControllerBindings, InvoiceCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a print-like preview for an invoice.
 *
 * @example
 * ```html
 * <salaxy-invoice-preview model="$ctrl.current"></salaxy-invoice-preview>
 * ```
 */
export class InvoicePreview extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = new ApiCrudObjectControllerBindings();

    /** Uses the InvoiceCrudController */
    public controller = InvoiceCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/invoices/InvoicePreview.html";

}
