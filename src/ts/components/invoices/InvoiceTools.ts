import { InvoiceToolsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the export tools for the invoices.
 *
 * @example
 * ```html
 * <salaxy-invoice-tools reader="$ctrl.dataReader"></salaxy-invoice-tools>
 * ```
 */
export class InvoiceTools extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** Reference to the data reader object. */
      reader: "<",
    };

    /** Uses the InvoiceToolsController */
    public controller = InvoiceToolsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/invoices/InvoiceTools.html";

}
