import { ApiCrudObjectControllerBindings, InvoicesCreateController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a view for creating invoices: PaymentChannel selection, validation errors if necessary,
 * Payment channel selection and button for creating invoices for Payroll or Calculation.
 * If pointed to a paid calculation, shows the invoices that were already created or legacy payment method.
 *
 * @example
 * ```html
 * <salaxy-invoices-create model="$ctrl.current"></salaxy-invoices-create>
 * ```
 */
export class InvoicesCreate extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = new ApiCrudObjectControllerBindings();

    /** Uses the InvoicesCreateController */
    public controller = InvoicesCreateController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/invoices/InvoicesCreate.html";

}
