import { AuthorizationSwitchController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * OBSOLETE: Refactor this out => Use simple Enable/Disable buttons in Authorization dialog.
 *
 * @deprecated Refactor this out => Use simple Enable/Disable buttons in Authorization dialog.
 */
export class AuthSwitch extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

        /** - name: Name of the input - also used as id. */
        name: "@",
        /** - id: */
        id: "@",
        /** - onText: */
        onText: "@",
        /** - offText: */
        offText: "@",
        /** - disabled: */
        disabled: "<",
    };

    /** Uses the AuthorizationSwitchController */
    public controller = AuthorizationSwitchController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/AuthSwitch.html";

}
