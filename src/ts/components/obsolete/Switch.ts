import { SwitchController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * OBSOLETE: Assure salaxy-input-boolean has the same functionality and remove this component.
 * Shows a salaxy switch icon
 *
 * @deprecated Assure salaxy-input-boolean has the same functionality and remove this component.
 *
 * Please note that also the ng-model directive is required.
 *
 * @example
 * ```html
 * <salaxy-switch xxx="tax"></salaxy-switch>
 * ```
 */
export class Switch extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {

       /** Name of the input - also used as id. */
        name: "@",
        /** - id: */
        id: "@",
        /** - onText: */
        onText: "@",
        /** - offText: */
        offText: "@",
        /** - disabled: */
        disabled: "<",
    };

    /** ngModel is required */
    public require = {
        model: "ngModel",
    };

    /** Uses the SwitchController */
    public controller = SwitchController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/form-controls/Switch.html";
}
