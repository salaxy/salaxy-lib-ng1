import { WorkerAssureController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * DEPRECATED: Being used by an isolated partner(s) that have special access.
 * => Remove in a next major release.
 * Alternative component for adding Worker accounts.
 * Uses the AssureWorkerAccount API service that is currently reserved for partner access.
 *
 * @deprecated Being used by an isolated partner(s) that have special access. => Remove in a next major release.
 *
 * @example
 * ```html
 * <salaxy-worker-assure></salaxy-worker-assure>
 * ```
 */
export class WorkerAssure extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {};

    /** Uses the WorkerAssureController */
    public controller = WorkerAssureController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/WorkerAssure.html";

}
