import { YearEndHouseholdController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * OBSOLETE: This component is used only for 2020 process: Household service for stopped at the end of 2020.
 * Rakennusliitto Palkkamylly will use a different process / component.
 * Renders a yearly feedback form that can be used by an authenticated user to check the calculations of the year and post fixing requests to Salaxy customer service.
 *
 * @deprecated This component is used only for 2020 process: Household service for stopped at the end of 2020.
 * Rakennusliitto Palkkamylly will use a different process / component.
 *
 * @example
 * ```html
 * <salaxy-year-end-feedback-household></salaxy-year-end-feedback-household>
 * ```
 */
export class YearEndFeedbackHousehold extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** If true, forces the hasBeenSent to false => Allows edit. */
      forceEdit: "<",
    };

    /** Uses the YearEndCompanyController */
    public controller = YearEndHouseholdController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/year-end/FeedbackHousehold.html";

}
