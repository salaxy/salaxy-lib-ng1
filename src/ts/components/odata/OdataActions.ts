import { ODataActionsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders the search box and export/import functions within OdataTable.
 * Requires salaxy-odata-table component as parent.
 */
export class OdataActions extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** Default locale for data exports/imports. */
        localeId: "<",
        /** Flag indicating if the export options are visible. */
        showExport: "@",
        /** Flag indicating if the imports options are visible. */
        showImport: "@",
    };

    /** Attribute must be used within salaxy-odata-table component to access the common ODataQueryController  */
    public require = {
      $odata: "^^salaxyOdataTable",
    };

    /** Actions */
    public transclude = true;

    /** Uses the ODataMenuController */
    public controller = ODataActionsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/odata/OdataActions.html";
}
