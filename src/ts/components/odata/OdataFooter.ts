import { ODataHelperController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders the footer within OdataTable.
 * Requires salaxy-odata-table component as parent.
 * TODO: Example
 */
export class OdataFooter extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** Type of paging */
        paging: "@",

        /**
         * Manually defined URL for the nextPageLink.
         * Browser is directed to this URL instead of loading more data from nextPageLink.
         */
        nextPageUrl: "@",
    };

    /** Attribute must be used within salaxy-odata-table component to access the common ODataQueryController  */
    public require = {
      $odata: "^^salaxyOdataTable",
    };

    /** Uses the ODataHelperController */
    public controller = ODataHelperController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/odata/OdataFooter.html";
}
