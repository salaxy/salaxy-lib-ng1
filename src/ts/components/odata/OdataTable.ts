import { ODataQueryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Helper for rendering an OData service based table / grid.
 *
 * @example
 * ```html
 * <salaxy-odata-table url="/v03-rc/api/cms/articles/manual" options="{ $count: true }">
 *   <table class="table table-condensed">
 *     <thead>
 *       <tr>
 *         <salaxy-odata th-orderby="title">Title</salaxy-odata>
 *       </tr>
 *     </thead>
 *     <tbody>
 *       <tr ng-repeat="row in $ctrl.items"><td>{{ row.title }}</td></tr>
 *     </tbody>
 *   </table>
 * </salaxy-odata-table>
 * ```
 */
export class OdataTable extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
        /** OData query options */
        options: "<",

        /** URL to the OData service. */
        url: "@",

        /** Data passed from the parent to the component / view */
        data: "<",

        /** Selected items for the case where the list is used as a select list (as opposed to link list). */
        selectedItems: "<",

        /** Current list of items. */
        items: "<",

        /**
         * Function that is called when user selects an item in the list.
         * Note that the event is called only in the selections (single and multi). Not when there is a link to details view in a link list.
         * Function has the following locals: value: true/false, item: the last selected/unselected item, allItems: Array of all currently selected items.
         * @example <salaxy-worker-list mode="select" on-list-select="$ctrl.updateFromEmployment(item.id)"></salaxy-worker-list>
         */
        onListSelect: "&",

        /** Function that is called when the data has been loaded and set as values of the items. */
        onDataLoaded: "&",

        /**
         * CRUD controller that typically defines the values for url template and
         * @example
         * <salaxy-odata-table crud-controller="PayrollCrudController as $crud">
         */
        crudController: "@",

        /**
         * Optional bindings for the CRUD controller. All properties are set to crudController before calling init.
         * @example
         * <salaxy-odata-table crud-controller="PayrollCrudController as $crud" crud-bindings="{ myProp: 'foobar' }">
         */
        crudBindings: "<",

        /** Defines the cache key. A non-null value also means that the caqche is enabled. */
        cacheKey: "@",

        /** Optional reader component for OData. Uses the given url and reads data from the OData source. */
        reader: "<",

        /** If readOnly is true, edit, copy etc. buttons from the new list view are hidden if readOnly property is supported */
        readOnly: "<",
    };

    /** Uses the ODataQueryController */
    public controller = ODataQueryController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/odata/lists/_default.html";

    /** Rendered HTML replaces the original element */
    public replace: true;
}
