import { SessionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows account authorization and Credentials information including
 * a link to Authorization document (digitally signed pdf) and
 * type and UID (typically e-mail) of the current credentials.
 *
 * @example
 * ```html
 * <salaxy-account-authorization-person></salaxy-account-authorization-person>
 * ```
 */
export class AccountAuthorizationPerson extends ComponentBase {

    /** The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to controller implementation
     */
    public bindings = {};

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountAuthorizationPerson.html";
}
