
import { PersonAccountController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Edit the avatar information: Picture upload, Initials + color or Gravatar.
 * Currently used only in Personal accounts, but this could potentially be also
 * used in Company side to edit Avatar?
 *
 * @example
 * ```html
 * <salaxy-account-avatar-edit-person></salaxy-account-avatar-edit-person>
 * ```
 */
export class AccountAvatarEditPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Current person account. Optional */
      current: "<",
    };

    /** Uses the PersonAccountController */
    public controller = PersonAccountController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountAvatarEditPerson.html";

}
