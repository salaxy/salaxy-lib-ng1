
import { PersonAccountController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Editing UI of Contact information for Personal account.
 *
 * @example
 * ```html
 * <salaxy-account-contact-edit-person></salaxy-account-contact-edit-person>
 * ```
 */
export class AccountContactEditPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Current person account. Optional */
      current: "<",
    };

    /** Uses the PersonAccountController */
    public controller = PersonAccountController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountContactEditPerson.html";

}
