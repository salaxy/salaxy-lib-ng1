import { PersonAccountController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Editor for main identity data of Personal account: Names (currently opens a wizard),
 * Personal ID (read-only) or IBAN (should require strong authentication).
 *
 * @example
 * ```html
 * <salaxy-account-details-edit-person></salaxy-account-details-edit-person>
 * ```
 */
export class AccountDetailsEditPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Current person account. Optional */
      current: "<",
    };

    /** Uses the PersonAccountController */
    public controller = PersonAccountController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountDetailsEditPerson.html";

}
