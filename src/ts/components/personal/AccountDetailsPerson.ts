
import { PersonAccountController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Editor view for Person's contact and payment information,
 * for example avatar, phone number and bank account number.
 *
 * @example
 * ```html
 * <salaxy-account-details-person></salaxy-account-details-person>
 * ```
 */
export class AccountDetailsPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Current tab in initialization. If not set, will be fetched from url hash. */
      currentTab: "@",
    };

    /** Uses the PersonAccountController */
    public controller = PersonAccountController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountDetailsPerson.html";

}
