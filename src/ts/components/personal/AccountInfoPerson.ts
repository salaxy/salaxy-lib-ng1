import { SessionController} from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Quick basic information about the Person account: Avatar, name, phone and e-mail.
 * The component may be bound with ng-model or if omitted binds to session current account.
 * The component is currently for Personal acocunt only, but it could be generalized
 * for companies as well.
 *
 * @example
 * ```html
 * <salaxy-account-info-person view-type="details" ng-model=""></salaxy-account-info-person>
 * ```
 */
export class AccountInfoPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {

    };

    /** ngModel may be used */
    public require = {
      model: "?ngModel",
    };

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountInfoPerson.html";

}
