
import { CompanySettingsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Edit UI for Household's own insurance information including Pension and Unemployment insurance.
 * Designed for Person Employer only, should not be shown in Worker scenarios as it may be confusing.
 *
 * @example
 * ```html
 * <salaxy-account-insurance-edit-person></salaxy-account-insurance-edit-person>
 * ```
 */
export class AccountInsuranceEditPerson extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
    };

    /** Uses the CompanySettingsController */
    public controller = CompanySettingsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/AccountInsuranceEditPerson.html";

}
