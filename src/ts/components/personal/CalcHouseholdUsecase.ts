import { CalcHouseholdUsecaseController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * UI for setting the Household Usecases within the calculator
 *
 * @example
 * ```html
 * <salaxy-calc-household-usecase calc="$ctrl.currentCalc"></salaxy-calc-household-usecase>
 * ```
 */
export class CalcHouseholdUsecase extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
    /** The calculation that the component edits */
    calc: "<",

    /** Alternative data binding: creates a calculation based on Worker defaults. */
    worker: "<",

    /**
     * Shows the Save and Reset buttons.
     * Currently, this is only enabled for WorkerAccount data binding, but may later be enabled for calc.
     */
    showSave: "<",

    /** isReadOnly*/
    isReadOnly: "<"
  };

  /** Uses the CalcHouseholdUsecaseController */
  public controller = CalcHouseholdUsecaseController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/personal/CalcHouseholdUsecase.html";

}
