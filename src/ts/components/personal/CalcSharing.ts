import { CalcSharingController } from "../../controllers";
import { ApiCrudObjectControllerBindings } from "../../controllers/bases";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the sharing control for the current calculation
 *
 * @example
 * ```html
 * <salaxy-calc-sharing model='url'></salaxy-calc-sharing>
 * ```
 */
export class CalcSharing extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
   public bindings = (new class extends ApiCrudObjectControllerBindings {

       /** If true, template shows option to share direct link to the calculation. Currently used only in Rakennusliitto site. */
      public showLinkSharing = "<";
    }());

    /** Uses the CalcSharingController */
    public controller = CalcSharingController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/CalcSharing.html";

}
