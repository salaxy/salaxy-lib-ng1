import { CalcSharingController } from "../../controllers";
import { ApiCrudObjectControllerBindings } from "../../controllers/bases";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the current status of sharing for the calculation
 *
 * @example
 * ```html
 * <salaxy-calc-sharing-info model='url'></salaxy-calc-sharing-info>
 * ```
 */
export class CalcSharingInfo extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = new ApiCrudObjectControllerBindings();

    /** Uses the CalcSharingController */
    public controller = CalcSharingController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/CalcSharingInfo.html";

}
