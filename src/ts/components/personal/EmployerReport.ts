import { EmployerReportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the employer report.
 *
 * @example
 * ```html
 * <salaxy-employer-report></salaxy-employer-report>
 * ```
 */
export class EmployerReport extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
  };

  /** Uses the EmployerReportController */
  public controller = EmployerReportController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/personal/EmployerReport.html";

}
