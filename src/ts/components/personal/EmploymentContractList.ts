import { EmploymentContractCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a read-only list of employment contracts.
 * Currently only available for private person (Household / Worker)
 * New Employment Contract that supports Companies is in roadmap.
 *
 * @example
 * ```html
 * <salaxy-employment-contract-list></salaxy-employment-contract-list>
 * ```
 */
export class EmploymentContractList extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = { };

    /** Uses the PayCertificateController */
    public controller = EmploymentContractCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/personal/EmploymentContractList.html";

}
