import { HouseholdDeductionReportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the household deduction report.
 *
 * @example
 * ```html
 * <salaxy-household-deduction-report></salaxy-household-deduction-report>
 * ```
 */
export class HouseholdDeductionReport extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
  };

  /** Uses the HouseholdDeductionReportController */
  public controller = HouseholdDeductionReportController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/personal/HouseholdDeductionReport.html";

}
