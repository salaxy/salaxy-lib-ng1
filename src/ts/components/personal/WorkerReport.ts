import { WorkerReportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the worker report.
 *
 * @example
 * ```html
 * <salaxy-worker-report></salaxy-worker-report>
 * ```
 */
export class WorkerReport extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
  };

  /** Uses the WorkerReportController */
  public controller = WorkerReportController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/personal/WorkerReport.html";

}
