import { AccountingReportEditorController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the accounting report configurator for editing chart of accounts and account mappings.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-editor></salaxy-accouting-report-editor>
 * ```
 */
export class AccountingReportEditor extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {};

    /** Uses the AccountingReportEditorController */
    public controller = AccountingReportEditorController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportEditor.html";

}
