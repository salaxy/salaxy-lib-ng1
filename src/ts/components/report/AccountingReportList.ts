import { AccountingReportCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of accounting reports.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-list></salaxy-accounting-report-list>
 * ```
 */
export class AccountingReportList extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /**
       * list view 'mode'.
       * If undefined, defaults to full list.
       * Use panel for dashboards etc.
       */
      mode: "@",

     };

    /** Uses the AccountingReportCrudController */
    public controller = AccountingReportCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportList.html";

}
