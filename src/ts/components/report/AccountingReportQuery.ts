import { AccountingReportQueryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the accounting report builder for ad hoc reporting queries.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-query></salaxy-accouting-report-query>
 * ```
 */
export class AccountingReportQuery extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
    };

    /** Uses the AccountingReportQueryController */
    public controller = AccountingReportQueryController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportQuery.html";

}
