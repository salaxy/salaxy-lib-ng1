import { AccountingReportRowsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the rows of the accounting report.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-rows data="$ctrl.data"></salaxy-accouting-report-rows>
 * ```
 */
export class AccountingReportRows extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** Accounting data */
      data: "<",
    };

    /** Uses the AccountingReportRowsController */
    public controller = AccountingReportRowsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportRows.html";

}
