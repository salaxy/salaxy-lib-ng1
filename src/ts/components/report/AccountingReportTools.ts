import { AccountingReportToolsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the export tools for the accounting report.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-tools reader="$ctrl.dataReader"></salaxy-accouting-report-tools>
 * ```
 */
export class AccountingReportTools extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
      /** Reference to the data reader object. */
      reader: "<",
      /** Called when the target has been changed. */
      onTargetChange: "&",
    };

    /** Uses the AccountingReportToolsController */
    public controller = AccountingReportToolsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportTools.html";

}
