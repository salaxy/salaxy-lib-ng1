import { AccountingReportCrudController } from "../../controllers";
import { ApiCrudObjectControllerBindings } from "../../controllers/bases";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the accounting report viewer for the pre-built reports.
 *
 * @example
 * ```html
 * <salaxy-accounting-report-viewer model='url'></salaxy-accounting-report-viewer>
 * ```
 */
export class AccountingReportViewer extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = new ApiCrudObjectControllerBindings();

    /** Uses the AccountingReportCrudController */
    public controller = AccountingReportCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/AccountingReportViewer.html";
}
