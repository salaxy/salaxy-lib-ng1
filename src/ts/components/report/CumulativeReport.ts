import { CumulativeReportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows the cumulative report builder for ad hoc cumulative reports.
 *
 * @example
 * ```html
 * <salaxy-cumulative-report></salaxy-cumulative-report>
 * ```
 */
export class CumulativeReport extends ComponentBase {

   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
    };

    /** Uses the CumulativeReportController */
    public controller = CumulativeReportController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/CumulativeReport.html";

}
