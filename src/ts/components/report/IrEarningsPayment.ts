import { ApiCrudObjectControllerBindings, IrEarningsPaymentCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a user interface for Earnings Payment Report ("Tulorekisteri-ilmoitus").
 * Currently, the UI is read-only, but it may later be extended to be editable
 * for purposes of corrections.
 *
 * @example
 * ```html
 * <salaxy-ir-earnings-payment model="$ctrl.current"></salaxy-ir-earnings-payment>
 * ```
 */
export class IrEarningsPayment extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {
    /**
     * Alternative model binding.
     * Instead of model="EarningsPaymentId", you may specify calculation-id="calculationId"
     */
    public calculationId = "<";

    /**
     * Alternative model binding.
     * Instead of model="EarningsPaymentId", you may specify calculation="calculation"
     */
    public calculation = "<";
  }());

  /** Uses the EarningsPaymentReportController */
  public controller = IrEarningsPaymentCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/report/IrEarningsPayment.html";

}
