import { IrEarningsPaymentCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of earnings payments.
 *
 * @example
 * ```html
 * <salaxy-ir-earnings-payment-list></salaxy-ir-earnings-payment-list>
 * ```
 */
export class IrEarningsPaymentList extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
  };

  /** Uses the AccountingReportCrudController */
  public controller = IrEarningsPaymentCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/report/IrEarningsPaymentList.html";

}
