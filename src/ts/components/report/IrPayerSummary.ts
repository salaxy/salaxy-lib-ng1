import { ApiCrudObjectControllerBindings, IrPayerSummaryCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a user interface for Payer Summary Report ("Erillisilmoitus").
 * Currently, the UI is read-only, but it may later be extended to be editable
 * for purposes of corrections.
 *
 * @example
 * ```html
 * <salaxy-ir-payer-summary model="$ctrl.current"></salaxy-ir-payer-summary>
 * ```
 */
export class IrPayerSummary extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {
  }());

  /** Uses the IrPayerSummaryCrudController */
  public controller = IrPayerSummaryCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/report/IrPayerSummary.html";

}
