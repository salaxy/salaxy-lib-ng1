import { IrPayerSummaryCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of payer summaries.
 *
 * @example
 * ```html
 * <salaxy-ir-payer-summary-list></salaxy-ir-payer-summary-list>
 * ```
 */
export class IrPayerSummaryList extends ComponentBase {
  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
  };

  /** Uses the AccountingReportCrudController */
  public controller = IrPayerSummaryCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/report/IrPayerSummaryList.html";

}
