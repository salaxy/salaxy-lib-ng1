import { PayCertificateController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a wizard for creating a pay certificate for a worker.
 *
 * @example
 * ```html
 * <salaxy-pay-certificate></salaxy-pay-certificate>
 * ```
 */
export class PayCertificate extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = { };

    /** Uses the PayCertificateController */
    public controller = PayCertificateController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/PayCertificate.html";

}
