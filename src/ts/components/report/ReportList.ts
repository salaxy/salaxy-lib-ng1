import { ReportsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of reports
 *
 * @example
 * ```html
 * <salaxy-report-list report-type="unemployment"></salaxy-report-list>
 * ```
 */
export class ReportList extends ComponentBase {
   /**
    * The following component properties (attributes in HTML) are bound to the Controller.
    * For detailed functionality, refer to [controller](#controller) implementation.
    */
    public bindings = {
        /** - report-type: Type of the report (string, see reportType enumeration) that is shown in the list.
         * Also supports value null/"current" for showing reports defined by reportsService.currentReportType.
         */
        reportType: "<",

        /** -title
         * Heading of the list. Supports translation keys.
         * Default is reportType based enumeration translation.
         */
        heading: "@",
    };

    /** Uses the ReportsController */
    public controller = ReportsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/report/ReportList.html";

}
