import { AccountantController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Payment Settings
 *
 * @example
 * ```html
 * <salaxy-accountant></salaxy-accountant>
 * ```
 */

export class Accountant extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {};

  /** Uses the AccountController */
  public controller = AccountantController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/Accountant.html";

}
