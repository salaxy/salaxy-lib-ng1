import { InsuranceWizardController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shop in shop modal for getting Lähitapiola insurance.
 *
 * @example
 * ```html
 * <salaxy-insurance></salaxy-insurance>
 * ```
 */
export class Insurance extends ComponentBase {

/**
 * The following component properties (attributes in HTML) are bound to the Controller.
 * For detailed functionality, refer to [controller](#controller) implementation.
 */
    public bindings = {};

    /** Uses the PensionWizardController */
    public controller = InsuranceWizardController;

    /** Uses the controller as aliasing */
    public controllerAs = "$ctrl";

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/modals/account/Insurance.html";

}