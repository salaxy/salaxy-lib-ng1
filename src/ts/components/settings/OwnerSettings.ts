import { OwnerSettingsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * User interfaces for Owner and Beneficiary lists.
 *
 * @example
 * ```html
 * <salaxy-owner-settings></salaxy-owner-settings>
 * ```
 */

export class OwnerSettings extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {};

  /** Uses the OwnerSettingsController */
  public controller = OwnerSettingsController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/OwnerSettings.html";

}
