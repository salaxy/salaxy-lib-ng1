import { PaymentChannelSettingsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows and modifies Payment Settings related to Payment Channels
 *
 * @example
 * ```html
 * <salaxy-payment-channel-settings></salaxy-payment-settings>
 * ```
 */

export class PaymentChannelSettings extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {};

  /** Uses the AccountController */
  public controller = PaymentChannelSettingsController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/PaymentChannelSettings.html";

}
