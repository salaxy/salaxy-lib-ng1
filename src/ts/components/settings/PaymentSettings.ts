import { CompanySettingsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Overall settings needed for starting salary payment.
 *
 * @example
 * ```html
 * <salaxy-payment-settings></salaxy-payment-settings>
 * ```
 */

export class PaymentSettings extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
    /** Array of section ids to show
     * By default: ['pensionContracts','insuranceContracts','incomesRegister']
     * All options:
     * 'pensionContracts': Pension related settings
     * 'insuranceContracts' : Insureance contract related settings
     * 'incomesRegister': Tax reporting related settings
     * 'eInvoice': EInvoice related settings
     * 'sepa' : Sepa payments related settings
     * 'taxAndSocialSecuritySelfHandling': Settings for tax payments
     * 'pensionSelfHandling': Settings for pension payments
     * 'workerSelfHandling': Settings for net salary payments
     * 'unemploymentSelfHandling' Settings for unemployment payments
     */
    sections: "<",
  };

  /** Uses the CompanySettingsController */
  public controller = CompanySettingsController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/PaymentSettings.html";

}
