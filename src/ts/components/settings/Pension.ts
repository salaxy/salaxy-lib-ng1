import { VarmaPensionWizardController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shop in shop modal for getting Tyel or Yel.
 *
 * @example
 * ```html
 * <salaxy-pension></salaxy-pension>
 * ```
 */
export class Pension extends ComponentBase {

/**
 * The following component properties (attributes in HTML) are bound to the Controller.
 * For detailed functionality, refer to [controller](#controller) implementation.
 */
    public bindings = {};

    /** Uses the PensionWizardController */
    public controller = VarmaPensionWizardController;

    /** Uses the controller as aliasing */
    public controllerAs = "$ctrl";

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/modals/account/Pension.html";

}
