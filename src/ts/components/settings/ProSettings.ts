import { CompanySettingsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * All settings
 *
 * @example
 * ```html
 * <salaxy-pro-settings></salaxy-pro-settings>
 * ```
 */

export class ProSettings extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {};

  /** Uses the CompanySettingsController */
  public controller = CompanySettingsController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/ProSettings.html";

}
