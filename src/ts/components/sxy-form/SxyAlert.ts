import { SxyFormElementController } from "../../controllers";

import { ComponentBase } from "../_ComponentBase";

/**
 * Shows an alert with type "error" or one of the bootstrap styles: "danger", "info", "warning", "success".
 *
 * @example
 * ```html
 * <sxy-alert type="primary"><strong>NOTE:</strong> Here is a primary message to user.</sxy-alert>
 * <sxy-alert type="error" icon="fa-sign-in" msg="SALAXY.NG1.WelcomeComponent.showAnon.main.html"></sxy-alert>
 * ```
 */
export class SxyAlert extends ComponentBase {

  /** Inner content is the message. */
  public transclude = true;

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

    /**
     * Possibility to speicfy a font-awesome icon.
     * Setting "none", will show no icon.
     * If not set, it is determined by type.
     */
    icon: "@",

    /** Type of the alert is the Bootstrap style: Note that also "primary" and "default" are supported. */
    type: "@",

    /**
     * Translation key to HTML that should be the main message of the alert.
     * You can alternatively provide html as main element (transclude).
     */
    msg: "@",

    /**
     * Alert main content as simple text.
     * You can alternatively provide html as main element.
     */
    text: "@",

    /**
     * Optional Details part of the alert.
     * Will automatically show "Read more" button.
     */
    aside: "@",
  };

  /** Uses the SxyFormElementController */
  public controller = SxyFormElementController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/sxy-form/SxyAlert.html";
}
