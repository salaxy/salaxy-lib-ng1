import { Objects } from "@salaxy/core";

import { SxyInputController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a fieldset based on a given object in form
 *
 * @example
 * ```html
 * <sxy-fieldset model="form.identity"></sxy-fieldset>
 * ```
 */
export class SxyFieldset extends ComponentBase {

    /** sxy-form is required */
    public require = {
        form: "^^sxyForm",
    };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(SxyInputController.bindings , {});

    /** Uses the SxyInputController */
    public controller = SxyInputController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/sxy-form/SxyFieldset.html";
}
