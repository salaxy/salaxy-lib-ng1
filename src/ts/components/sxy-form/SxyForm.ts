import { SxyFormController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * The sxy-form component encapsulates the data binding and auto-generation logic
 * for editable models (TypeScript interfaces).
 *
 * @example
 * ```html
 * <sxy-form type="Calculation" model="{}"></sxy-form>
 * ```
 */
export class SxyForm extends ComponentBase {

  /**
   * Inner DIV element may be transclude.
   * Future implementations will probably contain header, footer etc.
   */
  public transclude = {
    div: "?div",
  }

  /** Component bindings. */
  public bindings = {

    /** Data object that is being edited by the fieldset. */
    model: "<",

    /** The data type that describes the object and creates the user interface. */
    type: "@",

    /**
     * URL for the Open API document that contains the definition for the type.
     * The url can be an absolute url, relative url or an url with salaxy-secure/ -prefix.
     * The salaxy-secure/ -prefix will be replaced in runtime with the salaxy api server url of the curren environment.
     */
    openApi: "@",

    /**
     * The translation code for the label/legend/title.
     * Default is "SALAXY.MODEL.[type].title"] or if not available, just the type text.
     */
    label: "@",

    /**
     * If true, the form is auto-generated from the type and inner HTML is ignored.
     * If false (default), the inner HTML is used to show the form.
     */
    autoGen: "<",

    /** If true, shows the form debugger */
    debugger: "<",

    /** Sets the entire form as read-only. Individual field may still be set read-only=false to make it editable. */
    readOnly: "@",

  };

  /** Uses the SxyFormController */
  public controller = SxyFormController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/sxy-form/SxyForm.html";
}
