import { SxyInputController } from "../../controllers";

import { Objects } from "@salaxy/core";

import { ComponentBase } from "../_ComponentBase";

/**
 * Shows an input control with label, validation error, info etc.
 *
 * @example
 * ```html
 * <sxy-ipt-text data-sxy="form.owner" model="Calculation.owner"></sxy-ipt-text>
 * ```
 */
export class SxyInput extends ComponentBase {

    /** sxy-form is required */
    public require = {
        form: "?^^sxyForm",
    };

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = Objects.extend(SxyInputController.bindings , {});

    /**
     * Transclude optional templates for input parts.
     * Currently supports form, but may later support header, footer etc.
     */
    public transclude = {

      /** Defines the inner form of Array dialogs and later potentially other dialogs and sub-forms */
      form: "?form"
    };

    /** Uses the SxyIptController */
    public controller = SxyInputController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/sxy-form/SxyInput.html";
}
