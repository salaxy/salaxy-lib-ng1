import { AccountingTargetCrudController, ApiCrudObjectControllerBindings } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Provides UI for viewing and adding new (modifying) accounting targets.
 * Accounting targets define where the accounting data is sent and how it is mapped to a Chart of Accounts (CoA)
 *
 * @example
 * ```html
 * <salaxy-accounting-target-editor></salaxy-accounting-target-editor>
 * ```
 */

export class AccountingTargetEditor extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = new ApiCrudObjectControllerBindings();

  /** Uses the AccountController */
  public controller = AccountingTargetCrudController;

  /** Default template is the view  */
  public defaultTemplate = "salaxy-components/settings/AccountingTargetEditor.html";

}
