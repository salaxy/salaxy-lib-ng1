import { ImportController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * UNDERCON: This is a PROTOTYPE implementation, not ready for production use.
 *
 * Generic import data component.
 * At the moment, it is unclear whether we will have one generic component or case specific import components for Workers, Calculations, Payroll etc.
 *
 * @example
 * ```html
 * <salaxy-import></salaxy-import>
 * ```
 */
export class Import extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
    };

    /** Uses the ImportController */
    public controller = ImportController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/helpers/Import.html";
}
