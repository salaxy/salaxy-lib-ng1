import { SessionController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a language selection control.
 *
 * @example
 * ```html
 * <salaxy-language-selector></salaxy-language-selector>
 * ```
 */
export class LanguageSelector extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */

    public bindings = {};

    /** Uses the SessionController */
    public controller = SessionController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/account/LanguageSelector.html";

}
