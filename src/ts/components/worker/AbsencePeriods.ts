import { AbsencePeriodsController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * UI for the absences list (poissaolokirjanpito).
 *
 * @example
 * ```html
 * <salaxy-absence-periods></salaxy-absence-periods>
 * ```
 */
export class AbsencePeriods extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = AbsencePeriodsController.bindings;

    /** Uses the AbsencePeriodsController */
    public controller = AbsencePeriodsController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/AbsencePeriods.html";

}
