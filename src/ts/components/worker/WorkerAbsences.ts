import { ApiCrudObjectControllerBindings, WorkerAbsencesCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * UI for absences of a single worker / employment relation.
 *
 * @example
 * ```html
 * <salaxy-worker-absences employment-id="$ctrl.current.employmentId"></salaxy-worker-absences>
 * ```
 */
export class WorkerAbsences extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = (new class extends ApiCrudObjectControllerBindings {

      /**
       * Alternative model binding.
       * Instead of model="absenceObjectId", you may specify employment-id="employmentId"
       */
      public employmentId = "<";

    }());

    /** Uses the WorkerAbsencesCrudController */
    public controller = WorkerAbsencesCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/WorkerAbsences.html";

}
