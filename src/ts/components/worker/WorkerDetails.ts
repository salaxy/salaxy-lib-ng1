import { ApiCrudObjectControllerBindings, WorkerAccountCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a view of worker's contact and payment information, for example phone number and bank account number.
 *
 * @example
 * ```html
 * <salaxy-worker-details></salaxy-worker-details>
 * ```
 */
export class WorkerDetails extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = (new class extends ApiCrudObjectControllerBindings {
      /** Current tab in initialization. If not set, will be fetched from url hash. */
      public currentTab = "@";

      /** If true, the save/delete etc. buttons are not shown (they will come from the container / modal) */
      public hideButtons = "<";
    }());

    /** Uses the WorkerAccountCrudController */
    public controller = WorkerAccountCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/WorkerDetails.html";

}
