import { ApiCrudObjectControllerBindings, WorkerAccountCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a view of worker's contact and payment information, for example phone number and bank account number.
 *
 * @example
 * ```html
 * <salaxy-worker-details-edit></salaxy-worker-details-edit>
 * ```
 */
export class WorkerDetailsEdit extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = (new class extends ApiCrudObjectControllerBindings {

      /** If true, the save/delete etc. buttons are not shown (they will come from the container / modal) */
      public hideButtons = "<";
    }());

    /** Uses the WorkerAccountCrudController */
    public controller = WorkerAccountCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/WorkerDetailsEdit.html";

}
