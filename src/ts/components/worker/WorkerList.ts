import { ODataQueryController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Shows a list of workers for a particular employer
 *
 * @example
 * ```html
 * <salaxy-worker-list limit-to="5" mode="select"></salaxy-worker-list>
 * ```
 */
export class WorkerList extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

    /**
     * Function that is called when user selects an item in the list.
     * Note that the event is called only in the selections (single and multi). Not when there is a link to details view in a link list.
     * Function has the following locals: value: true/false, item: the last selected/unselected item, allItems: Array of all currently selected items.
     * @example <salaxy-worker-list mode="select" on-list-select="$ctrl.updateFromEmployment(item.id)"></salaxy-worker-list>
     */
    onListSelect: "&",

    /** Selected items for the case where the list is used as a select list (as opposed to link list). */
    selectedItems: "<",

    /** Type of the view. Currently supports "default" and "select" */
    mode: "@",

    /** Max count of workers to show in the list. */
    limitTo: "<",

    /** If readOnly is true, edit, copy etc. buttons from the new list view are hidden if readOnly property is supported */
    readOnly: "<",

  };

    /** Uses the ODataQueryController */
    public controller = ODataQueryController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/WorkerList.html";

}
