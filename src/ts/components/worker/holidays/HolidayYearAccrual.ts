import { HolidayYearAccrualController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * UI for holiday accrual (lomapäivien kertymä) of the annual leave for a selected holiday period.
 *
 * @example
 * ```html
 * <salaxy-holiday-year-accrual parent="$ctrl.current"></salaxy-holiday-year-accrual>
 * ```
 */
export class HolidayYearAccrual extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = HolidayYearAccrualController.bindings;

    /** Uses the HolidayYearAccrualController */
    public controller = HolidayYearAccrualController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/holidays/HolidayYearAccrual.html";

}
