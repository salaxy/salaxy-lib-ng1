import { HolidayYearHolidaysController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * UI for the planned holidays list (lomakirjanpito / lomakalenteri) for a selected holiday year.
 *
 * @example
 * ```html
 * <salaxy-holiday-year-holidays></salaxy-holiday-year-holidays>
 * ```
 */
export class HolidayYearHolidays extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = HolidayYearHolidaysController.bindings;

    /** Uses the HolidayYearHolidaysController */
    public controller = HolidayYearHolidaysController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/holidays/HolidayYearHolidays.html";

}
