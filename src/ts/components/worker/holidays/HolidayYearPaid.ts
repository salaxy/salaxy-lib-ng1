import { HolidayYearPaidController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * User interface for the annual leave payments: HolidayCompensation, HolidayBonus and HolidaySalary.
 * These payments are typically fetched from paid calculations automatically,
 * but may also be marked paid manually. Also, in client-side logic, payments are fetched
 * optionally from Draft calculations.
 *
 * @example
 * ```html
 * <salaxy-holiday-year-paid parent="$ctrl.current"></salaxy-holiday-year-paid>
 * ```
 */
export class HolidayYearPaid extends ComponentBase {
    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = HolidayYearPaidController.bindings;

    /** Uses the HolidayYearPaidController */
    public controller = HolidayYearPaidController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/holidays/HolidayYearPaid.html";

}
