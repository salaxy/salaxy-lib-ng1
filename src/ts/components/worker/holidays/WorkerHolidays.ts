import { ApiCrudObjectControllerBindings, HolidayYearCrudController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Viewing and modifying of Worker holidays (holiday years and related settings).
 *
 * @example
 * ```html
 * <salaxy-worker-holidays></salaxy-worker-holidays>
 * ```
 */
export class WorkerHolidays extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = (new class extends ApiCrudObjectControllerBindings {

      /**
       * Alternative model binding.
       * Instead of model="holidayYearId", you may specify employment-id="employmentId".
       * This loads all the holiday years to HolidayYearCrudController.employmentHolidayYears
       */
      public employmentId = "<";

      /**
       * Setting this value (iso date) will specify the the holiday year:
       * If the date is Jan-April will show the previous year, May-Dec will show the current year.
       * Default is today.
       */
      public forDate = "<";

    }());

    /** Uses the HolidayYearCrudController */
    public controller = HolidayYearCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/worker/holidays/WorkerHolidays.html";

}
