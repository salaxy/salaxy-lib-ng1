import { EmploymentTaxcardsController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Helps listing current all tax cards for an employment relation (latest and previous ones).
 *
 * @example
 * ```html
 * <salaxy-employment-taxcards employment-id="$ctrl.current.employmentId"></salaxy-employment-taxcards>
 * ```
 */
export class EmploymentTaxcards extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {
    /** Employment ID of the Worker whose taxcards are shown in the component. */
    employmentId: "<",

    /**
     * View mode:
     *
     * - "active" only shows the active taxcard.
     * - "list" only shows the history list.
     * - "all" (default) shows both active and history.
     */
    mode: "@",
  };

  /** Uses the EmploymentTaxcardsController */
  public controller = EmploymentTaxcardsController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/worker/taxcard/EmploymentTaxcards.html";
}
