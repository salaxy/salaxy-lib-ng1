import { ODataQueryController } from "../../../controllers";
import { ComponentBase } from "../../_ComponentBase";

/**
 * Shows a list of workers and their taxcards
 *
 * @example
 * ```html
 * <salaxy-tax-cards-list></salaxy-tax-cards-list>
 * ```
 */
export class TaxCardsList extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = {

  };

  /** Uses the ODataQueryController */
  public controller = ODataQueryController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/worker/taxcard/TaxCardsList.html";

}
