import { ApiCrudObjectControllerBindings, TaxcardCrudController } from "../../../controllers";

import { ComponentBase } from "../../_ComponentBase";

/**
 * Shows the editor for a taxcard.
 *
 * @example
 * ```html
 * <salaxy-taxcard-details model="$ctrl" mode="pro"></salaxy-taxcard-details>
 * ```
 */

export class TaxcardDetails extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {
    /** Mode is either "pro" (for admin / PRO-ui) or "default" */
    public mode = "@";
    /**
     * Either a valid Finnish Personal ID or "self" for the current Worker account.
     * Required for creating a new taxcard: The personal ID is used for setting the connection to Worker account or self.
     */
    public personalId = "<";

    /**
     * Event that is called when a taxcard is saved and the parent UI probably needs to be refreshed.
     * @example <salaxy-taxcard-details model="$ctrl.current.active" personal-id="$ctrl.current.personalId" on-save="$ctrl.reload()"></salaxy-taxcard-details>
     */
    public onSave = "&";

    /** If true, does not show the save / current buttons - the parent must show them. */
    public hideButtons = "<";
  }());

  /** Uses the TaxcardCrudController */
  public controller = TaxcardCrudController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/worker/taxcard/TaxcardDetails.html";

}
