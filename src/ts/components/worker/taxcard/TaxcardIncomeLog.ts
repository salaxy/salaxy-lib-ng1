import { ListControllerBaseBindings, TaxcardIncomeLogController } from "../../../controllers";

import { ComponentBase } from "../../_ComponentBase";

/**
 * Shows view / editor table for the income log of taxcard.
 *
 * @example
 * ```html
 * <salaxy-taxcard-income-log parent="$ctrl.current"></salaxy-taxcard-income-log>
 * ```
 */

export class TaxcardIncomeLog extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ListControllerBaseBindings {
    /** If true, the list can be edited, by default it is read-only. */
    public editable = "<";
  }());

  /** Uses the TaxcardIncomeLogController */
  public controller = TaxcardIncomeLogController;

  /** The default template for the component. */
  public defaultTemplate = "salaxy-components/worker/taxcard/TaxcardIncomeLog.html";

}
