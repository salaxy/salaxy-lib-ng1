import { ApiCrudObjectControllerBindings, CalendarEventsCrudController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders an editor / details component for a Calendar Event.
 *
 * @example
 * ```html
 * <salaxy-calendar-event-details model="'url"></salaxy-calendar-event-details>
 * ```
 */
export class CalendarEventDetails extends ComponentBase {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public bindings = (new class extends ApiCrudObjectControllerBindings {}());

    /** Uses the CalendarEventsCrudController */
    public controller = CalendarEventsCrudController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/workflow/CalendarEventDetails.html";
}
