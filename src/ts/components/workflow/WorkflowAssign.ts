import { WorkflowController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a workflow assignment control.
 *
 * @example
 * ```html
 * <salaxy-workflow-assign api-ctrl="$ctrl" disabled="!ok"></salaxy-workflow-assign>
 * ```
 */
export class WorkflowAssign extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Expression for the api controller . */
      apiCtrl: "<",
      /** Optional binding for enabling/disabling the control */
      disabled: "<",
      /** Called when workflow action has been taken */
      onChange: "&",
    };

    /** Uses the WorkflowController */
    public controller = WorkflowController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/workflow/WorkflowAssign.html";
}
