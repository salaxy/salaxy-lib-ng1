import { WorkflowController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a workflow close toggle.
 *
 * @example
 * ```html
 * <salaxy-workflow-close api-ctrl="$ctrl" message="'Checked!'" disabled="!ok" ></salaxy-workflow-close>
 * ```
 */
export class WorkflowClose extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Expression for the api controller . */
      apiCtrl: "<",
      /** Optional expression for the message to include in the event. */
      message: "<",
      /** Optional expression for the hint to show on hoover. */
      hint: "<",
      /** Optional binding for enabling/disabling the control */
      disabled: "<",
      /** Called when workflow action has been taken */
      onChange: "&",
    };

    /** Uses the WorkflowController */
    public controller = WorkflowController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/workflow/WorkflowClose.html";
}
