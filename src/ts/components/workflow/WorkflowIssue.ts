import { WorkflowController } from "../../controllers";
import { ComponentBase } from "../_ComponentBase";

/**
 * Renders a workflow issue control.
 *
 * @example
 * ```html
 * <salaxy-workflow-issue api-ctrl="$ctrl" disabled="!ok"></salaxy-workflow-issue>
 * ```
 */
export class WorkflowIssue extends ComponentBase {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to [controller](#controller) implementation.
     */
    public bindings = {
      /** Expression for the api controller . */
      apiCtrl: "<",
      /** Optional binding for enabling/disabling the control */
      disabled: "<",
      /** Called when workflow action has been taken */
      onChange: "&",
    };

    /** Uses the WorkflowController */
    public controller = WorkflowController;

    /** The default template for the component. */
    public defaultTemplate = "salaxy-components/workflow/WorkflowIssue.html";
}
