﻿import { Ajax, Avatar, CompanyAccount, PersonAccount } from "@salaxy/core";

import { AuthorizedAccountService, SessionService, UiHelpers } from "../../services";
import { CrudControllerBase } from "../bases/CrudControllerBase";

/**
 * Handles user interaction for viewing and modifying authorized and authorizing accounts.
 */
export class AccountAuthorizationController extends CrudControllerBase<Avatar> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AuthorizedAccountService",
    "$location",
    "$attrs",
    "UiHelpers",
    "SessionService",
    "AjaxNg1",
  ];

  constructor(
    private authorizedAccountService: AuthorizedAccountService,
    $location: angular.ILocationService,
    $attrs: angular.IAttributes,
    uiHelpers: UiHelpers,
    private sessionService: SessionService,
    private ajax: Ajax,
  ) {
    super(authorizedAccountService, $location, $attrs, uiHelpers);
  }

  /**
   * Returns the list of Integration partner sites available and the status of each partner.
   */
  public get partnerIntegrationSites() {
    return this.authorizedAccountService.getPartnerIntegrationSiteList();
  }

  /**
   * Gets a specific partner site for the current user
   * @param id Identifier of the site
   */
  public getPartnerSite(id: string) {
    return this.authorizedAccountService.getPartnerSite(id);
  }

  /**
   * Returns the current partner site
   */
  public get currentPartnerSite() {
    return this.authorizedAccountService.getCurrentPartnerSite();
  }

  /**
   * Shows a dialog for adding / removing authorizations from known partners that
   * frequently require authorizations. These are typically integrated software
   * that are added without Primary Partner flag.
   * @param id - The id of the partner site for which the dialog is shown.
   */
  public showPartnerSiteDialog(id: string) {
    const partnerSite = this.getPartnerSite(id);
    this.authorizedAccountService.setCurrentPartnerSite(partnerSite);
    this.uiHelpers.showDialog("salaxy-components/modals/account/Authorization.html", "AccountAuthorizationController");
  }

  /**
   * Returns the authorizing accounts for the current account
   */
  public get authorizingAccounts(): (PersonAccount | CompanyAccount)[] {
    return this.authorizedAccountService.getAuthorizingAccounts();
  }

  /** Deletes the account. Only in test. */
  public deleteAccount(accountId: string) {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin, tiliä ei voi palauttaa?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.ajax.remove("/accounts/authorizingAccount/" + accountId).then(() => {
            this.authorizedAccountService.reloadAuthorizingAccounts().then(() => {
              loading.dismiss();
            });
          });
        }
      });
  }

  /**
   * Shows a dialog for adding any new authorized account.
   */
  public showAuthorizedAccountAddDialog() {
    super.createNew();
    this.uiHelpers.showDialog("salaxy-components/modals/account/AuthorizedAccountAdd.html", "AccountAuthorizationController");
  }

  /** Adds authorized account */
  public saveCurrent(callback: (avatar: Avatar) => void = null): Promise<Avatar> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return super.saveCurrent().then((avatar: Avatar) => {
      loading.dismiss();
      if (callback) {
        callback(avatar);
      }
      return avatar;
    });
  }

  /**
   * Adds access token to url.
   * @param accountId - Account id to login as.
   */
  public getLoginAsUrl(accountId): string {

    let url = this.ajax.getServerAddress() + "/Security/LoginAs/" + accountId;
    url = url + "?access_token=" + encodeURIComponent(this.ajax.getCurrentToken());
    return url;
  }

  /**
   * Checks whether the user is in a given role
   * @param role - One of the known roles
   */
  public isInRole(role: any): boolean {
    return this.sessionService.isInRole(role);
  }

}
