import * as angular from "angular";

import { Ajax, Test } from "@salaxy/core";

import { UiHelpers } from "../../services";

/**
 * Controller for resetting account data in the test environment.
 */
export class AccountResetController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Test",
    "UiHelpers",
    "AjaxNg1",
  ];

  /**
   * Creates a new AccountResetController.
   * @param testApi - Api methods for resetting account data.
   * @param uiHelpers - Salaxy UI Helpers.
   * @param ajax - Salaxy ajax component.
   */
  constructor(
    private testApi: Test,
    private uiHelpers: UiHelpers,

    private ajax: Ajax,
  ) { }

  /**
   * Delete the current account data and credentials (including Auth0 user).
   * Can be called only in a test environment.
   */
  public deleteCurrent(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccount().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove all calculations, workers etc. user objects except products and signature from the account.
   * Can be called only in a test environment.
   */
  public deleteData(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tiliin liittyvän datan?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountData().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove all calculations, payrolls and payments from the account.
   * Can be called only in a test environment.
   */
  public deleteCalculations(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin laskelmat?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountCalculations().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove workers including calculations, employment contracts and tax cards from the account.
   * Can be called only in a test environment.
   */
  public deleteWorkers(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin työntekijät?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountWorkers().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove all holiday year from all workers. Does not touch the default values of holidays in Worker Employment relation.
   * Can be called only in a test environment.
   */
  public deleteHolidays(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa loma- ja poissaolotiedot?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountHolidays().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Delete all empty accounts (company or worker) created by this account.
   * Can be called only in a test environment.
   */
  public deleteAuthorizingAccounts(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin luomat muut tilit?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountAuthorizingAccounts().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove pension and insurance from the account.
   * Can be called only in a test environment.
   */
  public deletePensionAndInsurance(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tiliin liittyvän TyEL:n ja vakuutukset?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountPensionAndInsurance().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  /**
   * Remove the signature from the account.
   * Can be called only in a test environment.
   */
  public deleteSignature(): void {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa tilin valtuutuksen (allekirjoitus)?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.testApi.deleteCurrentAccountSignature().then(() => {
            loading.dismiss();
            this.logout();
          });
        }
      });
  }

  private logout() {
    this.ajax.setCurrentToken(null);
    window.location.assign("/");
  }
}
