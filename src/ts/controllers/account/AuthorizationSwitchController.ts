﻿import * as angular from "angular";

import { AuthorizedAccountService } from "../../services";

/**
 * Controller for Salaxy authorization switch (a graphic checkbox for giving authorization to your Salaxy account data for a certain partner site)
 */
export class AuthorizationSwitchController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AuthorizedAccountService"];

  /** Switch name */
  public name: string;

  /** Id for the switch */
  public id: string;

  /** The model that is bound to the switch */
  public model: any;

  /** The value of the on text */
  public onText: string;

  /** The value of the off text */
  public offText: string;

  /** If true, the switch will be disabled and its value cannot be changed by clicking/toggling it */
  public disabled: boolean;

  /** partnerSite object that this switch is tied to */
  private partnerSite: any;

  /**
   * Creates a new AuthorizationSwitchController
   * @ignore
   */
  constructor(
    private authorizedAccountService: AuthorizedAccountService,
  ) {

  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    // initialization
    if (this.name && !this.id) {
      this.id = this.name;
    }
    this.partnerSite = this.authorizedAccountService.getPartnerSite(this.id);
    if (this.partnerSite) {
      this.disabled = !!this.partnerSite.disabled;
      this.model = this.partnerSite.enabled;
    }
  }

  /** Toggles the authorization option and makes the corresponding api call. */
  public switchAuthorization() {
    if (this.disabled !== true) {
      if (this.partnerSite.enabled && this.partnerSite.accountId) {
        this.model = false;
        this.authorizedAccountService.delete(this.partnerSite.accountId);
      } else if (!this.partnerSite.enabled && this.partnerSite.accountId) {
        this.model = true;
        const authorizedAccount = this.authorizedAccountService.getBlank();
        authorizedAccount.id = this.partnerSite.accountId;
        this.authorizedAccountService.save(authorizedAccount);
      }
    }
  }
}
