import { Certificate } from "@salaxy/core";

import { CertificateService, UiHelpers } from "../../services";
import { CrudControllerBase } from "../bases/CrudControllerBase";

/** Certificate controller for listing certificate, revoking and requesting a new certificate. */
export class CertificateController extends CrudControllerBase<Certificate> {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["CertificateService", "$location", "$attrs", "UiHelpers"];

    constructor(
        certificateService: CertificateService,
        $location: angular.ILocationService,
        $attrs: angular.IAttributes,
        uiHelpers: UiHelpers,
    ) {
        super(certificateService, $location, $attrs, uiHelpers);
    }

    /** Creates a new certificate */
    public createNew(): Certificate {
        super.createNew();
        this.current.title = "Uusi varmenne";
        return this.current;
    }

    /**
     * Shows a dialog for adding nw certificate.
     */
    public showCertificateAddDialog() {
        this.createNew();
        this.uiHelpers.showDialog("salaxy-components/modals/account/CertificateAdd.html", "CertificateController");
    }

    /** Creates a new certificate */
    public saveCurrent(): Promise<Certificate> {
        const loading = this.uiHelpers.showLoading("Odota...");
        return super.saveCurrent().then( (certificate: Certificate) => {
            loading.dismiss();
            return certificate;
        });
    }
}
