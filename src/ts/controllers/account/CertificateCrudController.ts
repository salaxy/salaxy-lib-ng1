﻿import * as angular from "angular";

import { Certificate, Certificates } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * Plain CRUD controller for Certificates.
 */
export class CertificateCrudController extends ApiCrudObjectController<Certificate> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Certificates",
    "UiHelpers",
    "$location",
    "$routeParams",
  ];

  constructor(
    private fullApi: Certificates,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,

  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/certificates",
      detailsUrl: this.detailsUrl || "/certificates/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/Certificates.html",
      oDataOptions: { },
    };
  }
}
