import * as angular from "angular";

import { Accounts, Ajax, CompanyAccount } from "@salaxy/core";

import { SessionService, UiHelpers } from "../../services";

/** Company account controller for editing basic information. */
export class CompanyAccountController implements angular.IComponentController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Accounts", "SessionService", "AjaxNg1", "UiHelpers", "$attrs"];

  /** Current company account */
  public current: CompanyAccount = null;

  /** Constructor for dependency injection */
  constructor(
    private accounts: Accounts,
    private sessionService: SessionService,
    private ajax: Ajax,
    private uiHelpers: UiHelpers,
    private $attrs: angular.IAttributes,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    if (!this.$attrs.current) {
      this.accounts.getCompany().then((company) => {
        this.current = company;
      });
    }
  }

  /** Updates the company */
  public saveCurrent(): Promise<CompanyAccount> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return this.ajax.postJSON("/accounts/company",this.current).then((company: CompanyAccount) => {
      this.current = company;
      loading.dismiss();
      return company;
    });
  }
}
