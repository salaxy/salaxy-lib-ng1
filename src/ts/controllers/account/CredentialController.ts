import * as angular from "angular";

import { Avatar, AvatarPictureType, SessionUserCredential } from "@salaxy/core";

import { CredentialService, GravatarHelpers, SessionService, UiHelpers, UploadService } from "../../services";
import { CrudControllerBase } from "../bases/CrudControllerBase";

/** Credential controller for listing credentials and removing existing credentials. */
export class CredentialController extends CrudControllerBase<SessionUserCredential> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UploadService", "CredentialService", "SessionService", "$location", "$attrs", "UiHelpers"];

  private originalAvatar: Avatar = null;

  /** Constructor for dependency injection */
  constructor(
    private uploadService: UploadService,
    private credentialService: CredentialService,
    private sessionService: SessionService,
    $location: angular.ILocationService,
    $attrs: angular.IAttributes,
    uiHelpers: UiHelpers,
  ) {
    super(credentialService, $location, $attrs, uiHelpers);
  }

  /**
   * Uploads avatar image file to the server.
   * @param file - selected file
   */
  public uploadAvatarImage(file: any) {
    if (!file) {
      return;
    }
    this.uploadService.upload<SessionUserCredential>(this.credentialService.getAvatarUploadUrl(this.current.id), { file })
      .then((resp) => {
        this.current.avatar.url = resp.avatar.url;
        this.current.avatar.pictureType = resp.avatar.pictureType;
      });
  }

  /** Called when avatar changes */
  public emailForAvatarChanged() {
    if (!this.current || !this.current.avatar) {
      return;
    }
    if (this.current.avatar.pictureType === AvatarPictureType.Gravatar) {
      this.current.avatar.url = GravatarHelpers.getGravatarUrl(this.current.email);
    }
  }
  /** Called when avatar changes */
  public typeForAvatarChanged() {
    if (!this.current || !this.current.avatar) {
      return;
    }

    if (this.current.avatar.pictureType === AvatarPictureType.Gravatar) {
      this.current.avatar.url = GravatarHelpers.getGravatarUrl(this.current.email);
    } else {
      if (GravatarHelpers.isGravatarUrl(this.current.avatar.url)) {
        this.current.avatar.url = null;
      }
      if (this.current.avatar.pictureType === AvatarPictureType.Uploaded) {
        if (!this.current.avatar.url && this.originalAvatar && this.originalAvatar.url) {
          if (this.originalAvatar.pictureType === AvatarPictureType.Uploaded &&
             !GravatarHelpers.isGravatarUrl(this.originalAvatar.url)) {
            this.current.avatar.url = this.originalAvatar.url;
          }
        }
      }
    }
  }

  /** Set current credential for edit. */
  public setCurrent(item: SessionUserCredential) {
    if (item) {
      this.originalAvatar = angular.copy(item.avatar);
    }
    super.setCurrent(item);
  }

  /** Saves a new credential */
  public saveCurrent(): Promise<SessionUserCredential> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return super.saveCurrent().then((credential: SessionUserCredential) => {
      this.originalAvatar = angular.copy(credential.avatar);
      return this.sessionService.checkSession().then( () => {
        loading.dismiss();
        return credential;
      });
    });
  }
}
