import * as angular from "angular";

import { Configs } from "@salaxy/core";
import { PartnerService, SessionService } from "../../services";

/**
 * User interface logic for brand selection,
 */
export class PartnerController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["PartnerService", "SessionService"];

  /** If true, the all partners all shown */
  private showAll = false;

  /**
   * Creates a new PartnerController using dependency injection.
   * @ignore
   */
  constructor(
    private partnerService: PartnerService,
    private sessionService: SessionService,
    ) {}

  /** Controller initialization. */
  public $onInit = () => {
    // empty
  }

  /** Shows the skin that is currently in use: "default" for the real skin or overriden preview skin for preview purposes. */
  public get skin() {
    return this.partnerService.overrideSkin || "default";
  }

  /**
   * Shows the Brand ID that should be used for preview purposes: Either real service model or temporary override ID.
   * Note that the service model may also be null, if there is no partner
   */
  public get brandId() {
    return this.partnerService.overridePartnerId || this.sessionService.getSession()?.settings.partner?.serviceModelId;
  }

  /**
   * Shows a dialog for brand selection.
   */
  public showSwitchCss() {
    this.partnerService.showSwitchCss();
  }

  /**
   * Gets the URL for the login screen. Mainly for testing / preview purposes.
   */
  public getLoginUrl() {
    const server = Configs.current.wwwServer || "https://test-www.palkkaus.fi";
    return server +  "/Security/Authorize?client_id=" + (this.brandId || "");
  }

  /**
   * Gets the URL for the onboarding screen. Mainly for testing / preview purposes.
   */
   public getOnboardingUrl() {
    const server = Configs.current.apiServer || "https://test-secure.palkkaus.fi";
    let url = `${server}/Onboarding/NgApp?id=test&role=company`;
    if (this.brandId) {
      url = url + "&partner=" + this.brandId;
    }
    return url;
  }
}
