﻿import * as angular from "angular";

import { Ajax, Configs, PaymentChannel, SystemRole } from "@salaxy/core";

import { Ng1Translations, SessionService, UiHelpers } from "../../services";

/**
 * User interaction with the current session: UserCredentials, Current Account(s) and Login/Logout.
 */
export class SessionController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "SessionService",
    "Ng1Translations",
    "$location",
    "AjaxNg1",
    "UiHelpers",
  ];

  /** Default redirect URL for signIn and register if not specified by the method */
  public redirectUrl: string;

  constructor(
    private sessionService: SessionService,
    private translate: Ng1Translations,
    private $location: angular.ILocationService,
    private ajax: Ajax,

    private uiHelpers: UiHelpers,
  ) {

  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    //
  }

  /** If true, the session has been checked from theserver - i.e. isAuthenticated = false means that the user really cannot be authenticated. */
  public get isSessionChecked() {
    return this.sessionService.getIsSessionChecked();
  }

  /** If true, the session check call is progressing. */
  public get isSessionChecking() {
    return this.sessionService.getIsSessionChecking();
  }

  /** If true, the user is authenticated */
  public get isAuthenticated() {
    return this.sessionService.getIsAuthenticated();
  }

  /** Avatar to show in the login screen */
  public get avatar(): any {
    return this.sessionService.getAvatar();
  }

  /** The full session objcet */
  public get session(): any {
    return this.sessionService.getSession();
  }

  /** The company account if the current account is company - and if the full account info is fetched */
  public get company(): any {
    return this.sessionService.getCompanyAccount();
  }

  /** The person account if the current account is person - and if the full account info is fetched */
  public get person(): any {
    return this.sessionService.getPersonAccount();
  }

  /**
   * Opens the login form for the current user
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login.
   * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   * @param partnerSite Identifier of the partner or service model from which the login UI is fetched.
   * @param urlPostfix Additional string that is added to the OAuth2 URL.
   * Used in adding other parameters to the URL, e.g. "&salaxy_language=en"
   */
  public signIn(redirectUrl: string = null, role: string = null, partnerSite: string = null, urlPostfix: string = null) {
    this.sessionService.signIn(redirectUrl || this.redirectUrl, role, partnerSite, urlPostfix);
  }

  /**
   * Opens the login dialog with signUp / register functionality
   *
   * @param  redirectUrl - The URL where the user is taken after a successfull login
   * @param  role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   * @param partnerSite Identifier of the certified partner which should be granted access rights in account creation.
   * Also sets register dialog and onboarding wizard UI skin.
   * @param urlPostfix Additional string that is added to the OAuth2 URL.
   * Used in adding other parameters to the URL, e.g. "&salaxy_language=en"
   */

  public register(redirectUrl: string = null, role: string = null, partnerSite: string = null, urlPostfix: string = null) {
    this.sessionService.register(redirectUrl || this.redirectUrl, role, partnerSite, urlPostfix);
  }

  /** If false, the current authenticated user has not signed the contract. */
  public get isAccountVerified(): boolean {
    return this.sessionService.checkAccountVerification();
  }

  /**
   * Sends the user to the Sign-out page
   * @param redirectUrl - URL where user is redirected after log out.
   * Must be absolute URL. Default is the root of the current server.
   */
  public signOut(redirectUrl?: string) {
    this.sessionService.signOut(redirectUrl);
  }

  /**
   * Switches the current web site usage role between household and worker.
   * @param role - household or worker.
   * @returns A Promise with result data (new role as string)
   */
  public switchRole(role: "worker" | "household"): Promise<"household" | "worker"> {
    return this.sessionService.switchRole(role).then((resultRole) => {
      if (role === "worker") {
        window.location.href = "/Worker#/";
      } else {
        window.location.href = "/Household#/";
      }
      return resultRole;
    });
  }

  /**
   * When called, will show the login screen if the user is not logged in
   */
  public checkAuthenticated() {
    if (this.isSessionChecked && !this.isAuthenticated) {
      this.signIn(window.location.href);
    }
  }

  /**
   * Checks whether the user is in a given role
   * @param  role - One of the known roles
   */
  public isInRole(role: SystemRole): boolean {
    return this.sessionService.isInRole(role);
  }

  /**
   * Set the language for UI in the current session.
   * @param lang - Language to select: fi, en, sv.
   */
  public setLanguage(lang: string) {
    this.translate.setLanguage(lang);
  }

  /**
   * Get the current language for UI in the current session.
   */
  public getLanguage() {
    this.translate.getLanguage();
  }

  /**
   * Switches the CSS link to partner-specific CSS.
   * @param cssUrl URL for the CSS file.
   */
  public switchCss(cssUrl: string) {
    this.sessionService.switchCss(cssUrl);
  }

  /** Sign in error */
  public get signInError(): string {
    return this.sessionService.signInError;
  }
  /** Sign in error description */
  public get signInErrorDescription(): string {
    return this.sessionService.signInErrorDescription;
  }
  /** Sign in error page url */
  public get signInErrorPageUrl(): string {
    return this.sessionService.signInErrorUrl;
  }

  /** Returns current token if exists. */
  public get currentToken(): string {
    return this.ajax.getCurrentToken();
  }

  /** Returns the url to the authorization pdf. */
  public get authorizationPdfUrl() {
      return `${Configs.current.apiServer}/Onboarding/GetAuthorizationPdf?access_token=${encodeURIComponent(this.ajax.getCurrentToken())}`;
  }

  /** Returns owned payment channel for current account */
  public get ownedPaymentChannel(): PaymentChannel {
    return this.sessionService.getOwnedPaymentChannel();
  }
}
