import * as angular from "angular";

import { Ajax, ApiValidationError, Onboarding, Role } from "@salaxy/core";

import { OnboardingService, SessionService, WizardService, WizardStep } from "../../../services";
import { WizardController } from "../../bases/WizardController";

/**
 * Wizard for creating a new Palkkaus.fi-account for a company.
 */
export class CompanyOnboardingController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WizardService", "OnboardingService", "SessionService", "AjaxNg1", "$location", "data"];

  /**
   * Field to expose forms validity state.
   */
  public formDataValidity: boolean;

  /** If true, step is proceeding */
  public isStepProceeding = false;

  constructor(
    $scope: angular.IScope,
    wizardService: WizardService,
    private onboardingService: OnboardingService,
    private sessionService: SessionService,
    private ajax: Ajax,
    private $location: angular.ILocationService,
    data: any,
  ) {
    super($scope, wizardService, data);
  }

  /**
   * Implement IController
   */
  public $onInit() {

    this.wizardService.setSteps(this.getWizardSteps());
    if (this.model && this.model.signature.isProcura) {
      this.wizardService.activeStepNumber = 3;
    } else {
      this.wizardService.activeStepNumber = 1;
    }
  }

  /** Company wizard configuration */
  public getWizardSteps(): WizardStep[] {
    const steps: WizardStep[] = [];
    steps.push(
      {
        title: "Käyttäjän tiedot",
        intro: `Saat palvelun käyttöösi, kun luot Palkkaus.fi-tilin itsellesi tai yrityksellesi.
                  Palvelun käyttöönotto on ilmaista ja maksat palvelusta vain käytön mukaan ilman sitoumuksia.
                  Työntekijälle palvelu on aina ilmainen. Aloita täyttämällä omat henkilötietosi.`,
        active: true,
        view: "salaxy-components/modals/onboarding/company/wizard_index.html",
        buttonsView: "salaxy-components/modals/onboarding/company/firstPageButtons.html",
      });
    if (this.showCompanySelection) {
      steps.push(
        {
          title: "Nimenkirjoitus\u00ADoikeus",
          intro: `Luodaksesi tilitoimistotilin Palkkaus.fi-palveluun sinulla pitää olla yrityksen nimenkirjoitusoikeus.
             Tarkistamme tämän Suomi.fi-palvelusta.
             Suomi.fi-palvelu vaatii tarkistukseen henkilötunnuksen.
             Käyttöönoton kohdassa 5. allekirjoitat palvelusopimuksen / valtakirjan tämän henkilötunnuksen pankkitunnuksin.
             \n
             `,
          view: "salaxy-components/modals/onboarding/company/suomifi.html",
          buttonsView: "salaxy-components/modals/onboarding/company/suomifiButtons.html",
        });
    }
    steps.push(
      {
        title: "Yrityksen tiedot",
        view: "salaxy-components/modals/onboarding/company/company.html",
        buttonsView: "salaxy-components/modals/onboarding/company/defaultWizardButtons.html",
      },
      {
        title: "Viranomaisten kysymykset",
        view: "salaxy-components/modals/onboarding/company/company-legal.html",
        buttonsView: "salaxy-components/modals/onboarding/company/defaultWizardButtons.html",
      },
      {
        title: "Valtakirja",
        view: "salaxy-components/modals/onboarding/company/contract.html",
        buttonsView: "salaxy-components/modals/onboarding/company/contractButtons.html",
      },
    );
    return steps;
  }

  /**
   * Navigates to the next step if possible and saves the data.
   */
  public goNext(): Promise<boolean> {
    if (this.isStepProceeding) {
      return Promise.resolve(false);
    }
    this.isStepProceeding = true;
    return this.save().then(() => {
      super.goNext();
      this.isStepProceeding = false;
      return true;
    }).catch((reason) => {
      this.isStepProceeding = false;
      return false;
    });
  }

  /** Returns true if user can go forward in wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity) {
          /* if (this.model.validation.isOfficialIdUnique == true){ */
          return true;
          /* } */
        }
      }
    }
    return false;
  }

  /**
   * Navigates to the previous step if possible and saves the data.
   */
  public goPrevious() {
    if (this.isStepProceeding) {
      return;
    }
    this.isStepProceeding = true;
    this.save().then(() => {
      super.goPrevious();
      this.isStepProceeding = false;
    })
      .catch((reason) => {
        this.isStepProceeding = false;
      });
  }

  /**
   * Saves the data to server
   */
  public save(): Promise<Onboarding> {
    this.model.signature.email = this.model.person.contact.email;
    this.model.signature.telephone = this.model.person.contact.telephone;
    this.model.signature.personName = (this.model.person.firstName + " " + this.model.person.lastName).trim();
    return this.onboardingService.save();
  }

  /**
   * Returns signing url.
   */
  public get vismaSignUrl(): string {
    // HACK: Get rif of this: Use this.model.signature.digitalSignature.auth_service instead.
    const method = (this.model.signature as any).method;
    return this.onboardingService.getVismaSignUrl(method);
  }

  /**
   * The onboarding model is provided by the onboarding service.
   */
  public get model(): Onboarding {
    return this.onboardingService.model;
  }

  /** IF true, the Suomi.fi company selection is used. */
  public get showCompanySelection(): boolean {

    const isAccountant = this.model &&
    this.model.company &&
     this.model.company.roles &&
     this.model.company.roles.some( (r) => ( r === Role.Accountant || r === Role.AccountantCandidate));

    return isAccountant;
  }

  /** Returns the PDF preview address for the authorization pdf. */
  public getPdfPreviewAddress() {
    return this.onboardingService.getPdfPreviewAddress();
  }

  /**
   * Existing company alert.
   */
  public get existingCompanyAlert(): boolean {
    return (!!this.model && !!this.model.company.resolvedId);
  }

  /** Go to suomi fi ypa */
  public goToSuomifi() {

    const rawUrl = this.$location.url();
    let baseUrl = this.$location.absUrl();
    if (rawUrl.length > 1) {
      baseUrl = baseUrl.substring(0, baseUrl.indexOf(rawUrl));
    }
    baseUrl = baseUrl.replace(/\/$/, "");
    const accessToken = this.ajax.getCurrentToken();
    const token = accessToken ? `${encodeURIComponent(accessToken)}` : "";
    const cancelUrl = `${baseUrl}/onboarding/company/${this.model.id}${token ? "&ob_token=" + token : ""}`;
    const successUrl = `${baseUrl}/onboarding/company/${this.model.id}${token ? "&ob_token=" + token : ""}`;
    this.save().then(() => {
      window.location.assign(`${this.ajax.getServerAddress()}/Onboarding/CompanySelection?OnboardingId=${encodeURIComponent(this.model.id)}&SuccessUrl=${encodeURIComponent(successUrl)}&CancelUrl=${encodeURIComponent(cancelUrl)}${token ? "&access_token=" + token : ""}`);
    });
  }

  /**
   * Launches the wizard.
   * @param id Optional onboarding id.
   */
  public launch(id: string = null): Promise<any> {
    return this.onboardingService.launchCompanyOnboarding(id);
  }

  /**
   * Sets the company's business id, clears the YTJ search results fetches other company info's via YTJ search and saves
   *
   * @param businessId The business id that is set as the company's business id
   */
  public chooseBusinessId(businessId) {
    this.model.company.businessId = businessId;
    this.model.company.ytjSearch = null;
    this.searchYtj();
  }

  /**
   * Sets the DoYtjUpdate flag to true and saves.
   *
   * @param searchType - If true, sets the business ID to null
   * Use this option to force a YTJ name search, even if there already is something written in business ID field.
   */
  public searchYtj(searchType?: "businessId" | "name") {
    // Clear company's type and address
    this.model.company.companyType = null;
    this.model.company.contact.street = null;
    this.model.company.contact.city = null;
    this.model.company.contact.postalCode = null;
    // Clear previous YTJ search results
    this.model.company.ytjSearch = null;
    // Prepare for YTJ search
    this.model.company.doYtjUpdate = true;
    if (searchType === "name") {
      this.model.company.businessId = null;
    }
    if (searchType === "businessId") {

      this.model.company.name = null;
    }

    this.save();
  }

  /**
   * Returns validation error for key if exists.
   * @param key - Validation error key.
   */
  public getValidationError(key: string): ApiValidationError {
    if (this.model && this.model.validation) {
      return this.model.validation.errors.find((x) => x.key === key);
    }
    return null;
  }

}
