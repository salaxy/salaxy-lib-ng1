﻿import * as angular from "angular";

import { Ajax, Configs, Onboarding, PensionCompany, SettingsLogic } from "@salaxy/core";

import { WizardController } from "../../bases/WizardController";

import { AuthorizedAccountService, OnboardingService, SessionService, WizardService, WizardStep } from "../../../services";

/**
 * Wizard for Creating a new Palkkaus.fi company account by accountant
 */
export class CustomerOnboardingController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WizardService", "OnboardingService", "SessionService", "AuthorizedAccountService", "AjaxNg1", "$location", "data"];

  /** Form data validity */
  public formDataValidity: boolean;

  /** If true, step is proceeding */
  public isStepProceeding = false;

  constructor(
    $scope: angular.IScope,
    wizardService: WizardService,
    private onboardingService: OnboardingService,
    private sessionService: SessionService,
    private authorizedAccountService: AuthorizedAccountService,
    private ajax: Ajax,
    private $location: angular.ILocationService,
    data: any,
  ) {
    super($scope, wizardService, data);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    this.wizardService.setSteps(this.getWizardSteps());
    if (this.model &&
      this.model.company.resolvedId &&
      this.model.signature.personalId != null) {
      this.wizardService.activeStepNumber = this.wizardService.getSteps().length;
      // suomi.fi-->
      // } else if (this.model && this.model.signature.isProcura) {
      //   this.wizardService.activeStepNumber = 2;
      // <--suomi.fi
    } else {
      this.wizardService.activeStepNumber = 1;
    }
  }

  /** Company wizard configuration */
  public getWizardSteps(): WizardStep[] {
    const steps: WizardStep[] = [];
    steps.push(
      {
        title: "Yrityksen edustajan tiedot",
        heading: "",
        intro: "",
        view: "salaxy-components/modals/onboarding/customer/user.html",
        buttonsView: "salaxy-components/modals/onboarding/customer/user-buttons.html",
      },
      {
        title: "Yrityksen tiedot",
        heading: "",
        intro: "",
        view: "salaxy-components/modals/onboarding/customer/company.html",
        buttonsView: "salaxy-components/modals/onboarding/customer/buttons.html",
      },
      {
        title: "Viranomaisten kysymykset",
        heading: "",
        intro: "",
        view: "salaxy-components/modals/onboarding/customer/company-legal.html",
        buttonsView: "salaxy-components/modals/onboarding/customer/buttons.html",
      },
      {
        title: "Palkanmaksun asetukset",
        heading: "",
        intro: "",
        view: "salaxy-components/modals/onboarding/customer/insurance.html",
        buttonsView: "salaxy-components/modals/onboarding/customer/commit-buttons.html",
      },
      {
        title: "Kutsu yritys",
        heading: "",
        intro: "",
        view: "salaxy-components/modals/onboarding/customer/finish.html",
        buttonsView: "salaxy-components/modals/onboarding/customer/finish-buttons.html",
      });
    return steps;
  }

  /**
   * The onboarding model is provided by the onboarding service.
   *
   */
  public get model(): Onboarding {
    return this.onboardingService.model;
  }

  /**
   * Navigates to the next step if possible and saves the data.
   */
  public goNext() {
    if (this.isStepProceeding) {
      return;
    }
    this.isStepProceeding = true;
    this.save().then(() => {
      super.goNext();
      this.isStepProceeding = false;
    })
      .catch((reason) => {
        this.isStepProceeding = false;
      });
  }

  /**
   * Navigates to the previous step if possible and saves the data.
   */
  public goPrevious() {
    if (this.isStepProceeding) {
      return;
    }
    this.isStepProceeding = true;
    this.save().then(() => {
      super.goPrevious();
      this.isStepProceeding = false;
    })
      .catch((reason) => {
        this.isStepProceeding = false;
      });
  }

  /** Returns true if user can go forward in wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity) {
          return true;
        }
      }
    }
    return false;
  }

  /** Sharing link for authorization mail. */
  public get sharingLink(): string {
    if (!this.model) {
      return "";
    }
    return Configs.current.wwwServer + "/share/authorization/" + this.model.owner + "/" + this.model.id;
  }

  /** Mailto-link with sharing link. */
  public get mailtoLink(): string {
    if (!this.model || !this.model.person) {
      return "";
    }
    let link = "mailto:";
    link += this.model.person.contact.email;
    link += "?subject=" + encodeURIComponent("Palkkaus.fi-palvelun valtakirjan allekirjoitus");

    let body = "Hei " + this.model.person.firstName + " " + this.model.person.lastName + ",\n\n";
    body += "Olemme ottamassa käyttöön Palkkaus.fi-palvelun yrityksesi palkanmaksuun.";
    body += " Jotta he voivat hoitaa kaikki viranomaisvelvoitteet, tarvitaan allekirjoitus sinulta.";
    body += " Se hoituu helposti pankkitunnuksilla seuraamalla tätä linkkiä: ";
    body += this.sharingLink + ". \n\n";
    body += "Ystävällisin terveisin, \n";
    body += this.sessionService.getAvatar().displayName;
    link += "&body=" + encodeURIComponent(body);
    return link;
  }

  /**
   * Existing company alert.
   */
  public get existingCompanyAlert(): boolean {
    return (!!this.model && !!this.model.company.resolvedId);
  }

  /**
   * Proceeds to company selection in e-authorization service.
   */
  public goToCompanySelection(): void {

    /*  suomi.fi-->
    const rawUrl = this.$location.url();
    let baseUrl = this.$location.absUrl();
    if (rawUrl.length > 1) {
      baseUrl = baseUrl.substring(0, baseUrl.indexOf(rawUrl));
    }
    baseUrl = baseUrl.replace(/\/$/, "");
    const accessToken = this.ajax.getCurrentToken();
    const token = accessToken ? `${encodeURIComponent(accessToken)}` : "";
    const cancelUrl = `${baseUrl}/onboarding/customer/${this.model.id}${token ? "&ob_token=" + token : ""}`;
    const successUrl = `${baseUrl}/onboarding/customer/${this.model.id}${token ? "&ob_token=" + token : ""}`;
    this.save().then(() => {
      window.location.assign(`${this.ajax.getServerAddress()}/Onboarding/CompanySelection?OnboardingId=${encodeURIComponent(this.model.id)}&SuccessUrl=${encodeURIComponent(successUrl)}&CancelUrl=${encodeURIComponent(cancelUrl)}${token ? "&access_token=" + token : ""}`);
    });
     <--suomi.fi*/
    this.model.company.doYtjUpdate = true;
    this.goNext();
  }

  /** Commit onboarding */
  public commit(): void {
    if (this.isStepProceeding) {
      return;
    }
    this.isStepProceeding = true;
    this.model.signature.email = this.model.person.contact.email;
    this.model.signature.telephone = this.model.person.contact.telephone;
    this.model.signature.personName = (this.model.person.firstName + " " + this.model.person.lastName).trim();

    this.onboardingService.commit().then(() => {
      // refresh authorizing accounts
      this.authorizedAccountService.reloadAuthorizingAccounts();
      super.goNext();
      this.isStepProceeding = false;
    })
      .catch((reason) => {
        this.isStepProceeding = false;
      });
  }

  /** Sets pension fields (defaults) after user input. */
  public checkPensionCompanySelection() {

    if (!this.model.products.pension.isPensionContractDone) {
      this.model.products.pension.pensionCompany = PensionCompany.None;
      this.model.products.pension.pensionContractNumber = null;
    }
    const pensionCompany = this.model.products.pension.pensionCompany;
    if (this.model.products.pension.isPensionContractDone && pensionCompany === PensionCompany.Varma) {
      this.model.products.pension.isPendingContract = false;
    }
    // If the company is one that Palkkaus.fi/Salaxy does not have a process with, user is forced to take care of pension themselves
    if (pensionCompany === PensionCompany.Veritas || pensionCompany === PensionCompany.Apteekkien || pensionCompany === PensionCompany.Verso || pensionCompany === PensionCompany.Other) {
      this.model.products.pension.isPensionSelfHandling = true;
    }
    // Check if pending contract
    if (this.model.products.pension.isPendingContract) {
      this.model.products.pension.isPensionSelfHandling = false;
    }
  }

  /** Sets insurance fields (defaults) after user input. */
  public checkInsuranceCompanySelection() {
    if (!this.model.products.insurance.isInsuranceContractDone) {
      this.model.products.insurance.insuranceCompany = null;
      this.model.products.insurance.insuranceContractNumber = null;
    }
  }

  /**
   * Saves the data to server
   */
  public save(): Promise<Onboarding> {
    return this.onboardingService.save();
  }

  /**
   * Sets the pension contract number as a test number according to the selected company.
   */
  public setPensionNumberForTest() {
    this.model.products.pension.pensionContractNumber = SettingsLogic.getPensionNumberForTest(this.model.products.pension.pensionCompany);
  }

  /**
   * Launches the wizard.
   * @param accountId Optional account id.
   */
  public launch(accountId: string = null): Promise<any> {
    return this.onboardingService.launchCustomerOnboarding(accountId);
  }

}
