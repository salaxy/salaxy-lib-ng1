﻿import * as angular from "angular";

import { SignatureMethod, SignatureService } from "../../../services";

/**
 * Handles user interaction for Digital Signature
 */
export class SignatureController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [
        "SignatureService",
    ];

    /**
     * Creates a new SignatureController
     * @param signatureService - Service that handles the communication to the server
     * @ignore
     */
    constructor(private signatureService: SignatureService) {
    }

    /**
     * Implement IController
     */
    public $onInit = () => {
        //
    }

    /** List of supported Signature methods */
    public get methods() {
        return this.signatureService.getMethods();
    }

    /**
     * Returns the SignatureMethod for given value.
     * @param value - value for SignatureMethod, e.g. tupas-nordea.
     */
    public getMethod(value: string): SignatureMethod {
        const arr = this.methods.filter( (x) => x.value === value);
        if (arr.length > 0) {
            return arr[0];
        }
        return null;
    }
}
