﻿import * as angular from "angular";

import { Onboarding, WebSiteUserRole } from "@salaxy/core";

import { OnboardingService, SessionService, SignatureMethod, SignatureService, UiHelpers } from "../../../services";

/**
 * Controller for showing the signing user interface for unsigned account.
 */
export class SigningController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "OnboardingService", "SessionService", "SignatureService", "$sce", "UiHelpers"];

  /** If true (typically default), the signing person is also the contact person for the company */
  public isContactPersonSigner: boolean;

  /** If true, shows the component even if the Signing is already OK. Typically used only in test. */
  public showAlways: boolean;

  /**
   * Will be true if the component has done authentication and determines that signature is already OK
   * If true, the component is not shown to the end user unless showAlways is set true.
   */
  public isSigningOk: boolean;

  /** True when the session is available (auth) AND the signature is required. */
  public isSigningRequired: boolean;

  /** URL for the PDF preview. Changes when saved, to assure that PDF is not cached.  */
  public pdfPreviewUrl: string;

  constructor(
    private $scope: angular.IScope,
    private onboardingService: OnboardingService,
    private sessionService: SessionService,
    private signatureService: SignatureService,
    private $sce: angular.ISCEService,
    private uiHelpers: UiHelpers,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    if (this.sessionService.isSessionChecked && this.sessionService.isAuthenticated) {
      this.init();
    }
    this.sessionService.onAuthenticatedSession(this.$scope, () => {
      this.init();
    });
    this.isContactPersonSigner = true;
  }

  /** Saves the current onboarding model. */
  public save(): Promise<Onboarding> {
    if (this.isContactPersonSigner) {
      this.model.company.contact.email = this.model.signature.email;
      this.model.company.contact.telephone = this.model.signature.telephone;
    }
    return this.onboardingService.save()
      .then((onboarding) => {
        this.pdfPreviewUrl = this.$sce.trustAsResourceUrl(this.onboardingService.getPdfPreviewAddress(true) + "&t=" + new Date());
        return onboarding;
      });
  }

  /**
   * The onboarding model is provided by the onboarding service.
   */
  public get model(): Onboarding {
    return this.onboardingService.model;
  }

  /** Returns the PDF preview address for the authorization pdf. */
  public getPdfPreviewAddress() {
    return this.pdfPreviewUrl;
  }

  /**
   * Saves the model and starts the signature.
   * @param authMethod Method of authentication (bank selection).
   */
  public startVismaSign(authMethod: string) {
    this.uiHelpers.showLoading("Digitaalinen allekirjoitus", `Siirrytään allekirjoitukseen (${authMethod})`);
    // Saving is needed mainly for potential changes in name, e-mail and phone. AuthService, signerPersonalId and successUrl are passed in URL.
    this.save().then(() => {
      window.top.location.href = this.onboardingService.getVismaSignUrl(authMethod);
    });
  }

  /** List of supported Signature methods */
  public get methods() {
    return this.signatureService.getMethods();
  }

  /**
   * Returns the SignatureMethod for given value.
   * @param value - value for SignatureMethod, e.g. tupas-nordea.
   */
  public getMethod(value: string): SignatureMethod {
    const arr = this.methods.filter((x) => x.value === value);
    if (arr.length > 0) {
      return arr[0];
    }
    return null;
  }

  private init() {
    this.isSigningOk = this.sessionService.isSigningOk();
    if (this.sessionService.isSessionChecked && this.sessionService.isAuthenticated) {
      this.isSigningRequired = !this.sessionService.isSigningOk();
      const accountId = this.sessionService.session.currentAccount.id;
      const updateFromModel = () => {
        this.pdfPreviewUrl = this.$sce.trustAsResourceUrl(this.onboardingService.getPdfPreviewAddress(true) + "&t=" + new Date());
      };
      if (this.onboardingService.model && this.onboardingService.model.company.resolvedId === accountId) {
        updateFromModel();
        return;
      }
      const getModel = () => {
        return this.onboardingService.getOnboardingDataForAccount(this.sessionService.session.currentAccount.id).then(() => {
          if (this.onboardingService.model) {
            this.onboardingService.model.signature.isPep = null;
            this.onboardingService.model.company.ownership.isRequiredTracking = null;
            return this.onboardingService.model;
          } else {
            this.onboardingService.model = {
              accountType: WebSiteUserRole.Company,
              signature: {},
            };
            this.onboardingService.id = null;
            return this.onboardingService.save();
          }
        });
      };

      getModel().then( () => {
        updateFromModel();
      });

    }
  }
}
