﻿import * as angular from "angular";

import { ApiValidationError, Onboarding, SystemRole } from "@salaxy/core";

import { OnboardingService, SessionService, UiHelpers, WizardService, WizardStep } from "../../../services";

import { WizardController } from "../../bases/WizardController";

/**
 * Wizard for Creating a new Palkkaus.fi-account (worker)
 */
export class WorkerOnboardingController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WizardService", "OnboardingService", "SessionService", "UiHelpers"];

  /** Worker wizard configuration */
  public wizardSteps: WizardStep[] = [
    {
      title: "Käyttäjän tiedot",
      heading: "Tervetuloa, työntekijä!",
      intro: "",
      active: true,
      view: "salaxy-components/modals/onboarding/worker/user-info.html",
      buttonsView: "salaxy-components/modals/onboarding/worker/firstPageButtons.html",
    },
    {
      title: "Puhelinnumeron vahvistaminen",
      heading: "Puhelinnumeron vahvistaminen",
      intro: "Olemme lähettäneet sinulle SMS-viestin varmistaaksemme numerosi. Syötä viestissä oleva vahvistuskoodi tähän.",
      view: "salaxy-components/modals/onboarding/worker/phonenumber-verify.html",
      buttonsView: "salaxy-components/modals/onboarding/worker/phoneNumberVerifyButtons.html",
    },
    {
      title: "Sähköinen tunnistautuminen",
      heading: "Tilisopimus ja valtuutus",
      intro: `Valtuuta pankkitunnuksillasi Palkkaus.fi-palvelu hoitamaan palkanmaksuun
             liittyviä asioita puolestasi. Tarvitsemme valtakirjan, jotta voimme antaa esim. verokorttisi työnantajalle ja hoitaa asioita viranomaisten suuntaan puolestasi. Valtuutus pitää tehdä vain kerran.`,
      view: "salaxy-components/modals/onboarding/worker/contract-worker.html",
      buttonsView: "salaxy-components/modals/onboarding/worker/contractButtons.html",
    },
  ];

  /**
   * Field to expose forms validity state.
   */
  public formDataValidity: boolean;

  /** If true, step is proceeding */
  public isStepProceeding = false;

  /** if true, verification process is proceeding */
  public isVerificationProceeding = false;

  constructor(
    $scope: angular.IScope,
    wizardService: WizardService,
    private onboardingService: OnboardingService,
    private sessionService: SessionService,
    private uiHelpers: UiHelpers,
  ) {
    super($scope, wizardService);
  }

   /**
    * Implement IController
    */
   public $onInit() {
    this.wizardService.setSteps(this.wizardSteps);
    this.wizardService.activeStepNumber = 1;
  }

  /**
   * Navigates to the next step if possible and saves the data.
   */
  public goNext(): Promise<boolean> {
    if (this.isStepProceeding) {
      return Promise.resolve(false);
    }
    this.isStepProceeding = true;
    return this.save().then(() => {
      super.goNext();
      this.isStepProceeding = false;
      return true;
    }).catch((reason) => {
      this.isStepProceeding = false;
      return false;
    });
  }

  /** Returns true if user can go forward in wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity) {
          /* if (this.model.validation.isOfficialIdUnique == true){ */
          return true;
          /* } */
        }
      }
    }
    return false;
  }

  /** Runs a check function before going forward */

  public goNextIf(checkFunction: (goNext: () => Promise<boolean>) => void) {
    checkFunction(() => this.goNext());
  }

  /**
   * Navigates to the previous step if possible and saves the data.
   */
  public goPrevious() {
    if (this.isStepProceeding) {
      return;
    }
    this.isStepProceeding = true;
    this.save().then(() => {
      super.goPrevious();
      this.isStepProceeding = false;
    })
      .catch((reason) => {
        this.isStepProceeding = false;
      });
  }

  /**
   * Saves the data to server
   */
  public save(): Promise<Onboarding> {
    this.model.signature.email = this.model.person.contact.email;
    this.model.signature.telephone = this.model.person.contact.telephone;
    this.model.signature.personName = (this.model.person.firstName + " " + this.model.person.lastName).trim();
    return this.onboardingService.save();
  }

  /**
   * Returns signing url.
   */
  public get vismaSignUrl(): string {
    // HACK: Get rif of this: Use this.model.signature.digitalSignature.auth_service instead.
    const method = (this.model.signature as any).method;
    return this.onboardingService.getVismaSignUrl(method);
  }

  /**
   * Option to skip telepehone sms verification in the test enviroment.
   */
  public get skipSmsVerification(): boolean {
    return this.sessionService.isInRole(SystemRole.Test);
  }

  /**
   * The onboarding model is provided by the onboarding service.
   */
  public get model(): Onboarding {
    return this.onboardingService.model;
  }

  /** Returns the PDF preview address for the authorization pdf. */
  public getPdfPreviewAddress() {
    return this.onboardingService.getPdfPreviewAddress();
  }

  /**
   * Returns validation error for key if exists.
   * @param key - Validation error key.
   */
  public getValidationError(key: string): ApiValidationError {
    if (this.model && this.model.validation) {
      return this.model.validation.errors.find((x) => x.key === key);
    }
    return null;
  }

  /**
   * Resets validation error for key if exists.
   * @param key - Validation error key.
   */
  public removeValidationError(key: string): void {
    if (this.model && this.model.validation) {
      (this.model.validation as any).errors = this.model.validation.errors.filter((x) => x.key !== key);
    }
  }

  /** Sends a new pin code for verifying the telephone number in onboarding. */
  public sendSmsVerificationPin = (callback: () => Promise<boolean>) => {
    if (this.isVerificationProceeding) {
      return;
    }
    this.isVerificationProceeding = true;
    if (this.sessionService.isInRole(SystemRole.Test) && this.skipSmsVerification === true) {
      this.model.person.contactVerification.telephone.pin = null;
      this.removeValidationError("Person.ContactVerification.Telephone.Pin");
      callback().then(() => {
        this.isVerificationProceeding = false;
      });
    } else {
      this.save().then(() => {
        this.onboardingService.sendSmsVerificationPin().then((messageSent: boolean) => {
          if (messageSent) {
            this.model.person.contactVerification.telephone.pin = null;
            this.removeValidationError("Person.ContactVerification.Telephone.Pin");
          }
          callback().then(() => {
            this.isVerificationProceeding = false;
          });
        });
      });
    }
  }

  /** Checks the verfication pin and calls callback if success */
  public checkSmsVerificationPin = (callback: () => Promise<boolean>) => {
    if (this.isVerificationProceeding) {
      return;
    }
    this.isVerificationProceeding = true;
    if (this.sessionService.isInRole(SystemRole.Test) && this.skipSmsVerification === true) {
      callback().then(() => {
        this.isVerificationProceeding = false;
      });
    } else {
      this.onboardingService.checkSmsVerificationPin().then((result) => {
        if (result) {
          callback().then(() => {
            this.isVerificationProceeding = false;
          });
        } else {
          this.isVerificationProceeding = false;
        }
      });
    }
  }

  /**
   * Checks if the given string is other identifier than Finnish Personal Identification Number
   * @param ssn given social security number
   */
  public isOtherIdentifier(ssn: string){

    /** 8th character is 9 */
    const regex = /^.{7}[9]/;
    return regex.test((ssn || "").trim());
  }

  /**
   * Launches the wizard.
   * @param id Optional onboarding id.
   */
  public launch(id: string = null): Promise<any> {
    return this.onboardingService.launchWorkerOnboarding(id);
  }
}
