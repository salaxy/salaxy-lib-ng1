import * as angular from "angular";

import { ApiCrudObject, CrudApiBase, IUserObjectIndex, ODataQueryOptions, ODataResult, VersionInfo, WorkflowData, WorkflowEvent } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ODataQueryController } from "./ODataQueryController";

/**
 * Base class for version 03 CRUD controllers where OData list functionality has been
 * separated from the item editing CRUD functionality.
 * This version also shifts away from the old Service-infrastructure.
 */
export abstract class ApiCrudObjectController<TItem extends ApiCrudObject> implements angular.IController {

  /**
   * Defines the binding mode of the component.
   *
   * - "model" takes the model directly from the model parameter.
   * - "parentCtrl" takes the model from the given parent controller. Main actions (like Save, reload) are delegated to that controller.
   * - "id" fetches the item from the server using the current-id attribute (currentId property).
   * - "url" like "id", but the id is fetched from route parameters.
   * - "new" Creates a new item as current.
   * - "null": Current item is null
   *
   * The mode is set in the model property setter.
   */
  public bindingMode: "id" | "url" | "model" | "parentCtrl" | "new" | "null";

  /** Action for the controller */
  public action: "default" | "copy-as-new" | string;

  /**
   * For list-controls, this is the URL for item select event
   * as well as the URL where a new item is edited. Basically showing the Details view.
   * Id is added directly to the string and this is an angular route.
   * I.e. use "/calc/details/" (not "#/calc/details/" or "/calc/details")
   * For more control, use onListSelect or onCreateNew events.
   * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
   */
  public detailsUrl: string;

  /**
   * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
   * @example
   * <!-- Main worker list is in the front page in this case -->
   * <salaxy-worker-details list-url="/home"></salaxy-worker-details>
   */
  public listUrl: string;

  /** If the Crud controller is used from within a ODataQueryController, this is the containing controller. */
  public odataController: ODataQueryController;

  /** If the controller is bound to parent controller, the controller is set here. */
  public parentController: ApiCrudObjectController<TItem>;

  /**
   * Contains the inner value currentId if stored by this controller and not the parent.
   */
  protected _currentId: string;

  /**
   * The status of the data loading for the current controller.
   */
  private _status: "noInit" | "initialLoading" | "loaded" | "reloading" | "loadError" = "noInit";
  private _current: TItem;

  private _original: TItem;

  private _model: any;

  /** If true, current item is read only */
  private _isReadOnlyForced: boolean;
  private _versions: VersionInfo[];

  private _versionsId: string;
  private _version: { version: VersionInfo, item?: TItem };
  private _versionId: string;

  /**
   * Creates a new CrudControllerBase.
   * @param api Api module typically defined in @salaxy/core used for communicating to server.
   * @param uiHelpers - Salaxy ui helpers service.
   * @param $location Angular.js Location service that is used for navigation. Especially the list views.
   * @param $routeParams - Angular.js routing component
   */
  constructor(
    protected api: CrudApiBase<TItem>,
    protected uiHelpers: UiHelpers,
    protected $location: angular.ILocationService,
    protected $routeParams: any,
  ) {
    if (!api) {
      throw new Error("api is undefined in ApiCrudObjectController");
    }
    if (!uiHelpers) {
      throw new Error("uiHelpers is undefined in ApiCrudObjectController");
    }
    if (!$location) {
      throw new Error("$location is undefined in ApiCrudObjectController");
    }
    if (!$routeParams) {
      throw new Error("$routeParams is undefined in ApiCrudObjectController");
    }
  }

  /**
   * Implement IController by providing onInit method.
   * We currently do nothing here, but if you override this function,
   * you should call this method in base class for future compatibility.
   */
  public $onInit() {
    this.bindingMode = this.bindingMode || "null";
  }

  /** URL for the OData list service */
  public get odataServiceUrl(): string {
    return this.parentController ? this.parentController.odataServiceUrl : this.api.getODataUrl();
  }

  /**
   * Set the Current selected item (the model).
   * The model may be:
   *
   * - TItem: A bound item (default), which can also be null.
   * - ApiCrudObjectController<TItem>: Parent controller of the same type. Model is fetched from its current and all methods are delegated.
   * - "new": A new item is created.
   * - "url": Fetches the item from the server using the id query string (crudItemId in routeParams).
   * - string: Any other string is interpreted as id which is used to fetch the item from the server.
   */
  public set model(value: "new" | "url" | string | TItem | ApiCrudObjectController<TItem>) {
    this._model = value;
    if (!value) {
      this._currentId = null;
      this.bindingMode = "null";
      this.parentController = null;
    } else if (value === "new") {
      this._currentId = null;
      this.bindingMode = "new";
      this.parentController = null;
    } else if (value === "url") {
      this._currentId = this.getRouteData().id;
      this.bindingMode = (this.currentId ? "url" : "null");
      this.parentController = null;
      this.action = this.action || this.getRouteData().action;
    } else if (angular.isString(value)) {
      this._currentId = value;
      this.bindingMode = (this.currentId ? "id" : "null");
      this.parentController = null;
    } else {
      if (value && (value as ApiCrudObjectController<TItem>).$onInit) {
        // If parent controller, we do not call reload() - it shold be called by the parent itself.
        this.bindingMode = "parentCtrl";
        this.parentController = value as ApiCrudObjectController<TItem>;
        this.action = this.parentController.action;
        return;
      } else {
        this._currentId = null; // Getter gets it from the object itself.
        this.bindingMode = "model";
        this.parentController = null;
      }
    }
    this.action = this.action || "default";
    this.reload();
  }
  /** Gets the Current selected item (the model).  */
  public get model() {
    return this._model;
  }

  /** Gets the Current selected item. */
  public get current(): TItem {
    return this.parentController ? this.parentController.current : this._current;
  }

  /** The current Id for the controller. */
  public get currentId(): string {
    if (this.parentController) {
      return this.parentController.currentId;
    }
    return this._currentId || (this.current ? this.current.id : null);
  }

  /** Gets the Original item before changes. */
  public get original(): TItem {
    return this.parentController ? this.parentController.original : this._original;
  }

  /** Gets the isReadOnlyForced status. */
  public get isReadOnlyForced(): boolean {
    return this.parentController ? this.parentController.isReadOnlyForced : this._isReadOnlyForced;
  }

  /** Sets the isReadOnlyForced status. */
  public set isReadOnlyForced(value: boolean) {
    /* do not set values by non-bound variables */
    if (value === undefined) {
      return;
    }
    if (this.parentController) {
      this.parentController.isReadOnlyForced = value;
    } else {
      this._isReadOnlyForced = value;
    }
  }
  /**
   * Gets the defaults for service-specific URLs, options etc.
   * This method must be implemented when implementing ApiCrudObjectController.
   */
  public abstract getDefaults(): {
    /**
     * URL for the list view. This is list-url attribute with controller-specific default value (e.g. "/articles").
     * At the moment, if specified, the browser is redirected here after delete.
     */
    listUrl: string,

    /**
     * For lists, this is the URL for item select event as well as the URL where a new item is edited.
     * Basically showing the Details view. For more control, use onListSelect or onCreateNew events.
     * This is details-url attribute with controller-specific default value.
     * Id is added directly to the string and this is an angular route.
     * I.e. use "/calc/details/" (not "#/calc/details/" or "/calc/details")
     */
    detailsUrl: string,

    /** Template for displaying the items as list.  */
    oDataTemplateUrl: string,

    /** The OData options for default query. */
    oDataOptions: ODataQueryOptions,
  };

  /** If true, this is a new unsaved object */
  public isNew(): boolean {
    if (this.parentController) {
      return this.parentController.isNew();
    }
    return !this.currentId || this.currentId === "new" || this.action === "copy-as-new";
  }

  /** If true, the component is in the middle of loading data. */
  public get isLoading() {
    switch (this.status) {
      case "initialLoading":
      case "reloading":
        return true;
      case "loaded":
      case "loadError":
      case "noInit":
      default:
        return false;
    }
  }

  /**
   * If true, the form controls should be disabled.
   */
  public get isDisabled() {
    switch (this.status) {
      case "initialLoading":
      case "reloading":
      case "loadError":
      case "noInit":
        return true;
      case "loaded":
      default:
        return false;
    }
  }

  /**
   * If true, the form controls should be read-only (no input control at all).
   */
  public get isReadOnly() {
    // TODO: When editing this, search for: "// TODO: When moving to ES6, call super.isReadOnly instead."
    return this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly || this.isReadOnlyForced || false;
  }

  /** Returns true, if the current object has changes. */
  public get hasChanges(): boolean {
    return this.parentController ? this.parentController.hasChanges : !angular.equals(this._original, this._current);
  }

  /**
   * Gets the status of the data if it is being loaded from a remote server.
   */
  public get status(): "noInit" | "initialLoading" | "loaded" | "reloading" | "loadError" {
    return this.parentController ? this.parentController.status : this._status;
  }

  /** Makes an OData query  */
  public getOData(options: ODataQueryOptions): Promise<ODataResult<any>> {
    return this.api.getOData(options);
  }

  /** Returns base url of the underlying api. */
  public getBaseUrl(): string {
    return this.api.getBaseUrl();
  }

  /** Gets the data transmitted in the route / url */
  public getRouteData(): {
    /** id parameter in the route */
    id: string,
    /** Action parameter in the route. */
    action: "default" | "copy-as-new" | string,
  } {
    const result = {
      id: null,
      action: null,
    };
    if (this.$routeParams && this.$routeParams.crudItemId) {
      let crudItemIdArr = this.$routeParams.crudItemId.split("/");
      crudItemIdArr = crudItemIdArr.map(x => x ? x.replace(/[?&#](.*)/g, "") : x); // remove trailing parameters. eg. access_token
      result.id = crudItemIdArr[0] || null;
      if (crudItemIdArr[1]) {
        result.action = crudItemIdArr[1];
      }
    }
    return result;
  }

  /** Save changes to the current item */
  public save(): Promise<TItem> {
    if (this.parentController) {
      return this.parentController.save();
    }
    const wasNew = this.isNew();
    const loader = this.uiHelpers.showLoading("Tallennetaan...");
    this._status = "reloading";
    return this.api.save(this.current)
      .then((data) => {
        loader.dismiss();
        this._current = data;
        this._original = angular.copy(data);
        this._status = "loaded";
        if (this.bindingMode === "url" && wasNew) {
          this.$location.url(this.getDefaults().detailsUrl + data.id);
          this.action = "default";
        }
        return data;
      })
      .catch((error) => {
        loader.dismiss();
        this._status = "loadError";
        return null;
      });
  }

  /** Reset the changes in the current element */
  public reset(): TItem {
    if (this.parentController) {
      return this.parentController.reset();
    }
    return angular.copy(this._original, this._current);
  }

  /**
   * Shows the detail view for the item.
   * Typically, this takes the user to a new page with the ID.
   * @param item Item may be either Container or list item.
   */
  public showDetails(item: TItem | IUserObjectIndex): angular.ILocationService {
    if (this.parentController) {
      return this.parentController.showDetails(item);
    }
    return this.$location.url(this.getDefaults().detailsUrl + this.getId(item));
  }

  /**
   * Shows the detail view with a new item that is a copy of the given item.
   * Typically, this takes the user to a new page with the ID with action "copy-as-new".
   * @param item Item may be either Container or list item.
   */
  public showCopyAsNew(item: TItem | IUserObjectIndex): angular.ILocationService {
    if (this.parentController) {
      return this.parentController.showCopyAsNew(item);
    }
    return this.$location.url(this.getDefaults().detailsUrl + this.getId(item) + "/copy-as-new");
  }

  /**
   * Shows the "Are you sure?" dialog and if user clicks OK, deletes the item.
   * Cancels the started payment for the payroll too.
   *
   * @param item Item to be deleted.
   * If not specified, the current item.
   * @param confirmMessage Optional custom message for the confirm dialog.
   * If not specified, a generic message is shown.
   * If set to boolean false, the confirm message is not shown at all.
   *
   * @returns Promise that resolves to true if the item is deleted.
   * False, if user cancels and fails if the deletion fails.
   */
  public delete = (item?: TItem, confirmMessage?: string): Promise<boolean> => {
    if (this.parentController) {
      return this.parentController.delete(item, confirmMessage);
    }
    item = item || this.current;
    return this.uiHelpers.showConfirm(confirmMessage || "Haluatko varmasti poistaa tämän tietueen?")
      .then((result: boolean) => {
        if (result) {
          return this.deleteNoConfirm(item).then(() => {
            if (this.current === item) {
              this._currentId = null;
              this._current = null;
              this._original = null;
              this._status = "noInit";
            }
            if (this.odataController) {
              this.odataController.reload();
            } else {
              this.$location.url(this.getDefaults().listUrl);
            }
            return true;
          });
        } else {
          return Promise.resolve(false);
        }
      });
  }

  /**
   * Gets the id for the object regardless of whther the object is Container or list item.
   * Error is thrown if the item does not exist.
   * The method is an extension point for future item types that may have other ID's in the future.
   * @param item The item for which to fetch the ID.
   */
  public getId(item: TItem | IUserObjectIndex) {
    if (!item) {
      throw new Error("getId called for null item.");
    }
    if ((item as IUserObjectIndex).containerGuid) {
      return (item as IUserObjectIndex).containerGuid;
    }
    return item.id;
  }

  /**
   * Deletes an item without showing the confirm dialog.
   * The method shows the "Please wait..." loader, but does not refresh the
   * list or move the browser to listUrl. The caller should take care
   * of the UX actions after delete if necessary.
   *
   * @param item Item to be deleted.
   * @returns Promise that resolves to true (never false). Fails if the deletion fails.
   */
  public deleteNoConfirm(item: TItem | IUserObjectIndex): Promise<boolean> {
    if (this.parentController) {
      return this.parentController.deleteNoConfirm(item);
    }
    if (!item) {
      throw new Error("item not specified in deleteNoConfirm.");
    }
    const loading = this.uiHelpers.showLoading("Odota...");
    return this.api.delete(this.getId(item)).then(() => {
      loading.dismiss();
      return true;
    });
  }

  /**
   * Creates a copy of a given item.
   * This is a synchronous method that should basically convert a saved item to a new item.
   * @param copySource Item (container item) to copy as new.
   */
  public copyItem(copySource: TItem): TItem {
    // TODO: Consider moving to logic or probably API.
    const copy = JSON.parse(JSON.stringify(copySource)) as TItem;
    (copy as any).id = null;
    return copy;
  }

  /**
   * Starts the reload process depending on the bindingMode:
   */
  public reload(): Promise<TItem> {
    // TODO: The promise reloads here may require $scope.apply() so that .then() works on the promise.
    // We should go through whether $scope.apply() should be added here instead of caller needing to worry about it.
    if (this.parentController) {
      this._currentId = this.parentController.currentId;
      return this.parentController.reload().then((item) => {
        this._currentId = this.parentController.currentId;
        return item;
      });
    }
    switch (this.bindingMode) {
      case "id":
      case "url":
        if (this.bindingMode === "url") {
          this._currentId = this.getRouteData().id;
          if (!this.currentId) {
            this.setCurrentRef(null);
            this._status = "loaded";
            return Promise.resolve<TItem>(this.current);
          }
        }
        if (this.currentId === "new") {
          this.setCurrentRef(this.api.getBlank());
          this._currentId = null;
          this._status = "loaded";
          return Promise.resolve<TItem>(this.current);
        }
        // Reloads from server (status is set by the loading).
        return this.setStatus(this.api.getSingle(this.currentId))
          .then((item) => {
            if (!item) {
              this._status = "loadError";
              throw new Error(`Item ${this.currentId} not found.`);
            }
            if (this.action === "copy-as-new") {
              item = this.copyItem(item);
            }
            this.setCurrentRef(item);
            return item;
          });
      case "model":
        // Gets the model again from source.
        this.setCurrentRef(this._model as TItem);
        this._currentId = this.current ? this.current.id : null;
        this._status = this.current ? "loaded" : "noInit";
        return Promise.resolve<TItem>(this.current);
      case "parentCtrl":
        throw new Error("Binding mode parentCtrl without parentController.");
      case "new":
        // Recreates a new item
        this.setCurrentRef(this.api.getBlank());
        this._currentId = null;
        this._status = "loaded";
        return Promise.resolve<TItem>(this.current);
      case "null":
      default:
        // Sets the current as null.
        this.setCurrentRef(null);
        this._currentId = null;
        this._status = "loaded";
        return Promise.resolve<TItem>(this.current);
    }
  }

  /**
   * Sets the current item as the given object (reference to a new object).
   * This also resets the original item. This method is typically used when the object is first loaded,
   * when object that is being edited is changed or when object is reloaded from server after saving.
   * @param item The new current item.
   */
  public setCurrentRef(item: TItem) {
    if (this.parentController) {
      return this.parentController.setCurrentRef(item);
    }
    this._current = item;
    this._original = angular.copy(item);
  }

  /**
   * Sets the current item value:
   * The object reference remains the same, but all its properties are copied using angular.copy().
   * Original object remains as-is by default. This method is typically used at the end of modifyinjg user interfaces.
   * @param item The new current item.
   * @param resetOriginal If true, also resets the orginal value.
   */
  public setCurrentValue(item: TItem, resetOriginal = false) {
    if (this.parentController) {
      return this.parentController.setCurrentValue(item, resetOriginal);
    }
    if (this._current) {
      angular.copy(item, this._current);
    } else {
      // This should not really happen, but just in case the methods are used in a wrong way.
      this._current = item;
    }
    if (resetOriginal) {
      this._original = angular.copy(item);
    }
  }

  /**
   * Sets the current item. This also resets the original item.
   * @param item The new current item.
   * @param keepOriginalAsIs If true, will not update the original.
   */
  public setCurrent(item: TItem, keepOriginalAsIs = false): void {
    if (this.parentController) {
      return this.parentController.setCurrent(item, keepOriginalAsIs);
    }
    // TODO: We should probably have two distinct methods: setCurrentRef and setCurrentValue
    if (this._current) {
      angular.copy(item, this._current);
    } else {
      this._current = item;
    }
    if (!keepOriginalAsIs) {
      this._original = angular.copy(item);
    }
  }

  /**
   * Calls a promise updating the status on the controller.
   * @param loadAction The promise that executes the loading from server.
   * If action is null, the status is directly set as "loaded".
   */
  public setStatus(loadAction: Promise<TItem>): Promise<TItem> {
    if (this.parentController) {
      return this.parentController.setStatus(loadAction);
    }
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    if (!loadAction) {
      this._status = "loaded";
      return null;
    }
    switch (this._status) {
      case "noInit":
        this._status = "initialLoading";
        break;
      case "initialLoading":
        this._status = "initialLoading";
        break;
      case "loadError":
        this._status = (this.current ? "reloading" : "initialLoading");
        break;
      case "loaded":
        this._status = "reloading";
        break;
      case "reloading":
        this._status = "reloading";
        break;
      default:
        throw Error("Unexpected status: " + this._status);
    }
    return loadAction.then((data) => {
      this._status = "loaded";
      return data;
    })
      .catch((error) => {
        this._status = "loadError";
        return null;
      });
  }

  /**
   * Returns the current workflow event of the given type.
   * This does not access the API.
   *
   * @param wfType Event type of the workflow event.
   * @returns Returns the current workflow event of the given type.
   */
  public getWorkflowEvent(wfType: string): WorkflowEvent {
    if (!this.current || !wfType) {
      return null;
    }

    const workflowData: WorkflowData = (this.current as any).workflowData;
    if (!workflowData || !workflowData.events) {
      return null;
    }
    return workflowData.events.find((x) => (x.type || "").toLowerCase() === wfType.toLowerCase());
  }

  /**
   * Adds/updates the workflow event for the current using API.
   *
   * @param wfEvent - Workflow event to add/update.
   * @returns - Reloaded item.
   */
  public saveWorkflowEvent(wfEvent: WorkflowEvent): Promise<TItem> {
    return this.api.saveWorkflowEvent(this.current, wfEvent).then(() => {
      return this.reload();
    });
  }

  /**
   * Deletes the given event or all events with given type using API.
   *
   * @param wfIdOrType Id or type of the workflow event.
   * @returns - Reloaded item.
   */
  public deleteWorkflowEvent(wfIdOrType: string): Promise<TItem> {
    return this.api.deleteWorkflowEvent(this.current, wfIdOrType).then(() => {
      return this.reload();
    });
  }

  /** Returns the current api */
  public getApi(): CrudApiBase<TItem> {
    return this.api;
  }

  /**
   * Returns the versions for the current item.
   */
  public get versions(): VersionInfo[] {
    if (!this._versions || this._versionsId !== this.currentId) {
      this._versions = [];
      this._versionsId = this.currentId;
      if (this.currentId) {
        this.api.getVersions(this.currentId).then((data) => {
          this._versions.push(...data);
        });
      }
    }
    return this._versions;
  }

  /**
   * Gets a version of the current item.
   * @param version VersionInfo of the item.
   */
  public getVersion(version: VersionInfo): { version: VersionInfo, item?: TItem } {
    if (!this._version || this._versionId !== `${this.currentId}-${version?.versionId}`) {
      this._version = (this._version ?? {} as any);
      this._version.version = version;
      this._versionId = `${this.currentId}-${version?.versionId}`;
      if (this.currentId) {
        this.api.getVersion(this.currentId, version?.versionId).then((data) => {
          this._version.item = data;
        });
      }
    }
    return this._version;
  }
}
