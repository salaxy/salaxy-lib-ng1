/**
 * Bindings for the components that use ApiCrudObjectController.
 */
export class ApiCrudObjectControllerBindings {

  [boundProperty: string]: string;

  /**
   * URL to which the component navigates when an item is clicked.
   * The "id" or "rowIndex" property of the selected item is added to the URL.
   * URL is ignored if onListSelect is set. In this case, you may navigate yourself in that method.
   * Id is added directly to the string and this is an angular route.
   * I.e. use "/calc/details/" (not "#/calc/details/" or "#/calc/details")
   * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
   */
  public detailsUrl = "@";

  /**
   * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
   * @example
   * <!-- Main worker list is in the front page in this case -->
   * <salaxy-worker-details list-url="/home"></salaxy-worker-details>
   */
  public listUrl = "@";

  /**
   * Set the Current selected item (the model).
   * The model may be:
   *
   * - TItem: A bound item (default), which can also be null.
   * - "new": A new item is created.
   * - "url": Fetches the item from the server using the id query string (crudItemId in routeParams).
   * - string: Any other string is interpreted as id which is used to fetch the item from the server.
   */
  public model = "<";

   /** If true, current item is read only */
  public isReadOnlyForced = "<";
}
