import { IService, UiHelpers } from "../../services";

/**
 * Base class for CRUD controllers.
 * Implements the common UI functionality that is possible based on BaseService<T>.
 */
export class CrudControllerBase<T> implements angular.IController {

    /**
     * Bindings for the component which uses CRUD controller.
     */
    public static crudBindings = {

        /**
         * Function that is called when user selects an item in the list.
         * The selected item is the parameter of the function call.
         * @example <salaxy-payroll-list on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
         */
        onListSelect: "&",

        /**
         * Function that is called when an item is deleted.
         * The event is intended for user interface logic after delete (promise resolve)
         * and potentially when waiting for server to respond (from function call until promise resolve).
         * It is not meant for delete validation and/or for preventing deletion.
         * If onDelete is not specified, the browser is redirected to listUrl if specified.
         * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
         */
        onDelete: "&",

        /**
         * Function that is called after a new item has been created.
         * At this point, the item has been created, but not yet selected as current.
         * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
         * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc"></salaxy-payroll-list>
         */
        onCreateNew: "&",

        /**
         * URL to which the component navigates when an item is clicked.
         * The "id" or "rowIndex" property of the selected item is added to the URL.
         * URL is ignored if onListSelect is set. In this case, you may navigate yourself in that method.
         * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
         */
        detailsUrl: "@",

        /**
         * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
         * @example
         * <!-- Main worker list is in the front page in this case -->
         * <salaxy-worker-details list-url="/home"></salaxy-worker-details>
         */
        listUrl: "@",
    };

    /**
     * For list-controls, this is the URL for item select event
     * as well as the URL where a new item is edited. Basically showing the Details view.
     * For more control, use onListSelect or onCreateNew events.
     * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
     */
    public detailsUrl: string;

    /**
     * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
     * @example
     * <!-- Main worker list is in the front page in this case -->
     * <salaxy-worker-details list-url="/home"></salaxy-worker-details>
     */
    public listUrl: string;

    /**
     * Angular ng-model **if** the controller is bound to model using ng-model attribute.
     */
    public model: angular.INgModelController;

    /**
     * Function that is called when an item is deleted.
     * The event is intended for user interface logic after delete (promise resolve)
     * and potentially when waiting for server to respond (from function call until promise resolve).
     * It is not meant for delete validation and/or for preventing deletion.
     * If onDelete is not specified, the browser is redirected to listUrl if specified.
     * NOTE: the deleteResult should basically always be  Promise<true> or promise failure. Promise<false> does not really make sense here.
     * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc(deleteResult)"></salaxy-payroll-list>
     */
    public onDelete: (eventData: {
      /** The deleteResult should basically always be  Promise<true> or promise failure. Promise<false> does not really make sense here. */
      deleteResult: Promise<boolean>,
    }) => Promise<boolean>;

    /**
     * Function that is called after a new item has been created.
     * At this point, the item has been created, but not yet selected as current.
     * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
     * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc(item)"></salaxy-payroll-list>
     */
    public onCreateNew: (eventData: {
      /** New item which was created. */
       item: T,
      }) => void;

    /**
     * For list-controls, this is the function of on-list-select event.
     * If onListSelect is not specified, the browser is redirected to detailsUrl if specified.
     * If detailsUrl is not specified, only current item is set.
     * @example <salaxy-payroll-list on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
     */
    public onListSelect: (eventData: {
      /** The selected item. */
      item: T,
    }) => void;

    /**
     * Creates a new CrudControllerBase.
     * @param crudService The the BaseService instance that is used for communicating to server.
     * @param $location Angular.js Location service that is used for navigation. Especially the list views.
     * @param $attrs Angular.js Attisbutes for determining whether events have been bound to.
     * @param uiHelpers - Salaxy ui helpers service.
     */
    constructor(
        protected crudService: IService<T>,
        protected $location: angular.ILocationService,
        protected $attrs: angular.IAttributes,
        protected uiHelpers: UiHelpers,
    ) {
        if (!crudService) {
            throw new Error("crudService is undefined in CrudControllerBase");
        }
        if (!$location) {
            throw new Error("$location is undefined in CrudControllerBase");
        }
        if (!uiHelpers) {
            throw new Error("uiHelpers is undefined in CrudControllerBase");
        }
        if (!$attrs) {
            throw new Error("$attrs is undefined in CrudControllerBase");
        }
    }

    /**
     * Implement IController by providing onInit method.
     * We currently do nothing here, but if you override this function,
     * you should call this method in base class for future compatibility.
     */
    public $onInit() {
        // No init code at the moment, we may add later if necessary.
    }

    /** Gets the Current selected item. */
    public get current(): T {
      if (this.getBindingMode() === "model") {
        return this.model.$viewValue;
      }
      return this.crudService.current;
    }

    /** Gets the list of all CRUD objects listed. */
    public get list(): T[] {
        return this.crudService.list;
    }

    /**
     * Creates a new item. The item is not saved in this process yet.
     * Item is set as current unless onCreateNew is specified => Then you are responsible for doing it yourself.
     * If detailsUrl is specified, the browser is redirectedted there.
     * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
     * and passing it in current item is the only way using router.
     * Use onCreateNew event or crudService.getBlank() if you do not want to set the current item.
     * @param newItem Specify the new item if you want to initialize it with specific values.
     * In most cases, you should let the system create it with defaults.
     */
    public createNew(newItem: T = null): T {
        if (!newItem) {
            newItem = this.crudService.getBlank();
        }
        if (this.$attrs.onCreateNew) {
            this.onCreateNew({ item: newItem });
        } else {
            this.setCurrent(newItem);
            if (this.detailsUrl) {
                // unique fraction is required for page refresh, if the list and details are in the same page.
                this.$location.url(this.detailsUrl + "new#" + (new Date()).getTime());
            }
        }
        return newItem;
    }

    /** Copies the current item and sets it as the new current */
    public copyCurrent(): void {
        if (this.current) {
            this.setCurrent(this.crudService.copyAsNew(this.current));
        }
    }

    /**
     * Creates a copy of a given item and sets it as current
     * unless onCreateNew is specified => Then you are responsible for doing it yourself.
     * If detailsUrl is specified, the browser is redirectedted there.
     * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
     * and passing it in current item is the only way using router.
     * Use onCreateNew event or crudService.copyAsNew() if you do not want to set the current item.
     * @param copySource The item to copy as new.
     * @returns The new item that is created.
     */
    public copyAsNew(copySource: T): T {
        const copy = this.crudService.copyAsNew(copySource);
        return this.createNew(copy);
    }

    /**
     * Creates a copy of a given item and sets it as current
     * unless onCreateNew is specified => Then you are responsible for doing it yourself.
     * If detailsUrl is specified, the browser is redirectedted there.
     * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
     * and passing it in current item is the only way using router.
     * Use onCreateNew event or crudService.copyAsNew() if you do not want to set the current item.
     * @param copySourceId The id of the item to copy as new.
     * @returns The new item that is created.
     */
    public copyIdAsNew = (copySourceId: string): T => {
      const copySource = this.list.find( (x) => (x as any).id === copySourceId);
      if (copySource) {
        return this.copyAsNew(copySource);
      }
      return null;
    }

    /**
     * Mode for data binding is "singleton" by default.
     * This means that this.current is bound to crudService.current.
     * If ng-model is defined for the component (this.ngModelController),
     * this.current is bound to ngModelController view value (as with form controls).
     */
    public getBindingMode(): "model" | "singleton" {
      return this.model ? "model" : "singleton";
    }

    /** Called by the view when the item is clicked. */
    public listSelect = (selectedItem: T) => {
        if (this.$attrs.onListSelect) {
            this.onListSelect({ item: selectedItem });
        } else if (this.detailsUrl) {
            this.$location.url(this.detailsUrl + ((selectedItem as any).id || (selectedItem as any).rowIndex));
        } else {
            this.setCurrent(selectedItem);
        }
    }

    /** Called by the view when the item (id) is clicked. */
    public listSelectId = (selectedItemId: string) => {
      const selectedItem = this.list.find( (x) => (x as any).id === selectedItemId );
      if (selectedItem) {
        this.listSelect(selectedItem);
      }
  }

    /** Saves the current item. */
    public saveCurrent(): Promise<T> {
      return this.crudService.saveCurrent();
    }

    /**
     * Sets the current item: Either to ng-model view value or crudService.setCurrent().
     * @param item The item to set as Current.
     */
    public setCurrent(item: T) {
      if (this.getBindingMode() === "model") {
        this.model.$setViewValue(item);
      } else {
        this.crudService.setCurrent(item);
      }
    }

    /**
     * Shows the "Are you sure?" dialog and if user clicks OK, deletes the item.
     * Cancels the started payment for the payroll too.
     *
     * @param id Identifier of the item to be deleted.
     * @param confirmMessage Optional custom message for the confirm dialog.
     * If not specified, a generic message is shown.
     * If set to boolean false, the confirm message is not shown at all.
     *
     * @returns Promise that resolves to true if the item is deleted.
     * False, if user cancels and fails if the deletion fails.
     */
    public delete = (id: string, confirmMessage?: string): Promise<boolean> => {
      return this.uiHelpers.showConfirm(confirmMessage || "Haluatko varmasti poistaa tämän tietueen?")
          .then((result: boolean) => {
              if (result) {
                  if (this.$attrs.onDelete) {
                      return this.onDelete({ deleteResult: this.deleteNoConfirm(id)});
                  }
                  if (this.listUrl) {
                      this.$location.url(this.listUrl);
                  }
                  return this.deleteNoConfirm(id);
              } else {
                  return Promise.resolve(false);
              }
          });
    }

    /**
     * Deletes an item without showing the confirm dialog.
     * The method shows the "Please wait..." loader, but does not call onDelete
     * or move the browser to listUrl. The caller should take care
     * of the UX actions after delete if necessary.
     *
     * @param id Identifier of the item to be deleted.
     * @returns Promise that resolves to true (never false). Fails if the deletion fails.
     */
    public deleteNoConfirm(id: string): Promise<boolean> {
        const loading = this.uiHelpers.showLoading("Odota...");
        return this.crudService.delete(id).then(() => {
            loading.dismiss();
            return true;
        });
    }

}
