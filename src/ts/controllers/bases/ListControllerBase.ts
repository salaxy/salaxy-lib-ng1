import * as angular from "angular";

import { UiHelpers } from "../../services";

/**
 * Base class for list controllers that edit a list of items within a parent object (parent property).
 * Abstract class implementation defines the location of list with items of type TListItem.
 * Typically, the list items do not have ID's, they and all operations are synchronous.
 * Save operation is currently done to the parent, it may be later connected also to this controller.
 */
export abstract class ListControllerBase<TParent, TListItem> implements angular.IController {

  /**
   * Original parent value for reset. This value is set when the controller is created.
   */
  public original: TParent;

  /**
   * Event that is fired by the list controller if the user interface indicates that
   * the changes made by the list controller should be committed.
   * Typically this would result to saving of the object.
   * @example <salaxy-calc-rows on-commit="$ctrl.save()"></salaxy-calc-rows>
   */
  public onCommit: (eventData: {
    /** The parent object that contains the list (typically the one that is saved). */
    parent: TParent,
    /** The list that was modified by the control */
    list: TListItem[],
  }) => void;

  /**
   * Event that is fired by the list controller if the user interface indicates that
   * the changes made by the list controller should be reset (canceled).
   * Typically this would result to just closing the control, but some changes to the parent object may be necessary.
   * @example <salaxy-calc-rows on-reset="$ctrl.resetParentThings()"></salaxy-calc-rows>
   */
  public onReset: (eventData: {
    /** The parent object that contains the list  (typically the one that is reset). */
    parent: TParent,
  }) => void;

  /**
   * Event that is fired by the list controller if the user switches from read-only mode to edit.
   * Not all components support read-only vs. edit modes.
   * Typically there is no action needed, but e.g. external menu items may be modified.
   * @example <salaxy-calc-rows on-reset="$ctrl.showEditMenu()"></salaxy-calc-rows>
   */
  public onStartEdit: (eventData: {
    /** The parent object that contains the list  (typically the one that is reset). */
    parent: TParent,
  }) => void;

  /**
   * The edit mode of the component:
   *
   * - edit: Editable, if the calculation is editable.
   * - read-edit: first read-only, but there is an edit button if the calculation is editable.
   * - read-only: Always read-only.
   *
   * Note that not all components support the mode attribute. Also, the default depends on the component logic.
   */
  public mode: "edit" | "read-edit" | "read-only";

  /** Parent object to which the component is bound.  */
  private _parent: TParent;

  private _isInEdit: boolean;

  /**
   * Creates a new ListControllerBase.
   * @param uiHelpers - Salaxy ui helpers service for dialogs etc.
   */
  constructor(
      protected uiHelpers: UiHelpers,
  ) {
      if (!uiHelpers) {
        throw new Error("uiHelpers is undefined in ListControllerBase");
      }
  }

  /**
   * Implement IController by providing onInit method.
   * We currently do nothing here, but if you override this function,
   * you should call this method in base class for future compatibility.
   */
  public $onInit() {
    // init here.
  }

  /**
   * Gets or sets the parent object that contains the list that is being edited.
   * Typically this parent object is an ApiCrudObject: You must implement IService<IParent> for it.
   */
  public get parent(): TParent {
    return this._parent;
  }
  public set parent(value: TParent) {
    this.original =  angular.copy(this.parent);
    this._parent = value;
  }

  /**
   * When overriding the abstract class, you should provide the list within the parent that is edited / viewed.
   * If the parent object is not provided or list is not otherwise available, you should provide null.
   */
  public abstract get list(): TListItem[];

  /** The template URL for the edit dialog. Set to null if you do not want an edit dialog */
  public abstract getEditDialogTemplateUrl(): string;

  /**
   * Possiblity to define additional logic that is passed to the edit dialog as $ctrl.logic.
   * This may contain additional metadata as well as functions.
   */
  public getEditDialogLogic(): any {
    return {};
  }

  /** When overriding the abstract class, provide her the factory method that creates a new list item. */
  public abstract getBlank(): TListItem;

  /** View should call this method if there is a UI component that fires commit (typically save) to the parent. */
  public commit() {
    this.isInEdit = false;
    if (this.onCommit) {
      // Note: When used as component ("&"-binding), there is always a value here even if no function is bound. This if is just for controller use without component.
      this.onCommit({
        parent: this.parent,
        list: this.list,
      });
    }
  }

  /** View should call this method if there is a UI component that fires reset / cancel to the parent. */
  public reset() {
    this.isInEdit = false;
    if (this.onReset) {
      // Note: When used as component ("&"-binding), there is always a value here even if no function is bound. This if is just for controller use without component.
      this.onReset({
        parent: this.parent,
        // TODO: Add possibility to signal that reset has been done (from parent back to child).
      });
    }
    if (this.original) {
      angular.copy(this.original, this.parent);
    }
  }

  /**
   * Returns true if the parent can be edited by this component.
   * Takes into account parent being null, isReadOnly and the "read-only" mode of the component.
   */
  public get isEditable(): boolean {
    if (!this.parent || (this.parent as any).isReadOnly || this.mode === "read-only") {
      return false;
    }
    return true;
  }

  /**
   * Gets or sets the flag telling whether the component is in edit or read-only mode.
   * This is restricted by the mode and isReadOnly of the parent calculation.
   */
  public get isInEdit(): boolean {
    if (!this.isEditable) {
      return false;
    }
    if (this.mode === "edit") {
      return true;
    }
    return !!this._isInEdit;
  }
  public set isInEdit(value: boolean) {
    this._isInEdit = value;
  }

  /**
   * View should call this method if the component is changed from read-only mode to edit mode.
   * Not all components support read-only vs. edit modes.
   */
  public startEdit() {
    if (this.isEditable) {
      this.isInEdit = true;
      this.onStartEdit({
        parent: this.parent,
      });
    }
  }

  /**
   * Deletes an item from list (no confirm etc.)
   * The method shows the "Please wait..." loader, but does not call onDelete
   * or move the browser to listUrl. The caller should take care
   * of the UX actions after delete if necessary.
   *
   * @param item Item to delete.
   * @returns Promise that resolves to true (never false). Fails if the deletion fails.
   */
  public delete(item: TListItem): TListItem[] {
    return this.list.splice(this.list.indexOf(item), 1);
  }

  /**
   * Shows the edit dialog.
   * @param item Item to edit or string "new" for creating a new one.
   * @param isNew Optional way of specifying that the item is a new item (with default values, not yet added to the list).
   * If item is string "new", this parameter will have no effect (will always be true).
   */
  public showEditDialog(item: TListItem | "new", isNew = false) {
    if (!item) {
      throw new Error("showEditDialog called without a parameter.");
    }
    if (item === "new") {
      isNew = true;
      item = this.getBlank();
    }
    this.uiHelpers.openEditDialog(this.getEditDialogTemplateUrl(), item, this.getEditDialogLogic()).then((result) => {
      if (result.action === "ok" || result.action === "ok-no-save" ) {
        if (isNew) {
          this.list.push(result.item);
        }
        if (( isNew || result.hasChanges) && result.action !== "ok-no-save") {
          this.commit();
        }
      } else if (result.action === "delete" || result.action === "delete-no-save") {
        if (!isNew) {
          this.delete(item as TListItem);
          if (result.action !== "delete-no-save" ) {
            this.commit();
          }
        }
      } else {
        // No changes, cancel
      }
      });
  }
}
