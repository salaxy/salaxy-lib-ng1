/**
 * Bindings for the component which uses ListControllerBase.
 */
export class ListControllerBaseBindings {

  /**
   * Parent object that contains the list that is being edited.
   * Typically this parent object is an ApiCrudObject: You must implement IService<IParent> for it.
   */
  public parent = "<";

  /**
   * Event that is fired by the list controller if the user interface indicates that
   * the changes made by the list controller should be committed.
   * Typically this would result to saving of the object.
   * @example <salaxy-calc-rows on-commit="$ctrl.save()"></salaxy-calc-rows>
   */
  public onCommit = "&";

  /**
   * Event that is fired by the list controller if the user interface indicates that
   * the changes made by the list controller should be reset (canceled).
   * Typically this would result to just closing the control, but some changes to the parent object may be necessary.
   * @example <salaxy-calc-rows on-reset="$ctrl.resetParentThings()"></salaxy-calc-rows>
   */
  public onReset = "&";

  /**
   * Event that is fired by the list controller if the user switches from read-only mode to edit (within the component).
   * Typically there is no action needed, but e.g. external menu items may be modified.
   * Not all components support read-only vs. edit modes.
   * @example <salaxy-calc-rows on-reset="$ctrl.showEditMenu()"></salaxy-calc-rows>
   */
  public onStartEdit = "&";

  /**
   * Sets the component edit mode (true/false).
   * This is typically subject to isReadOnly (i.e. if isReadOnly is true on parent, true value is ignored).
   * Edit mode depends on the component UI logic and not all components support editable or non-editable modes.
   */
  public isInEdit = "<";

  /**
   * The edit mode of the component:
   *
   * - edit: Editable, if the calculation is editable.
   * - read-edit: first read-only, but there is an edit button if the calculation is editable.
   * - read-only: Always read-only.
   *
   * Note that not all components support the mode attribute.
   * Also, the default depends on the component logic and component may offer additional modes (e.g. calendar).
   */
  public mode = "@";
}
