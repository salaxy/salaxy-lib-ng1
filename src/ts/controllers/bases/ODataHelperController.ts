import * as angular from "angular";

import { AjaxNg1 } from "../../ajax";
import { ODataQueryController } from "./ODataQueryController";

/**
 * Controller that provides helper methods in components inside ODataQueryController.
 */
export class ODataHelperController  implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Signals that the component should render a table column header with orderby (OData sort) */
  public orderby: string;

  /**
   * Manually defined URL for the nextPageLink.
   * Browser is directed to this URL instead of loading more data from nextPageLink.
   */
  public nextPageUrl: string;

  /** The parent ODataQueryController that contains the list and all the methods. */
  public $odata: ODataQueryController;

  constructor(private ajax: AjaxNg1) {
    // Dependency injection
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
      // Init
  }

  /**
   * Gets the "No items" message if there is no items to show.
   * Null, if there are rows to show.
   * The message changesdepending on the situation: No items, Not initiated, Authentication required.
   */
  public getNoItemsMessage() {
    if (this.$odata.items && this.$odata.items.length > 0) {
      return null;
    }
    if (!this.$odata.options.anon && this.ajax.getTokenStatus() !== "ok") {
      return "Tämä listaus vaatii kirjautumisen. Kirjaudu sisään!";
    }
    if (this.$odata.status === "noInit") {
      // NOTE: This message is for developers => Does not need to be translated.
      return "OData Service not initialized: Set url property or call setUrl().";
    }
    if (this.$odata.status === "loadError") {
      return "Listan lataamisessa tapahtui virhe. Tarkista internet-yhteys, navigoi vasemmasta reunasta jollekin muulle sivulle ja tähän uudelleen. Jos ongelma jatkuu, ota yhteyttä asiakaspalveluun.";
    }
    if (this.$odata.status === "reloading") {
      return null; // Show previous results, just dimmed (opacity 0.7)
    }
    if (this.$odata.status === "initialLoading") {
      return "Ladataan...";
    }
    if (this.$odata.status === "loaded") {
      return "Listassa ei ole yhtään riviä.";
    }
    return "Listassa ei ole yhtään riviä.";
  }

  /** Gets the CSS classes for No items message  */
  public getNoItemsCssClass() {
    if (this.$odata.items && this.$odata.items.length > 0) {
      return "";
    }
    if (this.$odata.status === "noInit" || this.$odata.status === "loadError") {
      // Also invalid token is either noInit or loadError at the moment.
      return "alert alert-danger";
    }
    return "alert alert-default";
  }

  /** Gets the paging view to show */
  public getPagingView(): "clientLimit" | "nextPageLink" | "url" | "allVisible" {
    if (this.$odata.nextPageLink) {
      return this.nextPageUrl ? "url" : "nextPageLink";
    }
    if (this.$odata.options.$top) {
      if (this.$odata.count) {
        if (this.$odata.count > this.$odata.items.length) {
          return this.nextPageUrl ? "url" : "clientLimit";
        } else {
          return "allVisible";
        }
      } else {
        if (!this.$odata.items || this.$odata.items.length < ((this.$odata.options.$skip || 0) + this.$odata.options.$top)) {
          return "allVisible";
        } else {
          return this.nextPageUrl ? "url" : "clientLimit";
        }
      }
    }
  }
}
