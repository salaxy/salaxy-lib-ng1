import * as angular from "angular";

import { ApiCrudObject, CrudApiBase, OData, ODataQueryOptions, ODataResult } from "@salaxy/core";

import { AjaxNg1 } from "../../ajax";
import { CacheService } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * User interface logic for making and displaying an OData query and displaying its results as list.
 */
export class ODataQueryController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["CacheService", "AjaxNg1", "$timeout", "$scope", "$controller", "$http"];

  /**
   * CRUD controller that implements Delete and potentially other methods about the object.
   * If you wish to use this in the view use "as $controllerName" syntax
   * @example
   * <salaxy-odata-table crud-controller="PayrollCrudController as $crud" crud-bindings="{ myProp: 'foobar' }">
   */
  public crudController: string;

  /**
   * Optional bindings for the CRUD controller. All properties are set to crudController before calling init.
   * @example
   * <salaxy-odata-table crud-controller="PayrollCrudController as $crud" crud-bindings="{ myProp: 'foobar' }">
   */
  public crudBindings: { [key: string]: any; };

  /** OData endpoint URL */
  public url: string;

  /** The status of the data loading */
  public _status: "noInit" | "initialLoading" | "loaded" | "reloading" | "loadError" = "noInit";

  /**
   * If true, the items collection has data.
   * This means that items is not null. It may be an empty array though if there is nothing coming from data source.
   */
  public hasData: boolean;

  /** Query options for the next query */
  public options: ODataQueryOptions;

  /** Items once loaded from the server. This is an empty array when the loading starts. */
  public items: any[];

  /** Total count of the items (server-side/storage) if updated in the previous query. */
  public count: number | null = null;

  /** Link for the next page if available in the previous query */
  public nextPageLink: string = null;

  /** Data passed from the parent to the component / view */
  public data: any;

  /** If readOnly is true, edit, copy etc. buttons from the new list view are hidden if readOnly property is supported */
  public readOnly: any;

  /** Selected search items */
  public selectedItems: any[];

  /** If set, uses caching to store cache result between controller loads. */
  public cacheKey: string;

  /** Time in milliseconds when the data was loaded.  */
  public loadedAt: number;

  /** Optional OData reader. Defaults to AjaxNg1.getJSON -method */
  public reader: {
     /** Gets a JSON-message from server using the API */
     getJSON: (method: string ) => Promise<any>,
  } = null;

  /**
   * Function that is called when user selects an item in the list.
   * Note that the event is called only in the selections (single and multi). Not when there is a link to details view in a link list.
   * Function has the following locals: value: true/false, item: the last selected/unselected item, allItems: Array of all currently selected items.
   * @example <salaxy-worker-list mode="select" on-list-select="$ctrl.updateFromEmployment(item.id)"></salaxy-worker-list>
   */
  public onListSelect: (params: {
    /** In multi-select mode, the value may be true (selected) or false (unselected). */
    value: true | false,
    /** Item that was last selected / deselected. */
    item: any,
    /** All items that are currently selected. */
    allItems: any[],
  }) => void;

  /** Function that is called when the data has been loaded and set as values of the items. */
  public onDataLoaded: (params: {
    /** If true, the load was append to previous values. */
    append: boolean,
    /** The resulted data */
    result: ODataResult<any>,
  }) => void;

  /** HACK: Extract interface */
  private $crudController: ApiCrudObjectController<ApiCrudObject>;

  private filterTimeout: any;

  /**
   * Creates a new controller instance
   * @ignore
   */
  constructor(

    protected cacheService: CacheService,
    private ajax: AjaxNg1,
    private $timeout: angular.ITimeoutService,
    private $scope: angular.IScope,
    private $controller: angular.IControllerService,

    private $http: angular.IHttpService,
  ) { }

  /**
   * Implement IController
   */
  public $onInit = () => {

    if (!this.reader) {
      this.reader = this.ajax;
    }

    if (!this.options) {
      this.options = {
        $count: false,
      };
    }
    if (this.crudController) {
      this.$crudController = this.$controller(this.crudController, { $scope: this.$scope });
      this.$crudController.odataController = this;
      if (this.crudBindings) {
        Object.keys(this.crudBindings).forEach((key) => {
          this.$crudController[key] = this.crudBindings[key];
        });
      }
      this.$crudController.$onInit();
    }
    this.selectedItems = this.selectedItems || [];
    this.items = this.items || [];

    this.reload();
  }

  /**
   * Cleaning the timeout.
   */
  public $onDestroy = () => {
    if (angular.isDefined(this.filterTimeout)) {
      this.$timeout.cancel(this.filterTimeout);
    }
  }

  /**
   * Gets the status of the data if it is being loaded from a remote server.
   */
  public get status(): "noInit" | "initialLoading" | "loaded" | "reloading" | "loadError" {
    return this._status;
  }

  /**
   * Set the URL and start reload.
   * @param url OData enndpoint
   * @param queryOptions Optional initial query options.
   */
  public setUrl(url: string, queryOptions?: ODataQueryOptions) {
    this.url = url;
    this.options = queryOptions || this.options;
    this.reload();
  }

  /** Sets query options. Does not trigger reload.
   * @param queryOptions Query options.
   */
  public setQueryOptions(queryOptions?: ODataQueryOptions) {
    this.options = queryOptions || this.options;
  }

  /**
   * Method to set the sort by for a column
   * @param column Column to sort by
   * @param direction Optional direction is either "asc" for ascending or "desc" for descending.
   * If direction is not set, will reverse the sort order or ascending being the default.
   */
  public orderBy(column: string, direction?: "asc" | "desc") {
    if (!direction) {
      const currentOrder = this.getColumnOrder(column);
      direction = currentOrder === "asc" ? "desc" : "asc";
    }
    this.options.$orderby = column + " " + direction;
    this.options.$skip = 0; // Consider not resetting this, when implementing client-side paging.
    this.reload();
    return;
  }

  /**
   * Sets a column filter and reloads the data set.
   * @param column Name of the column.
   * Set the column name as null, if you want to clear all filters.
   * @param value Value of the filter. Null clears the filter (not filter-by-null).
   * TODO: This could potententially be a comma separated value if requirement comes up.
   * @param operation Possibility to negate the equality.
   * @param andFilters If set, adds these additional filters (with and) to the new primary filter.
   */
  public setFilter(column: string | null, value: string, operation: "eq" | "ne" = "eq", andFilters?: string): void {
    if (column && value) {
      this.options.$filter = `${column} ${operation || "eq"} '${value}'`;
      if (andFilters) {
        this.options.$filter += " and (" + andFilters + ")";
      }
    } else {
      this.options.$filter = andFilters || null;
    }
    this.options.$skip = 0;
    this.reload();
  }

  /**
   * Gets the text indicating the time that has passed since the data was loaded.
   */
  public getLoadedTime(unit: "min" | "10sec" | "sec" = "min"): number {
    if (!this.loadedAt) {
      return null;
    }
    const ms = new Date().getTime() - this.loadedAt;
    switch (unit) {
      case "min":{
        const minutes = Math.floor(ms / 60000);
        if (minutes < 1) {
          return null;
        }
        return minutes;
      }
      case "10sec":
        return Math.floor(ms / 10000) * 10;
      case "sec":
        return Math.floor(ms / 1000);
    }
  }

  /** Clears the cache and reloads from the server. */
  public refresh() {
    this.cacheService.clear(this.cacheKey);
    this.reload();
  }

  /** Clears the entire cache - all keys and reloads. */
  public refreshClearAll() {
    this.cacheService.clearAllKeys();
    this.reload();
  }

  /**
   * Sets an item as selected.
   * @param item Item to set as selected
   * @param value Selected true / false or "toggle" to toggle to other value.
   */
  public setSelected(item: any, value: true | false | "toggle" = true) {
    // TODO: This now only supports items with id. Add other identifiers if necessary.
    const ix = this.selectedItems.findIndex((x) => x.id === item.id);
    const exists = ix >= 0;
    if (exists) {
      this.selectedItems.splice(ix, 1);
    }
    switch (value) {
      case true:
        this.selectedItems.push(item);
        break;
      case false:
        // Already done
        break;
      case "toggle":
        if (!exists) {
          this.selectedItems.push(item);
        }
        break;
    }
    this.onListSelect({
      value: value === "toggle" ? !exists : value,
      item,
      allItems: this.selectedItems,
    });
  }

  /**
   * Returns true if the item is selected.
   * @param item Item to check for selected value.
   */
  public isSelected(item: any) {
    return this.selectedItems.findIndex((x) => x.id === item.id) >= 0;
  }

  /**
   * Gets a column ordering for the specified column.
   * @param columnName Name of the column
   * @returns Column order "asc" or "desc" if column is being sorted. Otherwise null.
   */
  public getColumnOrder(columnName: string): "asc" | "desc" | null {
    if (!columnName) {
      return null;
    }
    if (!(this.options.$orderby || "").trim()) {
      return null;
    }
    const columns = this.options.$orderby.split(",");
    for (const colSpec of columns) {
      const colArr = colSpec.trim().split(/\s+/);
      if (colArr[0].trim().toLowerCase() === columnName.trim().toLowerCase()) {
        return colArr.length > 1 && colArr[1].trim().toLowerCase() === "desc" ? "desc" : "asc";
      }
    }
  }

  /**
   * Loads items on next page
   * @param append If true, appends the data of the next page link to the current set of items
   */
  public loadNextPage(append: boolean) {
    if (this.nextPageLink) {
      const queryString = this.nextPageLink.indexOf("?");
      if (queryString >= 0) {
        const searchParams = new URLSearchParams(this.nextPageLink.substr(queryString));
        if (searchParams.get("$skip")) {
          this.options.$skip = Number(searchParams.get("$skip"));
        }
      }
      this.loadData(this.options, append);
    }
  }

  /** Called by the view when the search text changes */
  public searchChanged() {
    this.options.$skip = 0;
    this.reloadWithDelay(500);
  }

  /**
   * Starts the reload process.
   * @param delay Loading occurs asyncronously after the specified delay
   */
  public reloadWithDelay(delay: number): void {
    if (!this.getUrl()) {
      return;
    }
    if (angular.isDefined(this.filterTimeout)) {
      this.$timeout.cancel(this.filterTimeout);
    }
    this.filterTimeout = this.$timeout(() => {
      this.loadData(this.options, false);
    }, delay);
  }

  /**
   * Starts the reload process.
   * @returns A promise of OData result as returned by setValues() method.
   */
  public reload(): Promise<ODataResult<any>> {
    if (!this.getUrl()) {
      return;
    }
    if (angular.isDefined(this.filterTimeout)) {
      this.$timeout.cancel(this.filterTimeout);
    }
    return this.loadData(this.options, false);
  }

  /** Gets the URL for the server request using the current url and queryOptions */
  public getUrl() {
    const baseUrl = this.url || (this.$crudController ? this.$crudController.odataServiceUrl : null);
    if (!baseUrl) {
      return null;
    }

    return OData.getUrl(baseUrl, this.options);
  }

  /** If true, the component is in the middle of loading data. */
  public get isLoading() {
    switch (this.status) {
      case "initialLoading":
      case "reloading":
        return true;
      case "loaded":
      case "loadError":
      case "noInit":
      default:
        return false;
    }
  }

  /**
   * Gets the CSS classes for OData table container.
   * Main function is to get the `salaxy-loading class`.
   * Also renders `salaxy-component salaxy-odata-table` for convenience
   */
  public getCssClass() {
    return "salaxy-component salaxy-odata-table"
      + " salaxy-odata-table-status-" + this.status
      + (this.isLoading ? " salaxy-odata-table-loading" : "");
  }

  /**
   * Sets the values to items from an OData result.
   * @param data Data as returned from the Ajax component. This is expected being either ODataResult or array of items.
   * @param append If true, appends the result to existing values. Used particularly with nextPageLink if this is used fo "Load more", not "Next page".
   * @returns The data harmonized as OData result.
   */
  public setValues(data: any, append = false): ODataResult<any> {
    const result = OData.getODataResult<any>(data);

    this.items = this.items || [];
    if (!append && this.items.length > 0) {
       // remove values
       this.items.splice(0, this.items.length);
    }
    this.items.push(...result.items);
    this.count = result.count;
    this.nextPageLink = result.nextPageLink;
    this.hasData = true;
    this.loadedAt = data.$loaded || new Date().getTime(); // Cache service injects loaded time in $loaded.
    if (this.onDataLoaded) {
      this.onDataLoaded({ append, result });
    }
    return result;
  }

  /**
   * Calls a promise updating the status on the controller.
   * @param loadAction The promise that executes the loading from server.
   */
  public setStatus(loadAction: Promise<ODataResult<any>>): Promise<ODataResult<any>> {
    switch (this._status) {
      case "noInit":
        this._status = "initialLoading";
        break;
      case "initialLoading":
        // TODO: We should somehow cancel the first call and run only the last one.
        this._status = "initialLoading";
        break;
      case "loadError":
        this._status = (this.items ? "reloading" : "initialLoading");
        break;
      case "loaded":
        this._status = "reloading";
        break;
      case "reloading":
        // TODO: We should somehow cancel the first call and run only the last one.
        this._status = "reloading";
        break;
      default:
        throw Error("Unexpected status: " + this._status);
    }
    return loadAction.then((data) => {
      this._status = "loaded";
      return data;
    })
      .catch((error) => {
        this._status = "loadError";
        return null;
      });
  }

  /**
   * Returns the current crud api.
   */
  public getApi(): CrudApiBase<any> {
    if ( this.$crudController ) {
      return this.$crudController.getApi();
    }
    return null;
  }

  /**
   * Makes a http call to server using the curre
   * @param options - Options based on which the OData query is created.
   * @param append - If true, appends the result to the data.
   * @returns A promise of OData result as returned by setValues() method.
   */
  protected loadData(options: ODataQueryOptions, append = false): Promise<ODataResult<any>> {
    // HACK: Go though this logic: When to use the strongly typed API and how to enable hard-coded url.
    const url = this.getUrl();
    if (!append && this.cacheService.hasData(this.cacheKey, url)) {
      // TODO: Consider should we support append in cache as well?
      return this.setStatus(Promise.resolve(this.setValues(this.cacheService.getData(this.cacheKey, url), append)));
    }
    if (this.url) {
      // URL has been overriden
      return this.setStatus(this.reader.getJSON(url).then((data) => {
        this.cacheService.setData(this.cacheKey, url, data, 5 * 60);
        return this.setValues(data, append);
      }));
    }

    if (this.$crudController) {
      return this.setStatus(this.$crudController.getOData(options).then((data) => {
        this.cacheService.setData(this.cacheKey, url, data, 5 * 60);
        return this.setValues(data, append);
      }));
    }

    throw Error("No url given for OData queries");
  }
}
