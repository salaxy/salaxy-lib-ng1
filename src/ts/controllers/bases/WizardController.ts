﻿import * as angular from "angular";

import { WizardService, WizardStep } from "../../services";

/**
 * A generic Wizard controller for rendering the Wizard UI
 */
export class WizardController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$scope", "WizardService", "data"];

    /** Styling classes for columns. */
    public colWidthClasses = ["col-sm-12", "col-sm-12", "col-sm-6", "col-sm-4", "col-sm-3", "col-sm-fifth", "col-sm-2"];

    /** Path for the view that is shown in the wizard */
    public viewPath: string;

    /** Path to the buttons view that is shown in the footer of the wizard */
    public buttonsView?: string;

    /**
     * Creates a new WizardController
     * @param $scope - The Angular scope
     * @param wizardService - Maintains the state of the wizard
     * @param data - Any data to the controller
     *
     * @ignore
     */
    constructor(
        protected $scope: angular.IScope,
        protected wizardService: WizardService,
        protected data: any = null,
    ) {
    }

    /**
     * Implement IController
     */
    public $onInit() {
        //
    }

    /** Gets all the steps in the Wizard */
    public get steps(): WizardStep[] {
        return this.wizardService.getSteps();
    }

    /** Gets the number of the currently active step */
    public get step(): number {
        return this.wizardService.activeStepNumber;
    }
    /** Sets the number of the currently active step */
    public set step(stepNumber: number) {
        this.wizardService.activeStepNumber = stepNumber;
    }

    /** The currently selected step object */
    public get currentStep(): WizardStep {
        return this.wizardService.getCurrentStepObject();
    }

    /** Active styling properties */
    public get style() {
        return {
            colWidthClass: this.colWidthClasses[this.steps.length],
        };
    }

    /** Returns true if goNext is enabled  */
    public get canGoNext(): boolean {
        if (this.steps.length > this.step) {
            if (this.steps[this.step] && !this.steps[this.step].disabled) {
                return true;
            }
        }
        return false;
    }

    /** Returns true if goNext is enabled  */
    public get canGoPrevious(): boolean {
        if (this.step > 1 && !this.steps[this.step - 2].disabled) {
            return true;
        }
        return false;
    }

    /**
     * Navigates to the next step if possible
     */
    public goNext() {
        if (this.canGoNext) {
            this.step++;
        }
    }

    /** Navigates to the previous step if possible */
    public goPrevious() {
        if (this.canGoPrevious) {
            this.step--;
        }
    }
}
