﻿import * as angular from "angular";

import { Brand, Calculation } from "@salaxy/core";

/**
 * Controller for employer and worker charts using angular-chart
 */
export class CalcChartController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$filter"];

  /** Role for which the chart is rendered: employer (default) or worker */
  public role: "employer" | "worker";

  /** Calculation that is rendered. */
  public calc: Calculation;

  /** Y axis scale */
  public scaleYAxis: boolean;

  /** Chart type: "pie" and "bar" (default) are currently supported. */
  public chartType: "pie" | "bar";

  /** Data that is shown in the bar chart */
  public seriesData;

  /** Series labels */
  public seriesLabels;

  /** Pie chart data */
  public pieData;

  /** Colors for the chart. */
  public chartColors = this.getChartColors();

  /** Bar chart options */
  public chartOptions = {
    tooltips: {
      enabled: true,
      bodySpacing: 6,
      callbacks: {
        label: (tooltipItem, data) => {
          const seriesLabel = data.datasets[tooltipItem.datasetIndex].label;
          return seriesLabel;
        },
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true,
        ticks: { min: 0, max: 100 },
      }],
    },
    events: ["mousemove", "click"],
    title: {
      fullWidth: false,
    },
  };

  /** Pie chart options */
  public pieChartOptions = {
    tooltips: {
      enabled: true,
      mode: "single",
      bodySpacing: 6,
      callbacks: {
        label: (tooltipItem, data) => {
          const label = data.labels[tooltipItem.index];
          return label;
        },
      },
    },
    events: ["mousemove", "click"],
  };

  // Colors from component bindings
  private colors: string;

  private lastData: any;

  /**
   * Creates a new CalcChartController
   * @param $filter - Angular $filter service
   * @ignore
   */
  constructor(
    private $filter: angular.IFilterService,
  ) {

  }

  /** Initializes the controller. */
  public $onInit() {
    this.role = this.role || "employer";
    this.chartType = this.chartType || "bar";
    this.checkColors();
  }

  /** Gets the data depending on the type */
  public getData() {
    this.checkChanges();
    if (this.chartType === "pie") {
      return this.pieData;
    } else {
      return this.seriesData;
    }
  }

  /** Checks if there is changes since the last rendering and if so recreates the chart. */
  public checkChanges() {
    const data = this.getChartData(this.calc) as any;
    data.type = this.chartType;
    data.role = this.role;
    if (!angular.equals(data, this.lastData)) {
      this.lastData = data;
      this.createChart(data);
    }
  }

  /**
   * Executes everything needed for chart generation:
   * fetched the current calculation, stores needed numbers from that calculation,
   * checks colors, sets labels and sets chart data
   */
  private createChart(data): void {
    // Setting max y axis limits
    if (this.scaleYAxis) {
      if (data.workerChartScale > data.employerChartScale) {
        this.setChartYAxisLimit(data.workerChartScale);
      } else {
        this.setChartYAxisLimit(data.employerChartScale);
      }
    } else {
      if (this.role === "worker") {
        this.setChartYAxisLimit(data.workerChartScale);
      } else {
        this.setChartYAxisLimit(data.employerChartScale);
      }
    }
    if (this.role === "worker") {
      this.createChartWorker(data);
    } else {
      this.createChartEmployer(data);
    }
  }

  /**
   * Checks if any overriding colors have been given as an attribute
   * Checks if colors are valid hex colors, and then replaces default colors
   */
  private checkColors() {
    if (this.colors != null) {
      const colorList = this.colors.split(",");

      for (const color of colorList) {
        const colorNumber = Number(color);
        const regex = new RegExp("(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)");
        const trimmedColor = colorList[colorNumber].trim();
        const isColorHex = regex.test(trimmedColor);

        if (colorNumber < 4 && isColorHex) {
          const tempColor = this.chartColors[colorNumber];
          tempColor.backgroundColor = colorList[colorNumber];
          tempColor.pointBackgroundColor = colorList[colorNumber];
          tempColor.pointHoverBackgroundColor = colorList[colorNumber];
          tempColor.borderColor = colorList[colorNumber];
          tempColor.pointBorderColor = colorList[colorNumber];
          tempColor.pointHoverBorderColor = colorList[colorNumber];
          this.chartColors[colorNumber] = tempColor;
        }
      }
    }
  }

  /**
   * Calculates and sets a proper Y axis max value for a given value
   * @param value The number of the highest bar in chart
   */
  private setChartYAxisLimit(value: number) {
    const log = Math.round(Math.log10(value));
    const times = Math.ceil(value / Math.pow(10, log - 1));
    const newValue = Math.pow(10, log - 1) * times;

    this.chartOptions.scales = {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true,
        ticks: { min: 0, max: newValue },
      }],
    };
  }

  private getChartData(calc: Calculation) {
    if (!calc || !calc.result) {
      return {};
    }
    const result = calc.result;
    const data = {
      // Employer numbers
      totalGrossSalary: result.totals.totalBaseSalary,
      salaryAdditions: result.totals.totalGrossSalary -  result.totals.totalBaseSalary,
      allSideCosts: result.employerCalc.allSideCosts,
      finalCost: result.employerCalc.finalCost,

      // Worker numbers
      salaryPayment: result.workerCalc.salaryPayment,
      tax: result.workerCalc.tax,
      workerSideCosts: result.workerCalc.workerSideCosts,
      benefits: result.workerCalc.benefits,
      totalExpenses: result.totals.totalExpenses,
      totalWorkerPayment: result.workerCalc.totalWorkerPayment,

      employerChartScale: null,
      workerChartScale: null,
    };

    // Calculating max y axis limits
    data.employerChartScale = data.totalGrossSalary + data.salaryAdditions + data.allSideCosts + data.totalExpenses;
    data.workerChartScale = data.salaryPayment + data.tax + data.workerSideCosts + data.benefits + data.totalExpenses;
    return data;
  }

  /**
   * Sets the relevant employer data as chart data
   */
  private createChartEmployer(data) {
    this.pieData = [
      data.totalGrossSalary,
      data.salaryAdditions,
      data.allSideCosts,
      data.totalExpenses,
    ];

    this.seriesData = [
      [data.totalGrossSalary],
      [data.salaryAdditions],
      [data.allSideCosts],
      [data.totalExpenses],
    ];

    this.seriesLabels = [
      "Palkka " + this.formatCurrency(data.totalGrossSalary),
      "Palkan lisät " + this.formatCurrency(data.salaryAdditions),
      "Sivukulut " + this.formatCurrency(data.allSideCosts),
      "Kulut " + this.formatCurrency(data.totalExpenses),
    ];
  }

  /**
   * Sets the relevant worker data as chart data
   */
  private createChartWorker(data) {
    this.pieData = [
      data.salaryPayment,
      data.tax,
      data.workerSideCosts,
      data.benefits + data.totalExpenses,
    ];
    this.seriesData = [
      [data.salaryPayment],
      [data.tax],
      [data.workerSideCosts],
      [data.benefits + data.totalExpenses],
    ];
    this.seriesLabels = [
      "Työntekijän nettopalkka " + this.formatCurrency(data.salaryPayment),
      "Ennakonpidätys " + this.formatCurrency(data.tax),
      "Työntekijän sivukulut " + this.formatCurrency(data.workerSideCosts),
      "Kulut ja edut " + this.formatCurrency(data.benefits + data.totalExpenses),
    ];
  }

  private formatCurrency(value: number) {
    // TODO: Can we use a simpler (more effective) format function here.
    return this.$filter("currency")(value, "", 2) + " €";
  }

  /** Default chart colors */
  private getChartColors() {
    const getColorSet = (code: string) => {
      const color = Brand.getBrandColor(code as any, "rgba");
      return {
        backgroundColor: color,
        pointBackgroundColor: color,
        pointHoverBackgroundColor: color,
        borderColor: color,
        pointBorderColor: color,
        pointHoverBorderColor: color,
      };
    };
    return [
      getColorSet("info"),
      getColorSet("success"),
      getColorSet("warning"),
      getColorSet("danger"),
    ];
  }
}
