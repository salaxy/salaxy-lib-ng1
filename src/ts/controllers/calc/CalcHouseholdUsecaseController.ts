import * as angular from "angular";

import {
  Calculation, CalculationRowType, CalculationRowUnit, Calculations, CalculatorLogic, DateRange,
  HouseholdUsecase, HouseholdUsecasesLogic, UserDefinedRow, WorkerAccount, Workers, CalcWorktime, Dates, Translations,
} from "@salaxy/core";

import { UiHelpers
} from "../../services";

/**
 * Handles the user interaction of Worktime within the Calculator.
 * The Worktime contains the logic for fetching holidays and absences
 * for that particular period and adding calculation rows for them if necessary.
 */
export class CalcHouseholdUsecaseController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "UiHelpers",
    "Calculations",
    "Workers",
  ];

    /** User interface shortcuts for period selection */
    public periodShortcuts: {
      /** Text for current month. */
      monthCurrent: string,
      /** Text for last month. */
      monthLast: string,
      /** Text for other month. */
      monthOther: string,
      // monthMulti: "Monta kuukautta",
      /** Text for two weeks. */
      weeks2: string,
      /** Text for half a month */
      monthHalf: string,
      /** Text for other period. */
      other: string,
    };
  /** The calculation that the controller edits. */
  public calc: Calculation;

  /** Alternative data binding: creates a calculation based on Worker defaults. */
  public worker: WorkerAccount;

  /**
   * Shows the Save and Reset buttons.
   * Currently, this is only enabled for WorkerAccount data binding, but mayu later be enabled for calc.
   */
  public showSave: boolean;

  /**
   * If true, indicates that we are editing salary template information for employment relation:
   * Not a concrete calculation.
   */
  public isEmployment: boolean;

  /** If true, the UI is in read-only mode. */
  public isReadOnly: boolean;

  /** If there is a validation error in period start, it is added here. */
  public periodStartDateError = null;

  /**
   * Worktime data for the period fetched from the server
   */
  public worktime: CalcWorktime;


  /** The tree for selecting the calculation usecase (household) */
  private tree = HouseholdUsecasesLogic.getUsecaseTree();

  private _periodStartDate;

  private _dateRange: DateRange;


  /**
   * Creates a new CalcHouseholdUsecaseController
   * @ignore
   */
  constructor(
    private uiHelpers: UiHelpers,
    private calcApi: Calculations,
    private workersApi: Workers,

  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    if (!this.calc && this.worker) {
      this.resetEmployment();
    }
    this.initDateRanges();

  }


  /**
   * The use case that we are editing
   */
  public get usecaseData(): HouseholdUsecase {
    return this.calc.usecase.data as HouseholdUsecase;
  }

  /** Gets the view template that is used for showing the usecase details part. */
  public getTemplate() {
    let group = "other";
    switch (this.usecaseData.uri) {
      case HouseholdUsecasesLogic.knownUseCases.childCare:
      case HouseholdUsecasesLogic.knownUseCases.mll:
        group = "childCare";
        break;
      case HouseholdUsecasesLogic.knownUseCases.cleaning:
        group = "cleaning";
        break;
      case HouseholdUsecasesLogic.knownUseCases.construction:
        group = "construction";
        break;
      case HouseholdUsecasesLogic.knownUseCases.santaClaus:
      case HouseholdUsecasesLogic.knownUseCases.other:
      default:
        group = "other";
        break;
    }
    return `salaxy-components/calc/usecases/household-${group}.html`;
  }

  /**
   * Gets or sets the isHouseholdDeductible within the usecase.
   * Currently, this is part of the usecase specification and will be set to Salary (household properties in V03) from there.
   */
  public get isHouseholdDeductible(): boolean {
    return this.usecaseData.isHouseholdDeductible;
  }
  public set isHouseholdDeductible(value: boolean) {
    this.usecaseData.isHouseholdDeductible = value;
  }

   /** Gets the current calculation object */
   public get currentCalc(): Calculation {
    return this.calc;
  }


  /**
   * Submit the usecase data and recalculate
   */
  public submitHouseholdUsecase() {
    const loader = this.uiHelpers.showLoading("Lasketaan...");
    HouseholdUsecasesLogic.applyUseCase(this.calc);
    this.calcApi.recalculate(this.calc).then((data) => {
      angular.copy(data, this.calc);
      loader.dismiss();
    });
  }

  /** Saves the usecase to employment */
  public saveEmployment() {
    const loader = this.uiHelpers.showLoading("Tallennetaan...");
    this.isReadOnly = true;
    this.updateWorkerAccount().then(() => {
      this.workersApi.save(this.worker).then(() => {
        loader.dismiss();
      });
    });
  }

  /**
   * Does recalculation on the usecase and and applies it to worker account.
   * @returns The updated worker account. The changes are also applied to the worker property.
   */
  public updateWorkerAccount(): Promise<WorkerAccount> {
    HouseholdUsecasesLogic.applyUseCase(this.calc);
    return this.calcApi.recalculate(this.calc).then((calcResult) => {
      this.worker.employment.usecase = calcResult.usecase;
      this.worker.employment.usecase.data.copyToCalc = true;
      this.worker.employment.work.salaryDefaults = calcResult.rows;
      this.worker.employment.work.occupationCode = calcResult.info.occupationCode;
      return this.worker;
    });
  }

  /** Resets the usecase from employment */
  public resetEmployment() {
    this.isEmployment = true;
    this.calc = CalculatorLogic.getBlank();
    this.calc.usecase = {};
    angular.copy(this.worker.employment.work.salaryDefaults, this.calc.rows);
    angular.copy(this.worker.employment.usecase, this.calc.usecase);
    if (this.calc.usecase?.uri) {
      this.isReadOnly = true;
      HouseholdUsecasesLogic.applyUseCase(this.calc);
    } else {
      this.isReadOnly = false;
    }
  }

  /** Starts the edit mode if the component is in read-only mode.  */
  public startEdit() {
    this.isReadOnly = false;
  }

  /**
   * The salary row is the first row is the first row in the rows list
   */
  public get salaryRow(): UserDefinedRow {
    return CalculatorLogic.getSalaryRow(this.calc);
  }

  /** Returns true if the usecase has been selected. */
  public get isUsecaseSelected(): boolean {
    return !!this.calc?.usecase?.uri;
  }

  /** Gets the label for price input */
  public get priceLabel() {
    switch (this.salaryRow.rowType) {
      case CalculationRowType.Compensation:
        return "Työkorvaus";
      case CalculationRowType.Salary:
        return "Kertakorvaus";
      case CalculationRowType.HourlySalary:
        return "Tuntipalkka";
      case CalculationRowType.MonthlySalary:
        return "Kuukausipalkka";
      case CalculationRowType.TotalEmployerPayment:
        return "Kokonaiskustannus";
      case CalculationRowType.TotalWorkerPayment:
        return "Nettopalkka";
      default:
        return "Hinta";
    }
  }
  /**
   * Select and set usecase in UI
   */
  public selectUsecase(usecase) {
    this.calc.rows = [];
    HouseholdUsecasesLogic.setUsecase(this.calc, usecase);
  }

  /** Resets the price and amount when row type is changed. */
  public changeRowType() {
    this.salaryRow.price = null;
    if (this.salaryRow.rowType === CalculationRowType.HourlySalary) {
      this.salaryRow.count = null;
      this.salaryRow.unit = CalculationRowUnit.Hours;
    } else {
      this.salaryRow.count = 1;
    }
  }

  /**
   * Resets the usecase to empty: Shows the usecase selection again.
   */
  public resetUsecase() {
    this.calc.usecase = {};
  }

  /** Getter and setter for Calculation daterange in compatible format for new DateRange component. */
  public get dateRange(): DateRange {
    const info = (this.calc || {}).info;
    const framework = (this.calc || {}).framework;
    if (!this._dateRange) {
      this._dateRange = {
        start: info.workStartDate,
        end: info.workEndDate,
        daysCount: framework.numberOfDays,
      };
    }
    return this._dateRange;
  }
  public set dateRange(value: DateRange) {
    this._dateRange = value;
  }

  /**
   * Called when the date range changes.
   * @param noPeriodStartDateUpdate If true, the _periodStartDate is not updated.
   * Should be true if the change is triggered by that input to avoid UI flickering.
   */
  public dateRangeChange(noPeriodStartDateUpdate = false) {
    const info = (this.currentCalc || {}).info;
    const framework = (this.currentCalc || {}).framework;
    info.workStartDate = this.dateRange.start;
    if (!noPeriodStartDateUpdate) {
      this._periodStartDate = Dates.format(this.dateRange.start, "D.M.", null);
    }
    info.workEndDate = this.dateRange.end;
    framework.numberOfDays = this.dateRange.daysCount;
  }

  /**
   * Gets or sets the period start date, which is bound to editable input in case the period type
   * is not "other" (date the range / calendar component)
   */
  public get periodStartDate(): string {
    return this._periodStartDate;
  }
  public set periodStartDate(value: string) {
    this._periodStartDate = value;
    const parts = (value || "").split(".");
    if (parts.length < 2 || parts.length > 3) {
      this.periodStartDateError = "Syötä muodossa 'pv.kk.'";
      return;
    }
    let year = (parts.length === 3) ? Number((parts[2] || "").trim()) : null;
    if (!year || year < 2019 || year > 2100) {
      year = Number(this.dateRange.start.substr(0, 4));
    }
    const date = Dates.getDate(year, Number(parts[1]), Number(parts[0]));
    if (date) {
      this.periodStartDateError = null;
      switch (this.periodShortcut) {
        case "monthCurrent":
        case "monthLast":
        case "monthOther":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(1, "month").subtract(1, "day"));
          break;
        case "weeks2":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(2, "week").subtract(1, "day"));
          break;
        case "monthHalf":
          if (Dates.getMoment(date).date() === 15 || Dates.getMoment(date).date() === 16) {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).endOf("month"));
          } else {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(14, "day"));
          }
          break;
        case "other":
          this.dateRange = Dates.getDateRange(date, date);
          break;
      }
      this.dateRangeChange(true);
    } else {
      this.periodStartDateError = `${value} ei ole päivämäärä.`;
    }
  }

  /**
   * Gets the user interface shortcut for the period type:
   * Different user interfaces are shown depending on the period shortcut.
   */
  public get periodShortcut(): "monthCurrent" | "monthLast" | "monthOther" | "weeks2" | "monthHalf" | "other" {
    if (!this.dateRange.start || !this.dateRange.end) {
      return null;
    }
    if (this.dateRange.start === Dates.getDate("today", "today", 1)
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().endOf("month"))) {

      return "monthCurrent";
    }
    if (this.dateRange.start === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month"))
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month").endOf("month"))) {
      return "monthLast";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"))) {
      return "monthOther";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(2, "week").subtract(1, "day"))) {
      return "weeks2";
    }

    // if the range is 15 (14) days or
    // starts 15th or 16th and ends month end
    if ((Dates.getDuration(this.dateRange.start, this.dateRange.end).days() === 14)
      || (Dates.getMoment(this.dateRange.start).date() === 15 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))
      || (Dates.getMoment(this.dateRange.start).date() === 16 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))) {
      return "monthHalf";
    }
    return "other";
  }
  public set periodShortcut(value: "monthCurrent" | "monthLast" | "monthOther" | "weeks2" | "monthHalf" | "other") {
    const today = Dates.getTodayMoment();
    this.periodStartDateError = null;
    switch (value) {
      case "monthCurrent":
        this.dateRange.start = Dates.getDate("today", "today", 1);
        this.dateRange.end = Dates.asDate(Dates.getTodayMoment().endOf("month"));
        break;
      case "monthOther":
        if (today.date() === 1) {
          today.add(1, "day"); // Move to tomorrow so that the selection is monthOther and not monthCurrent.
        }
        this.dateRange.start = Dates.asDate(today);
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"));
        break;
      case "monthLast":
        this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month"));
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        break;
      case "weeks2":
        this.dateRange.start = Dates.asDate(today.clone().startOf("week").add(-2, "week").add(1, "day"));
        this.dateRange.end = Dates.asDate(today.clone().startOf("week"));
        break;
      case "monthHalf":

        // TODO Check this
        if (today.date() < 16) {
          // 16 - (28,29,30,31)
          this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month").add(15, "day"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        } else {
          // 1 - 15
          this.dateRange.start = Dates.asDate(today.clone().startOf("month"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(14, "day"));
        }
        break;
      case "other":
        this.dateRange.start = Dates.asDate(today.clone().add(-1, "day"));
        this.dateRange.end = Dates.asDate(today.clone());
        break;
    }
    this.dateRange = Dates.getDateRange(this.dateRange.start, this.dateRange.end);
    this.dateRangeChange();
  }
  private initDateRanges() {
    const currentMonth = Dates.getMonth(Dates.getToday());
    this.periodShortcuts = {
      monthCurrent: Translations.get("SALAXY.UI_TERMS.monthShort" + currentMonth) + "kuu",
      monthLast: Translations.get("SALAXY.UI_TERMS.monthShort" + (currentMonth === 1 ? 12 : currentMonth - 1)) + "kuu",
      monthOther: "Muu kuukausi",
      // monthMulti: "Monta kuukautta",
      weeks2: Translations.get("SALAXY.UI_TERMS.week2"),
      monthHalf: Translations.get("SALAXY.UI_TERMS.monthHalf"),
      other: Translations.get("SALAXY.UI_TERMS.otherPeriod"),
    };
    this._periodStartDate = Dates.format(this.dateRange.start, "D.M.", null);
  }
}
