import * as angular from "angular";

import {
  Calculations, IncomeTypeMetadata, IncomeTypesLogic, IrRow, IrRowSourceType, Unit, Workers,
} from "@salaxy/core";

import {
  InvoicesService,
  ReportsService,
  SessionService,
  UiCrudHelpers,
  UiHelpers,
} from "../../services";

import { Calculator2019Controller } from "./Calculator2019Controller";

/**
 * IR-rows based editor for calculation
 */
export class CalcIrRowsController extends Calculator2019Controller {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "UiHelpers", "UiCrudHelpers", "$location", "$routeParams", "SessionService", "ReportsService", "Workers", "InvoicesService"];

  /**
   * Active tab of editor
   */
  protected activeTab = 1;

  private _currentRow: IrRow;

  /**
   * Creates a new instance of the controller with dependency injection.
   * @ignore
   */
  constructor(
    calcApi: Calculations,
    uiHelpers: UiHelpers,
    uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    sessionService: SessionService,
    reportsService: ReportsService,
    protected workersApi: Workers,
    protected invoicesService: InvoicesService,
  ) {
    super(calcApi, uiHelpers, uiCrudHelpers, $location, $routeParams, sessionService, reportsService, workersApi, invoicesService);
  }

  /**
   * Implement IController
   */
  public $onInit () {
    /** Init component here */
  }

  /** The current row that is being viewed / edited. */
  public get currentRow (): IrRow {
    if (!this._currentRow || !this.currentCalc.result || !this.currentCalc.result.irRows) {
      return null;
    }
    if (!this.currentCalc.result.irRows.find((x) => x === this._currentRow)) {
      // Recalculation outside this controller has destroyed the current row.
      this._currentRow = null;
    }
    return this._currentRow;
  }

  /** The metadata that describes currentRow */
  public get currentRowType (): IncomeTypeMetadata {
    return this.getIncomeType(this.currentRow);
  }

  /** IR-rows that are viewed / edited by the controller */
  public get irRows (): IrRow[] {
    if (!this.currentCalc || !this.currentCalc.result) {
      return [];
    }
    return this.currentCalc.result.irRows;
  }

  /** Sets the current edited / viewd row */
  public setCurrentIrRow (row: IrRow) {
    this._currentRow = row;
  }

  /** Deletes a row */
  public deleteIrRow (row: any) {
    this.irRows.splice(this.irRows.indexOf(row), 1);
  }

  /** Returns true if the row is read-only: Cannot be directly edited. */
  public get isIrRowReadOnly () {
    return this.currentRow == null || this.currentRow.type !== "manual";
  }

  /** Updates the current row type. */
  public updateCurrentRowType () {
    if (this.currentRowType) {
      if (!this.currentRow.message || IncomeTypesLogic.allTypes.find((x) => x.label === this.currentRow.message)) {
        this.currentRow.message = this.currentRowType.label;
      }
    }
  }

  /** Gets the income type (the metadata) object for the currently selected row. */
  public getIncomeType (row: any): IncomeTypeMetadata {
    return IncomeTypesLogic.allTypes.find((x) => x.code === row.irData.code);
  }

  /** Adds a new IR row to the collection. */
  public addIrRow () {
    const row: IrRow = {
      type: IrRowSourceType.Manual,
      irData: {
        code: null,
      },
      count: 1,
      unit: Unit.Undefined,
      price: 0,
    };
    this.irRows.push(row);
    this.setCurrentIrRow(row as any);
  }
}
