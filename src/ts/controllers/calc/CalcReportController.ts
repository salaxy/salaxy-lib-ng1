import * as angular from "angular";

import {
  Ajax,
  Calculation,
  Dates,
  ReportType,
} from "@salaxy/core";

import {
  CalculationMapper,
  Templates,
} from "@salaxy/reports";

/**
 * Renders the report for the given calculation.
 * Uses Handlebars templates (@salaxy/reports).
 */
export class CalcReportController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Templates",
    "AjaxNg1",
    "$sce",
  ];

  /** The calculation for the report. */
  public calc: Calculation;

  /** The type for the calculation report. */
  public reportType: ReportType;

  /** Template for the report */
  public templateName: string;

  /** Boolean indicating if the refresh is in progress. */
  public refreshInprogress: boolean;

  private renderedCalc: Calculation;

  private renderedOptions: string;

  private htmlData: any;

  private reportData = {
    reportTask: {
      title: "",
      subTitle: "",
      date: Dates.getFormattedDate(Dates.getToday()),
      isPreview: false,
      layout: "a4Portrait",
      isLayoutA4Portrait: true,
      isLayoutA4Landscape: false,
      mode: "htmlPage",
      isModeHtmlPage: true,
      isModePdf: false,
    },
    report: {},
  };

  /**
   * Creates a new CalcReportController
   * @ignore
   */
  constructor(
    private templates: Templates,
    private ajax: Ajax,
    private $sce: angular.ISCEService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    // not initializations
  }

  /** Bindable and trusted html. */
  public get html(): any {
    if (!this.calc) {
      this.renderedCalc = null;
      this.renderedOptions = null;
    } else {
      const calc = angular.copy(this.calc);

      if (!this.renderedCalc || !angular.equals(calc, this.renderedCalc)
        || this.renderedOptions !== this.reportType + ";" + this.templateName) {
        this.renderedCalc = angular.copy(calc);
        this.renderedOptions = this.reportType + ";" + this.templateName;
        this.refresh();
      }
    }
    return this.htmlData;
  }

  /**
   * Refresh calculation report data.
   */
  private refresh() {
    if (this.refreshInprogress) {
      return;
    }
    this.refreshInprogress = true;

    this.reportData.report = CalculationMapper.getCalculationReport(this.calc, this.reportType);
    this.htmlData = this.$sce.trustAsHtml(this.templates.getHtml(this.templateName, this.reportData));

    this.refreshInprogress = false;
  }
}
