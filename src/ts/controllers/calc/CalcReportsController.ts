import * as angular from "angular";

import {
  Ajax,
  calcReportType,
  Calculation,
  CalculationStatus,
} from "@salaxy/core";

import {
  SessionService,
  UiHelpers,
} from "../../services";

/**
 * Displays the reports for the given calculations.
 */
export class CalcReportsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AjaxNg1",
    "UiHelpers",
    "SessionService",
  ];

  /** Report type */
  public reportType: calcReportType;

  /** Calculations for the reports. */
  public calcs: Calculation[] = [];

  /** Report templates and the currently selected template */
  private reports = {
    view: "partial",
    salarySlip: {
      full: "salarySlipV2",
      page1: "salarySlipV2Page1",
      partial: "workerCalculationTablesV2",
    },
    employerReport: {
      full: "employerReportV2",
      page1: "employerReportV2Page1",
      partial: "employerCalculationTablesV2",
    },
    paymentReport: {
      full: "paymentReportV2",
      page1: "paymentReportV2",
      partial: "paymentTablesV2",
    },
  };

  /**
   * Creates a new CalcReportsController
   * @ignore
   */
  constructor(
    private ajax: Ajax,
    private uiHelpers: UiHelpers,
    private sessionService: SessionService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    // not initializations
  }

  /**
   * Shows a download dialog for the given calculations.
   * @param inline - If true, the download link opens the pdf into a new web page.
   */
  public showDownloadDialog(inline = false) {

    const downloadDialogData = {
      waitHeading: "Odota...",
      readyHeading: "Valmis!",
      isReady: false,
      waitText: "Tiedostoa luodaan.",
      readyText: "Tiedosto on nyt valmis ladattavaksi.",
      openText: "Avaa",
      target: null,
      url: null,
    };

    this.uiHelpers.showDialog(
      "salaxy-components/modals/ui/Download.html",
      null,
      downloadDialogData,
      "none",
      "sm",
      true);

    const idsOnly = this.calcs.every((x) => x.workflow.status === CalculationStatus.PaymentSucceeded);
    let assure: () => Promise<string> = null;
    if (idsOnly) {
      let method = "/reports/assure/" + this.reportType + "?";
      this.calcs.map((x) => x.id).forEach((x) => { method += "ids=" + encodeURIComponent("" + x) + "&"; });
      method = method.replace(/[?&]$/, "");
      assure = () => this.ajax.getJSON(method);
    } else {
      const method = "/reports/assure/" + this.reportType;
      assure = () => this.ajax.postJSON(method, this.calcs);
    }
    assure().then((fileId) => {
      downloadDialogData.url = this.sessionService.getServerAddress()
        + `/ReportPdf/Content/${fileId}?inline=${inline}&access_token=${encodeURIComponent(this.sessionService.getCurrentToken())}`;
      downloadDialogData.target = inline ? "_blank" : null;
      downloadDialogData.isReady = true;
    });
  }
}
