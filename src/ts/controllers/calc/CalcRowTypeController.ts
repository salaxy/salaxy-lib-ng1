import { CalcRowConfig, CalcRowsLogic, CalculationRowCategory, CalculationRowType, Objects, Translations, UserDefinedRow } from "@salaxy/core";

import { SessionService, UiHelpers } from "../../services";
import { InputEnumOption } from "../form-controls/InputEnumOption";

/**
 * UI logic for changing the row type and potentially the kind for UserDefined row in calculation.
 * Note that changing the type may change the message of the row as well:
 * In the future, usecase defaults may also be changed.
 */
export class CalcRowTypeController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SessionService", "UiHelpers"];

  /** Type of the input element. Default is typeahead. */
  public type: "typehead" | "list" | "read-only";

  /** If set, filters the rows based on categories (plus rowTypes if set) */
  public categories: CalculationRowCategory[];

  /** If set, shows only these types (plus categories if set) */
  public rowTypes: CalculationRowType[];

  /** Name and identifier of the typehead input */
  public name: string;

  /**
   * Placeholder text in the typeahead input.
   * Default is 'SALAXY.UI_TERMS.select'.
   */
  public placeholder: string;

  /** If true, the list will show all the children under each parent that is shown */
  public showChildren: boolean;

  /**
   * Function that is called after the row type and potentially kind and message have been changed.
   * @example <salaxy-calc-row-type on-row-changed="$ctrl.commitNewRow()"></salaxy-calc-row-type>
   */
  public onRowChanged: (eventData: {
    /** New item which was created. */
    row: UserDefinedRow,
  }) => void;

  /** The calculation row that this component modifies. */
  public model: UserDefinedRow;

  /**
   * The default number of items that is returned from the typeahead search list.
   * Default is 50.
   */
  public top: number;

  private _internalValue: InputEnumOption;

  private modelRowType;

  private modelDataKind;

  private listCache: {
    /** Cache key */
    key: string,
    /** Cache value */
    value: CalcRowConfig[],
  } = null;

  constructor(private session: SessionService, private uiHelpers: UiHelpers) {
  }

  /** Sets the default values in init. */
  public $onInit() {
    this.top = this.top || 50;
    if (!this.type) {
      this.type = "typehead";
    }
  }

  /**
   * Detects the changes in the model properties "rowType" and "data.kind"
   * and reflects them to internalValue.
   */
  public $doCheck() {
    let hasChanges = false;
    if (this.model?.rowType !== this.modelRowType) {
      this.modelRowType = this.model?.rowType;
      hasChanges = true;
    }
    if (this.model?.data?.kind !== this.modelDataKind) {
      this.modelDataKind = this.model?.data?.kind;
      hasChanges = true;
    }
    if (hasChanges) {
      // NOTE: There is potential incoherent behavior here: depending on in which order attributes "categories"
      //       "rowTypes" and "model" are set, the filters may behave differently in edge case where model value
      //       is outside these filters. This was not considered significant at the time of writing.
      if (this.modelDataKind) {
        this._internalValue = this.getList().find((x) => x.value === `${this.modelRowType}.${this.modelDataKind}`);
      } else {
        this._internalValue = this.getList().find((x) => x.value === this.modelRowType);
      }
    }
  }

  /**
   * Internal value from view to the controller. This is one of the numerations returned by getList()
   * if model has been set and it matches to oneof those values.
   */
  protected get internalValue(): InputEnumOption {
    return this._internalValue;
  }
  protected set internalValue(val: InputEnumOption) {
    this._internalValue = val;
    if (!this.model) {
      return;
    }
    this.model.data = this.model.data || {};
    if (!val) {
      this.model.rowType = null;
      this.model.data.kind = null;
    } else if (val.value.indexOf(".")) {
      const valueArr = val.value.split(".");
      this.model.rowType = valueArr[0] as CalculationRowType;
      this.model.data.kind = valueArr[1] || undefined;
    } else {
      this.model.rowType = val.value as CalculationRowType;
      this.model.data.kind = undefined;
    }
    this.onRowChanged({ row: this.model });
  }

  /**
   * Gets the list of row types according to options:
   * This list changes and is cached based on role (household/company), language,
   * rowTypes and categories. Currently the same as search(null), but cached.
   */
  public getList() {
    return this.uiHelpers.cache(this, "list",
      () => { return this.search(null, true) },
      () => this.session.isInRole("household").toString() + Translations.getLanguage() + this.rowTypes?.join("") + this.categories?.join(""))
  }

  /**
   * Searches the row types.
   * @param searchText The search text. If null or empty, returns the full list.
   * @param getAll If true, ignores the this.top setting to return only top n items of the search
   * and also sets the showChildren property for this run.
   */
  public search(searchText: string, getAll = false): InputEnumOption[] {
    const toOption = (config: CalcRowConfig) => {
      return {
        text: config.label,
        value: config.name,
        title: config.descr,
        ui: {
          color: config.color,
          iconText: config.iconText,
          addedByChild: null,
        },
      };
    };
    const result: InputEnumOption[] = [];
    searchText = (searchText || "").trim().toLowerCase();
    this.getRowTypes().forEach((rowConfig) => {
      let isParentAdded = false;
      if (
        !searchText
        || rowConfig.label.toLowerCase().indexOf(searchText) >= 0
        || rowConfig.descr.toLowerCase().indexOf(searchText) >= 0
      ) {
        result.push(toOption(rowConfig));
        if (!getAll && result.length >= this.top) {
          return result;
        }
        isParentAdded = true;
      }
      if (rowConfig.kind?.values) {
        rowConfig.kind.values.forEach((kind, index) => {
          if (
            index > 0 &&
            (
              (getAll || this.showChildren)
              || (searchText && (kind.label ?? "").toLowerCase().indexOf(searchText) >= 0)
              || (searchText && (kind.descr ?? "").toLowerCase().indexOf(searchText) >= 0)
            )
          ) {
            if (!isParentAdded) {
              const option = toOption(rowConfig);
              option.ui.addedByChild = true;
              result.push(option);
              isParentAdded = true;
            }
            result.push({
              text: kind.label,
              value: rowConfig.name + "." + kind.name,
              title: kind.descr,
            });
            if (!getAll && result.length >= this.top) {
              return result;
            }
          }
        });
      }
    });
    if (result.length === 2 && result[0].ui.addedByChild) {
      // If only one parent with one child (child is exact match) => Merge to one row.
      return [{
        text: result[0].text + ": " + result[1].text,
        value: result[1].value,
        title: result[1].title,
        ui: result[0].ui,
      }];
    }
    return result;
  }

  /** Gets a label for the given internal value. */
  public getRowLabel(rowTypeKey: string) {
    if (!rowTypeKey) {
      return null;
    }
    let rowTypeName = null;
    let kindName = null;
    if (rowTypeKey.indexOf(".")) {
      const valueArr = rowTypeKey.split(".");
      rowTypeName = valueArr[0] as CalculationRowType;
      kindName = valueArr[1];
      if (kindName === "undefined") {
        kindName = null; // For undefined, take the main category.
      }
    } else {
      rowTypeName = rowTypeKey as CalculationRowType;
    }
    const rowType = this.getRowTypes().find((type) => type.name === rowTypeName);
    if (!rowType) {
      return null;
    }
    const kind = rowType.kind?.values?.find((x) => x.name === kindName);
    return kind ? kind.label : rowType.label;
  }

  /**
   * Gets the list of row types. Method filters based on rowTypes and categories,
   * the texts are language versioned and there is a role filter for "household"/"company".
   */
  private getRowTypes(): CalcRowConfig[] {
    const key = "" + this.categories + this.session.isInRole("household") + this.rowTypes;
    if (!this.listCache || this.listCache.key !== key) {
      const rowsLogic = new CalcRowsLogic(this.session.isInRole("household") ? "household" : "company");
      let value;
      if ((this.categories || []).length + (this.rowTypes || []).length > 0) {
        const rowConfigsByCategory =  Objects.copy(rowsLogic.getRowConfigsByCategory(this.categories));
        // Remove kind from IrIncomeType: prevent listing of income types in category based lists
        const irIncomeType = rowConfigsByCategory.find( (x) => x.name === CalculationRowType.IrIncomeType);
        if (irIncomeType != null) {
          irIncomeType.kind = null;
        }
        value = rowsLogic.getRowConfigsByType(this.rowTypes).concat(rowConfigsByCategory);
      } else {
        value = rowsLogic.getRowConfigs();
      }
      this.listCache = { key, value };
    }
    return this.listCache.value;
  }
}
