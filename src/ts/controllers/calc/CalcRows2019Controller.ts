import * as angular from "angular";

import {
  CalcRowConfig,
  CalcRowsLogic,
  Calculation,
  CalculationRowType,
  CalculatorLogic,
  IncomeTypesLogic,
  RowsUsecaseLogic,
  UserDefinedRow,
} from "@salaxy/core";

import {
  SessionService,
  UiHelpers,
} from "../../services";

import { ListControllerBase, ListControllerBaseBindings } from "../bases";

/**
 * Editor for Calculation rows / usecases.
 * Can be bound to a Calculation or to Employment relation.
 */
export class CalcRows2019Controller extends ListControllerBase<Calculation, UserDefinedRow> {
  /** Bindings for components that use this controller */
  public static bindings = (new class extends ListControllerBaseBindings {

    /** Sets the title in the table header. */
    public title = "@";

    /** Shows edit buttons. Used in Payroll. */
    public showEditButtons = "<";

    /** Shows gross totals and total payment in table footer */
    public showGrossTotals = "<";

    /**
     * Alternative binding for parent: Give just a stand-alone rows list.
     * Setting the row to an array will set these rows to the parent component
     * or create a new parent component if no parent component is specified.
     */
    public rows = "<";

    /** If set, filters the rows based on categories (plus rowTypes if set) */
    public categories = "<";

    /**
     * Defines the helper texts and branding color for row type selection list dialog.
     * Null for default "primary" (no texts)
     */
    public rowTypeSelectHelp = "@";

    /** If set, shows only these types (plus categories if set) */
    public rowTypes = "<";

  }());

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [ "UiHelpers", "$timeout", "SessionService" ];

  /** New empty row for databinding when creating a new row. */
  public newRow: UserDefinedRow;

  /** Title in the Table header */
  public title: string;

  /** Shows edit buttons. Used in Payroll. */
  public showEditButtons: boolean;

  /** Shows gross totals and total payment in table footer */
  public showGrossTotals: boolean;

  /**
   * Alternative binding for parent: Give just a stand-alone rows list.
   * Setting the row to an array will set these rows to the parent component
   * or create a new parent component if no parent component is specified.
   * The resolving occures in list get, so the order of setting is not an issue.
   */
  public rows: UserDefinedRow[];

  /** If set, filters the rows based on categories (plus rowTypes if set) */
  public categories: ("salary" | "salaryCalc" | "holidays" | "salaryAdditions" | "benefits" | "expenses" | "deductions" | "other")[];

  /**
   * Defines the helper texts and branding color for row type selection list dialog
   * (showRowTypeSelectionDialog method)
   */
  public rowTypeSelectHelp: "salaries" | "benefits" | "expenses" | "deductions";

  /** If set, shows only these types (plus categories if set) */
  public rowTypes: CalculationRowType[];

  /**
   * Creates a new CalcRows2019Controller
   * @param uiHelpers - Salaxy UI helpers service.
   * @ignore
   */
  constructor(
    uiHelpers: UiHelpers,
    private $timeout: angular.ITimeoutService,

    private session: SessionService,
  ) {
    super(uiHelpers);
  }

  /** Sets the default values in init. */
  public $onInit() {
    this.newRow = this.getBlank();
  }

  /** List of rows  */
  public get list(): UserDefinedRow[] | null  {
    if (!this.parent) {
      if (this.rows) {
        this.parent = CalculatorLogic.getBlank();
      } else {
        return null;
      }
    }
    // HACK: This does not belong here: Move to server-side (in v03 release candidate).
    CalculatorLogic.moveSalaryToRows(this.parent);
    if (this.rows && this.parent.rows !== this.rows) {
      // Rows has been set outside.
      this.parent.rows = this.rows;
    }
    return this.parent.rows;
  }

  /** List filtered by categories, rowTypes and potentially using custom filter. */
  public get filteredList(): UserDefinedRow[] |null {
    const typesToShow = this.rowTypesToShow;
    if (typesToShow.length === 0) {
      return this.list;
    }
    return this.list.filter((x) => typesToShow.indexOf(x.rowType) >= 0);
  }

  /** Combines together categories and rowTypes properties as one list of row types. */
  public get rowTypesToShow() {
    const rowsLogic = new CalcRowsLogic(this.session.isInRole("household") ? "household" : "company");
    const types = [...this.rowTypes || []];
    if (this.categories) {
      for (const cat of this.categories) {
        types.push(...rowsLogic.getRowTypesByCategory(cat as any));
      }
    }
    return types;
  }

  /** Creating of a new item. */
  public getBlank(): UserDefinedRow {
    const newRow: UserDefinedRow = {
      rowIndex: this.list.filter((x) => x.rowIndex >= 0).length,
      rowType: null,
      count: null,
      price: null,
      message: null,
    };
    return newRow;
  }

  /**
   * Returns a validation message for a row or null if none is required.
   * @param row The row that is validated.
   */
  public getValidation(row: UserDefinedRow): {
    /** Validation message */
    msg: string,
    /** Type of validation message */
    type: "default" | "error",
  } {
    return RowsUsecaseLogic.getValidation(row, this.parent);
  }

  /** Template for edit UI that is shown in a modal dialog. */
  public getEditDialogTemplateUrl() {
    return "salaxy-components/calc/rows/CalcRows2019EditDialog.html";
  }

  /**
   * Gets the UX configuration for a row.
   * @param row Row for which the config is fetched.
   * If not set, gets the config for current.
   */
  public getConfig(row: UserDefinedRow): CalcRowConfig {
    return this.getEditDialogLogic().getConfig((row));
  }

  /**
   * Gets the placeholder text for an input.
   */
  public getPlaceholderText(row: UserDefinedRow, field: "amount" | "price") {
    if (!row.rowType) {
      return "";
    }
    const config = this.getConfig(row);
    if (field === "amount") {
      return config.amount.label;
    } else {
      return config.price.label;
    }
  }

  /**
   * Return the total for the row
   */
  public getRowTotal(row: UserDefinedRow) {
    return this.getEditDialogLogic().getRowTotal(row);
  }

  /**
   * Returns true, if the row is disabled. The row is interpreted as disabled if it has no rowType
   * or it has been set as hidden in the configuration.
   * @param row Row to check
   * @param field Type of the input / field.
   */
  public isDisabled(row: UserDefinedRow, field: "amount" | "price") {
    return this.getEditDialogLogic().isDisabled(row, field);
  }

  /** Row type changes on a row */
  public rowTypeChanged(row: UserDefinedRow) {
    if (!row || !row.rowType) {
      return;
    }
    const config = this.getConfig(row);
    // row.message = EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType);
    if (config.amount.default) {
      row.count = config.amount.default;
    }
    if (config.price.default) {
      row.price = config.price.default;
    }
    this.$timeout(() => {
      if (this.isDisabled(row, "amount")) {
        document.getElementById("iptPrice" + row.rowIndex).focus();

      } else {
        document.getElementById("iptCount" + row.rowIndex).focus();
      }
    }, 500);
  }

  /**
   * Commits the new row in newRow property and adds it to the rows list.
   * Updates usecase and sets the newRow as new blank row.
   */
  public commitNewRow(): UserDefinedRow {
    if (!this.newRow || !this.newRow.rowType) {
      return null;
    }
    RowsUsecaseLogic.updateUsecase(this.newRow, this.parent);
    this.list.push(this.newRow);
    this.rowTypeChanged(this.newRow);
    // Reset new row type
    this.$timeout(() => {
      this.newRow = this.getBlank();
    }, 300); // avoid debounce
    return this.newRow;
  }

  /** Shows the selection dialog for the row type. */
  public showRowTypeSelectionDialog() {
    this.uiHelpers.openEditDialog("salaxy-components/modals/calc/row-type-select.html", this.newRow, {
      categories: this.categories,
      rowTypes: this.rowTypes,
      rowTypeSelectHelp: this.rowTypeSelectHelp,
      })
      .then((result) => {
        if (result.action === "ok" && result.hasChanges) {
          this.showEditDialog(this.commitNewRow());
        }
      });
  }

  /** Gets the needed logic for Edit dialog */
  public getEditDialogLogic() {
    const getConfig = (current: UserDefinedRow): CalcRowConfig => {
      return CalcRowsLogic.getRowConfig(current.rowType);
    };
    const isReadOnly = this.parent.isReadOnly;
    return {
      getConfig,
      isReadOnly,
      isDisabled: (current: UserDefinedRow, field: "amount" | "price") => {
        if (!current.rowType) {
          return true;
        }
        const config = getConfig(current);
        switch (field) {
          case "amount":
            return config.amount.input === "hidden";
          case "price":
            return config.price.input === "hidden";
          default:
            return false;
        }
      },
      getRowTotal: (current: UserDefinedRow) => {
        if (current == null) {
          return null;
        }
        if (current.count == null) {
          return current.price;
        } else {
          return current.count * current.price;
        }
      },
      updateUsecase: (current: UserDefinedRow) => {
        RowsUsecaseLogic.updateUsecase(current, this.parent);
      },
      irCodeData: (current: UserDefinedRow) => {
        if (current.data && current.data.irData) {
          return IncomeTypesLogic.allTypes.find((x) => x.code === current.data.irData.code);
        }
        return {};
      },
    };
  }

   /** Returns true if the filtered list has any rows. */
   public get hasRows(): boolean {
    return (this.filteredList || []).length > 0;
  }

  /** Gets the total for the current rows. */
  public getTotal() {
    return (this.filteredList || []).reduce((prev, curr) => prev + curr.count * curr.price, 0);
  }
 /**
  * Gets the gross total for the calculation.
  * Current implementation does not support filtering.
  */
  public getGrossTotal() {
    if (!this.parent || !this.parent.result || !this.parent.result.totals) {
      return null;
    }
    return this.parent.result.totals.totalGrossSalary;
  }

  /**
   * Gets the total payment for the calculation.
   * Current implementation does not support filtering.
   */
  public getFinalCost() {
    if (!this.parent || !this.parent.result || !this.parent.result.employerCalc) {
      return null;
    }
    return this.parent.result.employerCalc.finalCost;
  }
  /**
   * Gets the net salary for the calculation.
   * Current implementation does not support filtering.
   */
  public getNetSalary() {
    return this.parent.result.workerCalc.salaryPayment;
  }

  /**
   * Gets the total payment that is paid to palkkaus.fi.
   * Current implementation does not support filtering.
   */
  public getTotalPayment() {
    return this.parent.result.employerCalc.totalPayment;
  }
}
