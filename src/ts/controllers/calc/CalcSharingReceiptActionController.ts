import * as angular from "angular";

import { Calculation, Calculations, CalculationStatus, Workers } from "@salaxy/core";

import { InvoicesService, ReportsService, SessionService, UiHelpers } from "../../services";
import { CalculationCrudController } from "./CalculationCrudController";

/**
 * Controller for sharing actions: accept, decline
 */
export class CalcSharingReceiptActionController extends CalculationCrudController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "ReportsService", "UiHelpers", "$location", "$routeParams", "Workers", "SessionService", "InvoicesService"];

  /**
   * Creates a new CalcSharingController
   * @ignore
   */
  constructor(
    calculationsApi: Calculations,
    reportsService: ReportsService,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    workersApi: Workers,
    sessionService: SessionService,
    protected invoicesService: InvoicesService,
  ) {
    super(calculationsApi, reportsService, uiHelpers, $location, $routeParams, workersApi, sessionService, invoicesService);
  }

  /** Approve the the calculation requested by the worker. */
  public approve(): Promise<Calculation> {
    switch (this.current.workflow.status) {
      case CalculationStatus.SharedWaiting:
      case CalculationStatus.SharedRejected:
        this.current.workflow.status = CalculationStatus.SharedApproved;
        return this.save();
      case CalculationStatus.SharedApproved:
      default:
        return Promise.resolve(this.current);
    }
  }

  /** Reject the the calculation requested by the worker. */
  public reject(): Promise<Calculation> {
    switch (this.current.workflow.status) {
      case CalculationStatus.SharedWaiting:
      case CalculationStatus.SharedApproved:
        this.current.workflow.status = CalculationStatus.SharedRejected;
        return this.save();
      case CalculationStatus.SharedRejected:
      default:
        return Promise.resolve(this.current);
    }
  }
}
