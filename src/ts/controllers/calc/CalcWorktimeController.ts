import * as angular from "angular";

import {
  AbsenceCauseCode,
  Calculation, Calculations, Calculator, CalcWorktime, DateRange, Dates, HolidayCode, HolidayYear, MonthlyHolidayAccrual, Translations, WageBasis, WorkerAbsences,
} from "@salaxy/core";

import { SessionService, UiHelpers } from "../../services";
import { HolidayYearCrudController, WorkerAbsencesCrudController } from "../worker";
import { CalculatorSections } from "./CalculatorSections";

/**
 * Handles the user interaction of Worktime within the Calculator.
 * The Worktime contains the logic for fetching holidays and absences
 * for that particular period and adding calculation rows for them if necessary.
 */
export class CalcWorktimeController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "UiHelpers",
    "Calculations",
    "Calculator",
    "SessionService",
  ];

  /** User interface shortcuts for period selection */
  public periodShortcuts: {
    /** Text for current month. */
    monthCurrent: string,
    /** Text for last month. */
    monthLast: string,
    /** Text for other month. */
    monthOther: string,
    // monthMulti: "Monta kuukautta",
    /** Text for two weeks. */
    weeks2: string,
    /** Text for half a month */
    monthHalf: string,
    /** Text for other period. */
    other: string,
  };

  /** The calculation that the controller edits (shows in read-only mode). */
  public calc: Calculation;

  /**
   * Worktime data for the period fetched from the server
   */
  public worktime: CalcWorktime;

  /** If there is a validation error in period start, it is added here. */
  public periodStartDateError = null;

  /** Demodata for the anonymous demo calculator. */
  public demoData: {
    /** Start date for which the data was generated. */
    startDate: string,
    /** End date for which the data was generated. */
    endDate: string,
    /** The holiday data */
    holidays?: HolidayYear,
    /** Absences data. */
    absences?: WorkerAbsences,
  } = { startDate: null, endDate: null };

  private _periodStartDate;

  private _dateRange: DateRange;

  /**
   * Creates a new CalcWorktimeController
   * @ignore
   */
  constructor(
    private uiHelpers: UiHelpers,
    private calcApi: Calculations,

    private anonCalcApi: Calculator,

    private sessionService: SessionService,

  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    this.initDateRanges();
    if (this.currentCalc && this.currentCalc.info.workStartDate && this.currentCalc.info.workEndDate) {
      this.reloadHolidays();
    }
  }

  /** Gets the requested salary date */
  public get requestedSalaryDate(): string {
    return this.calc.workflow.requestedSalaryDate;
  }
  public set requestedSalaryDate(value: string) {
    this.calc.workflow.requestedSalaryDate = value;
    this.calc.workflow.salaryDate = value; // Set the salary date to the requestedSalaryDate until the next recalculation.
  }

  /**
   * Reloads the holidays from server.
   */
  public reloadHolidays(): void {
    this.worktime = null;
    if (this.sessionService.isAuthenticated) {
      this.calcApi.getWorktimeData(this.calc).then((result) => this.worktime = result);
    } else {
      this.checkDemoData();
      this.anonCalcApi.getWorktimeData(this.calc, this.demoData.holidays, this.demoData.absences).then((result) => this.worktime = result);
    }
  }

  /** Holiday year has been changed and must be saved and reloaded. */
  public holidaysChanged(holidaysCtrl: HolidayYearCrudController) {
    holidaysCtrl.save().then(() => {
      this.reloadHolidays();
    });
  }

  /** Absences has been changed and must be saved and reloaded. */
  public absencesChanged(absencesCtrl: WorkerAbsencesCrudController) {
    absencesCtrl.save().then(() => {
      this.reloadHolidays();
    });
  }

  /** Gets the current calculation object */
  public get currentCalc(): Calculation {
    return this.calc;
  }

  /** Getter and setter for Calculation daterange in compatible format for new DateRange component. */
  public get dateRange(): DateRange {
    const info = (this.currentCalc || {}).info;
    const framework = (this.currentCalc || {}).framework;
    if (!this._dateRange) {
      this._dateRange = {
        start: info.workStartDate,
        end: info.workEndDate,
        daysCount: framework.numberOfDays,
      };
    }
    return this._dateRange;
  }
  public set dateRange(value: DateRange) {
    this._dateRange = value;
  }

  /** Returns true if the holidays uses accrual. */
  public get isHolidayAccrual(): boolean {
    if (!this.worktime) {
      return false;
    }
    const code: HolidayCode = this.worktime.holidaySpec.code;
    return code && code !== HolidayCode.Undefined
      && code !== HolidayCode.NoHolidays
      && code !== HolidayCode.HolidayCompensation
      && code !== HolidayCode.HolidayCompensationIncluded;
  }

  /**
   * Called when the date range changes.
   * @param noPeriodStartDateUpdate If true, the _periodStartDate is not updated.
   * Should be true if the change is triggered by that input to avoid UI flickering.
   */
  public dateRangeChange(noPeriodStartDateUpdate = false) {
    const info = (this.currentCalc || {}).info;
    const framework = (this.currentCalc || {}).framework;
    info.workStartDate = this.dateRange.start;
    if (!noPeriodStartDateUpdate) {
      this._periodStartDate = Dates.format(this.dateRange.start, "D.M.", null);
    }
    info.workEndDate = this.dateRange.end;
    framework.numberOfDays = this.dateRange.daysCount;
    this.reloadHolidays();
  }

  /**
   * Gets or sets the period start date, which is bound to editable input in case the period type
   * is not "other" (date the range / calendar component)
   */
  public get periodStartDate(): string {
    return this._periodStartDate;
  }
  public set periodStartDate(value: string) {
    this._periodStartDate = value;
    const parts = (value || "").split(".");
    if (parts.length < 2 || parts.length > 3) {
      this.periodStartDateError = "Syötä muodossa 'pv.kk.'";
      return;
    }
    let year = (parts.length === 3) ? Number((parts[2] || "").trim()) : null;
    if (!year || year < 2019 || year > 2100) {
      year = Number(this.dateRange.start.substr(0, 4));
    }
    const date = Dates.getDate(year, Number(parts[1]), Number(parts[0]));
    if (date) {
      this.periodStartDateError = null;
      switch (this.periodShortcut) {
        case "monthCurrent":
        case "monthLast":
        case "monthOther":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(1, "month").subtract(1, "day"));
          break;
        case "weeks2":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(2, "week").subtract(1, "day"));
          break;
        case "monthHalf":
          if (Dates.getMoment(date).date() === 15 || Dates.getMoment(date).date() === 16) {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).endOf("month"));
          } else {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(14, "day"));
          }
          break;
        case "other":
          this.dateRange = Dates.getDateRange(date, date);
          break;
      }
      this.dateRangeChange(true);
    } else {
      this.periodStartDateError = `${value} ei ole päivämäärä.`;
    }
  }

  /**
   * Gets the user interface shortcut for the period type:
   * Different user interfaces are shown depending on the period shortcut.
   */
  public get periodShortcut(): "monthCurrent" | "monthLast" | "monthOther" | "weeks2" | "monthHalf" | "other" {
    if (!this.dateRange.start || !this.dateRange.end) {
      return null;
    }
    if (this.dateRange.start === Dates.getDate("today", "today", 1)
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().endOf("month"))) {

      return "monthCurrent";
    }
    if (this.dateRange.start === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month"))
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month").endOf("month"))) {
      return "monthLast";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"))) {
      return "monthOther";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(2, "week").subtract(1, "day"))) {
      return "weeks2";
    }

    // if the range is 15 (14) days or
    // starts 15th or 16th and ends month end
    if ((Dates.getDuration(this.dateRange.start, this.dateRange.end).days() === 14)
      || (Dates.getMoment(this.dateRange.start).date() === 15 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))
      || (Dates.getMoment(this.dateRange.start).date() === 16 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))) {
      return "monthHalf";
    }
    return "other";
  }
  public set periodShortcut(value: "monthCurrent" | "monthLast" | "monthOther" | "weeks2" | "monthHalf" | "other") {
    const today = Dates.getTodayMoment();
    this.periodStartDateError = null;
    switch (value) {
      case "monthCurrent":
        this.dateRange.start = Dates.getDate("today", "today", 1);
        this.dateRange.end = Dates.asDate(Dates.getTodayMoment().endOf("month"));
        break;
      case "monthOther":
        if (today.date() === 1) {
          today.add(1, "day"); // Move to tomorrow so that the selection is monthOther and not monthCurrent.
        }
        this.dateRange.start = Dates.asDate(today);
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"));
        break;
      case "monthLast":
        this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month"));
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        break;
      case "weeks2":
        this.dateRange.start = Dates.asDate(today.clone().startOf("week").add(-2, "week").add(1, "day"));
        this.dateRange.end = Dates.asDate(today.clone().startOf("week"));
        break;
      case "monthHalf":

        // TODO Check this
        if (today.date() < 16) {
          // 16 - (28,29,30,31)
          this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month").add(15, "day"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        } else {
          // 1 - 15
          this.dateRange.start = Dates.asDate(today.clone().startOf("month"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(14, "day"));
        }
        break;
      case "other":
        this.dateRange.start = Dates.asDate(today.clone().add(-1, "day"));
        this.dateRange.end = Dates.asDate(today.clone());
        break;
    }
    this.dateRange = Dates.getDateRange(this.dateRange.start, this.dateRange.end);
    this.dateRangeChange();
  }

  /**
   * Submit the data in the Worktime (period) dialog.
   */
  public submitWorktime() {
    const loader = this.uiHelpers.showLoading("Lasketaan ajanjaksoa");
    this.calc.worktime = this.worktime;
    this.calcApi.recalculateWorktime(this.calc).then((result) => {
      angular.copy(result, this.calc);
      this.calc.result = result.result;
      loader.dismiss();
      new CalculatorSections(this.currentCalc).setActive("salary");
    });
  }

  /** Returns true if the current worktime data seems to have holidays or absences to add as rows in the salary calculation.  */
  public hasHolidaysOrAbsences() {
    if (!this.worktime) {
      return false;
    }
    return this.worktime.holidaySpec.code === HolidayCode.HolidayCompensation
      || this.worktime.holidaySpec.code === HolidayCode.HolidayCompensationIncluded
      || this.worktime.leavesDays > 0
      || this.worktime.absencesDays > 0
      ;
  }

  /** Shows the pro-dialog */
  public showPeriodDetails() {
    const detailsModel = {
      worktime: this.worktime,
    };
    this.uiHelpers.showDialog("salaxy-components/calc/details/worktime-pro.html", null, detailsModel, null, "lg");
  }

  /**
   * Gets the status for the current holidays and absences fetching.
   * These are fetched from the server when the period and / or worker changes.
   */
  public getHolidaysStatus(): "noWorker" | "noPeriod" | "loading" | "ok" {
    if (!this.currentCalc.worker?.employmentId && this.sessionService.isAuthenticated) {
      return "noWorker";
    }
    if (!this.currentCalc.info.workStartDate || !(this.currentCalc.framework.numberOfDays > 1)) {
      return "noPeriod";
    }
    return this.worktime == null ? "loading" : "ok";
  }

  /**
   * Gets the monthly text for calendar month for accruals.
   * @param type Type of the text is either header or description.
   */
  public getAccrualMonthText(type: "header" | "description", month: MonthlyHolidayAccrual) {
    switch (type) {
      case "header":
        return month ? `${Translations.get("SALAXY.UI_TERMS.monthShort" + Dates.getMonth(month.month))}kuu` : "#err month";
      case "description": {
        switch (this.worktime.holidaySpec.code) {
          case HolidayCode.Permanent14Days:
            return "Lomia kertyy kalenterikuukaudelle, jos työpäiviä vähintään 14 päivää. Korjaa tarvittaessa työntekijätietoihin palkanmaksun jälkeen.";
          case HolidayCode.Permanent35Hours:
            return "Lomia kertyy kalenterikuukaudelle, jos työtunteja vähintään 35. Korjaa tarvittaessa työntekijätietoihin palkanmaksun jälkeen.";
          case HolidayCode.TemporaryTimeOff:
            return "Lomia kertyy kalenterikuukaudelle aina 2 päivää.";
          default:
            return "Ei lomakertymää (#err)";
        }
      }
    }
    return "#err " + type;
  }

  /**
   * Gets the accrual explanation text. This is currently not in use.
   */
  public getHolidayAccrualText(monthlyAccrual: MonthlyHolidayAccrual): string {
    if (this.worktime.holidaySpec.code === HolidayCode.Permanent14Days) {
      if (monthlyAccrual.workDays < 14) {
        return `${monthlyAccrual.workDays} pv < 14 pv => Ei lomakertymää.`;
      } else {
        return `${monthlyAccrual.workDays} pv > 14 pv => Lomakertymä ${monthlyAccrual.daysAccrued} pv.`;
      }
    } else if (this.worktime.holidaySpec.code === HolidayCode.Permanent35Hours) {
      if (monthlyAccrual.workHours == null) {
        return `Ei tietoa tunneista, oletus yli 35h => Lomakertymä ${monthlyAccrual.daysAccrued} pv.`;
      } else if (monthlyAccrual.workHours < 35) {
        return `${monthlyAccrual.workHours} h < 35 h => Ei lomakertymää.`;
      } else {
        return `${monthlyAccrual.workHours} h > 35 h => Lomakertymä ${monthlyAccrual.daysAccrued} pv.`;
      }
    } else if (this.worktime.holidaySpec.code === HolidayCode.TemporaryTimeOff) {
      return `Vapaan ansaintasäännössä lomapäivien kertymä on aina ${monthlyAccrual.daysAccrued} pv / kalenterikuukausi.`;
    } else {
      return "Ei lomapäivien kertymää.";
    }
  }

  /** Checks if the period has changed and if so resets the demo data. */
  public checkDemoData(): void {
    if (
      this.currentCalc.info.workStartDate == this.demoData.startDate && this.currentCalc.info.workEndDate == this.demoData.endDate) {
      return;
    }
    let holiday = null;
    let absenceDay = null;
    if (this.currentCalc.info.workStartDate && this.currentCalc.info.workEndDate) {
        const workdays = Dates.getWorkdays(this.currentCalc.info.workStartDate, this.currentCalc.info.workEndDate);
        if (workdays.length > 5) {
          holiday = workdays[Math.round(workdays.length / 2)];
          absenceDay = workdays[Math.round(workdays.length / 2) + 1];
        }
    }
    this.demoData = this.demoData || {} as any;
    this.demoData.startDate = this.currentCalc.info.workStartDate;
    this.demoData.endDate = this.currentCalc.info.workEndDate;

    if (!holiday) {
      this.demoData.holidays = null;
      this.demoData.absences = null;
      return;
    }
    this.demoData.holidays = {
      year: Dates.getYear(holiday),
      employmentId: null,
      spec: {
        wageBasis: WageBasis.Monthly,
      }
    };
    this.demoData.absences = {
      employmentId: null,
      periods: [
        {
          causeCode: AbsenceCauseCode.Illness,
          notes: "Poissaolo demodatassa",
          period: {
            start: absenceDay,
            end: absenceDay,
            daysCount: 1,
            days: absenceDay,
          },
        }
      ]
    };
  }

  private initDateRanges() {
    const currentMonth = Dates.getMonth(Dates.getToday());
    this.periodShortcuts = {
      monthCurrent: Translations.get("SALAXY.UI_TERMS.monthShort" + currentMonth) + "kuu",
      monthLast: Translations.get("SALAXY.UI_TERMS.monthShort" + (currentMonth === 1 ? 12 : currentMonth - 1)) + "kuu",
      monthOther: "Muu kuukausi",
      // monthMulti: "Monta kuukautta",
      weeks2: Translations.get("SALAXY.UI_TERMS.week2"),
      monthHalf: Translations.get("SALAXY.UI_TERMS.monthHalf"),
      other: Translations.get("SALAXY.UI_TERMS.otherPeriod"),
    };
    this._periodStartDate = Dates.format(this.dateRange.start, "D.M.", null);
  }

}
