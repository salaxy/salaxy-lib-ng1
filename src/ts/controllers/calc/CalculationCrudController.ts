import * as angular from "angular";

import { Calculation, Calculations, CalculationStatus, PaymentChannel, Workers } from "@salaxy/core";

import { InvoicesService, ReportsService, SessionService, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * The new ApiCrudObject type of CRUD controller for the calculations.
 */
export class CalculationCrudController extends ApiCrudObjectController<Calculation> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "ReportsService", "UiHelpers", "$location", "$routeParams", "Workers", "SessionService", "InvoicesService"];

  constructor(
    private calculationsApi: Calculations,
    protected reportsService: ReportsService,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    protected workersApi: Workers,
    protected sessionService: SessionService,
    protected invoicesService: InvoicesService,
  ) {
    // Dependency injection
    super(calculationsApi, uiHelpers, $location, $routeParams);
  }
  /**
   * Initialization code.
   */
  public $onInit() {
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/calc",
      detailsUrl: this.detailsUrl || "/calc/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/CalculationsAll.html",
      oDataOptions: {},
    };
  }

  /** Recalculates the current calculation */
  public recalculate() {
    this.setStatus(this.calculationsApi.recalculate(this.current))
      .then((calc) => {
        this.setCurrentValue(calc);
    });
  }

  /**
   * Gets the URL for a calculation pdf.
   * If report has not been saved (ID is null), returns null.
   *
   * @param calc - Calculation. This method requires that the calculation has been saved.
   *
   * @returns Url for specified report
   */
  public getPdfUrl(calc: Calculation) {
    if (!calc) {
      return null;
    }
    return this.reportsService.getPdfUrlForCalc("salarySlip", calc.id, true);
  }

  /**
   * If true, this is a new unsaved object.
   * Adds "new-for-worker" and "new-for-employment" actions as new item.
   */
  public isNew(): boolean {
    if (this.parentController) {
      return this.parentController.isNew();
    }
    return this.action === "new-for-worker" || this.action === "new-for-employment" || super.isNew();
  }

  /**
   * Starts the reload process depending on the bindingMode:
   * Overrides a different load process for "new-for-worker" and "new-for-employment".
   */
  public reload(): Promise<Calculation> {
    if (this.action === "new-for-worker" || this.action === "new-for-employment") {
      if (this.parentController) {
        return this.parentController.reload().then((item) => {
          this._currentId = null;
          return item;
        });
      } else {
        const calc = this.api.getBlank();
        if (this.currentId === "new") {
          this._currentId = null;
        }
        if (this.action === "new-for-worker") {
          calc.worker.accountId = this.currentId;
        } else {
          calc.worker.employmentId = this.currentId;
        }
        this._currentId = null;
        this.setDefaultPaymentChannel(calc);
        return this.setStatus(this.calculationsApi.updateFromEmployment(calc, false, true))
          .then((item) => {
            this.setCurrentRef(item);
            return item;
          });
      }
    } else {
      if (this.isNew() && this.sessionService.isInRole("worker")) {
        if (this.parentController) {
          return this.parentController.reload().then((item) => {
            this._currentId = null;
            return item;
          });
        } else {
          const getNewCalc = (): Promise<Calculation> => {
            if (this.action === "copy-as-new") {
              return this.setStatus(this.api.getSingle(this.currentId)).then((item) => {
                return this.copyItem( item || this.api.getBlank());
              });
            } else {
              return Promise.resolve(this.api.getBlank());
            }
          };
          return getNewCalc().then((newCalc) => {
            newCalc.worker.isSelf = true;
            newCalc.employer.isSelf = false;
            this._currentId = null;
            this.setDefaultPaymentChannel(newCalc);
            return this.setStatus(this.calculationsApi.recalculate(newCalc)).then((calc) => {
              this.setCurrentRef(calc);
              return calc;
          });
        });
        }
      }

      return super.reload().then( (reloaded) => {
        if (super.isNew()) {
          this.setDefaultPaymentChannel(reloaded);
        }
        return reloaded;
      });
    }
  }

  /**
   * Creates a copy of a given item.
   * This is a synchronous method that should basically convert a saved item to a new item.
   * @param copySource Item (container item) to copy as new.
   */
  public copyItem(copySource: Calculation): Calculation {
    const copy = super.copyItem(copySource);
    copy.id = null;
    copy.info.workStartDate = null;
    copy.info.workEndDate = null;
    copy.info.workerMessage = null;
    copy.info.paymentId = null;
    copy.info.payrollId = null;
    copy.info.backofficeNotes = null;
    copy.info.pensionPaymentDate = null;
    copy.info.pensionPaymentRef = null;
    copy.info.pensionPaymentSpecifier = null;
    (copy.info as any).paymentChannel = PaymentChannel.Undefined;
    copy.workflow.status = CalculationStatus.Draft;
    copy.workflow.paidAt = null;
    copy.workflow.salaryDate = null;
    copy.workflow.requestedSalaryDate = null;
    copy.workflow.salaryPaidAt = null;
    copy.createdAt = null;
    copy.updatedAt = null;
    if (copy.framework) {
      copy.framework.numberOfDays = 0;
    }
    // HACK: remove payroll-service flag. This will be replaced with invoicing flags eventually.
    if (copy.usecase &&
      ( copy.usecase.uri === "palkkaus.fi/usecases/payroll-service/pro-calc" ||
      copy.usecase.uri === "palkkaus.fi/usecases/payroll-service/calc")) {
      copy.usecase = {};
    }
    copy.isReadOnly = false;
    this.setDefaultPaymentChannel(copy);
    return copy;
  }

  /** Shows the paymentd dialog / the invoices UI. */
  public showPaymentDialog() {
    this.invoicesService.showPaymentDialog(this.current);
  }

  /**
   * Cancels the payment or invoice processing for the current invoice.
   */
  public cancel(): Promise<Calculation> {
    return this.uiHelpers.showConfirm("Peru maksuun siirto?", "Maksun tila  muutetaan luonnokseksi ja laskelmaan mahdollisesti liittyvät laskut perutaan. Voit maksaa palkan myös uudella maksutavalla.", "Kyllä, peru maksuun siirto", "Ei").then( (result ) => {
      if (result) {
        const loading = this.uiHelpers.showLoading("Odota...");
        return this.calculationsApi.cancel(this.currentId).then( (item: Calculation) => {
          this.setCurrentValue(item);
          loading.dismiss();
          return this.current;
        });
      } else {
        return this.current;
      }
    });
  }

   /**
    * If true, the form controls should be read-only (no control at all).
    */
  public get isReadOnly() {
    if (this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly || this.isReadOnlyForced)
    {
      // TODO: When moving to ES6, call super.isReadOnly instead.
      return true;
    }
    return this.current.workflow?.status === CalculationStatus.PaymentStarted;
  }

  private setDefaultPaymentChannel(calc: Calculation) {
    if (calc && calc.info && this.sessionService.getSession() && (!calc.info.paymentChannel || calc.info.paymentChannel === PaymentChannel.Undefined) ) {
      calc.info.paymentChannel = this.invoicesService.defaultChannel;
    }
  }
}
