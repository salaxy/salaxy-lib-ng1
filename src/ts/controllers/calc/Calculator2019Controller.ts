import * as angular from "angular";

import {
  Arrays, AvatarPictureType, calcReportType, Calculation, CalculationRowType,
  Calculations, CalculationStatus, CalculatorLogic, Dates, LegalEntityType,
  PensionCalculation, TaxCard2019Logic, TaxcardUiInfo, TaxDeductionWorkCategories,
  UserDefinedRow, WorkerAccount, WorkerLogic, Workers, WorkflowEventUi,
} from "@salaxy/core";

import { InvoicesService, ReportsService, SessionService, UiCrudHelpers, UiHelpers } from "../../services";
import { ApiCrudObjectControllerBindings } from "../bases";
import { CalculationCrudController } from "./CalculationCrudController";
import { CalculatorSections } from "./CalculatorSections";

/**
 * Year 2019 Calculator controller that is not dependent on sigleton Calculations service.
 * This controller extends ApiCrudObjectController (via CalculationCrudController).
 */
export class Calculator2019Controller extends CalculationCrudController {

  /** Component bindings */
  public static crudBindings = (new class extends ApiCrudObjectControllerBindings {
    /** Current tab in initialization. If not set, will be fetched from url hash. */
    public anonOptions = "<";
  }());

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "UiHelpers", "UiCrudHelpers", "$location", "$routeParams", "SessionService", "ReportsService", "Workers", "InvoicesService"];

  /** Type of the chart to show by default. */
  public chartType = "bar";

  /** Options for the behavior of the calculator when in anonymous (demo) mode. */
  public anonOptions = {
    role: "company" as "company" | "household",
  };

  constructor(
    private calcApi: Calculations,
    uiHelpers: UiHelpers,
    protected uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    sessionService: SessionService,
    reportsService: ReportsService,
    protected workersApi: Workers,
    protected invoicesService: InvoicesService,
  ) {
    // Dependency injection
    super(calcApi, reportsService, uiHelpers, $location, $routeParams, workersApi, sessionService, invoicesService);
  }
  /**
   * Initialization code.
   */
  public $onInit() {
    super.$onInit();
  }

  /** Returns the current. Called currentCalc for backward compatibility with the old views. */
  public get currentCalc(): Calculation {
    return this.current;
  }

  /**
   * Gets the Sections object for the current calculation.
   * It keeps track of which section is currently shown and what text etc. is shown in that section.
   */
  public get calcSections(): CalculatorSections {
    return new CalculatorSections(this.current);
  }

  /**
   * Gets or sets the isHouseholdDeductible.
   * This will be moved to a different location in V03, hence the abstraction.
   */
  public get isHouseholdDeductible(): boolean {
    if (!this.current) {
      return false;
    }
    return this.current.salary.isHouseholdDeductible;
  }
  public set isHouseholdDeductible(value: boolean) {
    this.current.salary.isHouseholdDeductible = value;
  }

  /**
   * Gets or sets the taxDeductionCategory (used to be salary.taxDeductionCategories).
   * This will be moved to a different location in V03, hence the abstraction.
   */
  public get taxDeductionCategory(): TaxDeductionWorkCategories {
    return this.current.salary.taxDeductionCategories;
  }
  public set taxDeductionCategory(value: TaxDeductionWorkCategories) {
    this.current.salary.taxDeductionCategories = value;
  }

  /**
   * Returns true if the calculator is in "household" mode (as opposed to default "company" mode).
   */
  public get isHousehold() {
    if (this.sessionService.isInRole("pro") && this.sessionService.isInRole("person")) {
      return true; // All person created calaulations are considered pro for now.
    }
    if (this.sessionService.isInRole("worker")) {
      return false; // Uses calculator for companies in old Worker site (to be replaced by Omapalkka)
    }
    if (this.sessionService.isInRole("anon")) {
      if (this.anonOptions && this.anonOptions.role === "household") {
        return true;
      }
      return false;
    }
    return this.sessionService.isInRole("household");
  }

  /**
   * Gets the first row of the calculation assuring that it exists
   * If there is no current calculation, the calculation is not added.
   */
  public getFirstRow(): UserDefinedRow {
    return CalculatorLogic.getFirstRow(this.current);
  }

  /**
   * Takes the current calculation, creates a copy of it in draft state
   * and sets that calculation as current.
   * This is done typically, when a calculation was in Paid state.
   */
  public copyAsNew() {
    const copy = this.copyItem(this.current);
    this.calcApi.recalculate(copy).then((calc) => {
      this.setCurrentRef(calc);
      this.calcSections.setActive("worktime");
    });
  }

  /**
   * Sets the worker to a given Worker account and does recalculation.
   * This deletes all the rows and resets the employment / worker Id.
   * @param worker Worker account to set as worker.
   * @returns The recalculated calculation and the assigned worker account.
   */
  public setWorker(worker: WorkerAccount): Promise<Calculation> {
    return this.updateFromEmployment(worker.employmentId, true);
  }

  /**
   * Updates the calculation from Employment ID by deleting all the rows and re-setting the employment.
   * @param employmentId Employement Id to set.
   * If null, the employment Id already in calculation is used:
   * This method cannot be used to remove the Worker (extend if necessary).
   * @returns The Calculation after recalc (no saving).
   */
  public updateFromEmployment(employmentId: string = null, updateRows: boolean): Promise<Calculation> {
    if (employmentId) {
      this.current.worker.employmentId = employmentId;
    }
    return this.calcApi.updateFromEmployment(this.current, false, updateRows).then((calc) => {
      super.setCurrentValue(calc);
      return calc;
    });
  }

  /**
   * In anonymous (demo) mode, sets the worker to one of the well-known example Workers:
   * @param id The identifier of the example worker.
   */
  public setExampleWorker(id: "example-default" | "example-17" | "example-pensioner") {
    this.current.worker.accountId = id;
    return this.recalculate();
  }

  /** Gets the example workers in anonymous (demo) mode */
  public getExampleWorkers(): WorkerAccount[] {
    return WorkerLogic.sampleWorkersSingleton;
  }

  /**
   * Reset worker in the calculations.
   */
  public resetWorker() {
    this.currentCalc.worker = CalculatorLogic.getBlank().worker;
  }

  /**
   * Gets misc. logic calculations and operations (client side, based on rows etc.)
   * that affect how the user interface is rendered.
   */
  public get calcLogic() {
    if (!this.current) {
      return {};
    }
    const workerCalc = ((this.current.result || {}).workerCalc || {});
    return {
      /** If true, calcualtion contains compensation. This is useful for customizing texts and other UI elements */
      hasCompensation: !!this.current.rows.find((x) => x.rowType === CalculationRowType.Compensation),
      /** Expenses box total. TODO: We should see can we move this to result / server side! */
      expensesTotal: workerCalc.prepaidExpenses + workerCalc.deductions - workerCalc.unemploymentInsurance - workerCalc.pension - workerCalc.tax,

      /** Sum for child care subsidy. This is shown separately in the UI. */
      childCareSubsidy: (this.current.result && this.current.result.rows) ? Arrays.sum(this.current.result.rows.filter((x) => x.rowType === CalculationRowType.ChildCareSubsidy), (x) => x.total) : null,
    };
  }

  /**
   * Opens the login form for the current user
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login.
   * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   */
  public signIn(redirectUrl: string = null, role: string = null) {
    this.sessionService.signIn(redirectUrl, role);
  }

  /**
   * Opens the login dialog with signUp / register functionality
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login
   * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   */
  public register(redirectUrl: string = null, role: string = null) {
    this.sessionService.register(redirectUrl, role);
  }

  /**
   * Shows a report for the current calculation using a modal dialog.
   * @param reportType - Type of report to show
   */
  public showReportDialog(reportType: calcReportType) {
    this.reportsService.showReportModalForCalc(reportType, this.current);
  }

  /**
   * Deletes the currently selected calculation
   */
  public deleteCalc() {
    this.uiHelpers.showConfirm("Haluatko varmasti poistaa palkkalaskelman?")
      .then((result: boolean) => {
        if (result) {
          const loading = this.uiHelpers.showLoading("Odota...");
          this.deleteNoConfirm(this.current).then(() => {
            this.setCurrentRef(this.api.getBlank());
            loading.dismiss();
            this.$location.url("/");
          });
        }
      });
  }

  /**
   * Recalculates the current calculation using the API.
   * @returns A Promise with result data (Calculation)
   */
  public recalculate(): Promise<Calculation> {
    // Sets the accounts before recalculate and not after in setCurrent.
    if (this.sessionService.isInRole("worker")) {
      this.current.worker.isSelf = true;
    } else {
      this.current.employer.isSelf = true;
    }
    return this.calcApi.recalculate(this.current).then((calc) => {
      // TODO: Check that the spinner works here. Also remove unnecessary spinners in other places.
      super.setCurrentValue(calc);
      return calc;
    });
  }

  /**
   * Sets the current item as the given object (reference to a new object).
   * This also resets the original item and sets the state as loaded.
   * Override also sets either employer or worker based on the current role.
   * @param item The new current item.
   */
  public setCurrentRef(item: Calculation) {
    if (item) {
      if (this.sessionService.isInRole("worker")) {
        item.worker.isSelf = true;
      } else {
        item.employer.isSelf = true;
      }
    }
    super.setCurrentRef(item);
  }

  /** Overrides the save method to allow anonymous saving to local storage */
  public save() {
    /** Not sure whether the local storage saving is enabled anywhere? */
    if (!this.sessionService.isAuthenticated) {
      this.current.id = "anon";
      return this.recalculate().then(() => {
        this.current.worker.accountId = "anon";
        this.current.worker.avatar.id = this.current.worker.accountId;
        this.current.worker.avatar.color = "lime";
        this.current.worker.avatar.displayName = "Rekisteröimätön";
        this.current.worker.avatar.entityType = LegalEntityType.Person;
        this.current.worker.avatar.pictureType = AvatarPictureType.Icon;
        this.setToLocalStorage(this.current);
        return this.current;
      });
    } else {
      if (this.current.worker.accountId === "anon") {
        this.current.worker.accountId = null;
      }
      if (!this.current.worker.accountId) {
        this.deleteFromLocalStorage();
      }
      if (this.current.id === "anon") {
        this.current.id = null;
      }
      return super.save();
    }
  }

  /**
   * Returns validation for current calculation or empty validation object if the calculation has not been recalculated (validated).
   */
  public get validation() {
    return this.currentCalc?.result?.validation || { errors: [], hasAllRequiredFields: true, isValid: true };
  }

  /** Returns date range text. */
  public getDatesRangeText() {
    if (!this.currentCalc.info.workStartDate || !this.currentCalc.info.workEndDate) {
      return "Täytä työn alku- ja loppupäivät!";
    }
    return Dates.getFormattedRange(this.currentCalc.info.workStartDate, this.currentCalc.info.workEndDate)
      + ` (${this.currentCalc.framework.numberOfDays}\xa0päivää)`; // "\xa0" is non-breaking space.
  }

  /** Returns household usecase text */
  public getUsecaseText() {
    if (!this.currentCalc.usecase && !this.currentCalc.framework.numberOfDays) {
      return "Täytä työpäivien määrä sekä valitse tehtävä työ!";
    }
    if (this.currentCalc.usecase && !this.currentCalc.framework.numberOfDays) {
      return ` ${this.currentCalc.usecase.label}. Täytä myös työpäivien määrä!`;
    }
    return Dates.getFormattedRange(this.currentCalc.info.workStartDate, this.currentCalc.info.workEndDate)
      + ` (${this.currentCalc.framework.numberOfDays} päivää, ${this.currentCalc.usecase.label})`;
  }

  /** Returns true if the payment is possible when the employer pays all.  */
  public get isForcePayAllAllowed(): boolean {
    const obj = this.validation.errors.find((x) =>
      (x.type !== "warning"));
    return !obj;
  }

  /**
   * Shows a dialog for adding a new worker.
   * If user goes through the wizard the worker is saved and set to the calculation.
   */
  public showNewWorkerDialog() {
    this.uiCrudHelpers.createNewWorker().then((result) => {
      if (result.action === "ok") {
        if (!result.item.worker.employmentId) {
          throw new Error("Employment ID null in setWorker.");
        }
        this.updateFromEmployment(result.item.worker.employmentId, true);
      }
    });
  }

  /**
   * Shows a dialog for adding a new taxcard to the current worker.
   */
  public showNewTaxcardDialog() {
    if (!this.current.worker || !this.current.worker.paymentData.socialSecurityNumberValid) {
      this.uiHelpers.showAlert("Työntekijää ei ole valittuna tai työntekijällä ei ole henkilötunnusta.");
      return;
    }
    this.uiCrudHelpers.createNewTaxcard(this.current.worker.paymentData.socialSecurityNumberValid).then((result) => {
      if (result.action === "ok") {
        this.recalculate();
      }
    });
  }

  /**
   * Shows a dialog for approving / rejecting a shared taxcard.
   */
  public showApproveTaxcardDialog() {
    if (!this.current.worker || !this.current.worker.employmentId) {
      this.uiHelpers.showAlert("Työntekijää ei ole valittuna tai työntekijällä ei ole henkilötunnusta.");
      return;
    }
    this.uiCrudHelpers.assureTaxcardAndOpenApprove(this.current.worker.employmentId).then(() => {
      this.recalculate();
    });
  }

  /**
   * Shows a dialog for modifying an existing Worker:
   * Saves changes and updates the calculation.
   */
  public showWorkerEditDialog(initialTab?: string) {
    this.uiCrudHelpers.openEditWorkerDialog(this.currentCalc.worker.accountId, "updateCalc" , initialTab as any)
      .then((result) => {
        if (result.action === "ok" || result.action === "ok-no-rows") {
          const loader = this.uiHelpers.showLoading("Päivitetään laskelmaa...");
          if (!result.item.employmentId) {
            throw new Error("No employmentId");
          }
          this.updateFromEmployment(result.item.employmentId, result.action === "ok").then(() => {
            loader.dismiss();
          });
        }
      });
  }

  /** UI related data and texts about the taxcard. */
  public get taxUi(): TaxcardUiInfo {
    return TaxCard2019Logic.getCalcTaxcardInfo(this.current);
  }

  /**
   * Gets the text for pension.
   * @param isOverview If true, returns the shorter text for overview.
   */
  public getPensionText(isOverview = false) {
    // TODO: Move this to UI-specific translations based on enum.
    if (this.current && this.current.result) {
      const pensionCalculation = this.current.result.responsibilities.pensionCalculation;
      switch (pensionCalculation) {
        case PensionCalculation.PartialOwner:
          return isOverview ? "TyEL:n alaista, osaomistaja" : "Työeläkesopimus: TyEL, osaomistaja";
        case PensionCalculation.Entrepreneur:
          return isOverview ? "YEL:n alaista" : "Työeläkesopimus: YEL";
        case PensionCalculation.Farmer:
          return isOverview ? "MYEL:n alaista" : "Työeläkesopimus: MyEL";
        case PensionCalculation.Athlete:
          return isOverview ? "Urheilijan eläkkeen alaista" : "Työeläkesopimus: Urheilijan eläke";
        case PensionCalculation.Compensation:
          return isOverview ? "Työkorvaus" : "Työeläkesopimus: Ei (työkorvaus)";
        case PensionCalculation.BoardRemuneration:
          return isOverview ? "Hallituspalkkio" : "Työeläkesopimus: Ei (hallituspalkkio)";
        case PensionCalculation.SmallEntrepreneur:
          return isOverview ? "YEL, ei vakuuttamisvelvollisuutta" : "Työeläkesopimus: YEL, ei vakuuttamisvelvollisuutta";
        case PensionCalculation.SmallFarmer:
          return isOverview ? "MYEL, ei vakuuttamisvelvollisuutta" : "Työeläkesopimus: MYEL, ei vakuuttamisvelvollisuutta";
        case PensionCalculation.Employee:
        default:
          return isOverview ? "TyEL:n alaista" : "Työeläkesopimus: TyEL";
      }
    }
  }

  /**
   * Sends the payroll from company to PRO.
   * @param message Message to the accountant
   */
   public sendToPro(message: string) {
    const loader = this.uiHelpers.showLoading("Lähetetään", "Lähetetään palkkalistaa tilitoimistolle.");
    this.current.workflow.status = CalculationStatus.ProDraft;
    this.save().then(() => {

      this.api.saveWorkflowEvent(this.current, {
        type:'PartnerMessageClosed',
        ui: WorkflowEventUi.Success,
        message: message ? "Viesti yritykseltä: " + message : "Yritys tarkistanut",
      }).then(() => {
        loader.dismiss();
        this.$location.path("/service");
      });
    });
    return;
  }

  /**
   * Saves a calculation to local storage
   *
   * @param calc - Calculation to store
   */
  private setToLocalStorage(calc: any): void {
    if (window.localStorage) {
      window.localStorage.setItem("salaxy-calculation", JSON.stringify(calc));
    }
  }

  /**
   * Deletes a calculation in local storage
   */
  private deleteFromLocalStorage(): void {
    if (window.localStorage) {
      window.localStorage.removeItem("salaxy-calculation");
    }
  }
}
