import { Calculation, CalculatorLogic } from "@salaxy/core";

/**
 * Defines the panels logic in the Calculator
 */
export class CalculatorPanels {

    /**
     * Creates a new CalculatorPanels helper for a calculation
     * @param calc - The Calculation object for which the helper operates
     */
    constructor(public calc: Calculation) {
        if (!calc || Object.keys(calc).length === 0) {
            calc = CalculatorLogic.getBlank();
        }
    }

    /** Stores the UI properties to the calculation until a roundtrip to server. Then the property is set to null */
    public get ui(): any {
      if (!this.calc) {
        return null;
      }
      const calcAsAny = this.calc as any;
      calcAsAny.$ui = calcAsAny.$ui || {};
      calcAsAny.$ui.activePanels = calcAsAny.$ui.activePanels || [];
      return calcAsAny.$ui;
    }

    /**
     * Toggles the user interface panels active (open/close) in a hierarchical way.
     * The functionality mimics a hierarchical panel/menu structure in HTML / components that are not hierarchical.
     * See the example to better understand the logic.
     * Hierarchy logic is supported to 2 levels only - it is not tested on deeper hierarchy though it may function.
     * @param panels - Identifiers of panel or two panels (panel and its child) that should be toggled.
     *
     * @example
     * Assume we have these two panels open: "panel1", "panel2"
     *
     * - togglePanel("panel1", "panel2") => ["panel1"]      // Normal 2nd level toggle opens and closes the last level.
     * - togglePanel("panel2") => ["panel1"]                // Alternative explicit last level close
     * - togglePanel("panel1") => []                        // Normal 1st level toggle opens and closes the first level
     * - togglePanel("panel3") => ["panel3"]                // Opening another "path" of the hierarchy
     * - togglePanel("panel3", "panel4") => ["panel3", "panel4"]
     * - togglePanel("panel1", "panel4") => ["panel1", "panel4"]    // Being a separate "path" is determined by last element only
     * - togglePanel("last") => ["panel1"]                  // Special keyword for closing the last panel
     * - togglePanel() => []                                // Closes all panels
     */
    public toggleActivePanel(...panels: any[]) {
        if (panels.length === 0 || !panels[0]) {
            // Closing all
            this.ui.activePanels = [];
            return;
        }
        if (panels[0] === "last") {
            // Closing last
            if (this.ui.activePanels.length > 0) {
                this.ui.activePanels.splice(this.ui.activePanels.length - 1);
            }
            return;
        }
        const positionOfLastPanel = this.ui.activePanels.indexOf(panels[panels.length - 1]);
        if (positionOfLastPanel === -1) {
            // New hierarchy path
            this.ui.activePanels = panels;
        } else {
            // Remove starting from the match
            this.ui.activePanels.splice(positionOfLastPanel, this.ui.activePanels.length);
        }
    }

    /**
     * Returns true if the panel is active (open).
     * @param panel - identifier of the panel.
     */
    public isPanelActive(panel: string) {
        return (this.ui.activePanels as string[]).indexOf(panel) >= 0;
    }
}
