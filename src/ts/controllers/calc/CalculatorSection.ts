/** Logical section / area of the calculator ui. */
export class CalculatorSection {

  /** Creates a new CalculatorSection
   * @param isActive - If true, the section is currently active / selected in te user interface.
   * @param isSelectionDone - If true, user has selected some values for the section
   * @param title - Short text title for the section
   */
  constructor(public isActive: boolean, public isSelectionDone: boolean, public title: string) {
  }

  /**
   * Shortcut for isSelectionDone or isActive.
   * This is when the overview window should be shown colored
   */
  get isOverviewActive() {
      return this.isSelectionDone || this.isActive;
  }
}
