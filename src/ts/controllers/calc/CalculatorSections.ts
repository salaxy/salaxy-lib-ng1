import { Calculation, CalculationStatus, CalculatorLogic, Numeric, UnionPaymentType } from "@salaxy/core";
import { CalculatorSection } from "./CalculatorSection";

/** Sections defines areas of the calculator: worker, household-usecase, worktime, salary, expenses and results. */
export class CalculatorSections {

  /**
   * Creates a new CalculatorSections helper for a calculation
   * @param calc The Calculation object for which the helper operates
   */
  constructor(public calc: Calculation) {
    if (!calc || Object.keys(calc).length === 0) {
      this.calc = CalculatorLogic.getBlank();
    }
  }

  /** Stores the UI properties to the calculation until a roundtrip to server. Then the property is set to null */
  public get ui(): any {
    return CalculatorLogic.getUiCache(this.calc);
  }

  /**
   * Activates a section in the calculator (opens the detail view).
   * @param section - Name of the section
   */
  public setActive(section: string) {
    this.ui.activeSection = section;
  }

  /**
   * Toggles the section
   */
  public toggleActive(section: string) {
    this.ui.activeSection = this.ui.activeSection === section ? null : section;
  }

  /**
   * Toggles the section active / non-active.
   */
  public toggle(section: string) {
    if (this.ui.activeSection === section) {
      this.ui.activeSection = null;
    } else {
      this.ui.activeSection = section;
    }
  }

  /** Gets a section */
  public get(section: string) {
    const isActive = this.ui.activeSection === section;
    switch (section) {
      case "worker":
        return new CalculatorSection(
          isActive,
          this.calc.worker && this.calc.worker.avatar.id != null,
          (this.calc.worker && this.calc.worker.avatar.displayName)
            ? this.calc.worker.avatar.displayName
            : "Oletustiedot",
        );
      case "household-usecase":
        return new CalculatorSection(
          isActive,
          this.calc.usecase == null || this.calc.usecase.label != null,
          null, // No longer in use
        );
      case "worktime":
        return new CalculatorSection(
          isActive,
          !!(this.calc.info.workStartDate && this.calc.info.workEndDate),
          null, // No longer in use in Calculator2018
        );
      case "expenses": {
        const expenses = CalculatorLogic.getTotalForCategory(this.calc, "expenses");
        const benefits = CalculatorLogic.getTotalForCategory(this.calc, "benefits");
        const deductions = CalculatorLogic.getTotalForCategory(this.calc, "deductions");
        const isHouseholdDeductible = this.calc.salary.isHouseholdDeductible;
        const unionPaymentType = (this.calc.framework as any).unionPayment;
        return new CalculatorSection(
          isActive,
          ((expenses + benefits > 0) || (deductions > 0) || isHouseholdDeductible || (unionPaymentType && unionPaymentType !== UnionPaymentType.NotSelected)),
          Numeric.toFixed(expenses + benefits, 2) + " € - " + Numeric.toFixed(deductions, 2) + " €",
        );
      }
      case "salary": {
        const allSalary = CalculatorLogic.getTotalForCategory(this.calc, "salary");
        const salaryAdditions = CalculatorLogic.getTotalForCategory(this.calc, "salaryAdditions");
        return new CalculatorSection(
          isActive,
          allSalary > 0,
          (
            allSalary === 0
              ? "0 €"
              : salaryAdditions > 0
                ? Numeric.toFixed(this.calc.result.totals.totalBaseSalary, 2) + " + " + Numeric.toFixed(salaryAdditions, 2) + " €"
                : Numeric.toFixed(this.calc.result.totals.totalBaseSalary, 2) + " €"
          ),
        );
      }
      case "result": {
        const isResultsActive = (this.calc.result && this.calc.result.totals &&
          (this.calc.rows.length > 0 || this.calc.result.totals.total !== 0 || this.calc.workflow.status !== CalculationStatus.Draft)) || false;
        return new CalculatorSection(
          isResultsActive,
          isResultsActive,
          "Palkkalaskelma",
        );
      }
    }
  }
}
