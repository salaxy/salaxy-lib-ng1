import * as angular from "angular";

import { Barcodes, Dates, Invoice, InvoicePreview, Numeric } from "@salaxy/core";

/**
 * Shows an invoice as credit transfer: Either as printable display, copy-pasteable barcode or both.
 */
export class CreditTransferController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope"];

  /** Invoice that is shown as credit transfer: The main data object. */
  public invoice: Invoice;

  /**
   * Display mode is either
   *
   * - "official": The official printable credit transfer (Finanssialan keskusliitto)
   * - "barcode-copy": The copy-paste input + button (in a div) for copy pasting the the barcode into a web bank
   * - "default": Both of the above separted by an explaining text.
   */
  public mode: "default" | "official" | "barcode-copy";

  /**
   * Angular barcode display options.
   * See https://github.com/isonet/angular-barcode for details.
   */
  public bcOptions = {
    width: 1.42,
    height: 35,
    displayValue: false,
    marginTop: 15,
    format: "CODE128C",
  };

  private _invoicePreview: InvoicePreview;

  /**
   * Creates a new CreditTransferController.
   * @ignore
   */
  constructor(private $scope: angular.IScope) {
    // Dependency injection if necessary.
  }

  /** Copies the barcode from input to clipboard. */
  public copyBarcode(): void {
    const id = "barcode" + this.getId();
    (document.getElementById(id) as HTMLInputElement).select();
    document.execCommand("copy");
  }

  /** Gets a unique id for the barcode input (from which the copying to clipboard is done). */
  public getId(): number {
    return this.$scope.$id;
  }

  /**
   * Sets the Invoice based on an invoice preview (list item based preview) if the invoice is not empty.
   * I.e. if you wish to set to null, you need to set the invoice null separately.
   * Gets the original preview object if set.
   */
  public get invoicePreview(): InvoicePreview {
    return this._invoicePreview;
  }
  public set invoicePreview(value: InvoicePreview) {
    this._invoicePreview = value;
    if (value?.invoice) {
      this.invoice = this.getInvoice(value);
    }
  }

  /** Barcode value */
  public get bcValue(): string {
    if (!this.invoice?.recipient.iban) {
      return "";
    }
    return Barcodes.getValue(
      this.invoice.recipient.iban || "",
      this.invoice.header.referenceNumber || "",
      Dates.asDate(this.invoice.header.dueDate),
      Numeric.round(this.invoice.header.total * 100, 0));
  }

  /** Maps an invoice preview object to invoice. */
  private getInvoice(preview: InvoicePreview): Invoice {
    if (!preview?.invoice) {
      return null;
    }
    return {
      header: {
        dueDate: preview.invoice.data.dueDate,
        total: preview.invoice.payment,
        referenceNumber: preview.invoice.reference,
      },
      rows: [],
      payer: {
        avatar: preview.invoice.ownerInfo.avatar,
        officialId: preview.invoice.ownerInfo.officialId,
        contact: {
          email: preview.invoice.ownerInfo.email,
          telephone: preview.invoice.ownerInfo.telephone,
        },
      },
      recipient: {
        fullName: preview.invoice.otherPartyInfo.avatar.displayName,
        iban: preview.invoice.otherPartyInfo.ibanNumber,
        officialId: preview.invoice.otherPartyInfo.officialId,
        contact: {
          email: preview.invoice.otherPartyInfo.email,
          telephone: preview.invoice.otherPartyInfo.telephone,
        },
      },
    };
  }
}
