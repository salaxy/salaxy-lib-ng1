import * as angular from "angular";

import {
  Avatar,
  Calculation,
  InvoicesLogic,
  PaymentChannel,
  PayrollDetails,
  Translations,
} from "@salaxy/core";

import { InvoicesService } from "../../services";

/**
 * Contains functionality for executing payments for payrolls or calculations.
 */
export class PaymentController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["InvoicesService"];

  /**
   * The current business item that is being paid: Either Calculation or PayrollDetails.
   * Currently only supports direct reference. May later support 'url' etc. like CRUD controllers.
   */
  public model: Calculation | PayrollDetails

  /** Button text set from the element */
  public label: string;

  /** Mode is either "pay-button" (default), "channel-select" or "button-and-channel" */
  public mode: "pay-button" | "channel-select" | "button-and-channel";

 /**
  * Function that is called when the payment channel has been chagned
  * Function has the following locals:
  * paymentChannel: the selected payment channel.
  * businessObject: the current business object which has been changed.
  */
  public onPaymentChannelChange: (params: {
    /** Selected payment channel */
    paymentChannel: PaymentChannel,
    /** The current business object which has been changed.  */
    businessObject: Calculation | PayrollDetails,
  }) => void;

  constructor(private invoicesService: InvoicesService) {
  }

  /**
   * Initializes the controller
   */
  public $onInit() {
    // Init here
  }

  /**
   * Text for the payment button
   * This is typically customized in custom payment scenarios.
   */
  public getPaymentButtonText(): string {
    return this.label ? Translations.get(this.label) : Translations.get("SALAXY.UI_TERMS.pay");
  }

  /**
   * Shows the payment process first page in a dialog (the invoice create process).
   */
  public goToInvoices() {
    this.invoicesService.showPaymentDialog(this.businessObject);
  }

  /** Returns current business object */
  public get businessObject(): Calculation | PayrollDetails {
    return this.model;
  }

  /**
   * Returns true if there is a selected channel for the business object.
   */
  public get isChannelDefined() {
    return this.getCurrentChannel() != PaymentChannel.Undefined
  }

  /** Gets the current channel avatar. */
  public get currentChannel(): Avatar {
    return InvoicesLogic.getChannelAvatar(this.getCurrentChannel());
  }

  /** Sets the payment channel and starts recalculation. */
  public setPaymentChannel(newChannel: PaymentChannel) {
    if (InvoicesLogic.isCalculation(this.businessObject)) {
      this.businessObject.info.paymentChannel = newChannel;
    } else if (InvoicesLogic.isPayroll(this.businessObject)) {
      this.businessObject.input.paymentChannel = newChannel;
    } else {
      throw new Error("Unknwon business object type.");
    }
    this.onPaymentChannelChange({ paymentChannel: newChannel, businessObject: this.businessObject });
  }

  /** Gets the enabled channels. */
  public get allChannels(): Avatar[] {
    return this.invoicesService.channels;
  }

  private getCurrentChannel(): PaymentChannel {
    let channel = null;
    if (InvoicesLogic.isCalculation(this.businessObject)) {
      channel = this.businessObject.info.paymentChannel;
    } else if (InvoicesLogic.isPayroll(this.businessObject)) {
      channel = this.businessObject.input.paymentChannel;
    }
    return channel ?? PaymentChannel.Undefined;
  }
}
