import * as angular from "angular";

import {
  ApiItemType, ApiListItemValidation, calcReportType, Calculation, CalculationResult, CalculationResultLogic, Calculations,
  CalendarActionType, CalendarEvent, CalendarEvents, Dates, PaymentChannel, Payroll03Info, PayrollDetails,
  Payrolls, PayrollStatus, Translations, WorkflowEventUi
} from "@salaxy/core";

import { InvoicesService, ReportsService, SessionService, UiCrudHelpers, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { PayrollLogic } from "./PayrollLogic";

/**
 * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
 * Typical usecase is that a a company has e.g. a monthly salary list that is paid
 * at the end of month. For next month, a copy is then made from the latest list and
 * the copy is potentially modified with the changes of that particular month.
 */
export class PayrollCrudController extends ApiCrudObjectController<PayrollDetails> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Payrolls", "UiHelpers", "UiCrudHelpers", "$location", "$routeParams", "Calculations", "ReportsService", "SessionService", "InvoicesService", "CalendarEvents"];

  /** Mode */
  public mode: "service" | null;

  /** Index of the active tab in the combined edit view. */
  public activeTab = 1;

  /** TODO: Items to refactor before going to production. */
  /** Cache for changedCalcCount so that it is called only once within on $digest loop. */
  protected changedCalcCountCache;

  /** User interface related properties. */
  private calcUiProperties: {
    /** Calculation ID */
    id: string,
    /** If true, the calc is in edit mode. */
    isInEdit: boolean,
    /** If true, the calculation is in collapsed mode (not showing the details) */
    isCollapsed: boolean,
  }[] = [];

  /**
   * Reference to the latest calculation result
   */
  private _calcsResult: CalculationResult;

  /**
   * Reference to empty calculation array.
   * Prevents infinite $digest iterations, when the reference to empty array does not change.
   */
  private emptyCalcList: Calculation[] = [];

  /** Ui Initialization */
  private isUiInitialized = false;

  /** Available channels */
  private _availableChannels: Array<{ value: string, text: string }> = [];

  constructor(
    private payrollsApi: Payrolls,
    uiHelpers: UiHelpers,
    private uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private calcsApi: Calculations,
    private reportsService: ReportsService,
    private sessionService: SessionService,
    private invoicesService: InvoicesService,
    private calendarEvents: CalendarEvents,
  ) {
    // Dependency injection
    super(payrollsApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Initialization code.
   */
  public $onInit() {
    super.$onInit();
  }

  /** Catched the return of reload to update values. */
  public reload(): Promise<PayrollDetails> {
    return super.reload().then((payroll) => {
      if (super.isNew()) {
        this.setPaymentChannel(payroll);
      }

      if (!this.isUiInitialized && this.mode === "service") {
        this.setUiForAll("isCollapsed", false);
      }
      this.isUiInitialized = true;
      return payroll;
    });
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/payroll",
      detailsUrl: this.detailsUrl || "/payroll/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/Payrolls.html",
      oDataOptions: {},
    };
  }

  /** Returns true if any calculations have been added to the Payroll */
  public get hasCalculations() {
    return this.current && this.current.calcs.length > 0;
  }

  /** Save changes to the payroll as well as to changed calculations */
  public save(): Promise<PayrollDetails> {
    if (this.changedCalcCount > 0) {
      // Just to not show "Saving calculations..."
      return this.commitAllCalcs().then(() => {
        return super.save();
      });
    }
    return super.save();
  }

  /**
   * Creates a copy of a payroll
   * This is a synchronous method that should basically convert a saved item to a new item.
   * @param copySource Item (container item) to copy as new.
   */
  public copyItem(copySource: PayrollDetails): PayrollDetails {
    const oldId = copySource.id;
    const copy = super.copyItem(copySource);
    if (copy.info.status === PayrollStatus.Template) {
      copy.input.template = oldId;
      copy.info.status = PayrollStatus.Draft;
    }
    // HACK: remove payroll-service flag. This will be replaced with invoicing flags eventually.
    if (copy.usecase &&
      ( copy.usecase.uri === "palkkaus.fi/usecases/payroll-service/pro-calc" ||
      copy.usecase.uri === "palkkaus.fi/usecases/payroll-service/calc")) {
      copy.usecase = {};
    }
    return copy;
  }

  /**
   * Reloads the validations and info for the current item.
   * These are constructed based on calculations data and if changed, will update the Payroll in storage.
   */
  public reloadValidation() {
    // TODO: Add indication that validation is being updated.
    // TODO: This would become obsolete when we separate calculations modification to a separate controller.
    if (this.currentId) {
      this.api.getSingle(this.currentId).then((payroll) => {
        this.current.validations = payroll.validations;
        this.current.info = payroll.info;
        this.original.validations = payroll.validations;
        this.original.info = payroll.info;
      });
    }
  }

  /** Gets a validation object for a calculation */
  public getValidation(calc: Calculation): ApiListItemValidation {
    return this.current.validations.find((x) => x.id === calc.id) || {
      id: calc.id, // Paid Payrolls will not have validations.
      isValid: true,
    };
  }

  /**
   * Returns true if the date is overdue (smaller than today).
   * @param date Date to compare to today.
   */
  public isOverdue(date: string): boolean {
    date = Dates.asDate(date);
    return date < Dates.getToday();
  }

  /** Gets the list of calculations in this payroll object */
  public get calcs(): Calculation[] {
    return this.current ? this.current.calcs : this.emptyCalcList;
  }

  /** Returns the constantly updating info component from the open calculations (without round-trip to server) */
  public get info(): Payroll03Info {
    return PayrollLogic.getInfo(this.current, this.calcs);
  }

  /** Summarized CalculationResult for all calculations in the payroll. */
  public get calcsResult(): CalculationResult {
    const result = CalculationResultLogic.add(this.calcs.map((x) => x.result));
    if (!angular.equals(result, this._calcsResult)) {
      this._calcsResult = result;
    }
    return this._calcsResult;
  }

  /** Shows the create/edit event (scheduling) dialog for the current payroll. */
  public showEditEvent(): void {
    const loader = this.uiHelpers.showLoading();
    this.getCalendarEvent().then((ev) => {
      loader.setHeading("SALAXY.UI_TERMS.edit");
      this.uiHelpers.openEditCalendarEvent(ev, "SALAXY.NG1.PayrollComponent.showEditEvent.title", "simplified").then((dialogResult) => {
        if (dialogResult.action === "ok") {
          loader.setHeading("SALAXY.UI_TERMS.isSaving");
          this.calendarEvents.save(dialogResult.item).then(() => {
            this.reload(); // Fetch data to own workflow.
            loader.dismiss();
          });
        } else if (dialogResult.action === "delete") {
          loader.setHeading("SALAXY.UI_TERMS.isDeleting");
          this.calendarEvents.delete(dialogResult.item.id).then(() => {
            this.reload();
            loader.dismiss();
          });
        } else {
          loader.dismiss();
        }
      });
    });
  }

  /**
   * Checs if user is in one of the given roles.
   * @param roles A comma separated list of roles, potentially with exlamation mark
   */
  public isInSomeRole(roles: string): boolean {
    return this.sessionService.isInSomeRole(roles)
  }

  /**
   * Gets a workflow event from the current payroll.
   * @param type Type of the event.
   */
  public getWorkflowEvent(type: string) {
    return this.current.workflowData.events.find((x) => x.type?.toLowerCase() === type?.toLowerCase());
  }

  /**
   * Gets a calendar event of the current Payroll:
   * Either one that is stored or a new one created from the Payroll.
   */
  public getCalendarEvent(): Promise<CalendarEvent> {
    const existing = this.getWorkflowEvent("calendarEvent");
    if (existing) {
      return this.calendarEvents.getSingle(existing.id);
    }
    const newEvent: CalendarEvent = this.calendarEvents.getBlank();
    newEvent.event.summary = Translations.get("SALAXY.NG1.PayrollComponent.newEvent.summaryPre") + this.current.input.title;
    newEvent.event.description = Translations.get("SALAXY.NG1.PayrollComponent.newEvent.descriptionPre") + this.current.input.title;
    newEvent.event.start = this.current.input.salaryDate;
    newEvent.event.isAllDay = true;
    newEvent.bo = {
      type: ApiItemType.PayrollDetails,
      id: this.current.id,
    };
    newEvent.event.actions.push({
      type: CalendarActionType.CreateItem,
      summary: Translations.get("SALAXY.NG1.PayrollComponent.newEvent.createSummary"),
      description: Translations.get("SALAXY.NG1.PayrollComponent.newEvent.createDescription"),
      triggerDuration: "-15",
    });
    const period = this.current.input.period;
    if (period?.start && period.end) {
      // HACK: Just a quick hack, make real implementation with https://github.com/jakubroztocil/rrule
      const duration = Dates.getDuration(period.start, period.end).add(1, "d");
      if (duration.asMonths() > 0.9 && duration.asMonths() < 1.1) {
        newEvent.event.recurrenceRules.push("FREQ=MONTHLY;INTERVAL=1");
      } else {
        const days = Math.round(duration.asDays());
        if (days % 7 === 0) {
          newEvent.event.recurrenceRules.push(`FREQ=WEEKLY;INTERVAL=${Math.round(duration.asWeeks())}`);
        } else {
          newEvent.event.recurrenceRules.push(`FREQ=DAILY;INTERVAL=${days}`);
        }
      }
    }
    return Promise.resolve(newEvent);
  }

  /** Gets the UI properties for a calculation */
  public getUi(calc: Calculation): {
    /** Calculation ID */
    id: string,
    /** If true, the calc is in edit mode. */
    isInEdit: boolean,
    /** If true, the calculation is in collabsed mode (not showing the details) */
    isCollapsed: boolean,
  } {
    if (!calc || !calc.id) {
      return {} as any;
    }
    let ui = this.calcUiProperties.find((x) => x.id === calc.id);
    if (ui == null) {
      ui = {
        id: calc.id,
        isCollapsed: true,
        isInEdit: false,
      };
      this.calcUiProperties.push(ui);
    }
    return ui;
  }

  /**
   * Set calculation rows editable
   */
  public setRowsEditable(calc: Calculation | "all") {
    if (calc === "all") {
      this.setUiForAll("isInEdit", true);
      this.setUiForAll("isCollapsed", false);
    } else {
      this.getUi(calc).isCollapsed = false;
      this.getUi(calc).isInEdit = false; // First to false to assure firing of the setter.
      this.getUi(calc).isInEdit = true;
    }
  }

  /**
   * Sets a UI property for all calculations.
   * @param prop Name of the property to set.
   * @param value New value for the property.
   */
  public setUiForAll(prop: "isCollapsed" | "isInEdit", value: boolean) {
    this.calcs.forEach((calc) => {
      this.getUi(calc)[prop] = value;
    });
  }

  /**
   * Recalculates and saves calculation to the server.
   * @param calc Calculation to save.
   * @param noReloadValidation If true, the parent should call reloadValidation().
   * @param forceSave If true, changes are saved even when there are no changes.
   */
  public commitCalc(calc: Calculation, noReloadValidation = false, forceSave = false): Promise<{
    /** Calculation (after save, if saved) */
    calc: Calculation,
    /** True, if the calculation was saved. Unchanged calculations are not saved. */
    isSaved: boolean,
  }> {
    this.getUi(calc).isInEdit = false;
    if (forceSave || this.hasCalcChanges(calc)) {
      return this.payrollsApi.calculationSave(calc, this.current.id).then((result) => {
        if (!noReloadValidation) {
          this.reloadValidation();
        }
        return this.setCalc(result);
      });
    }
    return Promise.resolve({ calc, isSaved: false });
  }

  /** Sets the calculation after round-trip from the server. */
  public setCalc = (newCalc: Calculation) => {
    const oldCalc = this.current.calcs.find((x) => x.id === newCalc.id);
    const originalCalc = this.original.calcs.find((x) => x.id === newCalc.id);
    angular.copy(newCalc, oldCalc);
    angular.copy(newCalc, originalCalc);
    return { calc: newCalc, isSaved: true };
  }

  /**
   * Reset any modifications to a calculation.
   * @param calc Calculation that was edited.
   */
  public resetCalc(calc: Calculation) {
    this.getUi(calc).isInEdit = false;
    if (this.hasCalcChanges(calc)) {
      const originalCalc = this.original.calcs.find((x) => x.id === calc.id);
      angular.copy(originalCalc, calc);
    }
  }

  /** Gets the count of changed calculations. */
  public get changedCalcCount() {
    // Cache within one $digest loop.
    this.changedCalcCountCache = this.calcs.filter((x) => this.hasCalcChanges(x)).length;
    return this.changedCalcCountCache;
  }

  /** Returns true if a calculation has changes to its original. */
  public hasCalcChanges(calc: Calculation): boolean {
    // TODO: Move this check to be run in every $digest loop
    const originalCalc = this.original.calcs.find((x) => x.id === calc.id);
    if (!originalCalc) {
      throw new Error("Unexpected: No original calc in hasCalcChanges.");
    }
    return !angular.equals(originalCalc, calc);
  }

  /**
   * Commits changes to all calculations that are in edit mode.
   * @returns A promise with the number of saved (changed) calculations.
   */
  public commitAllCalcs(): Promise<number> {
    const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayrollComponent.commitAllCalcs.heading");
    this.setUiForAll("isCollapsed", true);
    this.setUiForAll("isInEdit", false);
    const promises = this.calcs.map((x) => this.commitCalc(x, true));
    return Promise.all(promises).then((result) => {
      loader.dismiss();
      this.reloadValidation();
      return result.filter((x) => x.isSaved).length;
    });
  }

  /** Resets all calculations to their unsaved state. */
  public resetAllCalcs() {
    this.calcs.forEach((x) => this.resetCalc(x));
  }

  /**
   * Opens DateRange component in modal dialog.
   * If daterange is changed asks whether all calculations should be changed to a new period.
   * Saves changes to server.
   */
  public showDateRange() {
    this.uiHelpers.showDateRange(this.current.input.period, "Palkkakausi", "Tallenna").then((result) => {
      if (result.result === "ok" && result.hasChanges && this.calcs.length > 0) {
        this.uiHelpers.showConfirm("Muutetaanko kaikki laskelmat?",
          "Muutetaanko periodi kaikissa laskelmissa? Vaihtoehtona on vaihtaa periodi vain palkkalistaan ja jättää laskelmiin nykyiset päivämäärät.",
          "Kyllä", "Ei").then((value) => {
            if (value) {
              this.calcs.forEach((calc) => {
                calc.info.workStartDate = this.current.input.period.start;
                calc.info.workEndDate = this.current.input.period.end;
                calc.framework.numberOfDays = this.current.input.period.daysCount;
              });
              this.save();
            } else {
              super.save();
            }
          });
      }
    });
  }

  /**
   * Shows a DateRange edit component for a single calculation.
   * @param calc Calculation to modify.
   */
  public showDateRangeForCalculation(calc: Calculation) {
    this.uiHelpers.showDateRange(calc, "Palkkakausi", "OK");
  }

  /**
   * Shows the calculation in a new dialog.
   * @param calc Calculation to modify / view in dialog.
   */
  public showEditCalcDialog(calc: Calculation, dialog: "default" | "ir" = "default"): Promise<Calculation> {
    const template = dialog === "ir" ? "salaxy-components/modals/calc/CalcIr.html" : "salaxy-components/modals/calc/Calc.html";
    return this.uiHelpers.openEditDialog(
      template,
      calc,
      {
        title: "Muokkaa palkkalaskelmaa",
      },
      "lg",
    ).then((result) => {
      if (result.action === "ok") {
        if (result.hasChanges) {
          return this.commitCalc(result.item).then((result) => {
            return result.calc;
          });
        } else {
          return calc;
        }
      } else if (result.action === "delete") {
        this.deleteCalc(calc);
        return null;
      } else {
        this.resetCalc(calc);
        return calc;
      }
    });
  }

  /**
   * Shows a dialog for editing an existing worker.
   * @param calc - Calculation that should be edited
   */
  public showEditWorkerDialog(calc: Calculation): void {
    this.uiCrudHelpers.openEditWorkerDialog(calc.worker.accountId, "updateCalc").then((result) => {
      if (result.action === "ok" || result.action === "ok-no-rows") {
        const loader = this.uiHelpers.showLoading("Päivitetään laskelmaa...");
        this.calcsApi.updateFromEmployment(calc, true, result.action === "ok").then((result) => {
          this.reloadValidation();
          this.setCalc(result);
          loader.dismiss();
        });
      }
    });
  }

  /**
   * Shows a dialog for adding a new taxcard to the current worker.
   * If user goes through the wizard the worker is saved and set to the calculation.
   */
  public showNewTaxcardDialog(calc: Calculation) {
    if (!calc.worker || !calc.worker.paymentData.socialSecurityNumberValid) {
      this.uiHelpers.showAlert("Työntekijää ei ole valittuna tai työntekijällä ei ole henkilötunnusta.");
      return;
    }
    this.uiCrudHelpers.createNewTaxcard(calc.worker.paymentData.socialSecurityNumberValid).then((result) => {
      if (result.action === "ok") {
        this.commitCalc(calc, false, true);
      }
    });
  }

  /**
   * Shows a dialog for approving / rejecting a shared taxcard.
   */
  public showApproveTaxcardDialog(calc: Calculation) {
    if (!calc.worker || !calc.worker.employmentId) {
      this.uiHelpers.showAlert("Työntekijää ei ole valittuna tai työntekijällä ei ole henkilötunnusta.");
      return;
    }
    this.uiCrudHelpers.assureTaxcardAndOpenApprove(calc.worker.employmentId).then(() => {
      this.commitCalc(calc, false, true);
    });
  }

  /**
   * Shows a confirm dialog if payroll has any calculations.
   * Returns true if a user clicks OK -button. Otherwise (cancel or close) returns false.
   * @returns True if the user confirmed the operation.
   */
  public importEmploymentDataAll(): Promise<boolean> {
    const payroll = this.current as any;
    if (!payroll.info.calcCount || payroll.info.calcCount === 0) {
      return this.uiHelpers.showAlert("Ei laskelmia", "Palkkalistassa ei ole laskelmia. Työntekijöitä ei ole päivitetty.").then(() => false);
    } else {
      return this.uiHelpers.showConfirm(
        "Päivitä laskelmat työsuhteen tiedoista",
        "Kaikkien laskelmien kaikki lisätyt rivit nollautuvat ja laskelmat päivittyvät suoraan työsuhteen tiedoista. Samalla päivittyvät myös ammattiluokka ja 'kuvaus palkkalaskelmaan'.",
        "Jatka",
        "Peruuta",
      ).then((result: boolean) => {
        if (result) {
          const loader = this.uiHelpers.showLoading("Haetaan tietoja...");
          const promises = this.calcs.map((x) => this.calcsApi.updateFromEmployment(x, true, true).then((result) => this.setCalc(result)));
          return Promise.all(promises).then(() => {
            loader.dismiss();
            this.reloadValidation();
            return true;
          });
        } else {
          return false;
        }
      });
    }
  }

  /**
   * Adds default rows (SalaryDefault) from employment information to calculation.
   * Also saves the calculation
   * @param calc The calculation to update.
   */
  public importEmploymentData(calc: Calculation): Promise<{
    /** Calculation (after save, if saved) */
    calc: Calculation,
    /** True, if the calculation was saved. Unchanged calculations are not saved. */
    isSaved: boolean,
  }> {
    return this.uiHelpers.showConfirm(
      "Päivitä laskelma työsuhteen tiedoista",
      "Laskelman kaikki lisätyt rivit nollautuvat ja rivit päivittyvät suoraan työsuhteen tiedoista. Samalla päivittyvät myös ammattiluokka ja 'kuvaus palkkalaskelmaan'.",
      "Jatka",
      "Peruuta",
    ).then((result: boolean) => {
      if (result) {
        const loader = this.uiHelpers.showLoading("Haetaan tietoja...");
        return this.calcsApi.updateFromEmployment(calc, true, true).then((result) => {
          loader.dismiss();
          this.reloadValidation();
          return this.setCalc(result);
        });
      } else {
        return Promise.resolve({ calc, isSaved: false });
      }
    });
  }

  /**
   * Show confirm dialog and export employment data from calculation to Employment relation.
   * If the calculation has unmodified changes, the calculation is saved.
   * @param calc Calculation that should be exported to Employment.
   * @returns True if the user confirmed the operation.
   */
  public exportEmploymentData(calc: Calculation): Promise<boolean> {
    return this.uiHelpers.showConfirm(
      "Vie rivit työntekijän tietoihin",
      "Työsuhteen tietoihin tallennetaan tämän laskelman rivit ja ne tulevat jatkossa uusiin laskelmiin ja palkkalistoihin. Samalla päivittyvät myös ammattiluokka ja 'kuvaus palkkalaskelmaan'.",
      "Jatka",
      "Peruuta",
    ).then((result: boolean) => {
      if (result) {
        const loader = this.uiHelpers.showLoading("Viedään tietoja...");
        this.exportEmploymentDataNoConfirm(calc).then((resultInner) => {
          if (resultInner.isSaved) {
            this.reloadValidation();
          }
          loader.dismiss();
          return true;
        });
      } else {
        return false;
      }
    });
  }

  /**
   * Show confirm dialog and export employment data from all calculations
   * to their respective Employment relations.
   * If any of calculations have unmodified changes, they are saved.
   * @returns True if the user confirmed the operation.
   */
  public exportEmploymentDataAll(): Promise<boolean> {
    const payroll = this.current as any;
    if (!payroll.info.calcCount || payroll.info.calcCount === 0) {
      return this.uiHelpers.showAlert("Ei laskelmia", "Palkkalistassa ei ole laskelmia. Työntekijöitä ei ole päivitetty.").then(() => false);
    } else {
      return this.uiHelpers.showConfirm(
        "Vie rivit työntekijän tietoihin",
        "Kaikkien laskelmien työsuhteen tietoihin tallennetaan tämän palkkalistan rivit. Tämän palkkalistan rivit tulevat siis jatkossa uusiin laskelmiin ja palkkalistoihin. Samalla päivittyvät myös ammattiluokka ja 'kuvaus palkkalaskelmaan'.",
        "Jatka",
        "Peruuta",
      ).then((result: boolean) => {
        if (result) {
          const loader = this.uiHelpers.showLoading("Viedään tietoja...");
          const promises = this.calcs.map((x) => this.exportEmploymentDataNoConfirm(x));
          return Promise.all(promises).then((resultInner) => {
            if (resultInner.find((x) => x.isSaved)) {
              this.reloadValidation();
            }
            loader.dismiss();
            return true;
          });
        } else {
          return false;
        }
      });
    }
  }

  /**
   * Deletes the calculation
   * @param calc Calculation to delete
   * @param unlink If true, will not delete the actual calculation, but instead unlinks it as a separate calculation outside this payroll.
   */
  public deleteCalc(calc: Calculation, unlink = false) {

    if (unlink) {
      this.uiHelpers.showConfirm("SALAXY.NG1.PayrollComponent.payrollCalcs.unlinkCalcDialog.heading",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.unlinkCalcDialog.intro",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.unlinkCalcDialog.confirmButton",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.unlinkCalcDialog.cancelButton")
        .then((result: boolean) => {
          if (result) {
            const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayrollComponent.payrollCalcs.unlinkCalcDialog.loading");
            this.payrollsApi.unlinkCalculations(this.current, [calc.id])
              .then((savedPayroll) => {
                this.setCurrentValue(savedPayroll, true);
                loader.dismiss();
              });
          }
        });
    } else {
      this.uiHelpers.showConfirm(
        "SALAXY.NG1.PayrollComponent.payrollCalcs.deleteCalcDialog.heading",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.deleteCalcDialog.intro",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.deleteCalcDialog.confirmButton",
        "SALAXY.NG1.PayrollComponent.payrollCalcs.deleteCalcDialog.cancelButton")
        .then((result: boolean) => {
          if (result) {
            const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayrollComponent.payrollCalcs.deleteCalcDialog.loading");
            this.payrollsApi.deleteCalculations(this.current, [calc.id])
              .then((savedPayroll) => {
                this.setCurrentValue(savedPayroll, true);
                loader.dismiss();
              });
          }
        });
    }
  }

  /**
   * Shows a report for the payroll calculation using a modal dialog.
   * @param reportType - Type of report to show
   * @param calculation - Calculation for the report
   */
  public showReport(reportType: calcReportType, calculation: Calculation) {
    this.reportsService.showReportModalForCalc(reportType, calculation);
  }

  /**
   * Opens a list of workers in to a dialog window.
   * Creates a new calculation for the selected worker and current payroll.
   */
  public createNewCalc() {
    const workerList = [];
    this.uiHelpers.openEditDialog(
      "salaxy-components/modals/calc/WorkerList.html",
      workerList,
      {
        title: "Valitse työntekijät",
      }).then((result) => {
        if (result.action === "ok" && workerList.length > 0) {
          const loader = this.uiHelpers.showLoading("Luodaan laskelmia...");
          this.payrollsApi.addEmployments(this.current, workerList.map((x) => x.id))
            .then((savedPayroll) => {
              this.setCurrentValue(savedPayroll, true);
              loader.dismiss();
            });
        }
      });
  }

  /**
   * Opens the list of calculations into a dialog window for selection and then adds them to payroll
   * @param category Either "paid": Read-only, a copy is added ... or "draft": Editable, moved with status changed.
   */
  public addExistingCalc(category: "paid" | "draft" = "draft") {
    const title = (category === "paid" ? "Valitse maksetut palkat (kopioidaan)" : "Valitse luonnokset (siirretään)");
    this.uiHelpers.openSelectCalcs(category, title).then((result) => {
      if (result.action === "ok" && result.item.length > 0) {
        const loader = this.uiHelpers.showLoading("Siirretään laskelmia...");
        this.payrollsApi.addCalculations(this.current, result.item.map((x) => x.id))
          .then((savedPayroll) => {
            this.setCurrentValue(savedPayroll, true);
            loader.dismiss();
          });
      }
    });
  }

  /** Returns true if the period of a calculation is different than the period of the Payroll. */
  public isDateDifferent(calc: Calculation) {
    return calc.info.workStartDate !== this.current.input.period.start
      || calc.info.workEndDate !== this.current.input.period.end
      || calc.framework.numberOfDays !== this.current.input.period.daysCount;
  }

  /**
   * If true, the form controls should be read-only (no control at all).
   */
  public get isReadOnly() {
    const superIsReadOnly = this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly;
    return superIsReadOnly
      || (
        this.current.info.status === PayrollStatus.PaymentStarted &&
        this.current.input.paymentChannel !== PaymentChannel.Undefined
      )
      || (!this.sessionService.isInRole("pro") && this.current.info.status === PayrollStatus.PaymentStarted);
  }

  /**
   * Exports employment data from calculation to Employment relation.
   * If the calculation has unmodified changes, the calculation is saved.
   * @param calc Calculation that should be exported to Employment.
   */
  private exportEmploymentDataNoConfirm(calc: Calculation) {
    if (this.hasCalcChanges(calc)) {
      return this.calcsApi.updateToEmployment(calc).then(() => this.commitCalc(calc, true));
    } else {
      return this.calcsApi.updateToEmployment(calc).then(() => {
        return Promise.resolve({ calc, isSaved: false });
      });
    }
  }

  /**
   * TODO: Send payroll
   */
  public sendPayroll() {
    this.uiHelpers.showAlert("TODO: Palkkalistan lähetys");
    return;
  }

  /** Shows the paymentd dialog / the invoices UI. */
  public showPaymentDialog() {
    this.invoicesService.showPaymentDialog(this.current);
  }

  private setPaymentChannel(payroll: PayrollDetails) {
    if (payroll && payroll.input) {
      payroll.input.paymentChannel = this.invoicesService.defaultChannel;
    }
  }

  /**
   * Sends the payroll from company to PRO.
   * @param message Message to the accountant
   */
  public sendToPro(message: string) {
    const loader = this.uiHelpers.showLoading("Lähetetään", "Lähetetään palkkalistaa tilitoimistolle.");
    this.current.info.status = PayrollStatus.Draft;
    this.save().then(() => {

      this.api.saveWorkflowEvent(this.current, {
        type: 'PartnerMessageClosed',
        ui: WorkflowEventUi.Success,
        message: message ? "Viesti yritykseltä: " + message : "Yritys tarkistanut",
      }).then(() => {
        loader.dismiss();
        this.$location.path("/");
      });
    });
    return;
  }
}
