import { Arrays, Calculation, Numeric, Payroll03Info, PayrollDetails, Payrolls } from "@salaxy/core";

/**
 * Payroll logic that is independent from UI log (AngularJS).
 * TODO: Move to @salaxy/core
 */
export class PayrollLogic {

  /**
   * Gets the Payroll03Info object based on current data (without going to server).
   * @param payroll Payroll object, where info may be outdated.
   * @param calcs Calculations that may be loaded from the server.
   */
  public static getInfo(payroll: PayrollDetails, calcs: Calculation[]): Payroll03Info {
    if (!payroll) {
      return new Payrolls(null).getBlank().info;
    }
    if (!calcs || calcs.length === 0) {
      return payroll.info;
    }
    return {
      calcCount: calcs.length, // Should this be input.calcs.length? Probably does not really matter.
      fees: Arrays.sum(calcs, (x) => x.result.employerCalc.palkkaus),
      totalGrossSalary: Arrays.sum(calcs, (x) => Numeric.round(x.result.totals.totalGrossSalary)),
      totalPayment: Arrays.sum(calcs, (x) => Numeric.round(x.result.employerCalc.totalPayment)),
      // We could potentially do some client-side validation here, but it may not make sense: Real validation is on the server.
      isReadyForPayment: payroll.info.isReadyForPayment,
      // Not updated by this logic:
      date: payroll.info.date,
      salaryDate: payroll.info.salaryDate,
      paymentDate: payroll.info.paymentDate,
      paymentId: payroll.info.paymentId,
      status: payroll.info.status,
    };
  }
}
