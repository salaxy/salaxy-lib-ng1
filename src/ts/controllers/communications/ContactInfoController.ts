import * as angular from "angular";

/**
 * Controller rendering a contact info box in the page area.
 */
export class ContactInfoController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */

  /** If true, short version of the content is shown */
  public showShortContent: boolean;

  /** If true, contact info is hidden */
   public hideContactInfo: boolean;

  /**
   * Creates a new ContactInfoController
   * @ignore
   */
  constructor() {
    //
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    // initialization
  }

}
