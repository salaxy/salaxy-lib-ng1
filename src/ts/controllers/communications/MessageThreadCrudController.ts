import * as angular from "angular";

import { Message, MessageFrom, MessageThread, MessageThreads, ThreadedMessageType } from "@salaxy/core";

import { SessionService, UiHelpers, UploadService } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * User interface logic for a set of messages and/or attachments
 * that are grouped together as messages thread.
 */
export class MessageThreadCrudController extends ApiCrudObjectController<MessageThread> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["MessageThreads", "UiHelpers", "UploadService", "$location", "$routeParams", "$anchorScroll", "$timeout", "SessionService"];

  /** Text for a new message textarea */
  public newMessageText: string;

  /** If true, the message is currently in edit mode. */
  public isInEdit: boolean;

  /** Role of the current user. Owner is default. */
  public role: MessageFrom;

  /**
   * If set, the controller will mark a message thread as open once it has been loaded.
   * If the value is true, the message is marked as read immediately.
   * If the value is number, the controller waits the given amount of seconds before marking the item as read.
   */
  private _markAsRead: number | true;

  private _otherParty: string;

  private _otherPartyOptions: { [key: string]: string };

  private markAsReadTimeout: any;

  constructor(
    private messageApi: MessageThreads,
    uiHelpers: UiHelpers,
    private uploadService: UploadService,
    $location: angular.ILocationService,
    $routeParams: any,
    private $anchorScroll: angular.IAnchorScrollService,
    private $timeout: angular.ITimeoutService,

    private sessionService: SessionService,
  ) {
    // Dependency injection
    super(messageApi, uiHelpers, $location, $routeParams);
  }
  /**
   * Initialization code.
   */
  public $onInit() {
    this.role = this.role || MessageFrom.Owner;
    super.$onInit();
  }

  /**
   * Cleaning the timeout.
   */
  public $onDestroy = () => {
    if (angular.isDefined(this.markAsReadTimeout)) {
      this.$timeout.cancel(this.markAsReadTimeout);
    }
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/messages",
      detailsUrl: this.detailsUrl || "/messages/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/MessageThreads.html",
      oDataOptions: {},
    };
  }

  /**
   * Overrides the isReadOnly with workflow cases
   */
  public get isReadOnly(): boolean {
    if (this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly || this.isReadOnlyForced)
    {
      // TODO: When moving to ES6, call super.isReadOnly instead.
      return true;
    }
    return this.role === "owner" && this.current?.workflowData?.events.some((x) => x.type === "PartnerMessageClosed");
  }

  /**
   * If set, the controller will mark a message thread as open once it has been loaded.
   * If the value is true, the message is marked as read immediately.
   * If the value is number, the controller waits the given amount of seconds before marking the item as read.
   */
  public get markAsRead(): number | true {
    if (this.parentController) {
      return (this.parentController as MessageThreadCrudController).markAsRead;
    }
    return this._markAsRead;
  }
  public set markAsRead(value: number | true) {
    if (this.parentController) {
      (this.parentController as MessageThreadCrudController).markAsRead = value;
    }
    this._markAsRead = value;
  }

  /**
   * Salaxy ID (IBAN format) for the other party (besides the message thread owner).
   * Supports special values: "partner" for payment partner.
   */
  public get otherParty(): string {
    return this._otherParty;
  }
  public set otherParty(value: string) {
    if (value === "partner") {
      this._otherParty = this.sessionService.session.settings.partner.accountId;
    } else {
      this._otherParty = value;
    }
  }

  /**
   * If set, shows a drop-down for these options using the enum component.
   * Keys may have special values: "partner": payment partner.
   */
  public get otherPartyOptions(): { [key: string]: string } {
    return this._otherPartyOptions;
  }
  public set otherPartyOptions(value: { [key: string]: string }) {
    if (!value || Object.keys(value).length === 0) {
      this._otherPartyOptions = null;
      return;
    }
    this._otherPartyOptions = Object.assign({}, ...Object.keys(value).map((key) => {
      if (key === "partner") {
        const partner = this.sessionService.session.settings.partner;
        return { [partner.accountId]: value[key] || partner.serviceModel.site.webAppName || partner.info.avatar.displayName };
      }
      return { [key]: value[key] };
    }));
  }

  /**
   * Adds a text message (from the textarea / this.newMessage) to the current thread.
   */
  public sendMessage(): Promise<MessageThread> {
    this.newMessageText = (this.newMessageText || "").trim();
    if (!this.newMessageText) {
      return;
    }
    const msg: Message = {
      content: this.newMessageText,
      type: ThreadedMessageType.Text,
      fromParty: this.role,
    };
    return this.setStatus(this.messageApi.addMessage(this.currentId, msg))
      .then((thread) => {
        this.newMessageText = "";
        this.setCurrent(thread);
        return thread;
      });
  }

  /**
   * Sends an email notification to the thread owner.
   */
  public sendNotification(): Promise<MessageThread> {
    const endUserService = this.sessionService.session.settings.partner.serviceModel.site.webAppName || this.sessionService.session.settings.partner.info.avatar.displayName;
    const subject = `Uusi viesti palkanlaskennasta - ${endUserService}`;
    const body = `Hei,\n\npalkanlaskija on lähettänyt sinulle viestin.\n\nKirjaudu ${endUserService} -palveluun ja löydät saapuneen viestin palkat-osiosta.`;
    return this.setStatus(this.messageApi.sendNotification(this.currentId, subject, body))
      .then((thread) => {
        this.setCurrent(thread);
        return thread;
      });
  }

  /** Overrides the save to assure that the other party is defined (can be set in component properties).  */
  public save() {
    if (!this.current.otherParty) {
      if (!this.otherParty) {
        this.uiHelpers.showAlert("Viestin vastaanottajaa ei ole määritetty.");
        return;
      }
      this.current.otherParty = this.otherParty;
    }
    return super.save();
  }

  /** Overrides the reload to send the browser to the bottom */
  public reload() {
    const isInitialLoad = this.status === "noInit";
    return super.reload().then((value) => {
      if (!this.isNew()) {
        this.$timeout(() => {
          this.$anchorScroll("messageContainerBottom");
        });
      }
      if (isInitialLoad && this.markAsRead && !this.getReadTime()) {
        const relevantMessages = this.current.messages
          .filter((x) => x.fromParty === (this.role === "otherParty" ? "owner" : "otherParty"));
        if (relevantMessages.length > 0) {
          if (this.markAsRead === true) {
            this.markAllAsRead();
          } else {
            this.markAsReadTimeout = this.$timeout(() => {
              if (!this.isInEdit) {
                this.markAllAsRead();
              }
            }, this.markAsRead * 1000);
          }
        }
      }
      return value;
    });
  }

  /**
   * Marks all messages as read by a given party.
   * The command is immediately sent to the server and the current message is updated incrementally (just the read/unread info).
   * @param readBy The party for which the messages are marked as read.
   * If not specified, uses the current role or the controller.
   * @returns The entire thread with the messages marked as read.
   */
  public markAllAsRead(readBy?: MessageFrom): Promise<MessageThread> {
    readBy = readBy || this.role || MessageFrom.Owner;
    return this.messageApi.markAsRead(this.currentId, readBy)
      .then((result) => {
        this.current.messages.forEach((msg: Message) => {
          if (readBy === "otherParty") {
            msg.readByOtherParty = msg.readByOtherParty || new Date().toISOString();
          } else {
            msg.readByOwner = msg.readByOwner || new Date().toISOString();
          }
        });
        return result;
      });
  }

  /**
   * Marks a message / attachment as read/unread also changing all messages before that message.
   * The role of the read/unread is determined by the message: It is the opposite of fromParty (owner (default)/otherParty).
   * The changes are made only to the current message, not Saved to the server.
   * @param message The message that is marked as read/unread (with the messages before that).
   * @param readByValue Possibility to force whether the flag is set to read or unread.
   * If null, the flag is set to the opposite of the current.
   */
  public markItemAsRead(message: Message, readByValue: boolean = null) {
    const flagFor: "owner" | "otherParty" = message.fromParty === "otherParty" ? "owner" : "otherParty";
    if (readByValue == null) {
      readByValue = flagFor === "owner" ? !message.readByOwner : !message.readByOtherParty;
    }
    if (readByValue) {
      for (const item of this.current.messages) {
        if (flagFor === "owner") {
          item.readByOwner = item.readByOwner || new Date().toISOString();
        } else {
          item.readByOtherParty = item.readByOtherParty || new Date().toISOString();
        }
        if (item === message) {
          return;
        }
      }
    } else {
      const startFrom = this.current.messages.indexOf(message);
      for (let i = startFrom; i < this.current.messages.length; i++) {
        if (flagFor === "owner") {
          this.current.messages[i].readByOwner = null;
        } else {
          this.current.messages[i].readByOtherParty = null;
        }
      }
    }
  }

  /**
   * Gets the time when a message was read.
   * The time is dependent of the fromParty of the message: There are separate times for owner and other party.
   * If time is null, the message has not been read.
   * @param message Message for which the readTime is fetched.
   * If not set, uses the last message for the current party.
   * @returns If fromParty is "otherParty", then readByOwner is returned, otherwise readByOtherParty is returned.
   * The value should be ISO date (string) or null if the item has not been read yet.
   */
  public getReadTime(message: Message = null) {
    if (!message) {
      const relevantMessages = this.current.messages
        .filter((x) => x.fromParty === (this.role === "otherParty" ? "owner" : "otherParty"));
      if (relevantMessages.length === 0) {
        return null;
      }
      message = relevantMessages[relevantMessages.length - 1];
    }
    if (message.fromParty === "otherParty") {
      return message.readByOwner || null;
    } else {
      return message.readByOtherParty || null;
    }
  }

  /** temp upload progress */
  public uploadProgress = 0;

  /**
   * Uploads the files to the server
   * @param files The files collection from the upload component
   * @example
   * <button ngf-select="$ctrl.uploadFiles($files)" multiple accept="image/*">Select Files</button>
   */
  public uploadFiles(files: any[]) {
    if (files && files.length) {
      const loader = this.uiHelpers.showLoading("Tallennetaan tiedostoja...");
      this.uploadService.upload<MessageThread>(
        this.messageApi.getUploadUrl(this.currentId), {
          files,
          from: this.role || "owner",
        }, (progress, error) => {
          this.uploadProgress = progress;
        }).then((response) => {
          this.newMessageText = "";
          this.setCurrent(response);
          loader.dismiss();
          return response;
        }).catch((response) => {
            loader.dismiss();
            this.uiHelpers.showAlert("Virhe", "Tiedoston tallennus palvelimelle epäonnistui.");
            throw new Error(response);
        });
    }
  }

  /**
   * Gets the file display related information.
   * @param message Message that contains the file.
   */
  public getFileInfo(message: Message) {
    // TODO: Move to a helper
    const result = {
      isImage: false,
      previewUrl: null,
      downloadUrl: null,
      icon: null,
    };
    if (message.type !== "blobFile") {
      return result;
    }
    result.isImage = true;
    result.icon = this.uploadService.getFileIcon(message.content);
    if (message.contentId) {
      if (message.preview === message.contentId) {
        result.previewUrl = this.uploadService.getPreviewUrl(message.preview);
      }
      result.downloadUrl = this.uploadService.getFileUrl(message.contentId);
    }
    return result;
  }

  /**
   * Opens a list of workers in to a dialog window to link them into chat.
   */
  public addWorkers() {
    // TODO: Should add Employments, not Workers, but that needs a new UI component.
    const workerList = [];
    this.uiHelpers.openEditDialog(
      "salaxy-components/modals/calc/WorkerList.html",
      workerList,
      {
        title: "Valitse työntekijät",
      }).then((result) => {
        if (result.action === "ok" && workerList.length > 0) {
          const loader = this.uiHelpers.showLoading("Lisätään työntekijöitä...");
          const messages = workerList.map((x) => {
            const msg: Message = {
              content: x.otherId,
              fromParty: this.role || MessageFrom.Owner,
              type: ThreadedMessageType.WorkerAccount,
            };
            return msg;
          });
          this.current.messages.push(...messages);
          this.save().then(() => {
            loader.dismiss();
          });
        }
      });
  }

  /**
   * Opens the list of calculations into a dialog window for selection and then adds them to payroll
   * @param category Either "paid": Read-only, a copy is added ... or "draft": Editable, moved with status changed.
   */
  public addExistingCalc(category: "paid" | "draft" = "draft") {
    const calculations = [];
    this.uiHelpers.openEditDialog(
      "salaxy-components/modals/calc/CalcList.html",
      calculations,
      {
        title: (category === "paid" ? "Valitse maksetut palkat (kopioidaan)" : "Valitse luonnokset (siirretään)"),
        category,
      }).then((result) => {
        if (result.action === "ok" && calculations.length > 0) {
          const loader = this.uiHelpers.showLoading("Lisätään laskelmia...");
          const messages = calculations.map((x) => {
            const msg: Message = {
              content: x.id,
              fromParty: this.role || MessageFrom.Owner,
              type: ThreadedMessageType.Calculation,
            };
            return msg;
          });
          this.current.messages.push(...messages);
          this.save().then(() => {
            loader.dismiss();
          });
        }
      });
  }

  /** Deletes a message from the message list (must be saved still) */
  public deleteMessage(item: Message) {
    this.current.messages.splice(this.current.messages.findIndex((x) => x === item), 1);
  }
}
