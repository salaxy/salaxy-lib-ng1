import * as angular from "angular";

import {
  ApiListItem, Avatar, Calculation, CalculationListItem, Calculations, Dates,
  Iban,
  InsuranceContract, ODataResult, Overview, PensionContract, TaxCard2019Logic,
  Taxcards, Texts, WelcomeDataWorker, WorkerListItem, WorkerLogic, Workers,
} from "@salaxy/core";

import {
  ReportsService,
  SettingsService,
  SessionService,
  UiCrudHelpers,
  UiHelpers,
} from "../../services";

/**
 * Provides methods for profiled Welcome messages to the end user.
 */
export class WelcomeController implements angular.IController {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "SessionService",
    "Workers",
    "SettingsService",
    "$location",
    "UiCrudHelpers",
    "$rootScope",
    "Calculations",
    "Taxcards",
    "Overview",
    "UiHelpers",
    "ReportsService",
  ];

  /**
   * If set to true, the settings are handled by proxy:
   * we do not ask the user to change the settings in
   * the Welcome screen (mainly Pension or Insurance).
   */
  public settingsByProxy;

  /** Worker home page UI properties */
  public workerUI: {
    /** If true, worker has added taxcard. */
    taxCardAdded?: boolean,
    /** If true, worker has incoming payment but no (valid) taxcard. */
    taxCardAlert?: boolean,
    /** Titles for panels */
    panelTitles?: {
      /** worker's taxcard panel title */
      taxCard: string,
    },
  } = {};

  /** Household dashboard interface related properties. */
  public householdUI: {
    /** If > 0, Household has workers. */
    hasWorkers?: number,
    /** amount of calculations of different statuses. */
    details?: {
      /**
       * Household has paid calculations.
       * When > 0, show panel list
       * When null/zero, show primary action to create one or hide panel
       */
      paidCalculations?: number,
      /**
       * Household has draft calculations.
       * When 1, show primary action to pay salary.
       * When more, show panel list
       * When null/zero, show primary action to create one.
       */
      draftCalculations?: number,
      /**
       * Household has received calculations from worker which are accepted, but not paid.
       * When 1, show primary action to pay the calculation
       * When more, show panel list with normal actions
       */
      sharedCalculationsApproved?: number,
      /**
       * Household has received calculations from worker which are not accepted or declined.
       * When 1, show primary action to view the calculation.
       * When more, show panel list
       */
      sharedCalculationsWaiting?: number,

      /**
       * Household has received calculations from worker which are  accepted or waiting.
       * When 1, show primary action to view the calculation.
       * When more, show panel list
       */
      sharedCalculationsUnhandled?: number,
    },
  };

  /** Welcome page data for the Household account */
  public householdData: {
    /** Paid Calculations of the Household */
    paidCalculations?: CalculationListItem[],
    /** Draft Calculations of the Household */
    draftCalculations?: CalculationListItem[],
    /** Calculations shared to Household */
    // sharedCalculations?: ICalculationListItem[],
    /** Approved Calculations shared to Household */
    sharedCalculationsApproved?: CalculationListItem[],
    /** Calculations shared to Household waiting for approval/rejection */
    sharedCalculationsWaiting?: CalculationListItem[],
     /** Amount of Calculations shared to Household which are either waiting or accepted */
    sharedCalculationsUnhandled?: number;
    /** Workers for this Household */
    workers?: WorkerListItem[];
    /** True if the loading has started, but not completed. */
    isLoading: boolean,
    /** True if the loading is complete */
    isLoaded: boolean,
  };

  /** Welcome page (timeline) data forr the Worker role */
  public workerDataNew: WelcomeDataWorker;

  private workerList: WorkerListItem[] = [];

  private workerListStatus: "initial" | "loading" | "loaded" = "initial";

  /**
   * Creates a new WelcomeController
   * @ignore
   */
  constructor(
    private session: SessionService,
    private workers: Workers,
    private settingsService: SettingsService,
    private $location: angular.ILocationService,
    private uiCrudHelpers: UiCrudHelpers,
    private $rootScope: angular.IRootScopeService,
    private calculations: Calculations,

    private taxcards: Taxcards,
    private overview: Overview,
    private uiHelpers: UiHelpers,
    private reportsService: ReportsService,
  ) { }

  /**
   * Implement IController
   */
  public $onInit = () => {
    if (this.session.isSessionChecked && this.session.isAuthenticated) {
      this.loadWelcomeData();
    }
    this.session.onAuthenticatedSession(this.$rootScope, () => {
      this.loadWelcomeData();
    });
  }

  /**
   * Gets a preview URL for the image.
   * Currently only for a taxcard, but may later support other items.
   */
  // eslint-disable-next-line
  public getPreviewUrl(item: ApiListItem ) {

    // Previews disabled for every item 10.6.2021 /MJ
    return null;
  }

  /**
   * Returns the url where to download the tax card
   * @param taxCard Taxcard or if null, the current is used.
   */
  public getTaxcardDownloadUrl(taxCard): string {
    return this.taxcards.getDownloadUrl(taxCard);
  }

  /**
   * Moves the browser to create-new calculation screen
   * @param workerId Worker ID for which to create the calculation.
   */
  public createNewCalculationFor(workerId: string) {
    this.$location.path("/calc/details/" + workerId + "/new-for-worker");
  }

  /** Gets the current pension product or empty object */
  public getPension(): PensionContract {
    const forDate = Dates.getTodayMoment();
    return this.settingsService.current?.calc.pensionContracts.find( (x) =>
     Dates.getMoment(x.startDate ?? "1900-01-01").isSameOrBefore(forDate) &&
     Dates.getMoment(x.endDate ?? "2100-01-01").add(1,"day").isAfter(forDate)) || {};
  }

  /** Gets the current insurance product or empty object */
  public getInsurance(): InsuranceContract {
    const forDate = Dates.getTodayMoment();
    return this.settingsService.current?.calc.insuranceContracts.find( (x) =>
     Dates.getMoment(x.startDate ?? "1900-01-01").isSameOrBefore(forDate) &&
     Dates.getMoment(x.endDate ?? "2100-01-01").add(1,"day").isAfter(forDate)) || {};
  }

  /**
   * Loads the welcome data from the server.
   */
  public loadWelcomeData() {
    if (!this.session.isSessionChecked) {
      throw new Error("loadWelcomeData() called before session checked.");
    }
    if (this.session.isInRole("worker")) {
      this.loadWorkerData();
      return;
    }
    if (this.session.isInRole("household")) {
      this.loadHouseholdData();
      return;
    }
    if (this.session.isInRole("company")) {
      this.loadCompanyData();
      return;
    }
  }

  /** Loads the Worker role data and UI. */
  public loadWorkerData() {
    this.overview.getWelcomeDataWorker().then((result) => {
      this.workerDataNew = result;
      this.workerUI = {
        taxCardAdded: !!result.activeTaxcard,
        taxCardAlert: !!result.invitation,
        panelTitles: {
          taxCard: result.activeTaxcard ? "Lataamasi verokortti" : "Lähetä verokortti työnantajallesi",
        },
      };
    });
  }

  /** Loads the welcome data from the server for Household role */
  public loadHouseholdData() {
    this.householdData = {
      isLoading: true,
      isLoaded: false,
    };

    const checkAllLoaded = () => {
      this.householdData.workers = this.workerList;

      const householdUI = {
        hasWorkers: this.householdData.workers.length,
        details: {
          draftCalculations: this.householdData.draftCalculations.length,
          paidCalculations: this.householdData.paidCalculations.length,
          sharedCalculationsApproved: this.householdData.sharedCalculationsApproved.length,
          sharedCalculationsWaiting: this.householdData.sharedCalculationsWaiting.length,
          sharedCalculationsUnhandled: this.householdData.sharedCalculationsUnhandled,
        },
      };
      this.householdUI = householdUI;

      this.householdData.isLoading = false;
      this.householdData.isLoaded = true;
    };

    this.calculations.getOData({
      $orderby: "updatedAt desc",
    }).then((calculations: ODataResult<CalculationListItem>) => {
      this.householdData.paidCalculations = calculations.items.filter((x) => x.status === "paymentSucceeded");
      this.householdData.draftCalculations = calculations.items.filter((x) => x.status === "draft");
      this.householdData.sharedCalculationsApproved = calculations.items.filter((x) => x.status === "sharedApproved");
      this.householdData.sharedCalculationsWaiting = calculations.items.filter((x) => x.status === "sharedWaiting");
      this.householdData.sharedCalculationsUnhandled = this.householdData.sharedCalculationsWaiting.length + this.householdData.sharedCalculationsApproved.length;
      this.loadWorkers().then( () => {
        checkAllLoaded();
      });
    });
  }

  /** For household Ui demo. */
  public setHouseholdUI(uiSection) {
    switch (uiSection) {

      case "noSharedCalcs":
        this.householdUI.details.sharedCalculationsUnhandled = 0;
        this.householdUI.details.sharedCalculationsWaiting = 0;
        this.householdUI.details.sharedCalculationsUnhandled = 0;
        break;
      case "singleWaitingCalc":
        this.householdUI.details.sharedCalculationsWaiting = 1;
        this.householdUI.details.sharedCalculationsUnhandled = 1;
        this.householdUI.details.sharedCalculationsApproved = 0;
        break;
      case "singleApprovedCalc":
        if (this.householdUI.hasWorkers === 0) {
          this.householdUI.hasWorkers = this.householdData.workers.length;
        }
        this.householdUI.details.sharedCalculationsApproved = 1;
        this.householdUI.details.sharedCalculationsWaiting = 0;
        this.householdUI.details.sharedCalculationsUnhandled = 1;
        break;
      case "noDraftCalcs":
        this.householdUI.details.draftCalculations = 0;
        break;
      case "noPaidCalcs":
        this.householdUI.details.paidCalculations = 0;
        break;
      case "singleWorker":
        this.householdUI.hasWorkers = 1;
        break;
      case "noWorkers":
        this.householdUI.hasWorkers = 0;
        this.householdUI.details.paidCalculations = 0;
        this.householdUI.details.sharedCalculationsApproved = 0;
        break;
      case "reset":
        this.loadHouseholdData();
        break;
    }
  }

  /** Loads worker list initially */
  public loadWorkers(): Promise<WorkerListItem[]> {
    if (this.workerListStatus === "loaded") {
      return Promise.resolve(this.workerList);
    } else {
      this.workerListStatus = "loading";
      // Get all workers
      return this.workers.getOData(null).then((data) => {
        this.workerList = data.items;
        this.workerListStatus = "loaded";
        return this.workerList;
      });
    }
  }

  /** Loads the welcome data from the server for Company role */
  public loadCompanyData() {
    // TODO
  }

  /**
   * Gets the URL for a calculation pdf.
   * If report has not been saved (ID is null), returns null.
   *
   * @param calc - Calculation. This method requires that the calculation has been saved.
   *
   * @param inline - if false, downloads the pdf file
   *
   * @returns Url for specified report
   */
  public getPdfUrl(calc: Calculation, inline = false) {
    if (!calc) {
      return null;
    }

    return this.reportsService.getPdfUrlForCalc("salarySlip", calc.id, inline);
  }

  /**
   * Gets the chart data for taxcard, labels, colors and other settings.
   * Uses chartCache to avoid $digest loop.
   */
  public get taxcardChartData() {
    if (!this.workerDataNew || !this.workerDataNew.activeTaxcard) {
      return null;
    }
    if (!this.workerDataNew.activeTaxcard.card.incomeLimit){
      return this.uiHelpers.cache(this, "pieChart",
      () => TaxCard2019Logic.getPieChartData(this.workerDataNew.activeTaxcard),
      () => TaxCard2019Logic.getIncomeLimitInfo(this.workerDataNew.activeTaxcard));
    }
   return this.uiHelpers.cache(this, "lineChart",
      () => TaxCard2019Logic.getMainChart(this.workerDataNew.activeTaxcard),
      () => TaxCard2019Logic.getIncomeLimitInfo(this.workerDataNew.activeTaxcard));
  }


  /**
   * Gets the chart data for taxcard, labels, colors and other settings.
   * Uses chartCache to avoid $digest loop.
   */
  public get taxcardMobileChartData() {
    if (!this.workerDataNew || !this.workerDataNew.activeTaxcard) {
      return null;
    }
    return this.uiHelpers.cache(this, "lineChartMobile",
      () => {
       const data = TaxCard2019Logic.getMainChart(this.workerDataNew.activeTaxcard);
       data.options.legend.display = false;
       // TODO ? data.options.scales.yAxes.display = false;
       return data;
      },
      () => TaxCard2019Logic.getIncomeLimitInfo(this.workerDataNew.activeTaxcard));
  }

  /** Calls the Worker wizard and if a Worker is added, starts a calculation for that worker. */
  public launchWorkerWizard() {
    this.uiCrudHelpers.createNewWorker().then((result) => {
      if (result.action === "ok") {
        this.createNewCalculationFor(result.item.worker.id);
      }
    });
  }

  /** Gets the flags that indicate which tutorials to show. */
  public get tutorials(): {
    /** If true, shows info box that the site is runnin in anonymous mode. */
    showAnon: boolean,
    /** If true, should direct the user to create the first Worker */
    showAddWorker: boolean,
    /** If true, should direct user to create a new calculation.  */
    showAddCalculation: boolean,
    /** If true, should ask the user for digital signature. */
    showSign: boolean,
    /** If true, should ask the user to fill in the Pension contract information. */
    showPension: boolean,
    /** If true, should ask the user to fill in the Accident Insurance contract information. */
    showInsurance: boolean,
    /** If true, should promote Payroll feature. */
    showTryPayroll: boolean,
    /** If true, should promote new features. */
    showNewFeatures: boolean,
  } {
    const result = {
      showAnon: false,
      showAddWorker: false,
      showAddCalculation: true,
      showSign: false,
      showPension: false,
      showInsurance: false,
      showTryPayroll: false,
      showNewFeatures: false,
    };
    if (!this.session.isSessionChecked) {
      return result;
    }
    if (!this.session.isAuthenticated) {
      result.showAnon = true;
      return result;
    }
    if (!this.settingsByProxy) {
      if (!this.session.checkAccountVerification()) {
        result.showSign = true;
      }
      result.showPension = this.settingsService.current?.calc.pensionContracts.length == 0 || this.settingsService.current?.calc.pensionContracts.every( (x) => !x.company);
      result.showInsurance = this.settingsService.current?.calc.pensionContracts.length == 0 || this.settingsService.current?.calc.insuranceContracts.every( (x) => !x.company);
    }
    switch (this.workerListStatus) {
      case "loaded":
        if (this.workerList.length === 0) {
          result.showAddWorker = true;
          return result;
        }
        break;
      case "loading":
        return result;
      case "initial":
        this.loadWorkers();
        return result;
    }
    result.showAddCalculation = true;
    return result;
  }

  /**
   * Returns system alert html.
   */
  public get systemAlertHtml(): string {
    if (!this.session) {
      return null;
    }

    const userSession = this.session.getSession();
    if (!userSession) {
      return null;
    }

    const html = userSession.systemAlertHtml;
    if (!html) {
      return null;
    }
    return html;
  }

  /**
   * Returns true if system alert exists.
   */
  public get hasSystemAlert(): boolean {
    return !!this.systemAlertHtml;
  }

  /** Filters preloaded workers. */
  public filterWorkers = (searchString: string): Avatar[] => {
    if (!searchString) {
      const allResult = this.workerList
        .slice(0, 30).map(this.getAvatar);
      return allResult;
    }
    searchString = (searchString || "").trim().toLowerCase();
    const result = this.workerList
      .filter((w) =>
        (w.otherPartyInfo.avatar.displayName || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.avatar.sortableName || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.telephone || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.email || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.id || "").toLowerCase().indexOf(searchString) === 0 ||
        (w.otherPartyInfo.officialId || "").toLowerCase().indexOf(searchString) === 0)
      .map(this.getAvatar);
    return result.length > 0 ? result.slice(0, 30) : [];
  }

  /** Gets the display name for the selected worker. */
  public getWorkerName = (avatarId: string): string => {
    for (const worker of this.workerList) {
      if (avatarId === worker.otherId) {
        return Texts.escapeHtml(this.getAvatar(worker).displayName);
      }
    }
    return null;
  }

  private getAvatar = (worker: WorkerListItem): Avatar => {
    const result: Avatar = Object.assign({}, worker.otherPartyInfo.avatar);
    (result as any).displayId =
      ((worker.id || "").startsWith("FI") ? Iban.formatIban(worker.id) + " / " : "")
      + (worker.otherPartyInfo.officialId ? worker.otherPartyInfo.officialId : "-")
      ;
    result.displayName = Texts.escapeHtml(result.displayName);
    result.description = Texts.escapeHtml(WorkerLogic.getDescription(worker));
    return result;
  }
}
