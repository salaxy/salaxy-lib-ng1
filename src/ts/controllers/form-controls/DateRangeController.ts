import * as angular from "angular";

import { DateRange, Dates, Objects, Translations } from "@salaxy/core";

/**
 * Provides a user interface for picking up a date range
 * and optionally specifying also the number of working days within that range.
 */
export class DateRangeController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Label for the control */
  public label: string;

  /** Label for the scondary input. Translation is attempted. Default is SALAXY.NG1.DateRange.labelDaysCount.  */
  public labelDaysCount: string;

  /** The model that is bound to the input */
  public model: angular.INgModelController;

  /**
   * The selection mode is either:
   *
   * - "range" for (default) calendar control with start and end.
   * - "multiple" for selecting multiple individual dates from a calendar control
   * - "calc" for period selection that is optimized for salary calculation (months, 2 weeks, 1/2 months etc.)
   */
  public mode: "range" | "multiple" | "calc";

  /**
   * Minimum available date.
   * Bindable and ISO string version of the datepicker-options.minDate.
   */
  public minDate: string;

  /** Form controller, if available */
  public form: angular.IFormController;

  /**
   * Maximum available date.
   * Bindable and ISO string version of the datepicker-options.maxDate.
   */
  public maxDate: string;

  /** If there is a validation error in period start, it is added here. */
  public periodStartDateError = null;

  /**
   * If true, displays the control as read-only div instead of the input control.
   * This "read-only" attribute is visualized in a different way than standard html "readonly" (ng-readonly):
   * The input is not shown and display is more compact.
   * Also note, that you may change the same read-only for the form using readonly / ng-readonly attribute.
   * @example
   * ```html
   * <form name="foo" ng-readonly="true">
   * <!-- .... or plain HTML ... -->
   * <form name="foo" readonly="true">
   * <!-- .... or plain HTML ... -->
   * <salaxy-input name="input1" read-only="true"></salaxy-input>
   */
  public readOnly: boolean;

  /**
   * Fires an event when the model is changing: Any of the values specific to the model are changing: start, end or daysCount.
   * This should typically used instead of ng-change because ng-change will only fire if the object reference changes.
   * On-change fires when dates or day count changes and this is typically what you are looking for.
   * @example <salaxy-date-range ng-model="$ctrl.dateRange" on-change="$ctrl.dateRangeChange(value)"></salaxy-date-range>
   */
  public onChange: (eventData: {
    /** The changed date range. */
    value: DateRange,
  }) => void;

  /**
   * Gets or sets the internal value of the complex object.
   */
  public get value(): {
    /** Dately value for start date. */
    start?: string,
     /** Dately value for end date. */
    end?: string,
    /** Boolean indicating whether the end date has been explictly set: end date has been selected and is not the same as the start date. */
    isEndExplicit?: boolean,
    /** Number of days in the range. */
    daysCount?: number,
    /** Array of selected days in the range. */
    days?: string[],
    /** Latest selected date. */
    latest?: string,
    /** Formatted presentation of  range. */
    formattedRange?: string,
  } {
    return this._value;
  }
  public set value(val) {
    this._value = val;
    this.setModelValue();
  }

  /** User interface shortcuts for period selection */
  public periodShortcuts: {
    /** Text for current month. */
    monthCurrent: string,
    /** Text for last month. */
    monthLast: string,
    /** Text for other month. */
    monthOther: string,
    // monthMulti: "Monta kuukautta",
    /** Text for two weeks. */
    weeks2: string,
    /** Text for half a month */
    monthHalf: string,
    /** Text for other period. */
    other: string,
  };

  /** Form control name */
  public name: string;

  /** If true, the input value is required */
  public require: boolean;

  /**
   * Options for ui.bootstrap.datepicker
   * Note that if you use customClass, you need to handle the formatting of the selected range.
   * See getDayClass().
   * Also note that this value is not watched, it is read only once when the options are first needed-
   */
  public datepickerOptions: any;

  private internalOptions: any;

  /** When only first selection is done in Range, we set the minimum value here. */
  private tempMinDateForRangeSelection: Date;

  private _value;

  private _periodStartDate;

  constructor() { /**/ }

  /** Set the values and defaults on init */
  public $onInit() {
    const currentMonth = Dates.getMonth(Dates.getToday());
    this.periodShortcuts = {
      monthCurrent: Translations.get("SALAXY.UI_TERMS.monthShort" + currentMonth) + "kuu",
      monthLast: Translations.get("SALAXY.UI_TERMS.monthShort" + (currentMonth - 1)) + "kuu",
      monthOther: "Muu kuukausi",
      // monthMulti: "Monta kuukautta",
      weeks2: Translations.get("SALAXY.UI_TERMS.week2"),
      monthHalf: Translations.get("SALAXY.UI_TERMS.monthHalf"),
      other: Translations.get("SALAXY.UI_TERMS.otherPeriod"),
    };
    this.model.$render = () => {
      const modelValue = this.model.$viewValue;
      if (modelValue) {
        if (modelValue.start) {
          modelValue.latest = modelValue.start;
          modelValue.formattedRange = Dates.getFormattedRange(modelValue.start, modelValue.end);
        }
        if (modelValue.end) {
          modelValue.latest = modelValue.end;
          modelValue.isEndExplicit = true;
        }
        this._value = modelValue; // Update the internal model without firing any events.
        this._periodStartDate = Dates.format(modelValue.start, "D.M.", null);
      }
    };
  }

  /** HACK: Temporary shortcut peiod setting. Needs go-through / refactoring. */
  public get dateRange(): DateRange {
    return this.value;
  }
  public set dateRange(value: DateRange) {
    (value as any).isEndExplicit = true;
    (value as any).latest = value.end;
    this.value = value;
  }

  /**
   * This is the latest selection in the Datepicker component.
   * When in mode=range: First it sets the value start, then end and then again start (and sets end to null).
   * When in mode=multiple: Sets the values on or off in the array.
   */
  public get latestDateSelection() {
    return (this.value || {}).latest;
  }
  public set latestDateSelection(newvalue) {
    const value = this.value || {};
    newvalue = Dates.asDate(newvalue);
    value.latest = newvalue;
    if (!newvalue) {
      // Indicates failure in parsing. Datepicker component should not return null.
      return;
    }
    if (this.mode === "multiple") {
      if (value.days && value.days.indexOf(value.latest) > -1) {
        value.days = value.days.filter((x) => x !== value.latest).sort();
      } else {
        if (!value.days) {
          value.days = [];
        }
        value.days.push(value.latest);
        value.days = value.days.sort();
      }
      value.daysCount = value.days.length;
    } else {
      // mode=range - Used to be the only supported functionality.
      if (value.start && !value.isEndExplicit) {
        value.end = value.latest;
        value.isEndExplicit = true;
        this.tempMinDateForRangeSelection = null;
      } else {
        value.start = newvalue;
        value.end = newvalue;
        value.isEndExplicit = false;
        this.tempMinDateForRangeSelection = Dates.asJSDate(newvalue);
      }
      value.daysCount = Dates.getWorkdays(value.start, value.end).length;
      value.formattedRange = Dates.getFormattedRange(value.start, value.end);
    }
    this.setModelValue();
  }

  /** Gets or sets the period start date */
  public get periodStartDate(): string {

    return this._periodStartDate;
  }
  public set periodStartDate(value: string) {

    this._periodStartDate = value;
    const parts = (value || "").split(".");
    if (parts.length < 2 || parts.length > 3) {
      this.periodStartDateError = "Syötä muodossa 'pv.kk.'";
      return;
    }
    let year = (parts.length === 3) ? Number((parts[2] || "").trim()) : null;
    if (!year || year < 2019 || year > 2100) {
      year = Number(this.dateRange.start.substr(0, 4));
    }
    const date = Dates.getDate(year, Number(parts[1]), Number(parts[0]));
    if (date) {
      this.periodStartDateError = null;
      switch (this.periodShortcut) {
        case "monthCurrent":
        case "monthLast":
        case "monthOther":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(1, "month").subtract(1, "day"));
          break;
        case "weeks2":
          this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(2, "week").subtract(1, "day"));
          break;
        case "monthHalf":
          if (Dates.getMoment(date).date() === 15 || Dates.getMoment(date).date() === 16 ) {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).endOf("month"));
          } else {
            this.dateRange = Dates.getDateRange(date, Dates.getMoment(date).add(14, "day"));
          }
          break;
        case "other":
          this.dateRange = Dates.getDateRange(date, date);
          break;
    }
      this.dateRangeChange(true);
    } else {
      this.periodStartDateError = `${value} ei ole päivämäärä.`;
    }
  }

  /** Gets the user interface shortcut for the period. */
  public get periodShortcut(): string {
    if (!this.dateRange || !this.dateRange.start || !this.dateRange.end) {
      return null;
    }
    if (this.dateRange.start === Dates.getDate("today", "today", 1)
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().endOf("month"))) {

        return "monthCurrent";
    }
    if (this.dateRange.start === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month"))
      && this.dateRange.end === Dates.asDate(Dates.getTodayMoment().startOf("month").subtract(1, "month").endOf("month"))) {
      return "monthLast";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"))) {
      return "monthOther";
    }
    if (this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).add(2, "week").subtract(1, "day"))) {
      return "weeks2";

    }

    // if the range is 15 (14) days or
    // starts 15th or 16th and ends month end
    if ((Dates.getDuration(this.dateRange.start, this.dateRange.end ).days() === 14)
      || (Dates.getMoment(this.dateRange.start).date() === 15 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))
      || (Dates.getMoment(this.dateRange.start).date() === 16 && this.dateRange.end === Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month")))) {
        return "monthHalf";
    }

    // TODO: Potentially add other options.
    return "other";
  }
  public set periodShortcut(value: string) {
    const today = Dates.getTodayMoment();
    this.periodStartDateError = null;
    switch (value) {
      case "monthCurrent":
        this.dateRange.start = Dates.getDate("today", "today", 1);
        this.dateRange.end = Dates.asDate(Dates.getTodayMoment().endOf("month"));
        break;
      case "monthOther":
        if (today.date() === 1) {
          today.add(1, "day"); // Move to tomorrow so that the selection is monthOther and not monthCurrent.
        }
        this.dateRange.start = Dates.asDate(today);
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(1, "month").subtract(1, "day"));
        break;
      case "monthLast":
        this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month"));
        this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        break;
      case "weeks2":
        this.dateRange.start = Dates.asDate(today.clone().startOf("week").add(-2, "week").add(1, "day"));
        this.dateRange.end = Dates.asDate(today.clone().startOf("week"));
        break;
      case "monthHalf":

        // TODO Check this
        if (today.date() < 16) {
          // 16 - (28,29,30,31)
          this.dateRange.start = Dates.asDate(today.clone().startOf("month").add(-1, "month").add(15, "day"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).endOf("month"));
        } else {
          // 1 - 15
          this.dateRange.start = Dates.asDate(today.clone().startOf("month"));
          this.dateRange.end = Dates.asDate(Dates.getMoment(this.dateRange.start).add(14, "day"));
        }
        break;
      case "other":
        this.dateRange.start = Dates.asDate(today.clone().add(-1, "day"));
        this.dateRange.end = Dates.asDate(today.clone());
        break;
    }
    this.dateRange = Dates.getDateRange(this.dateRange.start, this.dateRange.end);
    this.dateRangeChange();
  }

  /**
   * Called when the date range changes.
   * @param noPeriodStartDateUpdate If true, the _periodStartDate is not updated.
   * Should be true if the change is triggered by that input to avoid UI flickering.
   */
  public dateRangeChange(noPeriodStartDateUpdate = false) {
    if (!noPeriodStartDateUpdate) {
      this._periodStartDate = Dates.format(this.dateRange.start, "D.M.", null);
    }
  }

  /**
   * Sets the ng-model value from this.value;
   * Called by daysCount input directly and the datepicker through latestDateSelection.
   * Also called by value setter, but that probably not really used by any code at the moment.
   */
  public setModelValue() {
    const modelValue = this.model.$viewValue as DateRange;
    modelValue.start = this.value.start;
    modelValue.end = this.value.end;
    modelValue.daysCount = this.value.daysCount;
    this.model.$setViewValue(modelValue);
    this.onChange({ value: modelValue });
  }

  /**
   * Gets the options for datepicker.
   * Uses datepickerOptions, but adds customClass for range formatting.
   * Also converts minDate and maxDate to Date() object if necessary.
   */
  protected getOptions() {
    if (!this.internalOptions) {
      this.internalOptions = Objects.copy(this.datepickerOptions || {});
    }
    if (!this.internalOptions.customClass) {
      this.internalOptions.customClass = this.getDayClass;
    }
    if (this.internalOptions.showWeeks == null) {
      // Switch default to false (if not explicitly specified to true)
      this.internalOptions.showWeeks = false;
    }
    if (this.tempMinDateForRangeSelection) {
      this.internalOptions.minDateText = "range selection";
      this.internalOptions.minDate = this.tempMinDateForRangeSelection;
    } else if (this.minDate !== this.internalOptions.minDateText) {
      this.internalOptions.minDateText = this.minDate;
      this.internalOptions.minDate = Dates.asJSDate(this.minDate);
    }
    if (this.maxDate !== this.internalOptions.maxDateText) {
      this.internalOptions.maxDateText = this.maxDate;
      this.internalOptions.maxDate = Dates.asJSDate(this.maxDate);
    }
    return this.internalOptions;
  }

  /** Gets the readonly value either from the controller or from the form */
  protected getReadOnly(): boolean {
    // HACK: Can we not just inherit from InputBase?
    if (this.readOnly) {
      return true;
    }
    if (this.form && this.form.$$element && this.form.$$element[0]) {
      return !!this.form.$$element[0].attributes.readonly;
    }
    return false;
  }

  private getDayClass = (data) => {
    const date = data.date;
    const mode = data.mode;
    if (!this.value) {
      return "";
    }
    if (mode === "day" && this.value.start && this.value.end) {
      const dayToCheck = Dates.asDate(new Date(date).setHours(0, 0, 0, 0));
      if (this.mode === "multiple") {
        if (this.value.days.indexOf(dayToCheck) > -1) {
          return "selected";
        }
      } else {
        if (dayToCheck === this.value.start) {
          return "selected start";
        }
        if (dayToCheck === this.value.end) {
          return "selected end";
        }
        if (dayToCheck > this.value.start && dayToCheck < this.value.end) {
          return "selected";
        }
      }
    }
    return "";
  }
}
