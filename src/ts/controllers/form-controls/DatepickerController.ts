import * as angular from "angular";

import { Dates, PaymentChannel, SalaryDateLogic } from "@salaxy/core";

/**
 * Controller for datepicker control.
 */
export class DatepickerController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
    public static $inject = [];

  /** Form controller, if available */
  public form: angular.IFormController;
  /** Label for the control */
  public label: string;

  /** The model that is bound to the input */
  public model: any;

  /** The value of the ui.bootstrap.datepicker */
  public dtValue: Date;

  /**
   * Preset algorithms for enabled/disabled dates.
   * Currently supported: "salary" for next possible salary date and "next-weekday" for next weekday / banking day.
   * The function is added to datepickerOptions in init, so if you set datePickerOptions after that, you may need to implement this on your own.
   */
  public dateDisabled: "salary" | "next-weekday" | null;

    /** Payment channel. This affects which dates are disabled in the salary date selection. */
    public paymentChannel: PaymentChannel;

  /** Gets or sets the value as ISO formatted date string. */
  public get value() {
    return Dates.asDate(this.dtValue);
  }
  public set value(val) {
    this.dtValue = Dates.asJSDate(val);
    this.onChange();
  }

  /** Form control name */
  public name: string;

  /** Validation error text */
  public validationError: string;

  /** Options for ui.bootstrap.datepicker */
  public datepickerOptions: any;

  /**
   * If true, displays the control as read-only div instead of the input control.
   * This "read-only" attribute is visualized in a different way than standard html "readonly" (ng-readonly):
   * The input is not shown and display is more compact.
   * Also note, that you may change the same read-only for the form using readonly / ng-readonly attribute.
   * @example
   * ```html
   * <form name="foo" ng-readonly="true">
   * <!-- .... or plain HTML ... -->
   * <form name="foo" readonly="true">
   * <!-- .... or plain HTML ... -->
   * <salaxy-input name="input1" read-only="true"></salaxy-input>
   */
  public readOnly: boolean;

  /**
   * @deprecated Use "require" instead:
   * This attribute overlaps with required/ng-required directive, which produces unexpected results.
   */
  public required: string;

  private requirePendingUpdate: boolean | undefined;
  private _minDate: string;
  private _maxDate: string;

  /** Set the values and defaults on init */
  public $onInit() {
    if (this.name && !this.label) {
      this.label = this.name;
    }
    this.model.$render = () => {
      this.dtValue = Dates.asJSDate(this.model.$viewValue);
    };
    this.refreshOptions();
    this.validate();
  }

  /**
   * Minimum available date.
   * Bindable and ISO string version of the datepicker-options.minDate.
   * Currently not supported together with dateDisabled filters.
   */
  public get minDate(): string {
    return this._minDate;
  }
  public set minDate(value: string) {
    this._minDate = Dates.asDate(value);
    this.datepickerOptions = this.datepickerOptions || {};
    this.datepickerOptions.minDate =  Dates.asJSDate(this._minDate);
  }

  /**
   * Maximum available date.
   * Bindable and ISO string version of the datepicker-options.maxDate.
   * Currently not supported together with dateDisabled filters.
   */
  public get maxDate(): string {
    return this._maxDate;
  }
  public set maxDate(value: string) {
    this._maxDate = Dates.asDate(value);
    this.datepickerOptions = this.datepickerOptions || {};
    this.datepickerOptions.maxDate =  Dates.asJSDate(this._maxDate);
  }

  /** Gets or sets the presence of the required validator in the underlying model. */
  public get require(): boolean {
    return !!this.model?.$validators.required;
  }
  public set require(value: boolean) {
    if (!this.model) {
      this.requirePendingUpdate = value;
      return;
    }
    const hasRequiredValidator = !!this.model.$validators.required;
    if (value && !hasRequiredValidator) {
      this.model.$validators.required = (modelValue, viewValue) => {
        return !this.model.$isEmpty(viewValue);
      };
    } else if (!value && hasRequiredValidator) {
      delete this.model.$validators.required;
    }
  }

  /** On change of the value, do preventive operations and set value to model. */
  public onChange() {
    this.model.$setViewValue(this.value);
    this.validate();
  }

  /** Do validations. Automatically called on onInit() and onChange() */
  public validate() {
    if (Dates.isValidDateTime(this.value)) {
      this.validationError = "";
      return true;
    }
    this.validationError = "Virheellinen päivämäärä, oletus: d.M.yyyy";
    return false;
  }

  /**
   * Refresh picker options
   */
  protected refreshOptions() {
    if (!this.datepickerOptions) {
      this.datepickerOptions = {};
    }
    if (this.dateDisabled) {
      switch (this.dateDisabled) {
        case "next-weekday":
          this.datepickerOptions.minDate = Dates.asJSDate(Dates.asDate("today"));
          this.datepickerOptions.dateDisabled = (data: any) => {
            const date = data.date;
            const mode = data.mode;
            return (mode === "day" || !mode) && (Dates.asDate(date) < Dates.addWorkdays("today", 1) || !Dates.isWorkday(date));
          };
          break;
        case "salary":
        default:
          this.datepickerOptions.minDate = Dates.asJSDate(SalaryDateLogic.CalculateSalaryDate(null, null, true, this.paymentChannel));
          this.datepickerOptions.dateDisabled = (data: any) => {
            const date = data.date;
            const mode = data.mode;
            return (mode === "day" || !mode) && (!SalaryDateLogic.isValidSalaryDate(date, null, null, true, this.paymentChannel));
          };
          break;
      }
    }
  }

  /** Gets the readonly value either from the controller or from the form */
  protected getReadOnly(): boolean {
    // HACK: Can we not just inherit from InputBase?
    if (this.readOnly) {
      return true;
    }
    if (this.form && this.form.$$element && this.form.$$element[0]) {
      return !!this.form.$$element[0].attributes.readonly;
    }
    return false;
  }
}
