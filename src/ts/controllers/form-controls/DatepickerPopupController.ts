import { DatepickerController } from "./DatepickerController";

/**
 * Controller for datepicker popup control.
 */
export class DatepickerPopupController extends DatepickerController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

    /** Options for ui.bootstrap.datepicker */
    public popupPlacement = "bottom";

    /** Placeholder text to show in the text input. Default is 'p.k.vvvv' (TODO: Language versioning) */
    public placeholder: string;

    /** Date format in text input. Default is d.M.yyyy */
    public format: string;

    /**
     * If set to true, sets datepicker-append-to-body to false.
     * By default we set it to true in Salaxy framework,
     * but you may want to set it back to false in e.g. Modal dialogs.
     */
    public appendInline: boolean;

    /** Popup control state. */
    public popup = {
        opened: false,
    };

    /** Open popup. */
    public open() {
      this.refreshOptions();
      this.popup.opened = true;
    }

      /** Returns the placeholder text: We will probably add stuff about validation here if not explicitly set. */
    protected getPlaceholder() {
      return this.placeholder;
    }
}
