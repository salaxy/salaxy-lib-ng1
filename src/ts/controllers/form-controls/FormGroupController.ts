import * as angular from "angular";
import { FormGroupLabelType } from "./FormGroupLabelType";

/**
 * Helper for rendering the HTML for FormGroup:
 * This component renders only the label - input html.
 * It does not do any of the real form-control logic like ng-model, validations etc.
 * The "input" part of the form group may be a non form control - e.g. just a text.
 * Also the label may be hidden.
 */
export class FormGroupController implements angular.IController {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Label for the control */
  public label: string;

  /** Form control name */
  public name: string;

  /** If true, shows the asterix in the label. */
  public require: boolean;

  /**
   * @deprecated Use "require" instead:
   * This attribute overlaps with required/ng-required directive, which produces unexpected results.
   */
  public required: string;

  /**
   * Positioning of the label of form-control.
   * Supported values are "horizontal" (default), "no-label", "plain", "basic", "inline" and "empty-label".
   * See FormGourpLabelType for details.
   */
  public labelType: FormGroupLabelType;

  /**
   * Label columns expressed as Bootstrap grid columns.
   * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
   * Other label-types do not have column classes at the moment.
   */
  public labelCols: string;

  constructor() { /* Dependency injeciton */ }

  /**
   * Checks the existence of name property.
   */
  public $onInit() {
    if (!this.name) {
      throw new Error(`Property 'name' is required for Salaxy form controls (label: ${this.label}).`);
    }
    if (this.name && this.label == null) {
      this.label = this.name;
    }
    if (this.required) {
      const legacyRequiredValue = this.required?.toLowerCase().trim();
      this.require = legacyRequiredValue === "true" || legacyRequiredValue === "1";
    }
  }

  /**
   * Gets the class (or classes space separated) for label or value element.
   */
  public getLabelCols(forElement: "label" | "value" | "no-label" ): string {
    switch (forElement) {
      case "label":
        return this.labelCols || "col-sm-4";
      case "value": {
        let classes = "";
        if (this.labelCols && this.labelCols.length >= 8) {
          const regex = /col-(xs|sm|md|lg)-([1-9][1-2]?)/gm;
          let m = regex.exec(this.labelCols);
          while (m !== null) {
              if (m.index === regex.lastIndex) {
                  regex.lastIndex++;
              }
              const colsCount = parseInt(m[2], 10);
              if (colsCount > 0 && colsCount < 13) {
                classes += `col-${ m[1] }-${ 12 - colsCount} `;
              }
              m = regex.exec(this.labelCols);
          }
        }
        return classes.trim() || "col-sm-8";
      }
      case "no-label":
        return this.labelCols || "col-sm-12";
    }
  }

  /** Gets the label type with "horizontal" as default / unknown value. */
  protected getLabelType() {
    switch (this.labelType) {
      case "no-label":
      case "plain":
      case "basic":
      case "empty-label":
      case "inline":
        return this.labelType;
      default:
        return "horizontal";
    }
  }
}
