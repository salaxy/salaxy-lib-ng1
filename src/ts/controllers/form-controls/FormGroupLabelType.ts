/**
 * Defines how the label is positioned in relation to input.
 * Supported values are:
 *
 * - "horizontal" (default): Bootstrap form-horizontal form-control (https://getbootstrap.com/docs/3.3/css/#forms-horizontal)
 * - "basic": Basic Bootstrap form-group with label on top of input (https://getbootstrap.com/docs/3.3/css/#forms-example)
 * - "plain": Just the payload (typically input) with span class="salaxy-form-group-plain" around it.
 * - "no-label": Form control of width "col-sm-12" without label.
 *
 * Default is currently horizontal, but it may later be made configurable.
 */
export type FormGroupLabelType = "horizontal" | "no-label" | "plain" | "basic" | "empty-label" | "inline";
