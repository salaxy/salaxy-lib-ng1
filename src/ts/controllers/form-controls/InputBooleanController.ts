import { InputBase } from "./_InputBase";

/**
 * Controller behind boolean form controls (checkbox, radio, switch)
 */

export class InputBooleanController extends InputBase<boolean> {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

    /** Type of the input element. Default is checkbox. */
    public type: "checkbox" | "radio" | "select" | "switch";

    /** Text to show as a label for input with value FALSE
     * Supported by types radio and dropdown
     */
     public labelFalse: string;

     /** Text to show as a label for input with value TRUE
      * Supported by types radio, checkbox and dropdown
      */
     public labelTrue: string;

    /**
     * TODO: Is this needed when there's support for 'empty-label' ?
     * BS class for offsetting the input (no-label)
     */

    public offsetCols: string;

    /** If true, the radio buttons are aligned horizontally side-by-side */
    public horizontal: boolean;

     /**
      * Creates a new InputController
      * @ignore
      */
      constructor() {
        super();
      }

      /** Toggles boolean value. */
      public toggle() {
        this.value = !this.value;
        this.onChange();
      }
}
