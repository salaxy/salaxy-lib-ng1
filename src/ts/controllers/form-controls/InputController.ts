import { InputBase } from "./_InputBase";

/**
 * Base controller for form control groups (label, input, validation errors etc.).
 */
export class InputController extends InputBase<string> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Minimum length, 0 is the default */
  public minlength: number;

  /** Maximum length, 1024 is the default */
  public maxlength: number;

  /** The rows property of the textarea. Default is 3. Not used input, only textarea. */
  public rows: number;

  /** Regular expression pattern for validation */
  public pattern: string;

  /** If true, sets the readonly attribute of the input with ng-readonly. */
  public readonly: boolean;

  /**
   * Creates a new InputController
   * @ignore
   */
  constructor() {
    super();
  }

  /** Set the values and defaults on init */
  public $onInit() {
    if (!this.maxlength) {
      this.maxlength = 1024;
    }
    if (this.minlength) {
      this.require = true;
    }
    if (this.required) {
      this.require = true;
    }
    super.$onInit();
  }

  /** On change of the value, do preventive operations and set value to model. */
  public onChange() {
    if (this.value && this.maxlength && this.value.length > this.maxlength) {
      this.value = this.value.substring(0, this.maxlength);
    }
    super.onChange();
  }
}
