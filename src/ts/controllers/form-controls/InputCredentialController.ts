import * as angular from "angular";

import { SessionUserCredential } from "@salaxy/core";

import { WorkflowService } from "../../services";
import { InputEnumController } from "./InputEnumController";

/**
 * Controller for selecting a credential.
 */

export class InputCredentialController extends InputEnumController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WorkflowService"];

  private credentials: SessionUserCredential[] = [];

  /**
   * Creates a new InputOccupationTypeController
   * @ignore
   */
  constructor(private $scope, private workflowService: WorkflowService) {
    super();
  }

  /** Options list */
  public get options(): any[] {
    const newCredentials = this.workflowService.credentials || [];
    if (!angular.equals(this.credentials, newCredentials)) {
      this._options = newCredentials.map( (x) => {
        return {
          value: x.id,
          text: x.avatar.displayName,
        };
      });
    }
    return this._options;
  }
  public set options(value: any[]) {
    // empty on purpose
    // this._options = value;
  }
}
