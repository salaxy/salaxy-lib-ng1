import * as angular from "angular";

import { Arrays, Avatar, EnumerationsLogic, Objects } from "@salaxy/core";

import { InputController } from "./InputController";
import { InputEnumOption } from "./InputEnumOption";

/**
 * Controller behind form controls that select an occupation type.
 */
export class InputEnumController extends InputController {

  /**
   * Binds to an enumeration defined by the Salaxy API.
   * Set the name of the enumeration.
   */
  public enum: string;

  /** Type of the input element. Options are typeahead, select (default) and radio */
  public type: "typeahead" | "select" | "radio" | "list" | "multi-select";

  /** Options of the select control as a key-value object. */
  protected _options: any;

  /**
   * Array or comma separated string to filter the option values to just the given ones.
   * Also sets the order to this order, so works for ordering a given set of values.
   * Note that hiddenOptions is applied first, so if you want e.g. "undefined" to appear if selected,
   * you may add it here and it behaves as expected (undefined is still hidden if a value is selevted).
   */
  public filter: string[];

  /** If set to true, does not cache enums. */
  public disableCache = false;

  /**
   * These values are visible only if they are selected in the data.
   * Default is ["undefined"], set to empty array to show all in every state.
   * I.e. after something else is selected, hidden value cannot be selected back.
   * Use for not-selected values ("Please choose...") when you do not want selection reversed
   * or legacy data that is not selectable, but may still exist on the server.
   */
  public hiddenOptions: string[];

  /**
   * These values are not considered as valid in the UI.
   * Default is ["unknown"] and only used when input is "required" and input type is "select"
   * Use in situations where an unknown/other/none value is set in API but should not be available as an option.
   */
  public invalidEnums: string[];

  /** Function that is used in the type=list layout to render the avatar */
  public avatarFunc: (itemValue: InputEnumOption) => Avatar;

  private enumCache: InputEnumOption[] = [];

  /** Sets the default values in init. */
  public $onInit() {
    super.$onInit();
    if (!this.type) {
      this.type = "select";
    }
    if (!this.hiddenOptions) {
      this.hiddenOptions = ["undefined"];
    }
    if (!this.invalidEnums) {
      this.invalidEnums = ["unknown"];
    }
  }

  /** Options list */
  public get options(): any[] {
    return this._options;
  }
  public set options(value: any[]) {
    this._options = value;
  }

  /**
   * Gets the label for an enumeration.
   * @param value Type of the enum.
   */
  public getEnumerationLabel(value: string): string {
    const e = this.getEnumerations().find((x) => x.value === value);
    if (e) {
      return e.text;
    }
    return null;
  }

  /** Returns true if the placeholder should be added to the options. */
  public get showPlaceholder(): boolean {
    if (!this.value || this.value === "undefined" || this.value === "unknown") {
      if (!this.getEnumerations().find((x) => x.value === this.value)) {
        return true;
      }
    }
    return false;
  }

  /**
   * If the enumeration supports avatar, returns the avatar for type=list.
   * Support is added by defining avatarFunc.
   */
  public getAvatar(option: InputEnumOption): Avatar {
    if (this.avatarFunc) {
      return this.avatarFunc(option);
    }
    return null;
  }

  /**
   * Returns items for current enumeration.
   */
  protected getEnumerations(): InputEnumOption[] {
    if (this.enumCache.length > 0 && !this.disableCache) {
      return this.enumCache;
    }
    let allValues: InputEnumOption[] = [];
    const selectedValue = this.value;
    if (this.options) {
      if (Array.isArray(this.options)) {
        allValues = this.options.map((item: any) => {
          if (item == null) {
            return { value: null, text: "[null]" };
          }
          if (Objects.has(item, "value") || Objects.has(item, "text")) {
            return {
              value: item.value,
              text: item.text,
              title: (item.title && item.title !== item.value) ? item.title : null,
            };
          }
          return { value: item, text: item };
        });
      } else {
        allValues = Object.keys(this.options)
          .map((key) => ({ value: key, text: this.options[key], title: null }));
      }
    } else if (this.enum) {
      const enums = EnumerationsLogic.getEnumMetadata(this.enum);
      if (enums) {
        allValues = enums.values
          .map((e) => ({ value: e.name, text: e.label, title: e.descr && e.descr !== e.name ? e.descr : null }));
      } else {
        allValues = [{
          value: "",
          text: `ERROR: ${this.enum} not found.`,
          title: null,
        }];
      }
    }
    if (this.hiddenOptions) {
      allValues = allValues.filter((option) => {
        const matchingHiddenValue = Arrays.assureArray(this.hiddenOptions).find((hiddenValue) => hiddenValue === option.value);
        // Hidden is shown only if it is currently selected in data.
        return matchingHiddenValue == null || matchingHiddenValue === selectedValue;
      });
    }
    const filterArr = Arrays.assureArray(this.filter);
    if (filterArr.length > 0) {
      allValues = filterArr.map((x) => allValues.find((y) => y.value === x)).filter((x) => x != null);
    }
    this.modifyOptions(allValues, this.enumCache);
    return this.enumCache;
  }

  private modifyOptions(
    source: ({
      /** Enum value. */
      value: any,
      /** Label for the value. */
      text: string,
      /** Description for the value. */
      title?: string,
    })[],
    target: ({
      /** Enum value. */
      value: any,
      /** Label for the value. */
      text: string,
      /** Description for the value. */
      title?: string,
    })[]) {
    // check if source values differ from target
    const ok = angular.equals(source, target);

    if (ok) {
      return;
    }
    // remove values
    target.splice(0, target.length);

    // add values
    target.push(...source);
  }
}
