/** Defines an option in enumeration or within select element more generally */
export interface InputEnumOption {
  /**
   * Enum value (option value attribute).
   * NOTE: This may potentially be extended to other types in the future (bool, number, even object.)
   */
  value: string;

  /** Label for the value (text of option element). */
  text: string;

  /** Description for the value (title attribute of option element). */
  title?: string;

  /** Optional UI data that is related to the option (e.g. avatar and other visualization). */
  ui?: any;
}
