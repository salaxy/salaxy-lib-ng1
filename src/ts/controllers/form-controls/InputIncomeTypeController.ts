import { IncomeTypesLogic } from "@salaxy/core";

import { SessionService } from "../../services";
import { InputController } from "./InputController";

/**
 * Controller behind form controls that selects Income type code for transaction in Income Registry (Tulorekisteri).
 */

export class InputIncomeTypeController extends InputController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SessionService"];

  /** Type of the input element. Default is typeahead. */
  public type: "typehead" | "list";

  /**
   * List of income type codes to exclude from the selection list.
   */
  public hiddenCodes: number[];

  /**
   * Creates a new InputIncomeTypeController
   * @ignore
   */
  constructor(private sessionService: SessionService) {
    super();
  }

  /** Sets the default values in init. */
  public $onInit() {
    super.$onInit();
    if (!this.type) {
      this.type = "typehead";
    }
  }

  /** Gets the list of all income types. */
  public getIncomeTypes(searchString: string) {
    let result = IncomeTypesLogic.search(searchString);
    if (this.hiddenCodes) {
      result = result.filter( (x) => !this.hiddenCodes.find( (c) => c === x.code) );
    }
    return result;
  }

  /**
   * Returns the label for given income type.
   * @param code - Income type code.
   */
  public getRowLabel(code: string): string {
    if (!code) {
      return null;
    }
    const type = IncomeTypesLogic.allTypes.find((x) => x.code === Number(code));
    return type ? (type.code + " - " + type.label) : "Tulolajia ei löydy: " + code;
  }

  /** Returns the placeholder text - this control has a default text. */
  public getPlaceholder() {
    // TODO: Translate
    return super.getPlaceholder() || "Hae Tulorekisterin tulolajeista...";
  }

  /** Called by the view, when a value is selected. */
  protected selectionClicked(value: number) {
    this.value = value as any;
    this.onChange();
  }
}
