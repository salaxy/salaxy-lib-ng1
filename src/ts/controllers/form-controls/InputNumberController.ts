import {  CalcRowsLogic, CalculationRowUnit, Numeric } from "@salaxy/core";

import { InputBase } from "./_InputBase";

/**
 * Controller behind form controls that select an occupation type.
 */
export class InputNumberController extends InputBase<number> {

    /**
     * Unit for the number.
     * If set, shows a visual clue of the unit.
     * For 'percent' editor value is multiplied by 100.
     */
    public unit: CalculationRowUnit;

    /**
     * If true, the input edits negative value: The value is multiplied by -1.
     * E.g. "3" days ago => -3.
     */
    public negative: boolean;

    /** Sets the default values in init. */
    public $onInit() {
      super.$onInit();
    }

    /**
     * Gets or sets the string value that is used in the view.
     * For percent, this is multiplied by 100.
     */
    public get viewValue(): string {
      let valueNum = Numeric.parseNumber(this.value); // Historically, the value may be string e.g. "3" instead of 3.
      if (valueNum != null && this.unit === CalculationRowUnit.Percent) {
        valueNum = valueNum * 100;
      }
      if (this.negative && valueNum) {
        valueNum = valueNum * -1;
      }
      return valueNum == null ? null : Numeric.toString(valueNum).replace(".", ",");
    }
    public set viewValue(value: string) {
      let valueNum = Numeric.parseNumber(value);
      if (valueNum != null && this.unit === CalculationRowUnit.Percent) {
        valueNum = valueNum / 100;
      }
      if (this.negative && valueNum) {
        valueNum = valueNum * -1;
      }
      this.value = valueNum;
      this.onChange();
    }

    /**
     * Gets an indicator string (1-2 characters) for the unit.
     */
    public getUnitIndicator() {
      return CalcRowsLogic.getUnitIndicator(this.unit);
    }
}
