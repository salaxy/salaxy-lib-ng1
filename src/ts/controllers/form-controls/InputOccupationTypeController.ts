import { Occupation, Occupations } from "@salaxy/core";

import { SessionService } from "../../services";
import { InputController } from "./InputController";

/**
 * Controller behind form controls that select an occupation type.
 */

export class InputOccupationTypeController extends InputController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SessionService"];

  /**
   * One or a list of occupation IDs or a known keyword to define which occupations are shown
   * Supported keywords: 'household' and 'company'
   */
  public defaultList: string;

  /**
   * Creates a new InputOccupationTypeController
   * @ignore
   */
  constructor(private sessionService: SessionService) {
    super();
  }

  /** Gets the list of all occupations. */
  public getOccupations(searchString: string): Occupation[] {
    if (!searchString || searchString.length < 2) {
      const defaultList = this.defaultList || this.sessionService.isInSomeRole("household,worker") ? "household" : "company";
      return Occupations.getByIds(defaultList);
    }
    return Occupations.search(searchString);
  }

  /**
   * Returns the label for given occupation id.
   * @param occupationId - Id of the occupation.
   */
  public getOccupationLabel(occupationId: string): string {
    for (const occupation of Occupations.getAll(null)) {
      if (occupationId === occupation.id) {
        return occupation.label;
      }
    }
    return null;
  }

  /** Returns the placeholder text - this control has a default text. */
  public getPlaceholder() {
    // TODO: Translate
    return super.getPlaceholder() || "Kirjoita hakusana luokitukselle...";
  }
}
