import * as angular from "angular";

import { ApiValidation, ApiValidationError, ApiValidationErrorType } from "@salaxy/core";

import { Ng1Translations, UiHelpers } from "../../services";

/**
 * Handles user interaction for viewing and modifying the current account data
 * including the products that are enabled for the current account and their properties.
 */
export class ValidationSummaryController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Ng1Translations", "UiHelpers"];

  /** The server-side API-validation object that is displayed by this controller. */
  public apiValidation: ApiValidation;

  /** HTML that should be shown when the bound validation data is null */
  public loadingHtml: "Ladataan validointi-tietoa...";

  constructor(
    private translate: Ng1Translations,
    private uiHelpers: UiHelpers,
  ) {
    // constructor code.
  }

  /**
   * Implement IController
   */
  public $onInit = () => { /* init code here */ };

  /** If false, there is no validation object bound to the component. */
  public get hasValidation(): boolean {
    return !!this.apiValidation;
  }

  /** If true, the validation has no errors. */
  public get isValid(): boolean {
    return this.getValidation().isValid;
  }

  /** If false, has empty required fields. These are shown separately from other errors. */
  public get hasAllRequiredFields(): boolean {
    return this.apiValidation.hasAllRequiredFields;
  }

  /** If true, has other errors than missing required fiels. */
  public get hasValidationOrGeneralErrors(): boolean {
    return this.getErrors("!required").length > 0;
  }

  /** Gets the errors, optionally filtered by a type.
   * @param type Type of the error: "general", "required", "invalid" or null.
   * If null, all errors are returned.
   * Also supports negation: "!general", "!required" or "!invalid".
   */
  public getErrors(type?: ApiValidationErrorType | string): ApiValidationError[] {
    if (type) {
      if (type.toString().startsWith("!")) {
        type = type.substr(1);
        return this.getValidation().errors.filter((x) => x.type !== type);
      } else {
        return this.getValidation().errors.filter((x) => x.type === type);
      }
    }
    return this.getValidation().errors;
  }

  /**
   * Returns the label for the validation result.
   * The label is translated using the key: `SALAXY.VALIDATION.[error.code].label`
   * If the translation does not exist the `error.msg` is returned.
   * @param error - Error in validation.
   */
  public getLabel(error: ApiValidationError) {
    const labelKey = "SALAXY.VALIDATION." + error.code + ".label";
    let translation = this.translate.get(labelKey);
    translation = translation !== labelKey ? translation : null;
    return translation || error.msg;
  }

  /**
   * Returns the description for the validation result.
   * The label is translated using the key: `SALAXY.VALIDATION.[error.code].description`
   * If the translation does not exist the `null` is returned.
   * @param error - Error in validation.
   */
  public getDescription(error: ApiValidationError): boolean {
    const descriptionKey = "SALAXY.VALIDATION." + error.code + ".description";
    let translation = this.translate.get(descriptionKey);
    translation = translation !== descriptionKey ? translation : null;
    return translation;
  }

  /**
   * Returns true if the translation for the key `SALAXY.VALIDATION.[error.code].description` exists.
   * @param error - Error in validation.
   */
  public hasDescription(error: ApiValidationError): boolean {
    return !!this.getDescription(error);
  }

  /** Shows details of the errors. */
  public showDetails() {
    this.uiHelpers.showDialog(
      `salaxy-components/form-controls/ValidationSummaryDetails.html`,
      null, // controller
      {
        controller: () => this,
      },
      null, // window template URL
    );
  }

  private getValidation(): ApiValidation {
    return this.apiValidation || {
      errors: [],
      hasAllRequiredFields: true,
      isValid: false,
    };
  }

}
