import * as angular from "angular";

import { Objects } from "@salaxy/core";

import { FormGroupLabelType } from "./FormGroupLabelType";

/**
 * Base controller for form control groups (label, input, validation errors etc.).
 */
export class InputBase<T> implements angular.IController {

  /**
   * The following component properties (attributes in HTML) are bound to the Controller.
   * For detailed functionality, refer to [controller](#controller) implementation.
   */
  public static crudBindings = {

    /** Name of the input - also used as id. */
    name: "@",

    /** Label for the control */
    label: "@",

    /**
     * @deprecated Use "require" instead:
     * This attribute overlaps with required/ng-required directive, which produces unexpected results.
     */
    required: "@",

    /** If true the field is required (form validation) */
    require: "<",

    /**
     * Expression for read-only display of the input control
     * This is visualized in a different way than readonly: The input is not shown and display is more compact.
     */
    readOnly: "<",

    /** Expression for ng-readonly of the input */
    readonly: "<",

    /** Expression for ng-disabled of the input */
    disabled: "<",

    /**
     * Positioning of the label of form-control.
     * Supported values are "horizontal" (default), "no-label", "plain" and "basic".
     * See FormGourpLabelType for details.
     */
    labelType: "@",

    /**
     * Label columns expressed as Bootstrap grid columns. Supports multiple classes (e.g. "col-xs-4 col-sm-2").
     * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
     * Other label-types do not have column classes at the moment.
     */
    labelCols: "@",

    /** Placeholder text */
    placeholder: "@",

    /**
     * TooltipHtml adds an info button after the input which opens a uib-popover-html.
     */
    tooltipHtml: "@",

    /** Options for ui.bootstrap.popover */
    tooltipPlacement: "@",

    /**
     * Disables the default validation error message.
     * Use this if you want to create custom error messages in the UI.
     */
    disableValidationErrors: "<",
  };

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Label for the control. If not set, it is derived from name. */
  public label: string;

  /**
   * Positioning of the label of form-control.
   * Supported values are "horizontal" (default), "no-label" | "plain" | "basic" | "empty-label" | "inline".
   * See FormGourpLabelType for details.
   */
  public labelType: FormGroupLabelType;

  /** The model that is bound to the input */
  public model: angular.INgModelController;

  /** Form controller, if available */
  public form: angular.IFormController;

  /** The value of the input */
  public value: T;

  /** Form control name */
  public name: string;

  /** List of validation error texts (typically language version keys that should be translated). */
  public validationErrors: string[] = [];

  /** Placeholder text */
  public placeholder: string;

  /**
   * @deprecated Use "require" instead:
   * This attribute overlaps with required/ng-required directive, which produces unexpected results.
   */
  public required: string;

  /**
   * If true, displays the control as read-only div instead of the input control.
   * This "read-only" attribute is visualized in a different way than standard html "readonly" (ng-readonly):
   * The input is not shown and display is more compact.
   * Also note, that you may change the same read-only for the form using readonly / ng-readonly attribute.
   * @example
   * ```html
   * <form name="foo" ng-readonly="true">
   * <!-- .... or plain HTML ... -->
   * <form name="foo" readonly="true">
   * <!-- .... or plain HTML ... -->
   * <salaxy-input name="input1" read-only="true"></salaxy-input>
   * ```
   */
  public readOnly: boolean;

  /** If true, disabled is set on the input with ng-disabled */
  public disabled: boolean;

  /** Placement of tooltip button */
  public tooltipPlacement = "bottom-right";

  /** Defines HTML for tooltip button */
  public tooltipHtml: string;

  private requirePendingUpdate: boolean | undefined;

  /**
   * Creates a new InputController
   * @ignore
   */
  constructor() {
    /* */
  }

  /**
   * Set the values and defaults on init.
   * When overridding at derived classes, do your own stuff first and then call this method
   * because this method calls validate() at the end.
   */
  public $onInit() {
    if (!this.name) {
      throw new Error(`Property 'name' is required for Salaxy form controls (label: ${this.label}).`);
    }
    if (this.name && this.label == null) {
      this.label = this.name;
    }
    if (this.requirePendingUpdate) {
      this.require = true;
      this.requirePendingUpdate = undefined;
    }
    this.model.$render = () => {
      this.value = this.model.$viewValue;
    };
    this.validate();
  }

  /** Gets or sets the presence of the required validator in the underlying model. */
  public get require(): boolean {
    return !!this.model?.$validators.required;
  }
  public set require(value: boolean) {
    if (!this.model) {
      this.requirePendingUpdate = value;
      return;
    }
    const hasRequiredValidator = !!this.model.$validators.required;
    if (value && !hasRequiredValidator) {
      this.model.$validators.required = (modelValue, viewValue) => {
        return !this.model.$isEmpty(viewValue);
      };
    } else if (!value && hasRequiredValidator) {
      delete this.model.$validators.required;
    }
  }

  /** Gets the readonly value either from the controller or from the form */
  protected getReadOnly(): boolean {
    if (this.readOnly) {
      return true;
    }
    if (this.form && this.form.$$element && this.form.$$element[0]) {
      return !!this.form.$$element[0].attributes.readonly;
    }
    return false;
  }

  /** Gets the disabled value either from the controller or from the form */
  protected getDisabled(): boolean {
    if (this.disabled) {
      return true;
    }
    if (this.form && this.form.$$element && this.form.$$element[0]) {
      return !!this.form.$$element[0].attributes.disabled;
    }
    return false;
  }

  /** Returns the placeholder text: We will probably add stuff about validation here if not explicitly set. */
  protected getPlaceholder() {
    return this.placeholder;
  }

  /** Gets the tooltip HTML with necessary escape. */
  protected getToolTipHtml() {
    // TODO: Add escaping if necessary.
    return this.tooltipHtml;
  }

  /** On change of the value, do preventive operations and set value to model. */
  protected onChange() {
    this.model.$setViewValue(this.value);
    this.validate();
  }

  /** Do validations. Automatically called on onInit() and onChange() */
  protected validate() {
    const validationErrors = [];
    for (const validationErrorKey in this.model.$error) {
      if (Objects.has(this.model.$error, validationErrorKey) && validationErrorKey !== "parse") {
        validationErrors.push("SALAXY.VALIDATION.ValidationErrors." + validationErrorKey);
      }
    }
    this.modifyValidationErrors(validationErrors, this.validationErrors);
    if (this.validationErrors.length > 0) {
      return false;
    }
    return true;
  }

  private modifyValidationErrors(source: string[], target: string[]) {
    // check if source values differ from target
    if (angular.equals(source, target)) {
      return;
    }
    // remove values
    target.splice(0, target.length);
    // add values
    target.push(...source);
  }
}
