import * as angular from "angular";

/**
 * Controller for action button
 */
export class ActionButtonController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Text for the button */
  public label: string;

  /** Additional Bootstrap or custom style classes. I.e. btn-danger, btn-sm, my-btn. Defaults to btn-primary. */
  public buttonClass: string;

  /**
   * Action to be executed.
   * newCalc: starts a new blank calculation.
   * newCalcForWorker: starts a new calculation for the current worker.
   * newPayroll: starts a new blank payroll.
   * newWorker: opens workerWizard for creating a new worker.
   * newMessage: starts a new message thread with partner.
   * payment: opens the payment dialog for the current calculation.
   * newTaxcard: Links to taxcards page. Currently supported only when role is Worker.
   */
  public action: "newCalc" | "newCalcForWorker" | "newPayroll" | "newWorker" | "newMessage" | "payment" | "newTaxcard";

  /** Disabled but visible */
  public disabled?: boolean;

  /** Optional options for button. */
  public options?: {
    /**
     * id: id of current item such as worker or calculation. Currently used only in newCalcForWorker action.
     */
    id: string,
  };

  /**
   * Creates a new InputController
   * @ignore
   */
  constructor() {
    //
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    this.buttonClass = this.buttonClass || "btn-primary";
  }

}
