import * as angular from "angular";

/**
 * Controller rendering an Alert box in the page area.
 */
export class AlertController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$transclude"];

    /** Type of the alert is the Bootstrap style: Note that also "primary" and "default" are supported. */
    public type: "default" | "primary" | "success" | "info" | "warning" | "danger";

    /**
     * Alert main content as simple text.
     * You can alternatively provide html as main element.
     * NOTE: If necessary we could provide a html property to bind from JavaScript and to allow HTML in the binded content.
     */
    public text: string;

    /**
     * Possibility to speicfy a font-awesome icon.
     * Setting "none", will show no icon.
     * If not set, it is determined by type.
     */
    public icon: "none" | string;

    /** If true (and hasMoreInfo is true), the more info html is shown. */
    public showMoreInfo: boolean;

    /**
     * Creates a new AlertController
     * @ignore
     */
    constructor(private $transclude: angular.ITranscludeFunction) {
     }

    /**
     * Implement IController
     */
    public $onInit = () => {
        // initialization
    }

    /** Returns true if details part (aside element) is filled / "More info" button should be shown. */
    public get hasMoreInfo() {
        return this.$transclude.isSlotFilled("aside");
    }

    /** Returns the icon based on the type. */
    public getIcon() {
        if (!this.icon) {
            switch (this.type) {
                case "danger":
                    return "fa-exclamation";
                case "success":
                    return "fa-check";
                case "warning":
                    return "fa-exclamation-triangle";
                default:
                    return "fa-info";
            }
        }
        if (this.icon === "none") {
            return null;
        }
        return this.icon;
    }
}
