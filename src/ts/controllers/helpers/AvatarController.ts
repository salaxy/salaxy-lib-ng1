import * as angular from "angular";

import {AvatarPictureType} from "@salaxy/core";

/**
 * Controller rendering a Salaxy account Avatar (image for the person / company).
 */
export class AvatarController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

    /** The Avatar object which the controller is rendering */
    public avatar: any;

    /**
     * Creates a new AvatarController
     * @ignore
     */
    constructor() {
        //
     }

    /**
     * Implement IController
     */
    public $onInit = () => {
        // initialization
     }

    /** Returns true if the avatar is an image url (as opposed to icon rendering) */
    public get isImage(): boolean {
        return this.avatar != null
            && ( this.avatar.pictureType === AvatarPictureType.Uploaded || this.avatar.pictureType === AvatarPictureType.Gravatar)
            && this.avatar.url != null
            && this.avatar.url.substr(0, 4).toLowerCase() === "http"
            ;
    }

    /** Returns true if the avatar should be rendered as a person icon */
    public get isPersonIcon(): boolean {
        return this.avatar != null && !this.isImage && this.avatar.entityType !== "company";
    }

    /** Returns true if the avatar should be rendered as a company icon */
    public get isCompanyIcon(): boolean {
        return this.avatar == null || (!this.isImage && this.avatar.entityType === "company");
    }

    /** Returns the avatar color or 'gray' if nothing is defined. */
    public get color(): string {
        if (this.avatar) {
            return this.avatar.color;
        }
        return "gray";
    }

    /** Returns the initials for rendering the avatar */
    public get initials(): string {
        if (this.avatar) {
            return this.avatar.initials || "-";
        }
        return "?";
    }
}
