import * as angular from "angular";

import { Dates, Translations } from "@salaxy/core";

import { CalendarDay, CalendarMonth, CalendarSeries, CalendarUiEvent } from "./calendar"
import { UiHelpers } from "../../services";
import { ListControllerBase } from "../bases";

/**
 * Renders a calendar control for visualisation of holidays.
 */
export class CalendarController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["UiHelpers"];

    /**
     * Start date for the calendar as ISO date yyyy-MM-dd. Renders from the beginning of this month.
     */
    public start: string;

    /**
     * End date of the calendar as ISO date yyyy-MM-dd. Renders until the end of this month.
     */
    public end: string;

    /**
     * Calendar data where single series can be represented as an array of events (CalendarUiEvent or date ISO string).
     * Several series should be represented as CalendarSeries that contain events.
     * Note that a DateRange also passes as CalendarUiEvent because it has start and end dates.
     */
    public data: CalendarSeries[] | (CalendarUiEvent | string)[] | any;

    /**
     * Function that is called when user selects an item in the calendar.
     * Function can have the following locals: type: "event" | "day", date: string, day: Full calendar day with all the day info,
     * calEvent: The calendar event if type is "event", series: Series that the event belongs to or the series that is clicked (not always available).
     * @example <salaxy-calendar on-list-select="$ctrl.myCustomSelectFunc(type, date, calEvent)"></salaxy-calendar>
     */
    public onListSelect: (params: {
      /** Selection type. */
      type: "event" | "day",
      /** Seleted date as ISO string. */
      date: string,
      /** Full calendar day with all the day info. */
      day: CalendarDay,
      /** Calendar event that was clicked: Only if selection type is "event." */
      calEvent?: CalendarUiEvent,
      /**
       * Series that the event belongs to or the series that is clicked.
       * Note that some clicks of type "day" might not have the series info, in current UI the header above series when calendar supports multiple series.
       */
      series?: CalendarSeries,
    }) => void;

    /** Language versioned weekdays short texts (e.g. "ma", "ti",...) */
    public weekdays: string[];

    /** Get the header days for the whole calendar. */
    public weekdaysForMonth: {
      /** Weekday index number (align-weekdays) or day of month (align-left). */
      ix: number,
      /** Week day number in week (align-weekdays). */
      weekday?: number,
      /** Text for the week day (align-weekdays) or day of month (align-left). */
      text: string,
    }[] = [];

    /** A mapper that is used in converting custom data to Calendar series array. */
    public mapper: (sourceData: any) => CalendarSeries[];

    /** If specified, this is the list controller that should be used for editing the item. */
    public listController: ListControllerBase<any, any>;

    private _today: string;

    private _mode: "align-weekdays" | "align-left" | "horizontal" | "list";

    private _series: CalendarSeries[];

    constructor(private uiHelpers: UiHelpers) {
      // for dependency injection
    }

    /**
     * Implement IController
     */
    public $onInit = () => {
      // Default values if not set, also validate and harmonize them for dates only (remove time and convert e.g. "today")
      this.start = Dates.asDate(this.start) || Dates.getDate("today", 1, 1);
      this.end = Dates.asDate(this.end) || Dates.getDate("today", 12, 31);
      this.today = Dates.asDate(this.today) || Dates.getToday();
      this.weekdays = Array.from("1234567").map((x) => Translations.getWithDefault(`SALAXY.ENUM.Weekday.day${x}.short`, "d" + x));
      this.initHeader();
    }

    /** Initialize the header (days) */
    public initHeader() {
      switch (this.mode) {
        case "align-left":
          this.weekdaysForMonth = [...Array(31)].map((val, ix) => ({ ix, text: (ix + 1).toString()}));
          break;
        case "horizontal":
          break;
        default:
          // Align the weekdays
          for (let i = 0; i < 5; i++) {
            this.weekdaysForMonth.push(...this.weekdays.map((x, index) => ({ ix: i * 7 + index, weekday: index + 1, text: x}) ));
          }
          this.weekdaysForMonth.push({ ix: 5 * 7 + 0, weekday: 1, text: this.weekdays[0]} , { ix: 5 * 7 + 1, weekday: 2, text: this.weekdays[1]});
          break;
      }
    }

    /**
     * Specifies how the weekdays are aligned from one month to another.
     * "horizontal" shows all months side-by-side horizontally, so there is no alignment.
     */
    public get mode(): "align-weekdays" | "align-left" | "horizontal" | "list" {
      switch (this._mode) {
        case "align-left":
        case "horizontal":
        case "list":
          return this._mode;
        default:
          return "align-weekdays"
      }
    }
    public set mode(value: "align-weekdays" | "align-left" | "horizontal" | "list") {
      this._mode = value;
    }

    /** Gets the Today date for the calendar as ISO date yyyy-MM-dd. */
    public get today(): string {
      if (!this._today) {
        this._today = Dates.getToday();
      }
      return this._today
    }
    /** Sets the Today date as dately object (e.g. ISO string, JS Date or key string "today") */
    public set today(value: string) {
      this._today = Dates.asDate(value);
    }

    /** Gets all days of all months */
    public get allDays(): CalendarDay[] {
      // TODO: create typing to months / allDays results.
      return this.uiHelpers.cache(this, "allDays", () => {
        return this.months.reduce((acc, current) => acc.concat(current.days), []);
      }, () => this.cacheCounter)
    }

    /**
     * Gets the data harmonized as an array of Calendar Series
     */
    public get series(): CalendarSeries[] {
      return this._series;
    }

    /**
     * Gets the monthly list: Only the dates that have events on them.
     * Only the start of event is considered except for the first month where all intercepting properties are.
     */
    public get monthlyList(): CalendarMonth[] {
      return this.uiHelpers.cache(this, "monthList", () => {
        const months = this.months.map((month) => {
          const monthResult: CalendarMonth = {
            daysInMonth: month.daysInMonth,
            firstDayDayOfWeek: month.firstDayDayOfWeek,
            month: month.month,
            title: month.title,
            year: month.year,
            days: month.days.map((day) => {
              let dayEventsCount = 0;
              Object.keys(day.events).reduce((acc, key) => {
                // TODO: We are only using dayEventsCount => Figure out a simpler way to find it.
                acc[key] = day.events[key];
                dayEventsCount += acc[key].length;
                return acc;
              }, {});
              if (dayEventsCount) {
                return {
                  day: day.day,
                  date: day.date,
                  weekday: day.weekday,
                  weekdayText: day.weekdayText,
                  dayType: day.dayType,
                  events: day.events.map((series) => series.filter((val) => day.date === val.start)),
                  periods: null, // Add support if needed
                  singleDays: null,
                };
              }
              return null;
            }).filter(x => !!x),
          };
          return monthResult;
        });
        return months;
      }, () => this.cacheCounter);
    }

    /** The collection of months that the calendar renders. */
    public get months(): CalendarMonth[] {
      return this.uiHelpers.cache(this, "months", () => {
        const result = [];
        const start = Dates.getMoment(this.start || "today").startOf("month");
        const end = Dates.getMoment(this.end || "today").startOf("month");
        this.assureNormalizedCopy();
        const data = this.series || [];
        if (!start.isValid || !end.isValid || start > end) {
          console.error("Invalid date range in CalendarController.");
          return [];
        }
        const currentMonth = start;
        while (currentMonth <= end) {
          const daysInMonth = currentMonth.daysInMonth();
          const firstDayDayOfWeek = currentMonth.isoWeekday();

          const month: CalendarMonth = {
            month: currentMonth.month() + 1,
            year: currentMonth.year(),
            title: Translations.get("SALAXY.UI_TERMS.monthShort" + (currentMonth.month() + 1)),
            firstDayDayOfWeek,
            daysInMonth,
            days: [],
          };
          for (let day = 1; day <= daysInMonth; day++) {
            const dateMoment = currentMonth.clone().date(day);
            const weekday = dateMoment.isoWeekday();
            const date = Dates.asDate(dateMoment);
            // TODO: Do we need separate periods/singleDays any more?
            const periods = data.map((series) => series.events.filter((val) => val.end && !val.icon && val.start <= date && val.end >= date));
            const singleDays = data.map((series) => series.events.filter((val) => (!val.end || val.icon) && date === val.start));
            // TODO: Is it more efficient to concatenate from the 2 above?
            const events = data.map((series) => series.events.filter((val) => date === val.start || (val.end && !val.icon && val.start < date && val.end >= date)));
            month.days.push({
              day,
              date,
              weekday,
              weekdayText: this.weekdays[weekday - 1],
              dayType: date === this.today ? "today" : (Dates.isHoliday(date) ? "holiday" : "normal"),
              events,
              periods,
              singleDays,
            });
          }
          result.push(month);
          currentMonth.add(1, "month");
        }
        return result;
      }, this.getCacheKey);
    }

    /**
     * Gets the short text "ma", "ti", "ke", "to", "pe", "la", "su".
     * @param date Date as ISO string or date
     */
    public getShortWeekdayText(date: any) {
      return this.weekdays[Dates.getMoment(date).isoWeekday() - 1];
    }

    /** Gets the CSS class(es) for a calendar event. */
    public getCssClass(event: CalendarUiEvent): string {
      if (!event) {
        return null;
      }
      if (event.icon) {
        // This is an icon (<i>-tag).
        return `fa ${event.icon} fa-fw event-single-day ${event.cssClass || "text-info"} clickable`;
      }
      return `event-period ${event.cssClass || "salaxy-cal-event-primary"} clickable`;
    }

    /**
     * Get the event CSS style for a single day. Main thing is to set the disposition and smaller size if the day has overlapping items.
     * @param event Event for which to calculate the styles.
     * @param allEvents All events in the given series/type: Either period or singleDay.
     */
    public getEventStyle(event: CalendarUiEvent, allEvents: CalendarUiEvent[]): any {
      if (event.icon) {
        const ix = allEvents.indexOf(event);
        switch (allEvents.length) {
          case 1:
            return null; /* bottom: 4px; left: -4px; font-size: 16px; */
          case 2:
            return {
              "font-size": "12px",
              bottom: (ix === 1 ? 0 : 8) + "px",
              left: (ix === 1 ? 2 : -4) + "px",
            };
          case 3:
            return {
              "font-size": "10px",
              bottom: (ix > 1 ? 0 : 8) + "px",
              left: (ix === 0 ? -4 : (ix === 2 ? 0 : 4)) + "px",
            };
          default:
            return {
              "font-size": "10px",
              bottom: (ix > 1 ? 0 : 8) + "px",
              left: (ix === 0 || ix === 2 ? -4 : 4) + "px",
            };
        }
      } else {
        if (allEvents.length > 1) {
          const height = (this.mode === "horizontal" ? 30 : 16)/allEvents.length;
          const ix = allEvents.indexOf(event);
          return {
            height: height + "px",
            bottom: (height*(allEvents.length-1-ix)) + "px",
          };
        }
      }
      return null;
    }

    /**
     * A date in the calendar was clicked.
     * @param day Day that was clicked.
     * @param series The series that contains the event.
     * If null, the click is in header etc. where series is not available.
     * @param $event The AngularJS event used to stop propagation.
     */
    public dateClicked(day: CalendarDay, series: CalendarSeries, $event: angular.IAngularEvent) {
      $event.stopPropagation();
      series = series || this.series[0];
      if (this.listController && series) {
        this.listController.parent = series.data;
        const newItem = this.listController.getBlank();
        if (newItem.period) {
          // HACK: Could we move this to object specific CRUD or List controllers.
          newItem.period.start = day.date;
          newItem.period.end = day.date;
        }
        this.listController.showEditDialog(newItem, true);
        return;
      }
      this.onListSelect({
        type: "day",
        date: day.date,
        day,
        series,
      });
    }

    /**
     * Called by view when an event is clicked.
     * @param ev Event that is being clicked
     * @param day Day on which the event occurs
     * @param series The series that contains the event. If null, the first series is taken.
     * @param $event AngularJS event: Used for stopping propagation.
     */
    public eventClicked(ev: CalendarUiEvent, day: CalendarDay, series: CalendarSeries, $event: angular.IAngularEvent) {
      $event.stopPropagation();
      series = series || this.series[0];
      if (this.listController && series) {
        this.listController.parent = series.data;
        this.listController.showEditDialog(ev.data);
        return;
      }
      this.onListSelect({
        type: "event",
        date: day.date,
        day,
        calEvent: ev,
        series,
      });
    }

    /**
     * Copies the data as normalized data set so that we can harmonize the dates and potentially make other changes in the future.
     * Copy is shallow copy of CalendarUiEvent.
     */
    private assureNormalizedCopy(): CalendarSeries[] {
      // TODO: The whole data pump is now dependent of the trigger of "months" getter ,which checks the cache key.
      //       This process could be more explicit / clearer (perhaps in this method?).
      const result: CalendarSeries[] = [];
      if (!this.data) {
        return result;
      }
      if (this.mapper) {
        const newData: CalendarSeries[] = this.mapper(this.data);
        newData.forEach((item, ix) => {
          result.push({
            key: item.key || "Series" + ix,
            title: item.title,
            description: item.description,
            avatar: item.avatar,
            events: item.events.map((ev) => Object.assign({}, ev)),
            data: item.data,
          });
        });
      } else {
        // Make a copy of the data and in process map to CalendarSeries[] array.
        const defaultSeries: CalendarSeries = {
          key: "default",
          avatar: null,
          title: null,
          events: [],
          data: null,
        };
        this.data.forEach((item, ix) => {
          if (typeof item === 'string') {
            defaultSeries.events.push({ start: item });
          } else if ((item as CalendarUiEvent)?.start) {
            defaultSeries.events.push(Object.assign({}, item));
          } else if ((item as CalendarSeries)?.key === "default") {
            defaultSeries.events.push(...item.events.map((ev) => Object.assign({}, ev)));
            defaultSeries.avatar = item.avatar || defaultSeries.avatar;
            defaultSeries.title = item.title || defaultSeries.title;
            defaultSeries.description = item.description || defaultSeries.description;
          } else if (Array.isArray((item as CalendarSeries)?.events)) {
            result.push({
              key: item.key || "Series" + ix,
              title: item.title,
              description: item.description,
              avatar: item.avatar,
              events: item.events.map((ev) => Object.assign({}, ev)),
              data: item.data,
            });
          }
        });
        if (defaultSeries.events.length) {
          result.splice(0, 0, defaultSeries);
        }
      }
      // Harmonize data
      result.forEach((series) => {
        series.events.forEach((ev) => {
          ev.start = Dates.asDate(ev.start);
          ev.end = Dates.asDate(ev.end);
          if (!ev.end) {
            ev.icon = ev.icon || "fa-circle";
          }
        });
      });
      this.cacheCounter++;
      this._series = result;
    }

    /** Get a simplified cache key for caching months / days. */
    private getCacheKey = () => {
      return angular.copy({
        data: this.data,
        start: "" + this.start,
        end: "" + this.end,
        mode: this.mode || "",
        today: this._today,
      });
    }

    /**
     * Cache counter is updated by months getter, which is the foundation of data refresh and observes full getCacheKey.
     * Rest of the methods can just check this simpler cacheCounter for refreshes.
     */
    private cacheCounter = 0;

}
