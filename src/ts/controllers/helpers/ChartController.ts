import * as angular from "angular";

import { Objects } from "@salaxy/core";

/**
 * Controller rendering a angular-chart.js (http://jtblin.github.io/angular-chart.js) with Salaxy defaults, caching, colors etc.
 */
export class ChartController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

    /**
     * Function that is called for chart data, options and other properties of chart.
     * The caching makes sure that the function is not called more often than once a second.
     * Binding to Angular UI is only done if the datra property changes.
     * @example <salaxy-chart on-get-chart="$ctrl.getChart()"></salaxy-chart>
     */
    public onGetChart: () => {

      // TODO: Get the official typing from DefinitelyTyped.

      /** Type of the chart */
      type: "line" | "bar" | "doughnut" | "radar" | "pie" | "polarArea" | "horizontalBar" | "bubble",

      /** series data */
      data: any[],

      /** x axis labels */
      labels: any,

      /** Chart.js options */
      options?: any,

      /** series labels */
      series?: any,

      /** onclick event handler */
      click?: any,

      /** onmousemove event handler */
      hover?: any,

      /** colors for the chart */
      colors: any

      /** override datasets individually */
      datasetOverride: any,
    };

    /** Type of the chart. */
    public type: "line" | "bar" | "doughnut" | "radar" | "pie" | "polarArea" | "horizontalBar" | "bubble";

    private chartCache: any = {};

    private chartCacheUpdated: number;

    constructor() {
      // Dependency injection here.
    }

    /**
     * Implement IController
     */
    public $onInit = () => {
        // initialization
    }

    /** Force refresh of cache */
    public refresh() {
      this.chartCache = null;
    }

    /**
     * Gets the cached version of chart data and options.
     * Cache refreshes if the type or data changes.
     * However, the check for data is done only every 1 second.
     */
    public getChartCached() {
      if (
        (!this.type || this.type === this.chartCache.type)
        && this.chartCache.data && this.chartCache.data.length > 0
        && Date.now() - (this.chartCacheUpdated || 0) < 1000) {
        // Time-based cache means that onGetChart is checked max once-a-second except when type changed or data is null / empty array.
        return this.chartCache;
      }
      const activeChart = (this.onGetChart ? this.onGetChart() : null) || { data: [], labels: [], type: "line" };
      // It is possible that the type comes from onGetChart, but the controller type property overrides.
      if (this.type) {
        activeChart.type = this.type;
      }
      activeChart.type = activeChart.type || "line";
      if (activeChart.type !== this.chartCache.type
        || !Objects.equal(activeChart.data, this.chartCache.data)) {
        this.chartCache = activeChart;
        this.chartCacheUpdated = Date.now();
      }
      return this.chartCache;
    }
}
