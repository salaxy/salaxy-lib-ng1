import { ImportLogic, ImportResult } from "@salaxy/core";

import { UiHelpers, WizardService, WizardStep } from "../../services";
import { WizardController } from "../bases";

/**
 * UNDERCON: Provides functionality to import Workers to the Salaxy system.
 */
export class ImportController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WizardService", "UiHelpers"];

  /** Results of the import. */
  public result: ImportResult<any>;

  private _tsv;

  constructor($scope: angular.IScope,  wizardService: WizardService, private uiHelpers: UiHelpers) {
    super($scope, wizardService);
  }

  /**
   * Initialization of the controller
   */
  public $onInit() {
    super.$onInit();
    this.wizardService.setSteps(this.getWizardSteps());
    this.wizardService.activeStepNumber = 1;
  }

  /** Pay certificate wizard configuration */
  public getWizardSteps(): WizardStep[] {
    return [
      {
        title: "Liitä / lataa taulukko",
        view: "salaxy-components/modals/import/step1.html",
      },
      {
        title: "Asetukset",
        view: "salaxy-components/modals/import/step2.html",
      },
      {
        title: "Validointi",
        view: "salaxy-components/modals/import/step3.html",
      },
      {
        title: "Tallennus",
        view: "salaxy-components/modals/import/step4.html",
      },
    ];
  }

  /**
   * Does the import process from Excel copy-paste until
   * the list of to-be-created Worker accounts.
   */
  public previewWorkerAccountImport() {
    if (this.result.errors.length) {
      alert("Virheitä taulukossa. Katso yksityiskohdat alla olevasta taulukosta.");
      return;
    }
  }

  /** Data as tab-separated values (tsv) */
  public get tsv(): string {
    return this._tsv;
  }
  public set tsv(value: string) {
    this._tsv = value;
    this.result = ImportLogic.tsvToJson(this.tsv);
  }
}
