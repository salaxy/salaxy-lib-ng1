
import * as angular from "angular";
import { UiHelpers } from "../../services";

/**
 * Provides user interface for the JSON Formatter UI that shows JSON as collapsible
 * tree view for debugging purposes.
 */
export class JsonFormatterController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers"];

  /**
   * Data that is shown in the component.
   * This property is being watched for changes in each $digest cycle so it needs to be cached
   */
  public json: any;

  /** If true json is compared to compare-to value. */
  public compare: boolean;

  /** The original JavaScript to which to show DIFF (show changes in data). */
  public compareTo: any;

  /** Key of the property that is shown in the component */
  public key: string;

  /** User interface type. Default is inline tree view. Dialog shows a button for a dialog. */
  public type: "default" | "dialog";

  /**
   * Number of children that are currently open.
   * Null sets the default value 1. Set zero for 0.
   */
  public open: number | null;

  /** If true, the node is currently open */
  public isOpen: boolean;

  /** If true, the current value (string) is interpreted as date */
  public isDate: boolean;

  /** If true, the current value (string) is interpreted as URL */
  public isUrl: boolean;

  /** Gets the changes object. Null if no changes or change detection is not enabled. */
  public changes: any;

  /** JavaScript type for the object */
  public jsonType: string;

  /** Keys that existed in compareTo, but are no longer present in json. */
  public deletedKeys: string[];

  /** Component configuration */
  public config = {
    hoverPreviewEnabled: false,
    hoverPreviewArrayCount: 100,
    hoverPreviewFieldCount: 5,
  };

  /**
   * Creates a new instance of the directive.
   */
  constructor(private uiHelpers: UiHelpers) {
    // For dependency injection
  }

  /**
   * Initialize default values.
   */
  public $onInit() {
    if (this.open == null) {
      this.open = 1;
    }
    this.isOpen = !!this.open;
    this.jsonType = this.getType(this.json);
    this.changes = this.getChanges();
    this.deletedKeys = this.getDeletedKeys();
  }

  /** Escapes quotation marks in text */
  public escapeString(str) {
    return str.replace(/"/g, '\\"');
  }

  /** From http://stackoverflow.com/a/332429 */
  public getObjectName(object) {
    if (object === undefined) {
      return "";
    }
    if (object === null) {
      return "Object";
    }
    if (typeof object === "object" && !object.constructor) {
      return "Object";
    }

    // ES6 default gives name to constructor
    if (object.__proto__ !== undefined && object.__proto__.constructor !== undefined && object.__proto__.constructor.name !== undefined) {
      return object.__proto__.constructor.name;
    }

    const funcNameRegex = /function (.{1,})\(/;
    const results = (funcNameRegex).exec((object).constructor.toString());
    if (results && results.length > 1) {
      return results[1];
    } else {
      return "";
    }
  }

  /** Gets the object type as string */
  public getType(object) {
    if (object === null) { return "null"; }
    const type = typeof object;
    if (type === "string") {
      // Add custom type for date
      if ((new Date(object)).toString() !== "Invalid Date") {
        this.isDate = true;
      }
      // Add custom type for URLs
      if (object.indexOf("http") === 0) {
        this.isUrl = true;
      }
    }
    return type;
  }

  /** Gets the value preview text. */
  public getValuePreview(value) {
    const type = this.getType(value);
    if (type === "null" || type === "undefined") { return type; }
    if (type === "string") {
      value = '"' + this.escapeString(value) + '"';
    }
    if (type === "function") {
      // Remove content of the function
      return value.toString()
        .replace(/[\r\n]/g, "")
        .replace(/\{.*\}/, "") + "{…}";
    }
    return value;
  }

  /** Gets a preview of the object. */
  public getPreview(object) {
    let value = "";
    if (angular.isObject(object)) {
      value = this.getObjectName(object);
      if (angular.isArray(object)) {
        value += "[" + object.length + "]";
      }
    } else {
      value = this.getValuePreview(object);
    }
    return value;
  }

  /** Returns true, if json is an array */
  public isArray() {
    return angular.isArray(this.json);
  }

  /** Returns true, if json is an object (including an array). */
  public isObject() {
    return angular.isObject(this.json);
  }

  /** Gets the keys for the object. Only returns keys whgere showKey is true. */
  public getKeys(): string[] {
    if (this.isObject()) {
      return Object.keys(this.json)
        .filter((key) => this.showKey(key))
        .map((key) => {
          if (key === "") { return '""'; }
          return key;
        });
    }
  }

  /** Hides keys like $$hashKey */
  public showKey(key: string) {
    if (!key) {
      // This would be an error, but probably show it to understand errors.
      return true;
    }
    if (key === "$ref") {
      // At least this key starting with $ needs to be shown. Perhaps there are others?
      return true;
    }
    if (key.startsWith("$")) {
      return false;
    }
    return true;
  }

  /** If true, the current JSON has a key */
  public get hasKey() {
    return typeof this.key !== "undefined";
  }

  /** Gets the constructor name */
  public getConstructorName() {
    return this.getObjectName(this.json);
  }

  /** Returns true if the the object is an empty object. */
  public isEmptyObject() {
    return this.getKeys() && !this.getKeys().length &&
      this.isOpen && !this.isArray();
  }

  /** Opens / closes the node */
  public toggleOpen() {
    this.isOpen = !this.isOpen;
  }

  /** Returns the number of open levels for immediate children (this open - 1, but not less than 0). */
  public childrenOpen() {
    if (this.open > 1) {
      return this.open - 1;
    }
    return 0;
  }

  /** Opens the link in a new window. */
  public openLink(isUrl) {
    if (isUrl) {
      window.location.href = this.json;
    }
  }

  /** Parses value (gets a preview) */
  public parseValue(value) {
    return this.getValuePreview(value);
  }

  /** Shows the current JSON as dialog. */
  public showDialog() {
    this.uiHelpers.openEditDialog("salaxy-components/helpers/JsonFormatterDialog.html", this.json, {
      compare: this.compare,
      compareTo: this.compareTo,
      key: this.key,
      open: this.open,
      type: "default",
      title: this.key ? "JSON Preview " + this.key : "JSON Preview",
    });
  }

  /** Returns true if the thumbnail should be shown */
  public showThumbnail() {
    return !!this.config.hoverPreviewEnabled && this.isObject() && !this.isOpen;
  }

  /** Gets the thumbnail value. */
  public getThumbnail() {
    if (this.isArray()) {

      // if array length is greater then 100 it shows "Array[101]"
      if (this.json.length > this.config.hoverPreviewArrayCount) {
        return "Array[" + this.json.length + "]";
      } else {
        // eslint-disable-next-line @typescript-eslint/unbound-method
        return "[" + this.json.map(this.getPreview).join(", ") + "]";
      }
    } else {

      const keys = this.getKeys();

      // the first five keys (like Chrome Developer Tool)
      const narrowKeys = keys.slice(0, this.config.hoverPreviewFieldCount);

      // json value schematic information
      const kvs = narrowKeys.map((key) => key + ":" + this.getPreview(this.json[key]));

      // if keys count greater then 5 then show ellipsis
      const ellipsis = keys.length >= 5 ? "…" : "";

      return "{" + kvs.join(", ") + ellipsis + "}";
    }
  }

  /** Gets the keys that are not in the current, but are in the original. */
  private getDeletedKeys(): string[] {
    if (!this.compare || !angular.isObject(this.compareTo)) {
      return [];
    }
    const currentKeys = this.getKeys();
    return Object.keys(this.compareTo).map((key) => {
      if (key === "") { return '""'; }
      if (currentKeys.find((ck) => ck === key)) {
        return null;
      }
      return key;
    }).filter((key) => key != null);
  }

  /** Gets the changes compared to the original element */
  private getChanges() {
    if (!this.compare || angular.equals(this.json, this.compareTo)) {
      return null;
    }
    let type = "changed";
    let icon = "*";
    const oldValue = this.getValuePreview(this.compareTo);
    if (oldValue === "undefined" || oldValue === "null") {
      type = "new";
      icon = "+";
    } else if (this.jsonType === "null" || this.jsonType === "undefined") {
      type = "deleted";
      icon = "-";
    }
    return {
      type,
      oldValue,
      icon,
    };
  }
}
