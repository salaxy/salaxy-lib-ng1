﻿import * as angular from "angular";

import { NaviService, SalaxySitemapNode, SessionService } from "../../services";

/**
 * Helper controller to generate navigation components and views:
 * Top- and side-menus, paths and controls that show the current title.
 * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
 *
 * NOTE: This is just an optional helper to make creating simple demo sites easier.
 * There is no need to use NaviService, NaviController or components in your custom site!
 * You can use something completely different.
 */
export class NaviController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["NaviService", "SessionService"];

  /**
   * Display mode of the sitemap / navi: "default" or "accordion"
   * default: Shows full two levels of the tree. Typically used as sitemap in the content area.
   * accordion: Shows first level of the navi tree and second level only it is in the current path. Typically used in left menu navigation.
   * 3-levels: Accordion with 3 levels.
   */
  public mode: "default" | "accordion" | "3-levels" = "default";

  /**
   * Creates a new NaviController
   * @param naviService - Service that stores the navigation state.
   * @ignore
   */
  constructor(private naviService: NaviService, private sessionService: SessionService) { }

  /**
   * Implement IController
   */
  public $onInit = () => {
    if (this.mode !== "accordion" && this.mode !== "3-levels") {
      this.mode = "default";
    }
  }

  /**
   * Returns the main sitemap object that is used in rendering the navigation.
   * Set the sitemap by setting the angular constant or object "SITEMAP"
   */
  public get sitemap() {
    return this.naviService.getSitemap();
  }

  /**
   * Retuns the identifier of the currently selected section.
   * If nothing else is selected, it will return the first section, by convention called 'home'.
   */
  public getSectionId() {
    if (!this.naviService.currentSection) {
      return "home";
    }
    return this.naviService.currentSection.id;
  }

  /**
   * Gets the navi URL from a sitemap node.
   * Currently, removes "*" at the end if url ends with "/*", but may be used for additional logic in the future.
   */
  public getUrl(node: SalaxySitemapNode):string {
    if (!node || !node.url) {
      return null;
    }
    if (node.url.endsWith("/*")) {
      return node.url.substr(0, node.url.length - 1);
    }
    return node.url;
  }

  /** Toggles the isNaviOpen property */
  public toggleNavi() {
    this.naviService.isNaviOpen = !this.naviService.isNaviOpen;
  }

  /**
   * True if the navigation (e.g. left sidebar) is open.
   * Typically, this property is used only in narrow (mobile) view, otherwise the sidebar is always shown.
   */
  public get isNaviOpen() { return this.naviService.isNaviOpen; }
  public set isNaviOpen(value: boolean) { this.naviService.isNaviOpen = value; }

  /** Toggles the isSidebarOpen property */
  public toggleSidebar() {
    this.naviService.isSidebarOpen = !this.naviService.isSidebarOpen;
  }

  /**
   * True if the secondary sidebar (e.g. right sidebar) is open.
   */
  public get isSidebarOpen() { return this.naviService.isSidebarOpen; }
  public set isSidebarOpen(value: boolean) { this.naviService.isSidebarOpen = value; }

  /**
   * Gets the current section: The first level node in the sitemap
   * @returns Section filtered by the roles.
   */
  public getSection(): SalaxySitemapNode {
    return this.naviService.getCurrentSection();
  }

  /**
   * Returns true if the node has non-hidden children.
   * @param node Node to check
   */
  public hasChildren(node: SalaxySitemapNode): boolean {
    return !!node?.children.find((x) => !x.hidden);
  }

  /**
   * Filters away the hidden nodes from node array.
   * @param nodes Node array.
   */
  public filterHidden(nodes: SalaxySitemapNode[]): SalaxySitemapNode[] {
    if (!nodes) {
      return [];
    }
    return nodes.filter((x) => !x.hidden);
  }

  /**
   * Returns true if the given node is the currently selected node.
   */
  public isCurrent(siteMapNode: SalaxySitemapNode): boolean {
    return this.naviService.isCurrent(siteMapNode);
  }

  /**
   * Returns true if the given node is the currently selected node.
   */
  public isCurrentSection(section: SalaxySitemapNode): boolean {
    return this.naviService.isCurrentSection(section);
  }

  /**
   * In test environment, returns true if the sitemap node has role "test" and it is visible only because of that role.
   * @param siteMapNode Node in the sitemap.
   */
  public isTest(siteMapNode: SalaxySitemapNode): boolean {
    return siteMapNode.roles?.indexOf("visible-test-only") >= 0;
  }

  /** Returns the page / node title */
  public get title() {
    return this.naviService.title || this.naviService.sectionTitle || "\xa0";
  }

  /** Gets the currently selected node. */
  public get current() {
    return this.naviService.currentNode;
  }

  /** Current path from root to the current node. */
  public get currentPath(): SalaxySitemapNode[] {
    return this.naviService.currentPath;
  }

  /** Returns true if the current window is in an IFrame */
  public get isInIframe(): boolean {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }
}
