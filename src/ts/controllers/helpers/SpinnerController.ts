import * as angular from "angular";

/**
 * Controller rendering a Spinner (please wait) user interfaces.
 */
export class SpinnerController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

    /** If true, the spinner is shown full screen. */
    public fullScreen: boolean;

    /**
     * Heading is the larger text under the spinner.
     * The text is translated.
     */
    public heading: string;

    /**
     * Small text - use pre for line breaks.
     * The text is translated.
     */
    public text: string;

    /**
     * Creates a new SpinnerController
     * @ignore
     */
    constructor() {
        //
     }

    /**
     * Implement IController
     */
    public $onInit = () => {
        // initialization
     }

}
