import * as angular from "angular";

/**
 * Controller for Salaxy switch (a graphic checkbox)
 */
export class SwitchController implements angular.IController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = [];

   /** Switch name */
   public name: string;

   /** Id for the switch */
   public id: string;

   /** The model that is bound to the switch */
   public model: any;

   /** The value of the on text */
   public onText: string;

   /** The value of the off text */
   public offText: string;

   /** If true, the switch will be disabled and its value cannot be changed by clicking/toggling it */
   public disabled: boolean;

    /**
     * Creates a new SwitchController
     * @ignore
     */
    constructor() {
       //
    }

    /**
     * Implement IController
     */
    public $onInit = () => {
        //
        if (this.name && !this.id) {
          this.id = this.name;
      }
    }

    /** Event handler for switch change. */
    public onChange() {
        if (this.disabled === false) {
            this.model.$setViewValue(!this.model.$viewValue);
        }
    }
}
