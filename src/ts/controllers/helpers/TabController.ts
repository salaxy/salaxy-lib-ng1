import * as angular from "angular";

import { TabsController } from "./TabsController";

/**
 * Controller for a single tab pane in the tabs control.
 */
export class TabController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$element"];

  /** Tabs controller */
  public tabsCtrl: TabsController;

  /** Selected tab flag. */
  public selected = false;

  /** Flag to disable tab. */
  public disable = false;

  /** Unique name/id for the tab. */
  public index: any = null;

  /** Text heading for the tab */
  public heading: string = null;

  /** Selection event handler */
  public onSelect: ( args: {
    /** Selection event. */
    $event: angular.IAngularEvent,
  }) => void;

  /** Heading DOM node for the tab. */
  public headingElement = null;

  /** Content DOM node for the tab. */
  public contentElement = null;

  /** Sorting field */
  public sort = 0;

  /**
   * Creates a new TabController.
   * @ignore
   */
  constructor(private $element) {
    // instantiation
  }

  /**
   * Change event handler. Re-sets the one-way bound fields.
   * This is needed, because the parent scope cannot detect the changes without the re-set.
   */
  public $onChanges(changes: any) {
    if (changes.disable) {
      this.disable = changes.disable.currentValue;
    }
    if (changes.index) {
      this.index = changes.index.currentValue;
    }
  }

  /**
   * Registers this tab to the tabs controller.
   * Passes the heading DOM element to be displayed in the header.
   */
  public $onInit() {
    this.sort = this.getPosition(this.$element);
    this.headingElement = this.findTranscludeElement(this.$element, "heading");
    this.contentElement = this.findTranscludeElement(this.$element, "content");
    this.index = this.tabsCtrl.register(this);
  }

  /**
   * De-registers this tab from the tabs controller.
   */
  public $onDestroy() {
    this.tabsCtrl.deregister(this);
  }

  /**
   * Tries to find the element with ng-transclude='heading' attribute.
   * @param el - Element to check.
   */
  private findTranscludeElement(el: any, slot): any {
    if (el.attr("ng-transclude") === slot) {
      return el[0];
    }
    const children = el.children();
    for (let i = 0; i < children.length; i++) {
      const found = this.findTranscludeElement(children.eq(i), slot);
      if (found) {
        return found;
      }
    }
    return null;
  }

  private getPosition(el: any) {
    // find current salaxy-tab
    const salaxyTabNode = this.findParentTag(el, "salaxy-tab");
    if (!salaxyTabNode) {
      return 0;
    }
    // find parent salaxy-tabs
    const salaxyTabsNode = this.findParentTag(salaxyTabNode, "salaxy-tabs");
    if (!salaxyTabsNode) {
      return 0;
    }
    // find all salaxy-tab nodes under salaxy-tabs
    const salaxyTabNodes = [];
    this.findChildren(salaxyTabsNode, salaxyTabNodes, "salaxy-tab");
    // return child index
    for (let i = 0; i < salaxyTabNodes.length; i++) {
      if (salaxyTabNode[0] === salaxyTabNodes[i][0]) {
        return i;
      }
    }
    return 0;
  }

  private findParentTag(el: any, tagName: string): any {
    if (el[0].tagName.toLowerCase() === tagName) {
      return el;
    }

    const parent = el.parent();
    if (parent.length === 1) {
      return this.findParentTag(parent, tagName);
    }
    return null;
  }

  private findChildren(el: any, nodes: any[], tagName: string) {
    if (el[0].tagName.toLowerCase() === tagName) {
      nodes.push(el);
      return;
    }

    const children = el.children();
    for (let i = 0; i < children.length; i++) {
      this.findChildren(children.eq(i), nodes, tagName);
    }
  }

}
