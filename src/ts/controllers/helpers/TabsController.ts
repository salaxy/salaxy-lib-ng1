import * as angular from "angular";

import { TabController } from "./TabController";

/**
 * Controller for the tabs control.
 */
export class TabsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Tabs array. Contains all registered tabs. */
  public tabs: TabController[] = [];

  /**
   * Creates a new TabsController.
   * @ignore
   */
  constructor() {
    // instantiation
  }

  /** Set the values and defaults on init */
  public $onInit() {
    // initialization
  }

  /**
   * Registers a single tab to this controller.
   * @param tab - Tab to register.
   */
  public register(tab: TabController): any {
    this.tabs.push(tab);
    this.setSortOrder(tab);
    this.tabs.sort((t1, t2) => {
      if (t1.sort > t2.sort) {
        return 1;
      }
      if (t1.sort < t2.sort) {
        return -1;
      }
      return 0;
    });

    const index = this.findIndex(tab);

    if (this.tabs.length === 1) {
      tab.selected = true;
    }

    return tab.index || index;
  }

  /**
   * De-registers a single tab from this controller.
   * @param tab - Tab to de-register.
   */
  public deregister(tab: TabController) {
    const index = this.findIndex(tab);

    if (this.tabs[index].index === this.active) {
      const newActiveTabIndex = index === (this.tabs.length - 1) ?
        index - 1 : (index + 1) % this.tabs.length;
      this.select(this.tabs[newActiveTabIndex]);
    }

    this.tabs.splice(index, 1);
  }

  /**
   * Sets the selected tab as selected.
   * @param selectedTab - Selected tab.
   */
  public select(selectedTab: TabController, evt?: angular.IAngularEvent) {
    if (selectedTab && selectedTab.disable === true) {
      return;
    }
    for (const tab of this.tabs) {
      if (tab.selected && tab !== selectedTab) {
        tab.selected = false;
      }
    }
    if (!selectedTab) {
      return;
    }
    selectedTab.selected = true;
    selectedTab.onSelect({
        $event: evt,
      });
  }

  /** Get the index of the active tab. */
  public get active() {
    for (const tab of this.tabs) {
      if (tab.selected) {
        return tab.index;
      }
    }
    return null;
  }

  /** Set the index of the active tab. */
  public set active(value) {
    if (!value) {
      return;
    }
    for (const tab of this.tabs) {
      if (tab.index === value) {
        this.select(tab);
        return;
      }
    }
  }

  private findIndex(tab: any) {
    for (let i = 0; i < this.tabs.length; i++) {
      if (this.tabs[i] === tab) {
        return i;
      }
    }
    return 0;
  }

  private setSortOrder(newTab: any) {
    // latest wins
    let resort = false;
    for (const tab of this.tabs) {
      if (tab !== newTab && tab.sort === newTab.sort) {
        resort = true;
      }
    }
    if (resort) {
      for (const tab of this.tabs) {
        if (tab !== newTab && tab.sort >= newTab.sort) {
          tab.sort++;
        }
      }
    }
  }
}
