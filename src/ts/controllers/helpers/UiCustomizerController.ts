import * as angular from "angular";

import { Configs, Objects } from "@salaxy/core";

import { bootstrapLessVariables } from "../../codegen/bootstrapLessVariables";
import { salaxyLessVariables } from "../../codegen/salaxyLessVariables";

/**
 * Tool that allows building user interfaces for customizing Bootstrap user interface with LESS-variables and linked CSS files.
 */
export class UiCustomizerController  implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /** Current customization */
  public customization: {
    /** LESS variables that make the customization */
    variables: {
      /** variable name */
      name: string,
      /** Variable value */
      value: string,
      /** Optional comment */
      comment: string,
    }[],
  } = {
    variables: [],
  };

  /** LESS variables in standard Bootstrap - grouped list. */
  public bsVariables = this.getGroupedVarArray(bootstrapLessVariables);

  /** LESS variables defined in Salaxy - grouped list. */
  public sxyVariables = this.getGroupedVarArray(salaxyLessVariables);

  /** All LESS variables - without the grouping */
  public allVariables: {
    /** Name of the variable */
    name: string,
    /** Data type: Guessed based on name and default value. */
    type: string,
    /** Default value */
    defaultValue: string,
    /** Description (html) */
    descr: string,
  // eslint-disable-next-line prefer-spread
  }[] =  [].concat.apply([], this.bsVariables.concat(this.sxyVariables).map((x) => x.variables));

  /**
   * Creates a new UiCustomizerController
   * @ignore
   */
  constructor() {
    // For dependency injection.
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    // this.load();
  }

  /** Loads an example style and applies it. */
  public loadExample(example: string) {
    // TODO: Valinta perustuen example-parametriin
    switch (example) {
      case "hearts":
        this.customization = {
          variables: [
            this.getVariable("@salaxy-navi-logo", `"https://cdn.salaxy.com/ng1/customizer/valentines-logo.png"`),
            this.getVariable("@salaxy-fonts-include", `"https://fonts.googleapis.com/css?family=Pacifico&display=swap"`),
            this.getVariable("@font-family-sans-serif", "Pacifico, cursive"),
            this.getVariable("@brand-primary", "pink"),
            this.getVariable("@brand-success", "rgb(248, 53, 86)"),
            this.getVariable("@brand-info", "rgb(173, 129, 137)"),
            this.getVariable("@brand-warning", "rgb(255, 30, 161)"),
            this.getVariable("@brand-danger", "rgb(213,85,82)"),
            this.getVariable("@navi-example-name", `"${example}"`),
          ],
        };
        break;
      case "bootstrap":
          this.customization = {
            variables: [
              this.getVariable("@salaxy-navi-logo", `"https://cdn.salaxy.com/ng1/customizer/bootstrap-logo.png"`),
              this.getVariable("@font-family-sans-serif", `"Helvetica Neue", Helvetica, Arial, sans-serif`),
              this.getVariable("@brand-primary", "darken(#428bca, 6.5%)"),
              this.getVariable("@brand-success", "#5cb85c"),
              this.getVariable("@brand-info", "#5bc0de"),
              this.getVariable("@brand-warning", "#f0ad4e"),
              this.getVariable("@brand-danger", "#d9534f"),
              this.getVariable("@navi-example-name", `"${example}"`),
            ],
          };
          break;
        default:
          this.customization = {
            variables: [
              this.getVariable("@salaxy-navi-logo", `"https://www.palkkaus.fi/img/Elems/palkkaus-www-logo.png"`),
              this.getVariable("@font-family-sans-serif", `Roboto, Helvetica, Arial, sans-serif`),
              this.getVariable("@brand-primary", "rgb(0,159,216)"),
              this.getVariable("@brand-success", "rgb(40,149,72)"),
              this.getVariable("@brand-info", "rgb(145,210,20)"),
              this.getVariable("@brand-warning", "rgb(255,145,30)"),
              this.getVariable("@brand-danger", "rgb(213,85,82)"),
              this.getVariable("@navi-example-name", `"${example}"`),
            ],
          };
          break;
    }
    this.applyLess();
  }

  /** Re-appplies the current less and its customizations. */
  public applyLess() {
    if (this.getLessJs()) {
      this.getLessJs().modifyVars(this.getVariablesObject());
    } else {
      this.setDynamicMode(true);
    }
  }

  /** Returns true if there is a saved customization in the local storage of this browser. */
  public hasStoredCustomization(): boolean {
    return !!(window.localStorage && localStorage.getItem("salaxy-ui-customizer"));
  }

  /** Clears the current customization. */
  public clear() {
    if (!window.localStorage) {
      throw new Error("Localstorage not available.");
    }
    localStorage.removeItem("salaxy-ui-customizer");
  }

  /** Saves the customization (currently to local storage) */
  public save(): void {
    if (!window.localStorage) {
      throw new Error("Localstorage not available.");
    }
    localStorage.setItem("salaxy-ui-customizer", JSON.stringify(this.customization));
  }

  /** Loads the customization (currently from local storage) */
  public load(): void {
    if (window.localStorage) {
      const textInStorage = localStorage.getItem("salaxy-ui-customizer");
      if (textInStorage) {
        this.customization = JSON.parse(textInStorage);
        this.applyLess();
        return;
      }
    }
  }

  /**
   * Gets the variables object from customization.variables.
   */
  public getVariablesObject() {
    const sourceArray = this.customization.variables;
    const rv = {};
    for (let i = 0; i < sourceArray.length; ++i) {
      const value = sourceArray[i].value;
      if (value) {
        rv[sourceArray[i].name] = value;
      }
    }
    return rv;
  }

  /**
   * Sets the dynamic LESS rendering mode ON / OFF.
   * Dynamic rendering mode means that styles are rendered dynamically from LESS files and variables instead of pre-rendered CSS files.
   * @param isDynamic If true, sets the dynamic mode ON if it is not already.
   * If false, sets the dynamic mode off (using pre-compiled CSS files). Currently, this is done by calling Reload().
   */
  public setDynamicMode(isDynamic: boolean) {
    if (isDynamic) {
      if (!this.getLessJs()) {
        // Reloading as new
        Configs.global.less = {
          modifyVars: this.getVariablesObject(),
        };
        this.addLink("less", "/less-preview/_bootstrap.less");
        this.addLink("less", "/less-preview/_salaxy-lib-ng1-bootstrap.less");
        this.addScript("/less-preview/less-dist/less.min.js");
      }
    } else {
      window.location.reload();
    }
  }

  /**
   * Gets the type of the variable if available based on variable name.
   * Currently, this is based on guessing on name and default value.
   * @param name Name of the variable.
   */
  public getType(name: string) {
    const variable = this.allVariables.find((x) => x.name === name);
    return variable ? this.guessType(variable) : null;
  }

  /** Adds a new row to the variables. */
  public addRow() {
    this.customization.variables.push({
      name: "",
      value: "",
      comment: null,
    });
  }

  /** Removes a varibale from the list. */
  public removeRow(row) {
    const ix = this.customization.variables.indexOf(row);
    if (ix >= 0) {
      this.customization.variables.splice(ix, 1);
    }
  }

  /**
   * Gets the LessJs object from window.less (https://github.com/less/less.js/blob/master/dist/less.js).
   * Only returns it, if it is an active loaded object (as opposed to just settings).
   * Otherwise returns null.
   */
  private getLessJs(): {
    /**
     * Enables run-time modification of Less variables. When called with new values, the Less file is recompiled without reloading.
     * With this function, it's possible to alter variables and re-render CSS without reloading less-files
     */
    modifyVars: (variables: { [key: string]: string; }) => void,
    /** Watch mode */
    watch: () => true,
    /** Remove watch mode */
    unwatch: () => false,
    /** Synchronously get all <link> tags with the 'rel' attribute set to "stylesheet/less". */
    registerStylesheetsImmediately: () => void,
    /** Asynchronously get all <link> tags with the 'rel' attribute set to "stylesheet/less", returning a Promise. */
    registerStylesheets: () => Promise<void>,

    /**
     * Does the refresh with options
     * Note that modifyVars() calls this method with (true, variables, false). This is typically what you want to use.
     * @param reload If true, re-renders the CSS from LESS
     * @param modifyVars Varibles to apply.
     * @param clearFileCache If true, clear the file cache.
     */
    refresh: (reload: boolean, modifyVars: { [key: string]: string; }, clearFileCache: boolean) => void,

  } {
    const potentialLess = Configs.global.less;
    if (!potentialLess
      || !Objects.has(potentialLess, "modifyVars")
      || typeof potentialLess.modifyVars !== "function") {
      return null;
    }
    return potentialLess;
  }

  private getVariable(name, value, comment: string = null) {
    return {
      name,
      value,
      comment,
    };
  }

  /** Harmonizes a LESS variables array for our purposes */
  private getGroupedVarArray(generatedArray: any[]): {
    /** Group heading */
    heading: string,
    /** Group description (html) */
    descr: string,
    /** Variables in the group */
    variables: {
      /** Name of the variable */
      name: string,
      /** Data type: Guessed based on name and default value. */
      type: string,
      /** Default value */
      defaultValue: string,
      /** Description (html) */
      descr: string,
    }[],
  }[] {
    return generatedArray
      .filter((x) => x.customizable)
      .map((x) => {
        let descr = x.docstring ? x.docstring.html : null;
        if (!descr) {
          descr = x.subsections.filter((sub) => !!sub.heading).map((sub) => sub.heading).join(", ");
        }
        // eslint-disable-next-line prefer-spread
        const allVariables = [].concat.apply([], x.subsections.map((x) => x.variables));
        return {
          heading: x.heading,
          descr,
          variables: allVariables.map((v) => ({
            name: v.name,
            type: this.guessType(v),
            defaultValue: v.defaultValue,
            descr: v.docstring ? v.docstring.html : null,
          })),
        };
      });
  }

  private guessType(variable: {
    /** Name of the variable */
    name: string,
    /** Data type: Guessed based on name and default value. */
    type: string,
    /** Default value */
    defaultValue: string,
    /** Description (html) */
    descr: string,
  }): "size" | "color" | "font" | null {
    const d = variable.defaultValue;
    const n = variable.name;
    if (d.endsWith("px") || d.includes("@font-size-")) {
      return "size";
    }
    if (n.endsWith("-color") || n.endsWith("-bg") || n.endsWith("-text") || n.endsWith("-border")) {
      return "color";
    }
    if (d.startsWith("#") || d.includes("lighten(") || d.includes("darken(") || d.includes("@brand-") || d.includes("white")  || d.includes("@gray-") ) {
      return "color";
    }
    if (variable.name.includes("font-family")) {
      return "font";
    }
    return null;
  }

  private addLink(type: "css" | "less" | "icon", href) {
    const link = document.createElement("link");
    switch (type) {
      case "css":
        link.rel = "stylesheet";
        link.type = "text/css";
        break;
      case "less":
        link.rel = "stylesheet/less";
        link.type = "text/css";
        break;
      case "icon":
        link.rel = "icon";
        link.type = "image/x-icon";
        break;
    }
    link.href = href;
    document.getElementsByTagName("head")[0].appendChild(link);
  }

  private addScript(src) {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    document.getElementsByTagName("head")[0].appendChild(script);
  }

}
