import { CalendarUiEvent } from "./CalendarUiEvent";

/** Single day in the calendar */
export interface CalendarDay {

  /** Date of month 1-31 */
  day: number,
  /** ISO date for the day */
  date: string,
  /** Weekday 1-7 (mon-sun) */
  weekday: number,
  /** Two-letter language versioned weekday text (ma,ti,ke,to,pe,la,su / mo,tu,we,th,fr,sa,su) */
  weekdayText: string,
  /** Special days: Today overrides holiday, if day is both holiday and today. */
  dayType: "holiday" | "today" | "normal",

  /** Events for the day: Events that start on the day or periods with end date (and no icon) that intersect the day. */
  events: CalendarUiEvent[][],

  /** Periods (repsesented as colored div) that intersect the given day. */
  periods: CalendarUiEvent[][],

  /** Single days (represented as icon) that occur on the given day */
  singleDays: CalendarUiEvent[][],
}
