import { CalendarDay } from "./CalendarDay";

/** Month in a calendar. */
export interface CalendarMonth {
  /** Month as 1-12 */
  month: number,
  /** Year for the month */
  year: number,
  /** Short text (3-6 letters) of the month (tammi,helmi,maalis,...) */
  title: string,
  /** Weekday of the first day 1-7 (mon-sun) */
  firstDayDayOfWeek,
  /** Number of days in the month */
  daysInMonth,
  /** Days in the month */
  days: CalendarDay[],
}
