import { Avatar } from "@salaxy/core";
import { CalendarUiEvent } from "./CalendarUiEvent";

/** Represents a single series shown in the calendar. */
export interface CalendarSeries {

  /** String key for the series. */
  key: string,

  /** Title that is shown for the series. */
  title?: string,

  /** Longer text together below the text */
  description?: string,

  /** Potential avatar visualization for the series. */
  avatar?: Avatar,

  /** Events: The data for the series */
  events: CalendarUiEvent[];

  /** Case specific data for dialog editing etc. */
  data?: any;

}
