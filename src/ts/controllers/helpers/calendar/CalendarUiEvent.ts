/**
 * An event in the calendar with User Interface data (icon, color etc.).
 * Compatible with CalendarVEvent for base data (start, end, summary, description).
 * If the end day is NOT specified OR if icon IS specified, an icon is displayed.
 * If the end day IS specified AND icon is NOT specified, the event is shown as period,
 * potentially spanning over several days (may also be just one day).
 */
 export interface CalendarUiEvent {

  /** Start date for an event. Required for any event. */
  start: string,

  /**
   * End date for period. NOTE: For single-day event markers (icon), set this null.
   * For single-day periods (period color, no icon), set this the same as start day.
   */
  end?: string,

  /** Summary title for the event. */
  summary?: string,

  /** Description for the period. */
  description?: string,

  /**
   * For single-day events (end day null), you can specify a Font-awesome version 4.7 icon (https://fontawesome.com/v4.7.0/icons/).
   * Default is "fa-circle".
   */
  icon?: string,

  /**
   * Specify a CSS class to the icon or period background.
   * For single day icons, good choices are: text-muted, text-primary, text-success, text-info, text-warning, text-danger.
   * For periods, good choices are: salaxy-cal-event-primary, [...]-success, [...]-info, [...]-warning, [...]-danger etc.
   */
  cssClass?: string,

  /** Case specific data for dialog editing etc. */
  data?: any;
}
