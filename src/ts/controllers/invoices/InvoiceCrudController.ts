﻿import * as angular from "angular";

import { Ajax, Configs, Dates, Invoice, InvoiceListItem, Invoices, InvoiceStatus, PaymentChannel } from "@salaxy/core";

import { InvoicesService, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { InputEnumOption } from "../form-controls";

/**
 * Provides read access for authenticated user to access his/her own invoices
 * and write access to create new salary and related payments based on calculations.
 */
export class InvoiceCrudController extends ApiCrudObjectController<Invoice> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Invoices",
    "UiHelpers",
    "$location",
    "$routeParams",
    "InvoicesService",
    "AjaxNg1",
  ];

  /** For list component, defines the layout / functionality of the list. */
  public mode: "default" | "panel";

  /** Data reader for selected items. */
  public selectionDataReader = {
    /** Function to read data into given array */
    read: (arr: Invoice[], message: string): Promise<void> => { // eslint-disable-line @typescript-eslint/no-unused-vars
      const items = this.odataController.selectedItems;
      let counter = 0;
      const next = (): Promise<void> => {
        if (counter === items.length) {
          return Promise.resolve();
        }
        const item = items[counter++];
        return this.api.getSingle(item.id).then((data) => {
          /*
          const wfEvent = {
            type: "PartnerMessageClosed",
            ui: "success" as any,
            message,
          };
          return this.filesApi.saveWorkflowEvent(item, wfEvent).then(() => {
              this.setWorkflowEvent(item, wfEvent);
          */
          if (data) {
            arr.push(data);
          }
          return next();
        });
        /* });*/
      };
      return next().then(() => {
        return Promise.resolve();
      });
    },
    /** Optional label for data export */
    exportLabel: "Luo aineisto valituista",
    /** Indicates if the export is not possible */
    disabled: () => !this.odataController.selectedItems || this.odataController.selectedItems.length === 0,
  };

  /** Status filters for lists: Groups statuses to logical groups. */
  public statusFilters: InputEnumOption[] = [
    {
      value: "all",
      text: "Kaikki maksut",
      ui: "",
    }, {
      value: "waitingConfirmation",
      text: "Maksamista odottavat",
      ui: "status eq 'unread' or status eq 'read' or status eq 'waitingConfirmation'",
    }, {
      value: "paid",
      text: "Maksetut ja perutut",
      ui: "status eq 'paid' or status eq 'paymentStarted' or status eq 'canceled'",
    }, {
      value: "forecast",
      text: "Arviot ja esikatselut",
      ui: "status eq 'forecast' or status eq 'preview'",
    },
  ];

  constructor(
    private invoicesApi: Invoices,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private invoicesService: InvoicesService,
    private ajax: Ajax,
  ) { // Dependency injection
    super(invoicesApi, uiHelpers, $location, $routeParams);
  }

  /** Gets the default channel for the current account. */
  public get defaultChannel(): PaymentChannel {
    return this.invoicesService.defaultChannel;
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "invoices",
      detailsUrl: this.detailsUrl || "/invoices/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/InvoicesPanel.html",
      oDataOptions: {},
    };
  }

  /** Gets the type of the due date vs. the current date and status for display purposes. */
  public getDueDateType(listItem: InvoiceListItem): "default" | "estimation" | "canceled" | "overdue" | "future" {
    if (listItem.entityType === "unemployment") {
      return "estimation";
    }
    switch (listItem.status) {
      case InvoiceStatus.Canceled:
        return "canceled";
      case InvoiceStatus.Forecast:
      case InvoiceStatus.Preview:
        if (listItem.endAt > Dates.getToday()) {
          return "future";
        }
        return "default";
      case InvoiceStatus.Undefined:
      case InvoiceStatus.Unread:
        if (listItem.endAt < Dates.getToday()) {
          return "overdue";
        }
        return "default";
      case InvoiceStatus.Read:
      case InvoiceStatus.WaitingConfirmation:
      case InvoiceStatus.WaitingPalkkaus:
      case InvoiceStatus.Paid:
      case InvoiceStatus.PaymentStarted:
        return "default";
    }
    return "default";
  }

  /** For list components, gets the OData query options. */
  public getListOptions() {
    return this.uiHelpers.cache(this, "listOptions", () => {
      if (this.mode === "panel") {
        return {
          $filter: "",
          $top: 5,
          $orderby: "endAt desc",
        };
      } else {
        // Add logic and e.g. if needed
        return {
          $orderby: "endAt desc",
        };
      }
    });
  }

  /** Returns content url for the invoice. */
  public getContentUrl(id: string, inline = false): string {
    return this.invoicesApi.getContentUrl(id, inline);
  }

  /**
   * Request the status update for the invoice.
   * @param status Status data. If not set, uses the requestedStatus property of the controller.
   */
  public requestStatusUpdate(status: InvoiceStatus = null): any {
    status = status || this.requestedStatus;

    if ( status === InvoiceStatus.Paid && (
      this.current.header.channel === PaymentChannel.Test ||
      this.current.header.channel === PaymentChannel.Procountor ||
      this.current.header.channel === PaymentChannel.VismaNetvisor)) {

      const loader = this.uiHelpers.showLoading("Maksuikkuna avattu...");
      let url = "";
      switch ( this.current.header.channel) {
        case PaymentChannel.Test:
          url =
          Configs.current.isTestData ?
          "https://test-integrations.salaxy.com/test#/dialogs/invoice" :
          "https://integrations.salaxy.com/test#/dialogs/invoice";
          break;
        case PaymentChannel.Procountor:
          url =
          Configs.current.isTestData ?
          "https://test-integrations.salaxy.com/procountor/invoice.html" :
          "https://integrations.salaxy.com/procountor/invoice.html";
          break;
        case PaymentChannel.VismaNetvisor:
         url =
         Configs.current.isTestData ?
          "https://test-integrations.salaxy.com/vismanetvisor/invoice.html" :
          "https://integrations.salaxy.com/vismanetvisor/invoice.html";
        break;
      }
      url += "?invoiceIds=" + this.currentId + "&token=" + this.ajax.getCurrentToken();
      return this.uiHelpers.showExternalDialog("invoiceCreation", url, {})
        .then(() => {
          loader.dismiss();
          return this.setStatus(this.api.getSingle(this.current.id)).then((invoice) => {
            if (invoice) {
              this.setCurrent(invoice);
            }
            return invoice;
          });
        });
    } else {

      return this.setStatus(this.invoicesApi.requestStatusUpdate(this.currentId, status)
        .then((invoice) => {
          if (invoice) {
            this.setCurrent(invoice);
          }
          return invoice;
        }));
    }
  }

  /** Requested status for binding */
  public get requestedStatus(): InvoiceStatus {
    if (this.current) {
      return (this.current.header as any).requestedStatus || this.current.header.status;
    }
    return null;
  }

  /** Requested status for binding */
  public set requestedStatus(value: InvoiceStatus) {
    (this.current.header as any).requestedStatus = value;
  }

  /**
   * Delete invoice and related business objects (calculations and payrolls)
   * @param item  - Invoice to delete.
   * @param confirmMessage - Optional message for confirm popup
   */
  public deleteWithBusinessObjects(item?: Invoice, confirmMessage?: string): Promise<boolean> {
    item = item || this.current;
    return this.uiHelpers.showConfirm(confirmMessage || "Haluatko varmasti poistaa tämän laskun ja siihen liittyvät laskelmat ja palkkalistat?")
      .then((result: boolean) => {
        if (result) {
          return this.ajax.remove(`/accounts/invoice?ids=${item.id}`).then(() => {
            if (this.current === item) {
              this.setCurrent(null);
            }
            if (this.odataController) {
              this.odataController.reload();
            } else {
              this.$location.url(this.getDefaults().listUrl);
            }
            return true;
          });
        } else {
          return Promise.resolve(false);
        }
      });
  }

  /**
   * Shows a dialog for selecting calculations for Create invoices.
   * If called from an list component (OData), reloads the list.
   * @param channel The payment channel ID.
   * @param category Either "paid": Read-only => Recreate invoices ... or "draft": Editable => Pay calulation.
   * @param $odataCtrl Optional controller to reload after the operation finishes.
   * @param $odataCtrl.reload Reload method
   */
  public showCreateInvoices(channel: string, category: "paid" | "draft" = "draft", $odataCtrl: {
    reload: () => any,
  } = null,
  ): void {
    this.uiHelpers.openSelectCalcs(category).then((result) => {
      if (result.action === "ok" && result.item.length > 0) {
        const loader = this.uiHelpers.showLoading("Luodaan laskuja...");
        const ids: string[] = result.item.map((x) => x.id);
        return this.invoicesApi.createInvoicesById(channel, ...ids).then(() => {
          if ($odataCtrl) {
            $odataCtrl.reload();
          }
          loader.dismiss();
        });
      }
    });
  }

  /**
   * Data reader for one item.
   * TODO: Should add interface for the reader.
   */
  public getItemDataReader = (item: {
    /** List item id */
    id: string,
    /** List item owner */
    owner: string,
  }) => {
    return {
      /** Function to read data into given array */
      read: (arr: Invoice[], message: string): Promise<void> => { // eslint-disable-line @typescript-eslint/no-unused-vars
        (item as any).isReading = true;
        return this.api.getSingle(item.id).then((data) => {
          /*
          const wfEvent = {
            type: "PartnerMessageClosed",
            ui: "success" as any,
            message,
          };
          return this.filesApi.saveWorkflowEvent(item, wfEvent).then(() => {
            this.setWorkflowEvent(item as any, wfEvent);
          */
          if (data) {
            arr.push(data);
          }
          (item as any).isReading = false;
          return Promise.resolve();
          /* });*/
        });
      },
      /** Optional label for data export */
      exportLabel: null,
      /** Indicates if the export is not possible */
      disabled: () => false,
    };
  }

  /** Unselect all */
  public unselectAll() {
    if (this.odataController.selectedItems) {
      this.odataController.selectedItems.splice(0, this.odataController.selectedItems.length);
    }
  }

  /** Select all */
  public selectAll() {
    if (this.odataController.selectedItems && this.odataController.items) {
      this.odataController.selectedItems.splice(0, this.odataController.selectedItems.length);
      this.odataController.selectedItems.push(...this.odataController.items);
    }
  }
}
