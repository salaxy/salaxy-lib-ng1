import * as angular from "angular";
import JSZip from "jszip";
import { saveAs } from "file-saver";

import { Ajax, Dates, Invoice, Texts } from "@salaxy/core";

import { UiHelpers } from "../../services";

/** File type id */
enum FileTypeId {
  SepaFile = "sepaFile",
  SepaCopy = "sepaCopy",
  PdfFile = "pdfFile",
}

/**
 * Provides functionality to export invoices in various formats.
 */
export class InvoiceToolsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AjaxNg1",
    "$http",
    "UiHelpers",
  ];

  /** Export format */
  public fileTypeId: string = FileTypeId.PdfFile;

  /** Supported export formats */
  public fileTypes: {
    /** File type id */
    id: string,
    /** File type label */
    label: string,
    /** Workflow message after export */
    workflowMessage,
    /** Icon */
    icon: string,
  }[] = [
      {
        id: FileTypeId.PdfFile,
        label: "Pdf-tiedosto",
        workflowMessage: "Pdf-tiedosto",
        icon: "fa-file-pdf-o",
      },
      {
        id: FileTypeId.SepaFile,
        label: "S€PA-tiedosto",
        workflowMessage: "Sepa-tiedosto",
        icon: "fa-file-code-o",
      },
      {
        id: FileTypeId.SepaCopy,
        label: "S€PA copy-paste",
        workflowMessage: "Sepa copy-paste",
        icon: "fa-clipboard",
      },
    ];

  /**
   * Object with read function to call for retrieving the invoice data.
   * Function has the following argument:  arr, which is an array of invoices into which the result will be appended.
   */
  public reader: {
    /** Function for reading invoices into given array. */
    read: (
      /** Array for invoices */
      arr: Invoice[],
      /** Message for workflow (loading) */
      message: string,
    ) => Promise<void>,
    /** Optional label for data export */
    exportLabel?: string,
    /** Indicates if the export is not possible */
    disabled: () => boolean,
  };

  constructor(
    private ajax: Ajax,
    private $http: angular.IHttpService,
    private uiHelpers: UiHelpers,
  ) {
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {
    // Nothing here
  }

  /** Set visible filetypes. Sets the first file type as default. */
  public setFileTypes(fileTypes: string[]) {
    if (fileTypes && fileTypes.length > 0) {
      const fts = [];
      fileTypes.forEach((x) => {
        const ft = this.fileTypes.find((f) => f.id === x);
        if (ft) {
          fts.push(ft);
        }
      });
      this.fileTypes.splice(0);
      this.fileTypes.push(...fts);
      this.fileTypeId = fileTypes[0];
    }
  }

  /** Exports data in defined format. */
  public export(
    reader: {
      /** Function for reading data into given array. */
      read: (
        /** Array for invoices */
        arr: Invoice[],
        /** Workflow loading message */
        message: string,
      ) => Promise<void>,
      /** Optional label for data export */
      exportLabel?: string,
      /** Indicates if the export is not possible */
      disabled: () => boolean,
    } = null,
    /** If true, does not show loading indicator. Default is false. */
    hideLoading = false,
  ): Promise<void> {
    if (!reader) {
      reader = this.reader;
    }
    if (!reader) {
      console.error("No reader for invoices.");
      return Promise.resolve();
    }
    const loading = hideLoading ? { dismiss: () => null } : this.uiHelpers.showLoading("Odota...");

    const arr: Invoice[] = [];
    return reader.read(arr, this.getWorkflowMessage()).then(() => {
      if (arr.length === 0) {
        loading.dismiss();
        return Promise.resolve();
      }
      switch (this.fileTypeId) {
        case FileTypeId.PdfFile:
          return this.getPdfData(arr).then((pdfArr) => {
            if (pdfArr.length === 1) {
              saveAs(pdfArr[0], `${this.getFileName(arr[0])}.pdf`);
              loading.dismiss();
              return;
            } else {
              let counter = 0;
              const zip = new JSZip();
              const next = () => {
                if (counter === pdfArr.length) {
                  return;
                }
                zip.file(`${this.getFileName(arr[counter])}.pdf`, pdfArr[counter]);
                counter++;
                next();
              };

              next();
              return zip.generateAsync({
                type: "blob",
                mimeType:
                  "application/zip",
              }).then((blob) => {
                saveAs(blob, `${this.getZipFileName(arr)}.zip`);
                loading.dismiss();
                return;
              });
            }
          });
          break;
        case FileTypeId.SepaFile:
          return this.getSepaData(arr).then((sepaData) => {
            saveAs(new Blob([sepaData], { type: "text/xml;charset=utf-8" }), arr.length === 1 ? `${this.getFileName(arr[0])}.xml` : `${this.getZipFileName(arr)}.xml`);
            loading.dismiss();
            return;
          });
          break;
        case FileTypeId.SepaCopy:
          return this.getSepaData(arr).then((sepaData) => {
            (navigator as any).clipboard.writeText(sepaData);
            loading.dismiss();
            return;
          });
      }
    });
  }

  /**
   * Returns the file type definition
   * @param fileTypeId File type id.
   */
  public getFileType(fileTypeId: string) {
    return this.fileTypes.find((x) => x.id === fileTypeId);
  }

  private getFileName(invoice: Invoice) {
    return Texts.escapeFileName(`Maksu_${invoice.payer.avatar.displayName}_${invoice.id}`);
  }

  private getZipFileName(invoices: Invoice[]) {
    return Texts.escapeFileName(`Maksut_${invoices[0].payer.avatar.displayName}_${Dates.getMoment(new Date()).format("YYYYMMDD")}`);
  }

  private getWorkflowMessage() {
    const fileType = this.getFileType(this.fileTypeId);
    if (fileType) {
      return fileType.workflowMessage;
    }
    return null;
  }

  private getPdfData(invoices: Invoice[]): Promise<Blob[]> {
    const pdfArr = [];
    let counter = 0;
    const next = (): Promise<void> => {
      if (counter === invoices.length) {
        return Promise.resolve();
      }
      const invoice = invoices[counter++];
      return this.download("/v03-rc/api/invoices/content", invoice, "blob").then((data) => {
        pdfArr.push(data);
        return next();
      });
    };

    return next().then(() => {
      return pdfArr;
    });
  }

  private getSepaData(invoices: Invoice[], payerIbanNumber: string = null): Promise<string> {
    const method = `/v03-rc/api/invoices/sepa${payerIbanNumber ? "?payerIbanNumber=" + encodeURIComponent(payerIbanNumber) : ""}`;
    return this.download(method, invoices, "text");
  }
  private download(method: string, data: any, responseType: string): Promise<any> {

    const request: any = {}; // angular.IRequestConfig

    const token: string = this.ajax.getCurrentToken();
    if (token) {
      request.headers = { Authorization: "Bearer " + token };
    }

    request.url = this.ajax.getServerAddress() + method;
    request.method = "POST";
    request.data = data;
    request.responseType = responseType;
    request.withCredentials = (token) ? false : this.ajax.useCredentials;

    return (this.$http(request).then(
      (response) => response.data,
      (error: any) => {
        return null;
      }) as any
    );
  }

}
