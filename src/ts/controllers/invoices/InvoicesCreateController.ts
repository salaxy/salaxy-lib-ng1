import * as angular from "angular";
import {
  Ajax, Calculation, Calculations, Configs,
  Invoice, Invoices, InvoicesLogic, InvoicePreview, InvoiceStatus, InvoiceType,
  PaymentChannel, PayrollDetails, Payrolls, PayrollStatus, SalaryDateLogic,
} from "@salaxy/core";

import { AlertService, InvoicesService, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { CalculationCrudController, PayrollCrudController } from "../calc";
import { InputEnumOption } from "../form-controls/InputEnumOption";
import { } from "../../components";

/**
 * Controller for the new Payment button and other functionality:
 * Creating (recreating) Invoices based on Calculations and Payrolls.
 */
export class InvoicesCreateController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Invoices", "InvoicesService", "Calculations", "Payrolls", "UiHelpers", "$location", "AlertService", "$window", "AjaxNg1"];

  /**
   * Defines the binding mode of the component.
   * Currently, only "model" is supported (Calculation or PayrollDetails),
   * but we might support e.g. id in the future.
   */
  public bindingMode: "model" | "null";

  /**
   * Collection of invoices if the channel and model have been set.
   * Null if the collection is being fetched.
   */
  public invoices: InvoicePreview[] = [];

  private _current: Calculation | PayrollDetails;

  private _model: any;

  private _primaryCalcNetInvoicePreview: InvoicePreview = null;

  private _primaryCalcTaxInvoicePreview: InvoicePreview = null;

  constructor(
    private invoicesApi: Invoices,
    private invoicesService: InvoicesService,
    private calculationsApi: Calculations,
    private payrollsApi: Payrolls,
    protected uiHelpers: UiHelpers,
    private $location: angular.ILocationService,
    private alertSrv: AlertService,
    private $window: angular.IWindowService,
    private ajax: Ajax
  ) {
  }

  /**
   * Implement IController by providing onInit method.
   * We currently do nothing here, but if you override this function,
   * you should call this method in base class for future compatibility.
   */
  public $onInit() {
    this.bindingMode = this.bindingMode || "null";
  }

  /**
   * Set the Current selected item (the model).
   * Currently, only Calculation or PayrollDetails is supported,
   * but we will probably support controller and url in the future.
   */
  public set model(value: Calculation | PayrollDetails | "url" | string | ApiCrudObjectController<Calculation> | ApiCrudObjectController<PayrollDetails>) {
    this._model = value;
    if (InvoicesLogic.isCalculation(value) || InvoicesLogic.isPayroll(value)) {
      this._current = value;
      this.bindingMode = "model";
    } else {
      throw new Error("Not implemented: Only Calculation or PayrollDetails is currently supported.");
    }
    this.reload();
  }
  /** Gets the Current selected item (the model). */
  public get model(): Calculation | PayrollDetails | "url" | string | ApiCrudObjectController<Calculation> | ApiCrudObjectController<PayrollDetails> {
    return this._model;
  }

  /** Changes the channel */
  public set channel(value: PaymentChannel) {
    if (InvoicesLogic.isCalculation(this.current)) {
      this.current.info.paymentChannel = value;
    } else {
      this.current.input.paymentChannel = value;
    }
    this.reload();
  }
  /** The current payment channel for user interface purposes. */
  public get channel(): PaymentChannel {
    if (!this.current) {
      return null;
    }
    const channel = InvoicesLogic.isCalculation(this.current)
      ? this.current.info.paymentChannel
      : this.current.input.paymentChannel;
    return channel || this.invoicesService.defaultChannel;
  }

  /** Gets the enabled channels. */
  public get allChannels() {
    return this.invoicesService.channelEnumOptions;
  }

  /** Gets the text for the Primary submit button. Null for hiding the button. */
  public getOkBtn(): string {
    if (!this.isValid) {
      return null;
    }
    switch (this.paymentStatus) {

      case "paid":
      case "exceptions":
      case "canceled":
      case "unknown":
        return null;
      case "preview":
        if(this.channel === PaymentChannel.PalkkausManual || this.channel === PaymentChannel.FinagoSolo || this.channel === PaymentChannel.AccountorGo){
          return "Luo maksut";
        }
        return "Lähetä maksuun";
      case "inProgress":
        if (this.channel === PaymentChannel.Procountor || this.channel === PaymentChannel.VismaNetvisor || this.channel === PaymentChannel.Test) {
          return "Tarkista maksun tila";
        }
        return null;
      case "invoicesCreated":
      default:
        if (!this.channel || this.channel === PaymentChannel.Undefined) {
          return null;
        }
        if (this.isZeroPayment()) {
          return "Merkitse maksetuksi";
        }
        return "Lähetä maksuun";
    }
  }

  /** Gets the title for the payment method */
  public get title(): string {
    if (!this.isValid) {
      return "Laskelmaa ei voi maksaa";
    }
    switch (this.paymentStatus) {
      case "paid":
        return "Palkkalaskelma(t) on maksettu";
      case "canceled":
        return "Peruttuja maksuja";
      case "exceptions":
        return "Maksuissa on tapahtunut virheitä";
      case "unknown":
        return "Ladataan maksuja...";
      case "inProgress":
        return "Maksu aloitettu";
      case "invoicesCreated":
      case "preview":
      default:
        break;
    }
    switch (this.channel) {
      case PaymentChannel.Undefined:
        return "Valitse maksumenetelmä";
      case PaymentChannel.AccountorGo:
        return "Palkkojen maksu Accountor Go -palvelussa";
      case PaymentChannel.FinagoSolo:
        return "Palkkojen maksu Procountor Solossa";
      case PaymentChannel.Kevytyrittaja:
        return "Palkkojen maksu Talenom Kevytyrittäjä -palvelussa";
      case PaymentChannel.PalkkausManual:
        return "Maksujen luonti SEPA- tai viitesiirtoaineistoja varten";
      case PaymentChannel.PalkkausWS:
        return "Maksujen luonti suoraan pankkiin";
      case PaymentChannel.PalkkausPersonal:
        return "Maksa laskut verkkopankissasi";
      case PaymentChannel.Test:
        return "Testimaksaminen";
      case PaymentChannel.ZeroPayment:
        return "Nollamaksu";
      case PaymentChannel.TalenomOnline:
        return "Palkkojen maksu Talenom Online -palvelussa";
      case PaymentChannel.Procountor:
        return "Palkkojen maksu Procountor -palvelussa";
      case PaymentChannel.VismaNetvisor:
        return "Palkkojen maksu Visma Netvisorissa";
      case PaymentChannel.PalkkausCfaFinvoice:
        return "Palkkojen maksu verkkolaskuna (yksi maksu asiakasvaratilille)";
      case PaymentChannel.PalkkausCfaPaytrail:
        return "Palkkojen maksu Paytrail verkkomaksuna (yksi maksu asiakasvaratilille)";
      case PaymentChannel.PalkkausCfaReference:
        return "Palkkojen maksu tilisiirtona viitenumerolla (yksi maksu asiakasvaratilille)";
      case PaymentChannel.PalkkausCfaTest:
        return "Asiakasvaratilin testimaksu";
      case PaymentChannel.HolviCfa:
        return "Holvi maksu";
      case PaymentChannel.TalenomCfa:
        return "Talenom verkkolasku"
      default:
        return "Maksumenetelmä ei tuettu";
    }
  }

  /** Gets the Current selected item. */
  public get current(): Calculation | PayrollDetails {
    return this._current;
  }

  /**
   * Returns true if the invoices have been created => the payment process has been started.
   * It may have been completed (paid), canceled or rejected.
   */
  public get isStarted(): boolean {
    if (InvoicesLogic.isPayroll(this.current) && this.current.info.status !== PayrollStatus.Draft) {
      return true;
    } else if (InvoicesLogic.isCalculation(this.current) && this.current.isReadOnly) {
      return true;
    }
    return false;
  }

  /**
   * Gets the payment status of the invoices.
   *
   * - unknown: The invoices have not been loaded from the server yet.
   * - preview: The invoices have not yet been created
   * - invoicesCreated: Invoices have been created (Unread), but payment channel has not yet read them (send to channel).
   * - inProgress: Any of the invoices is in progress (WaitingPalkkaus, Read, WaitingConfirmation, PaymentStarted)
   * OR some invoices have been paid, some not (default state).
   * - canceled: One of the invoices is canceled.
   * - exceptions: Any of the invoices have exceptions (Undefined, Error)
   * - paid: All the invoices have been paid
   *
   * This is ready only after the invoices have been loaded by Preview (may be already created invoices).
   */
  public get paymentStatus(): "unknown" | "preview" | "invoicesCreated" | "inProgress" | "exceptions" | "canceled" | "paid" {
    if (!this.invoices || this.invoices.length === 0) {
      return "unknown";
    }
    if (this.invoices.every((x) => !x.exists)) {
      return "preview";
    }

    const paidNet = this.invoices.find((x) => x.invoice.status == InvoiceStatus.Paid && x.invoice.entityType == "net");
    if (paidNet?.invoice.data.channel == PaymentChannel.PalkkausPersonal) {
      // HACK: As of writing PalkkausPersonal only sets Net payment as paid (not the tax).
      return "paid";
    }

    const inProgress = [InvoiceStatus.WaitingPalkkaus, InvoiceStatus.Read, InvoiceStatus.WaitingConfirmation, InvoiceStatus.PaymentStarted];
    if (this.invoices.find((x) => inProgress.indexOf(x.invoice.status) >= 0)) {
      return "inProgress";
    }
    if (this.invoices.find((x) => x.invoice.status === InvoiceStatus.Canceled )) {
      return "canceled";
    }
    const exceptions = [InvoiceStatus.Undefined, InvoiceStatus.Error];
    if (this.invoices.find((x) => exceptions.indexOf(x.invoice.status) >= 0)) {
      return "exceptions";
    }
    const invoicesCreated = [InvoiceStatus.Unread, InvoiceStatus.Forecast, InvoiceStatus.Preview];
    if (this.invoices.every((x) => invoicesCreated.indexOf(x.invoice.status) >= 0)) {
      return "invoicesCreated";
    }
    const paid = [InvoiceStatus.Paid, InvoiceStatus.Forecast, InvoiceStatus.Preview];
    if (this.invoices.every((x) => paid.indexOf(x.invoice.status) >= 0)) {
      return "paid";
    }
    return "inProgress";
  }

  /** Gets the type of current. */
  public get currentType(): "Calculation" | "PayrollDetails" | null {
    if (!this.current) {
      return null;
    }
    if (InvoicesLogic.isPayroll(this.current)) {
      return "PayrollDetails";
    }
    if (InvoicesLogic.isCalculation(this.current)) {
      return "Calculation";
    }
    return null;
  }

  /** Returns true if the calculations are valid for payment. */
  public get isValid(): boolean {
    if (InvoicesLogic.isPayroll(this.current)) {
      return this.current.validations.every((x) => x.isValid);
    }
    if (InvoicesLogic.isCalculation(this.current)) {
      return this.current.result && this.current.result.validation.isValid && this.current.worker.tax.isValid;
    }
    return false;
  }

  /**
   * If true, all invoices that would be paid now are zero-payments.
   * This means that the invoices will not be sent to the payment channel: They will just be marrked as paid.
   * The value is true only if there are payable invoices (status unread).
   * @param invoices The collection of full invoices to evaluate (from create methods).
   * If null, checks the invoices collection of the controller (preview / list items).
   */
  public isZeroPayment(invoices?: Invoice[]): boolean {
    if (!invoices) {
      // No input => Use the preview listing (ListItems)
      if (!this.invoices || this.invoices.length === 0) {
        return;
      }
      const payableInvoices = this.invoices.filter((x) => x.invoice.status === InvoiceStatus.Unread || x.invoice.status === InvoiceStatus.Paid);
      return payableInvoices.length > 0 && payableInvoices.every((x) => x.invoice.data.channel === PaymentChannel.ZeroPayment);
    }
    // Incoming list (full Invoice objects).
    if (invoices.length === 0) {
      return;
    }
    const payableInvoices = invoices.filter((x) => x.header.status === InvoiceStatus.Unread || x.header.status === InvoiceStatus.Paid);
    return payableInvoices.length > 0 && payableInvoices.every((x) => x.header.channel === PaymentChannel.ZeroPayment);
  }

  /** Gets and avatar image for a payment channel */
  public getChannelAvatar(option: InputEnumOption) {
    const channel = option.value || PaymentChannel.Undefined;
    return InvoicesLogic.getChannelAvatar(channel as PaymentChannel);
  }

  /** Reloads the preview invoices from the server. */
  public reload() {
    if (!this.channel || this.channel === PaymentChannel.Undefined || !this.isValid) {
      return; // Cannot start reload
    }
    if (this.bindingMode == null) {
      return;
    }
    if (this.bindingMode !== "model") {
      throw new Error("Not implemented: Only Calculation or PayrollDetails is currently supported.");
    }
    this.invoices = null;
    if (InvoicesLogic.isPayroll(this.current)) {
      this.invoicesApi.previewInvoices(this.channel, { payrollIds: [this.current.id] }).then((result) => {
        this.invoices = result;
      });
    } else if (InvoicesLogic.isCalculation(this.current)) {
      this.invoicesApi.previewInvoices(this.channel, { calcs: [this.current] }).then((result) => {
        this.invoices = result;
      });
    }
  }

  /** Gets the part of the UI that should be visible. */
  public get uiStatus(): "invalid" | "loading" | "created" | "zeroPayment" | "supportedChannel" | "unsupported" | "undefined" {
    if (!this.isValid) {
      return "invalid"; // Show validation errors
    }
    if (this.paymentStatus === "unknown") {
      return "loading"; // this.invoices is still loading => Show spinner
    }
    if (this.isZeroPayment()) {
      return "zeroPayment";
    }
    if (this.paymentStatus === "exceptions"
      || this.paymentStatus === "canceled"
      || this.paymentStatus === "inProgress"
      || this.paymentStatus === "invoicesCreated"
      || this.paymentStatus === "paid"
    ) {
      return "created"; // Invoices have already been created (no swithching channel).
    }
    if (!this.channel || this.channel === PaymentChannel.Undefined) {
      return "undefined"; // Choose channel
    }
    const supportedChannels = [
      PaymentChannel.AccountorGo,
      PaymentChannel.FinagoSolo,
      PaymentChannel.Kevytyrittaja,
      PaymentChannel.PalkkausManual,
      PaymentChannel.PalkkausPersonal,
      PaymentChannel.Test,
      PaymentChannel.ZeroPayment,
      PaymentChannel.TalenomOnline,
      PaymentChannel.VismaNetvisor,
      PaymentChannel.Procountor
    ];
    if (supportedChannels.find((x) => x === this.channel)) {
      return "supportedChannel"; // Supported channel
    }
    return "unsupported";
  }

  /**
   * Starts the invoices creation process from the current channel and model.
   * This method is used when controller is used first as previe and then creating invoices.
   */
  public createInvoices(closeFunc?: (action) => any) {
    if (!this.channel || this.channel === PaymentChannel.Undefined || !this.isValid) {
      throw new Error("Cannot start invoices creation");
    }

    const inProgress = [InvoiceStatus.WaitingPalkkaus, InvoiceStatus.Read, InvoiceStatus.WaitingConfirmation, InvoiceStatus.PaymentStarted];

    // TODO: We should allow redirection for payroll payments to TalenomOnline and Procountor, like with InvoicesLogic.isCalculation.
    if (InvoicesLogic.isPayroll(this.current)) {
      if (this.channel === PaymentChannel.Test ||
        this.channel === PaymentChannel.VismaNetvisor ||
        this.channel === PaymentChannel.Procountor) {

        if (this.paymentStatus === "inProgress") {
          const loader = this.uiHelpers.showLoading("Maksuikkuna avattu...");
          const idsToPay = this.invoices.filter((x) => inProgress.indexOf(x.invoice.status) >= 0).map((x) => x.invoice.id);
          let url = "";
          switch (this.channel) {
            case PaymentChannel.Test:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/test#/dialogs/payment" :
                  "https://integrations.salaxy.com/test#/dialogs/payment";
              break;
            case PaymentChannel.Procountor:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/procountor/payment.html" :
                  "https://integrations.salaxy.com/procountor/payment.html";
              break;
            case PaymentChannel.VismaNetvisor:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/vismanetvisor/payment.html" :
                  "https://integrations.salaxy.com/vismanetvisor/payment.html";
              break;
          }
          url += "?invoiceIds=" + idsToPay + "&token=" + this.ajax.getCurrentToken();
          this.uiHelpers.showExternalDialog("payrollPayment", url, {})
            .then(() => {
              loader.dismiss();
              // HACK: Need a way to configure the URL and potentially raise an event for reload.
              this.$location.path("/payroll/details/" + this.current.id + "&refresh=" + new Date().getTime());
              if (closeFunc) {
                closeFunc("ok");
              }
            });
        } else {
          const loader = this.uiHelpers.showLoading("Laskuja luodaan...");
          this.invoicesApi.createInvoicesForPayroll(this.channel, this.current.id).then((result) => {
            loader.setHeading("Maksuikkuna avattu...");
            const idsToPay = result.filter((x) => x.header.status === InvoiceStatus.Unread).map((x) => x.id);
            let url = "";
            switch (this.channel) {
              case PaymentChannel.Test:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/test#/dialogs/payment" :
                    "https://integrations.salaxy.com/test#/dialogs/payment";
                break;
              case PaymentChannel.Procountor:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/procountor/payment.html" :
                    "https://integrations.salaxy.com/procountor/payment.html";
                break;
              case PaymentChannel.VismaNetvisor:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/vismanetvisor/payment.html" :
                    "https://integrations.salaxy.com/vismanetvisor/payment.html";
                break;
            }
            url += "?invoiceIds=" + idsToPay + "&token=" + this.ajax.getCurrentToken();
            this.uiHelpers.showExternalDialog("payrollPayment", url, {})
              .then(() => {
                loader.dismiss();
                // HACK: Need a way to configure the URL and potentially raise an event for reload.
                this.$location.path("/payroll/details/" + this.current.id + "&refresh=" + new Date().getTime());
                if (closeFunc) {
                  closeFunc("ok");
                }
              });
          });
        }
      } else if (this.channel === PaymentChannel.TalenomOnline) {
        alert("ERROR: Talenom online payment not supported.");
        if (closeFunc) {
          closeFunc("ok");
        }
      } else {
        const loader = this.uiHelpers.showLoading("Laskuja luodaan...");
        this.invoicesApi.createInvoicesForPayroll(this.channel, this.current.id).then((result) => {
          loader.dismiss();
          if (this.isZeroPayment(result)) {
            // HACK: Need a way to configure the URL and potentially raise an event for reload.
            this.$location.path("/payroll/details/" + this.current.id + "&refresh=" + new Date().getTime());
            this.showInvoiceSuccessAlert(PaymentChannel.ZeroPayment);
          } else {
            // HACK: Need a way to configure the URL and potentially raise an event for reload.
            this.$location.path("/payroll/details/" + this.current.id + "&refresh=" + new Date().getTime());
            this.showInvoiceSuccessAlert(this.channel);
          }
          if (closeFunc) {
            closeFunc("ok");
          }
        });
      }
    } else if (InvoicesLogic.isCalculation(this.current)) {

      const refreshCurrentCalculation = (invoices: Invoice[]): Promise<Calculation> => {
        if (invoices) {
          const calcId = (invoices.find((x) => x.header.type === InvoiceType.Net || x.header.type === InvoiceType.Gross))?.header.businessObjects[0];
          if (calcId) {
            return this.calculationsApi.getSingle(calcId).then((calculation) => {
              angular.copy(calculation, this.current);
              return this.current;
            });
          }
        }
        return Promise.resolve(this.current);
      }
      if (this.channel === PaymentChannel.Test ||
        this.channel === PaymentChannel.VismaNetvisor ||
        this.channel === PaymentChannel.Procountor) {

        if (this.paymentStatus === "inProgress") {
          const loader = this.uiHelpers.showLoading("Maksuikkuna avattu...");
          const idsToPay = this.invoices.filter((x) => inProgress.indexOf(x.invoice.status) >= 0).map((x) => x.invoice.id);
          let url = "";
          switch (this.channel) {
            case PaymentChannel.Test:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/test#/dialogs/payment" :
                  "https://integrations.salaxy.com/test#/dialogs/payment";
              break;
            case PaymentChannel.Procountor:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/procountor/payment.html" :
                  "https://integrations.salaxy.com/procountor/payment.html";
              break;
            case PaymentChannel.VismaNetvisor:
              url =
                Configs.current.isTestData ?
                  "https://test-integrations.salaxy.com/vismanetvisor/payment.html" :
                  "https://integrations.salaxy.com/vismanetvisor/payment.html";
              break;
          }
          url += "?invoiceIds=" + idsToPay + "&token=" + this.ajax.getCurrentToken();
          this.uiHelpers.showExternalDialog("calcPayment", url, {})
            .then(() => {
              refreshCurrentCalculation(null).then(() => {
                loader.dismiss();
                // HACK: Need a way to configure the URL and potentially raise an event for reload.
                this.$location.path("/calc/details/" + this.current.id + "&refresh=" + new Date().getTime());
              });
              if (closeFunc) {
                closeFunc("ok");
              }
            });
        }
        else {
          const loader = this.uiHelpers.showLoading("Laskuja luodaan...");
          this.invoicesApi.createInvoices(this.current, this.channel).then((result) => {
            loader.setHeading("Maksuikkuna avattu...");
            const idsToPay = result.filter((x) => x.header.status === InvoiceStatus.Unread).map((x) => x.id);
            let url = "";
            switch (this.channel) {
              case PaymentChannel.Test:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/test#/dialogs/payment" :
                    "https://integrations.salaxy.com/test#/dialogs/payment";
                break;
              case PaymentChannel.Procountor:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/procountor/payment.html" :
                    "https://integrations.salaxy.com/procountor/payment.html";
                break;
              case PaymentChannel.VismaNetvisor:
                url =
                  Configs.current.isTestData ?
                    "https://test-integrations.salaxy.com/vismanetvisor/payment.html" :
                    "https://integrations.salaxy.com/vismanetvisor/payment.html";
                break;
            }
            url += "?invoiceIds=" + idsToPay + "&token=" + this.ajax.getCurrentToken();
            this.uiHelpers.showExternalDialog("calcPayment", url, {})
              .then(() => {
                refreshCurrentCalculation(result).then(() => {
                  loader.dismiss();
                  // HACK: Need a way to configure the URL and potentially raise an event for reload.
                  this.$location.path("/calc/details/" + this.current.id + "&refresh=" + new Date().getTime());
                });
                if (closeFunc) {
                  closeFunc("ok");
                }
              });
          });
        }
      } else if (this.channel === PaymentChannel.TalenomOnline) {
        alert("ERROR: Talenom online payment not supported.");
        if (closeFunc) {
          closeFunc("ok");
        }
      } else {
        const loader = this.uiHelpers.showLoading("Laskuja luodaan...");
        this.invoicesApi.createInvoices(this.current, this.channel).then((result) => {
          refreshCurrentCalculation(result).then(() => {
            loader.dismiss();
            if (this.isZeroPayment(result)) {
              // HACK: Need a way to configure the URL and potentially raise an event for reload.
              this.$location.path("/calc/details/" + this.current.id + "&refresh=" + new Date().getTime());
              this.showInvoiceSuccessAlert(PaymentChannel.ZeroPayment);
            } else {
              // HACK: Need a way to configure the URL and potentially raise an event for reload.
              this.$location.path("/calc/details/" + this.current.id + "&refresh=" + new Date().getTime());
              this.showInvoiceSuccessAlert(this.channel);
            }
          });
          if (closeFunc) {
            closeFunc("ok");
          }
        });
      }
    }
  }

  /**
   * Create invoices for calculation: Immediate execution without preview. Used in PRO and other advanced UI.
   * Shows a loader dialog, saves changes if necessary, creates invoices and reloads the controller.
   * @param calcController Controller that contains the calculation that should be paid. May have save changes.
   * @param channel Payment channel that is used for payment.
   */
  public createInvoicesForCalculation(calcController: CalculationCrudController, channel: PaymentChannel): Promise<Invoice[]> {
    const loader = this.uiHelpers.showLoading("Maksu aloitettu", "Luodaan laskuja");
    return this.invoicesApi.createInvoices(calcController.current, channel).then((result) => {
      calcController.reload();
      loader.dismiss();
      if (this.isZeroPayment(result)) {
        this.showInvoiceSuccessAlert(PaymentChannel.ZeroPayment);
      } else {
        this.showInvoiceSuccessAlert(channel);
      }
      return result;
    });
  }

  /**
   * Create invoices for payroll: Immediate execution without preview. Used in PRO and other advanced UI.
   * Shows a loader dialog, saves changes if necessary, creates invoices and reloads the controller.
   * @param payrollController Controller that contains the calculation that should be paid. May have save changes.
   * @param channel Payment channel that is used for payment.
   */
  public createInvoicesForPayroll(payrollController: PayrollCrudController, channel: PaymentChannel): Promise<Invoice[]> {
    const loader = this.uiHelpers.showLoading("Maksu aloitettu", "Luodaan laskuja");
    const reqSalaryDate = payrollController.current.input.salaryDate;
    const salaryDate = payrollController.current.info.salaryDate;
    const invoiceCreate = () => {
      return this.invoicesApi.createInvoicesForPayroll(channel, payrollController.current.id).then((result) => {
        payrollController.reload();
        loader.dismiss();
        if (this.isZeroPayment(result)) {
          this.showInvoiceSuccessAlert(PaymentChannel.ZeroPayment);
        } else {
          this.showInvoiceSuccessAlert(channel);
        }
        return result;
      });
    };

    if (payrollController.hasChanges || payrollController.isNew() ||
      (reqSalaryDate && !SalaryDateLogic.isValidSalaryDate(reqSalaryDate, null, null, true, channel)) ||
      (salaryDate && !SalaryDateLogic.isValidSalaryDate(salaryDate, null, null, false, channel))) {

      return payrollController.save().then(() => {
        return invoiceCreate();
      });
    } else {
      return invoiceCreate();
    }
  }

  /** Gets the type of the calculation for display purposes. */
  public getDueDateType(item: InvoicePreview): "default" | "estimation" | "forecast" | "preview" | "zero" {
    if (item.invoice.entityType === "unemployment") {
      return "estimation";
    }
    if (item.invoice.status === InvoiceStatus.Forecast) {
      return "forecast";
    }
    if (item.invoice.status === InvoiceStatus.Preview) {
      return "preview";
    }
    if (item.invoice.data.channel === PaymentChannel.ZeroPayment) {
      return "zero";
    }
    return "default";
  }

  /**
   * Returns the net invoice preview for current calculation.
   */
  public get primaryCalcNetInvoicePreview(): InvoicePreview {
    if (!this._primaryCalcNetInvoicePreview && this.invoices && this.invoices.length > 0) {
      const preview = this.invoices.find((x) => x.invoice.entityType === InvoiceType.Net && x.invoice.status !== InvoiceStatus.Canceled && x.invoice.status !== InvoiceStatus.Error);
      if (preview) {
        this._primaryCalcNetInvoicePreview = preview;
      }
    }
    return this._primaryCalcNetInvoicePreview;
  }

  /**
   * Returns the ysc invoice preview for current calculation.
   */
  public get primaryCalcTaxInvoicePreview(): InvoicePreview {
    if (!this._primaryCalcTaxInvoicePreview && this.invoices && this.invoices.length > 0) {
      const preview = this.invoices.find((x) => x.invoice.entityType === InvoiceType.Tax && x.invoice.status !== InvoiceStatus.Canceled && x.invoice.status !== InvoiceStatus.Error);
      if (preview) {
        this._primaryCalcTaxInvoicePreview = preview;
      }
    }
    return this._primaryCalcTaxInvoicePreview;
  }

  private getNetInvoiceId(invoices: Invoice[]): string {
    if (!invoices || invoices.length === 0) {
      return null;
    }
    return (invoices.find((x) => x.header.type === InvoiceType.Net) || {}).id;
  }

  private showInvoiceSuccessAlert(channel: PaymentChannel) {
    if (channel === PaymentChannel.ZeroPayment) {
      this.uiHelpers.showAlert("Laskelmat merkitty maksetuksi (nollamaksu)", `Laskelma(t) on merkitty maksetuiksi ja niistä tehdään tulorekisteri-ilmoitus viiden päivän kuluessa.`);
    } else if (channel === PaymentChannel.AccountorGo) {
      this.uiHelpers.showAlert("Maksun luonti onnistui",
        `Lasku(t) nettopalkasta on nyt luotu Accountor Go -palvelun Maksut-osioon. Käy hyväksymässä lasku kohdasta "Avoimet maksut". Lasku näkyy siellä noin 2 minuutin sisällä.

Laskut sivukuluista tulevat Avoimet maksut -osioon automaattisesti kuun vaihteen jälkeen. Palvelu tekee myös tarvittavat tulorekisteri-ilmoitukset palkanmaksuista.`);
    } else if (channel === PaymentChannel.FinagoSolo) {
      this.uiHelpers.showAlert("Maksun luonti onnistui",
        `Lasku(t) nettopalkasta on nyt luotu Procountor Solo Maksut-osioon. Käy hyväksymässä lasku kohdasta "Avoimet maksut". Lasku näkyy siellä noin 2 minuutin sisällä.

Laskut sivukuluista tulevat Avoimet maksut -osioon automaattisesti kuun vaihteen jälkeen. Palvelu tekee myös tarvittavat tulorekisteri-ilmoitukset palkanmaksuista.`);

    } else if (channel === PaymentChannel.Kevytyrittaja) {
      this.uiHelpers.showAlert("Maksun luonti onnistui",
        `Lasku(t) nettopalkasta on nyt luotu Talenom Kevytyrittäjä -Palveluun.

Laskut sivukuluista tulevat Talenom Kevytyrittäjä -palveluun automaattisesti kuun vaihteen jälkeen. Palvelu tekee myös tarvittavat tulorekisteri-ilmoitukset palkanmaksuista.`);

    } else if (channel === PaymentChannel.PalkkausManual) {
      this.uiHelpers.showAlert("Maksun luonti onnistui", `Lasku(t) nettopalkasta on nyt luotu Maksut -listalle. Merkitse lasku maksetuksi, kun olet maksanut laskua vastaavan summan.

Laskut sivukuluista tulevat Maksut-listalle automaattisesti kuun vaihteen jälkeen. Palvelu tekee myös tarvittavat tulorekisteri-ilmoitukset maksetuksi merkityistä nettopalkoista.`);

    } else if (channel === PaymentChannel.PalkkausPersonal) {
      this.uiHelpers.showAlert("Maksun luonti onnistui", `Laskut nettopalkasta ja verosta on nyt luotu Maksut -listalle. Merkitse nettopalkan lasku maksetuksi, kun olet maksanut laskua vastaavan summan.

Palvelu tekee myös tarvittavat tulorekisteri-ilmoitukset maksetuksi merkityistä nettopalkoista.`);

    }
  }
}
