﻿import * as angular from "angular";

import { Dates, Invoice, InvoicesLogic, InvoiceStatusNotification, PaymentChannel, PaymentChannelApi } from "@salaxy/core";

import { AjaxNg1 } from "../../ajax";
import { SessionService, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * Provides access for a payment channel to access invoices
 * that were sent to that channel and also the calculations etc. related to those invoices.
 * Only a Partner with registered Payment Channel can use this Controller.
 * Please contact Palkkaus.fi support for more information.
 */
export class PaymentChannelController extends ApiCrudObjectController<Invoice> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AjaxNg1",
    "UiHelpers",
    "$location",
    "$routeParams",
    "SessionService",
    "$scope",
  ];

  /** Status notification data for UI binding to update status. */
  public statusData: InvoiceStatusNotification;

  private _channel: PaymentChannel;

  private unread: Invoice[];

  constructor(
    private ajax: AjaxNg1,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private sessionService: SessionService,
    private $scope: angular.IScope,
  ) { // Dependency injection
    super(new PaymentChannelApi(null, ajax), uiHelpers, $location, $routeParams);
  }

  /**
   * Initialization code.
   */
  public $onInit() {
    const setChannel = () => {
      this.channel = this.sessionService.getOwnedPaymentChannel();
      this.reload();
      this.unread = null;
      if (this.odataController) {
         this.odataController.refreshClearAll();
      }
    };

    if (this.sessionService.isSessionChecked && this.sessionService.isAuthenticated) {
      setChannel();
    }
    this.sessionService.onAuthenticatedSession(this.$scope, () => {
      setChannel();
    });
    super.$onInit();
  }

  /** Name/key of the channel on which the controller operates. */
  public get channel(): PaymentChannel {
    return this._channel;
  }
  public set channel(value: PaymentChannel) {
    this._channel = value;
    if (value) {
      this.api = new PaymentChannelApi(value, this.ajax);
    }
  }

  /** Gets the full items for unread objects. */
  public get unreadInvoices(): Invoice[] {
    if (!this.unread) {
      this.unread = [];
      this.paymentChannelApi.getInvoicesByStatus().then((items) => {
        this.unread = items;
      });
    }
    return this.unread;
  }


  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "payment-channel",
      detailsUrl: this.detailsUrl || "/payment-channel/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/InvoicesPanel.html",
      oDataOptions: {},
    };
  }

  /** Returns content url for the invoice. */
  public getContentUrl(id: string, inline = false): string {
    return this.paymentChannelApi.getContentUrl(id, inline);
  }

  /**
   * Updates the status of the invoice.
   * @param data Status data. If not set, uses the statusData property of the controller.
   */
  public updateStatus(data: InvoiceStatusNotification = null): Promise<Invoice> {
    data = data || this.statusData;
    return this.setStatus(this.paymentChannelApi.updateStatus([data])
      .then((invoice) => {
        this.setCurrent(invoice);
        this.resetStatusData();
        return invoice;
      }));
  }

  /** Resets the status data according to current Invoice. */
  public resetStatusData() {
    if (!this.current) {
      this.statusData = null;
      return;
    }
    this.statusData = {
      id: this.current.id,
      date: Dates.getToday(),
      status: this.current.header.status,
      amount: this.current.header.total,
    };
  }

   /** Gets and avatar image for a payment channel */
   public getChannelAvatar() {
    return InvoicesLogic.getChannelAvatar(this.channel);
  }

  private get paymentChannelApi() {
    return this.api as PaymentChannelApi;
  }
}
