import { EditDialogParameters } from "../../services";

/**
 * Controller for Item edit dialogs.
 */
export class EditDialogController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["EditDialogParameters"];

    /**
     * The item that is being edited.
     * This a copy of the original item to allow cancel / reset and other cahnge tracking.
     */
    public current: any;

    /** Additional logic: Helper functions, metadata etc. that view can use to contruct the UI. */
    public logic: any;

    constructor(public editDialogParams: EditDialogParameters<any>) {

    }
    /**
     * Implement IController
     */
    public $onInit = () => {
      // initialization
      if (!this.editDialogParams) {
        throw new Error("No editDialogParams coming in.");
      }
      this.current = this.editDialogParams.current;
      this.logic = this.editDialogParams.logic;
    }
}
