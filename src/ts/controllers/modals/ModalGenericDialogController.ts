
/**
 * Controller for alert, confirm and loading dialogs.
 */
export class ModalGenericDialogController {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["data"];

    constructor(
        public data: any,
    ) {
    }
}
