import { calcReportType, ReportType, YearEndUserFeedback, YearlyFeedback } from "@salaxy/core";

import { ReportsService, SettingsService, UiHelpers, YearEndService } from "../../services";

import { CrudControllerBase } from "../bases/CrudControllerBase";

/**
 * Handles user interaction for viewing and modifying the current yearly feedback data.
 */
export class YearEndControllerBase extends CrudControllerBase<YearlyFeedback> {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["YearEndService", "$location", "$attrs", "UiHelpers", "SettingsService", "ReportsService", "$scope", "$uibModal"];

  /** If true, forces the hasBeenSent to false => Allows edit. */
  public forceEdit: boolean;

  constructor(
    protected yearEndService: YearEndService,
    protected $location: angular.ILocationService,
    protected $attrs: angular.IAttributes,
    protected uiHelpers: UiHelpers,
    protected settingsService: SettingsService,
    protected reportsService: ReportsService,
    protected $scope: angular.IScope,
    protected $uibModal: angular.ui.bootstrap.IModalService,
  ) {
    super(yearEndService, $location, $attrs, uiHelpers);
  }

  /** Returns true if the feedback has been sent */
  public get hasBeenSent(): boolean {
    if (this.forceEdit || !this.current || !this.current.userStatus) {
      return false;
    }
    return this.current.userStatus === YearEndUserFeedback.Ok
      || this.current.userStatus === YearEndUserFeedback.OkWithModifications
      || this.current.userStatus === YearEndUserFeedback.WillHandleMyself;
  }

  /** Resets the user status (designed for test only) */
  public resetUserStatus() {
    this.forceEdit = true;
    this.current.userFeedbackText = null;
  }

  /**
   * Gets a URL for a calculation report
   * @param reportType - Type of report
   * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
   */

  public getCalcReport(reportType: calcReportType, calcId: string) {
    // this.current.workerSummaries[0].worker.avatar.id
    return this.reportsService.getReportUrlForCalc(reportType, calcId);
  }

  /**
   * Gets a link URL for a yearly report. This is a full link with token and absolute URL.
   * @param type - Type of the report must be one of the yearly reports
   * @param year - Year for the report
   * @param id - Worker ID for those reports that are specific to one Worker.
   * @param id2 - Second Worker ID for those reports that have two Workers in one report
   */

  public getYearlyReport(type: ReportType, year: number, id?: string, id2?: string) {
    return this.reportsService.getYearlyReportUrl(type, year, id, id2);
  }

  /**
   * Gets the default configuration for submit modal dialog
   * @param userStatus User status for the feedback
   */
  protected getModalConfig(userStatus: YearEndUserFeedback): angular.ui.bootstrap.IModalSettings {
    const modalScope = this.$scope.$new() as any;
    const srv = this.yearEndService;
    modalScope.submit = () => {
      srv.current.userStatus = userStatus;
      srv.saveCurrent();
    };
    modalScope.current = srv.current;
    modalScope.title = "Virhetilanne";
    modalScope.content = "Lähetyksessä tapahtui virhe (parameters not handled).";

    const config: angular.ui.bootstrap.IModalSettings = {

      // title: "Virhetilanne",

      // content: "Lähetyksessä tapahtui virhe (parameters not handled).",

      // show: true,

      // html: true,
      windowTemplateUrl: "salaxy-components/modals/ui/DialogWindow.html",
      templateUrl: "salaxy-components/modals/year-end/FeedbackSubmit.html",

      scope: modalScope,

      backdrop: "static",

      size: "lg",

    };

    return config;

  }
  /**
   * Shows the submit modal dialog
   * @param config Modal configuration
   */
  protected showModal(config): Promise<any> {
    const modal = this.$uibModal.open(config);
    return Promise.resolve(modal.result);
  }
}
