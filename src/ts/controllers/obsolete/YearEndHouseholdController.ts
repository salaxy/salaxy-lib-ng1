import { Dates, InsuranceContract, Numeric, YearEndUserFeedback } from "@salaxy/core";
import { YearEndControllerBase } from "./YearEndControllerBase";

/**
 * Implements the YearendControllerBase for Household accounts.
 */
export class YearEndHouseholdController extends YearEndControllerBase {

  /**
   * Household tax deduction: Percentage from the total salary
   * This number is updated by getTaxDeductionTotal().
   */

  public percentDeductible = 0;

  /** Calculates the household tax deduction total */
  public getTaxDeductionTotal() {
    let totalSalary = 0;
    let deductibleSalary = 0;
    let deductibleSideCost = 0;
    if (!this.current || !this.current.workerSummaries) {
      return null;
    }

    for (const rpt of this.current.workerSummaries) {
      for (const calc of rpt.calculations) {
        totalSalary += calc.salary;
        if (calc.selectedIsDeductible) {
          deductibleSideCost += calc.sideCosts;
          deductibleSalary += calc.salary;
        }
      }
    }
    this.percentDeductible = totalSalary === 0 ? 0 : deductibleSalary / totalSalary;
    let deductibleTotal = deductibleSideCost;       // All side costs
    deductibleTotal += 0.15 * deductibleSalary;     // 15% of salary
    deductibleTotal += this.getInsuranceSum();      // All insurance
    return deductibleTotal;
  }

  /**
   * The insurance sum is either the insurance sum that the user has input
   * or the last known sum from the employer profile.
   */
  public getInsuranceSum() {
    if (!this.current) {
      return null;
    }
    return Numeric.parseNumber(this.current.household.insuranceAmount) || (this.current.household as any).insuranceAmountFromAccount;
  }

  /** Returns true if there is more than 2350€ of household deduction meaning that it should be split between spouses */

  public canSplitWithSpouse() {
    return this.getTaxDeductionTotal() > 2350;
  }

  /** Returns true if the spouse information is provided */
  public isSpouseInfoProvided() {
    return this.current.household.spouseName && this.current.household.spousePersonalId;
  }

  /** Gets the insurance product for the current user */

  public get insurance(): InsuranceContract {
    const forDate = Dates.getTodayMoment();
    return this.settingsService.current.calc.insuranceContracts.find( (x) =>
     Dates.getMoment(x.startDate ?? "1900-01-01").isSameOrBefore(forDate) &&
     Dates.getMoment(x.endDate ?? "2100-01-01").add(1,"day").isAfter(forDate)) || {};
  }

  /**
   * Shows the confirm dialog depending on which button is clicked
   * @param userStatus - Status corresponds to the button that user clicks.
   */
  public showConfirmDialog(userStatus: YearEndUserFeedback) {
    const config = this.getModalConfig(userStatus);
    const scope = config.scope as any;
    switch (userStatus) {
      case YearEndUserFeedback.Ok:
        scope.title = "Kyllä, tiedot tarkistettu / korjattu";
        scope.content = `
                    <h2>Kyllä, tiedot tarkistettu / korjattu</h2>
                    <p>Palkkaus.fi lähettää kotitalousvähennysilmoituksen 14B-lomakkeen verottajalle</p>`;
        break;
      case YearEndUserFeedback.OkWithModifications:
        scope.title = "Tiedoissa on puutteita / virheitä";
        scope.content = `
                    <h2>Tiedoissa on puutteita / virheitä</h2>
                    <p class="lead">Jos tiedoissa on puutteita, joita et voinut korjata tällä sivulla itse, listaa puutteet tähän. Palkkaus.fi on sinuun tarvittaessa yhteydessä.</p>`;
        scope.showTextbox = true;
        break;
      case YearEndUserFeedback.WillHandleMyself:
        scope.title = "Teen kotitalousvähennysilmoituksen itse";
        scope.content = `
                    <h2>Teen kotitalousvähennysilmoituksen itse</h2>
                    <p class="lead">
                    Palkkaus.fi ei lähetä kotitalousvähennysilmoitusta (14B-lomake) verottajalle. Valitse tämä vaihtoehto, jos olet jo itse tehnyt kotitalousvähennysilmoitukset OmaVerossa tai et halua hakea kotitalousvähennystä.
                    </p>
                    `;
        break;
    }
    this.showModal(config);
  }

}
