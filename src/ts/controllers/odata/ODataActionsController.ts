import * as angular from "angular";
import { saveAs } from "file-saver";

import { ApiCrudObject, ApiListItem, CrudApiBase, DataRow, Dates, ImportableCrudApi, ImportBatch, ImportBatches, Mapper, ODataQueryOptions, TableFormat, Tables, Workers } from "@salaxy/core";

import { AlertService, ExcelHelpers, UiHelpers, WizardService, WizardStep } from "../../services";

import { ODataQueryController } from "../bases/ODataQueryController";

/** Locale id */
enum LocaleId {
  Fi = "fi",
  En = "en",
}

/** File tupe id */
enum FileTypeId {
  ExcelFile = "excelFile",
  CsvFile = "csvFile",
  ExcelCopy = "excelCopy",
  CsvCopy = "csvCopy",
}

/** Export length id */
enum ExportLengthId {
  All = "all",
  Current = "current",
  Short = "short",
  Long = "long",
}

/**
 * Controller that provides actions (search, export, import) for OData tables.
 */
export class ODataActionsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AlertService", "UiHelpers", "WizardService", "$timeout"];

  /** The parent ODataQueryController that contains the list and all the methods. */
  public $odata: ODataQueryController;

  /** Length of the data export */
  public exportLengthId: string = ExportLengthId.All;

  /** Supported lengths for data exports */
  public exportLengths: {
    /** Export length id */
    id: string,
    /** Export length label */
    label: string,
    /** Max length */
    length: number,
  }[] = [
      {
        id: ExportLengthId.All,
        label: "Kaikki rivit",
        length: 0,
      },
      {
        id: ExportLengthId.Current,
        label: "Esikatselussa olevat rivit",
        length: 0,
      },
      {
        id: ExportLengthId.Short,
        label: "Viimeiset 1000 riviä",
        length: 1000,
      },
      {
        id: ExportLengthId.Long,
        label: "Viimeiset 10 000 riviä",
        length: 10000,
      },
    ];

  /** Locale for exports */
  public localeId: string = LocaleId.Fi;

  /** Supported locales for exports */
  public locales: {
    /** Locale id */
    id: string,
    /** Locale label */
    label: string,
    /** Local description */
    description: string,
  }[] = [
      {
        id: LocaleId.Fi,
        label: "Suomalainen",
        description: "Suomalainen (12,34 / 1.2.2020)",
      },
      {
        id: LocaleId.En,
        label: "Kansainvälinen",
        description: "Kansainvälinen (12.34 / 2020-02-01)",
      },
    ];

  /** Export format */
  public fileTypeId: string = FileTypeId.ExcelCopy;

  /** Supported export formats */
  public fileTypes: {
    /** File type id */
    id: string,
    /** File type label */
    label: string,
    /** Boolean indicating if the file type supports different locales */
    hasLocales: boolean,
    /** Workflow message after export */
    workflowMessage,
  }[] = [
      {
        id: FileTypeId.ExcelFile,
        label: "Excel-tiedosto",
        hasLocales: false,
        workflowMessage: "Excel tiedostoon",
      },
      {
        id: FileTypeId.ExcelCopy,
        label: "Excel copy-paste",
        hasLocales: true,
        workflowMessage: "Excel leikepöydälle",
      },
      {
        id: FileTypeId.CsvFile,
        label: "CSV-tiedosto",
        hasLocales: true,
        workflowMessage: "Csv tiedostoon",
      },
      {
        id: FileTypeId.CsvCopy,
        label: "CSV copy-paste",
        hasLocales: true,
        workflowMessage: "Csv leikepöydälle",
      },
    ];

  /** Current mapper */
  public mapper: Mapper<any, DataRow> = null;

  /** Flag for export progress */
  public isExporting = false;

  /** Messages from exports. */
  public exportMessage: string = null;

  /** List item rows */
  public exportRows: DataRow[] = [];

  /** Wizard steps */
  public importSteps: WizardStep[] = [
    {
      title: "Liitä / lataa taulukko",
      view: "salaxy-components/modals/odata/ImportStep1.html",
    },
    {
      title: "Asetukset",
      view: "salaxy-components/modals/odata/ImportStep2.html",
    },
    {
      title: "Validointi",
      view: "salaxy-components/modals/odata/ImportStep3.html",
    },
    {
      title: "Tallennus",
      view: "salaxy-components/modals/odata/ImportStep4.html",
    },
  ];

  /** Raw data to import. */
  public batch: ImportBatch<any, DataRow> = null;

  /** Flag for validate progress */
  public isValidating = false;

  /** Messages from validation. */
  public validateMessage: string = null;

  /** Flag for import progress */
  public isImporting = false;

  /** Messages from validation. */
  public importMessage: string = null;

  /** All list item mappers. */
  private _listItemMappers: Mapper<any, DataRow>[] = null;

  /** All mappers. */
  private _allMappers: Mapper<any, DataRow>[] = null;

  /** All object mappers. */
  private _objectMappers: Mapper<any, DataRow>[] = null;

  /** Map keys for binding */
  private _mapKeys: any[] = [];

  /**
   * Constructor for Dependency Injection.
   * @param alertService Alert service for communicating export / import events
   * @param uiHelpers Salaxy ui helpers.
   * @param  wizardService Wizard service.
   */
  constructor(private alertService: AlertService, private uiHelpers: UiHelpers, private wizardService: WizardService, private $timeout: angular.ITimeoutService) {

  }

  /** Available list item mappers for import/export */
  public get listItemMappers(): Mapper<ApiListItem, DataRow>[] {
    if (!this._listItemMappers) {
      const api = this.$odata.getApi();
      if (this.isImportableCrudApi(api)) {
        this._listItemMappers = api.getListItemMappers();
      } else {
        this._listItemMappers = [];
      }
    }
    return this._listItemMappers;
  }

  /** Available mappers for import/export */
  public get allMappers(): Mapper<any, DataRow>[] {
    if (!this._allMappers) {
      const api = this.$odata.getApi();
      if (this.isImportableCrudApi(api)) {
        this._allMappers = [].concat(this.listItemMappers).concat(this.objectMappers);
      } else {
        this._allMappers = [];
      }
    }
    return this._allMappers;
  }

  /** Available object mappers for import/export */
  public get objectMappers(): Mapper<any, DataRow>[] {
    if (!this._objectMappers) {
      const api = this.$odata.getApi();
      if (this.isImportableCrudApi(api)) {
        this._objectMappers = api.getObjectMappers();
      } else {
        this._objectMappers = [];
      }
    }
    return this._objectMappers;
  }

  /** Shows an export dialog for full export. */
  public showExport() {

    this.mapper = this.listItemMappers[0];
    this.fileTypeId = FileTypeId.ExcelCopy;
    const dialogData = {
      $parent: this,
    };

    this.getExportData().then(() => {
      this.uiHelpers.showDialog(
        "salaxy-components/modals/odata/OdataExport.html",
        null,
        dialogData,
        null,
        "lg");
    });
  }

  /** Exports data to desired format. */
  public export(mapper: Mapper<any, DataRow> = null, fileTypeId: string = null): Promise<void> {
    this.mapper = mapper || this.mapper;
    this.fileTypeId = fileTypeId || this.fileTypeId;
    this.isExporting = true;
    this.exportMessage = "Luodaan... ";
    const notify = (message, index, count) => {
      this.exportMessage = `${message} (${index + 1}${count ? "/" + count : ""})`;
    };

    return this.getExportData(notify).then((rows) => {
      this.isExporting = false;
      this.exportMessage = null;
      if (!rows || !rows.length) {
        this.alertService.addError(`Ei rivejä.`);
        return;
      }
      switch (this.fileTypeId) {
        case FileTypeId.CsvFile:{
          const csvFileData = Tables.export(rows, TableFormat.Csv, this.localeId as any);
          saveAs(new Blob([csvFileData], { type: "application/octet-stream;charset=utf-8" }), `${this.getFileName()}.csv`);
          return;}
        case FileTypeId.ExcelFile:
          return ExcelHelpers.export(Tables.exportAsArrays(rows), this.getFileName());
        case FileTypeId.CsvCopy:{
          const csvData = Tables.export(rows, TableFormat.Csv, this.localeId as any);
          this.copyToClipboard(csvData);
          this.alertService.addSuccess(`${rows.length} riviä kopioitu leikepöydälle.`);
          return;}
        case FileTypeId.ExcelCopy:{
          const tabbedData = Tables.export(rows, TableFormat.Txt, this.localeId as any);
          this.copyToClipboard(tabbedData);
          this.alertService.addSuccess(`${rows.length} riviä kopioitu leikepöydälle.`);
          return;}
        default:
          this.uiHelpers.showAlert("Ei vielä toteutettu", "Tätä tiedostotyyppiä ei ole toteutettu.");
          return;
      }
    });
  }

  /** Action for changing the mapper */
  public changeExportMapper(mapper: Mapper<any, DataRow>) {
    this.mapper = mapper;
    if (mapper.isListItemMapper) {
      this.getExportData();
    }
  }

  /** Shows import dialog. */
  public showImport() {
    this.wizardService.setSteps(this.importSteps);
    this.wizardService.activeStepNumber = 1;

    const dialogData = {
      $parent: this,
    };
    this.uiHelpers.showDialog(
      "salaxy-components/modals/odata/OdataImport.html",
      "WizardController",
      dialogData,
      null,
      "lg");
  }

  /** Paste data from clipboard. */
  public pasteFromClipboard(): Promise<string> {
    const api = this.$odata.getApi() as ImportableCrudApi<any>;
    return ImportBatches.open(null, null, this.mapper, api).then((batch) => {
      this.fileTypeId = null;
      this.batch = batch;
      return (navigator as any).clipboard.readText().then((text) => {
        this.batch.rawData = text;
        // Ensure refresh in UI
        return this.$timeout(() => {
          return text;
        });
      });
    });
  }

  /**
   * Reads the file
   * @param file Selected file
   */
  public readFile(file: File): Promise<string> {
    if (!file) {
      return Promise.resolve(null);
    }
    // TODO detect if excel file and read content using Excel reader.
    if (file.name) {
      if (file.name.toLowerCase().match(/\.xl(s|sx)$/g)) {
        this.uiHelpers.showAlert("Tiedoston muotoa ei tueta", "Excel-tiedostot eivät ole vielä tuettuja. Käytä csv tai tab -muotoa.");
        return Promise.resolve(null);
      }
    }

    const read = new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
      reader.onerror = () => reject;
      reader.readAsText(file);
    });

    const api = this.$odata.getApi() as ImportableCrudApi<any>;
    return ImportBatches.open(null, null, this.mapper, api).then((batch) => {
      this.fileTypeId = null;
      this.batch = batch;
      return read.then((text) => {
        this.batch.rawData = text;
        // Ensure refresh in UI
        return this.$timeout(() => {
          return text;
        });
      });
    });
  }

  /** Read batch data */
  public readBatch(newBatchRawData: string = null) {

    const getBatch = (): Promise<ImportBatch<any, DataRow>> => {
      if (newBatchRawData) {
        const api = this.$odata.getApi() as ImportableCrudApi<any>;
        return ImportBatches.open(null, null, this.mapper, api).then((batch) => {
          this.fileTypeId = null;
          this.batch = batch;
          this.batch.rawData = newBatchRawData;
          return this.batch;
        });
      } else {
        // If raw data not given, this is an existing batch
        return Promise.resolve(this.batch);
      }
    };
    getBatch().then(() => {
      const tableFormat = this.fileTypeId === FileTypeId.CsvCopy || this.fileTypeId === FileTypeId.CsvFile ? TableFormat.Csv :
        this.fileTypeId === FileTypeId.ExcelCopy ? TableFormat.Txt :
          this.fileTypeId === FileTypeId.ExcelFile ? TableFormat.Txt : null;

      if (tableFormat) {
        this.batch.read(
          null,
          tableFormat,
          // this.fileTypeId === FileTypeId.ExcelFile ? TableFormat.Xlsx : null,
          this.localeId as any);
      }
    });
  }

  /** Changes import mapper */
  public changeImportMapper(mapper: Mapper<any, DataRow>) {
    this.mapper = mapper;
    this.batch.changeMapper(mapper);
    this.readBatch();
  }

  /** Mapkeys for binding */
  public get mapKeys() {
    this._mapKeys.splice(0, this._mapKeys.length);
    if (this.mapper) {
      this._mapKeys.push("");
      this._mapKeys.push(...this.mapper.keys);
    }
    return this._mapKeys;
  }

  /** Read batch data */
  public mapBatch() {
    this.batch.map();
  }

  /** Validate batch data */
  public validateBatch() {
    this.isValidating = true;
    this.validateMessage = "Tarkistetaan... ";
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const notify = (message, index, count, result) => {
      this.validateMessage = `${"Tarkistetaan..."} (${index + 1}${count ? "/" + count : ""})`;
    };
    this.batch.validate(notify).then(() => {
      this.isValidating = false;
      this.validateMessage = null;
    });
  }

  /** Import batch data */
  public importBatch() {
    this.isImporting = true;
    this.importMessage = "Tallennetaan... ";
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const notify = (message, index, count, result) => {
      this.importMessage = `${"Tallennetaan..."} (${index + 1}${count ? "/" + count : ""})`;
    };
    this.batch.import(notify).then(() => {
      this.isImporting = false;
      this.importMessage = null;
    });
  }

  /**
   * Returns the file type definition
   * @param fileTypeId File type id.
   */
  public getFileType(fileTypeId: string) {
    return this.fileTypes.find((x) => x.id === fileTypeId);
  }

  /**
   * Returns the locale definition
   * @param localeId Locale id.
   */
  public getLocale(localeId: string) {
    return this.locales.find((x) => x.id === localeId);
  }

  /**
   * Returns the export length definition
   * @param exportLengthId Export length id.
   */
  public getExportLength(exportLengthId: string) {
    return this.exportLengths.find((x) => x.id === exportLengthId);
  }

  private isImportableCrudApi<T>(api: CrudApiBase<T>): api is ImportableCrudApi<T> {
    return typeof (api) === "object" && (
      !!((api || {}) as ImportableCrudApi<T>).getListItemMappers);
  }

  private getExportData(notify: (message?: string, index?: number, count?: number) => void = null): Promise<DataRow[]> {
    this.exportRows = [];
    if (this.mapper.isListItemMapper) {
      const rows = this.mapper.fromObjects(this.$odata.items);
      this.exportRows = rows;
      return Promise.resolve(rows);
    } else {
      return this.getItems(this.exportLengthId, notify).then((items) => {
        return this.getRows(items, notify).then((rows) => {
          // this extra resolve is required for the post copy -paste call to succeed
          return Promise.resolve(rows);
        });
      });
    }

  }

  private getItems(exportLengthId: string, notify: (message?: string, index?: number, count?: number) => void = null): Promise<ApiListItem[]> {
    if (exportLengthId === ExportLengthId.Current) {
      return Promise.resolve(this.$odata.items);
    }
    const api: CrudApiBase<ApiCrudObject> = this.$odata.getApi();
    const items: ApiListItem[] = [];
    const pageSize = 50;
    const options: ODataQueryOptions = {
      $orderby: "updatedAt desc",
      $top: pageSize,
      $skip: 0,
    };
    const maxLength = this.getExportLength(exportLengthId).length;
    const next = (): Promise<void> => {
      return api.getOData(options).then((odataResult) => {
        items.push(...odataResult.items);
        if (notify) {
          notify("Lasketaan objekteja...", items.length - 1, 0);
        }
        if (!odataResult.items.length || (maxLength && maxLength === items.length)) {
          return Promise.resolve();
        }

        options.$skip = items.length;
        options.$top = !maxLength ? pageSize : (maxLength - items.length > pageSize ? pageSize : maxLength - items.length);

        return next();

      });
    };
    return next().then(() => {
      return items;
    });
  }
  private getRows(items: ApiListItem[], notify: (message?: string, index?: number, count?: number) => void = null): Promise<DataRow[]> {
    const api: CrudApiBase<ApiCrudObject> = this.$odata.getApi();
    const rows: DataRow[] = [];
    let index = 0;
    const next = (): Promise<void> => {
      if (index === items.length) {
        return Promise.resolve();
      }
      const item = items[index];
      const id = api.getBaseUrl() === new Workers(null).getBaseUrl() ? item.otherId : item.id;
      return api.getSingle(id)
        .then((apiResult) => {
          const mapper = (this.mapper as Mapper<ApiCrudObject, DataRow>);
          rows.push(...mapper.fromObjects([apiResult]));
          if (notify) {
            notify("Luodaan objekteille rivejä...", index, items.length);
          }
          index++;
          return next();
        });
    };
    return next().then(() => {
      return rows;
    });
  }

  private copyToClipboard(data: string): Promise<void> {
    return (navigator as any).clipboard.writeText(data).then( () => {
      return this.$timeout(() => {
        return;
      });
    });
  }

  private getFileName() {
    return `export_${Dates.getMoment(new Date()).format("YYYYMMDD")}`;
  }

}
