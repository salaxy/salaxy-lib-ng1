import * as angular from "angular";

import {
  ApiListItem,
  Calculation,
  Calculations,
  Employments,
  ODataQueryOptions,
  ODataResult,
  SharingUriType,
  Taxcards,
  TaxcardState,
  Workers,
} from "@salaxy/core";

import { CalculationCrudController } from "../calc/CalculationCrudController";

import {
  InvoicesService, OnboardingService, ReportsService, SessionService, UiHelpers,
} from "../../services";

/**
 * Controller for sharing the calculation between worker and employer.
 */
export class CalcSharingController extends CalculationCrudController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "ReportsService", "UiHelpers", "$location", "$routeParams", "Workers", "SessionService", "InvoicesService", "Employments", "OnboardingService", "Taxcards"];

  /** Employing relations for the current account */
  public employingRelations: ApiListItem[] = [];

  /** True, if the current link has been copied to the clipboard */
  public uriCopied = false;

  /** If true, template shows option to share direct link to the calculation. Currently used only in Rakennusliitto site. */
  public showLinkSharing: boolean;

  /** Web sharing targets */
  public webSharingTargets = {
    email: {
      title: "Sähköposti",
      icon: "fa-envelope-o",
      action: () => this.showEmailSharing(),
    },
    whatsApp: {
      title: "WhatsApp",
      icon: "fa-whatsapp",
      getUrl: () => this.getWhatsAppUrl(),
    },
    telegram: {
      title: "Telegram",
      icon: "fa-telegram",
      getUrl: () => this.getTelegramUrl(),
    },
  };

  /** Web sharing targets */
  public mobileSharingTargets = {
    email: {
      title: "Sähköposti",
      icon: "fa-envelope-o",
      action: () => this.showEmailSharing(),

    },
    device: {
      title: "Jaa laitteessasi",
      icon: "fa-share",
      action: () => this.shareInMobile(),
    },
  };

  /** Loading indicator. */
  public isSharingType: string = null;

  /**
   * Creates a new CalcSharingController
   * @ignore
   */
  constructor(
    private sharingCalculationsApi: Calculations,
    reportsService: ReportsService,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    workersApi: Workers,
    sessionService: SessionService,
    invoicesService: InvoicesService,
    private employmentsApi: Employments,
    private onboardingService: OnboardingService,
    private taxcards: Taxcards,
  ) {
    super(sharingCalculationsApi, reportsService, uiHelpers, $location, $routeParams, workersApi, sessionService, invoicesService);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    super.$onInit();
    this.loadEmployingRelations();
  }

  /** Show sharing page for the current calculation. */
  public showSharing(action: string = null): Promise<angular.ILocationService> {
    const savePending = (): Promise<Calculation> => {
      if ((!action || action === "default")) {
        if (!this.current.isReadOnly) {
          return this.save();
        }
      }
      return Promise.resolve(this.current);
    };
    return savePending().then((calculation: Calculation) => {
      return this.$location.url(`/calc/sharing/${calculation.id}${action ? "/" + action : ""}`);
    });
  }

  /**
   * Shares the current calculation.
   * @param type Type of the sharing action or link.
   * @param employerId Id of the employer to share the calculation with. Mandatiry in Direct share.
   * @returns Sharing object of the calculation.
   */
  public share(type: SharingUriType = SharingUriType.Undefined, employerId: string = null): Promise<angular.ILocationService> {

    if (type !== SharingUriType.Undefined) {
      if (!this.sessionService.checkAccountVerification()) {
        this.showMissingSignatureAlert();
        return Promise.resolve(null);
      }
    }

    const checkTaxcard = (): Promise<boolean> => {
      if (type !== SharingUriType.Url) {
        return Promise.resolve(true);
      } else {
        return this.taxcards.getMyTaxcards().then((currentTaxcards) => {
          return currentTaxcards.active != null && currentTaxcards.active.card.state === TaxcardState.Shared;
        });
      }
    };

    this.isSharingType = `${type}${employerId ? employerId : ""}`;
    return checkTaxcard().then((checkTaxcardResult) => {
      if (!checkTaxcardResult) {
        this.isSharingType = null;
        this.showMissingTaxcardAlert();
        return Promise.resolve(null);
      }

      return this.sharingCalculationsApi.share(this.currentId, type, employerId).then(() => {
        return this.reload().then(() => {
          this.isSharingType = null;
          return this.showDetails(this.current);
        });
      });
    });
  }

  /**
   * Loads employing relations of the existing employers for the current account.
   * @param options OData query options.
   */
  public loadEmployingRelations(options: ODataQueryOptions = null): Promise<ApiListItem[]> {
    return this.employmentsApi.getODataForEmployingRelations(options).then((result: ODataResult<ApiListItem>) => {

      // filter out duplicates, default sort is updatedAt desc
      const distinctItems: ApiListItem[] = [];
      result.items.forEach((x) => {
        if (distinctItems.findIndex((d) => d.ownerId === x.ownerId) === -1) {
          distinctItems.push(x);
        }
      });
      this.employingRelations = distinctItems;
      return this.employingRelations;
    });
  }

  /**
   * Returns validation for current calculation or empty validation object if the calculation has not been recalculated (validated).
   */
  public get validation() {
    return this.current?.result?.validation || { errors: [], hasAllRequiredFields: true, isValid: true };
  }

  /** Returns the employer for the given id using employing employers as a source */
  public get employer(): any {
    if (this.current && this.current.sharing.type === SharingUriType.Employer && this.employingRelations) {
      return this.employingRelations.find((x) => x.ownerId === (this.current.sharing.uri));
    }
    return null;
  }

  /** Returns email address for current accoount */
  public get cc(): string {
    if (this.sessionService.session) {
      return this.sessionService.session.currentAccount.contact.email;
    }
    return null;
  }

  /** Copies link to clipboard */
  public copyUri() {
    (navigator as any).clipboard.writeText(this.current.sharing.uri);
    this.uriCopied = true;
  }

  /** Returns true if the given typing is just processing sharing */
  public isSharing(type: string): boolean {
    return type === this.isSharingType;
  }

  /** Returns true if the navigator.share -function exists. */
  public get isMobileSharingEnabled() {
    return !!(navigator as any).share;
  }

  /** Whatsapp-link */
  private getWhatsAppUrl(): string {
    if (!this.current) {
      return "";
    }
    return `https://wa.me/?text=${encodeURIComponent(this.getSharingMessage() + "\n" + this.current.sharing.uri)}`;
  }

  /** Telegram-link */
  private getTelegramUrl(): string {
    if (!this.current) {
      return "";
    }
    return `https://telegram.me/share/url?url=${encodeURIComponent(this.current.sharing.uri)}&text=${encodeURIComponent(this.getSharingMessage())}`;
  }

  /** Mobile sharing */
  private shareInMobile(): Promise<any> {
    const data = {
      url: this.current.sharing.uri,
      title: this.getSharingTitle(),
      text: this.getSharingMessage(),
    };
    return (navigator as any).share(data);
  }

  private showEmailSharing(): void {

    const item = {
      to: "",
      cc: this.cc,
      message: "",
    };
    this.uiHelpers.openEditDialog("salaxy-components/modals/calc/EmailSharing.html", item, null)
      .then((dialog) => {
        if (dialog.result === "ok") {
          this.isSharingType = "notification";
          this.sharingCalculationsApi.notifySharing(this.currentId, item.to, item.cc, item.message).then((result) => {
            this.isSharingType = null;
            if (result === true) {
              this.uiHelpers.showAlert("Sähköpostiviesti on nyt lähetetty onnistuneesti työnantajalle.");
            } else {
              this.uiHelpers.showAlert("Sähköpostiviesti on nyt lähetetty työnantajalle.");
            }
          });
        }
      });
  }

  private getSharingMessage(): string {
    return "Olen lähettänyt sinulle palkkalaskelman. \nKirjaudu sisään palveluun linkin kautta!";
  }

  private getSharingTitle(): string {
    return "Uusi palkkalaskelma";
  }

  /**
   * Shows an alert if the signature is missing.
   */
  private showMissingSignatureAlert() {
    this.uiHelpers.showConfirm("Valtakirjan allekirjoitus puuttuu",
      "Ennen kuin jaat palkkalaskelman, sinun täytyy viedä tilin luonti loppuun asti\n" +
      "ja allekirjoittaa valtakirja.\n\n" +
      "Jatka allekirjoitukseen (vaatii pankkitunnukset)?",
      "Jatka", "Sulje").then((ok: boolean) => {
        if (ok) {
          this.onboardingService.launchWorkerOnboarding();
        }
      });
  }

  /**
   * Shows an alert if the taxcard is missing.
   */
  private showMissingTaxcardAlert() {
    this.uiHelpers.showAlert("Jaettu verokortti puuttuu", "Laskelman linkkiä ei voi lähettää ilman jaettua verokorttia. Lisää jaettu verokortti.");
  }

}
