﻿import * as angular from "angular";

import { Configs, EmploymentContract, EmploymentContracts } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * Provides read access Employment contracts.
 * Currently only available for private person (Household / Worker)
 * New Employment Contract that supports Companies is in roadmap.
 */
export class EmploymentContractCrudController extends ApiCrudObjectController<EmploymentContract> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "EmploymentContracts",
    "UiHelpers",
    "$location",
    "$routeParams",
  ];

  constructor(
    private contractsApi: EmploymentContracts,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
  ) {
    super(contractsApi, uiHelpers, $location, $routeParams);
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "employment-contracts",
      detailsUrl: this.detailsUrl || "/employment-contracts/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/EmploymentContracts.html",
      oDataOptions: {},
    };
  }

  /**
   * Gets the link for HTML preview, pater may be switched to PDF link.
   * @param item Item for which to fetch the link.
   * @param item.id Identifier of the item.
   */
  public getPreviewLink(item: { id: string }) {
    return `${Configs.current.wwwServer}/Employment/PreviewPlain/${item.id}`
  }
}
