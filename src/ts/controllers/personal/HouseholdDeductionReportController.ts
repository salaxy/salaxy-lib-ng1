import * as angular from "angular";

import { Ajax, Dates } from "@salaxy/core";

/**
 * Employer report.
 */
export class HouseholdDeductionReportController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  // TODO: Implement an algorithm that craetes the year options, so that it is always up to date.
  /** Year options for the year select control. */
  public yearOptions = [
    { value: "2021", name: "2021" },
    { value: "2020", name: "2020" },
    { value: "2019", name: "2019" },
    { value: "2018", name: "2018" },
    { value: "2017", name: "2017" },
    { value: "2016", name: "2016" },
    { value: "2015", name: "2015" },
    { value: "2014", name: "2014" },
  ];

  private _currentReport: any = null;
  private _selectedYear: any = null;

  private _yearlyDeductionMaxAmounts = {
    "2021": 2250,
    "2020": 2250,
    "2019": 2400,
    "2018": 2400,
    "2017": 2400,
    "2016": 2400,
    "2015": 2400,
    "2014": 2400,
  };

  constructor(private ajax: Ajax) { }

  /**
   * Controller initializations
   */
  public $onInit = () => {
    this.refreshData();
  }

  /**
   * Returns the currently loaded report data.
   */
  public get currentReport(): any {
    return this._currentReport
  }

  /**
   * Returns the currently selected year. Defaults to current year minus one.
   */
  public get selectedYear(): any {
    if (!this._selectedYear) {
      // Defaults to current year (given that the yearOptions is up to date)
      this._selectedYear = this.yearOptions[0];
    }
    return this._selectedYear;
  }

  /**
   * Sets the currently selected year and reloads the data.
   */
  public set selectedYear(year) {
    this._selectedYear = year;
    this.refreshData();
  }

  /**
   * Returns the employer id from the current report data if available otherwise undefined.
   */
  public get employerId(): string {
    return this._currentReport?.employerReports?.[0]?.employerId;
  }

  /**
   * Returns the total deduction amount from a given employer report.
   *
   * @param employerReport The employer report for which the amount is calculated for.
   * @returns The total deduction amount from a given employer report.
   */
  public employerDeductionsTotalAmount(employerReport: any): string {
    return this.formatNumber(
      employerReport.totalTaxableDeduction +
      employerReport.totalMandatorySideCosts +
      employerReport.deductionInsuranceEtc
    );
  }

  /**
   * Returns the total deduction amount for a 14B form of a given year end report.
   */
  public yearlyForm14BTotalAmount(yearlyReport: any): string {
    return this.formatNumber(
      yearlyReport.employerReports
        .reduce((acc, cur) => acc + cur.deduction14BFormTotal, 0)
    );
  }

  /**
   * Returns the total house hold deduction amount of a given year end report.
   *
   * @param yearlyReport The year end report for which the amount is calculated for.
   */
  public yearlyDeductionsTotalAmount(yearlyReport: any): string {
    return this.formatNumber(this.calculateYearlyDeductionsTotalAmount(yearlyReport));
  }

  /**
   * Returns the total deduction amount for a given reports with maximum and deductible amount taken
   * into account.
   *
   * @param yearlyReport The year end report for which the amount is calculated for.
   * @param deductibleAmount The deductible amount (can change in the future?)
   * @returns The total deduction amount with maximum and deductible amount taken into account.
   */
  public yearlyDeductionTotal(yearlyReport: any, deductibleAmount = 100): string {
    return this.formatNumber(
      Math.min(
        Math.max(
          this.calculateYearlyDeductionsTotalAmount(yearlyReport) - deductibleAmount,
          0),
        this.deductionMaxAmount,
      )
    );
  }

  /**
   * Returns...
   */
  public get deductionMaxAmount(): number {
    return this._yearlyDeductionMaxAmounts[this.selectedYear.value];
  }

  /**
   * Return the given amount formatted as a currency string.
   *
   * @param amount The amount to be formatted.
   * @returns The amount formatted as currency.
   */
  public formatNumber(amount: number): string {
    return new Intl.NumberFormat(
      "fi-FI",
      {
        maximumFractionDigits: 2,
        minimumFractionDigits: 2,
      }
    ).format(amount);
  }

  /**
   * Formats the given start and end datetimes to an optimized datarange string.
   *
   * @param start The start datetime of the date range.
   * @param end The end datetime of the date range.
   * @returns Returns the daterange as an optimized date range string.
   */
  public formatDateRange(start: string, end: string): string {
    return Dates.getFormattedRange(start, end);
  }

  /**
   * Returns an avatar.
   *
   * @param url Avatart url.
   * @returns Avatar.
   */
  public avatarFrom(url: string): any {
    if (url.toLowerCase().startsWith("/person/icon/")) {
      return this.avatarDataFromIcon(url);
    }
    if (url.toLowerCase().startsWith("http")) {
      return this.avatarDataFromUrl(url);
    }
    return {
      color: "pink",
      initials: "#?"
    };
  }

  private calculateYearlyDeductionsTotalAmount(yearlyReport: any): number {
    return yearlyReport.employerReports.reduce((acc, cur) => {
      return acc +
        cur.totalTaxableDeduction +
        cur.totalMandatorySideCosts +
        cur.deductionInsuranceEtc;
    }, 0);
  }

  private avatarDataFromIcon(url: string) {
    const iconComponents = url.substring(url.lastIndexOf("/") + 1);
    const initials = iconComponents.substring(0, iconComponents.indexOf("?color=")) || "#?";
    const color = iconComponents.substring(iconComponents.indexOf("?color=") + "?color=".length) || "pink";
    const baseIcon = "fa-user";
    return {
      initials,
      color,
      baseIcon,
    };
  }

  private avatarDataFromUrl(url: string): any {
    return {
      url: url.trim().replace("https://az724081.vo.msecnd.net", "https://cdn.salaxy.com"),
    };
  }

  /**
   * Returns the worker email address from a report object otherwise null or undefined.
   *
   * @param report The report object to search for worker email.
   * @returns Returns the email address or null/undefined.
   */
  public workerEmailFrom(report: any): string {
    return report?.worker.contact.email || report?.worker.identity.email;
  }

  private refreshData() {
    this.ajax.getJSON(`/v03-rc/api/yearly/tax-household-deduction-report/${this.selectedYear.value}`).then((data) => {
      this._currentReport = data;
    });
  }
}