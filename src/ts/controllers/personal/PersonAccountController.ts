import * as angular from "angular";

import { Accounts, Avatar, AvatarPictureType, MessageType, PersonAccount, SystemRole, Years } from "@salaxy/core";

import { GravatarHelpers, SessionService, UiHelpers, UploadService } from "../../services";

/** Controller for editing contact, avatar etc. information on Person accounts (Worker or Household). */
export class PersonAccountController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Accounts", "SessionService", "UploadService", "UiHelpers", "$location"];

  /** Yearly sidecosts */
  public sideCosts = Years.getYearlyChangingNumbers(new Date()).sideCosts;

  /** Current person account being edited. */
  public current: PersonAccount;

  /** Current tab in initialization. If not set, will be fetched from url hash. */
  private _currentTab = null;

  /** Constructor for dependency injection */
  constructor(
    private accounts: Accounts,
    private sessionService: SessionService,
    private uploadService: UploadService,
    private uiHelpers: UiHelpers,
    private $location: angular.ILocationService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    if (!this.current && this.sessionService.getCurrentToken()) {
      this.accounts.getPerson().then((person) => {
        this.current = person;
      });
    }
  }

  /**
   * Switches the current web site usage role between household and worker.
   * TODO: This functionality will be removed
   * @param role - household or worker.
   * @returns A Promise with result data (new role as string)
   */
   public switchRole(role: "worker" | "household"): Promise<"household" | "worker"> {
    return this.sessionService.switchRole(role).then((resultRole) => {
      if (role === "worker") {
        window.location.href = "/Worker#/";
      } else {
        window.location.href = "/Household#/";
      }
      return resultRole;
    });
  }

  /**
   * Uploads avatar image file to the server.
   * @param avatar - selected file
   */
  public uploadAvatarImage(avatar: any) {
    if (!avatar) {
      return;
    }
    this.uploadService.upload<Avatar>(this.accounts.getAvatarUploadUrl(), { avatar })
      .then((resp) => {
        this.current.avatar.url = resp.url;
        this.current.avatar.pictureType = resp.pictureType;
      });
  }

  /** Updates the person */
  public saveCurrent(): Promise<PersonAccount> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return this.accounts.savePerson(this.current).then((person: PersonAccount) => {
      this.current = person;
      return this.sessionService.checkSession().then(() => {
        loading.dismiss();
        return this.current;
      });
    });
  }

  /** Request contact verification code. */
  public requestContactVerificationPin(): Promise<string> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return this.accounts.sendVerification(MessageType.Sms, this.current.contact.telephone).then((result) => {
      if (this.sessionService.isInRole(SystemRole.Test)) {
        (this.current as any).contactVerificationPin = result;
      }
      loading.dismiss();
      this.uiHelpers.showAlert("Koodi lähetetty", "Vahvistuskoodi on lähetetty antamaasi numeroon.");
      return result;
    });
  }

  /** Returns true if the telephone number changes   */
  public get isContactVerificationPinRequired(): boolean {
    if (!this.current || !this.sessionService.getPersonAccount()) {
      return false;
    }
    return !!this.current.contact.telephone && this.sessionService.getPersonAccount().contact.telephone !== this.current.contact.telephone;
  }

  /** Called when avatar changes */
  public emailForAvatarChanged() {
    if (!this.current || !this.current.avatar) {
      return;
    }
    if (this.current.avatar.pictureType === AvatarPictureType.Gravatar) {
      this.current.avatar.url = GravatarHelpers.getGravatarUrl(this.current.contact.email);
    }
  }
  /** Called when avatar changes */
  public typeForAvatarChanged() {
    if (!this.current || !this.current.avatar) {
      return;
    }
    if (this.current.avatar.pictureType === AvatarPictureType.Gravatar) {
      this.current.avatar.url = GravatarHelpers.getGravatarUrl(this.current.contact.email);
    } else {
      if (GravatarHelpers.isGravatarUrl(this.current.avatar.url)) {
        this.current.avatar.url = null;
      }
      if (this.current.avatar.pictureType === AvatarPictureType.Uploaded) {
        const originalAvatar = (this.sessionService.getPersonAccount() || {}).avatar;
        if (!this.current.avatar.url && originalAvatar && originalAvatar.url) {
          if (originalAvatar.pictureType === AvatarPictureType.Uploaded &&
            !GravatarHelpers.isGravatarUrl(originalAvatar.url)) {
            this.current.avatar.url = originalAvatar.url;
          }
        }
      }
    }
  }

  /**
   * For the tab UI control, returns the current tab, if defined in the path
   * or set explicitly. Defaults to "default".
   */

  public get currentTab(): "default" | "contact" | "avatar" | "insurance" | "identity" | "taxcard"  {
    if (this._currentTab) {
      return this._currentTab;
    }
    const supportedHashes = ["default", "contact", "avatar", "insurance", "identity",  "taxcard"];
    const hash = (this.$location ? this.$location.hash() || "" : "").trim().toLowerCase();
    return supportedHashes.indexOf(hash) > -1 ? hash as any : "default";
  }
  public set currentTab(value: "default" | "contact" | "avatar" | "insurance" | "identity" | "taxcard"  ) {
    this._currentTab = value;
  }

}
