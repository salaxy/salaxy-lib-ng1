import * as angular from "angular";

import { Ajax } from "@salaxy/core";

/**
 * Employer report.
 */
export class WorkerReportController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  // TODO: Implement an algorithm that craetes the year options, so that it is always up to date.
  /** Year options for the year select control. */
  public yearOptions = [
    { value: "2021", name: "2021" },
    { value: "2020", name: "2020" },
    { value: "2019", name: "2019" },
    { value: "2018", name: "2018" },
    { value: "2017", name: "2017" },
    { value: "2016", name: "2016" },
    { value: "2015", name: "2015" },
    { value: "2014", name: "2014" },
  ];

  private _currentReport: any = null;
  private _selectedYear: any = null;

  constructor(private ajax: Ajax) { }

  /**
   * Controller initializations
   */
  public $onInit = () => {
    this.refreshData();
  }

  /**
   * Returns the currently loaded report data.
   */
  public get currentReport(): any {
    return this._currentReport
  }

  /**
   * Returns the currently selected reporting year.
   */
  public get selectedYear(): any {
    if (!this._selectedYear) {
      // Defaults to current year (given that the yearOptions is up to date)
      this._selectedYear = this.yearOptions[0];
    }
    return this._selectedYear;
  }

  /**
   * Sets the currently selected reporting year and reloads the data.
   */
  public set selectedYear(year) {
    this._selectedYear = year;
    this.refreshData();
  }

  /**
   * Returns the worker email address from a report object otherwise null or undefined.
   *
   * @param report The report object to search for worker email.
   * @returns Returns the email address or null/undefined.
   */
  public employerEmailFrom(report: any): string {
    return report?.employer.contact.email || report?.employer.identity.email;
  }

  /**
   * Checks if the selected reporting year is before incomes registry was established and/or
   * implemented.
   *
   * @returns Returns true if selected year is before and false otherwise.
   */
  public get isSelectedYearBeforeIncomesRegistry(): boolean {
    return this.selectedYear.value < 2019;
  }

  /**
   * Returns avatar data based on the given url.
   *
   * @param url Avatar url.
   * @returns Avatar data.
   */
  public avatarFrom(url: string): any {
    if (url.toLowerCase().startsWith("/person/icon/")) {
      return this.avatarDataFromIcon(url);
    }
    if (url.toLowerCase().startsWith("http")) {
      return this.avatarDataFromUrl(url);
    }
    return {
      color: "pink",
      initials: "#?"
    };
  }

  /**
   * Returns the authorization token for the current user.
   */
  public get token(): string {
    return this.ajax.getCurrentToken();
  }

  /**
   * Returns the api server address.
   */
  public get apiServerAddress(): string {
    return this.ajax.getServerAddress();
  }

  private avatarDataFromIcon(url: string) {
    const iconComponents = url.substring(url.lastIndexOf("/") + 1);
    const initials = iconComponents.substring(0, iconComponents.indexOf("?color=")) || "#?";
    const color = iconComponents.substring(iconComponents.indexOf("?color=") + "?color=".length) || "pink";
    const baseIcon = "fa-user";
    return {
      initials,
      color,
      baseIcon,
    };
  }

  private avatarDataFromUrl(url: string): any {
    return {
      url: url.trim().replace("https://az724081.vo.msecnd.net", "https://cdn.salaxy.com"),
    };
  }

  private refreshData() {
    this.ajax.getJSON(`/v03-rc/api/yearly/worker-report/${this.selectedYear.value}`).then((data) => {
      this._currentReport = data;
    });
  }
}