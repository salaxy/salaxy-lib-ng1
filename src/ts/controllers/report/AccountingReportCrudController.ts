﻿import * as angular from "angular";

import { AccountingData, BlobFile, Dates, Files, Reports } from "@salaxy/core";

import { UiHelpers } from "../../services";

import { ApiCrudObjectController } from "../bases";

/**
 * The new ApiCrudObject type of CRUD controller for the BlobFile of the accounting report.
 */
export class AccountingReportCrudController extends ApiCrudObjectController<BlobFile> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Files",
    "Reports",
    "UiHelpers",
    "$location",
    "$routeParams",
    "$timeout",
  ];

   /** Default query options for panel */
   public panelQueryOptions = {
    $filter: "entityType eq 'MonthlyReport.MonthlyAccounting'",
    $orderby: "logicalDate desc",
    $top: 5,
  };

  /** Data reader for selected items. */
  public selectionDataReader = {
    /** Function to read data into given array */
    read: (target: string, ruleSet: string, arr: AccountingData[], message: string): Promise<void> => {
      const items = this.odataController.selectedItems;
      let counter = 0;
      const next = (): Promise<void> => {
        if (counter === items.length) {
          return Promise.resolve();
        }
        const item = items[counter++];
        return this.reportsApi.getAccountingData(item.id, target, ruleSet).then((data) => {
          const wfEvent = {
            type: "PartnerMessageClosed",
            ui: "success" as any,
            message,
          };
          return this.filesApi.saveWorkflowEvent(item, wfEvent).then(() => {
              this.setWorkflowEvent(item, wfEvent);
              if (data) {
                arr.push(data);
              }
              return next();
            });
        });
      };
      return next().then( () => {
        return Promise.resolve();
      });
    },
    /** Optional label for data export */
    exportLabel: "Luo aineisto valituista",
    /** Indicates if the export is not possible */
    disabled: () => !this.odataController.selectedItems || this.odataController.selectedItems.length === 0,
  };

  /** Data reader for the current loaded data. */
  public currentDataReader = {
    /** Function to read data into given array */
    read: (target: string, ruleSet: string, arr: AccountingData[], message: string): Promise<void> => {
      if (this.currentData) {
        const wfEvent = {
          type: "PartnerMessageClosed",
          ui: "success" as any,
          message,
        };
        return this.filesApi.saveWorkflowEvent(this.current, wfEvent).then(() => {
            arr.push(this.currentData);
            return this.reload().then(() => {
              return Promise.resolve();
            });
          });
      } else {
        return Promise.resolve();
      }
    },
    /** Optional label for data export */
    exportLabel: null,
    /** Indicates if the export is not possible */
    disabled: () => !this.currentData,
  };

  /** Status for current data . */
  private currentDataStatus: "initial" | "loading" | string = "initial";

  private _currentData: AccountingData;

  private logicalDate: string;

  constructor(
    private filesApi: Files,
    private reportsApi: Reports,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private $timeout: angular.ITimeoutService,

  ) { // Dependency injection
    super(filesApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Initialization code.
   */
  public $onInit() {
    super.$onInit();
    const now = Dates.getTodayMoment();
    if (now.month() === 0) {
      this.logicalDate = now.add(-1, "year").format("YYYY-01-01");
    } else {
      this.logicalDate = now.format("YYYY-01-01");
    }
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/accounting",
      detailsUrl: this.detailsUrl || "/accounting/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/ApiCrudObject.html",
      oDataOptions: {},
    };
  }

  /** Returns OData filter. */
  public getODataFilter = (): string => {
    const logicalDateExcluding = Dates.getMoment(this.logicalDate).add(1, "year").format("YYYY-01-01");
    return `entityType eq 'MonthlyReport.MonthlyAccounting' and logicalDate ge ${this.logicalDate} and logicalDate lt ${logicalDateExcluding}`;
  }

  /** Data reader for one item. */
  public getItemDataReader = (item: {
    /** List item id */
    id: string,
    /** List item owner */
    owner: string,
  }) => {
    return {
      /** Function to read data into given array */
      read: (target: string, ruleSet: string, arr: AccountingData[], message: string): Promise<void> => {
        (item as any).isReading = true;
        return this.reportsApi.getAccountingData(item.id, target, ruleSet).then((data) => {
          const wfEvent = {
            type: "PartnerMessageClosed",
            ui: "success" as any,
            message,
          };
          return this.filesApi.saveWorkflowEvent(item, wfEvent).then(() => {
            this.setWorkflowEvent(item as any, wfEvent);
            if (data) {
              arr.push(data);
            }
            (item as any).isReading = false;
            return Promise.resolve();
          });
        });
      },
      /** Optional label for data export */
      exportLabel: null,
     /** Indicates if the export is not possible */
     disabled: () => false,
    };
  }

  /** Event handler for logical date. */
  public logicalDateChanged() {
    if (this.odataController.selectedItems) {
      this.odataController.selectedItems.splice(0, this.odataController.selectedItems.length);
    }
    this.odataController.options.$filter = this.getODataFilter();
    this.odataController.refresh();
  }

  /** Unselect all */
  public unselectAll() {
    if (this.odataController.selectedItems) {
      this.odataController.selectedItems.splice(0, this.odataController.selectedItems.length);
    }
  }

  /** Select all */
  public selectAll() {
    if (this.odataController.selectedItems && this.odataController.items) {
      this.odataController.selectedItems.splice(0, this.odataController.selectedItems.length);
      this.odataController.selectedItems.push(...this.odataController.items);
    }
  }

  /** Accounting data of the current item. */
  public get currentData() {
    if (this.currentId) {
      this.loadCurrentData();
    } else {
      // No id, reset data.
      this._currentData = null;
      this.currentDataStatus = "initial";
    }
    return this._currentData;
  }

  /** Load current data */
  public loadCurrentData(targetId: string = null) {
    if (targetId == null) {
      if (this.currentDataStatus === "initial") {
        targetId = "default";
      } else if  (this.currentDataStatus === "loading") {
        return;
      } else {
        targetId = this.currentDataStatus.split(".")[1];
      }
    }
    const currentLoadingId = `${this.currentId}.${targetId}`;
    if (this.currentDataStatus !== currentLoadingId && this.currentDataStatus !== "loading") {
      // Start downloading for the new id
      this.currentDataStatus = "loading";
      const loading = this.uiHelpers.showLoading("Odota...");
      this.reportsApi.getAccountingData(this.currentId, targetId, null).then((data) => {
        if (data) {
          this._currentData = data;
        }
        this.currentDataStatus = currentLoadingId;
        loading.dismiss();
      });
    }
  }

  private setWorkflowEvent(
    item: {
      /** List item id */
      flags: string[],
      /** List item owner */
      messages: string[],
    },
    wfEvent: {
      /** workflow type */
      type: string,
      /** ui type */
      ui: string,
      /** message */
      message: string,
    }) {
      item.flags = (item.flags || []).filter( (x) => x !== wfEvent.type);
      item.messages = (item.messages || []).filter( (x) => !x.startsWith(wfEvent.type));

      item.flags.push(wfEvent.type);
      item.messages.push(`${wfEvent.type} (Ui${wfEvent.ui}): ${wfEvent.message} ()`);
  }
}
