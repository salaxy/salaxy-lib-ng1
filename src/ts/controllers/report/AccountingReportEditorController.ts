import * as angular from "angular";

import { AccountingSettings, CompanyAccountSettings } from "@salaxy/core";

import { SettingsService, UiHelpers } from "../../services";

/**
 * Provides functionality for editing chart of accounts and accounting rules.
 */
export class AccountingReportEditorController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SettingsService", "$timeout", "UiHelpers"];

  /** Current settings */
  public settings: AccountingSettings = null;
  private _targets  = [];
  constructor(
    private settingsService: SettingsService,
    private $timeout: angular.ITimeoutService,
    private uiHelpers: UiHelpers,
  ) {
    // Dependency injection
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {
    const getSettings = () : Promise<CompanyAccountSettings> => {
      return new Promise( (resolve) => {
        const check = () => {
          if (this.settingsService.current) {
            return resolve(this.settingsService.current);
          }
          this.$timeout( () => check(), 50);
        }

        check();
      });
    }
    getSettings().then( (companySettings) => {
      this.settings = companySettings.accounting;
    });
  }

  /** Returns targets */
  public get targets() {
    if (this.settings && this.settings.targets) {
      const newTargets = this.settings.targets.map( (x) => ({ value: x.id, text: x.avatar.displayName}));
      this.modifyOptions(newTargets, this._targets);
    }
    return this._targets;
  }


  /** Saves settings */
  public saveSettings() {
    const loading = this.uiHelpers.showLoading("Odota...");
    this.settingsService.save().then( () => {
      loading.dismiss();
    });
  }

  private modifyOptions(
    source: ({
      /** Enum value. */
      value: any,
      /** Label for the value. */
      text: string,
      /** Description for the value. */
      title?: string,
    })[],
    target: ({
      /** Enum value. */
      value: any,
      /** Label for the value. */
      text: string,
      /** Description for the value. */
      title?: string,
    })[]) {
    // check if source values differ from target
    const ok =  angular.equals(source, target);

    if (ok) {
      return;
    }
    // remove values
    target.splice(0, target.length);

   // add values
    target.push(...source);
 }
}
