import * as angular from "angular";

import { AccountingData, Dates, PeriodDateKind, PeriodType, Reports } from "@salaxy/core";
import { UiHelpers } from "../../services";

/**
 * Provides functionality for building ad hoc accounting reports.
 */
export class AccountingReportQueryController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Reports",
    "UiHelpers",
  ];

  /** Query type */
  public queryType: "periodQuery" | "selectionQuery" = "periodQuery";

  /** Period type for the query. */
  public periodType: PeriodType = PeriodType.Month;

  /** Period date kind for the query. */
  public periodDateKind: PeriodDateKind = PeriodDateKind.PaidAtDate;

  /** Ref date for the period. */
  public refDate?: string;

  /** End date for the custom period. */
  public endDate?: string;

  /** Accounting data */
  public data: AccountingData;

  /** Calculations. */
  public calculations: any[] = [];

  /** Data reader for the control. */
  public dataReader = {
    /**
     * Function to read data into given array
     * TODO: Should make interface for this or otherwise go-through.
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    read: (target: string, ruleSet: string, arr: AccountingData[], message: string): Promise<void> => {
      if (this.data) {
        arr.push(this.data);
      }
      return Promise.resolve();
    },
    /** Optional label for data export */
    exportLabel: null,
    /** Indicates if the export is not possible */
    disabled: () => !this.data,
  };

  constructor(
    private reportsApi: Reports,
    private uiHelpers: UiHelpers,
  ) {
    // Dependency injection
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {
    this.refDate = Dates.getTodayMoment().add(-1, "months").format("YYYY-MM-01");
  }

  /**
   * Opens the list of calculations into a dialog window for selection and then adds them to the selected calculations
   * @param category Either "paid" or "draft".
   */
  public addCalcs(category: "paid" | "draft" = "draft") {
    const selectedCalculations = [];
    this.uiHelpers.openEditDialog(
      "salaxy-components/modals/calc/CalcList.html",
      selectedCalculations,
      {
        title: (category === "paid" ? "Valitse maksetut palkat" : "Valitse luonnokset"),
        category,
      }).then((result) => {
        if (result.action === "ok" && result.item.length > 0) {
          for (const selectedCalculation of result.item) {
            if (!this.calculations.some((x) => x.id === selectedCalculation.id)) {
              this.calculations.push(selectedCalculation);
            }
          }
        }
      });
  }

  /** Removes the selected calculation from the calculations list. */
  public removeCalc(calculation: any) {
    this.calculations = this.calculations.filter((x) => x.id !== calculation.id);
  }

  /** Queries the report data. */
  public queryData(target = "default") {
    if (this.queryType === "periodQuery") {
      const loading = this.uiHelpers.showLoading("Odota...");
      this.reportsApi.getAccountingDataForPeriod(this.refDate, target, this.periodType, this.endDate, this.periodDateKind)
        .then((result) => {
          this.data = result;
          loading.dismiss();
        });
    } else if (this.queryType === "selectionQuery") {
      const loading = this.uiHelpers.showLoading("Odota...");
      const calcIds = this.calculations.map((x) => x.id);
      this.reportsApi.getAccountingDataForCalculationIds(calcIds, target)
        .then((result) => {
          this.data = result;
          loading.dismiss();
        });
    }
  }
}
