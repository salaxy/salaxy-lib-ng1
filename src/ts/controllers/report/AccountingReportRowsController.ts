import * as angular from "angular";

import { AccountingData } from "@salaxy/core";

/**
 * Provides functionality to show report rows.
 */
export class AccountingReportRowsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
  ];

  /** Accounting data */
  public data: AccountingData;

  constructor(
  ) {
    // Dependency injection
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {
    // empty
  }

  /** Expand/close all nodes */
  public expand(close = false) {
    if (this.data) {
      for (const ledgerAccount of this.data.ledgerAccounts) {
        (ledgerAccount as any).rowToggle = !close;
      }
    }
  }

  /** Returns true if at least one account is toggled */
  public get anyExpanded(): boolean {
    if (this.data) {
      for (const ledgerAccount of this.data.ledgerAccounts) {
        if ((ledgerAccount as any).rowToggle) {
          return true;
        }
      }
    }
    return false;
  }
}
