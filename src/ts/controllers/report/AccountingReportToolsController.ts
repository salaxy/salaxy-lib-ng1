import * as angular from "angular";
import JSZip from "jszip";
import { saveAs } from "file-saver";

import { Texts } from "@salaxy/core";

import { AccountingData, CompanyAccountSettings, Dates, LedgerEntry } from "@salaxy/core";
import { ExcelHelpers } from "../../services";
import { SettingsService, UiHelpers } from "../../services";

/** Locale id */
enum LocaleId {
  Fi = "fi",
  En = "en",
}

/** File tupe id */
enum FileTypeId {
  ExcelFile = "excelFile",
  CsvFile = "csvFile",
  ExcelCopy = "excelCopy",
  CsvCopy = "csvCopy",
}

/** Scheme id */
enum SchemeId {
  Default = "default",
  Extended = "extended",
}

/** TargetOption */
interface TargetOptions {
  [key: string]: TargetOption;
}

/** TargetOption */
interface TargetOption {
  /** Export function */
  getExportData?: (accountingData: AccountingData) => any[];
  /** Boolean indicating if the export has default and extended schemes */
  hasSchemes?: boolean;
  /** Default file type */
  defaultFileTypeId?: string;
  /** Boolean indicating if the csv export should contain quoted strings */
  noCsvQuotedStrings?: boolean;
}

/**
 * Provides functionality to export accounting data in various formats.
 */
export class AccountingReportToolsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "UiHelpers",
    "$timeout",
    "SettingsService",
  ];

  /** Locale for exports */
  public localeId: string = LocaleId.Fi;

  /** Supported locales for exports */
  public locales: {
    /** Locale id */
    id: string,
    /** Locale label */
    label: string,
    /** Local description */
    description: string,
  }[] = [
      {
        id: LocaleId.Fi,
        label: "Suomalainen",
        description: "Suomalainen (12,34 / 1.2.2020)",
      },
      {
        id: LocaleId.En,
        label: "Kansainvälinen",
        description: "Kansainvälinen (12.34 / 2020-02-01)",
      },
    ];

  /** Size of the data export */
  public schemeId: string = SchemeId.Default;

  /** Supported sizes for data exports */
  public schemes: {
    /** Scheme id */
    id: string,
    /** Scheme label */
    label: string,
  }[] = [
      {
        id: SchemeId.Default,
        label: "Vakioaineisto",
      },
      {
        id: SchemeId.Extended,
        label: "Laaja aineisto",
      },
    ];

  /** Export format */
  public fileTypeId: string = FileTypeId.ExcelFile;

  /** Supported export formats */
  public fileTypes: {
    /** File type id */
    id: string,
    /** File type label */
    label: string,
    /** Boolean indicating if the file type supports different locales */
    hasLocales: boolean,
    /** Workflow message after export */
    workflowMessage,
  }[] = [
      {
        id: FileTypeId.ExcelFile,
        label: "Excel-tiedosto",
        hasLocales: false,
        workflowMessage: "Excel tiedostoon",
      },
      {
        id: FileTypeId.ExcelCopy,
        label: "Excel copy-paste",
        hasLocales: true,
        workflowMessage: "Excel leikepöydälle",
      },
      {
        id: FileTypeId.CsvFile,
        label: "CSV-tiedosto",
        hasLocales: true,
        workflowMessage: "Csv tiedostoon",
      },
      {
        id: FileTypeId.CsvCopy,
        label: "CSV copy-paste",
        hasLocales: true,
        workflowMessage: "Csv leikepöydälle",
      },
      /*
      {
        id: "preview",
        label: "Esikatselu",
        hasLocales: false,
        workflowMessage: "Esikatselu",
      },
      {
        id: "pdf",
        label: "PDF / tuloste",
        hasLocales: false,
        workflowMessage: "PDF / tuloste",
      },
      */
    ];

  /** Export target */
  public targetId = "default";

  /** Targets */
  public targets: any[] = []; // TODO: GO through: (AccountingChannelSettings & TargetOption)[] = [];

  /**
   * Object with read function to call for retrieving the accounting data.
   * Function has the following argument:  arr, which is an array of accounting data into which the result will be appended.
   */
  public reader: {
    /** Function for reading data into given array. */
    read: (
      /** Target for accounting data */
      target: string,
      /** RuleSet for accounting data */
      ruleSet: string,
      /** Array for accounting data read results */
      arr: AccountingData[],
      /** Message for workflow (loading) */
      message: string,
    ) => Promise<void>,
    /** Optional label for data export */
    exportLabel?: string,
    /** Indicates if the export is not possible */
    disabled: () => boolean,
  };

  /**
   * Function that is called when the target has been chagned
   * Function has the following locals:  targetId: the selected accounting target id.
   */
  public onTargetChange: (params: {
    /** Selected target id */
    targetId: any,
  }) => void;

  /** Supported targets */
  private targetOptions: TargetOptions = {
    // Palkkaus.fi
    "36dff2e2-e181-4f0d-9004-0e4aa92f5e17":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getPalkkausExportData(accountingData);
      },
      hasSchemes: true,
      defaultFileTypeId: FileTypeId.ExcelFile,
    },
    // Palkkaus.fi ja lomat
    "8fd494ba-f98d-45e2-ae70-3570e16dfa3c":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getPalkkausExportData(accountingData);
      },
      hasSchemes: true,
      defaultFileTypeId: FileTypeId.ExcelFile,
    },
    // Fennoa
    "0db4e85f-93a8-46f1-b6d2-f7523d289d88":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getFennoaExportData(accountingData);
      },
      hasSchemes: false,
      defaultFileTypeId: FileTypeId.CsvFile,
    },
    // Finago Procountor
    "8613f133-3fce-4611-b372-51f6a2e007dd":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getProcountorExportData(accountingData);
      },
      hasSchemes: false,
      defaultFileTypeId: FileTypeId.ExcelCopy,
    },
    // Accountor Go
    "4b3335ad-dfd2-48f5-a088-6449874a0732":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getProcountorExportData(accountingData);
      },
      hasSchemes: false,
      defaultFileTypeId: FileTypeId.ExcelCopy,
    },
    // MeritAktiva
    "e62c10b0-222a-4170-b8b4-b1e02d9a7644":
    {
      getExportData: (accountingData: AccountingData) => {
        return this.getMeritAktivaExportData(accountingData);
      },
      hasSchemes: false,
      defaultFileTypeId: FileTypeId.CsvFile,
      noCsvQuotedStrings: true,
    },
  };

  constructor(
    private uiHelpers: UiHelpers,
    private $timeout: angular.ITimeoutService,
    private settingsService: SettingsService,
  ) {
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {
    // Get accounting targets

      const getSettings = () : Promise<CompanyAccountSettings> => {
        return new Promise( (resolve) => {
          const check = () => {
            if (this.settingsService.current) {
              return resolve(this.settingsService.current);
            }
            this.$timeout( () => check(), 50);
          }

          check();
        });
      }

      getSettings().then( (settings) => {
      this.targets = settings.accounting.targets.filter((x) => this.targetOptions[x.id]);
      for (const target of this.targets) {
        target.getExportData = this.targetOptions[target.id].getExportData;
        target.hasSchemes = this.targetOptions[target.id].hasSchemes;
        target.defaultFileTypeId = this.targetOptions[target.id].defaultFileTypeId;
        target.noCsvQuotedStrings = this.targetOptions[target.id].noCsvQuotedStrings;
      }

      if (settings.accounting.defaultTargetId && this.targetOptions[settings.accounting.defaultTargetId]) {
        this.targetId = settings.accounting.defaultTargetId;
      }
      if (this.targetId) {
        this.fileTypeId = this.targetOptions[this.targetId].defaultFileTypeId;
      }
    });
  }

  /** Exports data in defined format. */
  public export(
    reader: {
      /** Function for reading data into given array. */
      read: (
        /** Target for accounting data */
        target: string,
        /** RuleSet for accounting data */
        ruleSet: string,
        /** Array for accounting data read results */
        arr: AccountingData[],
        /** Workflow loading message */
        message: string,
      ) => Promise<void>,
      /** Optional label for data export */
      exportLabel?: string,
      /** Indicates if the export is not possible */
      disabled: () => boolean,
    } = null,
    /** If true, does not show loading indicator. Default is false. */
    hideLoading = false,
  ): Promise<void> {
    if (!reader) {
      reader = this.reader;
    }
    if (!reader) {
      console.error("No data reader for accounting data.");
      return Promise.resolve();
    }
    const loading = hideLoading ? { dismiss: () => null } : this.uiHelpers.showLoading("Odota...");

    const target = this.getTarget(this.targetId);
    const arr: AccountingData[] = [];
    return reader.read(this.targetId, null, arr, this.getWorkflowMessage()).then(() => {
      if (arr.length === 0) {
        loading.dismiss();
        return Promise.resolve();
      }
      switch (this.fileTypeId) {
        case FileTypeId.CsvFile:
          if (arr.length === 1) {
            const data = arr[0];
            saveAs(new Blob([this.getCsv(target.getExportData(data), this.localeId !== LocaleId.En, true, !target.noCsvQuotedStrings)], { type: "application/octet-stream;charset=utf-8" }), `${this.getFileName(data)}.csv`);
            loading.dismiss();
            return;
          } else {
            let counter = 0;
            const zip = new JSZip();
            const next = () => {
              if (counter === arr.length) {
                return;
              }
              const data = arr[counter++];
              const blob = new Blob([this.getCsv(target.getExportData(data), this.localeId !== LocaleId.En, true, !target.noCsvQuotedStrings)], { type: "application/octet-stream;charset=utf-8" });
              zip.file(`${this.getFileName(data)}.csv`, blob);
              next();
            };

            next();
            zip.generateAsync({
              type: "blob",
              mimeType:
                "application/zip",
            }).then((blob) => {
              saveAs(blob, `${this.getZipName(arr)}.zip`);
              loading.dismiss();
              return;
            });
          }
          break;
        case FileTypeId.ExcelFile:
          if (arr.length === 1) {
            const data = arr[0];
            ExcelHelpers.export(target.getExportData(data), this.getFileName(data)).then(() => {
              loading.dismiss();
              return;
            });
          } else if (arr.length > 1) {
            const tablesData = [];
            const next = () => {
              if (tablesData.length === arr.length) {
                return;
              }
              const data = arr[tablesData.length];
              tablesData.push({
                tableData: target.getExportData(data),
                fileName: this.getFileName(data),
              });
              next();
            };

            next();
            ExcelHelpers.exportMany(tablesData, this.getZipName(arr)).then(() => {
              loading.dismiss();
              return;
            });
          }
          break;
        case FileTypeId.CsvCopy:
          if (arr.length === 1) {
            const data = arr[0];
            this.copyToClipboard(this.getCsv(target.getExportData(data), this.localeId !== LocaleId.En, true, !target.noCsvQuotedStrings));
            loading.dismiss();
            return;
          } else if (arr.length > 1) {
            let counter = 0;
            let csv = "\uFEFF";
            const next = () => {
              if (counter === arr.length) {
                return;
              }
              const data = arr[counter++];
              csv += `${this.getFileName(data)}\r\n`;
              csv += this.getCsv(target.getExportData(data), this.localeId !== LocaleId.En, false, !target.noCsvQuotedStrings) + (counter < arr.length ? "\r\n" : "");
              next();
            };
            next();
            this.copyToClipboard(csv);
            loading.dismiss();
            return;
          }
          break;
        case FileTypeId.ExcelCopy:
          if (arr.length === 1) {
            const data = arr[0];
            this.copyToClipboard(this.getTabbed(target.getExportData(data), this.localeId !== LocaleId.En));
            loading.dismiss();
            return;
          } else if (arr.length > 1) {
            let counter = 0;
            let tabbed = "";
            const next = () => {
              if (counter === arr.length) {
                return;
              }
              const data = arr[counter++];
              tabbed += `${this.getFileName(data)}\r\n`;
              tabbed += this.getTabbed(target.getExportData(data), this.localeId !== LocaleId.En) + (counter < arr.length ? "\n" : "");
              next();
            };
            next();
            this.copyToClipboard(tabbed);
            loading.dismiss();
            return;
          }
          break;
        default:
          loading.dismiss();
          this.uiHelpers.showAlert("Ei vielä toteutettu", "Tätä tiedostotyyppiä ei ole toteutettu.");
          return;
      }
      loading.dismiss();
      return;
    });
  }

  /**
   * Change target and call event handler.
   * @param targetId - New target id.
   */
  public changeTarget(targetId: string) {
    this.targetId = targetId;
    this.fileTypeId = this.targetOptions[this.targetId].defaultFileTypeId;
    this.onTargetChange({ targetId });
  }

  /**
   * Returns the file type definition
   * @param fileTypeId File type id.
   */
  public getFileType(fileTypeId: string) {
    return this.fileTypes.find((x) => x.id === fileTypeId);
  }

  /**
   * Returns the target definition
   * @param targetId Target id.
   */
  public getTarget(targetId: string) {
    return this.targets.find((x) => x.id === targetId);
  }

  /**
   * Returns the locale definition
   * @param localeId Locale id.
   */
  public getLocale(localeId: string) {
    return this.locales.find((x) => x.id === localeId);
  }

  /**
   * Returns the scheme definition.
   * @param schemeId Scheme id.
   */
  private getScheme(schemeId: string) {
    return this.schemes.find((x) => x.id === schemeId);
  }

  private getFileName(data: AccountingData) {
    return Texts.escapeFileName(`Kirjanpito_${data.employer.displayName}_${Dates.getMoment(data.period.start ?? new Date()).format("YYYY_MM")}`);
  }

  private getZipName(all: AccountingData[]) { // eslint-disable-line @typescript-eslint/no-unused-vars
    return Texts.escapeFileName(`Kirjanpito_${Dates.getMoment(new Date()).format("YYYYMMDD")}`);
  }

  private getWorkflowMessage() {
    const fileType = this.getFileType(this.fileTypeId);
    if (fileType) {
      return fileType.workflowMessage;
    }
    return null;
  }

  private getCsv(data: any[], isFinnish = true, addPrefix: boolean, addQuotes: boolean) {
    const separator = (isFinnish ? ";" : ",");
    const rowMapper = this.formatRow(isFinnish, separator, addQuotes);
    const rows = data.map(rowMapper);
    return (addPrefix ? "\uFEFF" : "") + rows.join("\r\n");
  }

  private getTabbed(data: any[], isFinnish = true) {
    const rowMapper = this.formatRow(isFinnish, "\t");
    const rows = data.map(rowMapper);
    return rows.join("\n");
  }

  private formatRow(isFinnish: boolean, separator: string, addQuotes = true) {
    const fieldMapper = this.formatField(isFinnish, addQuotes);
    return (row) => {
      return row.map(fieldMapper).join(separator);
    };
  }

  private formatField(isFinnish: boolean, addQuotes = true) {
    return (value) => {
      if (value == null) { // we want to catch anything null-ish, hence just == not ===
        return "";
      }
      if (typeof (value) === "number") {
        if (isFinnish) {
          return value.toString().replace(".", ",");
        }
        return value;
      }
      if (typeof (value) === "boolean") {
        return (value ? "TRUE" : "FALSE");
      }
      if (typeof (value) === "string") {
        if (addQuotes) {
          return '"' + value.replace(/"/g, '""') + '"';
        }
        return value;
      }
      if (typeof (value) === "object") {
        if (value instanceof Date) {
          if (isFinnish) {
            return Dates.asMoment(value).format("D.M.YYYY");
          } else {
            return Dates.asMoment(value).format("YYYY-MM-DD");
          }
        } else {
          if (addQuotes) {
            return '"' + JSON.stringify(value).replace(/"/g, '""') + '"';
          }
          return JSON.stringify(value);
        }
      }
      return JSON.stringify(value);
    };
  }

  private copyToClipboard(data: string) {
    (navigator as any).clipboard.writeText(data);
  }

  /** Data exports for targets */

  private getPalkkausExportData(data: AccountingData): any[] {
    const result = [];
    const header = [];

    const accountKeys = ["accountNumber", "saldo", "accountName"];
    const accountKeyNames = ["Tilinumero", "Summa", "Tilin nimi"];
    const entryKeys = ["description", "isDebit", "amount", "vatPercent"];
    const entryKeyNames = ["Kuvaus", "Debet", "Arvo", "ALV-prosentti"];
    const dimensionKeys = ["calculationOwner", "calculationId", "calculationTitle", "projectNumber", "costCenter", "workerName"];
    const dimensionKeyNames = ["Laskelman tili", "Laskelman id", "Laskelman kuvaus", "Projektinumero", "Kustannuspaikka", "Työntekijä"];

    for (const accountKeyName of accountKeyNames) {
      header.push(accountKeyName);
    }
    if (this.schemeId === "extended") {
      for (const entryKeyName of entryKeyNames) {
        header.push(entryKeyName);
      }
      for (const dimensionKeyName of dimensionKeyNames) {
        header.push(dimensionKeyName);
      }
    }
    result.push(header);
    for (const rowData of data.ledgerAccounts) {
      let row = [];
      for (const accountKey of accountKeys) {
        row.push(rowData[accountKey]);
      }
      if (this.schemeId === "extended") {
        let first = true;
        for (const entryData of rowData.entries) {
          if (first) {
            first = false;
          } else {
            row = new Array(accountKeys.length);
          }
          for (const entryKey of entryKeys) {
            if (entryKey === "vatPercent") {
              row.push(entryData[entryKey] * 100);
            } else {
              row.push(entryData[entryKey]);
            }
          }
          for (const dimensionKey of dimensionKeys) {
            row.push(entryData.dimension[dimensionKey]);
          }

          result.push(row);
        }
      } else {
        result.push(row);
      }
    }
    return result;
  }

  /**
   * https://procountor.finago.com/hc/fi/articles/360000254997-Tallennusty%C3%B6kalu-uusi-#LIIT%C3%84VIENTEJ%C3%84-TOIMINTO
   */
  private getProcountorExportData(data: AccountingData): any[] {
    const result = [];

    const header = ["Kp-tili", "Brutto", "ALV-%", "ALV-väh.%", "ALV-tyyppi", "ALV-status", "Vientiselite"];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Vat key */
        vatPercent: number,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const vatPercent = entryData.vatPercent || 0;
        let keyEntryRows: {
          /** Vat key */
          vatPercent: number,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.vatPercent === vatPercent);
        if (keyEntryRows == null) {
          keyEntryRows = { vatPercent, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];
        // "Kp-tili",
        row.push(rowData.accountNumber);
        // "Brutto",
        row.push(keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0));
        // "ALV-%",
        row.push(keyEntryRows.vatPercent ? (keyEntryRows.vatPercent * 100) : "");
        // "ALV-väh.%",
        row.push(keyEntryRows.vatPercent ? 100 : "");
        // "ALV-tyyppi",
        row.push(keyEntryRows.vatPercent ? "P" : "");
        // "ALV-status",
        row.push(keyEntryRows.vatPercent ? "vat_1" : "");
        // "Vientiselite"
        row.push(rowData.accountName);

        result.push(row);
      }
    }
    return result;
  }

  /**
   * Merit aktiva format
   */
  private getMeritAktivaExportData(data: AccountingData): any[] {
    const result = [];

    const header = ["T", data.officialId, data.employer.displayName, Dates.asJSDate(data.period.end), null, Dates.asJSDate(data.period.start), Dates.asJSDate(data.period.end)];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const row = [];
      row.push("V");
      row.push(rowData.accountNumber);
      row.push(rowData.accountName);
      row.push(rowData.saldo);
      row.push(null);
      row.push(null);
      row.push(null);

      result.push(row);
    }
    return result;
  }

  /**
   * https://www.atsoft.fi/wkleikepoyta.htm
   */
  private getAsteriExportData(data: AccountingData): any[] {

    const result = [];

    const header = ["Tositenumero", "Päivämäärä", "Tositelaji", "Kustannuspaikka", "Laskunnumero", "Selite1", "Selite2", "Debet-tili", "Kredit-tili", "Summa"];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Cost center */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Cost center */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];

        // "Tositenumero",
        row.push(null);
        // "Päivämäärä",
        row.push(null);
        // "Tositelaji",
        row.push(null);
        // "Kustannuspaikka",
        row.push(keyEntryRows.costCenter);
        // "Laskunnumero",
        row.push(null);
        // "Selite1",
        row.push(rowData.accountName);
        // "Selite2",
        row.push(null);

        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        if (saldo >= 0) {
          // "Debet-tili",
          row.push(rowData.accountNumber);
          // "Kredit-tili",
          row.push("****");
          // "Summa"
          row.push(saldo);
        } else {
          // "Debet-tili",
          row.push("****");
          // "Kredit-tili",
          row.push(rowData.accountNumber);
          // "Summa"
          row.push(saldo * -1.0);
        }
        result.push(row);
      }
    }
    return result;
  }

  /**
   * https://support.netvisor.fi/hc/fi/articles/235300547-Kirjanpitotietojen-tuonti
   */
  private getNetvisorExportData(data: AccountingData): any[] {
    const result = [];

    const header = [
      "TositeLaji",
      "Tositenumero",
      "Päivämäärä (PVM)",
      "Selite 1",
      "Selite 2",
      "Selite 3",
      "Selite 4",
      "Selite 5",
      "Tilinumero",
      "Debet",
      "Kredit",
      "Summa",
      "Summa",
      "ALV-tunnus",
      "ALV-prosentti",
    ];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Vat key */
        vatPercent: number,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const vatPercent = entryData.vatPercent || 0;
        let keyEntryRows: {
          /** Vat key */
          vatPercent: number,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.vatPercent === vatPercent);
        if (keyEntryRows == null) {
          keyEntryRows = { vatPercent, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];
        // "TositeLaji",
        row.push(null);
        // "Tositenumero",
        row.push(null);
        // "Päivämäärä (PVM)",
        row.push(null);
        // "Selite 1",
        row.push(rowData.accountName);
        // "Selite 2",
        row.push(null);
        // "Selite 3",
        row.push(null);
        // "Selite 4",
        row.push(null);
        // "Selite 5",
        row.push(null);
        // "Tilinumero",
        row.push(rowData.accountNumber);

        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        if (saldo >= 0) {
          // "Debet",
          row.push(saldo);
          // "Kredit",
          row.push(0);
          // "Summa",
          row.push(saldo);
          // "Summa",
          row.push(saldo * -1.0);
        } else {
          // "Debet",
          row.push(0);
          // "Kredit",
          row.push(saldo * -1.0);
          // "Summa",
          row.push(saldo);
          // "Summa",
          row.push(saldo * -1.0);
        }
        // "ALV-tunnus",
        row.push(keyEntryRows.vatPercent ? "KOOS" : "");
        // "ALV-prosentti",
        row.push(keyEntryRows.vatPercent ? (keyEntryRows.vatPercent * 100) : "");
        result.push(row);
      }
    }
    return result;
  }

  /**
   * Generic export data format.
   */
  private getGenericExportData(data: AccountingData): any[] {
    const result = [];

    const header = [
      "Tilinumero",
      "Summa",
      "ALV-prosentti",
      "Kustannuspaikka",
      "Selite",
    ];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Vat key */
        vatPercent: number,
        /** Cost center key */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const vatPercent = entryData.vatPercent || 0;
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Vat key */
          vatPercent: number,
          /** Cost center key */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.vatPercent === vatPercent && x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { vatPercent, costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];
        // Tilinumero
        row.push(rowData.accountNumber);
        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        // Summa
        row.push(saldo);
        // ALV-prosentti
        row.push(keyEntryRows.vatPercent ? (keyEntryRows.vatPercent * 100) : "");
        // Kustannuspaikka
        row.push(keyEntryRows.costCenter);
        // "Selite",
        row.push(rowData.accountName);
        result.push(row);
      }
    }
    return result;
  }

  /**
   * Fennoa export data format.
   * Tuetut sarakkeet:
   *
   * - PVM - Tositepäivä. Ensimmäiseltä riviltä otetaan Fennoan tositteelle päivämäärä.
   * - TILI - Kirjanpidon tili
   * - SELITE - Vientirivin selite
   * - SUMMA (Debet +, Kredit -)
   * - ALV-KOODI
   * - tyhjä tarkoittaa tilikartasta tilin takaa haetaan oletus alv-koodi
   * - viiva ”-” tarkoittaa ei alv-koodia
   * - 1 = 24
   * - 2 = 14
   * - 3 = 10
   * - numero on vientiriville halutun alv-koodin id numero, jotka löytyvät ohjekannastamme ALV-kooditaulukosta (3.9.5 ALV-koodit / Arvonlisäverokoodit Fennoassa).
   * - LK1 - Laskentakohde 1
   * - LK2 - Laskentakohde 2
   * - LK… jne
   */
  private getFennoaExportData(data: AccountingData): any[] {
    const result = [];

    const header = [
      "PVM",
      "TILI",
      "SELITE",
      "SUMMA",
      "ALV-KOODI",
      "LK1",
    ];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Vat key */
        vatPercent: number,
        /** Cost center key */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const vatPercent = entryData.vatPercent || 0;
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Vat key */
          vatPercent: number,
          /** Cost center key */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.vatPercent === vatPercent && x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { vatPercent, costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];
        // PVM
        row.push(null);
        // TILI
        row.push(rowData.accountNumber);
        // SELITE,
        row.push(rowData.accountName);
        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        // SUMMA
        row.push(saldo);
        // ALV-KOODI
        row.push(keyEntryRows.vatPercent ?
          ((keyEntryRows.vatPercent * 100 === 24) ? 1 :
            (keyEntryRows.vatPercent * 100 === 14) ? 2 :
              (keyEntryRows.vatPercent * 100 === 10) ? 3 :
                "") : "");
        // LK
        row.push(keyEntryRows.costCenter);

        result.push(row);
      }
    }
    return result;
  }

  /**
   * https://community.visma.com/xqnef94594/attachments/xqnef94594/FI_SW_Visma_Fivaldi_kayttovinkit/41/1/visma-payroll--aineistojen-tuonti-visma-fivaldiin.pdf
   */
  private getFivaldiExportData(data: AccountingData): any[] {

    const result = [];

    const header = ["Tositenumero", "Päivämäärä", "Tili", "Debet", "Kredit", "Selite", "Kustannuspaikka"];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Cost center */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Cost center */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];

        // "Tositenumero",
        row.push(null);
        // "Päivämäärä",
        row.push(null);
        // "Tili",
        row.push(rowData.accountNumber);
        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        if (saldo >= 0) {
          // "Debet",
          row.push(saldo);
          // "Kredit",
          row.push(0);
        } else {
          // "Debet",
          row.push(0);
          // "Kredit",
          row.push(saldo * -1.0);
        }
        // "Selite",
        row.push(rowData.accountName);
        // "Kustannuspaikka",
        row.push(keyEntryRows.costCenter);
        result.push(row);
      }
    }
    return result;
  }

  /**
   * https://tikon.finago.com/hc/fi/article_attachments/360002246597/Excel.Kp-tap.pdf
   */
  private getTikonExportData(data: AccountingData): any[] {

    const result = [];

    const header = [
      "PVM",
      "TOSITE",
      "TILI",
      "EUR",
      "KPA",
      "SELITE"];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Cost center */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Cost center */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];

        // "PVM",
        row.push(null);
        // "TOSITE",
        row.push(null);
        // "TILI",
        row.push(rowData.accountNumber);
        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        // "EUR",
        row.push(saldo);
        // "KPA",
        row.push(keyEntryRows.costCenter);
        // "SELITE",
        row.push(rowData.accountName);

        result.push(row);
      }
    }
    return result;
  }

  /**
   * https://help.briox.fi/hc/fi/articles/115000585891-Tositteiden-tuominen
   */
  private getBrioxExportData(data: AccountingData): any[] {
    const result = [];

    const header = [
      "Kirjauspäivä",
      "Tositenumero",
      "Tilinro",
      "Arvo",
      "Kustannuspaikan koodi",
      "Tositeksti"];
    result.push(header);

    for (const rowData of data.ledgerAccounts) {
      const keyEntriesRows: {
        /** Cost center */
        costCenter: string,
        /** Ledger entries */
        entries: LedgerEntry[],
      }[] = [];
      for (const entryData of rowData.entries) {
        const costCenter = entryData.dimension.costCenter || "";
        let keyEntryRows: {
          /** Cost center */
          costCenter: string,
          /** Ledger entries */
          entries: LedgerEntry[],
        } = keyEntriesRows.find((x) => x.costCenter === costCenter);
        if (keyEntryRows == null) {
          keyEntryRows = { costCenter, entries: [] };
          keyEntriesRows.push(keyEntryRows);
        }
        keyEntryRows.entries.push(entryData);
      }
      // Append results
      for (const keyEntryRows of keyEntriesRows) {
        const row = [];

        // "Kirjauspäivä",
        row.push(null);
        // "Tositenumero",
        row.push(null);
        // "Tilinro",
        row.push(rowData.accountNumber);
        const saldo = keyEntryRows.entries.reduce((sum, x) => sum + (x.isDebit ? x.amount : -1.0 * x.amount), 0);
        // "Arvo",
        row.push(saldo);
        // "Kustannuspaikan koodi",
        row.push(keyEntryRows.costCenter);
        // "Tositeksti",
        row.push(rowData.accountName);

        result.push(row);
      }
    }
    return result;
  }
}
