import * as angular from "angular";

import { Ajax, Configs, CumulativeReportData, Dates, Numeric, PeriodType, PeriodDateKind, Texts } from "@salaxy/core";

import { UiHelpers } from "../../services";

/**
 * Controller for cumulative reports.
 */
export class CumulativeReportController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1", "UiHelpers", "$timeout", "$window"];

  /** Current data */
  public data: CumulativeReportData;

  /** Current report template */
  public templateId = "incomeTypes";

  /** templates */
  public templates: {
    /** Template id */
    id: string,
    /** Template label */
    label: string,
    /** Template configuration*/
    config: any,
    /** Boolean indicating if the template is a user template */
    isReadonly: boolean,
  }[] = [
      {
        id: "incomeTypes",
        label: "Palkkalajiraportti",
        config: null,
        isReadonly: true,
      },
      {
        id: "totals",
        label: "Palkanmaksujen erittely",
        config: null,
        isReadonly: true,
      },
      {
        id: "all",
        label: "Kaikki rivit",
        config: null,
        isReadonly: true,
      }
    ];

  private gridOptions: any;
  private grid: any;
  private agGrid = Configs.global.agGrid || require("@ag-grid-enterprise/all-modules");

  constructor(private ajax: Ajax, private uiHelpers: UiHelpers, private $timeout: angular.ITimeoutService, private $window: angular.IWindowService) {
  }

  /**
   * Initialize controller values.
   */
  public $onInit() {

    this.gridOptions = {
      statusBar: {
        statusPanels: [
          { statusPanel: 'agTotalAndFilteredRowCountComponent', key: 'totalAndFilter', align: 'left' },
          { statusPanel: 'agSelectedRowCountComponent', align: 'left' },
          { statusPanel: 'agAggregationComponent', align: 'right' }
        ]
      },
      enableMultiRowDragging: true,
      rowGroupPanelShow: "always",
      pivotPanelShow: "always",
      enableCharts: true,
      enableRangeSelection: true,

      defaultExportParams: {
        columnGroups: true,
        headerRowHeight: 30,
        rowHeight: 22,
        fontSize: 14,
      },
      defaultColDef: {
        flex: 1,
        minWidth: 200,
        sortable: true,
        filter: true,
        resizable: true,
        floatingFilter: true,
      },
      sideBar: { toolPanels: ['columns', 'filters'] },
      rowData: [],
    };

    try {
      const gridWrapper = (null ?? require("@salaxy/pro")).GridWrapper;
      if(gridWrapper) {
        console.debug(gridWrapper.AG_GRID_LICENCE_KEY);
        this.agGrid.LicenseManager.setLicenseKey(gridWrapper.AG_GRID_LICENCE_KEY);
      }
    } catch (e) {
      // No license
    }

    const eGridDiv = document.querySelector('#dataGrid');
    this.grid = new this.agGrid.Grid(eGridDiv, this.gridOptions);

    this.loadTemplates();
  }

  /**
   * Returns the template definition
   * @param templateId Template id.
   */
  public getTemplate(templateId: string) {
    return this.templates.find((x) => x.id === templateId);
  }

  /**
   * Shows the period selection dialog and makes the search.
   */
  public showPeriodSelection() {
    const period: {
      /** Period type for the query. */
      periodType: PeriodType,
      /** Ref date for the period. */
      refDate?: string,
      /** End date for the custom period. */
      endDate?: string,
    } = {
      periodType: PeriodType.Year,
      refDate: Dates.getTodayMoment().add(-1, "months").format("YYYY-MM-01"),
      endDate: null,
    }
    this.uiHelpers.openEditDialog(
      "salaxy-components/report/modals/CumulativeReportPeriodSelection.html",
      period,
      null).then((periodResult) => {
        if (periodResult.action === "ok") {
          this.gridOptions.api.showLoadingOverlay();
          this.queryDataForPeriod(periodResult.item.refDate, periodResult.item.periodType, periodResult.item.endDate, PeriodDateKind.SalaryDate)
            .then((dataResult) => {
              this.setData(dataResult);
            });
        }
      });
  }

  /**
   * Shows the calculation selection dialog and makes the search.
   */
  public showCalcsSelection() {
    const selectedCalculations = [];
    this.uiHelpers.openEditDialog(
      "salaxy-components/modals/calc/CalcList.html",
      selectedCalculations,
      {
        title: "Valitse maksetut palkat",
        category: "paid",
      }).then((selectionResult) => {
        if (selectionResult.action === "ok" && selectionResult.item.length > 0) {
          const ids = selectionResult.item.map((x) => x.id);
          this.gridOptions.api.showLoadingOverlay();
          this.queryDataForCalculationIds(ids).then((dataResult) => {
            this.setData(dataResult);
          });
        }
      });
  }


  /**
   * Selects the template to show
   * @param templateId The selected template.
   */
  public selectTemplate(templateId: string) {
    if (this.grid) {
      this.templateId = templateId;
      switch (this.templateId) {
        case "all":
          this.gridOptions.columnApi.resetColumnState();
          break;
        default:
          // Set
          this.gridOptions.columnApi.applyColumnState({ state: this.getTemplate(this.templateId).config});
          break;
      }
    }
  }

  /**
   * Save template
   */
  public showSaveTemplate() {
    const currentTemplate = this.getTemplate(this.templateId);
    const item: {
      /** Boolean indicating a new template */
      isNew: boolean,
      /** Boolean indicating if the template can be edited. */
      isEditable: boolean,
      /** Label for the template */
      label: string,
    } = {
      isNew: currentTemplate.isReadonly,
      isEditable: !currentTemplate.isReadonly,
      label: currentTemplate.isReadonly ? "Uusi raportti (" + (this.templates.filter((x) => !x.isReadonly).length + 1) + ")" : currentTemplate.label,
    }
    this.uiHelpers.openEditDialog(
      "salaxy-components/report/modals/CumulativeReportSaveTemplate.html",
      item,
      null).then((saveResult) => {
        if (saveResult.action === "ok") {
          const editedTemplate = saveResult.item;
          if (!editedTemplate.isNew) {
            currentTemplate.label = editedTemplate.label;
            currentTemplate.config = this.gridOptions.columnApi.getColumnState();
          } else {
            const newTemplate = {
              id: "" + (new Date().getTime()),
              label: editedTemplate.label,
              config: this.gridOptions.columnApi.getColumnState(),
              isReadonly: false,
            };
            this.templates.push(newTemplate);
            this.selectTemplate(newTemplate.id);
          }

          const customerTemplates = this.templates.filter((x) => !x.isReadonly);
          this.$window.localStorage.setItem("salaxy-cumulative-report-templates", JSON.stringify(customerTemplates));
        } else if (saveResult.action === "delete") {
          this.showDeleteTemplate();
        }
      });
  }

  /**
   * Delete template
   */
  public showDeleteTemplate() {
    if (this.$window.localStorage) {
      const template = this.getTemplate(this.templateId);
      this.uiHelpers.showConfirm(`Haluatko poistaa tämän raporttipohjan '${template.label}'?`).then((result) => {
        if (result) {
          if (!template.isReadonly) {
            const idx = this.templates.findIndex((x) => x.id === this.templateId);
            this.templates.splice(idx, 1);
            this.selectTemplate(this.templates[idx - 1].id);
            const customerTemplates = this.templates.filter((x) => !x.isReadonly);
            this.$window.localStorage.setItem("salaxy-cumulative-report-templates", JSON.stringify(customerTemplates));
          }
        }
      });
    }
  }

  /**
   * Processes and sets the data.
   */
  private setData(result: CumulativeReportData) {
    this.data = result;

    const sortingDateComparator = (a, b) => {
      if (!a || !b) {
        return a === b ? 0: a ? 1 : -1 ;
      }
      const mA = Dates.asMoment(Dates.asDate(a));
      const mB = Dates.asMoment(Dates.asDate(b));
      return mA.isSame(mB) ? 0: mA.isAfter(mB) ? 1 : -1;
    }
    const filterDateComparator = (a ,b) => sortingDateComparator(b, a);

    const columnDefs = [];
    columnDefs.push(
      {
        headerName: "Laskelmat",
        children: [
          {
            headerName: "Työntekijä",
            rowDrag: true,
            field: "worker",
            enableRowGroup: true,
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Laskelma",
            // rowDrag: true,
            field: "calculation",
            // enableRowGroup: true,
            valueGetter: (params) => {
              if (params.node && params.node.data) {
                return `${params.node.data.worker} ${params.node.data.salaryDate} ${Numeric.formatPrice(params.node.data["totals-totalGrossSalary"])}`;
              }
              return "";
            },
            cellRenderer: (params) => {
              if (params.node && params.node.data) {
                return `<a href="#/calc/details/${params.node.data.calculationId}" target="_blank" rel="noopener">${Texts.escapeHtml(params.node.data.worker)} ${params.node.data.salaryDate} ${Numeric.formatPrice(params.node.data["totals-totalGrossSalary"])}</a>`;
              }
              return "";
            }
          },
          {
            headerName: "Palkkapäivä",
            field: "salaryDate",
            valueFormatter: params => Dates.getFormattedDate(params.value),
            type: ["rightAligned"],
            filter: "agDateColumnFilter",
            comparator: sortingDateComparator,
            filterParams: { comparator: filterDateComparator, },
          },
          {
            headerName: "Brutto",
            field: "totalGrossSalary",
            valueGetter: (params) => {
              if (params.node && params.node.data) {
                return params.node.data["totals-totalGrossSalary"];
              }
              return null;
            },
            type: ["numericColumn"],
            valueFormatter: params => Numeric.formatNumber(params.value,2),
            filter: "agNumberColumnFilter",
          },
          {
            headerName: "Kuvaus",
            field: "workDescription",
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Aloituspäivä",
            field: "workStartDate",
            valueFormatter: params => Dates.getFormattedDate(params.value),
            type: ["rightAligned"],
            filter: "agDateColumnFilter",
            comparator: sortingDateComparator,
            filterParams: { comparator: filterDateComparator, },
          },
          {
            headerName: "Lopetuspäivä",
            field: "workEndDate",
            valueFormatter: params => Dates.getFormattedDate(params.value),
            type: ["rightAligned"],
            filter: "agDateColumnFilter",
            comparator: sortingDateComparator,
            filterParams: { comparator: filterDateComparator, },
          },
        ]
      },
      {
        headerName: "Dimensiot",
        children: [
          {
            headerName: "Kuukausi",
            field: "salaryMonth",
            enablePivot: true,
            enableRowGroup: true,
            type: ["numericColumn"],
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Vuosi",
            field: "salaryYear",
            enablePivot: true,
            enableRowGroup: true,
            type: ["numericColumn"],
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Projekti",
            field: "projectNumber",
            enablePivot: true,
            enableRowGroup: true,
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Kustannuspaikka",
            field: "costCenter",
            enablePivot: true,
            enableRowGroup: true,
            filter: "agMultiColumnFilter",
          },
          {
            headerName: "Eläketurva",
            field: "pensionCalculation",
            enablePivot: true,
            enableRowGroup: true,
            filter: "agMultiColumnFilter",
          }
        ],
      }
    );
    const irGroupDef = {
      headerName: "Tulolajit",
      children: [],
    };
    columnDefs.push(irGroupDef);
    Object.keys(this.data.irLabels).forEach((key) => {
      irGroupDef.children.push({
        field: key,
        headerName: this.data.irLabels[key],
        enableValue: true,
        type: ["numericColumn"],
        valueFormatter: params => Numeric.formatNumber(params.value,2),
        filter: "agNumberColumnFilter",
      });
    });

    const typeGroupDef = {
      headerName: "Rivityypit",
      children: [],
    };
    columnDefs.push(typeGroupDef);
    Object.keys(this.data.typeLabels).forEach((key) => {
      typeGroupDef.children.push({
        field: key,
        headerName: this.data.typeLabels[key],
        enableValue: true,
        type: ["numericColumn"],
        valueFormatter: params => Numeric.formatNumber(params.value,2),
        filter: "agNumberColumnFilter",
      });
    });

    const employerCalcDef = {
      headerName: "Työnantaja",
      children: [],
    };
    const workerCalcDef = {
      headerName: "Työntekijä",
      children: [],
    };
    const totalsDef = {
      headerName: "Yhteensä",
      children: [],
    };
    const totalGroupDef = {
      headerName: "Maksut",
      children: [
        employerCalcDef,
        workerCalcDef,
        totalsDef,
      ],
    };
    columnDefs.push(totalGroupDef);
    Object.keys(this.data.totalLabels).forEach((key) => {
      const def = key.indexOf("employerCalc") === 0 ? employerCalcDef :
        key.indexOf("workerCalc") === 0 ? workerCalcDef : totalsDef;

      def.children.push({
        field: key,
        headerName: this.data.totalLabels[key],
        enableValue: true,
        type: ["numericColumn"],
        valueFormatter: params => Numeric.formatNumber(params.value,2),
        filter: "agNumberColumnFilter",
      });
    });

    // Create presets
    this.getTemplate("incomeTypes").config = this.createIncomeTypesPreset(columnDefs);
    this.getTemplate("totals").config = this.createTotalsPreset(columnDefs);

    // create the grid passing in the div to use together with the columns & data we want to use
    this.$timeout(() => {
      this.gridOptions.api.setRowData(this.data.rows);
      this.gridOptions.api.setColumnDefs(columnDefs);
      this.selectTemplate(this.templateId);
    });
  }

  /**
   * Gets the monthly / quarterly / yearly cumulative data for the current account.
   *
   * @param refDate Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
   * @param periodType Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
   * @param endDate End date for the period. Required only for the custom period.
   * @param periodDateKind Period date type: paid at date or salary date.
   * @returns A Promise with result data.
   */
  private queryDataForPeriod(refDate: string, periodType?: PeriodType, endDate?: string, periodDateKind?: PeriodDateKind): Promise<CumulativeReportData> {
    refDate = Dates.asDate(refDate) || Dates.getToday();
    const prmsArr = [];
    if (periodType) {
      prmsArr.push(`periodType=${periodType}`);
    }
    if (endDate) {
      prmsArr.push(`endDate=${endDate}`);
    }
    if (periodDateKind) {
      prmsArr.push(`periodDateKind=${periodDateKind}`);
    }
    const prms = prmsArr.join("&");

    return this.ajax.getJSON(`/v03-rc/api/reports/cumulative/${refDate}${prms ? "?" + prms : ""}`);
  }

  /**
   * Gets the cumulative data based on given set of calculations.
   *
   * @param calculationIds - Calculations that are the bases for the report.
   * @returns A Promise with result data.
   */
  private queryDataForCalculationIds(calculationIds: string[]): Promise<CumulativeReportData> {
    const ids = calculationIds.map((x) => `calculationIds=${x}`).join("&");
    return this.ajax.getJSON(`/v03-rc/api/reports/cumulative?${ids}`);
  }

  private loadTemplates() {
    if (this.$window.localStorage) {
      const defaultTemplates = this.templates.filter((x) => x.isReadonly);
      const customerTemplates: any[] = JSON.parse(this.$window.localStorage.getItem("salaxy-cumulative-report-templates")) ?? [];
      this.templates.splice(0);
      this.templates.push(...defaultTemplates.concat(...customerTemplates));
    }
  }

  private createIncomeTypesPreset(columnDefs: any) {
    const preset = [];

    // Laskelmat
    const calculationDef = columnDefs[0];
    for (const def of calculationDef.children) {
      switch (def.field) {
        case "worker":
          preset.push(this.createPresetCol(def.field, true, true));
          break;
        case "calculation":
          preset.push(this.createPresetCol(def.field, false));
          break;
        default:
          preset.push(this.createPresetCol(def.field, true));
      }
    }
    // Dimensiot
    const dimensionDef = columnDefs[1];
    for (const def of dimensionDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }
    // Tulolajit
    const incomeTypeDef = columnDefs[2];
    for (const def of incomeTypeDef.children) {
      preset.push(this.createPresetCol(def.field, false, false, "sum"));
    }

    // Rivityypit
    const rowTypeDef = columnDefs[3];
    for (const def of rowTypeDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    // Maksut - työnantaja
    const employerTotalsDef = columnDefs[4].children[0];
    for (const def of employerTotalsDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    // Maksut - työntekijä
    const workerTotalsDef = columnDefs[4].children[1];
    for (const def of workerTotalsDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    // Maksut - yhteensä
    const totalsDef = columnDefs[4].children[2];
    for (const def of totalsDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    return preset;
  }

  private createTotalsPreset(columnDefs: any) {
    const preset = [];

    // Laskelmat
    const calculationDef = columnDefs[0];
    for (const def of calculationDef.children) {
      switch (def.field) {
        case "worker":
          preset.push(this.createPresetCol(def.field, true, true));
          break;
        case "salaryDate":
        case "workStartDate":
        case "workEndDate":
          preset.push(this.createPresetCol(def.field, false));
          break;
        default:
          preset.push(this.createPresetCol(def.field, true));
      }
    }
    // Dimensiot
    const dimensionDef = columnDefs[1];
    for (const def of dimensionDef.children) {
      switch (def.field) {
        case "pensionCalculation":
          preset.push(this.createPresetCol(def.field, false));
          break;
        default:
          preset.push(this.createPresetCol(def.field, true));
      }
    }
    // Tulolajit
    const incomeTypeDef = columnDefs[2];
    for (const def of incomeTypeDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    // Rivityypit
    const rowTypeDef = columnDefs[3];
    for (const def of rowTypeDef.children) {
      preset.push(this.createPresetCol(def.field, true));
    }

    const totalFields = [
      "totals-totalGrossSalary",
      "workerCalc-benefits",
      "totals-annualHolidayCompensation",
      "totals-holidayBonus",
      // "muut vähennettävät erät ml. luontoiseduista vähennettävät"
      "totals-totalTaxable",
      "workerCalc-tax",
      "workerCalc-pension",
      "workerCalc-unemploymentInsurance",
      "workerCalc-unionPayment",
      "workerCalc-foreclosure",
      "workerCalc-salaryAdvance",
      "workerCalc-otherDeductions",
      "workerCalc-deductions",
      "workerCalc-prepaidExpenses",
      "totals-totalExpenses",
      "workerCalc-totalWorkerPayment",
      "employerCalc-totalPayment",
      "employerCalc-palkkaus",
      "employerCalc-socialSecurity",
      "totals-pension",
      "totals-unemployment",
      "employerCalc-unemployment",
      "totals-totalPensionInsuranceBase",
      "totals-totalSocialSecurityBase",
      "totals-totalHealthInsuranceBase",
      "totals-totalUnemploymentInsuranceBase",
      "totals-totalAccidentInsuranceBase",
    ];

    for (const field of totalFields) {
      preset.push(this.createPresetCol(field, false, false, "sum"));
    }

    // Maksut - työnantaja
    const employerTotalsDef = columnDefs[4].children[0];
    for (const def of employerTotalsDef.children) {
      if (totalFields.indexOf(def.field) < 0) {
        preset.push(this.createPresetCol(def.field, true));
      }
    }

    // Maksut - työntekijä
    const workerTotalsDef = columnDefs[4].children[1];
    for (const def of workerTotalsDef.children) {
      if (totalFields.indexOf(def.field) < 0) {
        preset.push(this.createPresetCol(def.field, true));
      }
    }

    // Maksut - yhteensä
    const totalsDef = columnDefs[4].children[2];
    for (const def of totalsDef.children) {
      if (totalFields.indexOf(def.field) < 0) {
        preset.push(this.createPresetCol(def.field, true));
      }
    }

    return preset;
  }

  private createPresetCol(colId: string, hide: boolean, rowGroup = false, aggFunc: string = null) {
    return {
      aggFunc,
      colId,
      flex: 1,
      hide,
      pinned: null,
      pivot: false,
      pivotIndex: null,
      rowGroup,
      rowGroupIndex: null,
      sort: null,
      sortIndex: null,
      width: 200,
    };
  }
}
