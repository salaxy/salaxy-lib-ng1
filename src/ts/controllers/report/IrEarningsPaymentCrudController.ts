import * as angular from "angular";

import { Absences, ApiListItem, Calculations, Calculation, Dates, irepr, IrEarningsPayments, AbsenceCauseCode } from "@salaxy/core";

import { ApiCrudObjectController } from "../bases";
import { Calculator2019Controller } from "../../controllers";
import { SessionService, UiHelpers } from "../../services";

/**
 * Controller for user interface of Earnings Payment Report ("Tulorekisteri-ilmoitus").
 */
export class IrEarningsPaymentCrudController extends ApiCrudObjectController<irepr.EarningsPayment> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "IrEarningsPayments",
    "UiHelpers",
    "$location",
    "$routeParams",
    "Calculations",
    "Absences",
    "SessionService"
  ];

  /** Calculation controller  */
  public calcController: Calculator2019Controller;

  private _calculation: Calculation;
  private _calculationId: string;

  constructor(
    private fullApi: IrEarningsPayments,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private calcApi: Calculations,
    private absencesApi: Absences,
    private session: SessionService,
  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "irepr",
      detailsUrl: this.detailsUrl || "/irepr/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/IrEpr.html",
      oDataOptions: {},
    };
  }

  /** Gets calculation */
  public get calculation() {
    if (this.current?.info?.calculationId != null) {
      this.calculationId = this.current.info.calculationId;
    }
    return this._calculation;
  }

  /**
   * Sets calculation and if it is different then starts the loading of the EarningsPayment object to that calculation.
   */
  public set calculation(value: Calculation) {
    if (value == null || value.id == null) {
      this._calculation = null;
      this._calculationId = null;
      return;
    }
    if (this.calculationId === value?.id) {
      return;
    }
    this._calculation = value;
    this._calculationId = value.id;
    if (this.current?.info?.calculationId !== value.id) {
      this.setStatus(this.calcApi.getEpr(value.id)).then((data) => {
        this.setCurrent(data);
      });
    }
  }

  /** Gets calculation */
  public get calculationId(): string {
    return this._calculation?.id ?? this._calculationId;
  }

  /**
   * Sets calculation and if it is different then starts the loading of the EarningsPayment object to that calculation.
   */
  public set calculationId(value: string) {
    if (value == null) {
      this._calculation = null;
      this._calculationId = null;
      return;
    }
    if (this.calculationId === value) {
      return;
    }
    this._calculation = null;
    this._calculationId = value;
    this.calcApi.getSingle(value).then((calc) => {
      this._calculation = calc;
    });
    if (this.current?.info?.calculationId !== value) {
      this.setStatus(this.calcApi.getEpr(value)).then((data) => {
        this.setCurrent(data);
      });
    }
  }

  /** Returns true if the report can be canceled. */
  public get isCancellable() {
    return this.current && this.getLatestNonErrorReportLogEntry()?.eventType === irepr.ReportLogEventType.Sent;
  }

  /** Returns true if the item is read-only */
  public get isReadOnly(): boolean {
    if (this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly || this.isReadOnlyForced) {
      // TODO: When moving to ES6, call super.isReadOnly instead.
      return true;
    }
    return !(this as any).tempShowEditInNonAdminMode && !this.session.isInRole("admin");
  }

  /** Save resource */
  public saveChanges(): Promise<irepr.EarningsPayment> {
    if (this._calculation && this.calcController && this.calcController.hasChanges) {
      return this.calcApi.saveIr(this._calculation).then((calc) => {
        this._calculation = calc;
        return this.calcApi.convertToEpr(this._calculation).then((epr) => {
          this.current.deliveryData.reports[0].transactions = epr.deliveryData.reports[0].transactions;
          return this.save().then(() => {
            this.calcController.setCurrent(this._calculation);
            return this.current;
          });
        });
      });
    } else {
      return this.save().then(() => {
        return this.current;
      });
    }
  }

  /** Send to incomes register. */
  public sendToIncomesRegister(startAt: Date): Promise<irepr.EarningsPayment> {
    return this.saveChanges().then((epr) => {
      if (epr.validation.isValid) {
        const loader = this.uiHelpers.showLoading("SALAXY.NG1.EarningsPaymentComponent.common.loaderTextSending");
        const aso: irepr.IrApiScheduleObject = {
          id: this.current.id,
          startAt: startAt ? JSON.stringify(startAt).replace(/"/g, "") : null,
          action: irepr.IrScheduleAction.Send
        };
        return this.fullApi.sendSchedule(aso).then(() => {
          return this.reloadFromServer().then(() => {
            loader.dismiss();
            return this.current;
          })
        });
      } else {
        this.uiHelpers.showAlert("SALAXY.NG1.EarningsPaymentComponent.common.sendFailureTitle", "SALAXY.NG1.EarningsPaymentComponent.common.validationErrorText");
        return this.current;
      }
    });
  }

  /** Get latest incomes register report log entry */
  private getLatestNonErrorReportLogEntry(): irepr.ReportLogEntry {
    if (this.current.reportLog != null) {
      const nonErrorEntries = this.current.reportLog.filter((x) => x.eventType !== irepr.ReportLogEventType.Error);
      if (nonErrorEntries.length > 0) {
        return nonErrorEntries[nonErrorEntries.length - 1];
      }
    }
    return null;
  }

  /** Sends cancel request to incomes register */
  public cancelToIncomesRegister(): Promise<irepr.EarningsPayment> {
    const loader = this.uiHelpers.showLoading("SALAXY.NG1.EarningsPaymentComponent.common.loaderTextCanceling");
    const aso: irepr.IrApiScheduleObject = {
      id: this.current.id,
      action: irepr.IrScheduleAction.Cancel
    };
    return this.fullApi.sendSchedule(aso).then(() => {
      return this.reloadFromServer().then(() => {
        loader.dismiss();
        return this.current;
      })
    });
  }

  /** Opens sheculing dialog and sends scheduling after successful dialog close */
  public openSetScheduleDialog() {
    return this.uiHelpers.openEditDialog("salaxy-components/report/modals/IrScheduleSendDialog.html", {
      startAt: new Date()
    }, {})
      .then((dialog) => {
        if (dialog.result === "ok") {
          return this.sendToIncomesRegister(dialog.item.startAt);
        } else {
          return this.current;
        }
      });
  }

  /** Remove Incomes register queue item */
  public removeIrQueueItem(): Promise<irepr.EarningsPayment> {
    const loader = this.uiHelpers.showLoading("SALAXY.NG1.EarningsPaymentComponent.common.loaderTextRemoving");
    return this.fullApi.removeIrQueueItem(this.current.id).then(() => {
      return this.reloadFromServer().then(() => {
        loader.dismiss();
        return this.current;
      })
    });
  }

  /** Opens a dialog for importing absences. */
  public importAbsences() {
    this.absencesApi.getForEmployment(this.calculation.worker.employmentId).then((workerAbsences) => {
      this.uiHelpers.openEditDialog("salaxy-components/report/modals/IrImportAbsencesDialog.html", workerAbsences, {}).then((dialog) => {
        if (dialog.result === "ok" && workerAbsences.periods) {
          const selectedAbsences = workerAbsences.periods.filter((x) => (x as any).selected);
          if (selectedAbsences.length > 0) {
            const report = this.current.deliveryData.reports[0];
            report.absence = report.absence ?? {};
            let absencesStartDate = null;
            let absencesEndDate = null;
            selectedAbsences.forEach((period) => {
              if (period.isPaid) {
                report.absence.paidAbsence = report.absence.paidAbsence ?? {};
                report.absence.paidAbsence.paidAbsencePeriods = report.absence.paidAbsence.paidAbsencePeriods ?? [];
                report.absence.paidAbsence.paidAbsencePeriods.push({
                  startDate : period.period?.start,
                  endDate : period.period?.end,
                  causeCode : this.getPaidAbsenceCauseCode(period.causeCode),
                  absenceDays : period.period?.daysCount,
                  amount : period.amount,
                });
              } else {
                report.absence.unpaidAbsence = report.absence.unpaidAbsence ?? {};
                report.absence.unpaidAbsence.unpaidAbsencePeriods = report.absence.unpaidAbsence.unpaidAbsencePeriods ?? [];
                report.absence.unpaidAbsence.unpaidAbsencePeriods.push({
                  startDate : period.period?.start,
                  endDate : period.period?.end,
                  causeCode : this.getUnpaidAbsenceCauseCode(period.causeCode),
                  absenceDays : period.period?.daysCount,
                });
              }
              if (!absencesStartDate || ( period.period && period.period.start && Dates.getMoment(absencesStartDate).isAfter(Dates.getMoment(period.period.start)))) {
                absencesStartDate = period.period?.start;
              }
              if (!absencesEndDate || ( period.period && period.period.end && Dates.getMoment(absencesEndDate).isBefore(Dates.getMoment(period.period.end)))) {
                absencesEndDate = period.period?.end;
              }
            });
            report.absence.absenceRepStartDate = absencesStartDate;
            report.absence.absenceRepEndDate = absencesEndDate;
          }
        }
      });
    });
  }


  /** Checks if the IR report is modified. */
  public isIrModified(item: ApiListItem): boolean {
    return item.flags.some((x) => x === "modified");
  }

  /** Checks if the Epr is delayed. */
  public isDelayed(item: ApiListItem): boolean {
    if (!item.data.firstDeliveredAt) {
      const officialDate = Dates.getMoment(item.salaryDate).add(5, "days");
      const expiresAt = Dates.getMoment(Dates.addWorkdays(officialDate, 0));
      const today = Dates.getTodayMoment();
      return today.isAfter(expiresAt);
    }
    return false;
  }

  private reloadFromServer(): Promise<irepr.EarningsPayment> {
    return this.setStatus(this.api.getSingle(this.current.id)).then((data) => {
      this.setCurrent(data);
      return data;
    });
  }

  private getUnpaidAbsenceCauseCode(causeCode: AbsenceCauseCode): irepr.UnpaidAbsenceCauseCode {
    switch (causeCode) {
      case AbsenceCauseCode.Illness:
        return irepr.UnpaidAbsenceCauseCode.Illness;
      case AbsenceCauseCode.PartTimeSickLeave:
        return irepr.UnpaidAbsenceCauseCode.PartTimeSickLeave;
      case AbsenceCauseCode.ParentalLeave:
        return irepr.UnpaidAbsenceCauseCode.ParentalLeave;
      case AbsenceCauseCode.SpecialMaternityLeave:
        return irepr.UnpaidAbsenceCauseCode.SpecialMaternityLeave;
      case AbsenceCauseCode.Rehabilitation:
        return irepr.UnpaidAbsenceCauseCode.Rehabilitation;
      case AbsenceCauseCode.ChildIllness:
        return irepr.UnpaidAbsenceCauseCode.ChildIllness;
      case AbsenceCauseCode.PartTimeChildCareLeave:
        return irepr.UnpaidAbsenceCauseCode.PartTimeChildCareLeave;
      case AbsenceCauseCode.Training:
        return irepr.UnpaidAbsenceCauseCode.Training;
      case AbsenceCauseCode.JobAlternationLeave:
        return irepr.UnpaidAbsenceCauseCode.JobAlternationLeave;
      case AbsenceCauseCode.StudyLeave:
        return irepr.UnpaidAbsenceCauseCode.StudyLeave;
      case AbsenceCauseCode.IndustrialAction:
        return irepr.UnpaidAbsenceCauseCode.IndustrialAction;
      case AbsenceCauseCode.InterruptionInWorkProvision:
        return irepr.UnpaidAbsenceCauseCode.InterruptionInWorkProvision;
      case AbsenceCauseCode.LeaveOfAbsence:
        return irepr.UnpaidAbsenceCauseCode.LeaveOfAbsence;
      case AbsenceCauseCode.MilitaryRefresherTraining:
        return irepr.UnpaidAbsenceCauseCode.MilitaryRefresherTraining;
      case AbsenceCauseCode.MilitaryService:
        return irepr.UnpaidAbsenceCauseCode.MilitaryService;
      case AbsenceCauseCode.LayOff:
        return irepr.UnpaidAbsenceCauseCode.LayOff;
      case AbsenceCauseCode.ChildCareLeave:
        return irepr.UnpaidAbsenceCauseCode.ChildCareLeave;
      case AbsenceCauseCode.Other:
      default:
        return irepr.UnpaidAbsenceCauseCode.Other;
    }
  }

  private getPaidAbsenceCauseCode(causeCode: AbsenceCauseCode): irepr.PaidAbsenceCauseCode {
    switch (causeCode) {
      case AbsenceCauseCode.Illness:
        return irepr.PaidAbsenceCauseCode.Illness;
      case AbsenceCauseCode.PartTimeSickLeave:
        return irepr.PaidAbsenceCauseCode.PartTimeSickLeave;
      case AbsenceCauseCode.ParentalLeave:
        return irepr.PaidAbsenceCauseCode.ParentalLeave;
      case AbsenceCauseCode.SpecialMaternityLeave:
        return irepr.PaidAbsenceCauseCode.SpecialMaternityLeave;
      case AbsenceCauseCode.Rehabilitation:
        return irepr.PaidAbsenceCauseCode.Rehabilitation;
      case AbsenceCauseCode.ChildIllness:
        return irepr.PaidAbsenceCauseCode.ChildIllness;
      case AbsenceCauseCode.PartTimeChildCareLeave:
        return irepr.PaidAbsenceCauseCode.PartTimeChildCareLeave;
      case AbsenceCauseCode.Training:
        return irepr.PaidAbsenceCauseCode.Training;
      case AbsenceCauseCode.LeaveOfAbsence:
        return irepr.PaidAbsenceCauseCode.LeaveOfAbsence;
      case AbsenceCauseCode.MilitaryRefresherTraining:
        return irepr.PaidAbsenceCauseCode.MilitaryRefresherTraining;
      case AbsenceCauseCode.MidWeekHoliday:
        return irepr.PaidAbsenceCauseCode.MidWeekHoliday;
      case AbsenceCauseCode.AccruedHoliday:
        return irepr.PaidAbsenceCauseCode.AccruedHoliday;
      case AbsenceCauseCode.OccupationalAccident:
        return irepr.PaidAbsenceCauseCode.OccupationalAccident;
      case AbsenceCauseCode.AnnualLeave:
        return irepr.PaidAbsenceCauseCode.AnnualLeave;
      case AbsenceCauseCode.PartTimeAbsenceDueToRehabilitation:
        return irepr.PaidAbsenceCauseCode.PartTimeAbsenceDueToRehabilitation;
      case AbsenceCauseCode.Other:
      default:
        return irepr.PaidAbsenceCauseCode.Other;
    }
  }
}
