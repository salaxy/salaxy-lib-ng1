import * as angular from "angular";

import { ApiListItem, Dates, irpsr, IrPayerSummaries } from "@salaxy/core";

import { ApiCrudObjectController } from "../bases";
import { SessionService, UiHelpers } from "../../services";

/**
 * Controller for user interface of Payer Summary Report ("Tulorekisteri-ilmoitus").
 */
export class IrPayerSummaryCrudController extends ApiCrudObjectController<irpsr.PayerSummary> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "IrPayerSummaries",
    "UiHelpers",
    "$location",
    "$routeParams",
    "SessionService"
  ];

  constructor(
    private fullApi: IrPayerSummaries,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private session: SessionService,
  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "irpsr",
      detailsUrl: this.detailsUrl || "/irpsr/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/IrPsr.html",
      oDataOptions: {},
    };
  }

  /** Returns true if the report can be canceled. */
  public get isCancellable() {
    return this.current && this.getLatestNonErrorReportLogEntry()?.eventType === irpsr.ReportLogEventType.Sent;
  }

  /** Returns true if the item is read-only */
  public get isReadOnly(): boolean {
    if (this.status === "noInit" || this.status === "initialLoading" || !this.current || this.current.isReadOnly || this.isReadOnlyForced)
    {
      // TODO: When moving to ES6, call super.isReadOnly instead.
      return true;
    }
    return !(this as any).tempShowEditInNonAdminMode && !this.session.isInRole("admin");
  }


  /** Send to incomes register  */
  public sendToIncomesRegister(startAt: Date): Promise<irpsr.PayerSummary> {
    return this.save().then((psr) => {
      if (psr.validation.isValid) {
        const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayerSummaryComponent.common.loaderTextSending");
        const aso: irpsr.IrApiScheduleObject = {
          id: this.current.id,
          startAt: startAt ? JSON.stringify(startAt).replace(/"/g, ""): null,
          action: irpsr.IrScheduleAction.Send
        };

        return this.fullApi.sendSchedule(aso).then(() => {
          return this.reloadFromServer().then(() => {
            loader.dismiss();
            return this.current;
          });
        });

      } else {
        this.uiHelpers.showAlert("SALAXY.NG1.PayerSummaryComponent.common.sendFailureTitle", "SALAXY.NG1.PayerSummaryComponent.common.validationErrorText");
        return this.current;
      }
    });
  }

  /** Get latest incomes register report log entry */
  private getLatestNonErrorReportLogEntry(): irpsr.ReportLogEntry {
    if (this.current.reportLog != null) {
      const nonErrorEntries = this.current.reportLog.filter((x) => x.eventType !== irpsr.ReportLogEventType.Error);
      if (nonErrorEntries.length > 0) {
        return nonErrorEntries[nonErrorEntries.length - 1];
      }
    }
    return null;
  }

  /** Sends cancel request to incomes register */
  public cancelToIncomesRegister(): Promise<irpsr.PayerSummary> {
    const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayerSummaryComponent.common.loaderTextCanceling");
    const aso: irpsr.IrApiScheduleObject = {
      id: this.current.id,
      action: irpsr.IrScheduleAction.Cancel
    };
    return this.fullApi.sendSchedule(aso).then(() => {
      return this.reloadFromServer().then( () => {
        loader.dismiss();
        return this.current;
      });
    });
  }

  /** Opens sheculing dialog and sends scheduling after successful dialog close */
  public openSetScheduleDialog(): Promise<irpsr.PayerSummary>  {
    return this.uiHelpers.openEditDialog("salaxy-components/report/modals/IrScheduleSendDialog.html", {
      startAt: new Date() }, {}).then((dialog) => {
        if (dialog.result === "ok") {
          return this.sendToIncomesRegister(dialog.item.startAt);
        } else {
          return this.current;
        }
      });
  }

  /** Remove Incomes register queue item */
  public removeIrQueueItem(): Promise<irpsr.PayerSummary> {
    const loader = this.uiHelpers.showLoading("SALAXY.NG1.PayerSummaryComponent.common.loaderTextRemoving");
    return this.fullApi.removeIrQueueItem(this.current.id).then(() => {
      return this.reloadFromServer().then(() => {
        loader.dismiss();
        return this.current;
      })
    });
  }

  /** Checks if the IR report is modified. */
  public isIrModified(item: ApiListItem): boolean {
    return item.flags.some( (x) => x === "modified");
  }

  /** Checks if the Psr is delayed. */
  public isDelayed(item: ApiListItem): boolean {
    if (!item.data.firstDeliveredAt) {
      const officialDate = Dates.getMoment(item.logicalDate).add(1, "month").add(4, "days");
      const expiresAt = Dates.getMoment(Dates.addWorkdays(officialDate, 0)); // the day or next work day
      const today = Dates.getTodayMoment();
      return today.isAfter(expiresAt);
    }
    return false;
  }

  private reloadFromServer(): Promise<irpsr.PayerSummary> {
    return this.setStatus(this.api.getSingle(this.current.id)).then((data) => {
      this.setCurrent(data);
      return data;
    });
  }
}
