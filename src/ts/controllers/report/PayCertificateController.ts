import * as angular from "angular";

import { Ajax, ApiListItem, ApiValidation, Dates } from "@salaxy/core";

import { WizardService, WizardStep } from "../../services";
import { WizardController } from "../bases/WizardController";

/**
 * Provides methods for generating pay certificates.
 */
export class PayCertificateController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "WizardService", "AjaxNg1"];

  /** Worker for the report */
  public worker: ApiListItem = null;

  /** Selected workers */
  public selectedWorkers: ApiListItem[] = [];

  /** Period calculations */
  public periodCalculations: ApiListItem[] = [];

  /** Validation for the report. */
  public validation: ApiValidation = null;

  /** Pay certificate wizard configuration */
  public wizardSteps: WizardStep[] = [
    {
      title: "Työntekijän valinta",
      view: "salaxy-components/report/PayCertificateStep1.html",
    },
    {
      title: "Valitse kauden laskelmat",
      view: "salaxy-components/report/PayCertificateStep2.html",
    },
    {
      title: "Lisätiedot ja tulostettava PDF",
      view: "salaxy-components/report/PayCertificateStep3.html",
    },
  ];

  constructor($scope: angular.IScope, wizardService: WizardService, private ajax: Ajax) {
    super($scope, wizardService);
  }

  /**
   * Initialization of the controller
   */
  public $onInit() {
    super.$onInit();
    this.wizardService.setSteps(this.wizardSteps);
    this.wizardService.activeStepNumber = 1;
  }

  /** Returns true if goNext is enabled  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.step === 1 && this.selectedWorkers.length === 0) {
          return false;
        }
        if (this.step === 2 && this.periodCalculations.length === 0) {
          return false;
        }

        return true;

      }
    }
    return false;
  }

  /**
   * Navigates to the next step if possible
   */
  public goNext() {
    super.goNext();
    if (this.step === 3) {
      this.validate();
    }
  }

  /** Selects the worker and advances the workflow */
  public selectWorker(worker: ApiListItem, isSelected: boolean) {
    if (isSelected) {
      this.worker = worker;
      this.selectedWorkers.splice(0, this.selectedWorkers.length);
      this.selectedWorkers.push(this.worker);
      this.periodCalculations = [];
      this.validation = null;
      this.goNext();
    }
  }

  /** Selects calculations from the period */
  public selectCalculations(items: ApiListItem[], weeks: number) {

    // go x weeks to past
    let periodStart = Dates.getTodayMoment().add(-1 * weeks, "weeks");
    // set to monday of that week
    periodStart = periodStart.add("days", -1 * (periodStart.isoWeekday() - 1));

    this.periodCalculations.splice(0, this.periodCalculations.length);
    for (const item of items) {
      if (Dates.asMoment(item.salaryDate) >= periodStart) {
        this.periodCalculations.push(item);
      }
    }
  }

  /** Selects calculations from the period */
  public unselectCalculations() {
    this.periodCalculations.splice(0, this.periodCalculations.length);
  }

  /**
   * Returns the url for downloading pdf.
   * @param inline If true will open the pdf. The default is false: download as attachment.
   * @returns URL for downloading the report.
   */
  public getDownloadPdfUrl(inline = false): string {
    if (
      this.worker == null ||
      this.periodCalculations.length === 0) {
      return null;
    }
    let method = `${this.ajax.getServerAddress()}/ReportPdf/PayCertificate/${encodeURIComponent(this.worker.otherId)}?`;
    this.periodCalculations.forEach((item) => { method += "calculationIds=" + encodeURIComponent("" + item.id) + "&"; });
    method += `inline=${inline}`;
    method += `&access_token=${this.ajax.getCurrentToken()}`;
    return method;
  }

  /**
   * Validates the report server side.
   */
  public validate() {
    this.validation = null;
    if (
      this.worker == null ||
      this.periodCalculations.length === 0) {
      return;
    }
    let method = `/v03-rc/api/reports/validate/payCertificate/${encodeURIComponent(this.worker.otherId)}?`;
    this.periodCalculations.forEach((item) => { method += "calculationIds=" + encodeURIComponent("" + item.id) + "&"; });
    method = method.replace(/[?&]$/, "");
    this.ajax.getJSON(method).then((result: ApiValidation) => {
      this.validation = result;
    });
  }

  /**
   * Start new wizard
   */
  public startNew() {
    this.step = 1;
    this.worker = null;
    this.selectedWorkers.splice(0, this.selectedWorkers.length);
    this.periodCalculations = [];
    this.validation = null;
  }
}
