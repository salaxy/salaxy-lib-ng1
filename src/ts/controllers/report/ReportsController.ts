﻿import * as angular from "angular";

import { calcReportType, Report, reportPartial, ReportsLogic, ReportType } from "@salaxy/core";

import { ReportsService, SessionService } from "../../services";

/**
 * Handles user interaction for fetching, showing and later generating Reports
 */
export class ReportsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "ReportsService",
    "SessionService",
  ];

  /**
   * Current report type for this controller. Default for the getReports() method.
   */
  public reportType: ReportType;

  /**
   * When using getReportHtmlById(), the HTML is set here.
   * Also contains a spinner for loading and sets an alert if there is a problem.
   */
  public reportHtml;

  /**
   * Static instance of getReportTypes() to avoid a $digest problem that occured when calling getReportTypes() directly from the view.
   * HACK: This may not work in a scenario where Controller is created before the Session.
   */
  public types = this.getReportTypes();

  constructor(
    private reportsService: ReportsService,
    private sessionService: SessionService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    //
  }

  /**
   * Gets a list of reports (metadata only) filtered by a report type.
   * @param type - Type of report. See type (string enumeration) for possible values. Default is the property reportType.
   */
  public getReports(type: ReportType = this.reportType): Report[] | Promise<Report[]> {
    return this.reportsService.getReports(type);
  }

  /**
   * Gets a list of available report types for the current user.
   * TODO: This method is still under construction.
   */
  public getReportTypes() {
    // TODO: Generalize this role logic and merge with moveToCore
    const allReportTypes = ReportsLogic.getReportTypes();
    const filtered = allReportTypes.filter((x) => this.sessionService.isInSomeRole(x.roles));
    return filtered;
  }

  /**
   * Gets a. URL for a calculation report.
   * If report has not been saved (ID is null), returns null.
   * @param reportType - Type of report
   * @param calculationId - Identifier of the calculation. This method requires that the calculation has been saved.
   * @param cumulative If true, fetches the report as cumulative.
   * Currently only supported by salarySlip
   * @returns Url for specified report
   */
  public getReportUrl(reportType: calcReportType, calculationId: string, cumulative = false) {
    return this.reportsService.getReportUrlForCalc(reportType, calculationId, cumulative);
  }

  /**
   * Gets an HTML report based on Calculation ID, runs it through $sce
   * and sets it as reportHtml.
   *
   * @param reportType - Type of the report to fetch. See the HtmlReportType enumeration for possible values.
   * @param calculationId - GUID for the calculation
   *
   * @returns A Promise with result HTML - already run through $sce.
   */
  public getReportHtmlById(reportType: reportPartial, calculationId: string): Promise<any> {
    this.reportHtml = "<salaxy-spinner></salaxy-spinner>";
    return this.reportsService.getReportHtmlById(reportType, calculationId).then((html) => {
      this.reportHtml = html;
      return html;
    }).catch((reason) => {
      this.reportHtml = `<div class="alert alert-danger"><strong>Error in fetching report</strong><pre>${angular.toJson(reason)}</pre></div>`;
    });
  }
}
