﻿import * as angular from "angular";

import { Calculations, Workers } from "@salaxy/core";

import { InvoicesService, ReportsService, SessionService, UiHelpers } from "../../services";

import { CalculationCrudController } from "../calc";

/**
 * The new ApiCrudObject type of CRUD controller for the calculation reports.
 * Note that SalaryRepots does not have separate CRUD methods, but instead
 * the CRUD structure is based on Calculations (extends CalculationCrudController).
 */
export class SalaryReportCrudController extends CalculationCrudController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Calculations", "ReportsService", "UiHelpers", "$location", "$routeParams", "Workers", "SessionService", "InvoicesService"];

  /** Default query options for panel */
  public panelQueryOptions = {
    $filter: "status eq 'paymentSucceeded'",
    $top: 5,
    $orderby: "salaryDate desc",
  };
  /** Default query options for full list view */
  public queryOptions = {
    $filter: "status eq 'paymentSucceeded'",
    $top: 20,
    $orderby: "salaryDate desc",
  };

  constructor(
    calculationsApi: Calculations,
    reportsService: ReportsService,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    workersApi: Workers,
    sessionService: SessionService,
    protected invoicesService: InvoicesService,
  ) {
    super(calculationsApi, reportsService, uiHelpers, $location, $routeParams, workersApi, sessionService, invoicesService);
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/salary-reports",
      detailsUrl: this.detailsUrl || "/calc/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/SalaryReportsPanel.html",
      oDataOptions: {},
    };
  }
}
