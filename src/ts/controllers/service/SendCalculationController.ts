import * as angular from "angular";

import {
  Avatar, CalculationRowType, Calculations, DateRange, Dates, EmploymentRelationType, Iban, UserDefinedRow, WorkerAccount, WorkerListItem, WorkerLogic, Workers,
} from "@salaxy/core";

import { Calculator2019Controller } from "../calc";
import { InvoicesService, ReportsService, SessionService, UiCrudHelpers, UiHelpers } from "../../services";

/**
 * Controller for sending a salary calculation from company to Payroll service.
 */
export class SendCalculationController extends Calculator2019Controller {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Calculations",
    "UiHelpers",
    "UiCrudHelpers",
    "$location",
    "$routeParams",
    "SessionService",
    "InvoicesService",
    "ReportsService",
    "$anchorScroll",
    "Workers",
    "Taxcards",
  ];

  /** The worker account, if selected to the form */
  public worker: WorkerAccount;

  /** Daterange where the UI control is bound to. */
  public dateRange: DateRange = {};

  /** Status of the loading of workers */
  public workerListStatus: "initial" | "loading" | "loaded" = "initial";

  private rowTypes = {
    // Custom UI
    entrepreneur: ["phoneBenefit", "milageOwnCar", "dailyAllowance", "dailyAllowanceHalf", "mealCompensation"],
    hourlySalary: ["phoneBenefit", "overtime", "milageOwnCar", "dailyAllowance", "dailyAllowanceHalf", "mealCompensation"],
    monthlySalary: ["phoneBenefit", "overtime", "milageOwnCar", "dailyAllowance", "dailyAllowanceHalf", "mealCompensation"],
    salary: ["overtime", "milageOwnCar", "dailyAllowance", "dailyAllowanceHalf", "mealCompensation"],
    // No custom UI. Supported as standard rows editor though.
    "undefined": [],
    compensation: [],
    boardMember: [],
    farmer: [],
    employedByStateEmploymentFund: [],
    keyEmployee: [],
    athlete: [],
    performingArtist: [],
  };

  private workerList: WorkerListItem[] = [];

  constructor(
    calcApi: Calculations,
    uiHelpers: UiHelpers,
    uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,

    sessionService: SessionService,
    invoicesService: InvoicesService,

    reportsService: ReportsService,

    private $anchorScroll: angular.IAnchorScrollService,

    workers: Workers,
  ) {
    super(calcApi, uiHelpers, uiCrudHelpers, $location, $routeParams, sessionService, reportsService, workers, invoicesService);
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
    this.loadWorkers();
  }

  /** Starts a new calculation with payroll-service usecase */
  public startNew() {
    this.worker = null;
    this.model = "new"; // Setting the model also calls reload.
    this.currentCalc.usecase = {
      uri: "palkkaus.fi/usecases/payroll-service/calc",
      label: "Palkkapalvelu",
      data: {},
      description: "Palkka-aineisto standardissa palvelumallissa",
    };
    return this.currentCalc;
  }

  /** When worker account ID changes, resets the calculation and sets the worker */
  public workerAccountIdChanged(avatarId) {
    const loader = this.uiHelpers.showLoading("Haetaan tietoja", "Haetaan työntekijän tietoja...");
    this.workersApi.getSingle(avatarId).then((workerAccount) => {
      this.startNew();
      this.setWorker(workerAccount).then(() => {
        this.worker = workerAccount;
        loader.dismiss();
      });
    });
  }

  /**
   * Opens a dialog that shows the wizard to create a new Worker account.
   */
  public launchWorkerWizard(): void {
    this.uiCrudHelpers.createNewWorker().then((result) => {
      if (result.action === "ok") {
        const loader = this.uiHelpers.showLoading("Tallennetaan...");
        this.startNew();
        this.setWorker(result.item.worker).then(() => {
          this.worker = result.item.worker;
          loader.dismiss();
        });
      }
    });
  }

  /**
   * Gets the first row of the calculation assuring that it exists
   * if there is a current calculation, the calculation is not added.
   */
  public getFirstRow(): UserDefinedRow {
    const row = super.getFirstRow();
    if (!row.rowType || row.rowType === CalculationRowType.Unknown) {
      switch (this.worker.employment.type) {
        case EmploymentRelationType.MonthlySalary:
        case EmploymentRelationType.HourlySalary:
        case EmploymentRelationType.Salary:
        case EmploymentRelationType.Compensation:
          row.rowType = this.worker.employment.type as any;
          break;
        case EmploymentRelationType.BoardMember:
          row.rowType = CalculationRowType.Board;
          break;
        case EmploymentRelationType.Entrepreneur:
        default:
          row.rowType = CalculationRowType.MonthlySalary;
          break;
      }
    }
    return row;
  }

  /** Gets the usecase that calculation forms are based on. */
  public get usecaseData(): {
    /** Message for salary additions  */
    otherRowsMessage: string,
    /** Other requests message */
    message: string,
  } {
    return (this.currentCalc && this.currentCalc.usecase) ? this.currentCalc.usecase.data as any : null;
  }

  /** Gets the row types that should be shown in the rows editor. */
  public getRowTypes(): string[] {
    return this.rowTypes[this.worker.employment.type] || null;
  }

  /** Sends the calculation to Payroll service list */
  public sendCalculation() {
    const loading = this.uiHelpers.showLoading("Lähetetään...");
    this.currentCalc.info.workStartDate = this.dateRange.start;
    this.currentCalc.info.workEndDate = this.dateRange.end;
    this.currentCalc.framework.numberOfDays = this.dateRange.daysCount;
    this.save().then(() => {
      this.startNew();
      loading.dismiss();
      this.$location.hash("bodyTop");
      this.$anchorScroll();
      this.uiHelpers.showAlert("Palkka-aineiston lähetys", "Aineisto lähetetty onnistuneesti");
    }).catch((error) => {
      loading.dismiss();
      this.uiHelpers.showAlert("Palkka-aineiston lähetys", "Aineiston lähetys epäonnistui. Koita lähettää aineisto uudelleen.");
    } );
  }

  /**
   * Returns the number of days from today to requested salary date.
   */
  public getWorkdaysToSalaryDate(): number {
    if (!this.currentCalc || !this.currentCalc.workflow.requestedSalaryDate) {
      return null;
    }
    const days = Dates.getWorkdays("today", this.currentCalc.workflow.requestedSalaryDate).length;
    return days > 0 ? days : 0;
  }

  /** Gets the description for the worker account. */
  public getWorkerDescription(worker: WorkerAccount) {
    return WorkerLogic.getDescription(worker);
  }

  // Worker select -->

  /** Filters preloaded workers. */
  public filterWorkers = (searchString: string): Avatar[] => {
    if (!searchString) {
      const allResult = this.workerList
        .slice(0, 30).map(this.getAvatar);
      return allResult;
    }
    searchString = (searchString || "").trim().toLowerCase();
    const result = this.workerList
      .filter((w) =>
        (w.otherPartyInfo.avatar.displayName || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.avatar.sortableName || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.telephone || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.otherPartyInfo.email || "").toLowerCase().indexOf(searchString) >= 0 ||
        (w.id || "").toLowerCase().indexOf(searchString) === 0 ||
        (w.otherPartyInfo.officialId || "").toLowerCase().indexOf(searchString) === 0)
      .map(this.getAvatar);
    return result.length > 0 ? result.slice(0, 30) : [];
  }

  /** Gets the display name for the selected worker. */
  public getWorkerName = (avatarId: string): string => {
    for (const worker of this.workerList) {
      if (avatarId === worker.otherId) {
        return this.htmlEscape(this.getAvatar(worker).displayName);
      }
    }
    return null;
  }

  /** Flushes the workers cache and reloads them again */
  public reloadWorkers() {
    this.workerListStatus = "initial";
    this.workerList = [];
    this.loadWorkers();
  }

  private getAvatar = (worker: WorkerListItem): Avatar => {
    const result: Avatar = Object.assign({}, worker.otherPartyInfo.avatar);
    (result as any).displayId =
      ((worker.otherId || "").startsWith("FI") ? Iban.formatIban(worker.otherId) + " / " : "")
      + (worker.otherPartyInfo.officialId ? worker.otherPartyInfo.officialId : "-")
      ;
    result.displayName = this.htmlEscape(result.displayName);
    result.description = this.htmlEscape(WorkerLogic.getDescription(worker));
    return result;
  }

  private htmlEscape(str: string): string {
    if (!str) {
      return str;
    }
    return str
      .replace(/&/g, "&amp;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#39;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/\//g, "&#x2F;");
  }

  private loadWorkers() {
    if (this.workerListStatus === "initial") {
      this.workerListStatus = "loading";
      // Get all workers
      this.workersApi.getOData(null).then((data) => {
        this.workerList = data.items;
        this.workerListStatus = "loaded";
      });
    }
  }
}
