import * as angular from "angular";

import { AccountantType, Ajax, ApiListItem, LegalEntityType, OData, ODataQueryOptions, PrimaryPartners, Validation, PrimaryPartnerSettings } from "@salaxy/core";

import { SettingsService, UiHelpers } from "../../services";

/**
 * Controller for the accountant settings.
 */
export class AccountantController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1", "SettingsService", "PrimaryPartners", "UiHelpers"];

  constructor(private ajax: Ajax, private settingsService: SettingsService, private primaryPartners: PrimaryPartners, private uiHelpers: UiHelpers) {
  }

  /**
   * Returns current accountant.
   */
  public get accountant(): PrimaryPartnerSettings {
    if (this.settingsService.current) {
      return this.settingsService.current.partner;
    }
    return null;
  }

  /** Returns accountant main type */
  public get accountantMainType(): "person" | "company" | "none" {
    return this.getAccountantMainType(this.accountant);
  }

  /** Shows the edit dialog */
  public showEdit() {
    const odataPartnersUrl = "/v03-rc/api/settings/primary-partners";
    const odataQueryOptions: ODataQueryOptions = {
      $top: 10,
    };
    const accountant: PrimaryPartnerSettings = this.accountant || {
      type: null,
      info: {
        avatar: {},
      },
    };
    const logic = {
      type: this.getAccountantMainType(accountant),
      searchResult: [] as PrimaryPartnerSettings[],
      isLoading: false,
      selected: this.getAccountantMainType(accountant) === "company" ? accountant : null,
      select: (item: PrimaryPartnerSettings, row: PrimaryPartnerSettings) => {
        if (row.info.avatar.id) {
          row.type = AccountantType.PendingPrimaryPartner;
        } else {
          row.type = AccountantType.UnlinkedPrimaryPartner;
        }
        logic.selected = row;
        Object.assign(item, row);
      },
      reset: (item) => {
        logic.selected = null;
        Object.assign(item, accountant);
      },
      search: (search: string) => {
        logic.isLoading = true;
        if (Validation.formatCompanyIdFi(search)) {
          const officialId = this.formatOfficialId(search);
          odataQueryOptions.$filter = `ownerInfo/officialId eq '${officialId}' or data/description eq '${officialId}'`;
          odataQueryOptions.$search = null;
        } else {
          odataQueryOptions.$filter = null;
          odataQueryOptions.$search = search;
        }
        return OData.getOData<ApiListItem>(odataPartnersUrl, odataQueryOptions, this.ajax).then((result) => {
          logic.searchResult = result.items.map((x) => {
            const ownerInfo = x.ownerInfo;
            ownerInfo.officialId = ownerInfo.officialId || x.data.description;
            return { info: ownerInfo} as PrimaryPartnerSettings;
          });
          if (logic.searchResult.length === 0) {
            return this.primaryPartners.searchYtj(search).then((result) => {
              logic.searchResult = result.map( (x) => ({ info: x}));
              logic.isLoading = false;
              return logic.searchResult;
            });
          } else {
            logic.isLoading = false;
            return logic.searchResult;
          }
        });
      },
    };

    return this.uiHelpers.openEditDialog(
      "salaxy-components/modals/settings/AccountantSelection.html",
      accountant,
      logic,
      "lg").then((dialog) => {
        if (dialog.result === "ok") {
          const loading = this.uiHelpers.showLoading("Tallennetaan...");

          if (logic.type === "none") {
            dialog.item = {
              info: {
                avatar: {},
              },
              type: "none" as AccountantType, // AccountantType.None,
            };
          } else if (logic.type === "person") {
            dialog.item.type = "none" as AccountantType; // AccountantType.None;
            dialog.item.info.avatar.displayName = (dialog.item.info.avatar.firstName + " " + dialog.item.info.avatar.lastName).trim();
            dialog.item.info.avatar.color = "gray";
            dialog.item.info.avatar.entityType = LegalEntityType.Person;
            dialog.item.info.avatar.initials = dialog.item.info.avatar.firstName[0].toUpperCase() + dialog.item.info.avatar.lastName[0].toUpperCase();

          }

          this.settingsService.current.partner = dialog.item;
          return this.settingsService.save().then(() => {
            loading.dismiss();
            return;
          });
        }
      });
  }

  /** Removes pricing partner using confirmation dialog. */
  public showConfirmPartnerRemoval() {
    this.uiHelpers.showConfirm("Poista tilitoimistokumppani", "Poiston jälkeen tilitoimistollasi ei ole pääsyä raportteihisi. Haluatko varmasti poistaa kumppanin?")
      .then((result) => {
        if (result) {
          this.settingsService.current.partner = {
            info: {
              avatar: {},
            },
            type: "none" as AccountantType, // AccountantType.None,
          };
          const loading = this.uiHelpers.showLoading("Tallennetaan...");
          // MJ 16.9.2020 removed, side-effects to SSO return this.authorizedAccounts.delete(id).then( () => {
          return this.settingsService.save().then(() => {
              loading.dismiss();
          });
          // });
        }
      });
  }

  private getAccountantMainType(accountant: PrimaryPartnerSettings): "person" | "company" | "none" {
    if (!accountant || !accountant.type || accountant.type === AccountantType.Unknown) {
      return "none";
    }

    return accountant.type as string === "none" ? // AccountantType.None ?
      ((accountant.info.avatar.firstName || accountant.info.email) ? "person" : "none") : "company";

  }

  private formatOfficialId(search: string): string {
    if (Validation.formatCompanyIdFi(search)) {
      // The escape may be redundant, but needs to be tested.
      search = search.replace(/[\-–—]/g, ""); // eslint-disable-line no-useless-escape
      return search.substr(0, 7) + "-" + search.substr(7, 1);
    }

    return search;
  }
}
