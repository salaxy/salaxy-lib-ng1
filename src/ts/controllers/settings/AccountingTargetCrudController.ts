import * as angular from "angular";

import { AccountingChannel, AccountingRuleSetAccount, AccountingRuleSetRow, AccountingTarget, AccountingTargetListItem, AccountingTargets, IncomeTypesLogic } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * UI logic for viewing and adding new (modifying) accounting targets.
 * Accounting targets define where the accounting data is sent and how it is mapped to a Chart of Accounts (CoA)
 */
export class AccountingTargetCrudController extends ApiCrudObjectController<AccountingTarget> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AccountingTargets",
    "UiHelpers",
    "$location",
    "$routeParams",
  ];

  /** If the current item is based on template, it should be here once loaded from server. */
  public template: AccountingTarget;

  /** TODO: Move to core  */
  private flags = [
    { value: "customerSelfPayment", text: "Työnantaja hoitaa", description: "Työnantaja maksaa itse", isSelected: false },
    { value: "cfDeductionAtSalaxy", text: "Palkkaus pidättää", description: "Palkkaus pidättää asiakasvaratilin kautta", isSelected: false },
    { value: "incomeEarnerEntrepreneur", text: "Palkansaajan tyyppi: Yrittäjä", description: "Työntekijä on yrittäjä", isSelected: false },
    { value: "expenses", text: "Rivityyppi: Kulukorvaus", description: "Laskelman rivityyppi on kulukorvaus", isSelected: false },
    { value: "prepaidExpenses", text: "Rivityyppi: Kulukorvausennakko", description: "Laskelman rivityyppi on ennalta maksettu kulukorvaus", isSelected: false },
    { value: "foreclosure", text: "Rivityyppi: Ulosotto", description: "Laskelman rivityyppi on ulosotto", isSelected: false },
    { value: "unionPayment", text: "Rivityyppi: AY-jäsenmaksu", description: "Laskelman rivityyppi on AY-jäsenmaksu", isSelected: false },
    { value: "advance", text: "Rivityyppi: Palkkaennakko", description: "Laskelman rivityyppi on palkkaennakko", isSelected: false },
  ];

  private entryCodeGroups =
  {
    incomeType: "Tulolaji",
    totalType: "Laskettu erä",
  };

  private totalTypes = {
    totalPensionWorker: "Työntekijöiden TyEL-maksut",
    totalPensionEmployer: "Työnantajan TyEL-maksu",
    totalPension: "TyEL-maksut",
    totalUnemploymentWorker: "Työntekijöiden työttömyysvakuutusmaksut",
    totalUnemploymentEmployer: "Työnantajan työttömyysvakuutusmaksu",
    totalUnemployment: "Työttömyysvakuutusmaksut",
    totalSocialSecurityEmployer: "Työnantajan sairausvakuutusmaksu",
    totalTax: "Ennakonpidätykset",
    totalPalkkaus: "Palkkaus.fi-palkkio",
    totalWorkerPayment: "Työntekijän nettopalkka ja kulukorvaukset",
    totalPayment: "Maksettu asiakasvaratilille",
  };

  private incomeTypes = IncomeTypesLogic.getAll("fi").map((x) => ({ value: "" + x.code, text: x.code + " - " + x.label, title: x.description }));

  constructor(
    private fullApi: AccountingTargets,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/accounting-targets",
      detailsUrl: this.detailsUrl || "/accounting-targets/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/AccountingTargets.html",
      oDataOptions: {},
    };
  }

  /**
   * Additional setup after the item has been loaded.
   * @param item Item that has been loaded
   */
  public setCurrentRef(item: AccountingTarget) {
    super.setCurrentRef(item);
    if (this.current?.ruleSet?.templateId) {
      if (this.current.ruleSet.templateId !== this.template?.ruleSet?.templateId) {
        this.fullApi.getSingle(this.current.ruleSet.templateId).then((result) => {
          this.template = result;
        });
      }
    }
  }

  /**
   * Shows the detail view for the item.
   * Supports viewing public items from other users as read-only.
   * @param item Item may be either Container or list item.
   */
  public showDetails(item: AccountingTargetListItem | AccountingTarget): angular.ILocationService {
    // TODO: Consider adding this support in super
    if (this.parentController) {
      return this.parentController.showDetails(item);
    }
    return this.$location.url(this.getDefaults().detailsUrl + this.getId(item) + "@" + item.owner);
  }

  /**
   * Shows the detail view with a new item that is a copy of the given item.
   * Supports copying public items from other users.
   * @param item Item may be either Container or list item.
   */
  public showCopyAsNew(item: AccountingTargetListItem | AccountingTarget): angular.ILocationService {
    // TODO: Consider adding this support in super
    if (this.parentController) {
      return this.parentController.showCopyAsNew(item);
    }
    return this.$location.url(this.getDefaults().detailsUrl + this.getId(item) + "@" + item.owner + "/copy-as-new");
  }

  /** Returns flag description */
  public getFlag(value: string) {
    return this.flags.find((x) => x.value === value) || { value, text: value, description: value};
  }

  /** Gets entry description */
  public getEntryDescription(row: AccountingRuleSetRow) {
    switch (row.entryCodeGroup) {
      case "incomeType":
        return this.entryCodeGroups[row.entryCodeGroup] + ": " + this.incomeTypes.find( (x) => x.value === row.entryCode).text;
      case "totalType":
        return this.entryCodeGroups[row.entryCodeGroup] + ": " + this.totalTypes[row.entryCode];
    }
    return "";
  }

  /** Selects entry */
  public selectEntry(row: AccountingRuleSetRow) {
    this.uiHelpers.showDialog(
      "salaxy-components/report/modals/EntryCodes.html",
      null,
      {
        entryCodeGroups: this.entryCodeGroups,
        totalTypes: this.totalTypes,
        incomeTypes: this.incomeTypes,
        row: angular.copy(row),
      },
    ).then((result) => {
      if (result && result.entryCode && result.entryCodeGroup) {
        row.entryCode = result.entryCode;
        row.entryCodeGroup = result.entryCodeGroup;
      }
    });
  }

  /**
   * Creates a copy of the current set
   * Changes the settings so that the accounting target becomes a new item
   * @param copySource Item (container item) to copy as new.
   */
  public copyItem(copySource: AccountingTarget): AccountingTarget {
    const copy = super.copyItem(copySource);
    copy.info.target.avatar.id = null;
    copy.info.target.avatar.displayName = "Kopio " + copy.info.target.avatar.displayName;
    if (copy.info.channel == AccountingChannel.Template) {
      copy.info.channel = AccountingChannel.Manual;
    }
    copy.ruleSet.templateId = copySource.id + "@" + copySource.owner;
    copy.isReadOnly = false;
    return copy;
  }

  /** Selects the account */
  public selectAccount(row: AccountingRuleSetRow, isCredit: boolean) {
    const accounts = angular.copy(this.current.ruleSet.accounts);
    for (const account of accounts) {
      if (isCredit) {
        (account as any).isSelected = account.id === row.creditId;
      } else {
        (account as any).isSelected = account.id === row.debitId;
      }
    }

    this.uiHelpers.showDialog(
      "salaxy-components/report/modals/AccountNumbers.html",
      null,
      {
        accounts,
      },
    ).then((account) => {
      if (account) {
        if (isCredit) {
          row.creditId = account.id;
        } else {
          row.debitId = account.id;
        }
      }
    });
  }

  private groupBy<T>(source: T[], getKey: (item: T) => string, sortKeys = false): { [key: string]: T[] }  {
    let result = source.reduce((previousValue, currentValue) => {
      const key = getKey(currentValue);
      previousValue[key] = previousValue[key] || [];
      (previousValue[key]).push(currentValue);
      return previousValue;
    }, {});
    if (sortKeys) {
      result = Object.keys(result).sort().reduce((previousValue, key) => {
          previousValue[key] = result[key];
          return previousValue;
        }, {});
    }
    return result;
  }

  /** Gets the ruleset rouws grouped by account and grouping */
  public get groupedRows(): { [key: string]: AccountingRuleSetRow[] } {
    return this.uiHelpers.cache(this, "groupedRows", () => {
      if (!this.current) {
        return null;
      }
      const getKey = (row: AccountingRuleSetRow): string => {
        if (!row.isIncluded) {
          return "xxxx";
        }
        const debit = (this.getAccount(row.debitId)?.account || "9999") + (row.debitGrouping || "yyyy");
        const credit = (this.getAccount(row.creditId)?.account || "9999") + (row.creditGrouping || "yyyy");
        return (debit + credit)  || "zzzz";
      };
      return this.groupBy(this.current.ruleSet.rows, getKey, true);
    });
  }

  /** Returns account details */
  public getAccount(id: string) {
    return this.current.ruleSet.accounts.find((x) => x.id === id);
  }

  /** Adds new account */
  public addAccount(row: AccountingRuleSetAccount) {
    const idx = row ? this.current.ruleSet.accounts.findIndex((x) => x.id === row.id) : this.current.ruleSet.accounts.length;
    const copyRow = row || this.current.ruleSet.accounts[this.current.ruleSet.accounts.length - 1];
    const newRow = {
      isReadOnly: false,
      id: this.newGuid(),
      account: copyRow.account,
      text: copyRow.text,
    };
    this.current.ruleSet.accounts.splice(idx, 0, newRow);
  }

  /** Removes the account */
  public removeAccount(row: AccountingRuleSetAccount) {
    if (!row.isReadOnly) {
      // TODO: Consider adding a check if the account is in use in some of the derived rulesets. Perhaps in saving?
      if (this.current.ruleSet.rows.some((r) => r.creditId === row.id || r.debitId === row.id )) {
        this.uiHelpers.showAlert("Tiliä ei voi poistaa", "Tili on käytössä tiliöintimallissa.");
        return;
      }
      const idx = this.current.ruleSet.accounts.findIndex((x) => x.id === row.id);
      this.current.ruleSet.accounts.splice(idx, 1);
    }
  }

  /** Adds a new row */
  public addRow(row: AccountingRuleSetRow) {
    const idx = row ? this.current.ruleSet.rows.findIndex((x) => x.id === row.id) : this.current.ruleSet.rows.length;
    const copyRow = row || this.current.ruleSet.rows[this.current.ruleSet.rows.length - 1];
    const newRow = {
      isReadOnly: false,
      enabled: copyRow.enabled,
      id: this.newGuid(),
      entryCodeGroup: copyRow.entryCodeGroup,
      entryCode: copyRow.entryCode,
      isIncluded: copyRow.isIncluded,
      debitId: copyRow.debitId,
      debitGrouping: copyRow.debitGrouping,
      creditId: copyRow.creditId,
      creditGrouping: copyRow.creditGrouping,
      flags: angular.copy(copyRow.flags),
    };
    this.current.ruleSet.rows.splice(idx, 0, newRow);
  }

  /** Removes the row */
  public removeRow(row: AccountingRuleSetRow) {
    const idx = this.current.ruleSet.rows.findIndex((x) => x.id === row.id);
    if (!row.isReadOnly) {
      this.current.ruleSet.rows.splice(idx, 1);
    }
  }

  /** Selects flags */
  public selectFlags(row: AccountingRuleSetRow) {
    const flags = angular.copy(this.flags);
    for (const flag of flags) {
      flag.isSelected = row.flags.some( (x) => x === flag.value);
    }
    this.uiHelpers.showDialog(
      "salaxy-components/report/modals/Flags.html",
      null,
      {
        flags,
      },
    ).then((result) => {
      if (result) {
         row.flags = result.filter( (x) => x.isSelected).map( (x) => x.value);
      }
    });
  }

  private newGuid(): string {
    let dt = new Date().getTime();
    const uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
      /* eslint-disable-next-line no-bitwise */
      const r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      /* eslint-disable-next-line no-bitwise */
      return (c === "x" ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  /**
   * IN TEST ONLY: Regenerates the base data for the current account.
   * This method will be removed after development has ended.
   */
  public testRecreateBaseData() {
    this.fullApi.testRecreateBaseData().then((result) => {
      alert("Server responded: " + result);
    });
  }
}
