import * as angular from "angular";

import { CompanyAccountSettings, Dates, InsuranceContract, PensionContract, SettingsLogic, SettingsStatus, Years } from "@salaxy/core";

import { SessionService, SettingsService, UiHelpers } from "../../services";


type SettingsSection = "pensionContracts" |
  "insuranceContracts" |
  "incomesRegister" |
  "eInvoice" |
  "sepa" |
  "taxAndSocialSecuritySelfHandling" |
  "workerSelfHandling" |
  "unemploymentSelfHandling";

/**
 * Controller for the accountant settings.
 */
export class CompanySettingsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SessionService", "SettingsService", "UiHelpers"];

  /** Possibility to restrict sections that are visible in settings. */
  public sections: SettingsSection[];

  /** Yearly sidecosts */
  public sideCosts = Years.getYearlyChangingNumbers(new Date()).sideCosts;

  /** The current editable settings */
  private _current: CompanyAccountSettings;

  private saves: { [key: string]: boolean } = {};

  constructor(private sessionService: SessionService, private settingsService: SettingsService, private uiHelpers: UiHelpers) {
  }

  /**
   * Initializes the controller
   */
  public $onInit() {
    this.sections = this.sections || ["pensionContracts", "insuranceContracts", "incomesRegister"];
  }

  /** Copy current from SettingsService */
  public get current(): CompanyAccountSettings {
    if (!this._current && this.settingsService.current) {
      this._current = angular.copy(this.settingsService.current);
    }
    return this._current;
  }

  /** Returns current pension contracts once loaded assuring there is at least one active item. */
  public get pensionContracts(): PensionContract[] | null {
    if (!this.current) {
      return null;
    }
    if (!CompanySettingsController.getActive(this.current.calc.pensionContracts)) {
      this.current.calc.pensionContracts.push({});
    }
    return this.current.calc.pensionContracts;
  }

  /** Returns current insurance contracts once loaded assuring there is at least one active item. */
  public get insuranceContracts(): InsuranceContract[] | null {
    if (!this.current) {
      return null;
    }
    if (!CompanySettingsController.getActive(this.current.calc.insuranceContracts)) {
      this.current.calc.insuranceContracts.push({});
    }
    return this.current.calc.insuranceContracts;
  }

  /**
   * Returns true, if the contract should be visible:
   * Companies: All pending contracts and first active contract.
   * Personal: The first active contract, no pending contracts.
   * @param contract Contract to evaluate
   * @param allContracts Collection of contracts that contain the contract to evaluate.
   */
  public isVisibleContract<ContractType extends PensionContract | InsuranceContract>(contract: ContractType, allContracts: ContractType[]): boolean {
    if (contract.status === SettingsStatus.Pending) {
      // Show all pending for companies. For households, this is an error / legacy.
      return this.sessionService.isInRole("company") ? true : false;
    }
    return contract === CompanySettingsController.getActive(allContracts);
  }

  /** Gets the relevant contracts that are shown in personal (household) userinterface. */
  public get personal(): {
    insurance: InsuranceContract,
    pension: PensionContract,
  } {
    const result = {
      insurance: null,
      pension: null,
    }
    if (!this.current) {
      return result;
    }
    result.insurance = CompanySettingsController.getActive(this.insuranceContracts);
    result.pension = CompanySettingsController.getActive(this.pensionContracts);
    return result;
  }

  /**
   * Saves changes for settings
   * @param section Section to save: Copies only that section to settingsService.
   * Null saves nothing.
   */
  public save(section: SettingsSection | "all" | null) {
    switch (section) {
      case "pensionContracts":
        this.settingsService.current.calc.pensionContracts = angular.copy(this.current.calc.pensionContracts);
        this.settingsService.current.payments.customerFunds.isPensionSelfHandling = this.current.payments.customerFunds.isPensionSelfHandling;
        break;
      case "insuranceContracts":
        this.settingsService.current.calc.insuranceContracts = angular.copy(this.current.calc.insuranceContracts);
        break;
      case "incomesRegister":
        this.settingsService.current.calc.incomesRegister = angular.copy(this.current.calc.incomesRegister);
        break;
      case "eInvoice":
        this.settingsService.current.payments.invoice.eInvoiceReceiver = this.current.payments.invoice.eInvoiceReceiver;
        this.settingsService.current.payments.invoice.eInvoiceIntermediator = this.current.payments.invoice.eInvoiceIntermediator;
        break;
      case "sepa":
        this.settingsService.current.payments.invoice.sepaBankPartyId = this.current.payments.invoice.sepaBankPartyId;
        this.settingsService.current.payments.invoice.ibanNumber = this.current.payments.invoice.ibanNumber;
        break;
      case "taxAndSocialSecuritySelfHandling":
        this.settingsService.current.payments.customerFunds.isTaxAndSocialSecuritySelfHandling = this.current.payments.customerFunds.isTaxAndSocialSecuritySelfHandling;
        break;
      case "workerSelfHandling":
        this.settingsService.current.payments.customerFunds.isWorkerSelfHandling = this.current.payments.customerFunds.isWorkerSelfHandling;
        break;
      case "unemploymentSelfHandling":
        return;
      case "all":
        this.settingsService.current = angular.copy(this.current);
        break;
    }
    if (section) {
      this.saves[section] = true;
      this.settingsService.save().then((data) => {
        switch (section) {
          case "pensionContracts":
            this.current.calc.pensionContracts = angular.copy(data.calc.pensionContracts);
            this.current.payments.customerFunds.isPensionSelfHandling = data.payments.customerFunds.isPensionSelfHandling;
            break;
          case "insuranceContracts":
            this.current.calc.insuranceContracts = angular.copy(data.calc.insuranceContracts);
            break;
          case "incomesRegister":
            this.current.calc.incomesRegister = angular.copy(data.calc.incomesRegister);
            break;
          case "eInvoice":
            this.current.payments.invoice.eInvoiceReceiver = data.payments.invoice.eInvoiceReceiver;
            this.current.payments.invoice.eInvoiceIntermediator = data.payments.invoice.eInvoiceIntermediator;
            break;
          case "sepa":
            this.current.payments.invoice.sepaBankPartyId = data.payments.invoice.sepaBankPartyId;
            this.current.payments.invoice.ibanNumber = data.payments.invoice.ibanNumber;
            break;
          case "taxAndSocialSecuritySelfHandling":
            this.current.payments.customerFunds.isTaxAndSocialSecuritySelfHandling = data.payments.customerFunds.isTaxAndSocialSecuritySelfHandling;
            break;
          case "workerSelfHandling":
            this.current.payments.customerFunds.isWorkerSelfHandling = data.payments.customerFunds.isWorkerSelfHandling;
            break;
          case "all":
            this._current = angular.copy(data);
            break;
        }
        this.saves[section] = false;
      });
    }
  }

  /**
   * Sets pension number for test account to the given contract
   * @param item Pension contract
   */
  public setPensionNumberForTest(item: PensionContract) {
    item.contractNumber = SettingsLogic.getPensionNumberForTest(item.company);
  }

  /**
   * Shows the eInvoice selection dialog
   */
  public selectEInvoiceAddress() {
    this.settingsService.selectEInvoiceAddress(this.current.payments.invoice);
  }

  /**
   * Gets the progress of save operation.
   * @param section Section to check for saving.
   * Note that "all" will check only "all" operations, not underlying sub-sections saving ("any").
   */
  public isSaving(section: SettingsSection | "all"): boolean {
    return !!(this.saves[section] || this.saves["all"]);
  }

  private static getActive<ContractType extends PensionContract | InsuranceContract>(contracts: ContractType[]): ContractType {
    const today = Dates.getToday();
    return contracts?.find((x) => x.status !== SettingsStatus.Pending &&
      today >= x.startDate || "2000-01-01" && today < (x.endDate || "2100-01-01"));
  }
}