import * as angular from "angular";

import { Dates, LocalTapiolaInsuranceOrder, LocalTapiolaInsuranceOrderType, PartnerServices } from "@salaxy/core";

import { UiHelpers, WizardService, WizardStep } from "../../services";

import { WizardController } from "../bases/WizardController";

/**
 * Wizard for sending Local Tapiola Insurance Order
 */
export class InsuranceWizardController extends WizardController {

  /**
   * For NG-dependency injection
   */
  public static $inject = ["$scope", "WizardService", "PartnerServices", "UiHelpers"];


  /** Insurance action */
  public action: "newInsurance" | "moveInsurance" | "newInsuranceForEntrepreneur";

  /** Form data validity */
  public formDataValidity: boolean;

  /** Indicates if the order has been sent. */
  public isSent = false;

  /** Indicates if order is just sending */
  public isSending = false;

  /** Current Local Tapiola insurance order data. */
  public current: LocalTapiolaInsuranceOrder = null;


  /** Today (for datepicker minDate) */
  public today = Dates.getToday();

  /** Minimum date for new insurance contract start date (datepicker-options.minDate) */
  public insuranceDateOptions = {
    minDate: this.today,
  };

  /** Next possible dates for moving insurance to LocalTapiola */
  public insuranceStartingPointDateOptions = {}

  /** LocalTapiola Insurance wizard configuration */
  private wizardSteps: WizardStep[] = [
    { // 0
      title: "Turvaamme yrityksesi step",
      heading: "jotta voit keskittyä tärkeimpään",
      intro: "",
      active: true,
      view: "salaxy-components/modals/localTapiola/Start.html",
    },
    { // 1
      title: "Uusi työtapaturmavakuutus",
      heading: "Perustiedot",
      intro: "",
      view: "salaxy-components/modals/localTapiola/ContactInfo.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 2
      title: "Uusi työtapaturmavakuutus",
      heading: "Tunnistaminen",
      intro: "Selvitys yrityksen omistusrakenteesta",
      view: "salaxy-components/modals/localTapiola/Owner-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 3
      title: "Uusi työtapaturmavakuutus",
      heading: "Tunnistaminen",
      intro: "Yrityksen tosiasialliset edunsaajat",
      view: "salaxy-components/modals/localTapiola/Beneficiary-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    {
      // 4
      title: "Uusi työtapaturmavakuutus",
      heading: "Täydennä vakuutuksen tiedot",
      intro: "",
      view: "salaxy-components/modals/localTapiola/InsuranceInfo-new.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-send.html", //buttons send
    },
    {
      // 5
      title: "Yrittäjien tapaturmavakuutus",
      heading: "Täydennä vakuutuksen tiedot",
      intro: "",
      view: "salaxy-components/modals/localTapiola/Entrepreneur.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 6
      title: "Uusi työtapaturmavakuutus",
      heading: "Perustiedot",
      intro: "",
      view: "salaxy-components/modals/localTapiola/ContactInfo-entrepreneur.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 7
      title: "Yrittäjien tapaturmavakuutus",
      heading: "Tunnistaminen",
      intro: "Selvitys yrityksen omistusrakenteesta",
      view: "salaxy-components/modals/localTapiola/Owner-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 8
      title: "Yrittäjien tapaturmavakuutus",
      heading: "Tunnistaminen",
      intro: "Yrityksen tosiasialliset edunsaajat",
      view: "salaxy-components/modals/localTapiola/Beneficiary-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-send.html",
    },
    { // 9
      title: "Työtapaturmavakuutuksen siirto LähiTapiolaan",
      heading: "",
      intro: "Perustiedot",
      view: "salaxy-components/modals/localTapiola/ContactInfo.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 10
      title: "Työtapaturmavakuutuksen siirto LähiTapiolaan",
      heading: "Tunnistaminen",
      intro: "Selvitys yrityksen omistusrakenteesta",
      view: "salaxy-components/modals/localTapiola/Owner-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 11
      title: "Työtapaturmavakuutuksen siirto LähiTapiolaan",
      heading: "Tunnistaminen",
      intro: "Yrityksen tosiasialliset edunsaajat",
      view: "salaxy-components/modals/localTapiola/Beneficiary-info.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-default.html",
    },
    { // 12
      title: "Työtapaturmavakuutuksen siirto LähiTapiolaan",
      heading: "",
      intro: "",
      view: "salaxy-components/modals/localTapiola/InsuranceInfo-move.html",
      buttonsView: "salaxy-components/modals/localTapiola/buttons-send.html",
    },
    { // 13
      title: "Kiitos!",
      heading: "",
      intro: "",
      view: "salaxy-components/modals/localTapiola/ThankYou.html",

    },
  ];

  private newSteps = [
    this.wizardSteps[0], // start
    this.wizardSteps[1], // contact new
    this.wizardSteps[2], // owner info
    this.wizardSteps[3], // beneficiary info
    this.wizardSteps[4], // additional info
    this.wizardSteps[13], // thank you
  ];

  private newEntrepreneurSteps = [
    this.wizardSteps[0], // start
    this.wizardSteps[5], // additional info entrepreneur
    this.wizardSteps[6], // contact info
    this.wizardSteps[7], // owner info
    this.wizardSteps[8], // beneficiary info
    this.wizardSteps[13], // thank you
  ];

  private moveSteps = [
    this.wizardSteps[0], // start
    this.wizardSteps[9], // contact info (move)
    this.wizardSteps[10], // owner info
    this.wizardSteps[11], // beneficiary info
    this.wizardSteps[12], // additional info (move)
    this.wizardSteps[13], // thank you
  ];

  /**
   * Creates a new WizardController
   * @param $scope - The Angular scope
   * @param wizardService - Maintains the state of the wizard
   * @param partnerServices - Local Tapiola insurance orders API
   * @param uiHelpers - Salaxy UI helpers
   */
  constructor(
    $scope: angular.IScope,
    wizardService: WizardService,
    private partnerServices: PartnerServices,
    private uiHelpers: UiHelpers,
  ) {
    super($scope, wizardService);
  }

  /**
   * Sets the current wizard action and wizard steps
   * @param insuranceAction - Action to performs: order new or move existing insurance.
   */
  public setInsuranceAction(insuranceAction: "newInsurance" | "moveInsurance" | "newInsuranceForEntrepreneur") {
    this.isSending = false;
    this.isSent = false;
    this.formDataValidity = true;
    this.action = insuranceAction;
    this.getStartingPointDates();
    this.current.id = null;
    if (this.current.company.beneficiaries.length === 0) {
      this.current.company.beneficiaries.push({});
    }
    if (this.current.company.owners.length === 0) {
      this.current.company.owners.push({});
    }


    switch (insuranceAction) {
      case "newInsurance":
        this.current.items = [
          {
            orderType: LocalTapiolaInsuranceOrderType.NewWorkerInsurance,
            occupations: [{}],
          }
        ]
        this.wizardService.setSteps(this.newSteps);
        break;
      case "moveInsurance":
        this.current.items = [
          {
            orderType: LocalTapiolaInsuranceOrderType.MoveWorkerInsurance,
            occupations: [{}],
          }
        ]
        this.wizardService.setSteps(this.moveSteps);
        break;
      case "newInsuranceForEntrepreneur":
        this.current.items = [
          {
            orderType: LocalTapiolaInsuranceOrderType.NewEntrepreneurInsurance,
            occupations: [{}],
          }
        ]
        this.wizardService.setSteps(this.newEntrepreneurSteps);
        break;
    }
  }

  /**
   * Controller initializations
   */
  public $onInit = () => {
    this.partnerServices.getNewLocalTapiolaInsuranceOrder().then((data) => {
      this.current = data;
    });
  }


  /** Send the current order. */
  public send() {

    if (this.isSending) {
      return;
    }
    if (this.isSent) {
      this.goNext();
      return;
    }
    this.isSending = true;

    const loading = this.uiHelpers.showLoading("Odota...");

    this.partnerServices.sendLocalTapiolaInsuranceOrder(this.current).then(() => {
      loading.dismiss();
      this.goNext();
      this.isSent = true;
      this.isSending = false;
    });
  }

  /**
   * Get next possible dates for moving existing insurance contract to LocalTapiola
   */
  public getStartingPointDates() {
    // Quarter starting dates as strings
    const asap = Dates.getTodayMoment();
    const firstDate = Dates.getTodayMoment().startOf("quarter").add(1, "quarter");
    const secondDate = Dates.getTodayMoment().startOf("quarter").add(2, "quarter");
    const thirdDate = Dates.getTodayMoment().startOf("quarter").add(3, "quarter");
    const fourthDate = Dates.getTodayMoment().startOf("quarter").add(4, "quarter");


    this.insuranceStartingPointDateOptions = {
      [asap.format("YYYY-MM-DD")]: "Ensimmäinen mahdollinen ajankohta",
      [firstDate.format("YYYY-MM-DD")]: firstDate.format("D.M.YYYY"),
      [secondDate.format("YYYY-MM-DD")]: secondDate.format("D.M.YYYY"),
      [thirdDate.format("YYYY-MM-DD")]: thirdDate.format("D.M.YYYY"),
      [fourthDate.format("YYYY-MM-DD")]: fourthDate.format("D.M.YYYY"),
    }

  }

  /** Go to the next step */
  public goNext() {
    // TODO remove when ready and use super
    this.step++;
    //super.goNext();
  }

  /**
   * Go to the last step ("thank you" step)
   */
  public goToLast() {
    this.step = this.steps.length;
  }
  /**
   * Navigates to the previous step if possible and saves the data.
   */
  public goPrevious() {
    super.goPrevious();
  }

  /**
   * Returns true, if the given order type exists in the current order items.
   * @param orderTypes - Order type to check.
   * @returns True, if the order type is found, otherwise default.
   */
  public hasOrderType(...orderTypes: LocalTapiolaInsuranceOrderType[]): boolean {
    return (orderTypes ?? []).some((x) => !!(this.current.items ?? []).find((i) => i.orderType === x));
  }

  /** Returns true if user can go forward in wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity) {
          return true;
        }
      }
    }
    return false;
  }

  /** Opens the insurance dialog. */
  public openModal() {
    this.wizardService.setSteps([this.wizardSteps[0]]);
    this.step = 1;
    this.uiHelpers.showDialog(
      "salaxy-components/modals/account/Insurance.html",
      "InsuranceWizardController",
      null,
      null,
      "lg");
  }

  /**
   * Validates the order.
   */
  public validate(): Promise<LocalTapiolaInsuranceOrder> {
    return this.partnerServices.validateLocalTapiolaInsuranceOrder(this.current).then((order) => {
      this.current = order;
      return this.current;
    });
  }

  /**
   * Resets the current order.
   */
  public reset() {
    // this.current = null;
  }
}
