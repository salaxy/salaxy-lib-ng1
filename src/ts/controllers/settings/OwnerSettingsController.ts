import * as angular from "angular";

import { Arrays, BeneficialOwners, BeneficialOwnersApi } from "@salaxy/core";

/**
 * Handles user interfaces for Owner and Beneficiary lists
 */
export class OwnerSettingsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["BeneficialOwnersApi", "$timeout"];

  /** Mode */
  public mode: "byPercent" | "byAmount" = "byAmount";

  /** Current owners. */
  public current: BeneficialOwners = null;

  private saves: { [key: string] : boolean } = {};

  constructor(private beneficialOwnersApi: BeneficialOwnersApi, private $timoeut: angular.ITimeoutService
  ) {
  }

  /**
   * Controller initializations
   */
  public $onInit = () => {

    this.beneficialOwnersApi.get().then((data) => {
      this.current = data;
    });
  }

  /**
   * Total amount of user input shares
   */
  public getTotalAmount(): number {
    if (!this.mode || !this.current) {
      return 0;
    }
    return Arrays.sum(this.current.owners, (x) => x.numberOfShares ?? 0);
  }

  /**
   * Total percentage of user input shares
   */
  public getTotalPercent(): number {
    if (!this.mode || !this.current) {
      return 0;
    }
    return Arrays.sum(this.current.owners, (x) => x.percentOfShares ?? 0);
  }

  /**
   * Calculates percentage from the given share amount
   */
  public getPercentFromAmount(numberOfShares: number): number {
    if (this.mode == 'byAmount' && this.current && this.current.ownedCompanyDetails.totalNumberOfShares != null) {
      return (numberOfShares / this.current.ownedCompanyDetails.totalNumberOfShares) * 100;
    }
    return 0;
  }

  /**
   * Calculates share amount from the given share percentage
   */
  public getAmountFromPercent(percentOfShares: number): number {
    if (this.mode == 'byPercent' && this.current && this.current.ownedCompanyDetails.totalNumberOfShares != null) {
      return this.current.ownedCompanyDetails.totalNumberOfShares * (percentOfShares / 100);
    }
    return 0;
  }

  /**
   * update
   */
  public recalculateShares() {
    if (!this.mode || !this.current) {
      return;
    }
    if (this.mode == 'byPercent') {
      for (const singleOwner of this.current.owners) {
        singleOwner.numberOfShares = this.current.ownedCompanyDetails.totalNumberOfShares * ( singleOwner.percentOfShares / 100 );
      }
    }
    if (this.mode == 'byAmount') {
      for (const singleOwner of this.current.owners) {
        singleOwner.percentOfShares = ( singleOwner.numberOfShares / this.current.ownedCompanyDetails.totalNumberOfShares ) * 100;
      }
    }
  }

  /** Saves the current owners and beneficiaries to backend. */
  public save(target: "owners" | "beneficiaries") {
    this.saves[target] = true;
    this.beneficialOwnersApi.save(this.current).then( (data) => {
      this.current = data;
      this.saves[target] = false;
    })
  }

  /** Indicates the progress of save operation. */
  public isSaving(target: "owners" | "beneficiaries"): boolean {
    return this.saves[target] || false;
  }

}