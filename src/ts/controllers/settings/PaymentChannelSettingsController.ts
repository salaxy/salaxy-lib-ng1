import { Avatar, Configs, InvoicesLogic, PaymentChannel, PaymentChannelSettings, PaymentChannelSettingsInfo, PensionCompany } from "@salaxy/core";

import { EditDialogKnownActions, SessionService, SettingsService, UiHelpers } from "../../services";

/**
 * Handles user interfaces for specifying settings for Payment Channels (and other payment related settings).
 */
export class PaymentChannelSettingsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["SettingsService", "UiHelpers", "SessionService"];

  private lastPaymentChannelWindow: PaymentChannel;

  private _siteChannels: PaymentChannelSettings[] = null;

  constructor(
    private settingsService: SettingsService,
    private uiHelpers: UiHelpers,
    private sessionService: SessionService,
  ) {
  }

  /**
   * Controller initializations
   */
  public $onInit = () => {
    // none
  }

  /**
   * Gets the current settings object as soon as it is loaded.
   * Note that this is null until the settings have been loaded from server.
   */
  public get current() {
    return this.settingsService.current;
  }

  /**
   * If the channels are set site specific (e.g. custom sites), the settings cannot be saved.
   * Site specific changes are done to session, and exposed by InvoicesService
   */
  public get isReadOnly() {
    // no settings
    if (!this.current) {
      return true;
    }
    // partner override
    if (this.current.partner.serviceModel.features.payments && !this.current.payments.denyServiceModel) {
      return true
    }
    // site override
    if (this.isSiteSpecific) {
      return true;
    }

    return false;
  }

  /** Returns true, if the settings are site specific. */
  public get isSiteSpecific() {
    const enabled = this.current.payments.channels.filter( (x) => x.isAvailable && x.isEnabled);
    const sameLength = enabled.length === this.sessionService.getSession()?.settings?.paymentChannels?.length;
    if (sameLength && enabled.length > 0) {
      const sameIds = this.sessionService.getSession().settings.paymentChannels.every((x) => enabled.some((c) => c.channel === x));
      if (sameIds) {
        return false;
      }
    }
    return true;
  }

  /**
   * Gets the payment channel.
   * @param type Type of the channel to list:
   *
   * - enabled: Enabled channels
   * - available: Available channels that can be enabled (have not yet been enabled).
   * - unavailable: Unavailable channels (cannot be enabled)
   * - all: Really all channels, even the technical ones that cannot be enabled.
   */
  public getPaymentChannels(type: "all" | "available" | "enabled" | "unavailable"): PaymentChannelSettings[] {
    if (!this.current) {
      return null;
    }
    let all: PaymentChannelSettings[] = [];
    if (this.isSiteSpecific) {
      if (this._siteChannels === null) {
        this._siteChannels = this.sessionService.getSession().settings.paymentChannels.map<PaymentChannelSettings>((c) => ({ channel: c, isEnabled: true, isAvailable: true }));
      }
      all = all.concat(this._siteChannels);
    } else {
      all = all.concat(this.current.payments.channels);
    }
    all = all.filter((x) =>
      x.channel !== PaymentChannel.PalkkausManual ||
      (x.channel === PaymentChannel.PalkkausManual && this.sessionService.isInRole("pro"))
    );

    switch (type) {
      case "all":
        return all;
      case "available":
        return all.filter((x) => x.isAvailable && !x.isEnabled);
      case "unavailable":
        return all.filter((x) => !x.isAvailable);
      case "enabled":
      default:
        return all.filter((x) => x.isAvailable && x.isEnabled);
    }
  }

  /** Gets the visual representation and language versioned UI texts of the payment channel */
  public getAvatar(channel: PaymentChannel): Avatar {
    return InvoicesLogic.getChannelAvatar(channel);
  }

  /** Gets the default channel. */
  public get defaultChannel() {
    return this.sessionService.getSession().settings.defaultPaymentChannel;
  }

  /** Opens the channel setup dialog in a new window. */
  public openChannelSetupDialog(channel: PaymentChannel) {
    let url = null;
    let isExternal = false;
    switch (channel) {
      case PaymentChannel.Test:
        isExternal = true;
        url =
          Configs.current.isTestData ?
            "https://test-integrations.salaxy.com/test#/dialogs/settings" :
            "https://integrations.salaxy.com/test#/dialogs/settings";
        url += "?token=" + this.sessionService.getCurrentToken();
        break;
      case PaymentChannel.Procountor:
        isExternal = true;
        url =
          Configs.current.isTestData ?
            "https://test-integrations.salaxy.com/procountor/settings.html" :
            "https://integrations.salaxy.com/procountor/settings.html";
        url += "?token=" + this.sessionService.getCurrentToken();
        break;
      case PaymentChannel.VismaNetvisor:
        isExternal = true;
        url =
          Configs.current.isTestData ?
            "https://test-integrations.salaxy.com/vismanetvisor/settings.html" :
            "https://integrations.salaxy.com/vismanetvisor/settings.html";
        url += "?token=" + this.sessionService.getCurrentToken();
        break;
      default:
        break;
    }

    const channelInEdit = this.getPaymentChannels("all").find((x) => x.channel === channel);
    if (!channelInEdit) {
      this.uiHelpers.showAlert("Maksutapaa ei voi muokata", "Palkanlaskentasivuston asetukset estävät maksutavan muokkauksen.");
      return;
    }

    const p = this.current.payments;
    const supportedCompanies = [PensionCompany.None, PensionCompany.Ilmarinen, PensionCompany.Elo, PensionCompany.Varma];
    const isPensionIncludedSupported = this.current.calc.pensionContracts
      .every((x) => supportedCompanies.indexOf(x.company) >= 0);

    // TODO: Go through this data model and see what really is necessary?
    const channelSettings: PaymentChannelSettingsInfo | any = {
      id: channelInEdit.channel,
      isDefault: channelInEdit.channel == this.defaultChannel,
      settings: {
        data: channelInEdit.data,
        isAvailable: channelInEdit.isAvailable,
        isEnabled: channelInEdit.isEnabled,
        accountingTargetId: channelInEdit.accountingTargetId,
      },
      customerFundsSettings: {
        isPensionIncludedSupported,
        isPensionSelfHandling: p.customerFunds.isPensionSelfHandling,
        isTaxAndSocialSecuritySelfHandling: p.customerFunds.isTaxAndSocialSecuritySelfHandling,
        isWorkerSelfHandling: p.customerFunds.isWorkerSelfHandling,
      },
      invoiceSettings: {
        eInvoiceIntermediator: p.invoice.eInvoiceIntermediator,
        eInvoiceReceiver: p.invoice.eInvoiceReceiver,
        ibanNumber: p.invoice.ibanNumber,
        sepaBankPartyId: p.invoice.sepaBankPartyId,
      },
      isReadOnly: this.isReadOnly,
    };

    const saveSettings = (action: EditDialogKnownActions | "set-default", data: PaymentChannelSettingsInfo | any) => {

      if (action === EditDialogKnownActions.Cancel) {
        console.debug("Canceling: Remove this message");
        return;
      }
      if (data.id !== this.lastPaymentChannelWindow) {
        throw new Error(`Invalid response from channel ${data.id || 'undefined'}, expected ${this.lastPaymentChannelWindow}.`);
      }

      let channelToSave = p.channels.find((x) => x.channel === channel);
      if (!channelToSave) {
        channelToSave = channelInEdit;
        p.channels.push(channelToSave);
      }

      if (action === EditDialogKnownActions.Ok || action === "set-default") {

        channelToSave.data = data.settings.data;
        channelToSave.isEnabled = channelToSave.isAvailable && true;
        channelToSave.accountingTargetId = data.settings.accountingTargetId;
        if (action === "set-default") {
          p.defaultChannel = channel;
        }

        p.customerFunds.isPensionSelfHandling = data.customerFundsSettings.isPensionSelfHandling;
        p.customerFunds.isTaxAndSocialSecuritySelfHandling = data.customerFundsSettings.isTaxAndSocialSecuritySelfHandling;
        p.customerFunds.isWorkerSelfHandling = data.customerFundsSettings.isWorkerSelfHandling;

        p.invoice.eInvoiceIntermediator = data.invoiceSettings.eInvoiceIntermediator;
        p.invoice.eInvoiceReceiver = data.invoiceSettings.eInvoiceReceiver;
        p.invoice.ibanNumber = data.invoiceSettings.ibanNumber;
        p.invoice.sepaBankPartyId = data.invoiceSettings.sepaBankPartyId;

        this.settingsService.save();
      }
      if (action === EditDialogKnownActions.Delete) {
        channelInEdit.isEnabled = false;
        // TODO: Make sure that server-side does not allow deleting last channel and that it sets the default channel if none is selected.
        this.settingsService.save();
      }
    }

    this.lastPaymentChannelWindow = channel;
    if (isExternal) {
      this.uiHelpers.showExternalDialog(channelSettings.id, url, channelSettings).then((dialogResult) => {
          saveSettings(dialogResult.action as EditDialogKnownActions, dialogResult.item);
      });
    } else {
      this.uiHelpers.openEditDialog(`salaxy-components/modals/settings/PaymentChannel.html`, channelSettings,
        {
          channelAvatar: InvoicesLogic.getChannelAvatar(channelSettings.id),
          selectEInvoiceAddress: (itemRef) => {
            this.settingsService.selectEInvoiceAddress(itemRef);
          },
        }
      ).then((dialogResult) => {
          saveSettings(dialogResult.action as EditDialogKnownActions, dialogResult.item);
      });
    }
  }
}
