﻿
import * as angular from "angular";

import { Dates, VarmaPensionOrder, VarmaPensionOrderAction } from "@salaxy/core";

import { UiHelpers, VarmaPensionService, WizardService, WizardStep } from "../../services";

import { WizardController } from "../bases/WizardController";

/**
 * Wizard for choosing Varma pension
 */
export class VarmaPensionWizardController extends WizardController {

/**
 * For NG-dependency injection
 * @ignore
 */
  public static $inject = ["$scope", "WizardService", "VarmaPensionService", "UiHelpers"];

  /** Form data validity */
  public formDataValidity: boolean;

  /** Indicates if the order has been sent. */
  public isSent = false;

  /** Indicates if order is just sending */
  public isSending = false;

  /** Pension action */
  public action: VarmaPensionOrderAction;


  /** Next possible dates for moving pension contract to Varma */
  public pensionStartingPointDateOptions = {}

  /** Company wizard configuration */
  private wizardSteps: WizardStep[] = [
    {
      title: "Varmassa asiakkuus kannattaa aina",
      heading: "",
      intro: "",
      active: true,
      view: "salaxy-components/modals/varma/Start.html",
      buttonsView: "salaxy-components/modals/varma/buttons-start.html",
    },
    {
      title: "YEL-vakuutus",
      heading: "",
      intro: "Tarkista esitäytetyt omat tietosi",
      view: "salaxy-components/modals/varma/YelStep1.html",
      buttonsView: "salaxy-components/modals/varma/buttons-prev-next.html",
    },
    {
      title: "YEL-vakuutus",
      heading: "",
      intro: "Täydennä vakuutuksen tiedot",
      view: "salaxy-components/modals/varma/YelStep2.html",
      buttonsView: "salaxy-components/modals/varma/buttons-new-send.html",
    },
    {
      title: "TyEL-vakuutus",
      heading: "",
      intro: "Tarkista esitäytetyt yrityksen ja yhteyshenkilön tiedot",
      view: "salaxy-components/modals/varma/TyelStep1.html",
      buttonsView: "salaxy-components/modals/varma/buttons-prev-next.html",
    },
    {
      title: "TyEL-vakuutus",
      heading: "",
      intro: "Täydennä vakuutuksen tiedot",
      view: "salaxy-components/modals/varma/TyelStep2.html",
      buttonsView: "salaxy-components/modals/varma/buttons-new-send.html",
    },
    {
      title: "Vakuutuksen siirto",
      heading: "",
      intro: "Valitse siirrettävä vakuutus ja tarkista esitäytetyt tietosi",
      view: "salaxy-components/modals/varma/MoveStep1.html",
      buttonsView: "salaxy-components/modals/varma/buttons-move.html",
    },
    {
      title: "YEL-vakuutuksen siirto",
      heading: "",
      intro: "Täydennä vakuutuksen tiedot",
      view: "salaxy-components/modals/varma/MoveYel.html",
      buttonsView: "salaxy-components/modals/varma/buttons-move-send.html",
    },
    {
      title: "TyEL-vakuutuksen siirto",
      heading: "",
      intro: "Täydennä vakuutuksen tiedot",
      view: "salaxy-components/modals/varma/MoveTyel.html",
      buttonsView: "salaxy-components/modals/varma/buttons-move-send.html",
    },
    {
      title: "Kiitos vakuutuksen ottamisesta.",
      heading: "Tervetuloa Varman asiakkaaksi!",
      intro: "",
      view: "salaxy-components/modals/varma/EndYel.html",
      buttonsView: "salaxy-components/modals/varma/buttons-end.html",
    },
    {
      title: "Kiitos vakuutuksen ottamisesta.",
      heading: "Tervetuloa Varman asiakkaaksi!",
      intro: "",
      view: "salaxy-components/modals/varma/EndTyel.html",
      buttonsView: "salaxy-components/modals/varma/buttons-end.html",
    },
    {
      title: "Kiitos vakuutuksen ottamisesta.",
      heading: "Tervetuloa Varman asiakkaaksi!",
      intro: "",
      view: "salaxy-components/modals/varma/EndMove.html",
      buttonsView: "salaxy-components/modals/varma/buttons-end.html",
    },
  ];

  private tyelSteps = [
    this.wizardSteps[0],
    this.wizardSteps[3],
    this.wizardSteps[4],
    this.wizardSteps[9],
  ];

  private yelSteps = [
    this.wizardSteps[0],
    this.wizardSteps[1],
    this.wizardSteps[2],
    this.wizardSteps[8],
  ];

  private mvmtSteps = [
    this.wizardSteps[0],
    this.wizardSteps[5],
    this.wizardSteps[6],
    this.wizardSteps[7],
    this.wizardSteps[10],
  ];

  constructor(
    $scope: angular.IScope,
    wizardService: WizardService,
    private pensionService: VarmaPensionService,
    private uiHelpers: UiHelpers,
  ) {
    super($scope, wizardService);
  }

/**
 * Sets the current wizard action and wizard steps
 * @param pensionAction - Action to performs: order new TyEL, YEL or move pension.
 */
  public setPensionAction(pensionAction: VarmaPensionOrderAction) {
    this.formDataValidity = true;
    this.pensionService.reset();
    this.action = pensionAction;
    this.getStartingPointDates();
    switch (pensionAction) {
      case VarmaPensionOrderAction.NewTyel:
        this.wizardService.setSteps(this.tyelSteps);
        break;
      case VarmaPensionOrderAction.NewYel:
        this.wizardService.setSteps(this.yelSteps);
        break;
      case VarmaPensionOrderAction.Move:
        this.wizardService.setSteps(this.mvmtSteps);
        break;
    }
  }

/**
 * Sets steps in the move action.
 */
  public setMvmtSteps() {
    const mvmtSteps = [
      this.wizardSteps[0],
      this.wizardSteps[5],
    ];
    if (this.model.yel.isMove) {
      mvmtSteps.push(this.wizardSteps[6]);
    }
    if (this.model.tyel.isMove) {
      mvmtSteps.push(this.wizardSteps[7]);
    }
    mvmtSteps.push(this.wizardSteps[10]);
    this.wizardService.setSteps(mvmtSteps);
    this.wizardService.activeStepNumber = 2;
  }

/**
 * The pension order.
 */
  public get model(): VarmaPensionOrder {
    return this.pensionService.getCurrent();
  }

  /**
   * Get next possible dates for moving existing pension contract to Varma
   */
  public getStartingPointDates() {

    const firstValue = "q" + Dates.getTodayMoment().startOf("quarter").add(1, "quarter").quarter();
    const secondValue = "q" + Dates.getTodayMoment().startOf("quarter").add(2, "quarter").quarter();
    const thirdValue = "q" + Dates.getTodayMoment().startOf("quarter").add(3, "quarter").quarter();
    const fourthValue = "q" + Dates.getTodayMoment().startOf("quarter").add(4, "quarter").quarter();

    // Quarter starting dates as strings
    const firstDate = Dates.getTodayMoment().startOf("quarter").add(1, "quarter").format("D.M.YYYY");
    const secondDate = Dates.getTodayMoment().startOf("quarter").add(2, "quarter").format("D.M.YYYY");
    const thirdDate = Dates.getTodayMoment().startOf("quarter").add(3, "quarter").format("D.M.YYYY");
    const fourthDate = Dates.getTodayMoment().startOf("quarter").add(4, "quarter").format("D.M.YYYY");


    this.pensionStartingPointDateOptions = {
      "asap": "Ensimmäinen mahdollinen ajankohta",
      [firstValue]: firstDate,
      [secondValue]: secondDate,
      [thirdValue]: thirdDate,
      [fourthValue]: fourthDate,
    }

  }

  /** Send the current order. */
  public send() {
    if (this.isSending) {
      return;
    }
    if (this.isSent) {
      this.goNext();
      return;
    }
    this.isSending = true;
    this.model.action = this.action;
    if (this.model.action === VarmaPensionOrderAction.Move) {
      if (this.model.yel.isMove) {
        this.model.yel.personalId = this.model.tyel.personalId;
      }
    }

    const loading = this.uiHelpers.showLoading("Odota...");
    this.pensionService.send().then(() => {
      loading.dismiss();
      if (this.model.validation.errors.length > 0) {
        this.uiHelpers.showAlert("Tarkista tiedot", "Tilaukset tiedot ovat puutteelliset, tarkista tiedot.");
      } else {
        this.goNext();
        this.isSent = true;
      }
      this.isSending = false;
    });
  }

  /** Returns true if user can go forward in wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity) {
          return true;
        }
      }
    }
    return false;
  }

/**
 * Navigates to the next step if possible and saves the data.
 */
  public goNext() {
    super.goNext();
  }

/**
 * Navigates to the previous step if possible and saves the data.
 */
  public goPrevious() {
    super.goPrevious();
  }

  /** Navigates to specified step */
  public goToStep(step) {
    this.step = step;
  }

  /** Opens the pension dialog. */
  public openModal() {

    this.wizardService.setSteps([this.wizardSteps[0]]);
    this.step = 1;

    this.uiHelpers.showDialog(
      "salaxy-components/modals/account/Pension.html",
      "VarmaPensionWizardController",
      null,
      null,
      "lg");
  }

}
