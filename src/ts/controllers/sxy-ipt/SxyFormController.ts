import { OpenAPIV3 } from "openapi-types";

import { ApiValidation, ApiValidationError, ApiValidationErrorType, FormHelpers, InputMetadata, JsonInputType, JsonSchemaProperty, JsonSchemaUtils, Objects, Translations } from "@salaxy/core";

import { JsonSchemaService, Ng1Translations, UiHelpers } from "../../services";

/**
 * Sxy form contains the definition for an editor used interface that
 * typically corresponds to an HTML form.
 */
export class SxyFormController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["JsonSchemaService", "Ng1Translations", "$rootScope", "UiHelpers"];

  /** The model that is being edited. */
  public model: any;

  /** The schema model */
  public schemaModel: OpenAPIV3.SchemaObject;

  /**
   * The translation code for the label / title.
   * Default is "SALAXY.MODEL.[type].title"] or if not available, just the type text.
   */
  public label: string;

  /**
   * Sets the entire form as read-only. The value is true/1.
   * Default value false/0, has no real effect: The behavior comes from either input or schema.
   * Individual field may still be set read-only=false/1 to make it editable.
   */
  public readOnly: string;

  /**
   * Type of the model: Bases for the user interface.
   * This may be either a TypeScript typename or OpenApi schema component name.
   */
  public type: string;

  /** If true, the schema loading is ready: The form can be rendered. */
  public isReady: boolean;

  /**
   * URL for the Open API document that contains the definition for the type.
   * The url can be an absolute url, relative url or an url with salaxy-secure/ -prefix.
   * The salaxy-secure/ -prefix will be replaced in runtime with the salaxy api server url of the curren environment.
   */
  public openApi: string;

  /** If true, shows the debugger view for the form. */
  public debugger: boolean;

  /** Unique id for the form. Init assures that this is unique in the AngularJs root scope. */
  public id: string;

  /** The angular form with validation etc. */
  public ngForm: angular.IFormController;

  private lastUniqueId = 0;

  constructor(
    private jsonSchemaService: JsonSchemaService,
    private translations: Ng1Translations,
    private $rootScope: angular.IRootScopeService,
    private uiHelpers: UiHelpers,
  ) {
    // Dependency injection
  }

  /** Initializes the controller */
  public $onInit() {
    this.init();
  }

  /** Initializes or re-initializes the data model. */
  public init() {
    this.model = this.model || {};
    this.id = (this.type || "undefined") + this.$rootScope.$id.toString();
    if (this.openApi && this.type) {
      this.jsonSchemaService.assureSchemaDocument(this.openApi).then(() => {
        this.schemaModel = this.jsonSchemaService.findSchema(this.openApi, this.type);
        if (!this.model) {
          this.model = {};
        }
        this.label = this.label || this.translations.getWithDefault(`SALAXY.MODEL.${this.type}.title`, this.type);
        this.isReady = true;
      });
    } else {
      this.schemaModel = null;
      this.isReady = true;
    }
  }

  /**
   * Registers an input (control) to a data point in the form.
   * @param path Data path that the input registers to.
   * This should be either "form" to register to the root of the form or
   * more typically "form.prop.perhapsSubProperty" to register to a property.
   */
  public registerInput(path: string, type?: JsonInputType, format?: string): {
    /** Description of the property or if path is "form" the data model of the form. */
    prop: JsonSchemaProperty,
    /** True, if the element is the form root. Otherwise this is a property.  */
    isRoot: boolean,
    /** Unique ID within the form */
    id: string,
    /** AngularJS template for rendering the input. */
    templateUrl: string,
    /** Metadata that can be used for rendering the input */
    input: InputMetadata,
  } {
    if (!(path || "").trim()) {
      throw Error("Path / model is required for each sxy-input element. Use e.g. 'form.temp'.");
    }
    const result = {
      prop: this.getProperty(path),
      id: null,
      isRoot: false,
      templateUrl: null,
      input: null,
    };
    if (!result.prop) {
      // This is not a property
      result.isRoot = path === "form";
      if (result.isRoot) {
        // "form": bound to root
        result.id = `${this.id}_form${this.getUniqueId()}`;
        result.input = this.schemaModel ? FormHelpers.getInputForSelf(this.schemaModel, null, path) : null;
        if (result.input) {
          result.input.format = format || result.input.format;
        }
        result.prop = {
          isRequired: false,
          parentName: this.type,
          propertyName: null,
          schema: this.schemaModel,
        };
        result.templateUrl = result.input ? this.jsonSchemaService.getTemplate(result.input) : null;
      } else {
        // Property path is there, but it is not found in the schema (perhaps a new or unofficial property)
        const propertyName = path.substr(path.lastIndexOf(".") + 1);
        result.id = `${this.id}_${propertyName}${this.getUniqueId()}`;
        type = type || "string";
        result.input = {
          name: propertyName,
          format,
          isEnum: null,
          path,
          type,
          content: `Property "${path}" does not exist in schema.`,
        };
        result.prop = {
          isRequired: false,
          parentName: null,
          propertyName,
          schema: null,
        };
        result.templateUrl = this.jsonSchemaService.getTemplate(result.input);
      }
    } else {
      result.id = `${this.id}_${result.prop.propertyName}${this.getUniqueId()}`;
      result.input = FormHelpers.getInputMetadata(result.prop.schema, result.prop.propertyName, path);
      result.input.format = format || result.input.format;
      result.templateUrl = this.jsonSchemaService.getTemplate(result.input);
    }
    this.registeredInputs[result.id] = result.prop;
    return result;
  }

  /**
   * Gets the model for a property that an invidual input within the form can bind to.
   * @param path Property path, currently always starting with "form.",
   * e.g. "form.id" or "form.employer.accountId"
   */
  public getProperty(path: string): JsonSchemaProperty {
    const propPath = this.getFormPath(path);
    if (!propPath) {
      return null; // Path is "form" - this is not a property.
    }
    return JsonSchemaUtils.getProperty(this.schemaModel, propPath);
  }

  /**
   * Sets a value in the model using property path.
   * @param path Property path, currently always starting with "form.",
   * e.g. "form.id" or "form.owner.id"
   * @param value Value to set to model.
   * @returns The assigned value or void if the property cannot be set.
   */
  public setValue(path: string, value: any): any | void {
    const propPath = this.getFormPath(path);
    if (!propPath) {
      throw new Error("Cannot set the objects own value. setValue only supports setting property values.");
    }
    return Objects.setProperty(this.model, propPath, value);
  }

  /**
   * Gets a value from the model using property path.
   * @param path Property path, currently always starting with "form.",
   * e.g. "form.id" or "form.owner.id"
   */
  public getValue(path: string): any {
    const propPath = this.getFormPath(path);
    if (!propPath) {
      return this.model;
    }
    return Objects.getProperty(this.model, this.getFormPath(path));
  }

  /** Gets the validation based on the current form. */
  public getValidation(): ApiValidation {
    const rawErrors: string[][] = [];
    if (this.ngForm.$invalid) {
      for (const key of Object.keys(this.ngForm.$error)) {
        for (let index = 0; index < this.ngForm.$error[key].length; index++) {
          rawErrors.push([key, this.ngForm.$error[key][index].$name]);
        }
      }
    }
    return this.uiHelpers.cache(this, "validation", () => {
      const errors = rawErrors.map((raw) => {
          const key = raw[0];
          const prop = this.registeredInputs[raw[1]];
          const error: ApiValidationError = {
            code: key,
            key: raw[1],
            type: raw[0] === "required" ? ApiValidationErrorType.Required : ApiValidationErrorType.Invalid,
            msg: Translations.getWithDefault("SALAXY.VALIDATION.ValidationErrors." + key,
              Translations.get("SALAXY.VALIDATION.ValidationErrors.unknown", { error: key })),
          };
          if (prop?.propertyName) {
            if (prop.parentName) {
              // TODO: Better form path from a combination of prop/input
              error.key = prop.parentName + "." + prop.propertyName;
            } else {
              error.key = prop.propertyName;
            }
            error.msg = Translations.getWithDefault(`SALAXY.MODEL.${error.key}.title`, prop.propertyName) + ": " + error.msg;
          }
          return error;
      });
      return {
        hasAllRequiredFields: !errors.find((x) => x.type === ApiValidationErrorType.Required),
        isValid: errors.length === 0,
        errors,
      };
    }, () => rawErrors);
  }

  protected registeredInputs: {
    [key: string]: JsonSchemaProperty;
  } = {};

  private getFormPath(path: string): string {
    if (path === "form") {
      return null;
    }
    if (!path.startsWith("form.")) {
      throw new Error("Currently, only 'form' and 'form.' supported in property paths.");
    }
    return path.replace("form.", "");
  }

  /** Gets a unique id to form components / inputs. This may later further track the inputs. */
  private getUniqueId(): number {
    return this.lastUniqueId++;
  }
}
