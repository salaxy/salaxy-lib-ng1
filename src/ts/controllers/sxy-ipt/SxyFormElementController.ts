import { Ng1Translations } from "../../services";

/**
 * Controller for misc form elements that are not inputs:
 * These are not bound to models, but handle misc. form layout and grouping tasks.
 */
export class SxyFormElementController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [];

  /**
   * Typing depending on the component.
   *
   * - sxy-alert: "default" | "primary" | "success" | "info" | "warning" | "danger"
   */
  public type: "default" | "primary" | "success" | "info" | "warning" | "danger" | string;

  /**
   * For sxy-fieldset, defines the model that the fieldset should generate fields for.
   * Should be "form" to refer to the current form or property path starting
   * with "form", e.g. "form.result.employerCalc"
   */
  public model: string;

  /**
   * For alerts, possibility to speicfy a font-awesome icon.
   * Setting "none", will show no icon.
   * If not set, it is determined by type.
   */
  public icon: "none" | string;

  constructor(private translations: Ng1Translations) {
    // Dependency injection
  }

  /** Initializes the controller */
  public $onInit() {
    // init
  }

  /** For alert component, gets the classes that format the alert. */
  public getAlertClasses() {
    switch (this.type) {
      case "default":
      case "primary":
      case "success":
      case "info":
      case "warning":
      case "danger":
        return `alert alert-${this.type}`;
      case "error":
        return `alert alert-danger`;
      default:
        return `alert alert-info`;
    }
  }

  /** Returns the icon based on the type. */
  public getAlertIcon() {
    if (!this.icon) {
      switch (this.type) {
        case "danger":
        case "error":
          return "fa-exclamation";
        case "success":
          return "fa-check";
        case "warning":
          return "fa-exclamation-triangle";
        default:
          return "fa-info";
      }
    }
    if (this.icon === "none") {
      return null;
    }
    return this.icon;
  }
}
