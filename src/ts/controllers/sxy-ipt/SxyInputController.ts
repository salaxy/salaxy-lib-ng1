import * as angular from "angular";
import { OpenAPIV3 } from "openapi-types";

import { Arrays, EnumerationsLogic, FormHelpers, InputMetadata, JsonInputType, JsonSchemaUtils } from "@salaxy/core"

import { EditDialogKnownActions, EditDialogResult, Ng1Translations, UiHelpers } from "../../services";
import { SxyFormController } from "./SxyFormController";
import { InputEnumOption } from "../form-controls";

/** Handles user interface logic for different form inputs components (including select etc.) */
export class SxyInputController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Ng1Translations", "UiHelpers", "$transclude", "$element"];

  /** Default bindings to the components */
  public static bindings = {
    /**
     * Model in the form. Currently always starts with "form." to bind to main data object of the form.
     * For fieldset, this may also be just "form" to bind to main model in form (not a property).
     */
    model: "@",

    /** Possibility to override the label. */
    label: "@",

    /**
     * Type of the input component as one of JSON schema types:
     * 'boolean', 'object', 'number', 'string', 'integer' or 'array'.
     * Typically, this will be inferred from the model, but you may set it explicitly if the property is not yet in the model.
     */
    type: "@",

    /**
     * Format of the input component as JSON schema format:
     * Examples include "multiline", "date" (string) and "radio", "checkbox" (boolean).
     */
    format: "@",

    /**
     * True or false forces the read-only mode on the input regardless of the default value.
     * This "read-only" attribute behaves differently than the HTML "readonly" attribute:
     * The value is boolean (true/1 or false/0). Non existent attribute or empty/null uses
     * the default value from metadata (API model) or from the form.
     * Also, the input will become fully read-only view without any form control.
     */
    readOnly: "@",

    /**
     * True or false forces the disabled mode on the input.
     * This "disable" attribute behaves differently than the HTML "disabled" attribute:
     * The value is boolean (true/1 or false/0). Non existent attribute or empty/null
     * uses the default value: Currently always false, but may later come from metadata.
     */
    disable: "@",

    /**
     * True or false forces the required mode on the input.
     * This "require" attribute behaves differently than the HTML "required" attribute:
     * The value is boolean (true/1 or false/0). Non existent attribute or empty/null
     * uses the default value from metadata (API model).
     */
    require: "@",

    /**
     * Minimum value of the field
     */
    minimum: "@",

    /**
     * Maximum value of the field
     */
    maximum: "@",

    /**
     * Multiple of (step) of the value
     */
    multipleOf: "@",

    /**
     * For arrays, fieldsets and other components that enumerate children,
     * defines which children are shown (taken to inputs collection).
     * Comma separated list of property names.
     */
    include: "@",

    /**
     * For arrays, fieldsets and other components that enumerate children,
     * defines which children are hidden (excluded from inputs collection).
     * Comma separated list of property names.
     */
    exclude: "@",
  };

  /** Auto-generated unique id of the input */
  public id: string;

  /**
   * Auto-generated name of the input.
   * Currently, the same as ID, but this may change in the future.
   * E.g. to make it more compatible with form POST (no id numbers etc.).
   */
  public name: string;

  /** Model in the form. Currently always starts with "form." to bind to main data object of the form. */
  public model: string;

  /**
   * Possibility to override the label (default is "SALAXY.MODEL.[TypeName].[propName].title").
   * Translated by default, if translation not found, shown as is.
   * If starts with dot, will be interpreted as just the last part of the key,
   * e.g. ".specialLabel" will fetch key "SALAXY.MODEL.[TypeName].[propName].specialLabel".
   */
  public label: string;

  /** Schema of the property */
  public schemaModel: OpenAPIV3.SchemaObject | null;

  /** Name of the parent schema (data model). Used in language keys. */
  public parentSchemaName: string;

  /** Name of the property to which this input is bound in the parentSchema. */
  public propertyName: string;

  /** SxyForm component that holds the data model and submit / reset logic etc. */
  public form: SxyFormController;

  /**
   * Type of the input component as one of JSON schema types:
   * 'boolean', 'object', 'number', 'string', 'integer' or 'array'.
   * Typically, this will be inferred from the model, but you may set it explicitly if the property is not yet in the model.
   */
  public type: JsonInputType;

  /**
   * Format of the input component as JSON schema format:
   * Examples include "multiline", "date" (string) and "radio", "checkbox" (boolean).
   */
  public format: string;

  /**
   * The read-only value as the user set it in the "read-only" attribute
   * True or false forces the read-only mode on the input regardless of the default value or
   * the read-only attribute set in the sxy-form level.
   * This "read-only" attribute behaves differently than the HTML "readonly" attribute:
   * The value is boolean (true/1 or false/0). Non existent attribute or empty/null uses
   * the default value from metadata (API model) or from the form.
   * Also, the input will become fully read-only view without any form control.
   */
  public readOnly: string;

  /**
   * True or false forces the disabled mode on the input.
   * This "disable" attribute behaves differently than the HTML "disabled" attribute:
   * The value is boolean (true/1 or false/0). Non existent attribute or empty/null
   * uses the default value: Currently always false, but may later come from metadata.
   */
  public disable: string;

  /**
   * True or false forces the disabled mode on the input.
   * This "require" attribute behaves differently than the HTML "required" attribute:
   * The value is boolean (true/1 or false/0). Non existent attribute or empty/null
   * uses the default value from metadata (API model).
   */
  public require: string;

  /**
   * Minimum value
   */
  public minimum: number | null;

  /**
   * Maximum value
   */
  public maximum: number | null;

  /**
   * Multiple of (step) of the value
   */
  public multipleOf: number | null;

  /** Template URl that is used for rendering the input. */
  public templateUrl: string;

  /** Input metadata from the schema resolving process. */
  public input: InputMetadata;

  /**
   * For arrays, fieldsets and other components that enumerate children,
   * defines which children are shown (taken to inputs collection).
   * Comma separated list of property names.
   */
  public include: string | string[];

  /**
   * For arrays, fieldsets and other components that enumerate children,
   * defines which children are hidden (excluded from inputs collection).
   * Comma separated list of property names.
   */
  public exclude: string | string[];

  constructor(
    private translations: Ng1Translations,
    private uiHelpers: UiHelpers,
    private $transclude: angular.ITranscludeFunction,
    private $element: JQLite,
  ) {
    // Dependency injection
  }

  /** Initializes the controller */
  public $onInit() {
    this.initModel();
  }

  /**
   * Gets the formatted value for the input depending on the data type.
   * @param childName For object arrays, gets the value for the specified child input.
   * @param index For object the index of the item in the array.
   * May later support other child scenarios.
   */
  public formatValue(childName?: string, index: number = null) {
    if (childName && index != null) {
      const arrayItemsType = ((this.schemaModel as OpenAPIV3.ArraySchemaObject)?.items as OpenAPIV3.SchemaObject);
      if (arrayItemsType) {
        const childType = arrayItemsType.properties[childName] as OpenAPIV3.SchemaObject;
        const childvalue = this.value[index][childName];
        if (childvalue && childType.format && childType.enum?.length) {
          return EnumerationsLogic.getEnumLabel(childType.format, childvalue);
        }
        // TODO: Add number formatting etc. here as necessary.
        return childvalue;
      }
      return `#ERR: formatValue not an array ${childName}, ${index}.`;
    }
    return "#ERR: TODO Add other formatters here";
  }

  /**
   * Gets the label for the input.
   * Default is "SALAXY.MODEL.[TypeName].[propName].title" in the dictionary.
   * @param childName For object arrays, gets the label for the specified child input.
   * May later support other child scenarios.
   */
  public getLabel(childName?: string) {
    if (childName) {
      const arrayItemsType = ((this.schemaModel as OpenAPIV3.ArraySchemaObject)?.items as OpenAPIV3.SchemaObject);
      // TODO: We would need to move the type information away from format.
      if (arrayItemsType?.type === "object" && arrayItemsType.format) {
        return this.translations.getWithDefault(`SALAXY.MODEL.${arrayItemsType.format}.${childName}.title`, "#" + childName);
      }
      if (this.propertyName) {
        return this.translations.getWithDefault(`SALAXY.MODEL.${this.parentSchemaName}.${this.propertyName}.${childName}.title`, "#" + childName);
      }
      if (this.parentSchemaName) {
        return this.translations.getWithDefault(`SALAXY.MODEL.${this.parentSchemaName}.${childName}.title`, "#" + childName);
      }
      return "#ERR: " + childName;
    }
    if (this.label) {
      if (this.label[0] === "." && this.label[1] !== "." && this.propertyName) {
        this.label = `SALAXY.MODEL.${this.parentSchemaName}.${this.propertyName}${this.label}`;
      }
      return this.translations.get(this.label);
    }
    if (this.propertyName) {
      return this.translations.getWithDefault(`SALAXY.MODEL.${this.parentSchemaName}.${this.propertyName}.title`, "#" + this.propertyName);
    }
    if (this.parentSchemaName) {
      return this.translations.getWithDefault(`SALAXY.MODEL.${this.parentSchemaName}.title`, "#" + this.parentSchemaName);
    }
    return "#ERR: " + this.model;
  }

  /** Gets the description for the input */
  public getDescr(): string {
    return this.translations.getWithDefault(`SALAXY.MODEL.${this.parentSchemaName}.${this.propertyName}.descr`,
      this.schemaModel?.description);
  }

  /** Gets the read-only value based on defaults and explicit values. */
  public getReadOnly(): boolean {
    const readOnlyValue = (this.readOnly || "").trim().toLowerCase();
    if (readOnlyValue === "true" || readOnlyValue === "1") {
      return true;
    }
    if (readOnlyValue === "false" || readOnlyValue === "0") {
      return false;
    }
    const readOnlyFormValue = (this.form.readOnly || "").trim().toLowerCase();
    if (readOnlyFormValue === "true" || readOnlyFormValue === "1") {
      return true;
    }
    if (this.schemaModel?.readOnly != null) {
      return this.schemaModel.readOnly;
    }
    // Default logic - this might later expand and/or some stuff may move to server.
    const readOnlyFields = ["id", "createdAt", "updatedAt", "isReadOnly", "owner", "partner"];
    if (readOnlyFields.indexOf(this.propertyName) >= 0) {
      return true;
    }
    return false;
  }

  /** Gets the minimum value */
  public getMinimum(): number {
    if (this.minimum != null) {
      return this.minimum;
    }
    if (this.schemaModel?.minimum != null) {
      return this.schemaModel.minimum;
    }
    return null;
  }

  /** Gets the maximum value */
  public getMaximum(): number {
    if (this.maximum != null) {
      return this.maximum;
    }
    if (this.schemaModel?.maximum != null) {
      return this.schemaModel.maximum;
    }
    return null;
  }

  /** Gets the multipleOf value */
  public getMultipleOf(): number {
    if (this.multipleOf != null) {
      return this.multipleOf;
    }
    if (this.schemaModel?.multipleOf != null) {
      return this.schemaModel.multipleOf;
    }
    return null;
  }

  /** Gets the minimum length value for string input */
  public getMinLength(): number {
    if (this.schemaModel?.minLength != null) {
      return this.schemaModel.minLength;
    }
    return null;
  }

  /** Gets the maximum length value for string input */
  public getMaxLength(): number {
    if (this.schemaModel?.maxLength != null) {
      return this.schemaModel.maxLength;
    }
    return null;
  }

  /** Gets the minimum length value for string input */
  public getPattern(): string {
    if (this.schemaModel?.pattern != null) {
      return this.schemaModel.pattern;
    }
    return null;
  }
  /** Gets the require value based on defaults and explicit values. */
  public getRequire(): boolean {
    const requireValue = (this.require || "").trim().toLowerCase();
    if (requireValue === "true" || requireValue === "1") {
      return true;
    }
    if (requireValue === "false" || requireValue === "0") {
      return false;
    }
    return false;
  }

  /** Gets the disable value based on defaults and explicit values. */
  public getDisable(): boolean {
    const disableValue = (this.disable || "").trim().toLowerCase();
    if (disableValue === "true" || disableValue === "1") {
      return true;
    }
    if (disableValue === "false" || disableValue === "0") {
      return false;
    }
    return false;
  }

  /** Gets or sets the value in the model */
  public get value(): any {
    return this.form?.getValue(this.model);
  }
  public set value(value: any) {
    if (this.form) {
      this.form.setValue(this.model, value);
    }
  }

  /**
   * For datatype "object", gets the inputs for child properties.
   * For datatype "array", gets the inputs for items.
   * For other types, gets an array with single input for that type
   * (TODO: Does this make sense? It is essentially the same as parent).
   * @param defaultTop Returns top n children.
   * If include is specified (fields are hand-picked), this proeprty is ignored.
   */
  public getChildInputs(defaultTop: number): InputMetadata[] {
    return this.uiHelpers.cache(this, "childInputs", () => {
      if (!this.schemaModel) {
        return null;
      }
      let result: InputMetadata[];
      if (this.schemaModel.type === "object") {
        result = FormHelpers.getInputsForObject(this.schemaModel, this.model);
      } else if (JsonSchemaUtils.isArraySchemaObject(this.schemaModel)) {
        result = FormHelpers.getInputsForArray(this.schemaModel, this.model);
      } else {
        result = [FormHelpers.getInputForSelf(this.schemaModel, null, "form")];
      }
      const includeArr = Arrays.assureArray(this.include);
      if (includeArr.length > 0) {
        result = includeArr.map((x) => result.find((y) => y.name === x)).filter((x) => !!x);
        defaultTop = includeArr.length;
      }
      const excludeArr = Arrays.assureArray(this.exclude);
      if (excludeArr.length > 0) {
        result = result.filter((x) => excludeArr.indexOf(x.name) < 0);
      }
      return result.slice(0, defaultTop);
    })
  }

  /**
   * Shows an edit dialog for item.
   * @param item Item to edit
   * @param index Index of the item in the current model array if available.
   */
  public edit(item: any, index: number): Promise<EditDialogResult<any>> {
    // HACK: This now only works for array/object => Make more generic.
    const logic = {
      openApi: this.form.openApi,
      type: ((this.schemaModel as OpenAPIV3.ArraySchemaObject)?.items as OpenAPIV3.SchemaObject)?.format,
      title: index < 0 ? "Lisää uusi rivi" : "Muokkaa riviä",
      showDelete: index > -1 && !this.getReadOnly(),
      isReadOnly: this.getReadOnly(),
      debugger: this.form.debugger,
    };
    if (this.$transclude?.isSlotFilled("form")) {
      let html = "";
      this.$transclude((transEl, transScope) => { // eslint-disable-line @typescript-eslint/no-unused-vars
        angular.forEach(transEl, (textOrElem) => {
          if (textOrElem.tagName === "FORM") {
            html += textOrElem.innerHTML;
          }
        });
        html = html.trim();
        return null as any;
      }, this.$element, "form");
      const template = `<div class="modal-header">
      <button type="button" class="close" ng-click="$close('cancel')" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">{{ $ctrl.logic.title }}</h4>
    </div>
    <div class="modal-body">
      <sxy-form open-api="{{ $ctrl.logic.openApi }}" type="{{ $ctrl.logic.type }}" auto-gen="false" debugger="$ctrl.logic.debugger"
        model="$ctrl.current" read-only="{{$ctrl.logic.isReadOnly ? 'true' : ''}}">
        <div>${html || "Form tag contains no data"}</div>
      </sxy-form>
    </div>
    <div class="modal-footer">
      <button type="button" ng-if="$ctrl.logic.showDelete" class="btn btn-danger pull-left" ng-click="$close('delete-no-save')" sxy-text="SALAXY.UI_TERMS.delete"></button>
      <button type="reset" ng-if="!$ctrl.logic.isReadOnly" class="btn btn-default" ng-click="$close('cancel')" sxy-text="SALAXY.UI_TERMS.cancel"></button>
      <button type="button" ng-if="!$ctrl.logic.isReadOnly" class="btn btn-primary" ng-click="$close('ok')" sxy-text="SALAXY.UI_TERMS.ok"></button>
      <button type="button" ng-if="$ctrl.logic.isReadOnly" class="btn btn-primary" ng-click="$close('cancel')" sxy-text="SALAXY.UI_TERMS.close"></button>
    </div>`
      return this.uiHelpers.openEditDialog(null, item, logic, "", "EditDialogController", { template }).then((result) => {
        if (result.action === EditDialogKnownActions.DeleteNoCommit) {
          this.delete(item, index);
        }
        return result;
      });
    }
    return this.uiHelpers.openEditDialog("salaxy-components/sxy-form/SxyInput-dialog-edit.html", item, logic).then((result) => {
      if (result.action === EditDialogKnownActions.DeleteNoCommit) {
        this.delete(item, index);
      }
      return result;
    });
  }

  /**
   * Deletes an item (currently no "Are you sure?")
   * @param item Item to delete
   * @param index Index of the item in the current model array. This is currently required.
   */
  public delete(item: any, index: number): void {
    (this.value as []).splice(index, 1);
  }

  /** Creates a new item to the current array. */
  public createNew(): Promise<EditDialogResult<any>> {
    // HACK: This now only works for array/object => Make more generic.
    return this.edit({}, -1).then((result) => {
      if (result.action === "ok") {
        if (!this.value) {
          this.value = [];
        }
        (this.value as any[]).push(result.item);
      }
      return result;
    });
  }

  /**
   * Gets the options for enumerations based on schema.
   * For an array, gets the options for item enumeration.
   */
  public getEnumOptions(): InputEnumOption[] {
    // TODO: This is just quick partial implementation based on schema. Need to think through the different scenarios!
    return this.uiHelpers.cache(this, "getEnumOptions", () => {
      if (!this.schemaModel) {
        return [];
      }
      let enumArr = this.schemaModel.enum;
      let enumName = this.schemaModel.format;
      if (!enumArr || !enumArr.length) {
        enumArr = ((this.schemaModel as OpenAPIV3.ArraySchemaObject).items as OpenAPIV3.SchemaObject)?.enum;
        enumName = ((this.schemaModel as OpenAPIV3.ArraySchemaObject).items as OpenAPIV3.SchemaObject)?.format;
      }
      if (enumArr && enumArr.length > 0) {
        return enumArr.map(x => ({
          title: EnumerationsLogic.getEnumDescr(enumName, x),
          text: EnumerationsLogic.getEnumLabel(enumName, x),
          value: x,
        }));
      }
    }, () => this.schemaModel.type + this.schemaModel.format + this.schemaModel.enum?.length);
  }

  /**
   * Gets the format assuring that it is of supported type.
   * Currently, there is only special logic for enums.
   */
  public getFormat(): string {
    if (this.input.isEnum) {
      const supportedFormats = ["select", "radio", "multi-select", "typeahead", "list"];
      if (supportedFormats.indexOf(this.format) >= 0) {
        return this.format;
      }
      // Especially "enum", "default" and null.
      // TODO: Exact logic requires go-through.
      return "select";
    }
    return this.format;
  }

  private initModel() {
    if (!this.model) {
      throw new Error(`Input not bound to model.`);
    }
    if (!this.form) {
      this.input = {
        type: "error",
        content: "No sxy-form for input with model " + this.model,
        name: this.model,
        path: this.model,
        isEnum: false,
        format: null,
      };
      this.name = this.id = this.input.name;
      this.templateUrl = "salaxy-components/sxy-form/error/default.html";
      this.schemaModel = null;
      return;
    }
    const reg = this.form.registerInput(this.model, this.type, this.format);
    this.schemaModel = reg.prop.schema || null;
    this.parentSchemaName = reg.prop.parentName;
    this.name = this.id = reg.id;
    this.propertyName = reg.prop.propertyName;
    if (reg.prop.isRequired && !(this.require || "").trim()) {
      this.require = "true"; // Set from schema if not explicitly set.
    }
    this.input = reg.input;
    if (this.type && this.type !== this.input.type) {
      console.error(`Input type "${this.type}" not supported for property "${this.input.name}", defaulting to "${this.input.type}".`);
    }
    this.templateUrl = reg.templateUrl;
  }
}
