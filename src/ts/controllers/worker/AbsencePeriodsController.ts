import { AbsencePeriod, Arrays, Dates, EnumerationsLogic, HolidaysLogic, WorkerAbsences } from "@salaxy/core";

import { UiHelpers } from "../../services";

import { ListControllerBase, ListControllerBaseBindings } from "../bases";
import { CalendarSeries, CalendarUiEvent } from "../helpers";

/**
 * Controls the absences list (poissaolokirjanpito) for a selected holiday year.
 */
export class AbsencePeriodsController extends ListControllerBase<WorkerAbsences, AbsencePeriod> {

  /** Bindings for components that use this controller */
  public static bindings = (new class extends ListControllerBaseBindings {
    /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
    public condensed = "<";

    /** Date filter start value. Will be compared to period end date. */
    public filterStart = "<";

    /** Date filter end value. Will be compared to period start date. */
    public filterEnd = "<";
  }());

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers"];

  /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
  public condensed: boolean;

  /** Date filter start value. Will be applied to period start dates. */
  public filterStart: string;

  /** Date filter end value. Will be applied to period end dates. */
  public filterEnd: string;

  constructor(uiHelpers: UiHelpers) {
    super(uiHelpers);
  }

  /** List of items */
  public get list(): AbsencePeriod[] {
    if (!this.parent) {
      return null;
    }
    return this.parent.periods;
  }

  /** Maps WorkerAbsences to calendar series. */
  public mapToCalendar = (abs: WorkerAbsences): CalendarSeries[] => {
    return [{
      key: abs.id,
      title: abs.workerSnapshot?.displayName,
      avatar: abs.workerSnapshot,
      events: abs.periods.map((period) => ({
          start: period.period.start,
          end: period.period.end,
          summary: EnumerationsLogic.getEnumLabel("AbsenceCauseCode", period.causeCode),
          data: period,
        })),
    }];
  };

  /**
   * Filter that is applied to the list
   * @param value Item in the list
   */
  public filter = (value: AbsencePeriod) => {
    if (this.filterEnd && value.period.start > this.filterEnd) {
      return false;
    }
    if (this.filterStart && value.period.end < this.filterStart) {
      return false;
    }
    return true;
  }

  /** Creating of a new item. */
  public getBlank(): AbsencePeriod {
    return {
      period: {
        start: Dates.getToday(),
        end: Dates.getToday(),
        daysCount: 1,
      },
    };
  }

  /** Template for edit UI that is shown in a modal dialog. */
  public getEditDialogTemplateUrl() {
    return "salaxy-components/worker/AbsencePeriodsEditDialog.html";
  }

  /** Calendar is clicked => Show edit dialog accorging to calendar data. */
  public calendarClick(type: "event" | "day",  date: string, event: CalendarUiEvent) {
    switch (type) {
      case "event":
        this.showEditDialog(event.data);
        break;
      case "day": {
        const newItem = this.getBlank();
        newItem.period.start = date;
        newItem.period.end = date;
        this.showEditDialog(newItem, true);
        break;
      }
      default:
        throw new Error("Not supported: " + type);
    }
  }

  /** Gets the needed logic for Edit dialog */
  public getEditDialogLogic() {

    const updateIsHolidayAccrual = (current: AbsencePeriod): void => {
      current.isHolidayAccrual = current.isPaid;
    };

    return {
      /**
       * Returns true if the select component should be shown for absence isPaid.
       */
      canSelectIsPaid: (current: AbsencePeriod): boolean => {
        if (!current.causeCode) {
          return false;
        }
        const type = HolidaysLogic.getSalaryPayableType(current.causeCode);
        return type === "defaultPaid" || type === "defaultUnpaid" || type === "both";
      },

      /** Updates the IsPaid according to absence type. */
      updateIsPaid: (current: AbsencePeriod): void => {
        const type = HolidaysLogic.getSalaryPayableType(current.causeCode);
        switch (type) {
          case "defaultPaid":
          case "paid":
            current.isPaid = true;
            updateIsHolidayAccrual(current);
            break;
          case "defaultUnpaid":
          case "unpaid":
            current.isPaid = false;
            updateIsHolidayAccrual(current);
            break;
          case "both":
          default:
            // Keep existing
            break;
        }
      },

      updateIsHolidayAccrual,

      /** Updates the workdays count */
      updatePeriodDays: (current: AbsencePeriod) => {
        if (current.period.start > current.period.end) {
          current.period.end = current.period.start;
        }
        if (current.period.start && current.period.end) {
          if (current.period.days) {
            current.period.days = Dates.getWorkdays(current.period.start, current.period.end);
            current.period.daysCount = current.period.days.length;
          } else {
            current.period.daysCount = Dates.getWorkdays(current.period.start, current.period.end).length;
          }
        }
      },

      /** Sets the days array in period to enable days selection UI. */
      setDaysSelection: (period: AbsencePeriod) => {
        period.period.days = Dates.getWorkdays(period.period.start, period.period.end);
        period.period.daysCount = period.period.days.length;
      },
    };
  }

  /** Gets a total days calculation for different types. */
  public getTotalDays(type: "all" | "absencesPaid" | "absencesUnpaid" | "absencesHolidayAccrual" | "absencesNoHolidayAccrual" = "all") {
    const list = this.list.filter((x) => this.filter(x));
    switch (type) {
      case "all":
        return Arrays.sum(list, (x) => x.period.daysCount);
      case "absencesPaid":
        return Arrays.sum(list.filter((x) => x.isPaid), (x) => x.period.daysCount);
      case "absencesUnpaid":
        return Arrays.sum(list.filter((x) => !x.isPaid), (x) => x.period.daysCount);
      case "absencesHolidayAccrual":
        return Arrays.sum(list.filter((x) => x.isHolidayAccrual), (x) => x.period.daysCount);
      case "absencesNoHolidayAccrual":
        return Arrays.sum(list.filter((x) => !x.isHolidayAccrual), (x) => x.period.daysCount);
    }
    return null;
  }
}
