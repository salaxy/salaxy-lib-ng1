import * as angular from "angular";

import { Dates, EnumerationsLogic, Numeric, TaxCard2019Logic, TaxcardApprovalMethod, TaxcardKind, TaxcardListItem, Taxcards, TaxcardState, WorkerCurrentTaxcards } from "@salaxy/core";
import { Ng1Translations, UiCrudHelpers, UiHelpers } from "../../services";

/**
 * Helps listing current all tax cards for an employment relation (latest and previous ones).
 */
export class EmploymentTaxcardsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Taxcards", "UiCrudHelpers", "UiHelpers", "Ng1Translations"];

  /** The data after it has been loaded. Null if nothing is loaded yet.  */
  public current: WorkerCurrentTaxcards;

  /**
   * View mode:
   *
   * - "active" only shows the active taxcard.
   * - "list" only shows the history list.
   * - "all" (default) shows both active and history.
   */
  public mode: "active" | "list" | "all";

  /**
   * Today is explicitly defined, so that it can be changed in testing.
   */
  public today = Dates.getToday();

  private _employmentId: string;

  /** Creates a new EmploymentTaxcardsController */
  constructor(
    private taxcards: Taxcards,
    private uiCrudHelpers: UiCrudHelpers,
    private uiHelpers: UiHelpers,
    private translations: Ng1Translations) {}

  /** Initializes the controller. */
  public $onInit = () => {
    this.mode = this.mode || "all";
  }

  /**
   * Defines to which employment relation the functionality is bound to.
   * The setter also starts the loading process.
   */
  public get employmentId(): string {
    return this._employmentId;
  }
  public set employmentId(value: string) {
    this._employmentId = value;
    this.reload();
  }

  /** List of taxcards after it has been loaded. Null if nothing is loaded yet. */
  public get list(): TaxcardListItem[] {
    if (this.current) {
      return this.current.list;
    }
    return null;
  }

  /** Reloads the data from the server. */
  public reload() {
    this.current = null;
    if (this.employmentId) {
      this.taxcards.getEmploymentTaxcards(this.employmentId, this.today).then((result) => {
        this.current = result;
      });
    }
  }

  /**
   * Removes a taxcard from the list and does a refresh.
   * @param id Taxcard id.
   */
  public deleteTaxCard(id: string) {
    this.taxcards.delete(id).then(() => {
      this.reload();
    });

    try {
      alert("");
    }
    catch(ex) {
      alert("zsdfa");
    }
  }

  /**
   * Gets the tax card info text for active taxcard.
   * NOTE: This is called outside the component, from the Worker details component / view.
   */
  public getTaxCardInfo() {
    if (!this.current) {
      return null;
    }
    const taxCard = this.current.active;
    const result = {
      kind: taxCard ? taxCard.card.kind : TaxcardKind.Undefined,
      percent: TaxCard2019Logic.getIncomeLimitInfo(taxCard).percent,
      text: null as string,
    };
    if (result.kind === TaxcardKind.Undefined || result.kind === TaxcardKind.NoTaxCard || result.kind === TaxcardKind.NoWithholdingHousehold) {
      result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind);
    } else {
      result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind) + ": " + Numeric.formatPercent(result.percent);
    }
    return result;
  }

  /** Gets the info for a taxcard list item based on the index */
  public getInfo(listItem: TaxcardListItem) {
    const result = {
      classes: null as string,
      text: null as string,
      code: null as string,
    };
    if (listItem.flags.indexOf("active") >= 0) {
      result.classes = "bg-primary";
      result.text = "Käytössä";
    } else if (listItem.flags.indexOf("waiting") >= 0) {
      result.classes = "bg-warning";
      result.text = "Uusi työntekijältä";
    } else if (listItem.status === TaxcardState.SharedRejected || listItem.status === TaxcardState.SharedRejectedWithoutOpen) {
      result.classes = "bg-danger";
      result.text = "Hylätty";
      if (listItem === this.list[0]) {
        result.code = "latestRejected";
      }
    } else if (listItem.flags.indexOf("replaced") >= 0) {
      if ((listItem.data as any).validity !== "expired") {
        result.text = "Korvattu uudella";
      }
    } else {
      result.text = EnumerationsLogic.getEnumLabel("TaxcardValidity", (listItem.data as any).validity);
    }
    return result;
  }

  /** Shows a dialog / UI for viewing and then approving / rejecting a shared taxcard. */
  public showApproveShared() {
    this.approveShared(TaxcardApprovalMethod.AssureWaiting);
  }

  /** Shows a dialog / UI for viewing and then approving / rejecting an already rejected (not current) shared taxcard. */
  public showApproveRejected(item: TaxcardListItem) {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.taxcards.approveOwned(item.id, TaxcardApprovalMethod.AssureWaiting).then(() => {
      loader.dismiss();
      this.reload();
    });
  }

  /** Reject the shared taxcard. */
  public rejectShared() {
    this.approveShared(TaxcardApprovalMethod.Reject);
  }

  /**
   * Approve / reject the shared taxcard.
   */
  public approveShared(method: TaxcardApprovalMethod = TaxcardApprovalMethod.Approve) {
    if (!this.current || !this.current.waitingApproval || !this.current.waitingApproval.uri) {
      this.uiHelpers.showAlert("Ei jaettua verokorttia", "Työntekijällä ei ole jaettua verokorttia, jonka voisi hyväksyä / hylätä.");
      return;
    }
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    if (this.current.waitingApproval.fullCard) {
      this.taxcards.approveOwned(this.current.waitingApproval.fullCard.id, method).then(() => {
        loader.dismiss();
        this.reload();
      });
    } else {
      this.taxcards.approveShared(this.current.waitingApproval.uri, method).then(() => {
        loader.dismiss();
        this.reload();
      });
    }
  }

}
