import { Arrays, Brand, Dates, HolidayAccrualEntry, HolidayAccrualSource, HolidayYear, HolidayYears, Translations } from "@salaxy/core";

import { UiHelpers } from "../../services";

import { ListControllerBase, ListControllerBaseBindings } from "../bases";

/**
 * Controls holiday accrual (lomapäivien kertymä) of the annual leave for a selected holiday period.
 */
export class HolidayYearAccrualController extends ListControllerBase<HolidayYear, HolidayAccrualEntry> {

  /** Bindings for components that use this controller */
  public static bindings = new ListControllerBaseBindings();

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "HolidayYears",
    "UiHelpers",
  ];

  constructor(
    private holidayYears: HolidayYears,
    uiHelpers: UiHelpers,
  ) {
    super(uiHelpers);
  }

  /** List of items */
  public get list(): HolidayAccrualEntry[] {
    if (!this.parent || !this.parent.accrual || !this.parent.accrual.months) {
      return [];
    }
    return this.parent.accrual.months;
  }

  /** Creating of a new item. */
  public getBlank(): HolidayAccrualEntry {
    return {};
  }

  /**
   * Saves changes to the parent object using the CRUD API.
   */
  public saveParent(): void {
    const loader = this.uiHelpers.showLoading("Tallennetaan...");
    this.holidayYears.save(this.parent)
      .then(() => {
        loader.dismiss();
      });
  }

  /** Template for edit UI that is shown in a modal dialog. */
  public getEditDialogTemplateUrl() {
    return "salaxy-components/worker/holidays/HolidayYearAbsencesEditDialog.html";
  }

  /** Chart for eccrual overview. */
  public getChart(list: HolidayAccrualEntry[]) {
    const data = [[], []];
    const labels = [];
    const startSaldo: number = (this.parent.accrual as any).startSaldo;
    let cumulative = startSaldo;

    for (const entry of list) {
      cumulative += entry.daysAccrued;
      const startOfMonth = Dates.asDate(Dates.getTodayMoment().startOf("month"));
      data[0].push(entry.month <= startOfMonth ? cumulative : null);
      data[1].push(entry.month >= startOfMonth ? cumulative : null);
      labels.push(Translations.get("SALAXY.UI_TERMS.monthShort" + Dates.getMonth(entry.month)));
    }
    return {
      labels,
      series: ["Kertyneet", "Arvio tulevasta"],
      data,
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
          }],
        },
      },
    };
  }

  /** Gets the color for the row avatar */
  public getRowColor(row: HolidayAccrualEntry | "start" | "total") {
    if (row === "start" || row === "total") {
      return Brand.getBrandColor("primary");
    }
    if (row.month > this.getThisMonth()) {
      return "#ddd";
    }
    if (row.month === this.getThisMonth()) {
      return Brand.getBrandColor("danger");
    }
    return Brand.getBrandColor("primary");
  }

  /** Gets this month: The first date of this month */
  public getThisMonth() {
    return Dates.getToday().substr(0, 8) + "01";
  }

  /**
   * Returns the notes if available or, if not, description text based on the source.
   */
  public getAccrualNotes(row: HolidayAccrualEntry) {
    if (row.notes) {
      return row.notes;
    }
    switch (row.source) {
      case HolidayAccrualSource.Manual:
        return "Muokattu käsin";
      case HolidayAccrualSource.CalcDraft:
        return "Palkkalaskelma (luonnos)";
      case HolidayAccrualSource.CalcPaid:
        return "Maksettu palkka";
      case HolidayAccrualSource.Initial:
      default:
        if (row.month === this.getThisMonth()) {
          return "Kuluva kuukausi";
        }
        if (row.month > this.getThisMonth()) {
          return "Arvio tulevasta (lomavuoden luonti)";
        }
        return "Lomavuoden luonnista";
    }
  }

  /** Gets calculations related to holiday accruals */
  public getAccrualCalculations() {
    const startSaldo: number = (this.parent.accrual as any).startSaldo;
    const result = {
      accrual: Arrays.sum(this.list, (x) => x.daysAccrued),
      accrualToday: Arrays.sum(this.list.filter((x) => x.month < Dates.getToday()), (x) => x.daysAccrued),
      total: 0,
      totalToday: 0,
    };
    result.total = result.accrual + startSaldo;
    result.totalToday = result.accrualToday + startSaldo;
    return result;
  }
}
