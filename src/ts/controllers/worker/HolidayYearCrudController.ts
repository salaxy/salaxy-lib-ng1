﻿import * as angular from "angular";

import {
  Dates, HolidayBonusPaymentMethod, HolidayCode, HolidaysLogic, HolidaySpecification, HolidaySpecificationForYear,
  HolidayYear, HolidayYears, WageBasis
} from "@salaxy/core";

import { UiCrudHelpers, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { CalendarSeries } from "../helpers/calendar";

/**
 * Plain CRUD controller for HolidayYears.
 */
export class HolidayYearCrudController extends ApiCrudObjectController<HolidayYear> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "HolidayYears",
    "UiHelpers",
    "$location",
    "$routeParams",
    "UiCrudHelpers",
  ];

  /** View type currently shown in the UI */
  public viewType: "overview" | "accrual" | "holidays" | "absenses" = "overview";

  /**
   * If employmentId is set, the controller fetches all the holiday years for this employment relation.
   * The dafault of them will then be set to model and they can be switched easily e.g. using dropdown.
   */
  public employmentHolidayYears: HolidayYear[] = [];

  /** If getHolidayYears() is called, the result will be here */
  public allYears: HolidayYear[];

  /** If getHolidayYears() is called, the original unsaved result will be here */
  public allYearsOrig: HolidayYear[];

  /**
   * The date for which the UI is rendered. Default is today.
   * For employment holiday years (WorkerHolidays or CalcWorktime), set this to salary period begin
   * => Specifies the the holiday year to choose by default from all holiday years that Worker has:
   * If the date is Jan-April will show the previous year, May-Dec will show the current year.
   */
  public forDate: string;

  /** Parameters related to holiday report (lomalista). */
  public reportParams: {
    /** Today's date: Set this for testing how user interface is rendered (which view is presented) at different times of year. */
    today: string;
    /** Start date of the view: Can be get/set directly by the view */
    start: string,
    /** Start date of the view: Can be get/set directly by the view */
    end: string,
    /** Start date of the holiday year. Typically, just get: Set by the reportYear setter */
    yearStart: string,
    /** End date of the holiday year. Typically, just get: Set by the reportYear setter */
    yearEnd: string,
    /** Years that can be selected. */
    years: number[];
    /** Underlying field of reportView */
    _view: "summer" | "summerPeriod" | "winter" | "winterPeriod" | "list",
    /** Underlying field of reportYear */
    _year: number,
  };

  private _employmentId: string;

  private visualizationCache;
  constructor(
    private fullApi: HolidayYears,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private uiCrudHelpers: UiCrudHelpers,

  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /** Controller initialization */
  public $onInit() {
    this.forDate = this.forDate || Dates.getToday();
    const thisYear = Dates.getYear("today");
    this.reportParams = {
      today: Dates.getToday(),
      years: Array.from({ length: 5 }, (v, ix) => thisYear + 1 - ix),
    } as any;
    // Fetching reportView initializes the rest of parameters with default values. Comment for lint if necessary.
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const necessaryInitCall = this.reportView;
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/workers",
      detailsUrl: this.detailsUrl || "/workers/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/Holidays.html",
      oDataOptions: {},
    };
  }

  /**
   * Sets the employment ID and if it is different then starts the loading of the Absences object to that employment ID.
   */
  public set employmentId(value: string) {
    if (this._employmentId === value) {
      return;
    }
    this.employmentHolidayYears = [];
    if (!value) {
      if (this._employmentId) {
        this._employmentId = null;
        this.model = null;
        this.reload();
      }
      return;
    }
    this._employmentId = value;
    this.fullApi.getForEmployment(value).then((result) => {
      this.setHolidayYearsForEmployment(result);
    });
  }
  /** Gets the employment ID */
  public get employmentId() {
    return this._employmentId;
  }

  /** Initializes the holidays for the current Worker */
  public initHolidays(spec: HolidaySpecification) {
    const loader = this.uiHelpers.showLoading("Luodaan lomavuosia...");
    this.fullApi.initForEmployment(this.employmentId, spec)
      .then((years) => {
        this.setHolidayYearsForEmployment(years);
        loader.dismiss();
      });
  }

  /** Shows the Init holidays dialog for initializing the holiday years for the given worker */
  public showInitHolidays() {
    if (!this.employmentId) {
      throw Error("Employment ID not defined in showInitHolidays().");
    }
    const loader = this.uiHelpers.showLoading("Haetaan oletuksia...");
    this.fullApi.getDefaultSpecForEmployment(this.employmentId).then((defaults) => {

      loader.dismiss();
      const logic = {
        /** If true, has holiday years => A warning for overwrite should be shown. */
        hasData: !!this.current,
        /** True if the accrual is based on law. Ui binds to this. */
        accrualByLaw: !defaults.accrualFixed,
        /**
         * Sets the accrualFixed value based on accrualByLaw value
         * @param itemData The data that is being edited (current in view)
         */
        accrualByLawChanged: (itemData: HolidaySpecificationForYear) => {
          itemData.accrualFixed = logic.accrualByLaw ? null : (itemData.code === HolidayCode.TemporaryTimeOff ? 2 : 2.5);
        },
        /**
         * Sets the accrualByLaw value based on accrualFixed value
         * @param itemData The data that is being edited (current in view)
         */
        accrualFixedChanged: (itemData: HolidaySpecificationForYear) => {
          if (!itemData.accrualFixed) {
            itemData.accrualFixed = null; // in case of empty string or 0.
            logic.accrualByLaw = true;
          }
        },
        /**
         * Business logic for field visibility
         * @param itemData The data that is being edited (current in view)
         * @param field Name of the field for which the visibility is calculated.
         */
        getVisibility: (itemData: HolidaySpecificationForYear, field: "accrualFixed" | "compensation" | "bonusMethod" | "bonusPercent" | "startSaldo") => {
          switch (field) {
            case "startSaldo":
            case "accrualFixed":
            case "bonusMethod":
              return itemData.code === HolidayCode.Permanent14Days
                || itemData.code === HolidayCode.Permanent35Hours
                || itemData.code === HolidayCode.TemporaryTimeOff
                ;
            case "compensation":
              return itemData.code === HolidayCode.HolidayCompensation
                || itemData.code === HolidayCode.HolidayCompensationIncluded
                ;
            case "bonusPercent":
              return itemData.code !== HolidayCode.HolidayCompensationIncluded
                && itemData.code !== HolidayCode.NoHolidays
                && itemData.code !== HolidayCode.Undefined
                && itemData.bonusMethod !== HolidayBonusPaymentMethod.None
                ;
            default:
              return true;
          }
        },
      };

      this.uiHelpers.openEditDialog("salaxy-components/worker/holidays/WorkerHolidaysInitDialog.html", defaults, logic, "lg")
        .then((result) => {
          if (result.action === "ok") {
            this.initHolidays(result.item);
          }
        });
    });
  }

  /** Gets the visualisation data for the selected holiday year. */
  public getYearVisualisation() {
    if (!this.current) {
      return null;
    }
    if (!this.visualizationCache || this.visualizationCache.year !== this.current.year || this.visualizationCache.employmentId !== this.current.employmentId) {
      this.visualizationCache = HolidaysLogic.getYearVisualisation(this.current);
    }
    return this.visualizationCache;
  }

  /** Gets a total days calculation for different types. */
  public getTotalDays(type: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart" = "all", year: HolidayYear = null) {
    year = year || this.current;
    return HolidaysLogic.getPlannedLeavesCount(year, type);
  }

  /**
   * Gets items visibility based on properties of the selected holiday year.
   * @param elements Logical name of group of items in teh view.
   */
  public getVisibility(elements: "accrual" | "compensation" | "hourly") {
    const code = this.current.spec.code;
    switch (elements) {
      case "accrual":
        return code === HolidayCode.Permanent14Days
          || code === HolidayCode.Permanent35Hours
          || code === HolidayCode.TemporaryTimeOff
          ;
      case "hourly":
        return this.current.spec.wageBasis === WageBasis.Hourly
          || this.current.spec.wageBasis === WageBasis.PerformanceBased
          ;
      case "compensation":
        return code === HolidayCode.HolidayCompensation
          || code === HolidayCode.HolidayCompensationIncluded
          ;
      default:
        return true;
    }
  }

  /** For the reporting purposes, gets or sets the holiday year as year number. */
  public get reportYear(): number {
    if (!this.reportParams._year) {
      let year = Dates.getYear(this.reportParams.today || "today");
      if (Dates.getMonth(this.reportParams.today || "today") < 3) {
        year--;
      }
      this.reportYear = year; // Call the setter to init related values
    }
    return this.reportParams._year;
  }
  public set reportYear(value: number) {
    value = Math.round(Number(value));
    if (!value || value < 2018 || value > 2100) {
      return;
    }
    this.reportParams._year = value;
    this.reportParams.yearStart = value + '-05-01';
    this.reportParams.yearEnd = (value + 1) + '-04-30';
  }

  /**
   * Gets or sets the report view that is currently shown.
   */
  public get reportView(): "summer" | "summerPeriod" | "winter" | "winterPeriod" | "list" {
    if (!this.reportParams._view) {
      // Use the setter for setting the defaault value so that dependent fields are also set
      const month = Dates.getMonth(this.reportParams.today || "today");
      const year = Dates.getYear(this.reportParams.today || "today");
      if (year === this.reportYear) {
        if (month < 9) { this.reportView = "summer"; }
        else if (month === 12) { this.reportView = "winter"; } // Planning next Jan-May in december.
        else { this.reportView = "winterPeriod" }
      } else if (year - 1 === this.reportYear) {
        if (month < 4) { this.reportView = "winter"; } // Current holidays
        else if (month < 6) { this.reportView = "winterPeriod"; } // Current until 4, then pay salaries 5.
        else { this.reportView = "list" }
      } else {
        this.reportView = "list";
      }
    }
    return this.reportParams._view;
  }
  public set reportView(value: "summer" | "summerPeriod" | "winter" | "winterPeriod" | "list") {
    this.reportParams._view = value;
    switch (value) {
      case "summer":
        this.reportParams.start = this.reportYear + '-06-01';
        this.reportParams.end = this.reportYear + '-08-31';
        break;
      case "summerPeriod":
        this.reportParams.start = this.reportParams.yearStart;
        this.reportParams.end = this.reportYear + '-09-30';
        break;
      case "winter":
        this.reportParams.start = (this.reportYear + 1) + '-01-01';
        this.reportParams.end = (this.reportYear + 1) + '-03-31';
        break;
      case "winterPeriod":
        this.reportParams.start = this.reportYear + '-10-01';
        this.reportParams.end = this.reportParams.yearEnd;
        break;
      default:
        this.reportParams.start = this.reportParams.yearStart;
        this.reportParams.end = this.reportParams.yearEnd;
    }
  }

  /**
   * Saves a holiday year based on calendar input and updates the value in allYears property.
   * @param itemToSave Either a holiday year or keyword "changed" to save all cahnged items.
   */
  public saveYear(itemToSave: HolidayYear | "changed"): void {
    let itemsToSave: { ix: number, item: HolidayYear }[];
    if (itemToSave === "changed") {
      itemsToSave = this.allYears.map((item, ix) => angular.equals(this.allYears[ix], this.allYearsOrig[ix]) ? { ix: -1, item: null } : { ix, item });
    } else {
      itemsToSave = [{ ix: this.allYears.findIndex(x => x === itemToSave), item: itemToSave }];
    }
    itemsToSave = itemsToSave.filter(x => x.ix > -1);
    if (itemsToSave.length === 0) {
      this.uiHelpers.showAlert("Ei tallennettavaa", "Mikään tietueista ei ole muuttunut");
    }
    const loader = this.uiHelpers.showLoading(`Tallennetaan ${itemsToSave.length} tietuetta...`);
    const promises = itemsToSave.map((item) => {
      return this.api.save(item.item)
        .then((savedValue) => {
          this.allYears[item.ix] = savedValue;
          this.allYearsOrig[item.ix] = savedValue;
        });
    });
    Promise.all(promises).then(() => {
      loader.dismiss();
    });
  }

  /**
   * Show an edit dialog for the holiday year.
   * @param year The holiday year to update.
   */
  public showEditDialog(year: HolidayYear): void {
    /* HACK: The edit dialog is not optimal, as it shows the entire edit worker dialog. Should be a separate edit holidays / holiday periods dialog. */
    this.uiCrudHelpers.openEditWorkerDialog(year.workerId, "default", "holidays").then((result) => {
      if (result.action === "ok") {
        this.getHolidayYears(true);
      }
    });
  }

  /**
   * Gets the full holiday years for all Workers for reporting purposes
   * @param keepView If true, does not reset the view based on the incoming data.
   * If true, you should make sure the year does not change because then view will be invalid for the year
   * I.e. should be true only for reload scenarios.
   */
  public getHolidayYears(keepView = false) {
    this.fullApi.getForYear(this.reportYear).then((result: HolidayYear[]) => {
      this.allYears = result;
      this.allYearsOrig = angular.copy(this.allYears);
      if (!keepView) {
        this.reportParams._view = null;
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const getViewForInitAfterYearChange = this.reportView;
      }
    });
  }

  /** Maps an array of HolidayYear to calendar series. */
  public mapToCalendar = (years: HolidayYear[]): CalendarSeries[] => {
    return years.map((hy) => {
      return {
        key: hy.id,
        title: hy.workerSnapshot.displayName,
        description: `Suunnitellut ${this.getTotalDays("all", hy)}pv, kertymä ${hy.accrual.endSaldo} pv (koko vuosi).`,
        avatar: hy.workerSnapshot,
        data: hy,
        events: hy.leaves.planned.map((leave) => ({
          start: leave.period.start,
          end: leave.period.end,
          summary: leave.notes || `Loma ${leave.period.daysCount} päivää`,
          data: leave,
        }
        )),
      };
    });
  };

  /**
   * Returns true, if two objects are different.
   * @param obj1 First object to compare
   * @param obj2 Second object to compare
   */
  public isChanged(obj1, obj2): boolean {
    return !angular.equals(obj1, obj2);
  }

  private setHolidayYearsForEmployment(value: HolidayYear[]): void {
    this.employmentHolidayYears = value;
    let year = Dates.getYear(this.forDate || "today");
    if (Dates.getMonth(this.forDate || "today") < 5) {
      year--;
    }
    this.model = value.find((x) => x.year === year) || value[0];
  }
}
