import { AnnualLeave, Arrays, Dates, HolidaysLogic, HolidayYear } from "@salaxy/core";

import { UiHelpers } from "../../services";

import { ListControllerBase, ListControllerBaseBindings } from "../bases";

/**
 * Controls the planned holidays list (lomakirjanpito / lomakalenteri) for a selected holiday year.
 */
export class HolidayYearHolidaysController  extends ListControllerBase<HolidayYear, AnnualLeave> {

  /** Bindings for components that use this controller */
  public static bindings = (new class extends ListControllerBaseBindings {

    /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
    public condensed = "<";

    /** Date filter start value. Will be compared to period end date. */
    public filterStart = "<";

    /** Date filter end value. Will be compared to period start date. */
    public filterEnd = "<";

  }());

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers"];

  /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
  public condensed: boolean;

  /** Date filter start value. Will be applied to period start dates. */
  public filterStart: string;

  /** Date filter end value. Will be applied to period end dates. */
  public filterEnd: string;

  constructor(uiHelpers: UiHelpers) {
    super(uiHelpers);
  }

  /** List of items */
  public get list(): AnnualLeave[] {
    if (!this.parent || !this.parent.leaves || !this.parent.leaves.planned) {
      return [];
    }
    return this.parent.leaves.planned;
  }

  /**
   * Filter that is applied to the list when displayed.
   * This is done in view: Does not apply to list property, but does affect getTotalDays().
   * @param value Item in the list
   */
  public filter = (value: AnnualLeave) => {
    if (this.filterEnd && value.period.start > this.filterEnd) {
      return false;
    }
    if (this.filterStart && value.period.end < this.filterStart) {
      return false;
    }
    return true;
  }

  /** Creating of a new item. */
  public getBlank(): AnnualLeave {
    return {
      period: {
        start: Dates.getToday(),
        end: Dates.getToday(),
        daysCount: 1,
      },
    };
  }

  /** Template for edit UI that is shown in a modal dialog. */
  public getEditDialogTemplateUrl() {
    return "salaxy-components/worker/holidays/HolidayYearHolidaysEditDialog.html";
  }

  /** Logic for edit dialog. */
  public getEditDialogLogic() {
    return {
      /** Updates the workdays count */
      updatePeriodDays: (current: AnnualLeave) => {
        if (current.period.start > current.period.end) {
          current.period.end = current.period.start;
        }
        if (current.period.start && current.period.end) {
          if (current.period.days) {
            current.period.days = Dates.getVacationDays(current.period.start, current.period.end);
            current.period.daysCount = current.period.days.length;
          } else {
            current.period.daysCount = Dates.getVacationDays(current.period.start, current.period.end).length;
          }
        }
      },

      /** Sets the days array in period to enable days selection UI. */
      setDaysSelection: (period: AnnualLeave) => {
        period.period.days = Dates.getVacationDays(period.period.start, period.period.end);
        period.period.daysCount = period.period.days.length;
      },

    };
  }

  /** Gets the description text for the planned holiday. */
  public getDescription(row: AnnualLeave) {
    if (row.notes) {
      return row.notes;
    }
    return row.period.start < `${this.parent.year}-10-1` ? "Kesäloma" : "Talviloma";
  }

  /** Gets a total days calculation for different types. */
  public getTotalDays(type: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart" | "filtered" = "all") {
    // HACK: jos periodi menee yli filterin tästä tulee väärä tulos (alku ennen filtterin alkua, loppu filtterin alun jälkeen).
    if (type === "filtered") {
      return Arrays.sum(this.list.filter((x) => this.filter(x)), (x) => x.period.daysCount);
    }
    if (this.parent) {
      return HolidaysLogic.getPlannedLeavesCount(this.parent, type);
    }
    return null;
  }
}
