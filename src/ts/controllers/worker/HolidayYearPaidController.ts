import { AnnualLeavePayment, AnnualLeavePaymentKind, Avatar, Calculation, CalculationRowType, Calculations, CalculationStatus, Dates, HolidaysLogic, HolidayYear, LegalEntityType, UserDefinedRow } from "@salaxy/core";

import { UiHelpers } from "../../services";

import { ListControllerBase, ListControllerBaseBindings } from "../bases";

/**
 * Controls the annual leave payments: HolidayCompensation, HolidayBonus and HolidaySalary.
 * These payments are typically fetched from paid calculations automatically,
 * but may also be marked paid manually. Also, in client-side logic, payments are fetched
 * optionally from Draft calculations.
 */
export class HolidayYearPaidController  extends ListControllerBase<HolidayYear, AnnualLeavePayment> {

  /** Bindings for components that use this controller */
  public static bindings = new ListControllerBaseBindings();

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "UiHelpers",
    "Calculations",
  ];

  /** Total calculations */
  public totals: {
    /** Total calculation  */
    total: AnnualLeavePayment,
    /** Start saldo of the payments */
    startSaldo: AnnualLeavePayment,

    /** End saldo of the payments. */
    endSaldo: AnnualLeavePayment,
  };

  private _list: AnnualLeavePayment[];

  private _listCacheKey: string;

  constructor(
    uiHelpers: UiHelpers,
    private calcApi: Calculations,
  ) {
    super(uiHelpers);
  }

  /** Gets the worker ID for the current holiday year. */
  public get workerId(): string {
    if (!this.parent || !this.parent.workerSnapshot) {
      return null;
    }
    return this.parent.workerSnapshot.id;
  }

  /** Gets the employment ID for the current holiday year. */
  public get employmentId(): string {
    if (!this.parent || !this.parent.workerSnapshot) {
      return null;
    }
    return this.parent.employmentId;
  }

  /** Refreshes the cache */
  public refresh() {
    this._list = [];
    this._listCacheKey = null;
  }

  /**
   * List of items fetched from the calculations,
   * cached to the controller level, except when workerId changes.
   */
  public get list(): AnnualLeavePayment[] {
    // TODO: Such a complex cache is probably not needed once we move the getPaidHolidays() to server-side.
    if (!this.employmentId) {
      // Empty list case.
      this._listCacheKey = null;
      if (this._list) {
        this._list = null;
      }
      return this._list;
    }
    if (this.getCurrentCacheKey() !== this._listCacheKey) {
      this._list = null;
      this._listCacheKey = this.getCurrentCacheKey();
      this.getPaidHolidays().then((result) => {
        this._list = result;
      });
    }
    return this._list;
  }

  /**
   * List of paid holidayse fetched from the calculations (server-side).
   */
  public getPaidHolidays(): Promise<AnnualLeavePayment[]> {
    return this.calcApi.getForEmployment(this.employmentId).then((allCalcs) => {
      // TODO: Move this logic to server side to holiday service method.
      const calcs = allCalcs.filter((x) =>
        (x.workflow.salaryDate || x.workflow.paidAt || Dates.getToday())  >= this.parent.period.start
        && (x.workflow.salaryDate || x.workflow.paidAt || Dates.getToday()) <= this.parent.period.end);
      this.parent.leaves.paid = this.getPaymentsFromCalculations(calcs);
      // HACK: Need to figure out the holiday salary calculation here. Fetch from employment or add to holiday year?
      const monthlySalaryRow: UserDefinedRow = calcs
        .map((calc) => calc.rows.find((row) => row.rowType === CalculationRowType.MonthlySalary))
        .find((row) => row != null)
        ;
      const dailyHolidaySalary = monthlySalaryRow ? (monthlySalaryRow.price / 25) : null;
      this.totals = {
        startSaldo: HolidaysLogic.getPaidHolidaysCalculation(this.parent, "startSaldo", dailyHolidaySalary),
        total: HolidaysLogic.getPaidHolidaysCalculation(this.parent, "total", dailyHolidaySalary),
        endSaldo: HolidaysLogic.getPaidHolidaysCalculation(this.parent, "endSaldo", dailyHolidaySalary),
      };
      return this.parent.leaves.paid;
    });
  }

  /** Gets an avatar for a table row. */
  public getAvatar(type: "row" | "startSaldo" | "total" | "endSaldo", row: AnnualLeavePayment): Avatar {
    return HolidaysLogic.getPaidCalculationAvatar(type, row);
  }

  /**
   * Gets the payments fromcalculations with given filters.
   * This method has no caching.
   */
  public getPaymentsFromCalculations(calcs: Calculation[]): AnnualLeavePayment[] {
    let result: any[] = [];
    // TODO: Move to Logic and make unit testable.
    // HACK: Rewrite to allow manual edits. Consider moving to server-side.
    for (const calc of calcs) {
      const refDate = calc.workflow.paidAt || Dates.getToday();
      let status: AnnualLeavePaymentKind = AnnualLeavePaymentKind.Undefined;
      switch (calc.workflow.status) {
        case CalculationStatus.Draft:
        case CalculationStatus.PaymentStarted: // This could potentially be considered "paid".
          status = AnnualLeavePaymentKind.DraftCalc;
          break;
        case CalculationStatus.PaymentSucceeded:
          status = AnnualLeavePaymentKind.PaidCalc;
          break;
      }
      if (this.parent.period.start > refDate || this.parent.period.end < refDate) {
        // TODO: Add here calculations included by id even if they are outside the dates range. Also, exclude the excluded.
        status = AnnualLeavePaymentKind.Undefined;
      }
      if (status === AnnualLeavePaymentKind.DraftCalc) {
        status = AnnualLeavePaymentKind.Undefined;
      }
      if (status !== AnnualLeavePaymentKind.Undefined) {
        const period = Dates.getDateRange(calc.info.workStartDate, calc.info.workEndDate);
        // TODO: AnnualLeavePayment
        const payment: AnnualLeavePayment = {
          calcId: calc.id,
          date: refDate,
          kind: status,
          period,
          holidayBonus: null,
          holidayCompensation: null,
          holidayDays: null,
          holidaySalary: null,
        };
        for (const row of calc.result.rows) {
          switch (row.rowType) {
            case CalculationRowType.HolidayBonus:
              payment.holidayBonus += row.total;
              (payment as any).isHolidays = true;
              break;
            case CalculationRowType.HolidayCompensation:
              payment.holidayCompensation += row.total;
              if (row.count > 1) {
                // HACK: Need a better way of determining that this is really days and not percent / just one.
                payment.holidayDays += row.count;
              }
              (payment as any).isHolidays = true;
              break;
            case CalculationRowType.HolidaySalary:
              payment.holidaySalary += row.total;
              if (row.count > 1) {
                // HACK: Need a better way of determining that this is really days and not just one (or something else?).
                payment.holidayDays += row.count;
              }
              (payment as any).isHolidays = true;
              break;
          }
        }
        (payment as any).avatar = HolidaysLogic.getPaidCalculationAvatar("row", payment);
        result.push(payment);
      }
    }
    result = result.filter((x) => x.isHolidays);
    return result;
  }

  /** Creating of a new item. */
  public getBlank(): any {
    return {
      kind: AnnualLeavePaymentKind.Undefined,
      avatar: {
        displayName: "Käsin syötetty korjaus",
        description: null,
        color: "rgb(74, 146, 233)",
        initials: "edit",
        entityType: LegalEntityType.Company,
      },
      paidAt: Dates.getToday(),
      period: Dates.getDateRange(Dates.getToday(), Dates.getToday()),
      holidayBonus: null,
      holidayCompensation: null,
      holidayDays: null,
      holidaySalary: null,
      isHolidays: false,
    };
  }

  /** Logic for edit dialog. */
  public getEditDialogLogic() {
    return {
      /** Updates the workdays count */
      updatePeriodDays: (current: AnnualLeavePayment) => {
        if (current.period.start > current.period.end) {
          current.period.end = current.period.start;
        }
        if (current.period.start && current.period.end) {
          if (current.period.days) {
            current.period.days = Dates.getVacationDays(current.period.start, current.period.end);
            current.period.daysCount = current.period.days.length;
          } else {
            current.period.daysCount = Dates.getVacationDays(current.period.start, current.period.end).length;
          }
          current.holidayDays = current.period.daysCount;
        }
      },
    };
  }

  /** Template for edit UI that is shown in a modal dialog. */
  public getEditDialogTemplateUrl() {
    return "salaxy-components/worker/holidays/HolidayYearPaidEditDialog.html";
  }

  private getCurrentCacheKey() {
    return this.workerId + this.parent.year;
  }
}
