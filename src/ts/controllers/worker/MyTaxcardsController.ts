import * as angular from "angular";

import { Dates, EnumerationsLogic, TaxcardInput, TaxcardKind, TaxcardListItem, Taxcards, TaxcardState, Test, WorkerCurrentTaxcards } from "@salaxy/core";
import { SessionService, UiCrudHelpers, UiHelpers } from "../../services";

/**
 * Helps listing current account tax cards (latest and previous ones)
 * and also the employers that are using the latest taxcards.
 */
export class MyTaxcardsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Taxcards", "UiCrudHelpers", "SessionService", "UiHelpers", "Test"];

  /** The data after it has been loaded. Null if nothing is loaded yet.  */
  public current: WorkerCurrentTaxcards;

  /** The current active tab. */
  public currentTab: string;

  /**
   * Today is explicitly defined, so that it can be changed in testing.
   */
  public today = Dates.getToday();

  public apiTestData: TaxcardInput = {
    kind: TaxcardKind.Auto,
    forYear: Dates.getYear("today"),
    personalId: null,
    validity: {
      start: Dates.getToday(),
    },
  }

  /** Creates a new MyTaxcardsController */
  constructor(
    private taxcards: Taxcards,
    private uiCrudHelpers: UiCrudHelpers,
    private sessionService: SessionService,
    private uiHelpers: UiHelpers,
    private testApi: Test,
    ) {}

  /** Initializes the controller. */
  public $onInit = () => {
    this.reload();
  }

  /** List of taxcards after it has been loaded. Null if nothing is loaded yet. */
  public get list(): TaxcardListItem[] {
    if (this.current) {
      return this.current.list;
    }
    return null;
  }

  /** Returns true if runnin in test mode. */
  public get isTest() {
    return this.sessionService.isInRole("test");
  }

  /** Reloads the data from the server. */
  public reload() {
    this.current = null;
    this.taxcards.getMyTaxcards().then((result) => {
      this.current = result;
      if (this.current?.active) {
        this.currentTab = "calcs";
      }
      this.apiTestData.personalId = result.personalId;
    });
  }

  /**
   * Removes a taxcard from the list and does a refresh.
   * @param id Taxcard id.
   */
  public deleteTaxCard(id: string) {
    this.taxcards.delete(id).then(() => {
      this.reload();
    });
  }

  /** Gets the info for a taxcard list item based on the index */
  public getInfo(listItem: TaxcardListItem) {
    const result = {
      classes: null as string,
      label: null as string,
      description: null as string,
    };
    if (listItem.flags.indexOf("sharedNotSeen") >= 0) {
      result.classes = "bg-warning";
      result.label = "Odottaa";
      result.description = "Ei vielä nähty";
    } else if (listItem.flags.indexOf("sharedRejected") >= 0) {
      result.classes = "bg-danger";
      result.label = "Hylätty";
      result.description = null;
    } else if (listItem.flags.indexOf("sharedRejectedWithoutOpen") >= 0) {
      result.classes = "bg-danger";
      result.label = "Hylätty";
      result.description = "Sisältöä ei nähty";
    } else if (listItem.flags.indexOf("sharedWaiting") >= 0) {
      result.classes = "bg-warning";
      result.label = "Odottaa";
      result.description = "Odottaa hyväksyntää";
    } else if (listItem.status === TaxcardState.SharedApproved) {
      result.classes = "bg-success";
      result.label = "Hyväksytty";
      result.description = null;
    } else  {
      result.classes = null;
      result.label = null;
      result.description = EnumerationsLogic.getEnumLabel("TaxcardState", listItem.status);
    }
    return result;
  }

  /**
   * Sets the users automatic shared taxcard as ON or if already set refreshes the taxcard from tax authorities.
   */
  public setSharedAutoForSelf() {
    const loader = this.uiHelpers.showLoading("Haetaan verokorttia", "Haetaan ennakonpidätyksen prosentteja ja tulorajaa Veron rajapinnasta.");
    if (this.current.active?.card.kind === TaxcardKind.Auto) {
      this.taxcards.refreshAuto(this.current.active.id, null, true).then((x) => {
        if (x == null) {
          this.uiHelpers.showAlert("Ei verokorttia", "Verottajan tiedoista ei pystytty hakemaan uutta veroprosenttia.\n\nTämä voi olla tilapäinen ongelma. Käytämme toistaiseksi vanhoja prosentteja.");
        } else {
          this.reload();
        }
        loader.dismiss();
      });
    } else {
      this.taxcards.setAuto(this.current.personalId).then((x) => {
        if (x == null) {
          this.uiHelpers.showAlert("Ei verokorttia", "Verottajan tiedoista ei löydy verokorttia tälle henkilötunnukselle.\n\nTarkista verokorttisi Omavero-palvelusta tai Veron asiakaspalvelusta.");
        } else {
          this.reload();
        }
        loader.dismiss();
      });
    }
  }

  /**
   * TEST ONLY: Sets the test data for taxcards API.
   */
  public setTestCard() {
    const loader = this.uiHelpers.showLoading("Tallennetaan...");
    this.testApi.setTaxcardApi(this.apiTestData).then((result) => {
      this.apiTestData = result;
      loader.dismiss();
    });
  }
  /**
   * TEST ONLY: Gets the test data for taxcards API.
   */
   public getTestCard() {
    this.testApi.getTaxcardApi(this.current.personalId).then((result) => {
      if (result) {
        this.apiTestData = result;
      }
    });
  }

  /**
   * Adds a new taxcard for Workers own account
   * TODO: Remove this when testing ready.
   */
  public showAddNewCardForSelf() {
    this.uiCrudHelpers.createNewTaxcard(this.current.personalId).then((result) => {
      if (result.action === "ok") {
        this.reload();
      }
    });
  }

}
