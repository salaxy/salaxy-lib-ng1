﻿import * as angular from "angular";

import { Dates, IUserObjectIndex, PersonAccount, Taxcard, TaxCard2019Logic, TaxCardIncomeType, TaxcardKind, TaxcardListItem, Taxcards, TaxcardState } from "@salaxy/core";

import { EditDialogKnownActions, SessionService, UiCrudHelpers, UiHelpers, UploadService } from "../../services";
import { ApiCrudObjectController } from "../bases";

/**
 * UI logic for viewing, adding new and editing taxcards.
 * You can edit a single taxcard (model to object, id or parent controller).
 * Note that creating a new taxcards requires setting Personal ID.
 * You can create a new one by setting the model as "new" (full add new UI) as well as null (quick buttons).
 */
export class TaxcardCrudController extends ApiCrudObjectController<Taxcard> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Taxcards",
    "UiHelpers",
    "UiCrudHelpers",
    "$location",
    "$routeParams",
    "UploadService",
    "SessionService",
    "$timeout",
  ];

  /** Mode is either "pro" (for admin / PRO-ui) or "default" */
  public mode: "pro" | "default";

  /**
   * True if the taxcard is uploaded for the current Worker account (as opposed to Worker of an Employer) .
   * This should typically not be set: It will be set by init (if current account is Worker) and personalId (if 'self' or personal id matches).
   */
  public isSelf: boolean;

  /** The upload progress 0-100 */
  public uploadProgress: number = null;

  /** Edit taxcard form: Used for checking the validity. */
  public editForm: angular.IFormController;

  /**
   * Today is explicitly defined, so that it can be changed in testing.
   */
  public today = Dates.getToday();

  /** Settings / instructions for the "Create new" UI (show/hide inputs, formatting etc). */
  public addNewUi: any;

  /** If true, does not show the save / current buttons - the parent must show them. */
  public hideButtons: boolean;

  /** Possible taxcard kinds that should be available for current role to select. */
  public kindSelection: string;

  /**
   * Event that is called when a taxcard is saved and the parent UI probably needs to be refreshed.
   * @example <salaxy-taxcard-details model="$ctrl.current.active" personal-id="$ctrl.current.personalId" on-save="$ctrl.reload()"></salaxy-taxcard-details>
   */
  public onSave: (eventData: {
    /** The selected item. */
    item: Taxcard,
  }) => void;

  private _personalId: string | "self";

  constructor(
    private fullApi: Taxcards,
    uiHelpers: UiHelpers,
    private uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private uploadService: UploadService,
    private sessionService: SessionService,
    private $timeout: angular.ITimeoutService,
  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    this.mode = this.mode || "default";
    if (this.sessionService.isInRole("worker")) {
      // Just as a backup, this component should not be used for role worker any more.
      this.isSelf = true;
    }
    if (this.sessionService.isInRole("company")) {
      delete this.taxcardKindOptions.noWithholdingHousehold;
    }
    if (this.sessionService.isInRole("person")) {
      // Now, enabled for person
      if (this.current?.card.kind === TaxcardKind.Undefined) {
        this.current.card.kind = TaxcardKind.Auto;
      }
    } else if (this.sessionService.isInRole("test")) {
      // Also in test, but with marker
      if (this.current?.card.kind === TaxcardKind.Undefined) {
        this.current.card.kind = TaxcardKind.Auto;
      }
      this.taxcardKindOptions.auto += " (test)";
    }
    else {
      // TEMPORARY: Disable in company production until tested.
      delete this.taxcardKindOptions.auto;
      if (this.current?.card.kind === TaxcardKind.Undefined) {
        this.current.card.kind = TaxcardKind.DefaultYearly;
      }
    }
    super.$onInit();
  }

  /**
   * Either a valid Finnish Personal ID or "self" for the current Worker account.
   * Required for creating a new taxcard: The personal ID is used for setting the connection to Worker account or self.
   * The personal id is used when binding is null (buttons for create new) or "new" (actual new UI).
   */
  public get personalId(): string | "self" {
    return this._personalId;
  }
  public set personalId(value: string | "self") {
    if (!value) {
      this._personalId = null;
      return;
    }
    if (value === "self") {
      if (!this.sessionService.isInRole("worker")) {
        throw new Error("Cannot bind taxcard to 'self' for Account that is not a Worker.");
      }
      this._personalId = (this.sessionService.getCurrentAccount() as PersonAccount).identity.officialId;
      this.isSelf = true;
    } else {
      this._personalId = value;
      if ((this.sessionService.getCurrentAccount() as PersonAccount).identity.officialId === value) {
        this.isSelf = true;
      }
    }
    if (value && this.bindingMode === "new") {
      this.reload();
    }
  }

  /** Returns true if one of the edit forms is shown ad if it is valid. */
  public get isFormValid() {
    return this.editForm && this.editForm.$valid;
  }

  /**
   * Gets the chart type that should be shown.
   * Type "preview" means no chart: Only show the preview image of the card.
   */
  public get chartType(): "preview" | "full" | "pie" | "none" {
    if (!this.current || this.current.card.state === TaxcardState.SharedRejectedWithoutOpen) {
      return "none";
    }
    if (
      this.current.card.state === TaxcardState.SharedApproved
      || this.current.card.state === TaxcardState.SharedRejected
      || this.current.card.state === TaxcardState.SharedWaiting
      ) {
      return "preview";
    }
    if (this.current.card.incomeLimit) {
      return "full";
    }
    return "pie";
  }

  /**
   * Gets a row for data binding of external salaries paid outside Salaxy system.
   * Will create the row if it does not exist. The save method will remove any empty rows.
   */
  public get previousSalariesRow() {
    if (!this.current) {
      return null;
    }
    if (!this.current.incomeLog) {
      this.current.incomeLog = [];
    }
    if (!this.current.incomeLog.find((x) => x.type === TaxCardIncomeType.ExternalSalaries)) {
      this.current.incomeLog.push({
        type: TaxCardIncomeType.ExternalSalaries,
        id: null,
        description: "Muualla maksetut palkat (verokortin lisäys).",
        startDate: this.today,
        endDate: this.today,
        income: null,
        tax: null,
      });
    }
    return this.current.incomeLog.find((x) => x.type === TaxCardIncomeType.ExternalSalaries);
  }

  /**
   * Starts the reload process depending on the bindingMode:
   */
  public reload(): Promise<Taxcard> {
    if (this.bindingMode === "new") {
      this.setCurrentRef(TaxCard2019Logic.getBlank(this.personalId));
      this._currentId = null;
      this.setStatus(null);
      this.updateUISettings();
      return Promise.resolve(this.current);
    }
    return super.reload().then((result) => {
      // TODO: Go through this at the source: Could we assure UI updating in parent controller.
      this.$timeout(() => {
        this.updateUISettings();
      } );
      return result;
    });
  }

  /** Gets the user interface mode: The UI view that is shown by the component. */
  public getMode(): "pro" | "new" | "default" | "null" | "no-personal-id" {
    if (this.bindingMode === "null") {
      return this.personalId ? "null" : "no-personal-id";
    }
    if (this.isNew()) {
      return this.personalId ? "new" : "no-personal-id";
    }
    if (this.mode === "pro") {
      return this.mode;
    }
    return "default";
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/taxcards",
      detailsUrl: this.detailsUrl || null,
      oDataTemplateUrl: "salaxy-components/odata/lists/Taxcards.html",
      oDataOptions: {},
    };
  }

  /**
   * Shows the detail view for the item.
   * Typically, this takes the user to a new page with the ID.
   * @param item Item may be either Container or list item.
   */
  public showDetails(item: Taxcard | IUserObjectIndex): angular.ILocationService {
    if (this.parentController) {
      return this.parentController.showDetails(item);
    }
    if (!this.getDefaults().detailsUrl) {
      if ((item as IUserObjectIndex).otherId) {
        return this.$location.url("/workers/details/" + (item as IUserObjectIndex).otherId + "#taxcards");
      } else if ((item as Taxcard).worker && (item as Taxcard).worker.id) {
        return this.$location.url("/workers/details/" + (item as Taxcard).worker.id + "#taxcards");
      } else {
        return this.$location.url("/taxcards/details/" + item.id);
      }
    }
    return this.$location.url(this.getDefaults().detailsUrl + this.getId(item));
  }

  /** Save changes to the current item and then calls the on-save event. */
  public save(): Promise<Taxcard> {
    return super.save().then((savedCard) => {
      if (!savedCard) {
        throw new Error("Taxcard saving failed (api returns null): this should only occur with automatic taxcards, and only in adding (this controller is not designed for adding a new taxcard).");
      }
      this.onSave({ item: savedCard });
      return savedCard;
    });
  }

  /**
   * Saves the taxcard and checks the diff after save:
   * If there are paid calculations that should be added to the taxcards, shows a dialog for that.
   */
  public saveCheckDiff(): Promise<Taxcard> {
    return this.save().then((savedTaxcard) => {
      if (savedTaxcard.incomeLog.find((x) => x.diff && x.diff !== "default")) {
        return this.uiHelpers.openEditDialog("salaxy-components/modals/worker/taxcard-income-log.html", savedTaxcard, { }, "lg")
          .then((incomeLogResult) => {
            if (incomeLogResult.action === "ok") {
              const loader = this.uiHelpers.showLoading("Tallennetaan laskelmia...");
              this.fullApi.commitDiff(savedTaxcard).then((commitResult) => {
                loader.dismiss();
                return commitResult;
              });
            } else {
              return savedTaxcard;
            }
          });
      }
    });
  }

  /**
   * Starts the "Add new" process for the tax card.
   * By default we do not allow modifying taxcards to the end-user (only allowed for PRO-users).
   * Instead, they will always add a new taxcard.
   * Note: This method breaks the existing model binding of the controller and binds model="'new'".
   */
  public showAddNew() {
    if (!this.personalId) {
      if (!this.current.card.personalId) {
        throw new Error("Mo personal ID: Cannot add new card.");
      }
      this.personalId = this.current.card.personalId;
    }
    this.uiCrudHelpers.createNewTaxcard(this.personalId).then((result) => {
      if (result.action === "ok") {
        if (this.bindingMode === "url") {
          this.showDetails(result.item);
        } else {
          this.reload();
          this.onSave({ item: result.item });
        }
      }
    });
  }

  /** Shows the user interface for editing the minimum percent. */
  public editTaxPercentMin() {
    this.uiHelpers.openEditDialog("salaxy-components/modals/worker/taxcard-min-percent.html", {
      taxPercentMin: this.current.card.taxPercentMin,
    }, {}).then((result) => {
      if (result.action == EditDialogKnownActions.Ok) {
        this.current.card.taxPercentMin = result.item.taxPercentMin || null;
        this.save();
      }
    });
  }

  public taxcardKindOptions = {
    "auto": "Automaattinen verokortti",
    "defaultYearly": "Alkuperäinen verokortti",
    "replacement": "Muutosverokortti",
    "noTaxCard": "Ei verokorttia (60%)",
    "noWithholdingHousehold": "Ei ennakonpidätystä: Kotitalous alle 1500€",
  };

  /** Edit income log. */
  public editIncomeLog() {
    this.uiHelpers.openEditDialog("salaxy-components/modals/worker/taxcard-income-log.html", this.current,
    { editable: true }, "lg").then((result) => {
      if (result.action === EditDialogKnownActions.Ok) {
        this.save();
      }
    });
  }

  /**
   * Adds a new 'no tax card' kind card and directly **saves** it as latest/current tax card.
   * Note: This method breaks the existing model binding of the controller and binds model="'new'".
   */
  public chooseNoTaxCard() {
    this.model = "new";
    this.resetAsNew(TaxcardKind.NoTaxCard);
    this.save();
  }

  /**
   * Resets the current taxcard as new.
   * @param kind If specified, reset the taxcard to the given type.
   */
  public resetAsNew(kind?: TaxcardKind) {
    this.setCurrentValue(TaxCard2019Logic.getBlank(this.personalId, kind, null, this.today));
    this.updateUISettings();
  }

  /** Handles change of the year in the years dropdown. */
  public yearChanged() {
    this.setCurrentValue(TaxCard2019Logic.getBlank(this.personalId, this.current.card.kind, this.current.card.forYear, this.today));
    this.updateUISettings();
  }

  /**
   * Returns the url where to download the tax card
   * @param taxCard Taxcard or if null, the current is used.
   */
  public getTaxcardDownloadUrl(taxCard?: Taxcard): string {
    taxCard = taxCard || this.current;
    return this.fullApi.getDownloadUrl(taxCard);
  }

  /**
   * Returns the url where to preview the tax card
   * @param taxCard Taxcard or if null, the current is used.
   */
  public getTaxcardPreviewUrl(taxCard?: Taxcard): string {
    taxCard = taxCard || this.current;
    return this.fullApi.getPreviewUrl(taxCard);
  }

  /**
   * Returns the url where to preview the tax card in upload scenario.
   * When uploading (adding a new taxcard) the taxcard is not necessarily saved so the preview cannot be fetched based on taxcard id.
   * On the other hand we are always in current user context (current user is the file owner), so we can use previewUri directly
   * @param taxCard Taxcard or if null, the current is used.
   */
  public getUploadPreviewUrl(taxCard?: Taxcard): string {
    taxCard = taxCard || this.current;
    if (!taxCard || !taxCard.card.previewUri) {
      return null;
    }
    return this.uploadService.getPreviewUrl(taxCard.card.previewUri);
  }

  /**
   * Uploads tax card to the server.
   * @param file - selected file
   */
  public uploadTaxCard(file: any) {
    this.uploadProgress = null;
    if (!file) {
      return;
    }
    this.uploadService.upload<Taxcard>(this.fullApi.getUploadUrl(), { file }, (progress, error) => {
      this.uploadProgress = progress;
    }).then(
      (resp) => {
        this.uploadProgress = null;
        this.current.card.fileUri = resp.card.fileUri;
        this.current.card.previewUri = resp.card.previewUri;
      }).catch((response) => {
        this.uiHelpers.showAlert("Virhe", "Tiedoston tallennus palvelimelle epäonnistui.");
        throw new Error(response);
      });
  }

  /**
   * Gets the chart data, labels, colors and other settings.
   * Uses chartCache to avoid $digest loop.
   */
  public get mainChartData() {
    return this.uiHelpers.cache(this, "lineChart",
      () => TaxCard2019Logic.getMainChart(this.current),
      () => TaxCard2019Logic.getIncomeLimitInfo(this.current));
  }

  /** Gets the cached version of the chart data. */
  public get pieChartData() {
    return this.uiHelpers.cache(this, "pieChart",
      () => TaxCard2019Logic.getPieChartData(this.current),
      () => TaxCard2019Logic.getIncomeLimitInfo(this.current));
  }

  /**
   * Checks if the tax card is expired
   * A tax card still valid in the next year's January
   */
  public isTaxCardExpired(taxCard: Taxcard): boolean {
    return !TaxCard2019Logic.isTaxcardValid(taxCard, this.today);
  }

  /**
   * Gets a language versioned error description ng-file-upload invalidFile errors (client-side before actual upload).
   * @param invalidFile Invalid file of ng-file-upload component.
   */
  public getInvalidFileDescription(invalidFile) {
    return this.uploadService.getInvalidFileDescription(invalidFile);
  }

  /**
   * Adds a new taxcard for list item and after adding updates the current list without going to server.
   * This is needed because refreshing of the list may not provide the correct results as search is asynchronous.
   */
  public showTaxCardAddDialogForTaxCardsList(listItem: TaxcardListItem) {
    this.uiCrudHelpers.createNewTaxcard(listItem.otherPartyInfo.officialId).then((result) => {
      if (result.action === "ok") {
        const card = result.item.card;
        listItem.data.employmentId = result.item.employmentId;
        listItem.data.forYear = card.forYear;
        listItem.data.incomeLimit = card.incomeLimit;
        listItem.data.kind = card.kind as any;
        listItem.data.taxPercent = card.taxPercent;
        listItem.data.taxPercent2 = card.taxPercent2;
        (listItem as any).payment = result.item.result.payment;
        listItem.data.totalIncome = result.item.result.totalIncome;
        listItem.startAt = card.validity.start;
        listItem.endAt = card.validity.end;
      }
    });
  }

  /** Commits the DIFF's to storage: synchs the taxcard to actual paid calculations. */
  public commitDiff() {
    this.fullApi.commitDiff(this.current).then((result) => {
      this.setCurrentValue(result, true);
    });
  }

  /** Defines which fields to show/hide, require etc. in the view */
  private updateUISettings() {
    const settings = {
      /** If true, shown the 1st tax percent */
      showTaxPercent: false,
      /** If true, shown the income limit AND 2nd tax pecent. */
      showIncomeLimit: false,
      /** If true, the require the income limit AND 2nd tax percent. */
      requireIncomeLimit: false,
      /** If true, shows input for previous salaries paid. */
      showPreviousSalariesPaid: false,
      /** If true shows the image of an example tax card. */
      showExampleTaxCard: true,
      /** Upload is either hidden, shown as optional (at the end) or required (in the beginning). */
      upload: "hide" as "hide" | "show" | "required",
      /**
       * If true, the tax percents etc. cannot be edited.
       * In this case, also the helper numbers (1.-5.) that show where the number is in the PDF should not be shown.,
       */
      readonly: false,
      /** Options for the start date: currently min and max dates. */
      startDateOptions: null,
      /**
       * Years that are allowed in the year selection:
       * Current year + next year in December, previous year in January.
       */
      yearsAllowed: null,
    };
    if (!this.current) {
      this.addNewUi = settings;
      return;
    }
    const todayYear = Dates.getYear(this.today);
    const month = Dates.getMonth(this.today);
    let yearsAllowed: number[];
    if (month === 1) {
      yearsAllowed = [todayYear, todayYear - 1];
    } else if (month === 12) {
      yearsAllowed = [todayYear, todayYear + 1];
    } else {
      yearsAllowed = [todayYear];
    }
    if (yearsAllowed.indexOf(this.current.card.forYear) < 0) {
      this.current.card.forYear = todayYear;
    }
    const forYear = this.current.card.forYear;
    yearsAllowed = yearsAllowed.length < 2 ? null : yearsAllowed; // In the UI, the selection is not shown if there is only one item to select.
    settings.showTaxPercent = true;
    settings.showExampleTaxCard = true;

    switch (this.current.card.kind) {
      case TaxcardKind.Auto:
        settings.showExampleTaxCard = false;
        settings.showTaxPercent = false;
        break;
      case TaxcardKind.NoTaxCard:
      case TaxcardKind.NoWithholdingHousehold:
        settings.readonly = true;
        settings.showExampleTaxCard = false;
        break;
      case TaxcardKind.Undefined:
        settings.readonly = true;
        settings.showTaxPercent = false;
        break;
      case TaxcardKind.DefaultYearly:
        settings.showIncomeLimit = true;
        settings.showPreviousSalariesPaid = true;
        settings.requireIncomeLimit = true;
        settings.showExampleTaxCard = true;
        settings.upload = this.isSelf ? "required" : "show";
        settings.yearsAllowed = yearsAllowed;
        break;
      case TaxcardKind.Replacement:
        settings.showIncomeLimit = true;
        settings.showPreviousSalariesPaid = true;
        settings.showExampleTaxCard = true;
        settings.upload = this.isSelf ? "required" : "show";
        settings.yearsAllowed = yearsAllowed;
        settings.startDateOptions = {
            minDate: Dates.asJSDate(forYear + "-01-01"),
            maxDate: Dates.asJSDate(forYear + "-11-31"),
          };
        break;
    }
    this.addNewUi = settings;
  }
}
