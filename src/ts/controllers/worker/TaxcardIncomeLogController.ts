import { Arrays, Dates, Taxcard, TaxCardIncome, TaxCardIncomeType } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ListControllerBase } from "../bases";

/**
 * Handles user interface logic for viewing and editing the income log within a taxcard.
 */
export class TaxcardIncomeLogController extends ListControllerBase<Taxcard, TaxCardIncome> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers"];

  /** If true, the list can be edited, by default it is read-only. */
  public editable: boolean;

  /**
   * Creates a new TaxcardIncomeLogController.
   */
  constructor(protected uiHelpers: UiHelpers) {
    super(uiHelpers);
  }

  /** List that is edited */
  public get list(): TaxCardIncome[] {
    if (!this.parent) {
      return null;
    }
    return this.parent.incomeLog;
  }

  /**
   * Shows the edit dialog.
   * @param item Item to edit or string "new" for creating a new one.
   * @param isNew Optional way of specifying that the item is a new item (with default values, not yet added to the list).
   * If item is string "new", this parameter will have no effect (will always be true).
   */
  public showEditDialog(item: TaxCardIncome | "new", isNew = false) {
    if (!this.editable || (item !== "new" && this.isReadOnly(item))) {
      return null;
    }
    super.showEditDialog(item, isNew);
  }

  /** Gets the URL for the log entry edit dialog. */
  public getEditDialogTemplateUrl(): string {
    return "salaxy-components/worker/taxcard/TaxcardIncomeLogEditDialog.html";
  }

  /** Gets a new blank entry with default values. */
  public getBlank(): TaxCardIncome {
    return {
      type: TaxCardIncomeType.ExternalSalaries,
      paidAt: Dates.getToday(),
    };
  }

  /** Returns true, if the calculations paid are different than the calculations in the income log. */
  public get hasDiff(): boolean {
    return !!(this.list || []).find((x) => x.diff && x.diff !== "default");
  }

  /** Gets a sum from the list */
  public getSum(field: "tax" | "income"): number {
    if (!this.list) {
      return null;
    }
    return Arrays.sum(this.list, (x) => x[field]);
  }

  /** Returns true if the row is read-only */
  public isReadOnly(row: TaxCardIncome) {
    return row.type !== TaxCardIncomeType.ExternalSalaries;
  }
}
