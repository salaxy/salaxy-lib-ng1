﻿import * as angular from "angular";

import { AbsencePeriod, Absences, Arrays, DateRange, Dates, EnumerationsLogic, WorkerAbsences } from "@salaxy/core";

import { UiCrudHelpers, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { CalendarSeries } from "../helpers";

/**
 * Plain CRUD controller for WorkerAbsences.
 */
export class WorkerAbsencesCrudController extends ApiCrudObjectController<WorkerAbsences> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "Absences",
    "UiHelpers",
    "$location",
    "$routeParams",
    "UiCrudHelpers",
  ];

  /** UI view that is shown */
  public viewType: "overview" | "list" = "overview";

  /**
   * Years that are available in the drop-down selection.
   * Currently, only affects the calendar, but will probably also affect the lists etc.
   * Currently, starting from 2019 until this year.
   */
  public years: number[];

  /** Contains all the absences, once getAbsences() has been called. */
  public allAbsences: WorkerAbsences[];

  /** Contains original absences before unsaved changes, once getAbsences() has been called. */
  public allAbsencesOrig: WorkerAbsences[];

  /** Current year: Currently, affects only the chart */
  public currentYear: number;

  /**
   * The date for which the UI is rendered. Default is today.
   * For employment holiday years (WorkerHolidays or CalcWorktime), set this to salary period begin
   * => Specifies the the holiday year to choose by default from all holiday years that Worker has:
   * If the date is Jan-April will show the previous year, May-Dec will show the current year.
   */
  public forDate: string;

  /** Parameters related to holiday report (lomalista). */
  public reportParams: {
    /** Today's date: Set this for testing how user interface is rendered (which view is presented) at different times of year. */
    today: string;
    /** Start date of the view: Can be get/set directly by the view */
    start: string,
    /** Start date of the view: Can be get/set directly by the view */
    end: string,
    /** Underlying field of reportView */
    _view: "thisYear" | "lastYear" | "lastMonth" | "thisMonth" | "all",
  };

  private _employmentId: string;

  constructor(
    private fullApi: Absences,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    private uiCrudHelpers: UiCrudHelpers,
  ) { // Dependency injection
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Implement IController
   */
  public $onInit() {
    this.currentYear = Dates.getYear("today");
    this.years = Arrays.getRange(2019, this.currentYear);
    this.reportParams = {
      today: Dates.getToday(),
    } as any;
    this.reportView = "thisYear";
    // Fetching reportView initializes the rest of parameters with default values. Comment for lint if necessary.
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const necessaryInitCall = this.reportView;
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/workers",
      detailsUrl: this.detailsUrl || "/workers/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/Absences.html",
      oDataOptions: {},
    };
  }

  /**
   * Sets the employment ID and if it is different then starts the loading of the Absences object to that employment ID.
   */
  public set employmentId(value: string) {
    if (this._employmentId === value) {
      return;
    }
    if (!value) {
      if (this._employmentId) {
        this._employmentId = null;
        this.model = null;
        this.reload();
      }
      return;
    }
    this.fullApi.getForEmployment(value).then((model) => {
      this.model = model;
    });
  }
  /** Gets the employment ID */
  public get employmentId() {
    return this._employmentId;
  }

  /**
   * Gets or sets the report view that is currently shown.
   */
  public get reportView(): "thisYear" | "lastYear" | "lastMonth" | "thisMonth" | "all" {
    if (!this.reportParams._view) {
      // Use the setter for setting the defaault value so that dependent fields are also set
      this.reportView = "thisMonth";
    }
    return this.reportParams._view;
  }
  public set reportView(value: "thisYear" | "lastYear" | "lastMonth" | "thisMonth" | "all") {
    this.reportParams._view = value;
    const thisYear = Dates.getYear(this.reportParams.today);
    const todayMoment = Dates.getMoment(this.reportParams.today);
    switch (value) {
      case "thisYear":
        this.reportParams.start = thisYear + "-01-01";
        this.reportParams.end = thisYear + "-12-31";
        break;
      case "lastYear":
        this.reportParams.start = (thisYear - 1) + "-01-01";
        this.reportParams.end = (thisYear - 1) + "-12-31";
        break;
      case "thisMonth":
        this.reportParams.start = Dates.asDate(todayMoment.startOf("month"));
        this.reportParams.end = Dates.asDate(todayMoment.endOf("month"));
        break;
      case "lastMonth":
        this.reportParams.start = Dates.asDate(todayMoment.startOf("month").subtract(1, "month"));
        this.reportParams.end = Dates.asDate(todayMoment.endOf("month"));
        break;
      default:
        this.reportParams.start = this.years[0] + "-01-01";
        this.reportParams.end = this.years[this.years.length - 1] + "-12-31";
    }
  }

  /**
   * Maps an array of Absences to array of calendar series
   * @param source Absences to map.
   */
  public mapToCalendar = (source: WorkerAbsences[]): CalendarSeries[] => {
    return source.map((abs) => {
      return {
        key: abs.id,
        title: abs.workerSnapshot.displayName,
        description: `Palkallisia ${this.getPeriodCalculation(abs, "absencesPaid")}, palkattomia ${this.getPeriodCalculation(abs, "absencesUnpaid")}`,
        avatar: abs.workerSnapshot,
        data: abs,
        events: abs.periods.filter(this.filter).map((period) => ({
          start: period.period.start,
          end: period.period.end,
          summary: EnumerationsLogic.getEnumLabel("AbsenceCauseCode", period.causeCode) + ` ${Dates.getFormattedRange(period.period.start, period.period.end)}, ${period.period.days?.length || period.period.daysCount} päivää.`,
          data: period,
        })),
      };
    });
  };

  /**
   * Filter that is applied to the list
   * @param value Item in the list
   */
  public filter = (value: AbsencePeriod) => {
    if (this.reportParams.end && value.period.start > this.reportParams.end) {
      return false;
    }
    if (this.reportParams.start && value.period.end < this.reportParams.start) {
      return false;
    }
    return true;
  }

  /**
   * Saves a the Worker absences based on calendar input and updates the value in allAbsences property.
   * @param itemToSave Either a Worker absences object or keyword "changed" to save all cahnged items.
   */
  public saveAbsences(itemToSave: WorkerAbsences | "changed"): void {
    let itemsToSave: { ix: number, item: WorkerAbsences }[];
    if (itemToSave === "changed") {
      itemsToSave = this.allAbsences.map((item, ix) => angular.equals(this.allAbsences[ix], this.allAbsencesOrig[ix]) ? { ix: -1, item: null } : { ix, item });
    } else {
      itemsToSave = [{ ix: this.allAbsences.findIndex(x => x === itemToSave), item: itemToSave }];
    }
    itemsToSave = itemsToSave.filter(x => x.ix > -1);
    if (itemsToSave.length === 0) {
      this.uiHelpers.showAlert("Ei tallennettavaa", "Mikään tietueista ei ole muuttunut");
    }
    const loader = this.uiHelpers.showLoading(`Tallennetaan ${itemsToSave.length} tietuetta...`);
    const promises = itemsToSave.map((item) => {
      return this.api.save(item.item)
        .then((savedValue) => {
          this.allAbsences[item.ix] = savedValue;
          this.allAbsencesOrig[item.ix] = savedValue;
        });
    });
    Promise.all(promises).then(() => {
      loader.dismiss();
    });
  }

  /**
   * Show an edit dialog for the absences.
   * @param absences The Worker absences to update.
   */
   public showEditDialog(absences: WorkerAbsences): void {
    /* HACK: The edit dialog is not optimal, as it shows the entire edit worker dialog. Should be a separate edit absence periods dialog. */
    this.uiCrudHelpers.openEditWorkerDialog(absences.workerId, "default", "absences").then((result) => {
      if (result.action === "ok") {
        this.getAbsences();
      }
    });
  }

  /**
   * Shows the add worker dialog: A
   */
  public showAddWorker() {
    this.uiHelpers.openSelectEmployments().then((result) => {
      if (result.action === "ok" && result.item.length) {
        // TODO: Paranna openSelectEmployments-metodia niin, että ei voi valita jo luotuja työsuhteita.
        const existing = result.item.find((item) => {
          return this.allAbsences.find((x) => x.employmentId === item.id);
        });
        if (existing) {
          this.uiHelpers.showAlert("Työntekijä on jo poissaololistassa", `Työntekijä ${existing.otherPartyInfo.avatar.displayName} on jo poissaolojen listassa. Valitse työntekijät uudestaan.`);
          return;
        }
        result.item.forEach((item) => {
          const newItem = this.api.getBlank();
          newItem.employmentId = item.id;
          newItem.workerSnapshot = item.otherPartyInfo.avatar;
          this.allAbsences.push(newItem);
          this.allAbsencesOrig.push({} as any);
        });
        this.saveAbsences("changed");
      }
    });
  }

  /**
   * Gets the full absence objects for all Workers.
   */
  public getAbsences() {
    this.fullApi.getLatest().then((result: WorkerAbsences[]) => {
      this.allAbsences = result;
      this.allAbsencesOrig = angular.copy(result);
    });
  }

  /**
   * Returns true, if two objects are different.
   * @param obj1 First object to compare
   * @param obj2 Second object to compare
   */
  public isChanged(obj1, obj2): boolean {
    return !angular.equals(obj1, obj2);
  }

  /**
   * Gets a calculated value based on filtered absence periods.
   * TODO: Consider moving to core (there is the same code in AbsencePeriodsController)
   *
   * @param absences Worker absences object.
   * @param type Type of calculation
   */
  public getPeriodCalculation(absences: WorkerAbsences, type: "all" | "length" | "firstPeriod" | "lastPeriod" | "absencesPaid"
    | "absencesUnpaid" | "absencesHolidayAccrual" | "absencesNoHolidayAccrual" = "all"): number | DateRange {
    const list = absences.periods.filter(this.filter);
    switch (type) {
      case "all":
        return Arrays.sum(list, (x) => x.period.daysCount);
      case "length":
        return list.length;
      case "firstPeriod":
        return list.length ? list[0].period : null;
      case "lastPeriod":
        return list.length ? list[list.length - 1].period : null;
      case "absencesPaid":
        return Arrays.sum(list.filter((x) => x.isPaid), (x) => x.period.daysCount);
      case "absencesUnpaid":
        return Arrays.sum(list.filter((x) => !x.isPaid), (x) => x.period.daysCount);
      case "absencesHolidayAccrual":
        return Arrays.sum(list.filter((x) => x.isHolidayAccrual), (x) => x.period.daysCount);
      case "absencesNoHolidayAccrual":
        return Arrays.sum(list.filter((x) => !x.isHolidayAccrual), (x) => x.period.daysCount);
    }
    return null;
  }
}
