import * as angular from "angular";

import { EmploymentRelationType, WorkerAccount, WorkerListItem, WorkerLogic, Workers } from "@salaxy/core";

import { UiCrudHelpers, UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { EmploymentTaxcardsController } from "./EmploymentTaxcardsController";

/**
 * The new ApiCrudObject type of CRUD controller for the WorkerAccount.
 */
export class WorkerAccountCrudController extends ApiCrudObjectController<WorkerAccount> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Workers", "UiHelpers", "UiCrudHelpers", "$location", "$routeParams"];

  /** If true, the save/delete etc. buttons are not shown (they will come from the container / modal) */
  public hideButtons: boolean;

  /**
   * If set, the Worker details component can show infromation about the latest taxcard in the view.
   * @example
   * In the WorkerDetails.html view we set:
   * <salaxy-employment-taxcards employment-id="$ctrl.current.employmentId" ng-ref="$ctrl.taxcardsController"></salaxy-employment-taxcards>
   */
  public taxcardsController: EmploymentTaxcardsController;

  private _currentTab = null;

  /** Pension insurance type options for binding. */
  private pensionCalculationOptions = [];

  constructor(
    private fullApi: Workers,
    uiHelpers: UiHelpers,
    private uiCrudHelpers: UiCrudHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    ) {
    super(fullApi, uiHelpers, $location, $routeParams);
  }

  /**
   * Initialization code.
   */
  public $onInit() {
    super.$onInit();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/workers",
      detailsUrl: this.detailsUrl || "/workers/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/Workers.html",
      oDataOptions: {},
    };
  }

  /**
   * For the tab UI control, returns the current tab, if defined in the path
   * or set explicitly. Defaults to "default".
   */
  public get currentTab(): "default" | "calculations" | "taxcards" | "employment" | "calcRows" | "holidays" | "absences" {
    if (this._currentTab) {
      return this._currentTab;
    }
    const supportedHashes = ["default", "calculations", "taxcards", "employment", "calcRows", "holidays", "absences"];
    const hash = (this.$location ? this.$location.hash() || "" : "").trim().toLowerCase();
    return supportedHashes.indexOf(hash) > -1 ? hash as any : "default";
  }
  public set currentTab(value: "default" | "calculations" | "taxcards" | "employment" | "calcRows" | "holidays" | "absences") {
    this._currentTab = value;
  }

  /**
   * Gets a description text for a WorkerAccount that may be either
   * IWorkerListItem or WorkerAccount.
   * @param worker Worker account to describe. Either account or list item.
   */
  public getEmploymentDescription(worker: WorkerListItem | WorkerAccount): string {
    return WorkerLogic.getDescription(worker);
  }

  /**
   * Calls the Worker wizard and if a Worker is added continues to the given action.
   * @param mode What to do if the worker is created
   *
   * - "details" (default): Creates a new WorkerAccount and opens the Details-view for further editing.
   * - "list": Creates a new WorkerAccount and opens the list of workers view.
   * - "new-calc": Creates a new WorkerAccount and shows a new calculation for that worker.
   * - "edit-current": Edits the current item of Controller. Does not save.
   */
  public launchWorkerWizard(mode: "details" | "list" | "new-calc" | "edit-current") {
    if (mode === "edit-current")  {
      this.uiHelpers.openNewWorkerWizard(this.current)
      .then((result) => {
        if (result.action === "ok" && result.hasChanges) {
          // Modifications should already be reflected.
        }
      });
    } else {
      this.uiCrudHelpers.createNewWorker().then((result) => {
        if (result.action === "ok") {
          if (mode === "new-calc") {
            this.$location.path("/calc/details/" + result.item.worker.id + "/new-for-worker");
          } else if (mode === "list") {
            this.$location.path(this.getDefaults().listUrl);
          } else {
            this.$location.path(this.getDefaults().detailsUrl + result.item.worker.id);
          }
        }
      });
    }
  }

  /** Returns available pension insurance type options for employment relation type. */
  public getPensionCalculationOptions(employmentRelationType: EmploymentRelationType) {
    // get options
    const newPensionCalculationOptions = WorkerLogic.getPensionCalculationOptions(employmentRelationType);
    this.modifyOptions(newPensionCalculationOptions, this.pensionCalculationOptions);
    return this.pensionCalculationOptions;
  }

  private modifyOptions(
    source: ({
      /** Option value. */
      value: any,
      /** Option text.  */
      text: string,
      /** Option description. */
      title?: string,
    })[],
    target: ({
      /** Option value. */
      value: any,
      /** Option text.  */
      text: string,
      /** Option description. */
      title?: string,
    })[]) {
    if (angular.equals(source, target)) {
      return;
    }
    target.splice(0, target.length);
    target.push(...source);
  }

  /**
   * Checks if the given string is other identifier than Finnish Personal Identification Number
   * @param ssn given social security number
   */
  public isOtherIdentifier(ssn: string){

    /** 8th character is 9 */
    const regex = /^.{7}[9]/;
    return regex.test((ssn || "").trim());
  }

}
