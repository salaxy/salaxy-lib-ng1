import * as angular from "angular";

import { Ajax, WorkerAccount } from "@salaxy/core";

import { ODataQueryController } from "../../controllers/bases/ODataQueryController";

import { WorkerAccountCrudController } from "./WorkerAccountCrudController";

import { UiHelpers } from "../../services";

/**
 * Controller for creating stand-alone worker accounts.
 */
export class WorkerAssureController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers", "AjaxNg1"];

  /** Data binding field for submitAssureWorkerAccount() method  */
  public assureWorkerAccountReq = this.getEmptyAssureWorkerAccountReq();

  /**
   * Creates a new WorkerAssureController
   * @param uiHelpers - Salaxy ui helpers service.
   * @param ajax - Salaxy ajax service.
   * @ignore
   */
  constructor(
    private uiHelpers: UiHelpers,
    private ajax: Ajax,

  ) {
  }

  /** Controller initialization */
  public $onInit() {
    // Empty
  }

  /**
   * Calls the AssureWorkerAccount method in the API with the data defined in assureWorkerAccountReq
   */
  public submitAssureWorkerAccount(detailsCtrl: WorkerAccountCrudController, listCtrl: ODataQueryController): Promise<WorkerAccount> {
    const loading = this.uiHelpers.showLoading("Odota...");
    let method = "/partner/assureworker";
    method += "?officialId=" + encodeURIComponent(this.assureWorkerAccountReq.officialId);
    method += "&firstName=" + encodeURIComponent(this.assureWorkerAccountReq.firstName);
    method += "&lastName=" + encodeURIComponent(this.assureWorkerAccountReq.lastName);
    method += "&email=" + encodeURIComponent(this.assureWorkerAccountReq.email);
    method += "&telephone=" + encodeURIComponent(this.assureWorkerAccountReq.telephone);
    method += "&bankAccountIban=" + encodeURIComponent(this.assureWorkerAccountReq.bankAccountIban);
    if (this.assureWorkerAccountReq.startDate) {
      method += "&startDate=" + encodeURIComponent(this.assureWorkerAccountReq.startDate);
    }
    if (this.assureWorkerAccountReq.endDate) {
      method += "&endDate=" + encodeURIComponent(this.assureWorkerAccountReq.endDate);
    }
    return this.ajax.postJSON(method, "")
      .then((account: WorkerAccount) => {
          const worker = listCtrl.items.find((x) => x.otherId === account.id);
          if (worker == null) {
            listCtrl.items.unshift({
              createdAt: account.createdAt,
              updatedAt: account.updatedAt,
              otherId: account.id,
              otherPartyInfo: {
                avatar: account.avatar,
                officialId: account.officialPersonId,
                email: account.contact.email,
                telephone: account.contact.telephone,
                ibanNumber: account.ibanNumber,
              },
              data: {
                type: account.employment.type,
                pensionCalculation: account.employment.pensionCalculation,
              },
            });
          }
          detailsCtrl.model = account.id;

          this.resetAssureWorkerAccount();
          loading.dismiss();
          return account;
        });
  }

  /**
   * Resets assure worker account model.
   */
  public resetAssureWorkerAccount() {
    this.assureWorkerAccountReq = this.getEmptyAssureWorkerAccountReq();
  }

  /** Gets an empty assureWorkerAccountReq - used for reset. */
  private getEmptyAssureWorkerAccountReq() {
    return {
      officialId: null,
      firstName: null,
      lastName: null,
      email: null,
      telephone: null,
      bankAccountIban: null,
      startDate: null,
      endDate: null,
    };
  }

}
