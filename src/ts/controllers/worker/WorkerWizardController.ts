﻿import * as angular from "angular";

import { Avatar, SharedTaxcardExistsResult, Taxcard, TaxCard2019Logic, TaxcardApprovalMethod, TaxcardKind, Taxcards, WorkerAccount, WorkerLogic, Workers } from "@salaxy/core";

import { EditDialogParameters, UiHelpers, WizardService, WizardStep } from "../../services";
import { WizardController } from "../bases";
import { CalcHouseholdUsecaseController } from "../calc";
import { TaxcardCrudController } from "./TaxcardCrudController";
import { WorkerAccountCrudController } from "./WorkerAccountCrudController";

/**
 * Wizard for creating a new worker account.
 */
export class WorkerWizardController extends WizardController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$scope", "EditDialogParameters", "WizardService", "$controller", "Workers", "Taxcards", "UiHelpers"];

  /** Form data validity: This is bound from the form tag in the view. */
  public formDataValidity: boolean;

  /** The worker account that is being edited. */
  public model: WorkerAccount;

  /** Taxcard that is optionally added. */
  public taxcard: Taxcard;

  /**
   * Controller that is responsible for taxcard input.
   * Note, that this controller is only available when it has been spcifically set: Typically in the taxcard step / view.
   */
  public taxcardController: TaxcardCrudController;

  /**
   * Controller that is responsible for household usecase.
   * Note, that this controller is only available when it has been spcifically set: Typically in the work step / view.
   */
  public usecaseController: CalcHouseholdUsecaseController;

  /**
   * Result from checking whether the taxcard exists.
   */
  public taxCardExists: SharedTaxcardExistsResult;

  /** Crud controller that is used for business logic */
  private $crudController: WorkerAccountCrudController;

  /** Current profile */
  private profile: Avatar;

  /**
   * Creates a new WizardController with dependency injection.
   * @ignore
   */
  constructor(
    $scope: angular.IScope,
    private editDialogParameters: EditDialogParameters<WorkerAccount>,
    wizardService: WizardService,
    private $controller: angular.IControllerService,
    private workers: Workers,
    private taxcards: Taxcards,
    private uiHelpers: UiHelpers,
  ) {
    super($scope, wizardService);
  }

  /**
   * Initialization of the controller
   */
  public $onInit() {
    super.$onInit();
    this.wizardService.setSteps(this.getWizardSteps());
    if (!this.editDialogParameters) {
      throw new Error("No editDialogParameters coming in.");
    }
    this.model = this.editDialogParameters.current;
    this.$crudController = this.$controller("WorkerAccountCrudController as $crud", { $scope: this.$scope });
    this.$crudController.model = this.model;
    this.$crudController.$onInit();
  }

  /** Returns true if user can go forward in  the wizard  */
  public get canGoNext(): boolean {
    if (this.steps.length > this.step) {
      if (this.steps[this.step] && !this.steps[this.step].disabled) {
        if (this.formDataValidity === true || this.formDataValidity === null) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Navigates to the next step if possible.
   * Override adds the taxcard if personalId is there and resets it if personalId has changed.
   */
  public goNext() {
    if (this.usecaseController && this.usecaseController.updateWorkerAccount) {
      this.usecaseController.updateWorkerAccount();
    }
    if (this.model.officialPersonId) {
      if (!this.taxCardExists || this.taxCardExists.personalId !== this.model.officialPersonId) {
        this.taxcards.getSharedTaxcardExists(this.model.officialPersonId).then((result) => {
          this.taxCardExists = result;
          if (result.ownedExists && !result.sharedExists) {
            this.taxcard = result.ownedTaxcard;
          } else {
            this.taxcard = TaxCard2019Logic.getBlank(this.model.officialPersonId);
          }
          if (this.model.officialPersonId !== result.personalId) {
            // Validated personal ID may be slightly different.
            this.model.officialPersonId = result.personalId;
          }
        });
      }
    }
    super.goNext();
  }

  /** Sets the proposed employment values for the worker */
  public setProposedValues() {
    WorkerLogic.setEmploymentDefaultValues(this.model);
    // Remove empty rows
    this.model.employment.work.salaryDefaults = this.model.employment.work.salaryDefaults.filter((x) => x.rowType);
  }

  /** Populates worker with test data. */
  public populateWithTestData() {
    WorkerLogic.populateWorkerWithTestData(this.model);
  }

  /** Create worker for public profile */
  public selectFromProfile() {
    this.uiHelpers.showConfirm("Henkilötietojen haku",
    `Haluatko luoda työsuhteen henkilön ${this.profile.displayName} kanssa ja katsoa hänen palkanmaksutietonsa?\nHUOM: Tietoja saat katsoa vain, jos aiot todella maksaa palkkaa ko. henkilölle.\nTieto työsuhteesta välittyy työntekijälle.`)
      .then((result) => {
        if (result === true) {
          this.workers.getSingle(this.profile.id).then((worker) => {
            if (worker) {
              this.modifyObject(this.model, worker);
              this.$crudController.$onInit();
            } else {
              this.modifyObject(this.model, WorkerLogic.getBlank());
              this.model.id = this.profile.id;
              this.model.avatar = angular.copy(this.profile);
              this.workers.save(this.model).then((newWorker) => {
                this.modifyObject(this.model, newWorker);
                this.$crudController.$onInit();
              });
            }
          });
        } else {
          // do nothing
          this.profile = null;
          this.modifyObject(this.model, WorkerLogic.getBlank());
          this.$crudController.$onInit();
        }
      });
  }

  /**
   * Deletes a calculation row.
   * @param rowIndex - Zero based row index of the row that should be deleted.
   */
  public deleteSalaryDefaultsRow(rowIndex: number) {
    this.model.employment.work.salaryDefaults.splice(rowIndex);
  }

  /** Worker wizard configuration */
  public getWizardSteps(): WizardStep[] {
    return [
      {
        title: "SALAXY.NG1.WorkerWizardComponent.step1.title", // "Työntekijän tiedot",
        view: "salaxy-components/modals/worker/WorkerWizard/person.html",
      },
      {
        title: "SALAXY.NG1.WorkerWizardComponent.step2.title", // "Työsuhde",
        view: "salaxy-components/modals/worker/WorkerWizard/type.html",
      },
      {
        title: "SALAXY.NG1.WorkerWizardComponent.step3.title", // "Palkanmaksu",
        view: "salaxy-components/modals/worker/WorkerWizard/work.html",
      },
      {
        title: "SALAXY.NG1.WorkerWizardComponent.step4.title", // "Verokortti",
        view: "salaxy-components/modals/worker/WorkerWizard/taxcard.html",
      },
    ];
  }

  /** Saves the taxcard and closes the dialog. */
  public saveAndClose(closeFunction: (result: any) => any) {
    let card = this.taxcard;
    if (card.card.kind === TaxcardKind.Undefined) {
      // Currently, we allow no selection. This may change later, but then we need to pass the information to validation.
      card = null;
    }
    closeFunction({
      action: "ok",
      worker: this.model,
      taxcard: card,
    });
  }

  /**
   * Approve / reject the shared taxcard.
   * @param method "approve" or "reject".
   */
  public approveShared(method: TaxcardApprovalMethod) {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.taxcards.approveShared(this.taxCardExists.uri, method).then((result) => {
      loader.dismiss();
      if (method !== TaxcardApprovalMethod.Reject) {
        this.taxcard = result;
      }
      // Just mark it false to not show the UI.
      this.taxCardExists.sharedExists = false;
    });
  }

  /**
   * Checks if the given string is other identifier than Finnish Personal Identification Number
   * @param ssn given social security number
   */
  public isOtherIdentifier(ssn: string){

    /** 8th character is 9 */
    const regex = /^.{7}[9]/;
    return regex.test((ssn || "").trim());
  }

  /** Modify object without losing reference */
  private modifyObject(obj: any, source: any) {
    for (const prop of Object.getOwnPropertyNames(obj)) {
      delete obj[prop];
    }
    Object.assign(obj, source);
  }
}
