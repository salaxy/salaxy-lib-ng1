import * as angular from "angular";
import { rrulestr } from "rrule";

import { ApiItemType, CalendarAction, CalendarActionType, CalendarEvent, CalendarEvents, Dates, EnumerationsLogic } from "@salaxy/core";

import { UiHelpers, WizardService, WizardStep } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { CalendarHelper, RRuleFinnish } from "../../helpers";
import { CalendarSeries, CalendarUiEvent } from "../helpers";

/**
 * Controller for implementing Calendars that contain CalendarEvents (iCalendar).
 */
export class CalendarEventsCrudController extends ApiCrudObjectController<CalendarEvent> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "CalendarEvents",
    "UiHelpers",
    "$location",
    "$routeParams",
    "$sce",
    "WizardService",
  ];

  /** Instance of calendar helper for Date/Time related UI handling. */
  public helper: CalendarHelper;

  /** Styling classes for columns. */
  public colWidthClasses = ["col-sm-12", "col-sm-12", "col-sm-6", "col-sm-4", "col-sm-3", "col-sm-fifth", "col-sm-2"];

  /** Gets all the steps in the Wizard */
  public get steps(): WizardStep[] {
    return this.wizardService.getSteps();
  }

  /** Gets the number of the currently active step */
  public get step(): number {
      return this.wizardService.activeStepNumber;
  }
  /** Sets the number of the currently active step */
  public set step(stepNumber: number) {
      this.wizardService.activeStepNumber = stepNumber;
  }

  /** The currently selected step object */
  public get currentStep(): WizardStep {
      return this.wizardService.getCurrentStepObject();
  }

  /** Active styling properties */
  public get style() {
      return {
          colWidthClass: this.colWidthClasses[this.steps.length],
      };
  }

  /** Returns true if goNext is enabled  */
  public get canGoNext(): boolean {
      if (this.steps.length > this.step) {
          if (this.steps[this.step] && !this.steps[this.step].disabled) {
              return true;
          }
      }
      return false;
  }

  /** Returns true if goNext is enabled  */
  public get canGoPrevious(): boolean {
      if (this.step > 1 && !this.steps[this.step - 2].disabled) {
          return true;
      }
      return false;
  }

  /**
   * Navigates to the next step if possible
   */
  public goNext() {
      if (this.canGoNext) {
          this.step++;
      }
  }

  /** Navigates to the previous step if possible */
  public goPrevious() {
      if (this.canGoPrevious) {
          this.step--;
      }
  }

  /**
   * Creates a new WorkflowController
   * @ignore
   */
  constructor(
    private fullApi: CalendarEvents,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    $sce: angular.ISCEService,
    private wizardService: WizardService,
  ) {
    super(fullApi, uiHelpers, $location, $routeParams);
    this.helper = new CalendarHelper($sce);
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    this.wizardService.setSteps([
      {
        title: "Perustiedot",
        active: true,
      },
      {
        title: "Muistutukset ja viestit",
      },
      {
        title: "Esikatselu",
      },
    ])
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/calendar",
      detailsUrl: this.detailsUrl || "/calendar/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/calendar/Events.html",
      oDataOptions: {},
    };
  }

  /** Adds a new recurrence rule to the event */
  public addRecurrenceRule() {
    this.current.event.recurrenceRules = this.current.event.recurrenceRules || [];
    this.current.event.recurrenceRules.push("FREQ=MONTHLY;INTERVAL=1");
  }

  /**
   * Gets the recurrence rule as human readable text.
   * @param rrule The iCalendar Recurrence rule that should be interpreted.
   */
  public getRRuleText(rrule: string) {
    const rule = rrulestr(rrule, {
      // dtstart: Dates.asDate,
    });
    return rule.toText((id) => {
      return RRuleFinnish.texts[id.toString()] || id.toString();
    }, RRuleFinnish, (year: number, month: string, day: number) => {
      return `${day}. ${month}ta ${year}`;
    });
  }

  /** Adds a new action to the event */
  public addAction() {
    this.current.event.actions = this.current.event.actions || [];
    const newAction: CalendarAction = {
      type: this.current.event.actions.length ? CalendarActionType.Email : CalendarActionType.CreateItem,
    };
    this.current.event.actions.push(newAction);
  }

  /**
   * Deletes an item in the recurrence rules
   * @param index The index number of the item to delete.
   */
  public deleteRecurrence(index: number): void {
    this.current.event.recurrenceRules.splice(index, 1);
  }

  /**
   * Deletes an item in the actions
   * @param index The index number of the item to delete.
   */
  public deleteAction(index: number): void {
    this.current.event.actions.splice(index, 1);
  }

  /**
   * Saves the calendar item.
   * Override done so that durations (timespans) are converted from number to string.
   */
  public save(): Promise<CalendarEvent> {
    this.checkDurations();
    return super.save();
  }

  /**
   * Shows the detail view for the event in a dialog.
   * @param id Identifier of the calendar event to edit.
   * Use "new" to add a new item.
   */
  public showDetailsDialog(id: string | "new"): void {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.loading");
    const title = id === "new" ? "Uusi kalenteritapahtuma" : "Muokkaa tapahtumaa";
    this.fullApi.getSingle(id).then((item) => {
      loader.setHeading("Muokkaus auki");
      this.uiHelpers.openEditCalendarEvent(item, title).then((result) => {
        if (result.action === "ok") {
          loader.setHeading("SALAXY.UI_TERMS.isSaving");
          this.fullApi.save(result.item).then(() => {
            loader.dismiss();
            if (this.odataController) {
              this.odataController.reload();
            }
          });
        } else {
          loader.dismiss();
        }
      });
    });
  }

  /** Assures that the durations are strings, not numbers. The UI binding makes them numbers. */
  public checkDurations() {
    if (this.current?.event.duration) {
      this.current.event.duration = this.current.event.duration.toString();
    }
    (this.current?.event.actions || []).forEach((action) => {
      if (action.triggerDuration) {
        action.triggerDuration = action.triggerDuration.toString();
      }
    });
  }

  /**
   * Preview calendar is clicked
   * @param type event or empty day
   * @param date Date as string
   * @param calEvent Calendar event that is clicked.
   */
  public calendarClick(type: "event" | "day", date: string, calEvent: CalendarUiEvent) {
    if (type === "event") {
      let text;
      const action = this.preview.event.actions.find((x) => x.id === calEvent.data.id);
      if (action) {
        text = `${EnumerationsLogic.getEnumLabel("CalendarActionType", action.type)}: ${Dates.getFormattedDate(calEvent.start)}.
${action.summary || "Ei kuvausta"} (ajastus: ${action.triggerDuration || action.triggerDateTime})

${action.description || ""}`;
      } else {
        text = `Toistuva tapahtuma: ${Dates.getFormattedDate(calEvent.start)}.`;
      }
      this.uiHelpers.showAlert(calEvent.summary, text);
    }
  }

  public preview: CalendarEvent & {
    start?: string,
    end?: string,
  };

  /**
   * Reloads the preview from the server.
   */
  public reloadPreview() {
    this.preview = null;
    this.checkDurations();
    this.fullApi.recalculate(this.current).then((result: CalendarEvent) => {
      this.preview = result;
      // TODO: Figure out a good start/end based on the result.
      this.preview.start = "2021-04-01";
      this.preview.end = "2021-08-01";
    });
  }

  /** Maps the occurence from the preview object to calendar. */
  public calendarMapper = (src: CalendarEvent): CalendarSeries[] => {
    const result: CalendarSeries = {
      key: "default",
      title: "Preview",
      events: [],
    };
    const icons = {
      "undefined": null,
      "display": "fa-bell",
      "email": "fa-envelope",
      "audio": "fa-volume-off",
      "createItem": "fa-file",
      "script": "fa-cog",
    };
    const today = Dates.getToday();
    result.events = src.occurenceInfo.next.map((occ) => {
      return {
        start: occ.start,
        end: occ.end,
        summary: occ.summary,
        icon: icons[occ.actionType],
        cssClass: occ.start <= today ? "text-warning" : null,
        description: occ.debug,
        data: { id: occ.id },
      }
    });
    return [result];
  };

  /**
   * Selects business object for the current event
   */
  public selectBo() {
    const objType = this.current.bo.type;

    switch (objType) {
      case ApiItemType.Calculation:
        this.uiHelpers.openSelectCalcs("draft", "Valitse yksi laskelma").then((value) => {
          if (value.action === "ok") {
            this.current.bo.id = value.item[0]?.id;
          }
        });
        return;
      case ApiItemType.CalculationPaid:
        this.uiHelpers.openSelectCalcs("paid", "Valitse yksi laskelma").then((value) => {
          if (value.action === "ok") {
            this.current.bo.id = value.item[0]?.id;
          }
        });
        return;
      case ApiItemType.PayrollDetails:
        this.uiHelpers.showAlert("Ajasta Palkkkalistat-näkymästä", `Palkkalistan voit ajastaa Palkkalistat-osiosta => Uusi palkkalista.`);
        return;
      case ApiItemType.Employment:
        this.uiHelpers.openSelectEmployments("Valitse yksi työntekijä").then((value) => {
          if (value.action === "ok") {
            this.current.bo.id = value.item[0]?.id;
          }
        });
        return;
      default:
        this.uiHelpers.showAlert("Ei vielä tuettu", `Tietue tyyppiä "${objType}" ei ole vielä tuettu ajastuksessa.`);
        return;
    }
  }

  /**
   * Gets or sets the value of whether the e-mail notification should be sent.
   * Used by simplified view as boolean: Adds or removes a second item to actions array.
   */
   public get notificationEnabled() {
    return !!this.current.event.actions[1];
  }
  public set notificationEnabled(value: boolean) {
    if (value) {
      this.current.event.actions[1] = {
        type: CalendarActionType.Email,
        summary: "Muista hyväksyä palkkalista",
        description: null,
        attendees: [{ email: null}],
      };
    } else {
      this.current.event.actions.length = 1;
    }
  }

  /** Formats the duration as human readable string */
  public static formatDuration(duration: string): string {
    // TODO: Implement formatting using Moment or the future inccoming dates library.
    return duration;
  }
}
