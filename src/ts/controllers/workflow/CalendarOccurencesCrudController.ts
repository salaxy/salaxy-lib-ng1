import * as angular from "angular";

import { CalendarOccurences, CalendarOccurence, Dates, CalendarEventListItem, DateRange } from "@salaxy/core";

import { UiHelpers } from "../../services";
import { ApiCrudObjectController } from "../bases";
import { CalendarHelper } from "../../helpers";

/**
 * Controller for implementing Calendars that contain CalendarEvents (iCalendar).
 */
export class CalendarOccurencesCrudController extends ApiCrudObjectController<CalendarOccurence> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "CalendarOccurences",
    "UiHelpers",
    "$location",
    "$routeParams",
    "$sce",
  ];

  /** Instance of calendar helper for Date/Time related UI handling. */
  public helper: CalendarHelper;

  /** The date range that is shown in the calendar and loaded from the server. */
  public range: DateRange;

  /** Comma separated list of categories that are fetched from the server and shown in the calendar.. */
  public categories: string;

  /** Preset selection of categories user interface. */
  public categorySelections = {
    calendarEvent: "Palkanlaskijan kalenteri",
    action: "Automaattiset työnkulut",
    holiday: "Lomat",
    absencePeriod: "Poissaolot",
    "calculation,payroll": "Laskelmat ja palkkalistat",
  };

  /** Lists the occurences for the current week based on the current day. */
  public currentWeek: {
    /** Date of teh day */
    date: string,
    /** Occurences on the day */
    occurences: CalendarOccurence[],
  }[];

  /** Occurences on currentDay */
  public todayOccurences: CalendarEventListItem[];

  private _currentDay: string;

  /**
   * Creates a new WorkflowController
   * @ignore
   */
  constructor(
    private fullApi: CalendarOccurences,
    uiHelpers: UiHelpers,
    $location: angular.ILocationService,
    $routeParams: any,
    $sce: angular.ISCEService,
  ) {
    super(fullApi, uiHelpers, $location, $routeParams);
    this.helper = new CalendarHelper($sce);
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // TODO: Figure out the initial value and how to change this.
    this.range = this.range || {
      start: "2021-04-01",
      end: "2021-04-30",
    };
    this.categories = this.categories || "calendarEvent";
    // TODO: Probably not the cleanest way => Find a better approach to attach to new data in items.
    this.odataController.onDataLoaded = () => {
      this.dataLoaded();
      this.currentDay = Dates.getToday();
    };
  }

  /** Reload the OData query using the range and categories parameters */
  public reloadOdata(): void {
    // HACK: Add on day to end and use lt instead of le
    this.odataController.options.$filter = `logicalDate ge ${this.range.start || Dates.getToday()} and endAt le ${this.range.end || Dates.getToday()}`;
    this.odataController.reload();
  }

  /** Implements the getDefaults of ApiCrudObjectController */
  public getDefaults() {
    return {
      listUrl: this.listUrl || "/calendar-occurences",
      detailsUrl: this.detailsUrl || "/calendar-occurences/details/",
      oDataTemplateUrl: "salaxy-components/odata/lists/calendar/Occurences.html",
      oDataOptions: { },
    };
  }

  /**
   * Gets or sets the current day for the daily and weekly calendar.
   * Setting to null will set the day as today.
   */
   public get currentDay() {
    if (!this._currentDay) {
      this._currentDay = Dates.getToday();
    }
    return this._currentDay;
  }
  public set currentDay(value: string) {
    value = Dates.asDate(value);
    this._currentDay = value || Dates.getToday();
    const nextDay = Dates.asDate(Dates.getMoment(this._currentDay).add(1, "day"));
    this.todayOccurences = this.odataController.items.filter(x => x.data.end >= this._currentDay && x.data.start <= nextDay);
    const mondayMoment = Dates.getMoment(this._currentDay).day(1);
    this.currentWeek = [];
    for (let i = 0; i < 7; i++) {
      const date = Dates.asDate(mondayMoment);
      const nextDay = Dates.asDate(mondayMoment.add(1, 'days'))
      this.currentWeek.push({
        date,
        occurences: this.odataController.items.filter(x => x.data.end >= date && x.data.start <= nextDay),
      });
    }
  }

  /**
   * Called by the OData controller when the data is loaded.
   */
  public dataLoaded() {
    this.helper.calendarPeriods = this.odataController.items.map((x) => ({
      title: x.shortText,
      description: "DESCR: " + x.shortText,
      range: {
        start: x.data.start,
        end: x.data.end,
      },
    }));
  }
}
