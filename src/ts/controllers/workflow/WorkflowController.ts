import * as angular from "angular";

import {
  SessionUserCredential,
  WorkflowEvent,
} from "@salaxy/core";

import {
  CredentialService,
  SessionService,
  UiHelpers,
  WorkflowService,
} from "../../services";

import { ApiCrudObjectController } from "../bases/ApiCrudObjectController";

import { WorkflowEditorConfig } from "./WorkflowEditorConfig";

/**
 * Controller for implementing workflow logic.
 */
export class WorkflowController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "WorkflowService",
    "UiHelpers",
    "SessionService",
    "CredentialService",
  ];

  /**
   * API CRUD controller for the object.
   */
  public apiCtrl: ApiCrudObjectController<any>;

  /** Returns true if the update is in progress */
  public isUpdating = false;

  /**
   * Function that is called when a new workflow action has happened..
   * Function has the following locals:  item: the parent item for workflow, isDelete: true/false if the workflow action was a removal of the event.
   */
  public onChange: ( params: {
    /** Item details after change */
    item: any,
    /** Delete/creation information */
    isDelete: boolean,
   }) => void;

  /**
   * Creates a new WorkflowController
   * @ignore
   */
  constructor(
    protected workflowService: WorkflowService,
    protected uiHelpers: UiHelpers,
    protected sessionService: SessionService,
    protected credentialService: CredentialService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit() {
    // no initializations
  }

  /**
   * Returns the current workflow event of the given type.
   * @param wfType Event type of the workflow event.
   * @returns Returns the current workflow event of the given type.
   */
  public getWorkflowEvent(wfType: string): WorkflowEvent {
    return this.workflowService.getWorkflowEvent(this.apiCtrl, wfType);
  }

  /**
   * Returns true if the workflow of the current item has an event of the given type.
   * @param wfType Event type of the workflow event.
   * @returns Returns true if the current item contains an event of the given type.
   */
  public hasWorkflowEvent(wfType: string): boolean {
    return this.workflowService.hasWorkflowEvent(this.apiCtrl, wfType);
  }

  /**
   * Adds/updates the workflow event for the current item.
   *
   * @param wfEvent Workflow event to add/update.
   * @returns Reloaded item.
   */
  public saveWorkflowEvent(wfEvent: WorkflowEvent): Promise<any> {
    if (this.isUpdating) {
      return;
    }
    this.isUpdating = true;
    return this.workflowService.saveWorkflowEvent(this.apiCtrl, wfEvent).then ( (result) => {
      this.isUpdating = false;
      this.onChange({ item: result, isDelete: false });
      return result;
    });
  }

  /**
   * Deletes the given event or all events with given type.
   *
   * @param wfIdOrType Id or type of the workflow event.
   * @returns - Reloaded item.
   */
  public deleteWorkflowEvent(wfIdOrType: string): Promise<any> {
    if (this.isUpdating) {
      return;
    }
    this.isUpdating = true;
    return this.workflowService.deleteWorkflowEvent(this.apiCtrl, wfIdOrType).then ( (result) => {
      this.isUpdating = false;
      this.onChange({ item: result, isDelete: true});
      return result;
    });
  }

  /**
   * Shows a workflow editor for the given event.
   * Saves the event and reloads the item.
   *
   * @param config Configuration for the worklow editor.
   * @param wfEvent Workflow event for the editor.
   * @returns - Reloaded item.
   */
  public showWorkflowEventEditor(config: WorkflowEditorConfig, wfEvent: WorkflowEvent): Promise<any> {
    if (!this.apiCtrl.current) {
      return Promise.resolve(this.apiCtrl.current);
    }

    config.title =  config.title || "Ongelman kuvaus";
    config.okText = "SALAXY.UI_TERMS.save";
    config.cancelText = "SALAXY.UI_TERMS.close";

    return this.uiHelpers.openEditDialog(
      "salaxy-components/modals/workflow/WorkflowEventEdit.html",
      wfEvent || {},
      config,
    ).then((dialogResult) => {
      if (dialogResult.action === "ok") {
        return this.saveWorkflowEvent(dialogResult.item);
      } else {
        return Promise.resolve(this.apiCtrl.current);
      }
    });
  }

  /** Returns avatar for the given credential. */
  public getCredential(credentialId: string): SessionUserCredential {
    return (this.credentials.find( (x) => x.id === credentialId) || {});
  }

  /** Returns credentials for the current account. */
  public get credentials(): SessionUserCredential[] {
    return this.workflowService.credentials;
  }

  /** Returns the credential for current session */
  public get self(): SessionUserCredential {
    return this.workflowService.self;
  }

}
