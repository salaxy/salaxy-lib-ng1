/** Interface for workflow dialog configuration object. */
export interface WorkflowEditorConfig {

  /** Title text for the dialog */
  title: string;
  /** Possibility to override the ok-button text. The given text is run through translation. */
  okText: string;
  /** Possibility to override the cancel-button text. The given text is run through translation. */
  cancelText: string;
  /** If true, shows the control for the assignedTo -property. */
  // showAssignedTo: boolean;
  /** If true, shows the control for the message -property. */
  // showMessage: boolean;
  /** If true, shows the control for the ui -property. */
  // showUi: boolean;
}
