import * as angular from "angular";

/**
 * Appends the given element to the current element.
 * The existing element will be removed from the source parent element.
 * The attribute value should resolve to an existing DOM node or string.
 *
 * @example
 * ```html
 * <div salaxy-append-node="tab.headingElement"></div>
 * ```
 */
export class AppendNodeDirective implements angular.IDirective {

    /**
     * Factory for directive registration.
     * @ignore
     */
    public static salaxyAppendNode() {
        const factory = () => new AppendNodeDirective();
        factory.$inject = [];
        return factory;
    }

    /**
     * Applies to attributes only.
     * @ignore
     */
    public restrict = "A";

   /**
    * Creates a new instance of the directive.
    */
    constructor() {
        // initialization
     }

   /**
    * Evaluates the attribute value (DOM node or default string), and appends it to the current element.
    * @ignore
    */
    public link(scope: any, element: any, attrs: any) {
      const node = scope.$eval(attrs.salaxyAppendNode);
      element.append(node);
    }
}
