import * as angular from "angular";

import { Dates } from "@salaxy/core";

/**
 * Validates the datepicker manual input using the same rules
 * as the actual calendar selection control.
 * Currently this directive is used internally in salaxy-datepicker-popup only.
 *
 */
export class DatepickerInputValidationDirective implements angular.IDirective {

  /**
   * Factory for directive registration.
   * @ignore
   */
  public static salaxyDatepickerInputValidation() {
    const factory = () => new DatepickerInputValidationDirective();
    factory.$inject = [];
    return factory;
  }

  /**
   * Applies to attributes only.
   * @ignore
   */
  public restrict = "A";

  /**
   * Requires model.
   * @ignore
   */
  public require = "ngModel";

  /**
   * Creates a new instance of the directive.
   */
  constructor() {
    // initialization
  }

  /**
   * Validates the datepicker manual input using the same rules
   * as the actual calendar selection control.
   * @ignore
   */
  public link(scope: any, element: any, attrs: any, ngModel: any) {

    // Validates date if it has a value:
    // - within minDate and maxDate of datepicker options
    // - not disabled according to datepicker options' dateDisabled function
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ngModel.$validators.invalidDate = (modelValue, viewValue) => {
      if (!modelValue) {
        return true;
      }

      const datepickerOptions = scope.$eval(attrs.datepickerOptions);
      if (!datepickerOptions) {
        return true;
      }

      if (datepickerOptions.minDate || datepickerOptions.maxDate) {
        const modelValueMoment = Dates.getMoment(modelValue);

        if (datepickerOptions.minDate) {
          const minDateMoment = Dates.getMoment(datepickerOptions.minDate);
          if (modelValueMoment.isBefore(minDateMoment, "day")) {
            return false;
          }
        }

        if (datepickerOptions.maxDate) {
          const maxDateMoment = Dates.getMoment(datepickerOptions.maxDate);
          if (modelValueMoment.isAfter(maxDateMoment, "day")) {
            return false;
          }
        }
      }

      if (datepickerOptions.dateDisabled && datepickerOptions.dateDisabled({ date: Dates.asJSDate(modelValue) })) {
        return false;
      }

      return true;
    };
  }
}
