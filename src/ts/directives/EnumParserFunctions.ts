/**
 * Parser and formatter functions for different enum inputs.
 * This is required for AngularJS versions less than 1.6
 */
export class EnumParserFunctions {

  /**
   * Boolean conversion.
   * @example
   * ```html
   * <select ng-model="model.value" sxy-enum-as-boolean>
   *  <option value="null">Null</option>
   *  <option value="true">True</option>
   *  <option value="false">False</option>
   * </select>
   * ```
   */
  public static sxyEnumAsBoolean() {
    // eslint-disable-next-line @typescript-eslint/unbound-method
    return () => EnumParserFunctions.directive("sxyEnumAsBoolean", EnumParserFunctions.booleanParser);
  }

  private static directive(name: string, parser: (input: any) => any, formatter: (input: any) => string = null): any {
    return {
      restrict: "A",
      require: "ngModel",
      link: EnumParserFunctions.getLink(name, parser, formatter),
    };
  }

  private static getLink(name: string, parser: (input: any) => any, formatter: (input: any) => string): any {
    return (scope: any, element: any, attrs: any, ngModel: any) => {
      ngModel.$parsers.push(parser);
      // eslint-disable-next-line @typescript-eslint/unbound-method
      ngModel.$formatters.push(formatter || EnumParserFunctions.defaultFormatter);
    };
  }

  private static booleanParser(val: any): any {
    switch (val) {
      case "null":
        return null;
      case "false":
        return false;
      case "true":
        return true;
    }
    return val;
  }

  private static defaultFormatter(val: any): string {
    return "" + val;
  }

}
