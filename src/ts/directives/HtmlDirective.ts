import * as angular from "angular";
import marked from "marked";
import { Ng1Translations} from "../services/ui";

/**
 * Directive for binding html for the element.
 * The attribute sxy-html should contain the translation key for the html content.
 * This directive should be used instead of the AngularJs ng-bind-html element.
 * Interpolation parameters for translation can be given using params -attribute.
 *
 * @example
 * ```html
 * <p class="lead" sxy-html="SALAXY.NG1.UserInfoComponent.description.html" ></p>
 * ```
 */
export class HtmlDirective implements angular.IDirective {

    /**
     * Factory method for the directive creation.
     * @ignore
     */
    public static sxyHtml(): any {
        const factory = (translate: Ng1Translations, $sce: angular.ISCEService) => new HtmlDirective(translate, $sce);
        factory.$inject = ["Ng1Translations", "$sce"];
        return factory;
    }

    /**
     * Directive restrictions.
     * @ignore
     */
    public restrict = "A";

   /**
    * Creates a new instance of the directive.
    */
    constructor(private translate: Ng1Translations, private $sce: angular.ISCEService) {
    }

   /**
    * Compile function for the directive.
    * @ignore
    */
    public compile(cElement: any, cAttr: any): any { // eslint-disable-line @typescript-eslint/no-unused-vars
      return  (scope: any, element: any, attr: any) => {

        const endsWith = (str1: string, str2: string) => {
          return str1.substring(str1.length - str2.length, str1.length) === str2;
        };

        const params = () => scope.$eval(attr.params);
        const sxyHtml = () => attr.sxyHtml;

        scope.$watch( () => sxyHtml() + JSON.stringify(params()), () => {
          let html = this.translate.get(sxyHtml(), params());
          if (endsWith(sxyHtml(), ".md")) {
            html = marked(html);
          }
          element.html(this.$sce.getTrustedHtml(html));
        });
      };
    }
}
