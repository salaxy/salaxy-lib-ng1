import * as angular from "angular";
import { SessionService } from "../services";

/**
 * Shows the element if the user is in the given role / in one of the given roles.
 * The attribute value should resolve to either role string or array of role strings.
 *
 * @example
 * ```html
 * <div *salaxy-if-role="anon">You are not logged in.</div>
 * <div *salaxy-if-role="household,worker">This text is shown to Household or Worker roles.</div>
 * ```
 */
export class IfRoleDirective implements angular.IDirective {

    /**
     * Factory method for the directive creation.
     * @ignore
     */
    public static sxyIfRole() {
        const factory = (sessionService: SessionService, ngIfDirective: any) => new IfRoleDirective(sessionService, ngIfDirective);
        factory.$inject = ["SessionService", "ngIfDirective"];
        return factory;
    }

    /**
     * Directive restrictions.
     * @ignore
     */
    public restrict;

    /**
     * Transclusion.
     * @ignore
     */
    public transclude;

    /**
     * Priority parameter.
     * @ignore
     */
    public priority;

    /**
     * Terminal parameter.
     * @ignore
     */
    public terminal;

    /** Uses the original ngIf class as bases for the functionality. */
    private ngIf: any;

   /**
    * Creates a new instance of the directive.
    */
    constructor(private sessionService: SessionService, ngIfDirective: any) {
        this.ngIf = ngIfDirective[0];
        this.restrict = this.ngIf.restrict;
        this.transclude = this.ngIf.transclude;
        this.priority = this.ngIf.priority;
        this.terminal = this.ngIf.terminal;
     }

   /**
    * Link function for the directive.
    * @ignore
    */
    public link(scope: any, element: any, attrs: any) {
        const value = attrs.sxyIfRole || attrs.salaxyIfRole;
        attrs.ngIf = () => {
            return this.sessionService.isInSomeRole(value);
        };
        // eslint-disable-next-line prefer-spread, prefer-rest-params
        this.ngIf.link.apply(this.ngIf, arguments);
    }
}
