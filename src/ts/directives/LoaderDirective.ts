import * as angular from "angular";

/**
 * Loading indicator directive.
 * Toggles class "salaxy-loading" on an element. Toggling is based on  "salaxy-loader-show" and "salaxy-loader-hide" events from the
 * LoaderInterceptor which intercepts http calls.
 * @example
 * ```html
 * <div salaxy-loader class="spinner">
 *   <div class="bounce1"></div>
 *   <div class="bounce2"></div>
 *   <div class="bounce3"></div>
 * </div>
 * ```
 */
export class LoaderDirective implements angular.IDirective {

  /**
   * Factory for the directive creation.
   * @ignore
   */
  public static salaxyLoader() {
    const factory = () => new LoaderDirective();
    factory.$inject = [];
    return factory;
  }

  /**
   * Directive restrictions.
   * @ignore
   */
  public restrict = "A";

  /**
   * Creates a new instance of the directive.
   */
  constructor() {
    // empty
  }

  /**
   * Link function for the directive.
   * @ignore
   */
  public link(scope: any, element: any, attrs: any) { // eslint-disable-line @typescript-eslint/no-unused-vars
    scope.$on("salaxy-loader-show", () => {
      if (!element.hasClass("salaxy-loading")) {
        element.addClass("salaxy-loading");
      }
    });
    scope.$on("salaxy-loader-hide", () => {
      if (element.hasClass("salaxy-loading")) {
        element.removeClass("salaxy-loading");
      }
    });
  }
}
