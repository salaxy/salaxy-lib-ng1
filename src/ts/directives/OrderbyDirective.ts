import * as angular from "angular";

/**
 * Adds the OData $orderby functionality to table column header.
 * Primary design is for thead > tr > th element or for span / div within that tag.
 * Requires salaxy-odata-table component as parent.
 * @example
 * ```html
 * <thead>
 *   <th>
 *     <span sxy-orderby="title">Title</span>
 *     <br /> other text
 *   </th>
 *   <th sxy-orderby="section" class="right"><small>Section</small></th>
 * ```
 */
export class OrderbyDirective implements angular.IDirective {

    /**
     * Factory for directive registration.
     * @ignore
     */
    public static sxyOrderby() {
      const factory = () => new OrderbyDirective();
      factory.$inject = [];
      return factory;
    }

    /**
     * Applies to attributes only.
     * @ignore
     */
    public restrict = "A";

    /** The view replaces the original element */
    public replace = true;

    /** Contents of the tag is transcluded: HTML can be used as input. */
    public transclude = true;

    /** Set isolate scope */
    public scope = true;

    /** Attribute must be used within salaxy-odata-table component to access the common ODataQueryController  */
    public require = {
      $odata: "^^salaxyOdataTable",
    };

   /**
    * Creates a new instance of the directive.
    */
    constructor() {
        // initialization
     }

     /**
      * Template is based on the original tag.
      */
    public template(element, attrs: any ) { // eslint-disable-line @typescript-eslint/no-unused-vars
      const tag = element[0].nodeName;
      return `<${tag} ng-click="$odata.orderBy(orderby)" class="clickable">
            <ng-transclude></ng-transclude><i ng-if="$odata.getColumnOrder(orderby)" class="fa salaxy-odata-table-sort fa-sort-{{ $odata.getColumnOrder(orderby) }}" aria-hidden="true"></i></${tag}>`;
    }

     /**
      * Links the directive: Access to scope, element and attrs.
      * @ignore
      */
    public link = (scope: any, element: any, attrs: any, controllers: any) => {
        scope.$odata = controllers.$odata;
        scope.orderby = attrs.sxyOrderby;
    }
}
