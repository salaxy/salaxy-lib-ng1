import * as angular from "angular";
import { Ng1Translations } from "../services/ui";

/**
 * Directive for binding text for the element.
 * The attribute sxy-text should contain the translation key for the text content.
 * This directive should be used to replace AngularJS expression and translation pipe filter.
 * Small additions to the beginning and end can be added with pre and ps attributes.
 * Interpolation parameters for translation can be given using params -attribute.
 * @example
 * ```html
 * <div sxy-text="SALAXY.NG1.UserInfoComponent.authType" ps=":" pre="HUOM: "></div>
 * ```
 */
export class TextDirective implements angular.IDirective {

  /**
   * Factory method for the directive creation.
   * @ignore
   */
  public static sxyText(): any {
    const factory = (translate: Ng1Translations) => new TextDirective(translate);
    factory.$inject = ["Ng1Translations"];
    return factory;
  }

  /**
   * Directive restrictions.
   * @ignore
   */
  public restrict = "A";

  /**
   * Creates a new instance of the directive.
   */
  constructor(private translate: Ng1Translations) {
  }

  /**
   * Compile function for the directive.
   * @ignore
   */
  public compile(cElement: any, cAttr: any): any { // eslint-disable-line @typescript-eslint/no-unused-vars
    return (scope: any, element: any, attr: any) => {
      // Translation which preserver whitespace
      const wsTranslate = (key: string, interpolateParams?: any) => {
        const text = this.translate.get(key, interpolateParams);
        if (key.trim() ===  text) {
          return key;
        }
        return text;
      };

      // Returns the untrimmed attribute value (as opposite to attr)
      const getUntrimmedAttributeValue = (name: string) => {
        return (element[0].attributes[name] || {value: ""} ).value;
      };

      const params = () => scope.$eval(attr.params);

      scope.$watch(() => getUntrimmedAttributeValue("pre") + getUntrimmedAttributeValue("sxy-text") + JSON.stringify(params()) + getUntrimmedAttributeValue("ps"), () => {
        element.text(wsTranslate(getUntrimmedAttributeValue("pre")) + wsTranslate( getUntrimmedAttributeValue("sxy-text"), params()) + wsTranslate(getUntrimmedAttributeValue("ps")));
      });
    };
  }
}
