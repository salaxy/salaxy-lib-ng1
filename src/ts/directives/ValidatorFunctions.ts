import { Arrays, Validation, Numeric } from "@salaxy/core";

/* eslint-disable @typescript-eslint/unbound-method */

/**
 * Validator directives for different form inputs.
 * See mthods for examples of use.
 */
export class ValidatorFunctions {

    /**
     * Tax percent validator.
     * @example
     * ```html
     * <input name="taxPercent" type="text" ng-model="taxPercent" sxy-tax-percent>
     * ```
     */
    public static sxyTaxPercent() {
        return () => ValidatorFunctions.directive("sxyTaxPercent", Validation.isTaxPercent);
    }

    /**
     * Currency value validator.
     * @example
     * ```html
     * <input name="currency" type="text" ng-model="currency" sxy-currency>
     * ```
     */
    public static sxyCurrency() {
        return () => ValidatorFunctions.directive("sxyCurrency", Validation.isCurrency);
    }

    /**
     * Iban validator.
     * @example
     * ```html
     * <input name="iban" type="text" ng-model="iban" sxy-iban>
     * ```
     */
    public static sxyIban() {
        return () => ValidatorFunctions.directive("sxyIban", Validation.isIban);
    }

    /**
     * Email validator.
     * @example
     * ```html
     * <input name="email" type="text" ng-model="email" sxy-email>
     * ```
     */
    public static sxyEmail() {
        return () => ValidatorFunctions.directive("sxyEmail", Validation.isEmail);
    }

    /**
     * Mobile phone validator.
     * @example
     * ```html
     * <input name="mobilePhone" type="text" ng-model="mobilePhone" sxy-mobile-phone>
     * ```
     */
    public static sxyMobilePhone() {
        return () => ValidatorFunctions.directive("sxyMobilePhone", Validation.isMobilePhone);
    }

    /**
     * Finnish personal id validator.
     * @example
     * ```html
     * <input name="personalIdFi" type="text" ng-model="personalIdFi" sxy-personal-id-fi>
     * ```
     */
    public static sxyPersonalIdFi() {
        return () => ValidatorFunctions.directive("sxyPersonalIdFi", Validation.isPersonalIdFi);
    }

    /**
     * Finnish company id validator.
     * @example
     * ```html
     * <input name="companyIdFi" type="text" ng-model="companyIdFi" sxy-company-id-fi>
     * ```
     */
    public static sxyCompanyIdFi() {
        return () => ValidatorFunctions.directive("sxyCompanyIdFi", Validation.formatCompanyIdFi);
    }

    /**
     * Finnish official id validator. Official ID may be either company id orr personal id.
     * @example
     * ```html
     * <input name="officialIdFi" type="text" ng-model="officialIdFi" sxy-official-id-fi>
     * ```
     */
     public static sxyOfficialIdFi() {
      return () => ValidatorFunctions.directive("sxyOfficialIdFi", (input) => {
        return Validation.formatCompanyIdFi(input, true) || Validation.isPersonalIdFi(input);
      });
    }

    /**
     * Finnish postal code validator.
     * @example
     * ```html
     * <input name="postalCodeFi" type="text" ng-model="postalCodeFi" sxy-postal-code-fi>
     * ```
     */
    public static sxyPostalCodeFi() {
        return () => ValidatorFunctions.directive("sxyPostalCodeFi", Validation.isPostalCodeFi);
    }

    /**
     * Sms verification code validator.
     * @example
     * ```html
     * <input name="smsVerificationCode" type="text" ng-model="smsVerificationCode" sxy-sms-verification-code>
     * ```
     */
    public static sxySmsVerificationCode() {
        return () => ValidatorFunctions.directive("sxySmsVerificationCode", Validation.isSmsVerificationCode);
    }

    /**
     * Number validator.
     * @example
     * ```html
     * <input name="number" type="text" ng-model="number" sxy-number>
     * ```
     */
    public static sxyNumber() {
        return () => ValidatorFunctions.directive("sxyNumber", Validation.isNumber);
    }

    /**
     * Pension contract number validator.
     * @example
     * ```html
     * <input name="pensionContractNumber" type="text" ng-model="pensionContractNumber" sxy-pension-contract-number>
     * ```
     */
     public static sxyPensionContractNumber() {
        return () => ValidatorFunctions.parserAndFormatterDirective(["sxyPensionContractNumber", "sxyTemporaryPensionContractNumber"], [(val) => !!Validation.isPensionContractNumber(val), (val) => !Validation.isTemporaryPensionContractNumber(val)]);
     }

    /**
     * Integer validator.
     * @example
     * ```html
     * <input name="intger" type="text" ng-model="integer" sxy-integer>
     * ```
     */
    public static sxyInteger() {
        return () => ValidatorFunctions.directive("sxyInteger", Validation.isInteger);
    }

    /**
     * Maximum value validator
     * <input name="number" type="text" ng-model="number" sxy-min="$ctrl.maximum">
     */
    public static sxyMin() {
      return () => ValidatorFunctions.directive("sxyMin", (value: string, attrValue: any) => {
        if(!Validation.isNumber(value) || !Validation.isNumber(attrValue)){
          return true;
        }
        return Numeric.parseNumber(value) >= Numeric.parseNumber(attrValue);
      } );
    }

    /**
     * Maximum value validator
     * <input name="number" type="text" ng-model="number" sxy-max="$ctrl.maximum">
     */
    public static sxyMax() {
      return () => ValidatorFunctions.directive("sxyMax", (value: string, attrValue: any) => {
        if(!Validation.isNumber(value) || !Validation.isNumber(attrValue) ){
          return true;
        }
        return Numeric.parseNumber(value) <= Numeric.parseNumber(attrValue);
      } );
    }

    /**
     * Multiple of value validator
     * <input name="number" type="text" ng-model="number" sxy-multiple-of="$ctrl.multipleOf">
     */
    public static sxyMultipleOf() {
      return () => ValidatorFunctions.directive("sxyMultipleOf", (value: string, attrValue: any) => {
        if(!Validation.isNumber(value) || !Validation.isNumber(attrValue)){
          return true;
        }
        if(Numeric.parseNumber(attrValue) <= 0){
          return false;
        }
        return Numeric.parseNumber(value) % Numeric.parseNumber(attrValue) === 0;
      } );
    }

    /**
     * Validator for values which should not be considered as valid in the UI (i.e. unknown, none)
     */
    public static sxyExcludeUnknowns() {
      return () => ValidatorFunctions.directive("sxyExcludeUnknowns", (value: string, attrValue: any) => {
        const invalidValues = Arrays.assureArray(attrValue);
        return !(invalidValues.indexOf(value) >= 0);
      } );
    }


    private static directive(name: string, validationFunction: (input: string, attrValue: any) => boolean): any {
        return {
            restrict: "A",
            require: "ngModel",
            link: ValidatorFunctions.setValidator(name, validationFunction),
        };
    }

    private static parserAndFormatterDirective(names: string[], validationFunctions: ((input: string) => boolean)[]): any {
        return {
            restrict: "A",
            require: "ngModel",
            link: ValidatorFunctions.setParserAndFormatter(names, validationFunctions),
        };
    }
    private static setValidator(name: string, validationFunction: (input: string, attrValue: any) => boolean): any {
        return (scope: any, element: any, attrs: any, ctrl: any) => {
            ctrl.$validators[name] = (modelValue, viewValue) => {
                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty models to be valid
                    return true;
                }
                return validationFunction(viewValue,  scope.$eval(attrs[name]));
            };
        };
    }

    private static setParserAndFormatter(names: string[], validationFunctions: ((input: string) => boolean)[]): any {
        return (scope: any, element: any, attrs: any, ctrl: any) => {

                ctrl.$parsers.unshift( (value) => {
                    let allValid = true;
                    for ( let i = 0; i < names.length; i++) {
                    // test and set the validity after update.
                        const valid = validationFunctions[i](value);
                        ctrl.$setValidity(names[i], valid);
                        if (!valid) {
                            allValid = false;
                        }
                    }

                    // if it's valid, return the value to the model,
                    // otherwise return undefined.
                    return allValid ? value : undefined;
                });
            };
        }
    }
