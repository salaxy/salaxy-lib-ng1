import {
  AppendNodeDirective,
  DatepickerInputValidationDirective,
  EnumParserFunctions,
  HtmlDirective,
  IfRoleDirective,
  LoaderDirective,
  OrderbyDirective,
  TextDirective,
  ValidatorFunctions,
} from ".";

/** Provides methods for registering the Directives to module
 * (and other related metadata in the future).
 */
export class DirectivesRegistration {

  /** Gets the directives for Module registration. */
  public static getDirectives() {
    return {
      // TODO: Remove depency to directives starting with "salaxy-" => "sxy-".
      salaxyIfRole: IfRoleDirective.sxyIfRole(),
      // TODO: Go through: are these needed? If so => "sxy-"
      salaxyAppendNode: AppendNodeDirective.salaxyAppendNode(), // Useful? Used in tabs, are tabs really used?
      salaxyDatepickerInputValidation: DatepickerInputValidationDirective.salaxyDatepickerInputValidation(), // Could this be in ValidatorFunctions or is there special functionality? This is used only in date picker.
      // TODO: Move as <salaxy-loader> component instead of directive (removes unnecessary HTML from the views).
      //       Also evaluate relation to salaxy-spinner: There should be only one if possible with different layout options.
      salaxyLoader: LoaderDirective.salaxyLoader(),

      // All new directives should start with "sxy-"
      sxyIfRole: IfRoleDirective.sxyIfRole(),
      sxyOrderby: OrderbyDirective.sxyOrderby(),
      // Validator functions
      sxyTaxPercent: ValidatorFunctions.sxyTaxPercent(),
      sxyCurrency: ValidatorFunctions.sxyCurrency(),
      sxyIban: ValidatorFunctions.sxyIban(),
      sxyEmail: ValidatorFunctions.sxyEmail(),
      sxyEnumAsBoolean: EnumParserFunctions.sxyEnumAsBoolean(),
      sxyMobilePhone: ValidatorFunctions.sxyMobilePhone(),
      sxyExcludeUnknowns: ValidatorFunctions.sxyExcludeUnknowns(),
      sxyPersonalIdFi: ValidatorFunctions.sxyPersonalIdFi(),
      sxyCompanyIdFi: ValidatorFunctions.sxyCompanyIdFi(),
      sxyOfficialIdFi: ValidatorFunctions.sxyOfficialIdFi(),
      sxyPostalCodeFi: ValidatorFunctions.sxyPostalCodeFi(),
      sxyNumber: ValidatorFunctions.sxyNumber(),
      sxyInteger: ValidatorFunctions.sxyInteger(),
      sxyMax: ValidatorFunctions.sxyMax(),
      sxyMin: ValidatorFunctions.sxyMin(),
      sxyMultipleOf: ValidatorFunctions.sxyMultipleOf(),
      sxySmsVerificationCode: ValidatorFunctions.sxySmsVerificationCode(),
      sxyPensionContractNumber: ValidatorFunctions.sxyPensionContractNumber(),
      sxyHtml: HtmlDirective.sxyHtml(),
      sxyText: TextDirective.sxyText(),
    };
  }
}
