
import marked from "marked";

import { ApiListItem, CalcInfo, CalcRowsLogic, DatelyObject, DateRange, Dates, EnumerationsLogic, Iban, Numeric, Occupations, Role, SystemRole, Unit } from "@salaxy/core";

import { Ng1Translations, SessionService } from "../services";

/**
 * Contains the simple filter functions where implementation
 * is in some other class (typically @salaxy/core) and thus the filter implementation is very small.
 * This should typically the case, but some filters may contain a lot of NG1 specific
 * implementation (e.g. SalaxyTranslateFilter) and/or dependency injection and thus deserve their own class.
 */
export class FilterFunctions {

  /**
   * Formats a Workflow message string from index.
   * List of allowed message types can be given as a comma seprated list.
   *
   * Parameters of the filter are:
   *
   * - messageString: Workflow message
   * - types: Allowed message types.
   */
  public static sxyWorkflowMessage = ["$sce", ($sce: any) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.
    return (messageString: string, types: string) => {
      if (!messageString) {
        return $sce.trustAsHtml("<span></span>");
      }
      const messageTypes = (types || "").split(",");
      const uiClassMarker = messageString.indexOf("(Ui");
      const uiClassEndMarker = messageString.indexOf("):", uiClassMarker + 3);
      const type = (messageString.substring(0, uiClassMarker - 1) || "UnknownEvent");

      if (messageTypes.length > 0) {
        if (!messageTypes.some((x) => x.trim() === type)) {
          return $sce.trustAsHtml("<span></span>");
        }
      }

      const uiClass = messageString.substring(uiClassMarker + 3, uiClassEndMarker).toLowerCase();
      let fullMessage = messageString.substring(uiClassEndMarker + 3);
      const dtReg = /([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):([0-5][0-9])/g;
      fullMessage = fullMessage.replace(dtReg, (match: string, year, month, day, hours, minutes) => {
          const d = Date.UTC(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(day, 10), parseInt(hours, 10), parseInt(minutes, 10));
          return Dates.getMoment(d).format("D.M.YYYY HH:mm");
      });
      const shortMessage = fullMessage.substring(0, fullMessage.indexOf("("));
      return $sce.trustAsHtml(`<span class="badge bg-${uiClass}" title="${fullMessage}">${shortMessage}</span>`);
    };
  }];

  /**
   * Translate filter with key and interpolation parameter.
   * Converts content with translationId ending with '.html', to safe html.
   * Converts content with translationId ending with '.md', first to html and then to safe html.
   * If the text contains a variable placeholder like: 'Hello {{name}}!', it will be replaced by given parameter.
   *
   * Parameters of the filter are:
   *
   * - translationId: Translation key
   * - interpolateParams: Interpolation parameters applied if text had interpolation.
   *
   * @example
   * ```html
   * <p sxy-text="SALAXY.NG1.test1"></p>
   * <p>{{ 'SALAXY.NG1.hello' | sxyTranslate: { name: 'John'} }}</p>
   * ```
   */
  public static sxyTranslate = ["Ng1Translations", "$sce", (translate: Ng1Translations, $sce: any) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.
    return (translationId: string, interpolateParams: any): string => {
      const endsWith = (str1: string, str2: string) => {
        return str1.substring(str1.length - str2.length, str1.length) === str2;
      };
      const str = translate.get(translationId, interpolateParams) as string;
      if (translationId) {
        // TODO: When adding English & Swedish: trustAsHtml => getTrustedHtml
        if (endsWith(translationId, ".html")) {
          return $sce.trustAsHtml(str);
        } else if (endsWith(translationId, ".md")) {
          return $sce.trustAsHtml(marked(str));
        }
      }
      return str;
    };
  }];

  /**
   * Formats a string using markdown and returns trusted HTML.
   *
   * Parameters of the filter are:
   *
   * - markdown: Markdown input to format as HTML.
   *
   * @example
   * ```html
   * <div ng-bind-html="$ctrl.current.example | sxyMarkdown"></div>
   * ```
   */
  public static sxyMarkdown = ["$sce", ($sce: any) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.
    return (markdown: string): string => {
      if (markdown == null) {
        return null;
      }
      return $sce.getTrustedHtml(marked(markdown));
    };
  }];

  /**
   * IsInRole filter returns true if the user is in any of the given roles.
   * The input may be Array of roles or comma separated string containing role names.
   * This can be used instead of sxy-in-role directive when the component requires transcusion
   * and also in constructing other logic in expressions.
   *
   * Parameters of the filter are:
   *
   * - commaSeparatedRolesList: Comma separated list of roles
   *
   * NOTE: Because the actual value is constant, the filter is not rerun after session check).
   * Because of this, the method should only be used after the session has been checked.
   * Typically, by making sure, the filters are inside `sxy-if-role="$ctrl.current"`, `sxy-if-role="auth"` or `sxy-if-role="!unknown"`.
   *
   * @example
   * ```html
   * <div sxy-if-role="auth">
   *   <uib-tab index="'holidays'" heading="Lomat" ng-if="'company' | sxyIsInRole">
   *   <p>{{ 'company' | sxyIsInRole ? 'Company text' : 'Default text' }}</p>
   * </div>
   * ```
   */
  public static sxyIsInRole = ["SessionService", (sessionService: SessionService) => {
    return (commaSeparatedRolesList: (SystemRole | Role | string)[] | string): boolean => {
      return sessionService.isInSomeRole(commaSeparatedRolesList);
    };
  }];

  /**
   * Returns the label for an enumeration.
   *
   * Parameters of the filter are:
   *
   * - input: Enumeration in format "[EnumType].[EnumName]". First letter is case-insensitive so you can use lowerCamelCase or UpperCamelCase.
   * - defaultValue: Optional default value when language version is not available.
   * Special values "name" and "#name", may be used to return the enum member name either with hash or without it.
   * Default is "#name", which returns "#[MemberName]" if the translation is not found.
   *
   * @example
   * ```html
   * <p>{{ 'CalculationStatus.Draft' | sxyEnum }} - The enum full name</p>
   * <p>{{ 'CalculationStatus.paymentSucceeded' | sxyEnum }} - you may also use JSON value where first letter is lower-case.</p>
   * <p>{{ 'CalculationStatus.' + calc.status | sxyEnum }} - ...which makes it easy to use with data.</p>
   * ```
   */
  public static sxyEnum() {
    return (input: string, defaultValue: null | string | "name" | "#name" = "#name") => {
      const splitValues = (input || "").split(".");
      if (splitValues.length !== 2) {
        return "#ERR: sxyEnum";
      }
      if (!splitValues[1]) {
        return null;
      }
      return EnumerationsLogic.getEnumLabel(splitValues[0], splitValues[1], defaultValue);
    };
  }

  /**
   * Returns the description for an enumeration value in current language.
   *
   * Parameters of the filter are:
   *
   * - input: Enumeration in format "[EnumType].[EnumName]". First letter is case-insensitive so you can use lowerCamelCase or UpperCamelCase.
   * - defaultValue: Optional default value when language version is not available. Default is null
   * Special values "name" and "#name", may be used to return the enum member name either with hash or without it.
   *
   * @example
   * ```html
   * <p>{{ 'CalculationStatus.Draft' | sxyEnumDescr }} - The enum full name</p>
   * <p>{{ 'CalculationStatus.paymentSucceeded' | sxyEnumDescr }} - you may also use JSON value where first letter is lower-case.</p>
   * <p>{{ 'CalculationStatus.' + calc.status | sxyEnumDescr }} - ...which makes it easy to use with data.</p>
   * ```
   */
  public static sxyEnumDescr() {
    return (input: string, defaultValue?: null | string | "name" | "#name") => {
      const splitValues = (input || "").split(".");
      if (splitValues.length !== 2) {
        return "#ERR: sxyEnumDescr";
      }
      if (!splitValues[1]) {
        return null;
      }
      return EnumerationsLogic.getEnumDescr(splitValues[0], splitValues[1], defaultValue);
    };
  }

  /**
   * Formats the date with Dates.getFormattedDate in core project.
   * If the input is not recognized as date or if it is empty or less than year 1900, shows a dash.
   *
   * Parameters of the filter are:
   *
   * - input: Input to format
   *
   * @example
   * ```html
   * <p>{{ calc.workflow.paidAt | sxyDate }}</p>
   * ```
   */
  public static sxyDate() {
    return (input: string) => {
      const year = Dates.getYear(input);
      if (!year || year < 1900) {
        return "-";
      }
      return Dates.getFormattedDate(input);
    };
  }

  /**
   * Formats a DateRange with Dates.getFormattedRange() in core project.
   * If input is null, shows a dash.
   * Also supports the ApiListItem (startAt, endAt) and CalcInfo type in Calculation.info property (workStartDate and workEndDate).
   *
   * Parameters of the filter are:
   *
   * - input: Input to format
   *
   */
  public static sxyDateRange() {
    return (input: DateRange) => {
      if (!input) {
        return "-";
      }
      const info = input as CalcInfo;
      if (info.workStartDate || info.workEndDate) {
        return Dates.getFormattedRange(info.workStartDate, info.workEndDate);
      }
      const listItem = input as ApiListItem;
      if (listItem.startAt || listItem.endAt) {
        return Dates.getFormattedRange(listItem.startAt, listItem.endAt);
      }
      return Dates.getFormattedRange(input.start, input.end);
    };
  }

  /**
   * Iban formatting filter.
   *
   * Parameters of the filter are:
   *
   * - input: Input text to format as IBAN.
   *
   */
  public static sxyIban() {
    return (input: string) => {
      if (!input) {
        return null;
      }
      return Iban.formatIban(input);
    };
  }

  /**
   * Formats a number as count optionally taking into account the related unit
   *
   * Parameters of the filter are:
   *
   * - input: Input to format
   * - unit: Unit that is used in formatting.
   * - decimals: Number of decimals to show.
   * - nullText: Text to show if the value is null / undefined. By default returns null, which can then be cought by another filter or logical or ("||").
   * - zeroText: If specified, will use this text without the Unit formatting if the value is zero. By default zero is formatted as "0 [unitText]".
   */
  public static sxyCount() {
    return (
      input: number,
      unit: Unit = null,
      decimals: number = null,
      nullText: string = null,
      zeroText: string = null) => {
      if (input == null) {
        return nullText;
      }
      if (zeroText != null && input === 0) {
        return zeroText;
      }
      const rounded = Numeric.round(input, decimals || 2);
      switch (unit) {
        case Unit.One:
          return "";
        case Unit.Period:
          return rounded === 1 ? "1 jakso" : rounded + " jaksoa";
        case Unit.Percent:
          return Numeric.round(input * 100, decimals || 2) + "%";
        case Unit.Hours:
        case Unit.Days:
        case Unit.Weeks:
        case Unit.Count:
        case Unit.Kilometers:
        case Unit.Undefined:
        default:
          return rounded + " " + (CalcRowsLogic.getUnitIndicator(unit) || "");
      }
    };
  }

  /**
   * Formats a datetime ISO string (or other moment compatible time)
   * As time with texts "Today", "Yesterday", "D.M. HH:mm" for this year
   * and "D.M.YYYY" for other years (if necessary, add option for adding time to previous years).
   *
   * Parameters of the filter are:
   *
   * - time: Input to format
   *
   */
  public static sxyTime() {
    // TODO: This is being rewritten in CalendarEventsCrudController. When ready, move to core and use the same function here.
    return (time: string) => {
      const moment = Dates.getMoment(time);
      if (!moment) {
        return "-";
      }
      if (Dates.getTodayMoment().isSame(moment, "d")) {
        return "Tänään " + moment.format("HH:mm");
      }
      if (Dates.getTodayMoment().add(-1, "d").isSame(moment, "d")) {
        return "Eilen " + moment.format("HH:mm");
      }
      if (Dates.getTodayMoment().isSame(moment, "y")) {
        return moment.format("D.M. HH:mm");
      }
      return moment.format("D.M.YYYY");
    };
  }

  /**
   * Calls the Salaxy standard Dates.asDate to convert a DatelyObject to ISO date string (without time).
   * ISO dates can then be used in comparison and equality.
   * Also note that, DatelyObject can be constants like "today" | "yesterday" | "tomorrow". See documentation for details.
   *
   * Parameters of the filter are:
   *
   * - input: Input to format as DatelyObject
   * - addWorkDays: If set, adds the number of workdays to the input.
   * May also be zero (0) to return input if it is a workday or next workday if input is not a workday.
   *
   */
  public static sxyAsDate() {
    return (input: DatelyObject, addWorkDays: number) => {
      if (addWorkDays != null) {
        return Dates.addWorkdays(input, addWorkDays);
      }
      return Dates.asDate(input);
    };
  }

  /**
   * Converts the occupation to a language versioned string.
   *
   * Parameters of the filter are:
   *
   * - input: The occupation code as stored / returned from the API.
   * - format: Default is just the label for the occupation. "code-label" is in format "[code]: [label]`
   *
   */
  public static sxyOccupation() {
    return (input: string, format: "default" | "code-label") => {
      const occupation = Occupations.getById(input);
      if (!occupation) {
        // TODO: Language versioning
        return "Ei työn tyyppiä!";
      }
      if (format === "code-label") {
        return `${occupation.code}: ${occupation.label}`;
      }
      return occupation.label;
    };
  }

}
