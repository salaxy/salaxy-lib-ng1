import { FilterFunctions } from "./FilterFunctions";

/**
 * Provides the method for registering filters to the module.
 */
export class FiltersRegistration {

  /** Gets the filters for Module registration. */
  public static getFilters() {
    /* eslint-disable @typescript-eslint/unbound-method */
    return {
      sxyAsDate: FilterFunctions.sxyAsDate,
      sxyCount: FilterFunctions.sxyCount,
      sxyDate: FilterFunctions.sxyDate,
      sxyDateRange: FilterFunctions.sxyDateRange,
      sxyEnum: FilterFunctions.sxyEnum,
      sxyEnumDescr: FilterFunctions.sxyEnumDescr,
      sxyIban: FilterFunctions.sxyIban,
      sxyMarkdown: FilterFunctions.sxyMarkdown,
      sxyOccupation: FilterFunctions.sxyOccupation,
      sxyTime: FilterFunctions.sxyTime,
      sxyTranslate: FilterFunctions.sxyTranslate,
      sxyIsInRole: FilterFunctions.sxyIsInRole,
      sxyWorkflowMessage: FilterFunctions.sxyWorkflowMessage,
    };
    /* eslint-enable @typescript-eslint/unbound-method */
  }
}
