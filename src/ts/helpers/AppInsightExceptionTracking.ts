import * as angular from "angular";

import { Configs } from "@salaxy/core";

/** Exception tracking with AppInsight */
export class AppInsightExceptionTracking {

  /**
   * For NG-dependency injection
   * @ignore
   */
  protected static $inject = ["$provide"];

  constructor($provide: angular.auto.IProvideService) {
    $provide.decorator("$exceptionHandler", ["$delegate", ($delegate: angular.IExceptionHandlerService) => {
      return (exception: Error, cause?: string) => {
        $delegate(exception, cause);
        const appInsights = Configs.global.appInsights;
        const config = Configs.current;
        if (appInsights && config && config.instrumentationKey) {
          try {
            appInsights.trackException({ exception });
            appInsights.flush();
          } catch (err) {
            console.debug("Application insight", err);
          }
        }
      };
    }]);
  }
}
