import { CalendarVEvent, CalendarOccurence, CalendarEventData, DateTimes, Dates, DateRange } from "@salaxy/core";

/** Helper component for calendar related user interfaces: Dates and Times formatting etc. */
export class CalendarHelper {

  /** Periods / date ranges plotted on the chart. */
  public calendarPeriods: {
    /** Period to plot. */
    range: DateRange,
    /** Title for the period. */
    title: string,
    /** Description for the period. */
    description: string,
  }[];

  constructor(private $sce: angular.ISCEService) {}

  /**
   * Gets a HTML formatted version of the description.
   * @param description Description text to format
   */
   public formatDescription(description): any
   {
     if (!description) {
       return null;
     }
     const html = description.replace(/<(http[^>]*)>/g, (match: string, p1: string) => {
       if (p1.endsWith(".jpg") || p1.endsWith(".jpeg") || p1.endsWith(".png") || p1.endsWith(".gif"))
       {
         return `<a href="${p1}" title="${p1}" target="_blank"><img src="${p1}" alt="External image" style="max-width: 400px"></a>`
       }
       return `<a href="${p1}" title="${p1}" target="_blank">LINK</a>`
     });
     return this.$sce.getTrustedHtml(html);
   }

   /**
    * Formats a time component for user interface taking into account isAllDay property.
    * @param event A component that has properties: start, end, duration and isAllDay.
    * @param key Name of the property to format.
    */
    public format(event: CalendarVEvent | CalendarOccurence | CalendarEventData, key: "start" | "end" | "duration" | "triggerDuration") {
     if (!event || !key || !event[key]) {
       return "-";
     }
     if (key.toLowerCase().indexOf("duration") >= 0) {
       return DateTimes.formatDuration(event[key]);
     }
     if (event.isAllDay) {
       return Dates.getFormattedDate(event[key]);
     } else {
       return DateTimes.format(event[key]);
     }
   }

}