import * as angular from "angular";

/**
 * Polyfill which replaces ES6 promises with $q.
 * This is required, for example if Promise.resolve -methods are used.
 * They are not correctly digested using ES6 promises only.
 */
export class PromisePolyfill {

  /**
   * For NG-dependency injection
   * @ignore
   */

   /** Dependencies */
  public static $inject = ["$q", "$window"];
  constructor($q: angular.IQService, $window: angular.IWindowService) {
    $window.Promise = function (resolver) {
      return $q(resolver);
    };

    $window.Promise.all = $q.all.bind($q);
    $window.Promise.reject = $q.reject.bind($q);
    $window.Promise.resolve = $q.resolve.bind($q);
    $window.Promise.race = $q.race.bind($q);
  }
}
