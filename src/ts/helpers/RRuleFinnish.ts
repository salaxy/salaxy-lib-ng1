export const RRuleFinnish: {
  /** Names of days */
  dayNames: string[]
  /** Names of months */
  monthNames: string[]
  /** Tokens for natural language parsing (NOT IN USE!) */
  tokens: {
    [k: string]: RegExp
  }
  /** The key texts that are used by the gettext method. */
  texts: {
    [k: string]: string
  }
} = {
  dayNames: [
    "Sunnuntai", "Maanantai", "Tiisatai", "Keskiviikko",
    "Torstai", "Perjantai", "Lauantai"
  ],
  monthNames: [
    "Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu",
    "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu",
    "Marraskuu", "Joulukuu"
  ],
  tokens: {},
  texts: {
    "(~ approximate)": "(~ noin)",
    "and": "ja",
    "at": "klo",
    "day": "päivä",
    "days": "päivä",
    "every": "joka",
    "for": ",",
    "hour": "tunti",
    "hours": "tunti",
    "in": ", vain kuukausina:", // TODO: Is this used in other cases
    "last": "viimeisenä päivänä",
    "minutes": "min",
    "month": "kuukausi",
    "months": "kuukausi",
    "nd": ".",
    "on the": "aina",
    "on": "on", // TODO
    "or": "tai",
    "rd": ".",
    "st": ".",
    "th": ".",
    "the": "",
    "time": "kerta",
    "times": "kertaa",
    "until": "päättyen",
    "week": "viikko",
    "weekday": "viikonpäivä",
    "weekdays": "viikonpäivä",
    "weeks": "viikko",
    "year": "vuosi",
    "years": "vuosi",
    "RRule error: Unable to fully convert this rrule to text": "Virhe: En ymmärrä tekstiä"
  }
}
