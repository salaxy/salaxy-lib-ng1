import * as angular from "angular";

/** Helps creating Salaxy standard routing. */
export class RouteHelperProvider implements angular.IServiceProvider {
  /**
   * For NG-dependency injection
   * @ignore
   */
  protected static $inject = ["$routeProvider"];

  /** The root folder that contains the custom views. Default is "./views" */
  public customSectionRoot = "./views";

  /**
   * TODO: Consider implementing the IServiceProvider interface
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public $get = ["$rootScope" , ($rootScope: any) => {
    return "todo";
  }];

  public constructor(private $routeProvider: angular.route.IRouteProvider) {
  }

  /**
   * Adds a routing section where views are coming by default from custom folder.
   * @param section Navigation section in routing.
   * Also used as default for views folder.
   * @param defaultViews Exceptions to the rule: Individual views / pages that should be mapped
   * to default views stored in ng1 library under "salaxy-components/pages/".
   * @param defaultViewsFolder The name of the default views folder if not the same as section.
   * If not specified, the folder is "salaxy-components/pages/[section]".
   * If the paths starts with "salaxy-components", "/" or ".", the path is assumed being a full path.
   * Otherwise the "salaxy-components/pages/" is added before defaultViewsFolder. So typically this can be just a folder / section name (e.g. "calc").
   * @param customViewsFolder If specified, this is the path to the custom folder where the views are.
   * If NOT specified, "[customSectionRoot]/[section]" is used.
   * If the paths starts with "/" or ".", the path is assumed being a full path.
   * Otherwise the customSectionRoot (default "./views/") is added before customViewsFolder. So typically this can be just a folder / section name (e.g. "calc").
   * @example
   * RouteHelperProvider.customSection("calc", ["details"]).
   * // Adds a route as "/calc/:viewName?/:crudItemId*?" where all views are located in "./views/calc/[:viewName].html",
   * // except "/calc/details/:crudItemId*?", which is mapped to "salaxy-components/pages/calc/details.html"
   */
  public customSection(section: string, defaultViews?: string[], defaultViewsFolder?: string, customViewsFolder?: string): RouteHelperProvider {
    defaultViewsFolder = defaultViewsFolder || section;
    if (!defaultViewsFolder.startsWith(".") && !defaultViewsFolder.startsWith("/") && !defaultViewsFolder.startsWith("salaxy-components/")) {
      defaultViewsFolder = "salaxy-components/pages/" + defaultViewsFolder;
    }
    customViewsFolder = customViewsFolder || section;
    if (!customViewsFolder.startsWith(".") && !customViewsFolder.startsWith("/") && !customViewsFolder.startsWith("salaxy-components/")) {
      customViewsFolder = this.customSectionRoot + "/" + customViewsFolder;
    }
    if (defaultViews && Array.isArray(defaultViews)) {
      defaultViews.forEach((view) => {
        this.when(`/${section}/${view}/:crudItemId*?`, { templateUrl: `${defaultViewsFolder}/${view}.html` }, section);
      });
    }
    this.when(`/${section}/:viewName?/:crudItemId*?`, {
      templateUrl: (path) => `${customViewsFolder}/${path.viewName || "index"}.html`,
    }, section);
    return this;
  }

  /**
   * Adds a routing section where views are coming by default from the product ng1 library compiled to "salaxy-components/pages/".
   * @param section Section for the views, e.g. "calc" for "salaxy-components/pages/calc"
   * @param customViews Exceptions to the rule:
   * Individual views that should be mapped to custom folder under customSectionRoot.
   * @param customViewsFolder The name of the custom views folder if not the same as section.
   * If the paths starts with "/" or ".", the path is assumed being a full path.
   * Otherwise the customSectionRoot (default "./views/") is added before customViewsFolder. So typically this can be just a folder / section name (e.g. "calc").
   * @param defaultViewsFolder The name of the default views folder if not the same as section.
   * If not specified, the folder is "salaxy-components/pages/[section]".
   * If the paths starts with "salaxy-components", "/" or ".", the path is assumed being a full path.
   * Otherwise the "salaxy-components/pages/" is added before defaultViewsFolder. So typically this can be just a folder / section name (e.g. "calc").
   * @example
   * RouteHelperProvider.defaultSection("calc", ["details"], "ng1/calc").
   * // Adds a route as "/calc/:viewName?/:crudItemId*?" where all views are located in "salaxy-components/pages/calc/" folder
   * // except "/calc/details/:crudItemId*?", which is mapped to "./views/ng1/calc/details.html".
   */
  public defaultSection(section: string, customViews?: string[], customViewsFolder?: string, defaultViewsFolder?: string): RouteHelperProvider {
    customViewsFolder = customViewsFolder || section;
    if (!customViewsFolder.startsWith(".") && !customViewsFolder.startsWith("/") && !customViewsFolder.startsWith("salaxy-components/")) {
      customViewsFolder = this.customSectionRoot + "/" + customViewsFolder;
    }
    defaultViewsFolder = defaultViewsFolder || section;
    if (!defaultViewsFolder.startsWith(".") && !defaultViewsFolder.startsWith("/") && !defaultViewsFolder.startsWith("salaxy-components/")) {
      defaultViewsFolder = "salaxy-components/pages/" + defaultViewsFolder;
    }
    if (customViews && Array.isArray(customViews)) {
      customViews.forEach((view) => {
        if (view === "index") {
          this.when(`/${section}/index/:crudItemId*?`, { templateUrl: `${customViewsFolder || section}/index.html` }, section);
          this.when(`/${section}`, { templateUrl: `${customViewsFolder || section}/index.html` }, section);
        } else {
          this.when(`/${section}/${view}/:crudItemId*?`, { templateUrl: `${customViewsFolder || section}/${view}.html` }, section);
        }
      });
    }
    this.when(`/${section}/:viewName?/:crudItemId*?`, {
      templateUrl: (path) => `${defaultViewsFolder}/${path.viewName || "index"}.html`,
    }, section);
    return this;
  }

  /**
   * Fluid syntax for setting customSectionRoot.
   * @param customSectionRoot Root folder for the custom views.
   * You can use null to set the customSection to default "./views", empty string to none.
   * No trailing dashes i.e. "./views", not "./views/".
   */
  public setCustomSectionRoot(customSectionRoot: string | null): RouteHelperProvider {
    if (customSectionRoot == null) {
      customSectionRoot = "./views";
    }
    this.customSectionRoot = customSectionRoot;
    return this;
  }

  /**
   * Adds the standard index/home pages: "/", "/index" and "/welcome".
   * May later contain special pages like standard error etc.
   * NOTE: If any of the sections already exists, will not add them.
   * @param customViewsFolder if set, uses the given custom folder. Otherwise, sets the paths from library.
   */
  public homeDefault(customViewsFolder?: string): RouteHelperProvider {
    const homePage = customViewsFolder ? `${this.customSectionRoot}/${customViewsFolder}/index.html` : "salaxy-components/pages/home/index.html";
    if (!this.sectionExists("")) {
      this.when("/", { templateUrl: homePage }, "");
    }
    if (!this.sectionExists("index")) {
      this.when("/index", { templateUrl: homePage }, "index");
    }
    if (!this.sectionExists("welcome")) {
      this.when("/welcome", { templateUrl: homePage }, "welcome");
    }
    return this;
  }

  /**
   * For backward compatibility only: Use commit() instead.
   * @deprecated For backward compatibility only: Use commit() instead.
   */
  public otherwiseDefault(): void {
    this.commit();
  }

  /**
   * Equivalent for calling $routeProvider.when(): Adds a new route definition to the $route service.
   * Call commit() method for these configurations to have an effect.
   *
   * @param path Route path (matched against $location.path). If $location.path contains redundant trailing slash or is missing one, the route will still match and the $location.path will be updated to add or drop the trailing slash to exactly match the route definition.
   *
   * - path can contain named groups starting with a colon: e.g. :name. All characters up to the next slash are matched and stored in $routeParams under the given name when the route matches.
   * - path can contain named groups starting with a colon and ending with a star: e.g.:name*. All characters are eagerly stored in $routeParams under the given name when the route matches.
   * - path can contain optional named groups with a question mark: e.g.:name?.
   *
   * For example, routes like /color/:color/largecode/:largecode*\/edit will match /color/brown/largecode/code/with/slashes/edit and extract: color: brown and largecode: code/with/slashes.
   *
   * @param route Mapping information to be assigned to $route.current on route match.
   * @param section identifies the section for later evaluating whether a certain section has been defined or not:
   * This is used in adding the deafults at the commit(): If section is found, it is not filled with defaults
   * E.g. for path "/calc/details" this would be "calc" and "/", it would be "".
   * Please note that in the "calc" case you would need to make sure all the other "calc section" routes are handled,
   * typically by calling defaultSection("calc") or customSection("calc") within the router config.
   */
  public when(path: string, route: angular.route.IRoute, section: string): RouteHelperProvider {
    this.routeModel.when.push({ section, path, route });
    return this;
  }

  /**
   * Equivalent for calling $routeProvider.otherwise():
   * Sets route definition that will be used on route change when no other route definition is matched.
   * Call commit() method for these configurations to have an effect.
   *
   * @param params information to be assigned to $route.current.
   */
  public otherwise(params: angular.route.IRoute | string): RouteHelperProvider {
    this.routeModel.otherwise = params;
    return this;
  }

  /**
   * Used in Development and PRO sites to add the Person (Worker/Household) related routes.
   * Typically, sites that target Person accounts are specialized and will not use this method.
   */
  public addPersonRoutes() : RouteHelperProvider {
    this.customSection("person-archive", [], "salaxy-components/pages-personal/archive", "salaxy-components/pages-personal/archive");
    this.customSection("person-account", [], "salaxy-components/pages-personal/account", "salaxy-components/pages-personal/account");
    this.customSection("person-calc", [], "salaxy-components/pages-personal/calc", "salaxy-components/pages-personal/calc");
    this.customSection("person-taxcards", [], "salaxy-components/pages-personal/taxcards", "salaxy-components/pages-personal/taxcards");
    return this;
  }

  /**
   * Commits the intermediary routeModel to the AngularJS $routeProvider.
   * If skipDefault is not set to true, will set the defaults by calling
   * homeDefault(), defaultSection for each product section and otherwiseDefault
   * if the corresponding sections have not already been defined.
   * @param skipDefaults By default, the method first adds all the product-defined
   * default routes to the routeModel. If true, will not add the default routes.
   */
  public commit(skipDefaults = false): void {
    if (this.routeModel.isCommitted) {
      throw new Error("RouteHelperProvider.commit() called more than once. This is not supported. Are you calling both otherwiseDefault() and commit()? The first is obsolete since the introduction of commit().");
    }
    if (!skipDefaults) {
      this.homeDefault();
      this.addIfNotExists("home");
      this.addIfNotExists("accounting");
      this.addIfNotExists("calc");
      this.addIfNotExists("messages");
      this.addIfNotExists("service");
      this.addIfNotExists("payroll");
      this.addIfNotExists("reports");
      this.addIfNotExists("irepr");
      this.addIfNotExists("irpsr");
      this.addIfNotExists("settings");
      this.addIfNotExists("accounting-targets");
      this.addIfNotExists("articles");
      this.addIfNotExists("welcome");
      this.addIfNotExists("workers");
      this.addIfNotExists("partners");
      this.addIfNotExists("invoices");
      this.addIfNotExists("payment-channel");
      this.addIfNotExists("earnings-payments");
      this.addIfNotExists("salary-reports");
      if (!this.routeModel.otherwise) {
        this.routeModel.otherwise = { templateUrl: "salaxy-components/pages/home/error404.html" };
      }
    }
    for (const when of this.routeModel.when) {
      this.$routeProvider.when(when.path, when.route);
    }
    if (this.routeModel.otherwise) {
      this.$routeProvider.otherwise(this.routeModel.otherwise);
    }
    this.routeModel.isCommitted = true;
  }

  /** Stores the route definition until Commit is being called. */
  public routeModel: routeProviderModel = {
    when: [],
  };

  /**
   * Returns true if the sction is already defined in the route model.
   * @param section Section for the views, e.g. "calc" for "salaxy-components/pages/calc"
   */
  public sectionExists(section: string): boolean {
    return !!this.routeModel.when.find((x) => x.section === section);
  }

  /**
   * Calls the defaultSection(section), but only if the sectrion has not been defined with custom values.
   */
  public addIfNotExists(section: string): boolean {
    if (this.sectionExists(section)) {
      return false;
    }
    this.defaultSection(section);
    return true;
  }
}

/** Defines a model that sotres the route until Commit is called to really register the route to Angular. */
export interface routeProviderModel {

  /** Array of when statements that make up the route */
  when: {
    /** Identifies the section as simple string (e.g. 'calc'). */
    section: string,
    /**
     * Route path (matched against $location.path). If $location.path contains redundant trailing slash or is missing one, the route will still match and the $location.path will be updated to add or drop the trailing slash to exactly match the route definition.
     *
     * - path can contain named groups starting with a colon: e.g. :name. All characters up to the next slash are matched and stored in $routeParams under the given name when the route matches.
     * - path can contain named groups starting with a colon and ending with a star: e.g.:name*. All characters are eagerly stored in $routeParams under the given name when the route matches.
     * - path can contain optional named groups with a question mark: e.g.:name?.
     *
     * For example, routes like /color/:color/largecode/:largecode*\/edit will match /color/brown/largecode/code/with/slashes/edit and extract: color: brown and largecode: code/with/slashes.
     */
    path: string,

    /** Mapping information to be assigned to $route.current on route match. */
    route: angular.route.IRoute,
  }[]

  /** The deafult route (otherwise) */
  otherwise?: angular.route.IRoute | string,

  isCommitted?: boolean;

}
