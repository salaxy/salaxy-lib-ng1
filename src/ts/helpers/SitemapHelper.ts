import angular from "angular";
import { SalaxySitemapNode } from "../services";

/**
 * Helper for managing standard web sitemaps in navigation.
 */
export class SitemapHelper {

  /**
   * Gets the standard company sitemap for Company web sites.
   */
  public static getCompanySiteMap(): SalaxySitemapNode[] {
    return this.setSitemapTitles(this.baseTree);
  }

  /**
   * Gets the combined sitemap that contains the structur for both company
   * and personal (Worker, Household). this is used mainly in development and PRO service:
   * typically, company and personal sites are separate and this tree is not used.
   */
   public static getAllRolesSiteMap(): SalaxySitemapNode[] {
     const tree = angular.copy(this.baseTree as SalaxySitemapNode[]);
     tree.splice(tree.length - 1, 0, ...this.personLeafs as SalaxySitemapNode[]);
     return this.setSitemapTitles(tree);
  }

  /**
   * Sets the titles for each node in given sitemap based on node id from the given tranlsation.
   * @param sitemap Selected sitemap to add titles to
   * @param currentPath Current node in sitemap object
   * @param i18Prefix Translation key prefix (defaults to SALAXY.NG1.Sitemap)
   */
  public static setSitemapTitles(sitemap: SalaxySitemapNode[], currentPath = "", i18Prefix = "SALAXY.NG1.Sitemap"): SalaxySitemapNode[] {
    for (const node of sitemap) {
      /* Temporarily disabled
      if (!node.id){
         console.error("Missing id in sitemap under " + currentPath)
       } */
      const newPath = currentPath ? `${currentPath}.${node.id}` : node.id;
      const sectionPostFix = node.children ? ".main" : "";
      node.title = node.title ? node.title : `${i18Prefix}.${newPath}${sectionPostFix}`;
      if (node.children) {
        this.setSitemapTitles(node.children, newPath, i18Prefix);
      }
    }
    return sitemap;
  }


  /**
   * Removes a section based on id.
   * @param sitemap Sitemap from which the section is removed.
   * @param sectionId Identifier for finding the section to remove.
   */
  public static removeSection(sitemap: SalaxySitemapNode[], sectionId: string): void {
    const index = sitemap.findIndex((x) => x.id === sectionId);
    if (index >= 0) {
      sitemap.splice(index, 1);
    }
  }

  /**
   * Gets a section based on id.
   * @param sitemap Sitemap from which the section is fetched.
   * @param sectionId Identifier for finding the section to remove.
   */
  public static getSection(sitemap: SalaxySitemapNode[], sectionId: string): SalaxySitemapNode {
    return sitemap.find((x) => x.id === sectionId);
  }

  /**
   * Gets a node (subpage) based on id.
   * @param sitemap Sitemap from which the section is fetched.
   * @param sectionId Identifier for finding the section.
   * @param nodeId Identifier for finding the node (subpage to modify or hide).
   */
  public static getNode(sitemap: SalaxySitemapNode[], sectionId: string, nodeId: string): SalaxySitemapNode {
    const section = this.getSection(sitemap, sectionId);
    if (!section || !section.children) {
      return null;
    }
    return section.children.find((x) => x.id === nodeId);
  }

  private static personLeafs = [
    {
      title: "Raportit (henkilö)",
      url: "#/person-archive/employer-report",
      id: "reports",
      roles: "person",
      children: [
      {
        title: "Työnantajan maksamat palkat",
        url: "#/person-archive/employer-report",
        id: "personEmployerReport",
      },
      {
        title: "Kotitalousvähennykset",
        url: "#/person-archive/household-deduction-report",
        id: "personHouseholdDeductionReport",
      },
      {
        title: "Työntekijälle maksetut palkat",
        url: "#/person-archive/worker-report",
        id: "personWorkerReport",
      },
      {
        url: "#/reports/yearly-summary",
        isFullWidth: false,
        id: "yearlySummary",
      },
      {
        url: "#/reports/pay-certificate",
        isFullWidth: false,
        roles: "test",
        id: "payCertificate",
      },
      {
        url: "#/reports/cumulative-report",
        isFullWidth: true,
        id: "cumulativeReport",
        roles: "test",
      },
      {
        title: "Laskut",
        url: "#/invoices",
        id: "invoices",
        children: [
          {
            title: "Laskun tiedot",
            url: "#/invoices/details/*",
            isFullWidth: false,
            hidden: true,
            id: "details",
          },
        ],
      },
      {
        title: "Palkkatietoilmoitukset",
        url: "#/irepr",
        isFullWidth: false,
        children: [{
          title: "Palkkatietoilmoitus",
          url: "#/irepr/details/*",
        }],
      },
      {
        title: "Erillisilmoitukset",
        url: "#/irpsr",
        id: "irpsr",
        isFullWidth: false,
        children: [{
          title: "Erillisilmoitus",
          url: "#/irpsr/details/*",
          id: "details",
        }],
      },
      ]
    },
    {
      title: "Asetukset (henkilö)",
      url: "#/person-account",
      id: "settings",
      roles: "person",
      children: [
        {
          title: "Omat tiedot",
          url: "#/person-account",
          id: "personAccount",
        },
        {
          url: "#/settings",
          id: "index",
        },
        {
          title: "Oma verokortti (työntekijä)",
          url: "#/person-taxcards/worker",
          id: "workerTaxcard",
        },
        {
          title: "Työntekijöiden verokortit",
          url: "#/person-taxcards/household",
          id: "householdTaxcards",
        },
        {
          url: "#/settings/pro-settings",
          roles: "test",
          id: "proSettings",
          isFullWidth: false,
        },
        {
          url: "#/settings/service-settings",
          id: "serviceSettings",
        },
        {
          url: "#/settings/authorizations",
          id: "authorizations",
        },
        {
          title: "Asiakaspalvelu",
          id: "accountants",
          url: "#/settings/accountants",
        },
        {
          id: "accountReset",
          url: "#/settings/account-reset",
          roles: "test",
        },
      ]
    },
  ];

  private static baseTree = [
    {
      url: "#/",
      id: "home",
    },
    {
      url: "#/calc",
      id: "calculations",
      children: [
        {
          url: "#/calc",
          id: "index",
        },
        {
          url: "#/calc/details/*/new-for-worker",
          hidden: true,
          id: "newForWorker",
        },
        {
          url: "#/calc/details/*/copy-as-new",
          hidden: true,
          id: "copyAsNew",
        },
        {
          url: "#/calc/details/*",
          hidden: true,
          id: "details",
        },
        {
          url: "#/calc/paid",
          hidden: false,
          id: "paid",
          roles: "company"
        },
        {
          title: "Työntekijälle maksetut",
          url: "#/person-calc/paid-worker",
          id: "paidWorker",
          roles: "person"
        },
        {
          title: "Kotitalouden maksamat",
          url: "#/person-calc/paid-household",
          id: "paidHousehold",
          roles: "person"
        },
        {
          url: "#/calc/draft",
          id: "draft",
        },
        {
          url: "#/calc/shared",
          id: "shared",
        },
        {
          title: "Työntekijän lähetetyt",
          url: "#/person-calc/sent",
          roles: "person",
          id: "sent",
        },
        {
          title: "Perutut ja virheet",
          url: "#/calc/error",
          roles: "person",
          id: "error",
        },
      ],
    },
    {
      url: "#/payroll",
      id: "payroll",
      roles: "company",
      children: [
        {
          url: "#/payroll/details/*",
          hidden: true,
          id: "details"
        },
      ],
    },
    {
      url: "#/service",
      roles: "test",
      id: "service",
      children: [
        {
          title: "Tarkista palkkalista",
          url: "#/service/approve-payroll/*",
          hidden: true,
        },
        {
          title: "Tarkista laskelma",
          url: "#/service/approve-calc/*",
          hidden: true,
        },
        {
          title: "Tilapäisen työntekijän palkka",
          url: "#/service/calculation",
        },
        {
          title: "Yrittäjän palkka",
          url: "#/service/entrepreneur",
        },
        {
          title: "Hallituspalkkio",
          url: "#/service/board",
        },
      ],
    },
    {
      id: "messages",
      url: "#/messages",
      roles: "partnerMessaging",
      children: [{
        url: "#/messages",
        id: "index",
      },
      {
        url: "#/messages/details/new",
        id: "new",
      },
      {
        url: "#/messages/details/*",
        hidden: true,
        id: "details",
      }],
    },
    {
      url: "#/workers",
      id: "workers",
      children: [
        {
          url: "#/workers",
          id: "index",
        },
        {
          url: "#/workers/details/*",
          hidden: true,
          id: "details",
        },
        {
          url: "#/workers/taxcards",
          id: "taxcards",
        },
      ],
    },
    {
      title: "Lomat ja poissaolot",
      url: "#/reports/holidays",
      id: "worktime",
      isFullWidth: true,
      roles: "test",
      children: [
        {
          title: "Lomalista",
          url: "#/reports/holidays",
          isFullWidth: true,
        },
        {
          title: "Poissaolot",
          url: "#/reports/absences",
          isFullWidth: true,
        },
        {
          title: "Lisää työntekijä",
          url: "#/partners/assure-worker-account",
          id: "assureWorkerAccount",
          isFullWidth: true,
          roles: "assureWorkerAccount",
        },
      ],
    },
    {
      url: "#/accounting",
      id: "reports",
      roles: "company",
      isFullWidth: true,
      children: [
        {
          title: "Kirjanpito",
          url: "#/accounting",
          id: "accounting",
          isFullWidth: true,
          children: [
            {
              title: "SALAXY.NG1.Sitemap.reports.accounting",
              url: "#/accounting/details/*",
              isFullWidth: true,
              hidden: true,
              id: "details",
            },
          ],
        },
        {
          title: "Kirjanpitokyselyt",
          url: "#/accounting/query",
          isFullWidth: true,
          id: "query"
        },
        {
          url: "#/reports/yearly-summary",
          isFullWidth: false,
          id: "yearlySummary",
        },
        {
          url: "#/reports/pay-certificate",
          isFullWidth: false,
          roles: "pro",
          id: "payCertificate",
        },
        {
          isFullWidth: true,
          url: "#/reports/earnings-reports",
          roles: "tbd",
          id: "earningsReports",
        },
        {
          url: "#/reports/accounting-legacy",
          isFullWidth: false,
          id: "accountingLegacy"
        },
        {
          url: "#/reports/archive-2018",
          isFullWidth: false,
          id: "archive",
        },
        {
          url: "#/reports/cumulative-report",
          isFullWidth: true,
          id: "cumulativeReport",
          roles: "test",
        },
        {
          title: "Laskut",
          url: "#/invoices",
          id: "invoices",
          roles: "pro,test",
          children: [
            {
              title: "Laskun tiedot",
              url: "#/invoices/details/*",
              isFullWidth: false,
              hidden: true,
              id: "details",
            },
          ],
        },
        {
          title: "Palkkatietoilmoitukset",
          url: "#/irepr",
          roles: "pro",
          isFullWidth: false,
          children: [{
            title: "Palkkatietoilmoitus",
            url: "#/irepr/details/*",
          }],
        },
        {
          title: "Erillisilmoitukset",
          url: "#/irpsr",
          id: "irpsr",
          isFullWidth: false,
          roles: "pro",
          children: [{
            title: "Erillisilmoitus",
            url: "#/irpsr/details/*",
            id: "details",
          }],
        },
      ],
    },
    {
      url: "#/settings",
      id: "settings",
      roles: "company",
      children: [
        {

          url: "#/settings",
          id: "index",
        },
        {

          url: "#/settings/payment-channel-settings",
          id: "paymentChannelSettings",
        },
        {
          url: "#/settings/pro-settings",
          roles: "test",
          id: "proSettings",
          isFullWidth: false,
        },
        {
          url: "#/settings/owner-settings",
          roles: "test",
          id: "ownerSettings",
        },
        {
          url: "#/accounting/editor",
          isFullWidth: true,
          roles: "test",
          id: "accountingEditor",
        },
        {
          url: "#/accounting-targets",
          id: "accountingTargets",
          roles: "test",
          children: [{
            url: "#/accounting-targets/details/*",
            isFullWidth: true,
            id: "details",
            roles: "test",
          }]
        },
        {
          url: "#/settings/service-settings",
          id: "serviceSettings",
        },
        {
          url: "#/settings/authorizations",
          id: "authorizations",
        },
        {
          id: "accountants",
          url: "#/settings/accountants",
          roles: "company",
        },
        {
          id: "integrations",
          url: "#/settings/integrations",
        },
        {
          id: "accountReset",
          url: "#/settings/account-reset",
          roles: "test",
        },
        {
          id: "paymentChannel",
          url: "#/payment-channel",
          hidden: true,
        },
      ],
    },
    {
      url: "#/articles",
      id: "cms",
      children: [
        {
          descr: "Ohjeet ja yhteys",
          url: "#/articles",
          id: "index",
        },
        {
          title: "SALAXY.NG1.Sitemap.cms.main",
          descr: "Ohjeet ja yhteys",
          url: "#/articles/abc", // temporary fallback
          id: "index",
          hidden: true,
        },
        {
          url: "#/articles/invitation",
          hidden: true, // only visible in company service
          id: "invite",
        },
        {
          descr: "Tietoa palvelustamme",
          img: "https://cdn.salaxy.com/img/elems/product-logos/palkkaus.png",
          url: "#/articles/cms-palkkaus",
          hidden: true, // only visible in company service
          id: "cmsPalkkaus",
        },
      ],
    },
  ];

}
