import { Ng1DictionaryLanguage } from "./Ng1DictionaryLanguage";

/**
 * Defines the current structure for the NG1 dictionary (all languages).
 */
export interface Ng1Dictionary {
  /** English */
  en: Ng1DictionaryLanguage,
  /** Finnish */
  fi: Ng1DictionaryLanguage,
  /** Swedish */
  sv: Ng1DictionaryLanguage,
  /** The technical language: Finnish or English from developers. */
  sx: Ng1DictionaryLanguage,
}
