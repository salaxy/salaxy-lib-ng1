/**
 * Defines the current structure for the NG1 dictionary for each language.
 * Lanugages are fi, en, sv and sx.
 */
export interface Ng1DictionaryLanguage {
  /** SALAXY is the dictionary root object for each language. */
  SALAXY: {

    /** NG1-library root */
    NG1: any,

    /** Enumerations from the core project */
    ENUM?: any,

    /** Model from the core project */
    MODEL?: any,

    /** The common user interface terms from the core project */
    UI_TERMS?: any,
  }

}