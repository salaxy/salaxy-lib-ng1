import { Ng1Dictionary } from "./Ng1Dictionary";
import { Ng1DictionaryLanguage } from "./Ng1DictionaryLanguage";

import * as enComponents from "./en/ng1-components.json";
import * as enOther from "./en/ng1-other.json";
import * as enSitemap from "./en/ng1-sitemap.json";

import * as fiComponents from "./fi/ng1-components.json";
import * as fiLegacyNavi from "./fi/ng1-legacy-navi.json";
import * as fiOther from "./fi/ng1-other.json";
import * as fiSitemap from "./fi/ng1-sitemap.json";

import * as svComponents from "./sv/ng1-components.json";
import * as svOther from "./sv/ng1-other.json";
import * as svSitemap from "./sv/ng1-sitemap.json";

import * as sxComponents from "./sx/ng1-components.json";
import * as sxOther from "./sx/ng1-other.json";
import * as sxSitemap from "./sx/ng1-sitemap.json";

export const dictionary: Ng1Dictionary = {
  en: {
    SALAXY: {
      NG1: { ...enComponents.SALAXY.NG1, ...enOther.SALAXY.NG1, ...enSitemap.SALAXY.NG1 } as any,
    },
  } as Ng1DictionaryLanguage,
  fi: {
    SALAXY: {
      NG1: { ...fiComponents.SALAXY.NG1, ...fiOther.SALAXY.NG1, ...fiSitemap.SALAXY.NG1, ...fiLegacyNavi.SALAXY.NG1 } as any,
    },
  } as Ng1DictionaryLanguage,
  sv: {
    SALAXY: {
      NG1: { ...svComponents.SALAXY.NG1, ...svOther.SALAXY.NG1, ...svSitemap.SALAXY.NG1 } as any,
    },
  } as Ng1DictionaryLanguage,
  sx: {
    SALAXY: {
      NG1: { ...sxComponents.SALAXY.NG1, ...sxOther.SALAXY.NG1, ...sxSitemap.SALAXY.NG1 } as any,
    },
  } as Ng1DictionaryLanguage,
}
