import * as angular from "angular";

/**
 * Interceptor for monitoring $http-service calls.
 */
export class LoaderInterceptor {

    /**
     * Factory method for creating the interceptor.
     */
    public static factory() {
      const factory = ($q: angular.IQService, $rootScope: angular.IRootScopeService) => {
        if (LoaderInterceptor.instance == null) {
          LoaderInterceptor.instance = new LoaderInterceptor($q, $rootScope);
        }
        return LoaderInterceptor.instance;
      };
      factory.$inject = ["$q", "$rootScope"];
      return factory;
  }

    /**
     * Singleton
     */
    private static instance: LoaderInterceptor;

    private loadingCount = 0;

    /**
     * Constructor creating a new interceptor.
     * @param  $q - $q service.
     * @param  $rootScope - Angular root scope.
     */
    private constructor(private $q: angular.IQService, private $rootScope: angular.IRootScopeService) {
    }

    /**
     * Intercepting method for request.
     * @param config - $http request.
     */
    public request = (config: angular.IRequestConfig) => {
        this.loadingCount++;
        this.$rootScope.$broadcast("salaxy-loader-show");
        return config || this.$q.when(config);
    }

    /**
     * Intercepting method for response.
     * @param response - $http response.
     */
    public response = (response: angular.IHttpPromiseCallbackArg<any>) => {
        if ((--this.loadingCount) === 0) {
            this.$rootScope.$broadcast("salaxy-loader-hide");
        }
        return response || this.$q.when(response);
    }

    /**
     * Intercepting method for response error.
     * @param rejection - $http error.
     */
    public responseError = (rejection: any) => {
        if ((--this.loadingCount) === 0) {
            this.$rootScope.$broadcast("salaxy-loader-hide");
        }
        return this.$q.reject(rejection);
    }
}
