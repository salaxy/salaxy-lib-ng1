import * as angular from "angular";

import { Dates } from "@salaxy/core";

/**
 * Interceptor for enabling non-cached views.
 * The interceptor adds a 'v'-query parameter to the template path.
 * The value of the v-parameter is a timestamp formatted by the given format.
 */
export class TemplateInterceptor {

    /**
     * Factory method for creating the interceptor.
     * Interceptor for enabling non-cached views.
     * The interceptor adds a 'v'-query parameter to the template path.
     * The value of the v-parameter is a timestamp formatted by the given format.
     * @param views - Regex for views folders (template paths), default is /views\//
     * @param format - Moment.js format for the current time based timestamp. default is "YYYYMMDDHH"
     *
     */
    public static factory(views = /views\//, format = "YYYYMMDDHH") {
      const factory = () => {
        return new TemplateInterceptor(views, format);
      };
      factory.$inject = [];
      return factory;
  }

    /**
     * Constructor for creating a new interceptor.
     */
    private constructor(private views: RegExp, private format: string) {
    }

    /**
     * Intercepting method for request.
     * @param config - $http request.
     */
    public request = (config: angular.IRequestConfig) => {
      if (config && config.url) {
        if (this.views.test(config.url)) {
          config.url = `${config.url}${config.url.indexOf("?") < 0 ? "?" : "&" }v=${Dates.getMoment(new Date()).format(this.format)}`;
        }
      }
      return config;
    }
}
