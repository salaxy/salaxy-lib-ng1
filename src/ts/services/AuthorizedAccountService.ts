import * as angular from "angular";

import { AuthorizedAccounts, Avatar, CompanyAccount, PersonAccount } from "@salaxy/core";

import { BaseService } from "./BaseService";
import { SessionService } from "./SessionService";

/** HACK: THis is just temp typing. Moving these to server side */
type partner = {
  /** todo */
  title: string,
  /** todo */
  id: string,
  /** todo */
  accountId: string,
  /** todo */
  img: string,
  /** todo */
  status: string,
  /** todo */
  enabled?: boolean,
};

/**
 * CRUD functionality for the authorized accounts.
 * Additionally listing of authorizing accounts.
 */
export class AuthorizedAccountService extends BaseService<Avatar> {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "SessionService", "AuthorizedAccounts"];

  /**
   * String that identifies the service event (onChange/notify).
   * Must be unique for the service class.
   */
  protected eventPrefix = "authorizedAccount";

  private authorizingAccounts: (PersonAccount | CompanyAccount)[] = [];

  private currentPartnerSite: any;

  /**
   * List of Software/Integration partner sites
   */
  private partnerIntegrationSiteList: partner[] = [
    {
      title: "Holvi",
      id: "holvi",
      accountId: "FI24POYH0021937564",
      img: "https://cdn.salaxy.com/img/salaxy/customer/holvi/logo.png",
      status: "Palkanmaksupalvelu Holvissa",
    },
    {
      title: "Isolta",
      id: "isolta",
      accountId: "FI24POYI0018540478",
      img: "https://cdn.salaxy.com/img/salaxy/customer/isolta/logo.png",
      status: "Isolta Arkhimedes -palkanmaksu",
    },
    {
      title: "Procountor Solo",
      id: "finago",
      accountId: "FI15POYA0008369224",
      img: "https://cdn.salaxy.com/img/salaxy/customer/finago-solo/logo.png",
      status: "Sooloyrittäjän kirjanpito- ja laskutusohjelma",
    },
    {
      title: "Accountor Go",
      id: "accountor",
      accountId: "FI03POYA0003689675",
      img: "https://cdn.salaxy.com/img/salaxy/customer/accountor-go/logo.png",
      status: "Accountor Go palvelu pienyrityksillä",
    },
    {
      title: "Briox",
      id: "briox",
      accountId: "FI33POYB0024403824",
      img: "https://cdn.salaxy.com/img/salaxy/customer/briox/logo.png",
      status: "Palkanmaksupalvelu Brioxissa",
    },
  ];

  constructor(
    $rootScope: angular.IRootScopeService,
    sessionService: SessionService,
    private authorizedAccountsApi: AuthorizedAccounts,
  ) {
    super($rootScope, sessionService, authorizedAccountsApi);

    if (sessionService.isSessionChecked && sessionService.isAuthenticated) {
      this.reloadAuthorizingAccounts();
    }
    sessionService.onAuthenticatedSession($rootScope, () => {
      this.reloadAuthorizingAccounts();
    });
  }

  /**
   * Refreshes existing authorizing accounts.
   *
   * @returns A Promise with result data (PersonAccount|CompanyAccount)[]
   */
  public reloadAuthorizingAccounts(): Promise<(PersonAccount | CompanyAccount)[]> {
    return this.authorizedAccountsApi.getAuthorizingAccounts().then((data) => this.authorizingAccounts = data);
  }

  /**
   * Lists all accounts on behalf of which this account has been authorized to act.
   *
   * @returns A Promise with result data (PersonAccount|CompanyAccount)[]
   */
  public getAuthorizingAccounts(): (PersonAccount | CompanyAccount)[] {
    return this.authorizingAccounts;
  }

  /**
   * Sets the current partner site
   * @param partnerSite A partner site to be set as current
   */
  public setCurrentPartnerSite(partnerSite: any): void {
    this.currentPartnerSite = partnerSite;
  }

  /**
   * Returns the current partner site
   */
  public getCurrentPartnerSite(): any {
    return this.currentPartnerSite;
  }

  /**
   * Return a list of all possible Integration partner sites
   *
   * Partner sites are services that have Salaxy or Palkkaus functionality in them. Others are services that have a different sign-in method,
   * and these services need an authorization to access user's Salaxy/Palkkaus account. Others use the Salaxy/Palkkaus sign-in method, so they don't
   * need separate authorization. Due to some services needing an authorization and others not needing it, all partner services need to be stored in a
   * separate property, simply listing authorizations does not suffice.
   *
   * Partner services that need an authorization have a property accountId, that is the IBAN for their Salaxy acccount
   *
   * @returns List of partner site objects.
   */
  public getPartnerIntegrationSiteList(): any {
    this.partnerIntegrationSiteList.forEach((x) => x.enabled = !!this.list.find((y) => y.id === x.accountId));
    return this.partnerIntegrationSiteList;
  }

  /**
   * Returns the partner site with given id
   *
   * @param id The name of the partner site
   *
   * @returns Partner site object with the given id, or undefined.
   */
  public getPartnerSite(id): any {
    const sites = this.getPartnerIntegrationSiteList().filter((x) => x.id === id);
    return sites.length > 0 ? sites[0] : null;
  }
}
