import * as angular from "angular";

import { CrudApiBase } from "@salaxy/core";

import { IService } from "./IService";
import { SessionService } from "./SessionService";

/**
 * Base class for Angular 1.5 (NG1) CRUD services in Salaxy environment.
 */
export abstract class BaseService<T> implements IService<T> {

    /**
     * List of items loaded to the service.
     * Empty list if items are still been loaded.
     */
    public list: T[] = [];

    /** Currently selected item. */
    public current: T = null;

    /** Set to true when the initial data hase been loaded from the server (not the anonymous dummy data). */
    public isInitialDataLoaded: boolean;

    /**
     * String that identifies the service event (onChange/notify).
     * Must be unique for the service class.
     */
    protected abstract eventPrefix;

    constructor(
        private $rootScope: angular.IRootScopeService,
        protected sessionService: SessionService,

        private api: CrudApiBase<T>,
    ) {
        if (sessionService.isSessionChecked && sessionService.isAuthenticated) {
            this.init();
        }
        sessionService.onAuthenticatedSession($rootScope, () => {
            this.init();
        });
    }

    /**
     * Reloads the list from the server - called in init and e.g. after Delete and Add new
     *
     * @returns A Promise with result data
     */
    public reloadList(): Promise<T[]> {
        return this.api.getAll().then((result) => {
          this.list = result;
          this.isInitialDataLoaded = true;
          this.notify();
          return this.list;
        });
    }

    /**
     * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
     *
     * @returns A Blank item with default values.
     */
    public getBlank(): T {
        return this.api.getBlank();
    }

    /**
     * Get the identifier of the current item, null if none is selected.
     *
     * @returns The identifier of current item, or null if not set
     */
    public getCurrentId(): string | null {
        // TODO: Consider typing T either as IIdentifiable or IApiCrudObject
        return this.current ? (this.current as any).id : null;
    }

    /**
     * Set the current item by id.
     * If id parameter is "new", the item is set to a new blank item.
     * If id paramter is null, the current item is set to null.
     *
     * @param id - The Identifier for the item to set as current
     */
    public setCurrentId(id: "new" | string): void {
        if (!id) {
            this.setCurrent(null);
            return;
        }
        if (id === "new") {
          this.newCurrent();
          return;
        }
        if (id != null && this.list.length > 0) {
            const matches = this.list.filter((x) => (x as any).id === id);
            if (matches.length > 0) {
                this.setCurrent(matches[0]);
            } else {
                throw Error(`Item ${id} could not be set as current.`);
            }
        }
    }

    /**
     * Set the current item to given item
     *
     * @param item - The item to set as current
     */
    public setCurrent(item: T): void {
        this.current = item;
        this.notify();
    }

    /**
     * Sets the current item to a new blank item.
     */
    public newCurrent(): void {
        this.setCurrent(this.getBlank());
    }

    /**
     * Saves the current item to database.
     *
     * @returns A Promise with result data: The current object after round-trip to server.
     */
    public saveCurrent(): Promise<T> {
        if (this.current) {
            return this.save(this.current);
        } else {
            throw new Error("SaveCurrent called when current is not set.");
        }
    }

    /**
     * Saves the given item to database.
     *
     * @param item - Item to be saved
     *
     * @returns A Promise with the item after the round-trip to server.
     */
    public save(item: T): Promise<T> {
        return this.api.save(item).then((result) => {
            this.setCurrent(result);
            return this.reloadList().then(() => {
                return result;
            });
        });
    }

    /**
     * Deletes the specified item.
     *
     * @param id - The identifier for the item to be deleted
     *
     * @returns A Promise with result data ("Object deleted")
     */
    public delete(id: string): Promise<string> {
        return this.api.delete(id).then((result) => {
            return this.reloadList().then(() => {
                return result;
            });
        });
    }

    /**
     * Creates a copy of a given item.
     * This method does not yet set the item as current.
     * @param copySource Item to copy as new.
     */
    public copyAsNew(copySource: T): T {
        const copy = JSON.parse(JSON.stringify(copySource)) as T;
        (copy as any).id = null;
        return copy;
    }

    /**
     * INTERNAL ONLY: This functionality is under consideration.
     * We may not support it in the future and we may remove it without it being a breaking change.
     *
     * Controllers can subscribe to changes in service data using this method.
     * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
     *
     * @param scope - Controller scope for the subscribing controller (or directive etc.)
     * @param callback - The event listener function. See $on documentation for details
     *
     * @ignore
     */
    public onChange(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
        const handler = this.$rootScope.$on(this.eventPrefix + "-service-event", callback);
        scope.$on("$destroy", handler);
    }

    /** INTERNAL ONLY: Emits the service event (typically list reload). This functionality may be dropped in future versions without warning. */
    protected notify(): void {
        this.$rootScope.$emit(this.eventPrefix + "-service-event");
    }

    private init() {
        this.reloadList();
    }
}
