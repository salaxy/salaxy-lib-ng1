/** Provides a way to cache lists and items to centralized cache. */
export class CacheService {
  /** The raw cache array */
  private rawCache: { [key: string]: {
    /**
     * URL that was called to fetch the cache.
     * Technically, this can be any string that just determines whether the URL / query has changed.
     */
    url: string,

    /** The payload for the cache. */
    data: any,

    /**
     * Time when the cache expires.
     * This is time in milliseconds, i.e. Date().getTime().
     */
    validMs: number,
  }; } = {};

  /** Return true if the given item is in the cache. */
  public hasData(key: string, url: string): boolean {
    if (!key) {
      return false;
    }
    const cacheItem = this.rawCache[key];
    if (!cacheItem) {
      return false;
    }
    if (cacheItem.validMs < + new Date().getTime()) {
      this.clear(key);
      return false;
    }
    return cacheItem.url === url;
  }

  /** Returns the data if exists / is valid. */
  public getData(key: string, url: string): any {
    if (!key) {
      return null;
    }
    if (this.hasData(key, url)) {
      return this.rawCache[key].data;
    }
  }

  /** Returns the full cache data based on key without any validation. */
  public getFullCache(key: string) {
    if (!key) {
      return null;
    }
    return this.rawCache[key] || {
      data: null,
      key,
      url: null,
      validMs: null,
    };
  }

  /**
   * Sets the new data to the cache.
   * @param key The cache key. Defining the instance where the cache is being used.
   * If null, does nothing.
   * @param url URL that was called to fetch the cache.
   * Technically, this can be any string that just determines whether the URL / query has changed.
   * @param data Data that is cached
   * @param expiresSeconds Time to live for the data in seconds starting from the current time.
   */
  public setData(key: string, url: string, data: any, expiresSeconds: number): void {
    if (!key) {
      return null;
    }
    data.$loaded = new Date().getTime();
    this.rawCache[key] = {
      data,
      url,
      validMs: new Date().getTime() + (expiresSeconds * 1000),
    };
  }

  /** Clears the entire cache - all keys. */
  public clearAllKeys() {
    this.rawCache = {};
  }

  /** Clears a given cache key */
  public clear(key: string) {
    if (!key) {
      return;
    }
    this.rawCache[key] = null;
  }
}
