import * as angular from "angular";

import { Certificate, Certificates } from "@salaxy/core";

import { BaseService } from "./BaseService";
import { SessionService } from "./SessionService";

/**
 * CRUD functionality for the Payroll objects and functionality for payroll in general.
 */
export class CertificateService extends BaseService<Certificate> {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$rootScope", "SessionService", "Certificates"];

    /**
     * String that identifies the service event (onChange/notify).
     * Must be unique for the service class.
     */
    protected eventPrefix = "certificate";

    constructor(
        $rootScope: angular.IRootScopeService,
        sessionService: SessionService,
        private certificateApi: Certificates,
    ) {
        super($rootScope, sessionService, certificateApi);
    }
}
