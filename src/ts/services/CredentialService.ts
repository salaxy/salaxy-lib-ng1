import * as angular from "angular";

import { Credentials, SessionUserCredential } from "@salaxy/core";

import { BaseService } from "./BaseService";
import { SessionService } from "./SessionService";

/**
 * CRUD functionality for the Credentials objects and functionality for Credentials in general.
 */
export class CredentialService extends BaseService<SessionUserCredential> {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$rootScope", "SessionService", "Credentials"];

    /**
     * String that identifies the service event (onChange/notify).
     * Must be unique for the service class.
     */
    protected eventPrefix = "certificate";

    constructor(
        $rootScope: angular.IRootScopeService,
        sessionService: SessionService,
        private credentialsApi: Credentials,
    ) {
        super($rootScope, sessionService, credentialsApi);
    }

    /**
     * Returns the url for uploading the avatar image file to the database
     *
     * @returns Id of the credential
     */
    public getAvatarUploadUrl(credentialId): string {
      return this.credentialsApi.getAvatarUploadUrl(credentialId);
  }
}
