/** Defines the repository interface for Salaxy CRUD objects */
export interface IService<T> {

    /** List of items in the repository */
    list: T[];

    /** Currently selected item in the repository */
    current: T;

    /**
     * Creates a copy of a given item.
     * This method does not yet set the item as current.
     * @param copySource Item to copy as new.
     */
    copyAsNew(copySource: T): T;

    /**
     * Gets a new blank object with default values, suitable for UI binding.
     * This is a synchronous method - does not go to the server and it is not 100% reliable in that way.
     * However, it shoud provide a basic object, good enough for most views.
     */
    getBlank?(): T;

    /** Reloads the list from the server - called e.g. after delete and add new */
    reloadList(): Promise<T[]>;

    /**
     * Sets the current repository item.
     * If not loaded, starts loading from the server to the current.
     * @param id - Identifier of the calculation
     */
    setCurrentId(id: string): void;

    /**
     * Sets the current calculation
     * @param item - Item to set as current
     */
    setCurrent(item: T): void;

    /**
     * Creates a new item and sets it as current for editing and saving.
     * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
     */
    newCurrent?(): void;

    /** Saves changes to the current item. */
    saveCurrent(): Promise<T>;

    /**
     * Adds or updates a given item.
     * Operation is determined based on id: null/''/'new' adds, other string values update.
     */
    save(item: T): Promise<T>;

    /** Deletes the given item from repository if possible */
    delete?(id): Promise<string>;

    /**
     * Subscribe to changes with given callback.
     */
    onChange(scope, callback): void;

}
