import {
  Ajax, Avatar, Calculation, CalculationCollection, CalculationResultLogic, Calculations, CalculationStatus, CalculatorLogic,
  Invoice, InvoicePreview, Invoices, InvoiceStatus, InvoicesLogic, InvoiceType,
  PaymentChannel, PayrollDetails
} from "@salaxy/core";
import angular from "angular";

import { InputEnumOption } from "../controllers/form-controls/InputEnumOption";
import { SessionService } from "./SessionService";
import { SettingsService } from "./SettingsService";
import { EditDialogKnownActions, UiHelpers } from "./ui";

/** Implements the user interface logic for new Invoices: Salary and side-cost payments. */
export class InvoicesService {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Invoices", "SessionService", "UiHelpers", "SettingsService", "AjaxNg1", "$location", "Calculations"];

  constructor(
    private invoicesApi: Invoices,
    private sessionService: SessionService,
    private uiHelpers: UiHelpers,
    private settingsService: SettingsService,
    private ajax: Ajax,
    private $location: angular.ILocationService,

    private calculations: Calculations,

  ) {
  }

  /**
   * Application level setting of the default payment channel.
   */
  public get defaultChannel(): PaymentChannel {
    if (this.channels) {
      const defaultChannel = this.sessionService.getSession().settings.defaultPaymentChannel;
      const confirmedChannel = this.channels.filter(x => x.id === defaultChannel).concat(this.channels).find(() => true)?.id as PaymentChannel;
      return confirmedChannel;
    }
    return null;
  }

  /**
   * Gets the enabled payment channels as avatars for the current session.
   */
  public get channels(): Avatar[] {
    const channelsForRole = () => this.sessionService.getSession().settings?.paymentChannels.filter((x) =>
      x !== PaymentChannel.PalkkausManual ||
      (x === PaymentChannel.PalkkausManual && this.sessionService.isInRole("pro"))
    );
    return this.uiHelpers.cache(this, "channels",
      () => channelsForRole()?.map((x) => InvoicesLogic.getChannelAvatar(x)),
      () => channelsForRole());
  }

  /**
   * Gets the enabled payment channels as input enum options for the current session.
   */
  public get channelEnumOptions(): InputEnumOption[] {
    return this.uiHelpers.cache(this, "channelEnumOptions",
      () => this.channels?.map((x) => ({ text: x.displayName, value: x.id, title: x.description })),
      () => this.sessionService.getSession()?.settings.paymentChannels);
  }

  /**
   * Shows the paymment page as dialog.
   * @param paymentObject - The object to pay: Either Calculation or Payroll
   */
  public showPaymentDialog(paymentObject: Calculation | PayrollDetails) {
    let channel = null;
    if (InvoicesLogic.isPayroll(paymentObject)) {
      channel = paymentObject.input.paymentChannel;
    } else if (InvoicesLogic.isCalculation(paymentObject)) {
      if (this.sessionService.isInRole("person")) {
        paymentObject.info.paymentChannel = PaymentChannel.PalkkausPersonal;
      }
      channel = paymentObject.info.paymentChannel;
    }

    const isCfaChannel = [PaymentChannel.PalkkausCfaFinvoice,
    PaymentChannel.PalkkausCfaTest,
    PaymentChannel.PalkkausCfaPaytrail,
    PaymentChannel.PalkkausCfaReference,
    PaymentChannel.HolviCfa,
    PaymentChannel.TalenomCfa].indexOf(channel) >= 0;

    const logic = {
      title:
        channel === PaymentChannel.HolviCfa ? "Palkan maksaminen" :
          channel === PaymentChannel.TalenomCfa ? "Maksaminen Talenom-onlinessa" : "Maksun esikatselu",
      type: InvoicesLogic.isPayroll(paymentObject) ? "payroll" : "calc",
      okButtonText:
        channel === PaymentChannel.HolviCfa ? "Luo maksu" :
          channel === PaymentChannel.TalenomCfa ? "Maksa Talenom-onlinessa" :
            isCfaChannel ? "Maksa" : "Lähetä maksuun",
      channel,
      paymentFunc: null, 
      isReadyForPayment: InvoicesLogic.isPayroll(paymentObject) ?
        paymentObject.info &&
        paymentObject.info.isReadyForPayment :
        InvoicesLogic.isCalculation(paymentObject) ?
          paymentObject.worker &&
          paymentObject.worker.tax.isValid &&
          !paymentObject.result.validation.errors.find((e) => (e.type !== "warning")) :
          false,
      instructions:
        channel === PaymentChannel.HolviCfa ? "Voit luoda maksun Holvin palveluun. Maksun luonnin jälkeen laskelma(t) siirtyvät \"maksu aloitettu\" tilaan. Kun vahvistat luotuja maksuja, laskelmat siirtyvät \"maksettu\" tilaan." :
          channel === PaymentChannel.TalenomCfa ? "Voit maksaa laskelma(t) Talenom-onlinessa. Palvelu lähettää sähköisen laskun Talenom-onlineen ja laskelmat siirtyvät \"maksu aloitettu\" tilaan." :
            null,

    };

    const saveForPreview = (): Promise<Calculation | PayrollDetails> => {
      if (InvoicesLogic.isCalculation(paymentObject) && 
          (
            channel == PaymentChannel.PalkkausPersonal ||
            channel == PaymentChannel.PalkkausCfaPaytrail
          )) {
        if (!paymentObject.workflow?.status || paymentObject.workflow.status === CalculationStatus.Draft) {
          const loader = this.uiHelpers.showLoading("Tallennetaan laskelmaa...");
          return this.calculations.save(paymentObject).then((savedCalculation) => {
            loader.dismiss();
            angular.copy(savedCalculation, paymentObject);
            this.$location.path("/calc/details/" + paymentObject.id + "&refresh=" + new Date().getTime());
            return paymentObject;
          });
        }
        else
        {
          return Promise.resolve(paymentObject);
        }
      }
      else
      {
        return Promise.resolve(paymentObject);
      }
    };
    

    if (isCfaChannel && channel !== PaymentChannel.PalkkausCfaPaytrail) {
        this.uiHelpers.openEditDialog("salaxy-components/modals/calc/CfaPaymentOverview.html", paymentObject, logic, "lg").then((result) => {
          if (result.action !== EditDialogKnownActions.Ok) {
            return;
          }
          this.createCfaInvoice(channel, paymentObject);
      });
    } else {
      switch (channel) {
        case PaymentChannel.PalkkausCfaPaytrail: 
        {
          logic.paymentFunc = (pObj) => this.createPalkkausCfaPaytrailInvoice(pObj);
          saveForPreview().then(() => this.uiHelpers.openEditDialog("salaxy-components/modals/calc/CfaPaymentOverview.html", paymentObject, logic, "lg"));
        }
        break;
        case PaymentChannel.PalkkausPersonal:
          {
            saveForPreview().then(() => {
              this.uiHelpers.openEditDialog("salaxy-components/modals/calc/PalkkausPersonalPaymentOverview.html", paymentObject, logic, "lg").then((result) => {
                if (result.action) {
                  if (result.action == InvoiceStatus.Canceled) {
                    this.uiHelpers.showConfirm("Haluatko varmasti perua palkanmaksun?",
                      "Haluatko varmasti perua palkanmaksun? Jos laskelma perutaan, perutaan myös siitä tehdyt tulorekisteri-ilmoitukset. Peruttua laskelmaa ei voi avata uudelleen.",
                      "Peru maksu ja tulorekisteri-ilmoitus", "Sulje perumatta").then((result) => {
                        if(result) {
                          this.createPalkkausPersonalInvoice(channel, paymentObject, InvoiceStatus.Canceled);
                        }
                      });
                  } else if (result.action !== EditDialogKnownActions.Cancel) {
                    this.createPalkkausPersonalInvoice(channel, paymentObject, result.action as InvoiceStatus);
                  }
                }
              });
            });
          }
          break;
        default:
          this.uiHelpers.openEditDialog("salaxy-components/modals/calc/PaymentOverview.html", paymentObject, logic, "lg");
          break;
      }
    }
  }

  private createCfaInvoice(channel: PaymentChannel, paymentObject: Calculation | PayrollDetails) {

    const previewInvoices = (): Promise<InvoicePreview[]> => {
      const coll: CalculationCollection = {};
      if (InvoicesLogic.isPayroll(paymentObject)) {
        coll.payrollIds = [paymentObject.id];
      } else {
        coll.calcs = [paymentObject];
      }
      return this.invoicesApi.previewInvoices(channel, coll);
    };

    const createNewInvoice = () => {
      if (InvoicesLogic.isPayroll(paymentObject)) {
        return this.invoicesApi.createInvoicesForPayroll(channel, paymentObject.id);
      } else {
        return this.invoicesApi.createInvoices(paymentObject, channel);
      }
    };

    const createInvoice = (): Promise<Invoice> => {
      return previewInvoices().then((previews) => {
        const grossInvoicePreview = previews.find((x) => x.invoice.entityType == InvoiceType.Gross);
        if (!grossInvoicePreview) {
          return null;
        }
        if (grossInvoicePreview.exists) {
          return this.invoicesApi.getSingle(grossInvoicePreview.invoice.id);
        }
        return createNewInvoice().then((invoices) => {
          return invoices.find((x) => x.header.type == InvoiceType.Gross);
        })
      });
    };

    const navigateToPaymentObject = (businessObjectId) => {
      if (InvoicesLogic.isPayroll(paymentObject)) {
        this.$location.path("/payroll/details/" + businessObjectId + "&refresh=" + new Date().getTime());
      } else {
        this.$location.path("/calc/details/" + businessObjectId + "&refresh=" + new Date().getTime());
      }
    }

    const isPaid = (invoice: Invoice) => {
      if (invoice.header.status === InvoiceStatus.Paid) {
        this.uiHelpers.showAlert("Maksu", "Laskelma/palkkalista on maksettu.");
        return true;
      }
      else if (invoice.header.status !== InvoiceStatus.PaymentStarted && invoice.header.status !== InvoiceStatus.Unread)
      {
        this.uiHelpers.showAlert("Maksua ei voitu luoda", "Maksua ei voitu luoda. Tarkista, onko laskelma/palkkalista jo maksettu.");
        return true;
      }
      return false;
    }

    const loader = this.uiHelpers.showLoading("Luodaan laskua...");
    createInvoice().then((grossInvoice) => {
      if (!grossInvoice) {
        loader.dismiss();
        this.uiHelpers.showAlert("Maksua ei voitu luoda", "Maksua ei voitu luoda. Tarkista, onko laskelma/palkkalista jo maksettu.");
        return;
      }
      switch (channel) {
        case PaymentChannel.PalkkausCfaFinvoice:
          {
            if (isPaid(grossInvoice)) {
              loader.dismiss();
              navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
            } else {
              loader.dismiss();
              const settingsReceiver = this.settingsService.current.payments.invoice.eInvoiceReceiver;
              const settingsIntermediator = this.settingsService.current.payments.invoice.eInvoiceIntermediator;
              const item = {
                eInvoiceReceiver: settingsReceiver,
                eInvoiceIntermediator: settingsIntermediator,
              };
              const showAddressSelection = !settingsReceiver || !settingsIntermediator;

              const sendEInvoice = (address: { eInvoiceReceiver: string, eInvoiceIntermediator: string }): Promise<any> => {
                const sendLoader = this.uiHelpers.showLoading("Lähetetään...");
                if (address.eInvoiceReceiver !== settingsReceiver || address.eInvoiceIntermediator !== settingsIntermediator) {
                  const settings = this.settingsService.current;
                  settings.payments.invoice.eInvoiceReceiver = address.eInvoiceReceiver;
                  settings.payments.invoice.eInvoiceIntermediator = address.eInvoiceIntermediator;
                  this.settingsService.save();
                }
                const path = `/v03-rc/api/invoices/${grossInvoice.id}/einvoice?receiver=${encodeURIComponent(address.eInvoiceReceiver)}&intermediator=${encodeURIComponent(address.eInvoiceIntermediator)}`;

                return this.ajax.postJSON(path, {}).then((result) => {
                  sendLoader.dismiss();
                  let title = "Verkkolasku on lähetetty";
                  let msg = `Maksu on lähetetty verkkolaskuna osoitteeseen ${address.eInvoiceIntermediator}/${address.eInvoiceReceiver}.`;
                  let freeText = "";
                  if (result.Response.Status !== "OK") {
                    title = "Verkkolaskua ei lähetetty";
                    msg = "Maksua ei voitu lähettää verkkolaskuna.";
                    if (result.Response.FreeText) {
                      for (const ft of result.Response.FreeText) {
                        freeText += ft["#text"] + "\n";
                      }
                    }
                  }
                  freeText = this.textToRows(freeText, 80, "\n");
                  this.uiHelpers.showAlert(title, msg + "\nStatus operaattorilta: \n" + result.Response.Status + " " + result.Response.StatusCode + "\n" + freeText);
                  return result;
                });
              };

              if (showAddressSelection) {
                const eInvoiceLogic = {
                  selectEInvoiceAddress: (itemRef) => {
                    this.settingsService.selectEInvoiceAddress(itemRef);
                  },
                };

                this.uiHelpers.openEditDialog("salaxy-components/modals/payment/cfa/EInvoice.html", item, eInvoiceLogic, null).then((result) => {
                  if (result.action !== EditDialogKnownActions.Ok) {
                    navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
                  } else {
                    sendEInvoice(result.item).then(() => {
                      navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
                    });
                  }
                })
              } else {
                sendEInvoice(item).then(() => {
                  navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
                });
              }
            }
          }
          break;
        case PaymentChannel.PalkkausCfaReference:
          {
            loader.dismiss();

            const cfaReferenceCalculations = [];
            if (InvoicesLogic.isPayroll(paymentObject)) {
              cfaReferenceCalculations.push(...paymentObject.calcs);
            } else if (InvoicesLogic.isCalculation(paymentObject)) {
              cfaReferenceCalculations.push(paymentObject)
            }

            const cfaReferenceTotalResultCalculation = CalculatorLogic.getBlank();
            cfaReferenceTotalResultCalculation.worker.tax = cfaReferenceTotalResultCalculation.worker.tax || {};
            cfaReferenceTotalResultCalculation.result = CalculationResultLogic.add(cfaReferenceCalculations.map((x) => x.result));

            const referenceLogic = {
              printClass: "sxyFullPrint",
              print: (pcls: "sxyFullPrint" | "sxySecurePrint") => {
                referenceLogic.printClass = pcls;
                setTimeout(() => {
                  window.print();
                }, 0);
              },
              calculations: cfaReferenceCalculations,
              totalResultCalculation: cfaReferenceTotalResultCalculation,
            };
            // show dialog for every invoice status, for paid too
            this.uiHelpers.openEditDialog("salaxy-components/modals/payment/cfa/Transfer.html", grossInvoice, referenceLogic, "lg").then(() => {
              navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
            });
          }
          break;
        case PaymentChannel.PalkkausCfaTest:
          {
            if (isPaid(grossInvoice)) {
              loader.dismiss();
              navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
            } else {
              loader.setHeading("Maksetaan...")
              this.invoicesApi.requestStatusUpdate(grossInvoice.id, InvoiceStatus.Paid).then(() => {
                loader.dismiss();
                navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
              });
            }
          }
          break;
        case PaymentChannel.HolviCfa:
          {
            if (isPaid(grossInvoice)) {
              loader.dismiss();
              navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
            } else {
              loader.setHeading("Luodaan maksua Holviin...");
              this.ajax.postJSON(this.ajax.getServerAddress() + "/partner/holvi/invoice/" + grossInvoice.id, null).then((holviResult) => {
                loader.dismiss();
                if (holviResult) {
                  this.uiHelpers.showAlert("Palkka/palkat on nyt siirretty maksuun", "Mene Holvin Tapahtumat-sivulle ja vahvista maksu.\nMaksun vahvistuksen jälkeen työntekijän nettopalkka \ntilittyy työntekijän tilille 2–4 pankkipäivän kuluessa.\nPalkkaus.fi tekee tulorekisteri-ilmoitukset ja tilittää maksut eri tahoille.");
                }
                navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
              });
            }
          }
          break;
        case PaymentChannel.TalenomCfa:
          {
            if (isPaid(grossInvoice)) {
              loader.dismiss();
              navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
            } else {
              loader.setHeading("Luodaan maksua Talenom-onlineen...");
              this.ajax.postJSON(this.ajax.getServerAddress() + "/partner/talenom/invoice/" + grossInvoice.id, null).then((talenomResult) => {
                loader.dismiss();
                if (talenomResult) {
                  this.uiHelpers.showAlert("Lähetys onnistui", "Maksusta on lähetetty lasku Talenom Online -palveluun.\nMaksun päivittymisessä voi kestää 1-15 minuuttia.\nKäy vielä hyväksymässä maksu Talenom Online -palvelussa!");
                }
                navigateToPaymentObject(grossInvoice.header.businessObjects[0]);
              });
            }
          }
          break;
      }
    });
  }

  private createPalkkausCfaPaytrailInvoice(paymentObject: Calculation | PayrollDetails) {

    const navigateToPaymentObject = (businessObjectId) => {
      if (InvoicesLogic.isPayroll(paymentObject)) {
        this.$location.path("/payroll/details/" + businessObjectId + "&refresh=" + new Date().getTime());
      } else {
        this.$location.path("/calc/details/" + businessObjectId + "&refresh=" + new Date().getTime());
      }
    }

    const getPaytrailUrl = (businessObjectId) => {
      if (InvoicesLogic.isPayroll(paymentObject)) {
        return this.ajax.getServerAddress() + `/SalaryPayment/PayInvoiceForPayroll/${businessObjectId}?access_token=${this.ajax.getCurrentToken()}`;
      } else {
        return this.ajax.getServerAddress() + `/SalaryPayment/PayInvoiceForCalculation/${businessObjectId}?access_token=${this.ajax.getCurrentToken()}`;
      }
    }

    const paytrailUrl = getPaytrailUrl(paymentObject.id);
    this.uiHelpers.showExternalDialog("PaytrailPayment", paytrailUrl, {}).then((result) => {
        if (result.action == "user-cancel") {
          this.uiHelpers.showDialog("salaxy-components/modals/payment/PaymentCancel.html").then(() => {
            navigateToPaymentObject(paymentObject.id);
          });
        } else if (result.action == "invoice-paid") {
          this.uiHelpers.showAlert("Maksu", "Laskelma/palkkalista on jo maksettu.").then(() => {
            navigateToPaymentObject(paymentObject.id);
          });
        } else if (result.action == "invoice-missing") {
          this.uiHelpers.showAlert("Maksua ei voitu luoda", "Maksua ei voitu luoda. Tarkista, onko laskelma/palkkalista jo maksettu.").then(() => {
          navigateToPaymentObject(paymentObject.id);
          });
        } else if (result.action == EditDialogKnownActions.Ok) {
          this.uiHelpers.showDialog("salaxy-components/modals/payment/PaymentConfirm.html").then(() => {
            navigateToPaymentObject(paymentObject.id);
          })
        } else {
          navigateToPaymentObject(paymentObject.id);
        }
    });
  }

  private createPalkkausPersonalInvoice(channel: PaymentChannel, paymentObject: Calculation | PayrollDetails, status: InvoiceStatus) {
    if (InvoicesLogic.isPayroll(paymentObject)) {
      this.uiHelpers.showAlert("Palkkalista ja kotitaloudet", "Palkkalistan käyttöä ei tueta toistaiseksi kotitalouksien palkanmaksussa.");
      return;
    }

    const updateInvoiceStatus = (invoice: Invoice, newStatus: InvoiceStatus): Promise<Invoice> => {
      if (invoice.header.status === newStatus) {
        return Promise.resolve(invoice);
      }
      return this.invoicesApi.requestStatusUpdate(invoice.id, newStatus);
    };

    const previewInvoices = (): Promise<InvoicePreview[]> => {
      return this.invoicesApi.previewInvoices(channel, {
        calcs: [paymentObject],
      });
    };

    const createNewInvoice = (): Promise<Invoice[]> => {
      return this.invoicesApi.createInvoices(paymentObject, channel);
    };

    const createInvoice = (): Promise<Invoice> => {
      return previewInvoices().then((previews) => {
        const netInvoicePreview = previews.find((x) => x.invoice.entityType == InvoiceType.Net && x.invoice.status !== InvoiceStatus.Canceled && x.invoice.status !== InvoiceStatus.Error);
        if (!netInvoicePreview) {
          return null;
        }
        if (netInvoicePreview.exists) {
          return this.invoicesApi.getSingle(netInvoicePreview.invoice.id);
        }
        return createNewInvoice().then((invoices) => {
          return invoices.find((x) => x.header.type == InvoiceType.Net);
        })
      });
    };

    const navigateToPaymentObject = (businessObjectId) => {
      this.$location.path("/calc/details/" + businessObjectId + "&refresh=" + new Date().getTime());
    }

    const loader = this.uiHelpers.showLoading("Päivitetään maksun tilaa...");
    createInvoice().then((netInvoice) => {
      if (!netInvoice) {
        loader.dismiss();
        this.uiHelpers.showAlert("Maksun tilaa ei voitu päivittää", "Maksun tilaa ei voitu päivittää. Tarkista, onko maksu peruttu.");
        return;
      }
      updateInvoiceStatus(netInvoice, status).then(() => {
        loader.dismiss();
        navigateToPaymentObject(netInvoice.header.businessObjects[0]);
      });
    });
  }

  private textToRows(text: string, rowLength: number, separator: string): string {
    if (!text) {
      return text;
    }
    const result = [];
    const rows = text.split(separator);
    for (const row of rows) {
      let remaining = row;
      while (remaining.length > rowLength) {
        result.push(remaining.substr(0, rowLength));
        remaining = remaining.slice(rowLength);
      }
      if (remaining.length > 0) {
        result.push(remaining);
      }
    }
    return result.join(separator);
  }
}