import * as angular from "angular";

import { Ajax, Onboarding, Onboardings, WebSiteUserRole } from "@salaxy/core";

import { SessionService } from "./SessionService";
import { UiHelpers } from "./ui";

/**
 * Manages the onboarding process where an account is created.
 */
export class OnboardingService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "$sce", "Onboardings", "SessionService", "UiHelpers", "AjaxNg1", "$location"];

  /** The current onboarding model */
  public model: Onboarding;

  /** Unique identifier of the onboarding data in storage */
  public id: string;

  /** Server address for onboarding pages etc. */
  public serverAddress: string;

  private lastPdfPreviewUrl;

  constructor(
    private $rootScope: angular.IRootScopeService,
    private $sce: angular.ISCEService,
    private onboardingsApi: Onboardings,
    private sessionService: SessionService,
    private uiHelpers: UiHelpers,
    private ajax: Ajax,
    private $location: angular.ILocationService,
  ) {
    this.init();
  }

  /** Gets / refreshes the onboarding data from the server  */
  public getOnboardingData(id: string = null): Promise<Onboarding> {
    return this.onboardingsApi.getOnboarding(id || "latest").then((result) => {
      this.model = result;
      this.id = result.id;
      this.notify();
      return this.model;
    });
  }

  /**
   * Get the onboarding object for given account.
   * @param accountId - Account id for existing account.
   */
  public getOnboardingDataForAccount(accountId: string): Promise<Onboarding> {
    return this.onboardingsApi.getOnboardingForAccount(accountId).then((data) => {
      if (data) {
        this.id = data.id;
      } else {
        this.id = null;
      }
      this.model = data;
      return this.model;
    });
  }

  /**
   * Saves the current onboarding to database.
   *
   * @returns A Promise with result data (Onboarding)
   */
  public save(): Promise<Onboarding> {
    const method = (this.model.signature as any).method;
    return this.onboardingsApi.saveOnboarding(this.model).then((result) => {
      if (this.id !== result.id) {
        this.id = result.id;
      }
      this.model = result;
      (this.model.signature as any).method = method;
      return this.model;
    });
  }

  /**
   * Commits current onboarding  and creates/changes the account.
   *
   * @returns A Promise with result data (Onboarding)
   */
  public commit(): Promise<Onboarding> {
    return this.onboardingsApi.commitOnboarding(this.model).then((data: Onboarding) => {
      this.model = data;
      return this.model;
    });
  }

  /**
   * Sends a new pin code for verifying the telephone number in onboarding.
   * Uses onboarding model stored in this service.
   *
   * @returns A Promise with result data (true if sending succeeded).
   */
  public sendSmsVerificationPin(): Promise<boolean> {
    return this.onboardingsApi.sendSmsVerificationPin(this.model);
  }

  /**
   * Checks the pin code.
   *
   * @returns A Promise with result data (true if sending succeeded).
   */
  public checkSmsVerificationPin(): Promise<boolean> {
    return this.save().then(() => {
      if (this.model && this.model.validation) {
        const error = this.model.validation.errors.find((x) => x.key === "Person.ContactVerification.Telephone.Pin");
        return !error;
      }
    });
  }

  /**
   * Return Visma sign url for current model.
   * @param authService Select the signature service (bank / mobile auth).
   */
  public getVismaSignUrl(authService: string): string {
    const returnUrl = (parent !== window) ? document.referrer : null;
    return this.getServerAddress() + "/onboarding/VismaSignBegin?id="
      + this.model.owner + "|" + this.id
      + "&signerPersonalId=" + (this.model.signature.personalId || "")
      + "&authService=" + (authService || "")
      + (returnUrl ? "&successUrl=" + encodeURIComponent(returnUrl) : "")
      ;
  }

  /**
   * Gets the PDF preview address taking into account potential changes in the model.
   *
   * @param asUntrusted - Boolean indicating if the return url should be returned as untrusted (not wrapped).
   * By default, the url is returned as trusted url.
   * @returns Url address
   */
  public getPdfPreviewAddress(asUntrusted = false): string {
    const url = this.getServerAddress() + "/onboarding/GetAuthorizationPdfPreview?id=" +
      this.model.owner + "|" + this.id + "&signer.personalId=" + (this.model.signature.personalId || "PPKKVV-nnnX");
    if (!this.lastPdfPreviewUrl || this.lastPdfPreviewUrl.url !== url) {
      this.lastPdfPreviewUrl = {
        url,
        sce: this.$sce.trustAsResourceUrl(url),
      };
    }
    if (asUntrusted) {
      return this.lastPdfPreviewUrl.url;
    }
    return this.lastPdfPreviewUrl.sce;
  }

  /**
   * Controllers can subscribe to changes in service data using this method.
   * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
   *
   * @param scope Controller scope for the subscribing controller (or directive etc.)
   * @param callback The event listener function. See $on documentation for details
   */
  public subscribe(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
    const handler = this.$rootScope.$on("onboarding-service-event", callback);
    scope.$on("$destroy", handler);
  }

  /**
   * Opens customer wizard as modal dialog.
   * @param accountId - Optional accountId for existing company.
   */
  public launchCustomerOnboarding(accountId: string): Promise<any> {

    const getModel = () => {
      return this.getOnboardingDataForAccount(accountId).then(() => {
        if (!this.model) {
          this.model = {
            accountType: WebSiteUserRole.Company,
            signature: {},
          };
          this.id = null;
          return this.save();
        } else {
          return this.model;
        }
      });
    };

    return getModel().then(() => {
      // ensure correct role
      this.model.accountType = WebSiteUserRole.Company;
      return this.uiHelpers.showDialog(
        "salaxy-components/modals/onboarding/customer/index.html",
        "CustomerOnboardingController",
        null,
        null,
        "lg");
    });
  }

  /**
   * Opens a generic wizard as modal dialog.
   * If account type is known, opens the the wizard for that type.
   * @param id - Optional onboarding id for existing onboarding.
   * @param isModelLoaded  - If true, the method does not load the model from backend any more.
   */
  public launchGenericOnboarding(id: string = null, isModelLoaded = false): void {
    const getModel = () => {
      if (isModelLoaded) {
        return Promise.resolve(this.model);
      }
      return this.getOnboardingData(id);
    };

    getModel().then(() => {
      if (this.launchOnboardingOfType(this.model.accountType, id, true)) {
        return;
      }
      this.uiHelpers.showDialog(
        "salaxy-components/modals/onboarding/generic/index.html",
        null,
        {
          launch: (close) => {
            if (this.launchOnboardingOfType(this.model.accountType, id, true)) {
              close();
            }
          },
          model: this.model,
        },
        null);
    });
  }

  /**
   * Launches an onboarding of a type if type is specific.
   * @param role Role that is evaluated: Launched if it matches one of the specific roles.
   * Does not start onboarding if role is None / null etc.
   * @param id Identifier of onboarding object.
   * @param isModelLoaded If true, the model is already loaded.
   * @returns True if the dialog is launched.
   */
  public launchOnboardingOfType(role: WebSiteUserRole, id: string = null, isModelLoaded = false) {
    switch (this.model.accountType) {
      case WebSiteUserRole.Company:
        this.launchCompanyOnboarding(id, isModelLoaded);
        return true;
      case WebSiteUserRole.Household:
        this.launchHouseholdOnboarding(id, isModelLoaded);
        return true;
      case WebSiteUserRole.Worker:
        this.launchWorkerOnboarding(id, isModelLoaded);
        return true;
      default:
        return false;
    }
  }

  /**
   * Opens worker wizard as modal dialog.
   * @param id - Optional onboarding id for existing worker.
   * @param isModelLoaded  - If true, the method does not load the model from backend any more.
   */
  public launchWorkerOnboarding(id: string = null, isModelLoaded = false): Promise<any> {
    return this.launchOnboarding(WebSiteUserRole.Worker, "salaxy-components/modals/onboarding/worker/index.html", "WorkerOnboardingController", id, isModelLoaded);
  }

  /**
   * Opens company wizard as modal dialog.
   * @param id - Optioanl onboarding id for existing company.
   * @param isModelLoaded  - If true, the method does not load the model from backend any more.
   */
  public launchCompanyOnboarding(id: string = null, isModelLoaded = false): Promise<any> {
    return this.launchOnboarding(WebSiteUserRole.Company, "salaxy-components/modals/onboarding/company/index.html", "CompanyOnboardingController", id, isModelLoaded);
  }

  /**
   * Opens household wizard as modal dialog.
   * @param id - Optioanl onboarding id for existing company.
   * @param isModelLoaded  - If true, the method does not load the model from backend any more.
   */
  public launchHouseholdOnboarding(id: string = null, isModelLoaded = false): Promise<any> {
    return this.launchOnboarding(WebSiteUserRole.Household, "salaxy-components/modals/onboarding/household/index.html", "HouseholdOnboardingController", id, isModelLoaded);
  }

  /**
   * Opens a simple signature page for federation (currently for Raksa / Palkkamylly only)
   * @param id - Optioanl onboarding id for existing person.
   * @param isModelLoaded  - If true, the method does not load the model from backend any more.
   */
   public launchFederateSign(id: string = null, isModelLoaded = false): Promise<any> {
    return this.launchOnboarding(WebSiteUserRole.Worker, "salaxy-components/modals/onboarding/federate-sign.html", "WorkerOnboardingController", id, isModelLoaded);
  }

  private launchOnboarding(role: WebSiteUserRole, index: string, ctrl: string, id: string = null, isModelLoaded = false): Promise<any> {
    const getModel = () => {
      if (isModelLoaded) {
        return Promise.resolve(this.model);
      }

      return this.getOnboardingData(id);
    };

    return getModel().then(() => {
      // ensure correct role
      this.model.accountType = role;
      return this.uiHelpers.showDialog(
        index,
        ctrl,
        null,
        null,
        "lg");
    });
  }

  private notify(): void {
    this.$rootScope.$emit("onboarding-service-event");
  }

  private getServerAddress(): string {
    if (this.serverAddress) {
      return this.serverAddress.replace(/\/+$/, "");
    }
    return this.sessionService.getServerAddress();
  }

  /**
   * Continues based on returnUrl parameter from the current url.
   */
  private launchReturnUrlOnboarding() {

    const returnUrlParameters = this.getReturnUrlOnboardingParameters();

    if (returnUrlParameters) {

      const authenticate = (): Promise<any> => {
        const token = this.readTokenFromUrl();
        if (token) {
          this.ajax.setCurrentToken(token);
          return this.sessionService.checkSession();
        } else {
          return Promise.resolve();
        }
      };

      authenticate().then(() => {
        switch (returnUrlParameters.onboardingType) {
          case "company":
            this.launchCompanyOnboarding(returnUrlParameters.id);
            break;
          case "household":
            this.launchHouseholdOnboarding(returnUrlParameters.id);
            break;
          case "worker":
            this.launchWorkerOnboarding(returnUrlParameters.id);
            break;
          case "customer":
            this.launchOnboarding(WebSiteUserRole.Company, "salaxy-components/modals/onboarding/customer/index.html", "CustomerOnboardingController", returnUrlParameters.id);
            break;
          default:
            break; // do nothing
        }
        this.$rootScope.$evalAsync(() => {
          this.$location.path("/");
        });

      });
    }
  }

  private getReturnUrlOnboardingParameters() {
    const url = this.$location.url();
    if (url.indexOf("/onboarding/") !== 0) {
      return null;
    }
    // onboarding/company/xyz
    // use regex...
    const params = url.substr("/onboarding/".length).split("/");
    if (params.length < 2) {
      return;
    }
    const onboardingType = params[0];
    const id = params[1].split("&")[0];
    return {
      onboardingType,
      id,
    };
  }

  private readTokenFromUrl(): string {
    // shoud use regex...
    const key = "ob_token=";
    const url = this.$location.url();
    const start = url.toLowerCase().indexOf(key);
    if (start >= 0) {
      const end = url.indexOf("&", start);
      return end >= 0
        ? url.substring(start + key.length, end)
        : url.substring(start + key.length);
    }
    return null;
  }

  private init() {
    this.$rootScope.$on("$locationChangeStart", () => {
      this.launchReturnUrlOnboarding();
    });
  }
}
