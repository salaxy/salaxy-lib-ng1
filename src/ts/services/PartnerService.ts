import { BrandListItem } from "@salaxy/core";

import { SessionService } from "./SessionService";
import { EditDialogKnownActions, EditDialogResult } from "./ui";
import { UiHelpers } from "./ui/UiHelpers";

/**
 * Provides methods for managing the Primary partner,
 * which is primary source of service in payroll matters.
 * Typically, this would be customer's primary Accounting company.
 */
export class PartnerService {

  /**
   * For NG-dependency injection.
   * @ignore
   */
  public static $inject = ["SessionService", "UiHelpers"];

  /** Skin that is currently in use as a temporary override (for preview purposes) or null if default skin is used. */
  public overrideSkin: string;

  /** Partner id that is currently in use as a temporary override (for preview purposes) or null if real id used. */
  public overridePartnerId: string;

  /**
   * Creates a new instance of the PartnerService with dependency injection.
   */
  constructor(private sessionService: SessionService, private uiHelpers: UiHelpers) {
  }

  /**
   * Shows a dialog for brand selection.
   * The result may be "ok" for selected brand or "set-default" for reset to original brand.
   */
  public showBrandSelection() : Promise<EditDialogResult<BrandListItem>> {
    const item: {
      brand: BrandListItem
    } = {
      brand: null
    };
    return this.uiHelpers.openEditDialog("salaxy-components/modals/settings/BrandSelection.html", item, null).then((dialog) => {
      const result: EditDialogResult<BrandListItem> = {
        action: dialog.action,
        hasChanges: dialog.hasChanges,
        logic: dialog.logic,
        result: dialog.result,
        item: dialog.result == EditDialogKnownActions.Ok ? dialog.item.brand : null,
      };
      return result;
    });
  }

  /**
   * Shows a dialog for switching the CSS.
   */
   public showSwitchCss(): void {
     this.showBrandSelection().then((dialog) => {
      if (dialog.result == EditDialogKnownActions.Ok) {
        this.overrideSkin = dialog.item.data.skin;
        this.overridePartnerId = dialog.item.id;
        this.sessionService.switchCss(dialog.item.data.skin);
      } else if (dialog.result == "set-default") {
        this.overrideSkin = null;
        this.overridePartnerId = null;
        this.sessionService.switchCss(null);
      }
    })
  }
}
