﻿import * as angular from "angular";

import { AccountingReportTable, AccountingReportTableType, calcReportType, Calculation, PeriodType, Report, reportPartial, Reports, ReportsLogic, ReportType } from "@salaxy/core";

import { SessionService } from "./SessionService";
import { UiHelpers } from "./ui";

/**
 * Methods for viewing and later also generating different reports.
 */
export class ReportsService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "SessionService", "UiHelpers", "Reports", "$sce"];

  /**
   * Convenience property for navigation controls to set a current report type.
   * Checked, when getReports is called with null or "current"
   */
  public currentReportType: ReportType = null;

  /** Cache for reports-by-type as generated on the server */
  private reportMetadataCache = {};

  /**
   * Creates a new instance of ReportsService
   *
   * @param $rootScope - Angular rootscope.
   * @param sessionService - Session service notifies when the user is known to be authenticated.
   * @param uiHelpers - Salaxy ui helpers service.
   * @param reportsApi - The Reports API that is used to communicating with the server.
   * @param $sce - $sce is a service that provides Strict Contextual Escaping services to AngularJS
   */
  constructor(
    $rootScope: angular.IRootScopeService,
    private sessionService: SessionService,
    private uiHelpers: UiHelpers,
    private reportsApi: Reports,

    private $sce: angular.ISCEService,
  ) {
    if (sessionService.isSessionChecked && sessionService.isAuthenticated) {
      this.reportMetadataCache = {};
    }

    sessionService.onAuthenticatedSession($rootScope, () => {
      this.reportMetadataCache = {};
    });
  }

  /**
   * Gets a list of reports (metadata only) filtered by a report type.
   *
   * @param type - Type of report. See type (string enumeration) for possible values.
   * Also supports value "current" for showing reports defined by currentReportType.
   *
   * @returns A Promise with result data (<Report[]) if dataset not fetched, else the fetched dataset
   */
  public getReports(type: ReportType | "current"): Promise<Report[]> | Report[] {
    if (!type || type === "current") {
      type = this.currentReportType;
    }
    if (!type || type as any === "undefined") {
      return [];
    }
    let list = this.reportMetadataCache[type];
    if (!list) {
      list = [];
      this.reportMetadataCache[type] = [];
      return this.reportsApi.getReportsByType(type).then((result) => {
        this.reportMetadataCache[type] = result;
        return result;
      });
    }
    return list;
  }

  /**
   * Gets a link URL for a yearly report. This is a full link with token and absolute URL.
   *
   * @param type - Type of the report must be one of the yearly reports
   * @param year - Year for the report
   * @param id - Worker ID for those reports that are specific to one Worker.
   * @param id2 - Second Worker ID for those reports that have two Workers in one report
   *
   * @returns Yearly report URL string
   */
  public getYearlyReportUrl(type: ReportType, year: number, id?: string, id2?: string): string {
    return this.sessionService.getServerAddress()
      + ReportsLogic.getYearlyReportUrl(type, year, this.sessionService.getCurrentToken(), id, id2);
  }

  /**
   * Gets a. URL for a calculation report.
   * If report has not been saved (ID is null), returns null.
   * @param reportType - Type of report
   * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
   * @param cumulative If true, fetches the report as cumulative.
   * Currently only supported by salarySlip
   * @returns Url for specified report
   */
  public getReportUrlForCalc(reportType: calcReportType, calcId: string, cumulative = false): string {
    if (!calcId) {
      return null;
    }
    return this.sessionService.getServerAddress()
      + ReportsLogic.getCalcReportUrl(reportType, calcId, this.sessionService.getCurrentToken(), cumulative);
  }

  /**
   * Gets a. URL for a calculation pdf.
   * If report has not been saved (ID is null), returns null.
   *
   * @param reportType - Type of report
   * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
   * @param inline - If true, the Content-Disposition header is returned with inline parameter.
   *
   * @returns Url for specified report
   */
  public getPdfUrlForCalc(reportType: calcReportType, calcId: string, inline = false): string {
    if (!calcId) {
      return null;
    }
    return this.sessionService.getServerAddress()
      + ReportsLogic.getCalcPdfUrl(reportType, calcId, inline, this.sessionService.getCurrentToken());
  }

  /**
   * Show a report modal (a preview modal dialog) for a given calculation.
   * The calculation may not be stored to back end. I.e. this reporting method is available also to
   * non-authenticated users unlike the more resource intensive methods that generate PDF-files and require CRUD rights.
   */
  public showReportModalForCalc(reportType: calcReportType, calculation: any): void {

    const calcs: Calculation[] = [calculation];

    const data = {
      reportType,
      calcs,
    };

    this.uiHelpers.showDialog(
      "salaxy-components/modals/calc/CalcReports.html",
      null,
      data,
      null,
      "lg",
      true);
  }

  /**
   * Gets the monthly / quarterly/ yearly accounting data for the current account.
   *
   * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
   * @param tableType  - Accounting table type.
   * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
   * @param endDate - End date for the period. Required only for the custom period.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingReportTableForPeriod(refDate: string, tableType: AccountingReportTableType = AccountingReportTableType.Classic, periodType: PeriodType = PeriodType.Month, endDate: string = null): Promise<AccountingReportTable> {
    return this.reportsApi.getAccountingReportTableForPeriod(refDate, tableType, periodType, endDate);
  }

  /**
   * Experimental: Gets the accounting report based on given set of calculations.
   *
   * @param calculationIds - Calculations that are the bases for the report.
   * @param tableType  - Accounting table type.
   * @returns Account report based on the calculations.
   * @ignore
   */
  public getAccountingReportTableForCalculationIds(calculationIds: string[], tableType: AccountingReportTableType = AccountingReportTableType.Classic): Promise<AccountingReportTable> {
    return this.reportsApi.getAccountingReportTableForCalculationIds(calculationIds, tableType);
  }

  /**
   * Experimental: Gets the accounting report based on given set of calculations.
   *
   * @param calculations - Calculations that are the bases for the report.
   * @param tableType  - Accounting table type.
   * @returns Account report based on the calculations.
   * @ignore
   */
  public getAccountingReportTableForCalculations(calculations: Calculation[], tableType: AccountingReportTableType = AccountingReportTableType.Classic): Promise<AccountingReportTable> {
    return this.reportsApi.getAccountingReportTableForCalculations(calculations, tableType);
  }

  /**
   * Gets an HTML report based on Calculation ID and runs it through $sce.
   *
   * @param reportType - Type of the report to fetch. See the HtmlReportType enumeration for possible values.
   * @param calculationId - GUID for the calculation
   *
   * @returns A Promise with result HTML - already run through $sce.
   */
  public getReportHtmlById(reportType: reportPartial, calculationId: string): Promise<any> {
    return this.reportsApi.getReportHtmlById(reportType, calculationId).then((html) => {
      return this.$sce.getTrustedHtml(html);
    });
  }
}
