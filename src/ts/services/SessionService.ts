﻿import * as angular from "angular";

import {
  AccountBase,
  Accounts,
  Ajax,
  AppStatus,
  Arrays,
  Avatar,
  CompanyAccount,
  Configs,
  OAuthSalaxyAuthorizeMode,
  PaymentChannel,
  PersonAccount,
  Role,
  RoleLogic,
  Session,
  SystemRole,
  Token,
  UserSession,
} from "@salaxy/core";
import { AlertService } from "./ui";

/**
 * Helps in managing the login process and provides information of the current session
 */
export class SessionService {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "$rootScope",
    "Session",
    "Accounts",
    "$window",
    "$location",
    "AjaxNg1",
    "$timeout",
    "AlertService",
  ];

  /** If true, the user is authenticated */
  public isAuthenticated: boolean;

  /** If true, the session has been checked from the server */
  public isSessionChecked: boolean;

  /** If true, the session call is progressing  */
  public isSessionChecking: boolean;

  /** Avatar to show in the login screen */
  public avatar: Avatar;

  /** The full session objcet */
  public session: UserSession;

  /** The person account object if the currently selected account is a person */
  public personAccount: PersonAccount = null;

  /** The company account object if the currently selected account is a company */
  public companyAccount: CompanyAccount = null;

  /** Sign in error */
  public signInError: string;

  /** Sign in error description */
  public signInErrorDescription: string;

  /** Sign in error page url */
  public signInErrorUrl: string;

  /** Partner site */
  public partnerSite: string;

  /**
   * Application specific roles that are set on the client (browser code)
   * as opposed to normal server-side defined roles.
   */
  public clientRoles: string[] = [];

  constructor(
    private $rootScope: angular.IRootScopeService,
    private sessionApi: Session,
    private accountApi: Accounts,
    private $window: angular.IWindowService,
    private $location: angular.ILocationService,
    private ajax: Ajax,

    private $timeout: angular.ITimeoutService,
    private alertService: AlertService,
  ) {
    this.subscribeToLocationChange();
  }

  /**
   * Initializes the session service.
   * Note that the session loading will currently begin already in constructor.
   */
  public init(): void {
    this.alertService.init(); // This should be done automatically in constructor, but we make sure here so that there is an explicit reference.
  }

  /**
   * Controllers can subscribe to changes in service data using this method.
   * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
   *
   * @param scope - Controller scope for the subscribing controller (or directive etc.)
   * @param callback - The event listener function. See $on documentation for details
   */
  public subscribe(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
    const handler = this.$rootScope.$on("session-service-event", callback);
    scope.$on("$destroy", handler);
  }

  /**
   * Services can subscribe to this event to be notified when it is known that there is an authenticated session
   * (as opposed to anonymous session). Uses the same pattern as subscribe.
   *
   * @param scope - Controller scope for the subscribing controller (or directive etc.)
   * @param callback - The event listener function. See $on documentation for details
   */
  public onAuthenticatedSession(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
    const handler = this.$rootScope.$on("session-auth-session-event", callback);
    scope.$on("$destroy", handler);
  }

  /** If true, the user is authenticated */
  public getIsAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  /** If true, the session has been checked from the server */
  public getIsSessionChecked(): boolean {
    return this.isSessionChecked;
  }

  /** If true, the session call is progressing */
  public getIsSessionChecking(): boolean {
    return this.isSessionChecking;
  }

  /** Avatar to show in the login screen */
  public getAvatar(): Avatar {
    return this.avatar;
  }

  /** The full session objcet */
  public getSession(): UserSession {
    return this.session;
  }

  /** Gets either the Company account or PErson account depending of the type of the current account. */
  public getCurrentAccount(): AccountBase {
    return this.getCompanyAccount() || this.getPersonAccount();
  }

  /** Get the person account object if the currently selected account is a person */
  public getPersonAccount(): PersonAccount {
    return this.personAccount;
  }

  /** The company account object if the currently selected account is a company */
  public getCompanyAccount(): CompanyAccount {
    return this.companyAccount;
  }

  /** Gets the address for the API server where the session is connected */
  public getServerAddress(): string {
    return this.sessionApi.getServerAddress();
  }

  /**
   * Returns current access token
   */
  public getCurrentToken(): string {
    return this.ajax.getCurrentToken();
  }

  /**
   * Checks whether the user is in a given role
   * @param role - One of the known roles or role from server.
   * You can also use exclamation mark for negative (e.g. "!test")
   * @returns True if user is in the given role,
   * or if given roles is null/empty.
   */
  public isInRole(role: SystemRole | Role | string): boolean {
    return RoleLogic.isInRole(this.getSession(), this.getAppStatus(), role);
  }

  /**
   * Checks if the user is in ANY of the roles.
   * @param commaSeparatedRolesList - Array of roles or comma separated string containing role names.
   * @returns True if user is in one of the given roles,
   * or if given roles is null or an empty array.
   */
  public isInSomeRole(commaSeparatedRolesList: (SystemRole | Role | string)[] | string): any {
    return RoleLogic.isInSomeRole(this.session, this.getAppStatus(), Arrays.assureArray(commaSeparatedRolesList));
  }

  /**
   * Tests if the current account has ALL the given roles.
   * @param accountRoles - Array of roles.
   * @returns Returns true if the account has all the given roles,
   * or if given roles is null or an empty array.
   */
  public isInAllRoles(accountRoles: (SystemRole | Role | string)[]): boolean {
    return RoleLogic.isInAllRoles(this.session, this.getAppStatus(), accountRoles);
  }

  /** Posts a message to the parent window. The message is sent as salaxySessionEvent  */
  public postMessageToParent(message: string) {
    if (window.parent !== window && window.parent.postMessage) {
      window.parent.postMessage({ salaxySessionEvent: message }, "*");
    }
  }

  /**
   * Checks the current session from the server if user has token (either in-memory or in cookie)
   *
   * @returns A Promise with result data (UserSession)
   */
  public checkSession(): Promise<UserSession> {
    this.isSessionChecking = true;

    let token = this.ajax.getCurrentToken();
    if (Token.validate(token) !== "ok") {
      token = null;
      this.ajax.setCurrentToken(token);
    }

    if (!token) {
      this.isSessionChecked = true;
      this.isAuthenticated = false;
      this.avatar = null;
      this.session = null;
      this.companyAccount = null;
      this.notify();
      this.isSessionChecking = false;
      return new Promise((resolve) => {
        this.$timeout(() => {
          resolve(null);
        });
      });
    }
    this.switchCss(Token.parsePayload(token).account_partner_css);

    return this.sessionApi.getSession().then((result: UserSession) => {
      this.isSessionChecking = false;
      this.isSessionChecked = true;
      if (result) {
        this.isAuthenticated = result.isAuthorized;
        this.avatar = result.avatar;
        this.session = result;
        if (this.session.isAuthorized) {
          const entityType = (result.currentAccount as AccountBase).entityType;
          if (entityType === "company") {
            this.companyAccount = this.session.currentAccount;
          } else if (entityType === "person") {
            this.personAccount = this.session.currentAccount;
          }
          this.notify();
          this.notifyAuthenticatedSession();
          this.postMessageToParent("success");
          return this.session;
        } else {
          this.notify();
          this.postMessageToParent("failure");
          return this.session;
        }
      } else {
        this.isAuthenticated = false;
        this.avatar = null;
        this.session = null;
        this.companyAccount = null;
        this.notify();
        this.postMessageToParent("failure");
        return result;
      }
    });
  }

  /**
   * Switches the current web site usage role and refreshes the session.
   * @param role - household or worker.
   * @returns A Promise with result data (new role as string)
   */
  public switchRole(role: "household" | "worker"): Promise<"household" | "worker"> {
    return this.accountApi.switchRole(role).then((result) => {
      return this.checkSession().then(() => {
        return result;
      });
    });
  }

  /**
   * Sends the browser to standard Sign-in page on Salaxy API server.
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login
   * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   * @param partnerSite Identifier of the partner or service model from which the login UI is fetched.
   * @param urlPostfix Additional string that is added to the OAuth2 URL.
   * Used in adding other parameters to the URL, e.g. "&salaxy_language=en"
   */
  public signIn(redirectUrl: string = null, role: string = null, partnerSite: string = null, urlPostfix: string = null): void {
    this.authorize(redirectUrl, role, partnerSite, OAuthSalaxyAuthorizeMode.Sign_in, urlPostfix);
  }

  /**
   * Sends the browser to standard signUp / register page on Salaxy API server.
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login
   * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
   * @param partnerSite Identifier of the certified partner which should be granted access rights in account creation.
   * Also sets SignIn dialog and onboarding wizard UI skin.
   * @param urlPostfix Additional string that is added to the OAuth2 URL.
   * Used in adding other parameters to the URL, e.g. "&salaxy_language=en"
   */
  public register(redirectUrl: string = null, role: string = null, partnerSite: string = null, urlPostfix: string = null): void {
    this.authorize(redirectUrl, role, partnerSite, OAuthSalaxyAuthorizeMode.Sign_up, urlPostfix);
  }

  /**
   * Sends the user to the Sign-out page
   *
   * @param redirectUrl - URL where user is redirected after log out.
   * Must be absolute URL. Default is the root of the current server.
   */
  public signOut(redirectUrl?: string): void {
    this.ajax.setCurrentToken(null);

    const getOrigin = () =>
        window.location.protocol +
        "//" +
        window.location.hostname +
        (window.location.port ? ":" + window.location.port : "");

    const regEx = new RegExp(/www2\./, "ig");
    redirectUrl = (redirectUrl || getOrigin()).replace(regEx, "www." );
    this.checkSession().then(() => {
      this.$window.location.href = redirectUrl;
    });
  }

  /**
   * Returns true if the current authenticated user has signed the contract.
   */
  public isSigningOk(): boolean {
    if (this.session
      && this.session.currentAccount
      && this.session.currentAccount.identity.contract
      && this.session.currentAccount.identity.contract.isSigned) {
      return true;
    }
    return false;
  }

  /**
   * Returns true if the current authenticated user has signed the contract
   * OR if the user does not have session OR if user is anonymous.
   */
  public checkAccountVerification(): boolean {
    // Anon user cannot have contract, return always true
    if (!this.session || !this.session.currentAccount) {
      return true;
    }
    // Return true if session and signing
    if (this.session.currentAccount.identity.contract
      && this.session.currentAccount.identity.contract.isSigned) {
      return true;
    }
    return false;
  }

  /**
   * Gets the application status that supports the UserSession object by providing additional
   * information about the status of fetching the session, data, expiration etc.
   */
  public getAppStatus(): AppStatus {
    // TODO: We should probably add isTestData to Ajax, but that needs changes in all environments.
    // Fetching from config should work fine in NG as is compatible with future changes.
    let isTestData: boolean = null;
    const config = Configs.current;
    if (config && config.isTestData != null) {
      isTestData = config.isTestData;
    }
    return {
      sessionCheckInProgress: this.getIsSessionChecking() || !this.getIsSessionChecked(),
      clientRoles: this.clientRoles,
      isTestData,
    };
  }

  /**
   * TODO: Encapsulate this functionality to Ui-customization service. MJ
   *
   * Switches the CSS link to partner-specific CSS.
   * @param cssUrl URL for the CSS file.
   */
  public switchCss(cssUrl: string ) {
    const linkId = "salaxyMainCss";
    const link = document.getElementById(linkId) as HTMLLinkElement;
    // If link id exists, we can customize the css
    if (link) {
      cssUrl = cssUrl || "css/skins/palkkaus.css";
      if (link.href !== cssUrl) {
        link.href = cssUrl;
      }
    }
  }

  // TODO: This method should return an array of channels
  /** Returns the owned payment channel for the current account. */
  public getOwnedPaymentChannel(): PaymentChannel {
    const channelAccounts = {
      FI03POYA0003689675 : PaymentChannel.AccountorGo, // Accountor
      FI15POYA0008369224 : PaymentChannel.FinagoSolo, // FinagoSolo
      FI06POYT0009439335 : PaymentChannel.Kevytyrittaja, // Kevytyrittaja
    };
    if (this.session && this.session.currentAccount) {
      return channelAccounts[this.session.currentAccount.id] || null;
    }
    return null;
  }

  private notify(): void {
    this.$rootScope.$emit("session-service-event");
  }

  private notifyAuthenticatedSession() {
    this.$rootScope.$emit("session-auth-session-event");
  }

  /**
   * Sends the browser to standard Sign-in page on Salaxy API server.
   *
   * @param redirectUrl The URL where the user is taken after a successfull login.
   * @param role Optional role (household, worker or company) for the situations where it is known - mainly for new users.
   * @param partnerSite The partner site is passed as client_id to the sign-in process.
   * @param mode Salaxy login mode: "sign_in" or "sign_up".
   * @param urlPostfix Additional string that is added to tha URL.
   * Used in adding other parameters to the URL, e.g. "&salaxy_language=en"
   */
  private authorize(
    redirectUrl: string = null,
    role: string = null,
    partnerSite: string = null,
    mode: OAuthSalaxyAuthorizeMode = null,
    urlPostfix: string = null,
  ): void {
    const config = Configs.current;
    const partner =
      partnerSite || this.partnerSite || config?.partnerSite || "unknown";
    const currentUrl = [
      this.$window.location.protocol,
      "//",
      this.$window.location.host,
      this.$window.location.pathname,
    ].join("");
    const successUrl = redirectUrl || currentUrl;
    let url =
      this.getServerAddress() +
      "/oauth2/authorize?response_type=token&redirect_uri=" +
      encodeURIComponent(successUrl || "") +
      "&client_id=" +
      encodeURIComponent(partner || "") +
      "&salaxy_role=" +
      encodeURIComponent(role || "") +
      "&salaxy_authorize_mode=" +
      encodeURIComponent("" + mode);
    if (urlPostfix) {
      url = url + urlPostfix;
    }
    this.$window.location.href = url;
  }

  private readTokenFromUrl(key: string, path: string): string {
    // use regex...
    const start = path.toLowerCase().indexOf(key);
    if (start >= 0) {
      const end = path.indexOf("&", start);
      return end >= 0
        ? path.substring(start + key.length, end)
        : path.substring(start + key.length);
    }
    return null;
  }

  private checkSessionFromUrl() {
    const url = this.$location.url();
    const token = this.readTokenFromUrl("access_token=", url);
    if (token) {
      this.ajax.setCurrentToken(token);
      const removeTokensFromPath = ( (tt: string[], p: string) => {
        for( const t of tt) {
            const rg = new RegExp(`\\b${t}\\=[^&]+&?`);
            p = p.replace(rg,"");
        }
        return p;
      });
      const cleanPath = (path: string) => {
        // remove trailing & and ?
        path = path.replace(/[?&]$/, "").trim();
        // remove trailing slash if not following hash, cannot use backward looking regex, not supported in Safari
        if (path.endsWith("/") && !path.endsWith("#/")) {
          path = path.replace(/\/$/, "").trim();
        }
        return path;
      }

      this.$rootScope.$evalAsync(() => {
        this.$location
        .path(cleanPath(removeTokensFromPath(["access_token","token_type", "state", "expires_in", "scope"], this.$location.path())))
        .search("access_token", null)
        .search("token_type", null)
        .search("state", null)
        .search("expires_in", null)
        .search("scope", null);
      });
      return this.checkSession();
    } else {
      const error = this.readTokenFromUrl("error=", url);
      if (error) {
        const errorDescription =
          this.readTokenFromUrl("error_description=", url) || "";
        const errorUri = this.readTokenFromUrl("error_uri=", url) || "";
        const state = this.readTokenFromUrl("state=", url) || "";
        this.signInError = decodeURIComponent(error);
        this.signInErrorDescription = decodeURIComponent(errorDescription.replace(/\+/g, "%20"));

        this.$rootScope.$evalAsync(() => {
          this.$location
            .path(this.signInErrorUrl || "/")
            .search("error", decodeURIComponent(error.replace(/\+/g, "%20")))
            .search("error_description", decodeURIComponent(errorDescription.replace(/\+/g, "%20")))
            .search("error_uri", decodeURIComponent(errorUri.replace(/\+/g, "%20")))
            .search("state", decodeURIComponent(state.replace(/\+/g, "%20")));
        });
        return this.checkSession();
      }
    }
    return Promise.resolve(null);
  }

  private subscribeToLocationChange() {
    this.$rootScope.$on("$locationChangeStart", () => {
      this.checkSessionFromUrl().then(() => {
        if (!this.isSessionChecked && !this.isSessionChecking) {
          this.checkSession();
        }
      });
    });
  }
}
