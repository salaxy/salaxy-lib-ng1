import * as angular from "angular";

import { AccountSettings, Ajax, CompanyAccountSettings } from "@salaxy/core";

import { SessionService } from "./SessionService";
import { EditDialogResult, UiHelpers } from "./ui";

/**
 * Provides singleton for the full editable settings object:
 * Mainly for editing, but may also be used by other parts that require access to settings.
 * This implementation is for Company employers. There may be a specialized Household employers and/or Worker, Pro version later.
 */
export class SettingsService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "SessionService", "AccountSettings", "UiHelpers", "AjaxNg1"];

  /**
   * The current settings object or null if the object is still being loaded.
   * The loading starts as soon as the settings service is loaded in the first controller.
   */
  public current: CompanyAccountSettings;

  constructor($rootScope: angular.IRootScopeService, private sessionService: SessionService, private accountSettings: AccountSettings, private uiHelpers: UiHelpers, private ajax: Ajax){
    if (sessionService.isSessionChecked && sessionService.isAuthenticated) {
      this.refresh();
    }
    sessionService.onAuthenticatedSession($rootScope, () => {
      this.refresh();
    });
  }

  /** Refreshes the current settings from the server. */
  public refresh(): Promise<CompanyAccountSettings> {
    return this.accountSettings.get().then((result) => {
      this.current = result;
      return result;
    });
  }

  /** Saves the current settings back to storage. */
  public save(): Promise<CompanyAccountSettings> {
    return this.accountSettings.save(this.current).then((result) => {
      this.current = result;
      return this.sessionService.checkSession().then( () => {
        return result;
      })
    });
  }

   /** Sets the eInvoice address using a dialog */
   public selectEInvoiceAddress(address: {eInvoiceReceiver?: string, eInvoiceIntermediator?: string }): Promise<EditDialogResult<any>> {
    const item = {
      receiver: address.eInvoiceReceiver,
      intermediator: address.eInvoiceIntermediator,
    };
    const logic = {
      title: "Hae osoite Y-tunnuksella",
      officialId: this.sessionService.session.currentAccount.identity.officialId,
      isLoading: false,
      addresses: [],
      isSelected: (current, item) => {
        return current.receiver === item.receiver;
      },
      isAnySelected: (current) => {
        return logic.addresses.some((x) => logic.isSelected(current, x));
      },
      setCurrent: (current, item) => {
        current.receiver = item.receiver;
        current.intermediator = item.intermediator;
      },
      search: () => {
        logic.isLoading = true;
        const path = `/payments/einvoice/receiver?officialId=${encodeURIComponent(logic.officialId)}`;
        this.ajax.getJSON(path).then((items) => {
          logic.addresses = items;
          logic.isLoading = false;
        });
      },
    };
    logic.search();
    return this.uiHelpers.openEditDialog("salaxy-components/modals/account/EInvoiceAddressList.html", item, logic).then((result) => {

      if (result.action === "ok") {
        address.eInvoiceReceiver = result.item.receiver;
        address.eInvoiceIntermediator = result.item.intermediator;
      } else if (result.action === "reset") {
        address.eInvoiceReceiver = null;
        address.eInvoiceIntermediator = null;
      }
      return result;
    });
  }
}
