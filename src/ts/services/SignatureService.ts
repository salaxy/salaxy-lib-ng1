﻿import * as angular from "angular";

import { OnboardingLogic } from "@salaxy/core";

/**
 * Defines a supported signature method.
 */
export class SignatureMethod {
    /** Title for the signing method. */
    public title?: string;
    /** Name for the signing method. */
    public name: string;
    /** Id for the signing method. */
    public value: string;
    /** Image url for the signing method. */
    public img: string;
    /** Boolean to indicate that the method is popular and should be shown among first methods. */
    public isPopular?: boolean;
}

/**
 * Handles Digital Signature scenarios to Onnistuu.fi-service.
 */
export class SignatureService {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$rootScope"];

    private methods: SignatureMethod[] = [];

    /**
     * Creates a new instance of SignatureService
     *
     * @param $rootScope - Angular root scope. Used for event routing
     */
    constructor(
        private $rootScope: angular.IRootScopeService,
    ) {

        this.methods = OnboardingLogic.getTupasMethods().map((x) => {
            return {
                id: x.id,
                title: x.title,
                name: x.id.substr(6), // for backward compatibility
                value: x.id,
                img: x.img,
                isPopular: x.isPopular,
            };
        });

    }

    /**
     * Gets a list of available signature methods. Use img-property to show
     * Note that the methods are only available after they have been loaded from the Onnistuu.fi server.
     */
    public getMethods(): SignatureMethod[] {
        return this.methods;
    }

    /**
     * Controllers can subscribe to changes in service data using this method.
     * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
     *
     * @param scope - Controller scope for the subscribing controller (or directive etc.)
     * @param listener - The event listener function. See $on documentation for details
     */
    public subscribe(scope: angular.IScope, listener: (event: angular.IAngularEvent, ...args: any[]) => any) {
        const handler = this.$rootScope.$on("signature-service-event", listener);
        scope.$on("$destroy", handler as any); // Casting to any because imperfect typing info.
    }

    private notify(): void {
        this.$rootScope.$emit("signature-service-event");
    }
}
