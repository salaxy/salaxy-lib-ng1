import { Numeric } from "@salaxy/core";

import { AjaxNg1 } from "../ajax";
import { Ng1Translations } from "./ui";

/** Abstracts the Upload process to a helper class instead of using upload directly. */
export class UploadService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Upload", "AjaxNg1", "$q", "Ng1Translations"];

  constructor(
    private angularUpload: angular.angularFileUpload.IUploadService,
    private ajax: AjaxNg1,
    private $q: angular.IQService,
    private translations: Ng1Translations,
  ) {
  }

  /**
   * Standard upload method that uploads data as form POST (multipart/form-data)
   * @param url Service URL that receives the upload.
   * @param data The form data (including the files) that is sent to the server.
   * @param notify Optional function that is called for upload notifications: Progress and error.
   * @returns Message from server as defined by IReturnType.
   */
  public upload<IReturnType>(url: string, data: any, notify?: (
    /** Upload progress is a number between 0-100 */
    progress: number,
    /** In the case of error, the english error message is returned here and progress is null. */
    error?: string,
  ) => any): angular.IPromise<IReturnType> {
    return this.$q((resolve, reject) => {
      this.angularUpload.upload<IReturnType>({
        method: "POST",
        url,
        data,
        headers: { Authorization: "Bearer " + this.ajax.getCurrentToken() },
      }).then((response) => {
        resolve(response.data);
      }, (response) => {
        response = response || {};
        const error = (response.status || "ERROR") + ": " + (response.data || "No data");
        notify(null, error);
        reject(error);
      }, (evt) => {
        const progress = Math.min(100, Numeric.round(100.0 * evt.loaded / evt.total, 0));
        if (notify) {
          notify(progress);
        }
      });
    });
  }

  /**
   * Gets a the URL (with token) for a stream of given uploaded file.
   * @param fileId Identifier of the file.
   */
  public getFileUrl(fileId: string) {
    return `${this.ajax.getApiAddress()}/files/stream/${fileId}?access_token=${this.ajax.getCurrentToken()}`;
  }

  /**
   * Gets a preview URL (with token) for a given uploaded file.
   * @param fileId Identifier of the file.
   */
  public getPreviewUrl(fileId: string) {
    return `${this.ajax.getApiAddress()}/files/preview/${fileId}?access_token=${this.ajax.getCurrentToken()}`;
  }

  /**
   * Gets a language versined error description ng-file-upload invalidFile errors (client-side before actual upload).
   * @param invalidFile Invalid file of ng-file-upload component.
   * @param invalidFile.$error Error key, e.g. 'pattern', minSize' or  'maxSize'
   * @param invalidFile.$errorParam Additional error param, e.g. '50KB' or '10MB' for file sizes.
   */
  public getInvalidFileDescription(invalidFile: {
    $error: string,
    $errorParam: string,
  }): string {
    if (!invalidFile || !invalidFile.$error) {
      return null;
    }
    // TODO: Error messages should be moved to the right path in dictionary.
    switch (invalidFile.$error) {
      case "pattern":
        return this.translations.get("SALAXY.NG1.CurrentTaxCardComponent.uploadErrorFormat");
      case "minSize":
        // $errorParam contains the size, e.g '50KB' if that is needed.
        return this.translations.get("SALAXY.NG1.CurrentTaxCardComponent.uploadErrorSmall");
      case "maxSize":
        // $errorParam contains the size, e.g '10MB' if that is needed.
        return this.translations.get("SALAXY.NG1.CurrentTaxCardComponent.uploadErrorLarge");
      default:
        return this.translations.get("SALAXY.NG1.CurrentTaxCardComponent.uploadError");
    }
  }

  /**
   * Gets a font-awesome icon for supported file types.
   * @param fileName The extension of the file name is used for determining the icon.
   */
  public getFileIcon(fileName: string): string {
    if (!fileName || fileName.indexOf(".") < 0) {
      return "fa-exclamation-triangle";
    }
    const extension = fileName.substring(fileName.lastIndexOf(".") + 1);
    switch (extension) {
      case "txt":
      case "text":
      case "csv":
      case "cs":  // If these are really used at some point, you probalby want to reconsider theses
      case "css":
      case "htm":
      case "html":
      case "js":
      case "json":
      case "xml":
        return "fa-file-text-o";
      case "bmp":
      case "gif":
      case "jpg":
      case "jpeg":
      case "png":
      case "svg":
      case "tif":
      case "tiff":
        return "fa-file-image-o";
      case "pdf":
        return "fa-file-pdf-o";
      case "xls":
      case "xlsx":
        return "fa-file-excel-o";
      case "doc":
      case "docx":
        return "fa-file-word-o";
      case "zip":
        return "fa-file-archive-o";
      case "ppt":
      case "pptx":
        return "fa-file-powerpoint-o";
      default:
        return "fa-file-o";
    }
  }
}
