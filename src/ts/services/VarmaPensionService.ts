import * as angular from "angular";

import { PartnerServices, VarmaPensionOrder } from "@salaxy/core";

import { SessionService } from "./SessionService";

/**
 * Varma pension service. Contains methods for getting order template, validating the order
 * and sending the order.
 */
export class VarmaPensionService {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$rootScope", "SessionService", "PartnerServices"];

    /** Current order  */
    private current: VarmaPensionOrder;

    constructor(
        private $rootScope: angular.IRootScopeService,
        private sessionService: SessionService,
        private partnerServicesApi: PartnerServices,
    ) {
    }

    /** Lazy loading of current pension order template */
    public getCurrent(): VarmaPensionOrder {
        if (this.current == null) {
            this.current = {
                orderer: {
                    contact: {
                    },
                },
                tyel: {},
                yel: {},
                validation: {},
            };
            if (this.sessionService.isSessionChecked && this.sessionService.isAuthenticated) {
                this.init();
            }
            this.sessionService.onAuthenticatedSession(this.$rootScope, () => {
                this.init();
            });

        }
        return this.current;
    }

    /**
     * Sends order for processing.
     */
    public send(): Promise<VarmaPensionOrder> {
        return this.partnerServicesApi.sendVarmaPensionOrder(this.current).then((order) => {
            this.current = order;
            // refresh products
            return this.sessionService.checkSession().then ( () => {
                return  this.current;
            });
        });
    }

    /**
     * Validates the order.
     */
    public validate(): Promise<VarmaPensionOrder> {
        return this.partnerServicesApi.validateVarmaPensionOrder(this.current).then((order) => {
            this.current = order;
            return this.current;
        });
    }

    /**
     * Resets the current order.
     */
    public reset() {
        this.current = null;
    }

    /**
     * Reloads pension order template from server.
     */
    private init() {
        return this.partnerServicesApi.getNewVarmaPensionOrder().then((order) => {
            this.current = order;
        });
    }
}
