import { Accounts, MessageType } from "@salaxy/core";

/**
 * Handles email/telephone verification
 */
export class VerificationService {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["Accounts"];

   /**
    * Creates a new instance of VerificationService
    */
    constructor(private accountApi: Accounts) { }

    /**
     * Sends a verification
     *
     * @param type Type of the message
     * @param address Target address
     *
     * @returns A Promise with result data (pin code in the test environment) as the success state of the request.
     */
    public sendVerification(type: MessageType, address: string): Promise<string> {
        return this.accountApi.sendVerification(type, address);
    }

    /**
     * Confirms a verification
     *
     * @param type Type of the message
     * @param pin Pin code that is verified.
     *
     * @returns A Promise with result data (boolean) as the success state of the request.
     */
    public confirmVerification(type: MessageType, pin: string): Promise<boolean> {
        return this.accountApi.confirmVerification(type, pin);
    }
}
