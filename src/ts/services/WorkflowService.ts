import { ApiCrudObject, SessionUserCredential, WorkflowEvent } from "@salaxy/core";

import {CredentialService} from "./CredentialService";
import {SessionService} from "./SessionService";

import {ApiCrudObjectController} from "../controllers/bases/ApiCrudObjectController";

/**
 * Service for workflow logic.
 * For implementation specific workflows you shoud extend this class
 * and inject the extended class as a WorkflowService.
 */
export class WorkflowService {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["SessionService", "CredentialService"];

   /**
    * Creates a new instance of WorkflowService.
    */
    constructor(protected sessionService: SessionService, protected credentialService: CredentialService) { }

  /**
   * Returns credentials for the current account.
   * Override this in the pro environment.
   */
  public get credentials(): SessionUserCredential[] {
    return this.credentialService.list;
  }

  /**
   * Returns the credential for current session.
   * Override this in the pro environment.
   */
  public get self(): SessionUserCredential {
    return this.sessionService.session.currentCredential;
  }

  /**
   * Returns the current workflow event of the given type.
   *
   * @param apiCtrl API CRUD object controller for the item.
   * @param wfType Event type of the workflow event.
   * @returns Returns the current workflow event of the given type.
   */
  public getWorkflowEvent<TItem extends ApiCrudObject>(apiCtrl: ApiCrudObjectController<TItem>, wfType: string): WorkflowEvent {
    return apiCtrl.getWorkflowEvent(wfType);
  }

  /**
   * Returns true if the workflow of the current item has an event of the given type.
   *
   * @param apiCtrl API CRUD object controller for the item.
   * @param wfType Event type of the workflow event.
   * @returns Returns true if the current item contains an event of the given type.
   */
  public hasWorkflowEvent<TItem extends ApiCrudObject>(apiCtrl: ApiCrudObjectController<TItem>, wfType: string): boolean {
    return !!this.getWorkflowEvent(apiCtrl, wfType);
  }

  /**
   * Adds/updates the workflow event for the current item.
   *
   * @param apiCtrl API CRUD object controller for the item.
   * @param wfEvent Workflow event to add/update.
   * @returns Reloaded item.
   */
  public saveWorkflowEvent<TItem extends ApiCrudObject>(apiCtrl: ApiCrudObjectController<TItem>, wfEvent: WorkflowEvent): Promise<TItem> {
    return apiCtrl.saveWorkflowEvent(wfEvent);
  }

  /**
   * Deletes the given event or all events with given type.
   *
   * @param apiCtrl API CRUD object controller for the item.
   * @param wfIdOrType Id or type of the workflow event.
   * @returns - Reloaded item.
   */
  public deleteWorkflowEvent<TItem extends ApiCrudObject>(apiCtrl: ApiCrudObjectController<TItem>, wfIdOrType: string): Promise<TItem> {
    return apiCtrl.deleteWorkflowEvent(wfIdOrType);
  }
}
