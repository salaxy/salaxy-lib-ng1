import { YearEnd, YearlyFeedback } from "@salaxy/core";

import { BaseService } from "./BaseService";
import { SessionService } from "./SessionService";

import { UiHelpers } from "./ui";

/**
 * Methods for checking yearly calculations and sending corrections.
 */
export class YearEndService extends BaseService<YearlyFeedback>  {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "SessionService", "YearEnd", "UiHelpers"];

 /**
  * String that identifies the service event (onChange/notify).
  * Must be unique for the service class.
  */
  protected eventPrefix = "yearEnd";

  /**
   * Creates a new instance of YearEndService.
   *
   * @param $rootScope - Angular root scope. Used for event routing.
   * @param sessionService - Session service notifies when the user is known to be authenticated.
   * @param yearEndApi - The Yearly Feedback API that is used to communicating with the server.
   * @param uiHelpers - Salaxy UI Helpers service.
   */
  constructor(
    $rootScope: angular.IRootScopeService,
    sessionService: SessionService,
    yearEndApi: YearEnd,
    private uiHelpers: UiHelpers,
  ) {
    super($rootScope, sessionService, yearEndApi);
  }
  /**
   * Saves changes (submit) to the current feedback
   */
  public saveCurrent(): Promise<YearlyFeedback> {
    const loading = this.uiHelpers.showLoading("Odota...");
    return super.saveCurrent().then((result) => {
      loading.dismiss();
      this.uiHelpers.showAlert("Palautteen tiedot tallennettu");
      return result;
    });
  }

   /**
    * Reloads the list from the server - called in init and e.g. after Delete and Add new
    *
    * @returns A Promise with result data
    */
  public reloadList(): Promise<YearlyFeedback[]> {
    return super.reloadList().then( (result: YearlyFeedback[]) => {
        if (!this.current && result && result.length !== 0) {
          this.setCurrent(result[0]);
        }
        return result;
    });
  }
}
