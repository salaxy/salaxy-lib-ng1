// Services from this project.
import {
  AlertService,
  AuthorizedAccountService,
  CacheService,
  CertificateService,
  CredentialService,
  InvoicesService,
  JsonSchemaService,
  NaviService,
  Ng1Translations,
  OnboardingService,
  PartnerService,
  ReportsService,
  SessionService,
  SettingsService,
  SignatureService,
  UiCrudHelpers,
  UiHelpers,
  UploadService,
  VarmaPensionService,
  VerificationService,
  WizardService,
  WorkflowService,
  YearEndService,
} from ".";

import { AjaxNg1 } from "../ajax/AjaxNg1";

// @salaxy/core services and interfaces
import {
  Absences,
  AccountingTargets,
  Accounts,
  AccountSettings,
  AuthorizedAccounts,
  BeneficialOwnersApi,
  Calculations,
  Calculator,
  CalendarEvents,
  CalendarOccurences,
  Certificates,
  Client,
  Credentials,
  EmploymentContracts,
  Employments,
  Files,
  HolidayYears,
  Invoices,
  IrEarningsPayments,
  IrPayerSummaries,
  MessageThreads,
  OAuth2,
  Onboardings,
  Overview,
  PartnerServices,
  PaymentChannelApi,
  Payrolls,
  PrimaryPartners,
  Reports,
  Session,
  Taxcards,
  Test,
  Workers,
  YearEnd,
} from "@salaxy/core";

// @salaxy/report services
import {
  Templates,
} from "@salaxy/reports";

/** Provides methods for registering the Services to module
 * (and other related metadata in the future).
 */
export class ServicesRegistration {

  /** Gets the services for Module registration. */
  public static getServices () {
    return {
      AjaxNg1,
      AlertService,
      AuthorizedAccountService,
      CacheService,
      CertificateService,
      CredentialService,
      InvoicesService,
      JsonSchemaService,
      NaviService,
      Ng1Translations,
      OnboardingService,
      PartnerService,
      ReportsService,
      SessionService,
      SettingsService,
      SignatureService,
      UiCrudHelpers,
      UiHelpers,
      UploadService,
      VarmaPensionService,
      VerificationService,
      WizardService,
      WorkflowService,
      YearEndService,
    };
  }

  /** Gets the services from the @salaxy/core project that need to be registered for NG1 dependency injection. */
  public static getCoreServices () {
    return {
      Absences,
      AccountingTargets,
      Accounts,
      AccountSettings,
      AuthorizedAccounts,
      BeneficialOwnersApi,
      Calculations,
      Calculator,
      CalendarEvents,
      CalendarOccurences,
      Certificates,
      Client,
      Credentials,
      EmploymentContracts,
      Employments,
      Files,
      HolidayYears,
      Invoices,
      IrEarningsPayments,
      IrPayerSummaries,
      MessageThreads,
      OAuth2,
      Onboardings,
      Overview,
      PartnerServices,
      PaymentChannelApi,
      Payrolls,
      PrimaryPartners,
      Reports,
      Session,
      Taxcards,
      Templates,
      Test,
      Workers,
      YearEnd,
    };
  }
}
