﻿import { AjaxNg1 } from "../../ajax";

/**
 * Handles error, warning and debug alerts and other such error handling situations.
 * Default functionality is to find #salaxy-alert-container and insert a bootstrap alert box into that container.
 */
export class AlertService {

    /**
     * For NG-dependency injection
     * @ignore
     */
    public static $inject = ["$rootScope", "$timeout", "$sce"];

    /** Default duration for alerts to be visible */
    public duration = 10;

    /** Service level counter of alerts. Used for identifier. */
    private alertCounter = 0;

    private alerts: {
      /** Id for the alert. */
      id: number,
      /** Message of the alert. */
      message: string,
      /** Type of the alert. */
      alertType: string,
    }[] = [];

    private eventPrefix = "alert";

    /** Dependency injection etc. */
    constructor(private $rootScope: angular.IRootScopeService, private $timeout: angular.ITimeoutService, private $sce: angular.ISCEService) {
        this.init();
    }

    /**
     * Adds an error message
     * @param message Message to add
     */
    public addError(message: string): void { this.addAlert(message, "danger"); }

    /**
     * Adds a warning message
     * @param message Message to add
     */
    public addWarning(message: string): void { this.addAlert(message, "warning"); }

    /**
     * Adds a success message
     * @param message Message to add
     */
    public addSuccess(message: string): void { this.addAlert(message, "success"); }

    /**
     * Adds an info message
     * @param message Message to add
     */
    public addInfo(message: string): void { this.addAlert(message, "info"); }

    /**
     * Last message - consider this a temporary implementation of the custom alert handling
     *
     * @returns Last message
     */
    public getLastMessage(): string {
        return this.alerts[this.alerts.length - 1].message || null;
    }

    /**
     * Last alertType is bootstrap type: info (default), success, warning or danger.
     * Consider this a temporary implementation of the custom alert handling
     *
     * @returns Last alert type
     */
    public getLastAlertType(): string {
        return this.alerts[this.alerts.length - 1].alertType || null;
    }

    /**
     * INTERNAL ONLY: This functionality is under consideration.
     * We may not support it in the future and we may remove it without it being a breaking change.
     *
     * Controllers can subscribe to changes in service data using this method.
     * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
     *
     * @ignore
     *
     * @param scope - Controller scope for the subscribing controller (or directive etc.)
     * @param callback - The event listener function. See $on documentation for details
     */
    public onChange(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
        const handler = this.$rootScope.$on(this.eventPrefix + "-service-event", callback);
        scope.$on("$destroy", handler);
    }

    /**
     * Attaches the alert service to AjaxNg1.
     * The init is currently called automatically in constructor, but here you can make an explicit call.
     */
    public init() {
      AjaxNg1.alertService = this;
    }

    /**
     * Adds an alert message
     *
     * @param message - A message text for the Alert. Preferably a short text so that it fits to the pull-righ box.
     * @param alertType - Bootstrap color code for the alert info (default), success, warning or danger
     */
    private addAlert(message: string, alertType: "info" | "success" | "warning" | "danger"): void {

        const alert = {
            message,
            alertType: alertType || "info",
            id: this.alertCounter++,
        };

        this.alerts.push(alert);
        this.notify();
        this.renderAlerts();

        this.$timeout(() => {
            this.alerts.splice(0, 1);
            this.notify();
            this.renderAlerts();
        }, 1000 * this.duration);
    }

    private renderAlerts() {
        const container = document.getElementById("salaxy-alert-container");
        if (!container) {
            return;
        }

        const hiddenAlerts = [];
        const currentList = container.children;
        for (let i = 0; i < currentList.length; i++) {
            if (currentList[i].classList.contains("hide")) {
                hiddenAlerts.push(currentList[i].id);
            }
        }

        let html = "";
        const length = this.alerts.length;
        for (let i = length; i-- !== 0;) {
            const alert = this.alerts[i];
            const alertElementId = "salaxy-alert-id-" + alert.id;
            if (hiddenAlerts.find((x) => x === alertElementId)) {
                continue;
            }

            html += `<div id="${alertElementId}" class="alert alert-${alert.alertType} alert-dismissible" role="alert">
            ${this.$sce.getTrustedHtml(alert.message)}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="document.getElementById('${alertElementId}').classList.toggle('hide');">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>`;
        }
        container.innerHTML = html;
    }

    private notify(): void {
        this.$rootScope.$emit(this.eventPrefix + "-service-event");
    }
}
