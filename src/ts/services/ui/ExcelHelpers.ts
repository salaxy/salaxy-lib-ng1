import JSZip from "jszip";
import { saveAs } from "file-saver";

import { Dates, Texts } from "@salaxy/core";

/**
 * Copied and modified from https://github.com/egeriis/zipcelx#readme
 */
class Zipcelx {
  private CELL_TYPE_STRING = "string";
  private CELL_TYPE_NUMBER = "number";
  private CELL_TYPE_DATE = "date";
  private CELL_TYPE_BOOLEAN = "boolean";

  private MISSING_KEY_FILENAME = "Zipclex config missing property filename";
  private INVALID_TYPE_FILENAME = "Zipclex filename can only be of type string";
  private INVALID_TYPE_SHEET = "Zipcelx sheet data is not of type array";
  private INVALID_TYPE_SHEET_DATA = "Zipclex sheet data childs is not of type array";

  private stylesXML = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" >
    <fonts count="1">
      <font>
        <sz val="11" />
        <color theme="1" />
        <name val="Calibri" />
        <family val="2" />
        <scheme val="minor" />
      </font>
    </fonts>
    <fills count="2">
      <fill>
        <patternFill patternType="none" />
      </fill>
      <fill>
        <patternFill patternType="gray125" />
      </fill>
    </fills>
    <borders count="1">
      <border>
        <left />
        <right />
        <top />
        <bottom />
        <diagonal />
      </border>
    </borders>
    <cellStyleXfs count="1">
      <xf numFmtId="0" fontId="0" fillId="0" borderId="0" />
    </cellStyleXfs>
    <cellXfs count="2">
      <xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0" />
      <xf numFmtId="14" fontId="0" fillId="0" borderId="0" xfId="0" applyNumberFormat="1" />
    </cellXfs>
    <cellStyles count="1">
      <cellStyle name="Normal" xfId="0" builtinId="0" />
    </cellStyles>
  </styleSheet>
  `;
  private workbookXML = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
    <sheets>
      <sheet name="Sheet1" sheetId="1" r:id="rId1" />
    </sheets>
  </workbook>`;
  private workbookXMLRels = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
    <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml" />
    <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/sheet1.xml" />
  </Relationships>`;
  private rels = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
    <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="xl/workbook.xml" />
  </Relationships>`;
  private contentTypes = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">
    <Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml" />
    <Default Extension="xml" ContentType="application/xml" />
    <Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml" />
    <Override PartName="/xl/worksheets/sheet1.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml" />
    <Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml" />
  </Types>`;
  private templateSheet = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
             xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
    <cols>{cols}</cols>
    <sheetData>{rows}</sheetData>
  </worksheet>`;

  /**
   * Generates excel blob from the configuration
   */
  public generateExcel = (config): Promise<any> => {
    if (!this.validator(config)) {
      throw new Error("Validation failed.");
    }

    const zip = new JSZip();
    const xl = zip.folder("xl");
    xl.file("workbook.xml", this.workbookXML);
    xl.file("styles.xml", this.stylesXML);
    xl.file("_rels/workbook.xml.rels", this.workbookXMLRels);
    zip.file("_rels/.rels", this.rels);
    zip.file("[Content_Types].xml", this.contentTypes);

    const worksheet = this.generateXMLWorksheet(config.sheet.data);
    xl.file("worksheets/sheet1.xml", worksheet);

    return zip.generateAsync({
      type: "blob",
      mimeType:
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    });
  }

  private childValidator = (array) => {
    return array.every( (item) => Array.isArray(item));
  }

  private validator = (config) => {
    if (!config.filename) {
      console.error(this.MISSING_KEY_FILENAME);
      return false;
    }

    if (typeof config.filename !== "string") {
      console.error(this.INVALID_TYPE_FILENAME);
      return false;
    }

    if (!Array.isArray(config.sheet.data)) {
      console.error(this.INVALID_TYPE_SHEET);
      return false;
    }

    if (!this.childValidator(config.sheet.data)) {
      console.error(this.INVALID_TYPE_SHEET_DATA);
      return false;
    }

    return true;
  }

  private generateColumnLetter = (colIndex) => {
    if (typeof colIndex !== "number") {
      return "";
    }

    const prefix = Math.floor(colIndex / 26);
    const letter = String.fromCharCode(97 + (colIndex % 26)).toUpperCase();
    if (prefix === 0) {
      return letter;
    }
    return this.generateColumnLetter(prefix - 1) + letter;
  }

  private generatorCellNumber = (index, rowNumber) => (
    `${this.generateColumnLetter(index)}${rowNumber}`
  )

  private generatorStringCell = (index, value, rowIndex) => (`<c r="${this.generatorCellNumber(index, rowIndex)}" t="inlineStr"><is><t>${Texts.escapeHtml(value)}</t></is></c>`);
  private generatorNumberCell = (index, value, rowIndex) => (`<c r="${this.generatorCellNumber(index, rowIndex)}" t="n"><v>${value}</v></c>`);
  private generatorDateCell =  (index, value, rowIndex) => (`<c r="${this.generatorCellNumber(index, rowIndex)}" t="d" s="1"><v>${value}</v></c>`);
  private generatorBooleanCell = (index, value, rowIndex) => (`<c r="${this.generatorCellNumber(index, rowIndex)}" t="b"><v>${value}</v></c>`);
  private generatorEmptyCell = (index, value, rowIndex) => (`<c r="${this.generatorCellNumber(index, rowIndex)}"><v></v></c>`);

  private formatCell = (cell, index, rowIndex) => {
    return (
      cell.type === this.CELL_TYPE_STRING ?
         this.generatorStringCell(index, cell.value, rowIndex) :
      cell.type === this.CELL_TYPE_NUMBER ?
         this.generatorNumberCell(index, cell.value, rowIndex) :
      cell.type === this.CELL_TYPE_DATE ?
         this.generatorDateCell(index, cell.value, rowIndex) :
      cell.type === this.CELL_TYPE_BOOLEAN ?
         this.generatorBooleanCell(index, cell.value, rowIndex) :
         this.generatorEmptyCell(index, cell.value, rowIndex)
    );
  }

  private formatRow = (row, index) => {
    // To ensure the row number starts as in excel.
    const rowIndex = index + 1;
    const rowCells = row
      .map((cell, cellIndex) => this.formatCell(cell, cellIndex, rowIndex))
      .join("");

    return `<row r="${rowIndex}">${rowCells}</row>`;
  }

  private generatorRows = (rows) => (
    rows
      .map((row, index) => this.formatRow(row, index))
      .join("")
  )

  private formatCol = (col, index) => {
    // To ensure the row number starts as in excel.
    const colIndex = index + 1;
    return `<col min="${colIndex}" max="${colIndex}" width="30" customWidth="1"/>`;
  }

  private generatorCols = (cols) => (
    cols
      .map((col, index) => this.formatCol(col, index))
      .join("")
  )

  private generateXMLWorksheet = (rows) => {
    const xmlCols = this.generatorCols(rows.length > 0 ? rows[0] : []);
    const xmlRows = this.generatorRows(rows);
    return this.templateSheet.replace("{rows}", xmlRows).replace("{cols}", xmlCols);
  }
}

/** Utility service for generating excel files. */
export class ExcelHelpers {

  /**
   * Converts a data table to an excel file.
   *
   * @param tableData Array of rows to convert to an excel file. Each row consists of an array of cell values.
   * @param fileName Name for the excel file without the file extension.
   */
  public static export(tableData: any[], fileName: string): Promise<void> {
    const rowMapper = this.getRowMapper();
    const config = {
      filename: fileName,
      sheet: {
        data: tableData.map(rowMapper),
      },
    };
    return this.zipcelx.generateExcel(config).then( (blob) => {
      saveAs(blob, `${config.filename}.xlsx`);
      return;
    });
  }

  /**
   * Converts multiple data tables to a zip file containing excel files.
   *
   * @param tablesData Multiple data tables. Each table contains an array of rows to convert to a excel file. Each row consists of an array of cell values.
   * @param zipName Name for the zip file without the file extension.
   */
  public static exportMany(
    tablesData: {
      /** Array of rows to convert to an excel file. Each row consists of an array of cell values. */
      tableData: any[],
      /** Name for the excel file without the file extension. */
      fileName: string,
    }[],
    zipName: string): Promise<void> {

    const rowMapper = this.getRowMapper();
    let counter = 0;
    const zip = new JSZip();
    const next = (): Promise<void> => {
      if (counter === tablesData.length) {
        return Promise.resolve();
      }
      const item = tablesData[counter++];
      const config = {
        filename: item.fileName,
        sheet: {
          data: item.tableData.map(rowMapper),
        },
      };
      return this.zipcelx.generateExcel(config).then( (blob) => {
        zip.file(`${config.filename}.xlsx`, blob);
        return next();
      });
    };

    return next().then ( () => {
      return zip.generateAsync({
        type: "blob",
        mimeType:
          "application/zip",
      }).then( (blob) => {
        saveAs(blob, `${zipName}.zip`);
        return;
      });
    });
  }

  private static zipcelx = new Zipcelx();

  private static getRowMapper() {
    const fieldMapper = this.getFieldMapper();
    return (row) => {
      return row.map(fieldMapper);
    };
  }

  private static getFieldMapper() {
    return (value) => {
      if (value == null) { // we want to catch anything null-ish, hence just == not ===
        return { value, type: null };
      }
      if (typeof (value) === "number") {
        return { value, type: "number" };
      }
      if (typeof (value) === "boolean") {
        return { value: (value ? "true" : "false"), type: "boolean" };
      }
      if (typeof (value) === "string") {
        return { value: value.replace(/"/g, '""'), type: "string" };
      }
      if (typeof (value) === "object" && (value instanceof Date)) {
        return { value: Dates.asMoment(value).format("YYYY-MM-DD"), type: "date" };
      }

      return { value: JSON.stringify(value), type: "string" };
    };
  }
}
