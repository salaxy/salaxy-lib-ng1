/* eslint-disable no-bitwise */

/** Copied and modified from https://css-tricks.com/snippets/javascript/javascript-md5 */
export class GravatarHelpers {

  /** Create gravatar url from the given email. */
  public static getGravatarUrl(email: string): string {
    return `https://www.gravatar.com/avatar/${this.md5((email + "").trim().toLowerCase())}?d=identicon`;
  }

  /** Returns true if the url is gravatar url. */
  public static isGravatarUrl(url: string): boolean {
    return (url + "").trim().toLowerCase().startsWith("https://www.gravatar.com/avatar/");
  }

  /**
   * Calculates md5.
   */
  private static md5(str: string): string {
    const rotateLeft = (lValue, iShiftBits) => {
      return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    };
    const addUnsigned = (lX, lY) => {
      const lX8 = (lX & 0x80000000);
      const lY8 = (lY & 0x80000000);
      const lX4 = (lX & 0x40000000);
      const lY4 = (lY & 0x40000000);
      const lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
      if (lX4 & lY4) {
        return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
      }
      if (lX4 | lY4) {
        if (lResult & 0x40000000) {
          return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
        } else {
          return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
        }
      } else {
        return (lResult ^ lX8 ^ lY8);
      }
    };
    const f = (x, y, z) => (x & y) | ((~x) & z);
    const g = (x, y, z) => (x & z) | (y & (~z));
    const h = (x, y, z) => (x ^ y ^ z);
    const i = (x, y, z) => (y ^ (x | (~z)));
    const ff = (a, b, c, d, x, s, ac) => {
      a = addUnsigned(a, addUnsigned(addUnsigned(f(b, c, d), x), ac));
      return addUnsigned(rotateLeft(a, s), b);
    };
    const gg = (a, b, c, d, x, s, ac) => {
      a = addUnsigned(a, addUnsigned(addUnsigned(g(b, c, d), x), ac));
      return addUnsigned(rotateLeft(a, s), b);
    };
    const hh = (a, b, c, d, x, s, ac) => {
      a = addUnsigned(a, addUnsigned(addUnsigned(h(b, c, d), x), ac));
      return addUnsigned(rotateLeft(a, s), b);
    };
    const ii = (a, b, c, d, x, s, ac) => {
      a = addUnsigned(a, addUnsigned(addUnsigned(i(b, c, d), x), ac));
      return addUnsigned(rotateLeft(a, s), b);
    };
    const convertToWordArray = (str) => {
      let lWordCount;
      const lMessageLength = str.length;
      const lNumberOfWordsTemp1 = lMessageLength + 8;
      const lNumberOfWordsTemp2 = (lNumberOfWordsTemp1 - (lNumberOfWordsTemp1 % 64)) / 64;
      const lNumberOfWords = (lNumberOfWordsTemp2 + 1) * 16;
      const lWordArray = Array(lNumberOfWords - 1);
      let lBytePosition = 0;
      let lByteCount = 0;
      while (lByteCount < lMessageLength) {
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
        lByteCount++;
      }
      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
      lBytePosition = (lByteCount % 4) * 8;
      lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
      lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
      lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
      return lWordArray;
    };
    const wordToHex = (lValue) => {
      let wordToHexValue = "";
      let wordToHexValueTemp = "";
      let lByte;
      let lCount;
      for (lCount = 0; lCount <= 3; lCount++) {
        lByte = (lValue >>> (lCount * 8)) & 255;
        wordToHexValueTemp = "0" + lByte.toString(16);
        wordToHexValue = wordToHexValue + wordToHexValueTemp.substr(wordToHexValueTemp.length - 2, 2);
      }
      return wordToHexValue;
    };
    const utf8Encode = (str) => {
      str = str.replace(/\r\n/g, "\n");
      let utftext = "";
      for (let n = 0; n < str.length; n++) {
        const c = str.charCodeAt(n);
        if (c < 128) {
          utftext += String.fromCharCode(c);
        } else if ((c > 127) && (c < 2048)) {
          utftext += String.fromCharCode((c >> 6) | 192);
          utftext += String.fromCharCode((c & 63) | 128);
        } else {
          utftext += String.fromCharCode((c >> 12) | 224);
          utftext += String.fromCharCode(((c >> 6) & 63) | 128);
          utftext += String.fromCharCode((c & 63) | 128);
        }
      }
      return utftext;
    };

    let aa;
    let bb;
    let cc;
    let dd;
    let a;
    let b;
    let c;
    let d;
    const s11 = 7;
    const s12 = 12;
    const s13 = 17;
    const s14 = 22;
    const s21 = 5;
    const s22 = 9;
    const s23 = 14;
    const s24 = 20;
    const s31 = 4;
    const s32 = 11;
    const s33 = 16;
    const s34 = 23;
    const s41 = 6;
    const s42 = 10;
    const s43 = 15;
    const s44 = 21;

    str = utf8Encode(str);
    const x = convertToWordArray(str);
    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
    for (let k = 0; k < x.length; k += 16) {
      aa = a; bb = b; cc = c; dd = d;
      a = ff(a, b, c, d, x[k + 0], s11, 0xD76AA478);
      d = ff(d, a, b, c, x[k + 1], s12, 0xE8C7B756);
      c = ff(c, d, a, b, x[k + 2], s13, 0x242070DB);
      b = ff(b, c, d, a, x[k + 3], s14, 0xC1BDCEEE);
      a = ff(a, b, c, d, x[k + 4], s11, 0xF57C0FAF);
      d = ff(d, a, b, c, x[k + 5], s12, 0x4787C62A);
      c = ff(c, d, a, b, x[k + 6], s13, 0xA8304613);
      b = ff(b, c, d, a, x[k + 7], s14, 0xFD469501);
      a = ff(a, b, c, d, x[k + 8], s11, 0x698098D8);
      d = ff(d, a, b, c, x[k + 9], s12, 0x8B44F7AF);
      c = ff(c, d, a, b, x[k + 10], s13, 0xFFFF5BB1);
      b = ff(b, c, d, a, x[k + 11], s14, 0x895CD7BE);
      a = ff(a, b, c, d, x[k + 12], s11, 0x6B901122);
      d = ff(d, a, b, c, x[k + 13], s12, 0xFD987193);
      c = ff(c, d, a, b, x[k + 14], s13, 0xA679438E);
      b = ff(b, c, d, a, x[k + 15], s14, 0x49B40821);
      a = gg(a, b, c, d, x[k + 1], s21, 0xF61E2562);
      d = gg(d, a, b, c, x[k + 6], s22, 0xC040B340);
      c = gg(c, d, a, b, x[k + 11], s23, 0x265E5A51);
      b = gg(b, c, d, a, x[k + 0], s24, 0xE9B6C7AA);
      a = gg(a, b, c, d, x[k + 5], s21, 0xD62F105D);
      d = gg(d, a, b, c, x[k + 10], s22, 0x2441453);
      c = gg(c, d, a, b, x[k + 15], s23, 0xD8A1E681);
      b = gg(b, c, d, a, x[k + 4], s24, 0xE7D3FBC8);
      a = gg(a, b, c, d, x[k + 9], s21, 0x21E1CDE6);
      d = gg(d, a, b, c, x[k + 14], s22, 0xC33707D6);
      c = gg(c, d, a, b, x[k + 3], s23, 0xF4D50D87);
      b = gg(b, c, d, a, x[k + 8], s24, 0x455A14ED);
      a = gg(a, b, c, d, x[k + 13], s21, 0xA9E3E905);
      d = gg(d, a, b, c, x[k + 2], s22, 0xFCEFA3F8);
      c = gg(c, d, a, b, x[k + 7], s23, 0x676F02D9);
      b = gg(b, c, d, a, x[k + 12], s24, 0x8D2A4C8A);
      a = hh(a, b, c, d, x[k + 5], s31, 0xFFFA3942);
      d = hh(d, a, b, c, x[k + 8], s32, 0x8771F681);
      c = hh(c, d, a, b, x[k + 11], s33, 0x6D9D6122);
      b = hh(b, c, d, a, x[k + 14], s34, 0xFDE5380C);
      a = hh(a, b, c, d, x[k + 1], s31, 0xA4BEEA44);
      d = hh(d, a, b, c, x[k + 4], s32, 0x4BDECFA9);
      c = hh(c, d, a, b, x[k + 7], s33, 0xF6BB4B60);
      b = hh(b, c, d, a, x[k + 10], s34, 0xBEBFBC70);
      a = hh(a, b, c, d, x[k + 13], s31, 0x289B7EC6);
      d = hh(d, a, b, c, x[k + 0], s32, 0xEAA127FA);
      c = hh(c, d, a, b, x[k + 3], s33, 0xD4EF3085);
      b = hh(b, c, d, a, x[k + 6], s34, 0x4881D05);
      a = hh(a, b, c, d, x[k + 9], s31, 0xD9D4D039);
      d = hh(d, a, b, c, x[k + 12], s32, 0xE6DB99E5);
      c = hh(c, d, a, b, x[k + 15], s33, 0x1FA27CF8);
      b = hh(b, c, d, a, x[k + 2], s34, 0xC4AC5665);
      a = ii(a, b, c, d, x[k + 0], s41, 0xF4292244);
      d = ii(d, a, b, c, x[k + 7], s42, 0x432AFF97);
      c = ii(c, d, a, b, x[k + 14], s43, 0xAB9423A7);
      b = ii(b, c, d, a, x[k + 5], s44, 0xFC93A039);
      a = ii(a, b, c, d, x[k + 12], s41, 0x655B59C3);
      d = ii(d, a, b, c, x[k + 3], s42, 0x8F0CCC92);
      c = ii(c, d, a, b, x[k + 10], s43, 0xFFEFF47D);
      b = ii(b, c, d, a, x[k + 1], s44, 0x85845DD1);
      a = ii(a, b, c, d, x[k + 8], s41, 0x6FA87E4F);
      d = ii(d, a, b, c, x[k + 15], s42, 0xFE2CE6E0);
      c = ii(c, d, a, b, x[k + 6], s43, 0xA3014314);
      b = ii(b, c, d, a, x[k + 13], s44, 0x4E0811A1);
      a = ii(a, b, c, d, x[k + 4], s41, 0xF7537E82);
      d = ii(d, a, b, c, x[k + 11], s42, 0xBD3AF235);
      c = ii(c, d, a, b, x[k + 2], s43, 0x2AD7D2BB);
      b = ii(b, c, d, a, x[k + 9], s44, 0xEB86D391);
      a = addUnsigned(a, aa);
      b = addUnsigned(b, bb);
      c = addUnsigned(c, cc);
      d = addUnsigned(d, dd);
    }

    const temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

    return temp.toLowerCase();
  }

}
