import { OpenAPIV3 } from "openapi-types";

import { InputMetadata, JsonInputType, JsonSchemaCache, JsonSchemaCacheItem, JsonSchemaProperty, JsonSchemaUtils } from "@salaxy/core";

import { AjaxNg1 } from "../../ajax";


/**
 * Service for caching JSON schemas and creating user interfaces based on them.
 */
export class JsonSchemaService {
  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  private cache: JsonSchemaCache;

  constructor(private ajax: AjaxNg1) {
    this.cache = new JsonSchemaCache(ajax);
  }

  /**
   * Assures that a schema document identified by URL is loaded from server to the cache
   * Currently, supports only OpenApi 3, but we may later support OpenApi 2 and/or plain JSON schema.
   * @param openApiUrl URL to the open API document.
   * @returns A promise that resolves when the document has been loaded and you can call other methods on it.
   */
  public assureSchemaDocument (openApiUrl: string): Promise<OpenAPIV3.Document> {
    return this.cache.assureSchemaDocument(this.getAbsoluteUrl(openApiUrl));
  }

  /**
   * Finds a schema document from the cache.
   * This method is syncronous base method for other schema operations
   * Make sure that the schme document has been loaded before calling as this will not load the document,
   * but will fail instead if the schema document is not there.
   * @param openApiUrl URL to the open API document.
   * @param throwIfNotFound If true, will throw an error if the document is not found in the cache.
   */
  public findSchemaDoc (openApiUrl: string, throwIfNotFound: boolean): JsonSchemaCacheItem {
    return this.cache.findSchemaDoc(this.getAbsoluteUrl(openApiUrl), throwIfNotFound);
  }

  /**
   * Finds a single schema (Data model) within a schema document
   * Make sure that the schme document has been loaded before calling as this will not load the document,
   * but will fail instead if the schema document is not there.
   * @param openApiUrl URL to the open API document.
   * @param name Name of the schema (data model)
   */
  public findSchema (openApiUrl: string, name: string): OpenAPIV3.SchemaObject | null {
    return this.cache.findSchema(this.getAbsoluteUrl(openApiUrl), name);
  }

  /**
   * Gets a property from a schema document. Supports single property names or longer property paths.
   * @param openApiUrl URL to the open API document.
   * @param schemaName The root type from which the property is found.
   * @param path Property path starting with the root schema.
   */
  public getProperty (openApiUrl: string, schemaName: string, path: string): JsonSchemaProperty {
    const schema = this.findSchema(this.getAbsoluteUrl(openApiUrl), schemaName);
    return JsonSchemaUtils.getProperty(schema, path);
  }

  /**
   * Registers a new custom input for type/format combination
   * @param type JSON type
   * @param format Format in the JSON schema
   * @param template Template for the type/foramt combination.
   * Special key "sxy" can be used to set default templates based on type/format,
   * e.g. ("boolean", "default", "sxy") => "salaxy-components/sxy-form/boolean/default.html".
   */
  public registerInput (type: JsonInputType, format: "default" | string, template: "sxy" | string) {
    this._registeredInputs[type][format] = (template === "sxy" ?
      `salaxy-components/sxy-form/${type}/${format}.html` : template);
  }

  /** Gets the AngularJS tempalte for the input. */
  public getTemplate (input: InputMetadata): string {
    const type = this._registeredInputs[input.type];
    if (!type) {
      input.content = `Unregistered type: ${input.type}`;
      return 'salaxy-components/sxy-form/error/default.html';
    }
    if (type[input.format]) {
      return type[input.format].template === "sxy" ? `salaxy-components/sxy-form/${input.type}/${input.format}.html` : type[input.format].template;
    }
    return `salaxy-components/sxy-form/${input.type}/${(input.isEnum ? "enum" : "default")}.html`
  }

  /**
   * Maps the types and formats to templates.
   */
  private _registeredInputs = {
    "array": {
      "default": {
        template: 'sxy'
      },
    },
    "boolean": {
      "default": {
        template: 'sxy'
      },
      "select": {
        template: 'salaxy-components/sxy-form/boolean/default.html'
      },
      "radio": {
        template: 'sxy'
      },
      "checkbox": {
        template: 'sxy'
      },
      "switch": {
        template: 'sxy'
      },
    },
    "integer": {
      "default": {
        template: 'sxy'
      },
    },
    "number": {
      "default": {
        template: 'sxy'
      },
      "count": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
      "percent": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
      "days": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
      "kilometers": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
      "hours": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
      "undefined": {
        template: 'salaxy-components/sxy-form/number/default.html'
      },
    },
    "object": {
      "default": {
        template: 'sxy'
      },
      "Avatar": {
        template: 'sxy'
      },
      "DateRange": {
        template: 'sxy'
      },
    },
    "string": {
      "default": {
        template: 'sxy'
      },
      "multiline": {
        template: 'sxy'
      },
      "email": {
        template: 'sxy'
      },
      "password": {
        template: 'sxy'
      },
      "telephone": {
        template: 'sxy'
      },
      "date": {
        template: 'sxy'
      },
      "date-time": {
        template: 'sxy'
      },
      "enum": {
        template: 'sxy'
      },
      "radio": {
        template: 'salaxy-components/sxy-form/string/enum.html'
      },
      "typeahead": {
        template: 'salaxy-components/sxy-form/string/enum.html'
      },
      "list": {
        template: 'salaxy-components/sxy-form/string/enum.html'
      },
      "pension-contract-number": {
        template: 'salaxy-components/sxy-form/string/pension-contract-number.html',
      }
    },
  }

  private getAbsoluteUrl (url: string): string {
    if (url.startsWith("/")) {
      const urlArr = window.location.href.split("/");
      url = `${urlArr[0]}//${urlArr[2]}${url}`;
    } else {
      url = url.replace(/^salaxy-secure\//g, `${this.ajax.getServerAddress()}/`);
    }
    return url;
  }
}
