import * as angular from "angular";

import { SessionService } from "./../SessionService";
import { SalaxySitemapNode } from "./model";
import { SitemapHelper } from "../../helpers";

/**
 * Helper service to generate navigation controls:
 * Top- and side-menus, paths and controls that show the current title.
 * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
 * NOTE: This is just an optional helper to make creating simple demo sites easier.
 * There is no need to use NaviService, NaviController or components in your custom site!
 * You can use something completely different.
 */
export class NaviService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "$location", "SessionService", "$injector"];

  /** Section where the current node is located. */
  public currentSection: SalaxySitemapNode;

  /** Currently selected sitemap node. */
  public currentNode: SalaxySitemapNode;

  /** Current path from root to the current node. */
  public currentPath: SalaxySitemapNode[];

  /** Title of the section (first level in the sitemap) */
  public sectionTitle: string;

  /** Title of the current sitemap node */
  public title: string;

  /**
   * True if the navigation (e.g. left sidebar) is open.
   * Typically, this property is used only in narrow (mobile) view, otherwise the sidebar is always shown.
   */
  public isNaviOpen = false;

  /**
   * True if the secondary sidebar (e.g. right sidebar) is open.
   */
  public isSidebarOpen = false;

  /** Sitemap that describes the structure of this application: For all roles. */
  private sitemapForAllRoles: SalaxySitemapNode[];

  /** Sitemap filtered for the soles of the current user. */
  private sitemapInRole: SalaxySitemapNode[];

  /** Cache for whether the system is using test data. */
  private isTest: boolean;

  constructor(
    $rootScope: angular.IRootScopeService,
    private $location: angular.ILocationService,
    private sessionService: SessionService,
    $injector: angular.auto.IInjectorService,
  ) {
    if ($injector.has("SITEMAP")) {
      this.setSitemap($injector.get("SITEMAP"));
    } else {
      this.setSitemap([]);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    $rootScope.$on("$routeChangeSuccess", (event, current, previous) => {
      this.resolveAndSetCurrentNode();
    });
    sessionService.subscribe($rootScope, () => {
      this.updateSitemapInRole();
    });
  }

  /**
   * Gets the current sitemap.
   * @returns Sitemap filtered for the roles of the current user.
   *
   */
  public getSitemap(): SalaxySitemapNode[] {
    return this.sitemapInRole;
  }

  /**
   * Sets the sitemap any time during the application lifecycle.
   * Alternative way is to set angular constant "SITEMAP", but that must be done before config.
   * @param newSitemap New sitemap to use.
   */
  public setSitemap(newSitemap: SalaxySitemapNode[]) {
    this.sitemapForAllRoles = SitemapHelper.setSitemapTitles(newSitemap);
    this.updateSitemapInRole();
  }

  /**
   * Gets the current section: The first level node in the sitemap
   * @returns Section filtered by the roles.
   */
  public getCurrentSection(): SalaxySitemapNode {
    return this.currentSection;
  }

  /** Determines if a given node is the current node */
  public isCurrent(siteMapNode: SalaxySitemapNode): boolean {
    if (!siteMapNode || !this.currentPath) {
      return false;
    }
    return !!this.currentPath.find((x) => x === siteMapNode);
    // return !!this.currentPath.find((x) => x.url && x.url === siteMapNode.url);
  }

  /** Determines if a given section is the current section */
  public isCurrentSection(section: SalaxySitemapNode): boolean {
    return this.currentSection && section && this.currentSection.url === section.url;
  }

  /**
   * Get sitemap node using path.
   * @param path - Page URL path for the node.
   * @returns - Node if exists, otherwise null.
   */
  public getNodeByPath(path: string): SalaxySitemapNode | null {
    const sitemap = this.sitemapInRole;
    for (const section of sitemap) {
      if (this.checkUrl(section.url, path)) {
        return section;
      }
      for (const node of section.children || []) {
        if (this.checkUrl(node.url, path)) {
          return node;
        }
      }
    }
    return null;
  }

  /**
   * Filters the sitemap using the role-restrictions in the sitemap
   */
  public updateSitemapInRole(): void {
    const newSitemap: SalaxySitemapNode[] = angular.copy(this.sitemapForAllRoles);
    this.isTest = this.sessionService.isInRole("test");
    this.sitemapInRole = this.filterSitemapSectionsByRole(newSitemap);
    this.resolveAndSetCurrentNode();
  }

  /**
   * Evaluates wildcard string.
   *
   * @param nodeUrl - URL in the sitemap file (e.g. "filename.html#/this/is/nodeUrl"). From this, we take only the hash part ("#/this/is/nodeUrl").
   * @param pathUrl - URL to evaluate - this is the hash path (e.g. "#/this/is/pathUrl")
   *
   * @returns Boolean whether the path url resides within the given node url.
   */
  private checkUrl(nodeUrl: string, pathUrl: string): boolean {
    if (nodeUrl.indexOf("#") !== -1) {
      nodeUrl = nodeUrl.substr(nodeUrl.indexOf("#"));
    }
    // Check and replace route parameters (:-strings) with wildcard *
    const parts: string[] = nodeUrl.split("/");
    // eslint-disable-next-line @typescript-eslint/no-for-in-array
    for (const i in parts) {
      if (parts[i].indexOf(":") === 0) {
        parts[i] = "*";
      }
    }
    const newNodeUrl = parts.join("/");
    return this.wildCardMatch(newNodeUrl, pathUrl);
  }

  /**
   * Evaluates wildcard string
   *
   * @param rule - Wildcard rule
   * @param str - String to evaluate
   *
   * @returns Boolean whether the string matches the given wildcard rule.
   */
  private wildCardMatch(rule: string, str: string): boolean {
    // "."  => Find a single character, except newline or line terminator
    // ".*" => Matches any string that contains zero or more characters
    rule = rule.split("*").join(".*");

    // "^"  => Matches any string with the following at the beginning of it
    // "$"  => Matches any string with that in front at the end of it
    rule = "^" + rule + "$";

    // Create a regular expression object for matching string
    const regex = new RegExp(rule, "i");

    // Returns true if it finds a match, otherwise it returns false
    return regex.test(str);
  }

  private resolveAndSetCurrentNode(): void {
    const urlFromPath = "#" + this.$location.path().toLowerCase();
    this.currentPath = this.resolveCurrent(urlFromPath, this.sitemapInRole);
    if (!this.currentPath) {
      this.currentPath = [this.sitemapInRole[0]];
    }
    this.currentSection = this.currentPath[0];
    this.currentNode = this.currentPath[this.currentPath.length - 1];
    this.sectionTitle = this.currentSection?.title;
    this.title = this.currentNode?.title;
  }

  /**
   * Recursive resolver for any depth of sitemap nodes.
   * @param url Url to check for.
   * Note that in AngularJS paths, this should be th NG path starting with "#".
   * @param nodes Nodes to check.
   */
  private resolveCurrent(url: string, nodes: SalaxySitemapNode[] | null): SalaxySitemapNode[] | null {
    if (!nodes || nodes.length === 0) {
      return null;
    }
    for (const node of nodes) {
      const childMatch = this.resolveCurrent(url, node.children);
      if (childMatch) {
        return [node, ...childMatch];
      }
      if (this.checkUrl(node.url, url)) {
        return [node];
      }
    }
    return null;
  }

  /**
   * Filters a node tree bases on user roles.
   * @param nodes - Array of nodes to filter
   * @returns Filtered list of nodes.
   */
  private filterSitemapSectionsByRole(nodes: SalaxySitemapNode[]): SalaxySitemapNode[] {
    if (!nodes) {
      return [];
    }
    const filteredNodes = nodes.filter((node) => {
      if (this.sessionService.isInSomeRole(node.roles)) {
        if (this.isTest && node.roles?.trim()) {
          // Add marker that this node is visible only because we are in test mode.
          const rolesArr = node.roles.split(",");
          const rolesWithoutTest = rolesArr.filter((x) => x.trim().toLowerCase() !== "test");
          if (rolesWithoutTest.length == 0 ||
             (rolesWithoutTest.length !== rolesArr.length && !this.sessionService.isInSomeRole(rolesWithoutTest))
            ) {
            node.roles += ",visible-test-only";
          }
        }
        return true;
      } else {
        return false;
      }
    });
    filteredNodes.forEach((node) => {
      node.children = this.filterSitemapSectionsByRole(node.children);
    });
    return filteredNodes;
  }
}
