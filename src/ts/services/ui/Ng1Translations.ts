import * as angular from "angular";

import { Objects, Translations } from "@salaxy/core";

import { dictionary } from "../../i18n/dictionary";
import { I18nTerm } from "./model";

/**
 * Translation service for Ng1.
 * Provides basic methods to utilize i18n translations both in code and html (filter).
 * Please use always this class for translations instead of any other framework, which can be utilized through this class.
 */
export class Ng1Translations {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$translate", "$translateProvider"];

  constructor(
    private $translate: angular.translate.ITranslateService,
    private $translateProvider: angular.translate.ITranslateProvider,
  ) {
    this.init();
  }

  /**
   * Gets the translated value of a key (or an array of keys) synchronously
   * Main point here is that we do not use the async promise infrastructure
   * of ngx-translate as it is obsolete for us.  The translations must be
   * loaded as the app loads: It does not make sense to allow loading them
   * from server asyncronously.
   *
   * @param key Key or array of keys.
   * @param interpolateParams Optional parameters used in interpolation.
   *
   * @returns The translated key, or an object of translated keys
   */
  public get(key: string | string[], interpolateParams?: any): any {
    if (typeof (key) === "string") {
      return this.$translate.instant(key, interpolateParams);
    } else {
      return this.$translate.instant(key, interpolateParams);
    }
  }

  /**
   * Translates given key to the current language with default value.
   * @param key - Key for the text.
   * @param defaultValue - The default value that is returned if the key is not found. Null is the default.
   * @returns The translated value in current language or if none is found the key.
   */
  public getWithDefault(key: string, defaultValue = null): string {
    if (!key) {
      return null;
    }
    const value = this.get(key);
    return value === key ? defaultValue : value;
  }

  /**
   * Adds a dictionary to the current module. The dictionary translations
   * are merged to any existing translations already in the module.
   *
   * @param lang - ISO language code for the dictionary to be added: fi, en or sv
   * @param translations - A translations object (json)
   */
  public addDictionary(lang: string, translations: any): void {
    this.$translateProvider.translations(lang, translations as angular.translate.ITranslationTable);
  }

  /**
   * Gets all the texts in all languages for a specified NG1 component
   * (or other root level text in NG1-library).
   * @param componentName Name of the component
   */
  public getComponentTranslations(componentName: string): { [key: string]: I18nTerm } {
    const result: { [key: string]: I18nTerm } = {};
    const keyPrefix = `SALAXY.NG1.${componentName}`;
    this.addToArray("fi", result, keyPrefix, dictionary.fi.SALAXY.NG1[componentName], keyPrefix);
    this.addToArray("en", result, keyPrefix, dictionary.en.SALAXY.NG1[componentName], keyPrefix);
    this.addToArray("sv", result, keyPrefix, dictionary.sv.SALAXY.NG1[componentName], keyPrefix);
    return result;
  }

  /**
   * Gets all the texts in all languages for a specified Core model (typically interface from API).
   * @param modelName Name of the model.
   */
  public getModelTranslations(modelName: string): { [key: string]: I18nTerm } {
    const result: { [key: string]: I18nTerm } = {};
    const keyPrefix = `SALAXY.MODEL.${modelName}`;
    this.addToArray("fi", result, keyPrefix, (dictionary.fi.SALAXY.MODEL || {})[modelName], keyPrefix);
    this.addToArray("en", result, keyPrefix, (dictionary.en.SALAXY.MODEL || {})[modelName], keyPrefix);
    this.addToArray("sv", result, keyPrefix, (dictionary.sv.SALAXY.MODEL || {})[modelName], keyPrefix);
    return result;
  }

  /**
   * Adds a hierarchical dictionary object in one language to a terms array that will contain all the languages.
   * @param language Language of the incoming dictionary
   * @param termsArray The result terms array where items are added
   * @param prefix String prefix for the keys, e.g. SALAXY.NG1.MyComponent
   * @param dictionary The source hierarchical dictionary tree that is looped and values added to array.
   * @param omitPrefix If specified, removes this prefix from the array key (not the object/dictionary key)
   * Used to return e.g. { SALAXY.NG1.MyComponent.foo.faa: { key: "foo.faa", fi: "Foobar" } }
   */
  public addToArray(language: "fi" | "en" | "sv", termsArray: { [key: string]: I18nTerm }, prefix: string, dictionary: any, omitPrefix?: string): void {
    Object.keys(dictionary || {}).forEach((localKey) => {
      const fullKey = prefix ? `${prefix}.${localKey}` : localKey;
      if (dictionary[localKey].constructor === Object) {
        this.addToArray(language, termsArray, fullKey, dictionary[localKey], omitPrefix);
      } else {
        const resultItem = termsArray[fullKey] || (termsArray[fullKey] = {
          key: omitPrefix ? fullKey.substr(omitPrefix.length + 1) : fullKey
        });
        resultItem[language] = dictionary[localKey];
      }
    });
  }

  /**
   * Add all dictionaries from a single language object.
   * This object must have language codes as first level keys. The dictionary translations
   * are merged to any existing translations already in the module.
   *
   * @param translations - A translations object (json)
   */
  public addDictionaries(translations: any): void {
    // HACK: Why do we have two dictionaries? Should only have one?
    Translations.addDictionaries(translations);
    for (const key in translations) {
      if (Objects.has(translations, key)) {
        this.addDictionary(key, translations[key]);
      }
    }
  }

  /**
   * Set the language of the current user interface to the given language.
   * Should be called at least once at the beginning of the App.
   *
   * @param lang - ISO language code for the language of the UI: fi (default), en or sv
   *
   * @returns Promise object with loaded language file data or string of the currently used language.
   */
  public setLanguage(lang: string): angular.IPromise<string> {

    this.$translate.preferredLanguage(lang);

    // core
    Translations.setLanguage(lang);

    return this.$translate.use(lang);
  }

  /**
   * Returns current language.
   *
   * @returns Promise object with loaded language file data or string of the currently used language.
   */
  public getLanguage(): string {
    return this.$translate.use();
  }

  /**
   * Adds default language  and default dictionaries.
   *
   * @param lang - Default language for dictionaries.
   */
  private addDefaultDictionaries(lang = "fi") {
    this.addDictionaries(Translations.getCurrentDictionary());
    this.addDictionaries(dictionary);
    this.setLanguage(lang);
  }

  private init() {
    this.addDefaultDictionaries();
  }
}
