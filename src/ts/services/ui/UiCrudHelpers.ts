import { DatelyObject, Taxcard, TaxcardApprovalMethod, Taxcards, Translations, WorkerAccount, Workers } from "@salaxy/core";

import { EditDialogKnownActions, EditDialogResult } from "./model";
import { UiHelpers } from "./UiHelpers";

/** Extends UiHelpers methods with business logic (typically save, load etc.) */
export class UiCrudHelpers {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers", "Workers", "Taxcards"];

  /**
   * Creates a new UiCrudHelpers with dependency injection.
   */
  constructor(
    private uiHelpers: UiHelpers,
    private workers: Workers,

    private taxcards: Taxcards,
  ) {
  }

  /**
   * Shows a dialog for editing an existing worker.
   * If user clicks OK, the changes are saved.
   * @param workerId - Identifier of the Worker Account object to edit in the dialog.
   * @param buttonTypes "default" shows standard save button,
   * "updateCalc" shows two buttons "ok" for full calc update and "ok-no-rows" update without touching default rows.
   * @param initialTab - Tab to open.
   * @returns Dialog result. Note that OK result may be either "ok" or "ok-no-rows".
   * The latter means that for calculation only worker info should be updated, not the default rows.
   */
  public openEditWorkerDialog(
    workerId: string,
    buttonTypes: "default" | "updateCalc",
    initialTab: "default" | "calculations" | "taxcards" | "employment" | "calcRows" | "holidays" | "absences" = null,
  ): Promise<EditDialogResult<WorkerAccount>> {
    const loader = this.uiHelpers.showLoading("Ladataan työntekijän tietoja...");
    return this.workers.getSingle(workerId).then((worker) => {
      loader.dismiss();
      return this.uiHelpers.openEditWorkerDialog(worker, initialTab, buttonTypes).then((result) => {
        if (result.action === "ok" || result.action === "ok-no-rows") {
          const loader = this.uiHelpers.showLoading("Tallennetaan muutoksia...");
          return this.workers.save(result.item).then((savedWorker) => {
            loader.dismiss();
            result.item = savedWorker;
            return result;
          });
        } else {
          return result;
        }
      });
    });
  }

  /**
   * Opens a wizard dialog for creating a new Worker and Taxcard.
   * If user commits at the end, the changes are saved to both items and the resulting
   * item has the saved IDs.
   */
  public createNewWorker(): Promise<EditDialogResult<{
    /**
     * Worker account that was created (edited in testing) by the wizard.
     * This always exists, if the is "ok".
     */
    worker: WorkerAccount,
    /**
     * The taxcard added as part of the Wizard.
     * Adding taxcard may currently be skipped (kind is lef to undefined). In that case this is null.
     */
    taxcard: Taxcard,
  }>> {
    return this.uiHelpers.openNewWorkerWizard()
      .then((result) => {
        if (result.action === "ok") {
          const loader = this.uiHelpers.showLoading("Tallennetaan...");
          return this.workers.save(result.item.worker).then((savedWorker) => {
            result.item.worker = savedWorker;
            if (result.item.taxcard) {
              return this.taxcards.save(result.item.taxcard).then((savedTaxcard) => {
                result.item.taxcard = savedTaxcard;
                loader.dismiss();
                if (!savedTaxcard) {
                  this.uiHelpers.showAlert(Translations.get("SALAXY.NG1.Services.UiCrudHelpers.taxcards.noTaxcardTitle"),
                    Translations.get("SALAXY.NG1.Services.UiCrudHelpers.taxcards.noTaxcardText"));
                }
                return result;
              });
            } else {
              loader.dismiss();
              return result;
            }
          });
        } else {
          return result;
        }
      });
  }

  /**
   * Shows the add new taxcard dialog.
   * If user commits at the end, the changes are saved and the ite is the stored taxcard from server.
   * @param personalId Worker personal id is required.
   */
  public createNewTaxcard(personalId: string): Promise<EditDialogResult<Taxcard>> {
    return this.uiHelpers.openNewTaxcardDialog(personalId)
      .then((result) => {
        if (result.action === "ok") {
          const loader = this.uiHelpers.showLoading("Tallennetaan...");
          return this.taxcards.save(result.item).then((savedTaxcard) => {
            result.item = savedTaxcard;
            loader.dismiss();
            if (!savedTaxcard) {
              this.uiHelpers.showAlert(Translations.get("SALAXY.NG1.Services.UiCrudHelpers.taxcards.noTaxcardTitle"),
                Translations.get("SALAXY.NG1.Services.UiCrudHelpers.taxcards.noTaxcardText"));
            }
            if (savedTaxcard?.incomeLog.find((x) => x.diff && x.diff !== "default")) {
              return this.uiHelpers.openEditDialog("salaxy-components/modals/worker/taxcard-income-log.html", savedTaxcard, { }, "lg")
                .then((incomeLogResult) => {
                  if (incomeLogResult.action === "ok") {
                    const loader = this.uiHelpers.showLoading("Tallennetaan laskelmia...");
                    return this.taxcards.commitDiff(savedTaxcard).then((commitResult) => {
                      loader.dismiss();
                      result.item = commitResult;
                      return result;
                    });
                  } else {
                    return result;
                  }
                });
            }
            return result;
          });
        } else {
          return result;
        }
      });
  }

  /**
   * Opens a dialog for approving / rejecting a taxcard that is shared by the Worker.
   * At this point, the taxcard may or may not be revealed to the Employer (full copy created).
   * If the full copy has not been created, it will be created at this point.
   * @param employmentId Employment ID for the employment relation for which the taxcard should be approved.
   * @param paymentDate If set defines the payment date for which the taxcard is fetched.
   * @returns The dialog result as returned by approveTaxcardWithDialog() except i no taxcard waiting for
   * approval is found: Then te result is null. Note that the later scenario also shows an error alert.
   */
  public assureTaxcardAndOpenApprove(employmentId: string, paymentDate: DatelyObject = "today"): Promise<EditDialogResult<Taxcard>> {
    return this.taxcards.getEmploymentTaxcards(employmentId, paymentDate).then((cards) => {
      if (!cards.waitingApproval) {
        this.uiHelpers.showAlert("Ei verokorttia", "Työntekijällä ei ole uutta tarkistamatonta jaettua verokorttia.");
        return null;
      }
      if (cards.waitingApproval.fullCard) {
        return this.approveTaxcardWithDialog(cards.waitingApproval.fullCard);
      }
      if (cards.waitingApproval.isRelationUnconfirmed) {
        return this.uiHelpers.showConfirm("Yksityisyydensuoja: Aiotko maksaa palkan?",
          "Verokortti sisältää henkilötietoja, joita saat katsoa vain, jos aiot todella maksaa palkkaa ko. henkilölle. Tieto verokortin avaamisesta välittyy työntekijälle.\n\nHaluatko varmasti avata verokortin?")
          .then((confirmResult) => {
            if (confirmResult) {
              const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
              return this.taxcards.approveShared(cards.waitingApproval.uri, TaxcardApprovalMethod.AssureWaiting).then((result) => {
                loader.dismiss();
                return this.approveTaxcardWithDialog(result);
              });
            }
          });
      } else {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
        return this.taxcards.approveShared(cards.waitingApproval.uri, TaxcardApprovalMethod.AssureWaiting).then((result) => {
          loader.dismiss();
          return this.approveTaxcardWithDialog(result);
        });
      }
    });
  }

  /**
   * Opens a dialog for approving / rejecting a specific Employer copy of a shared taxcard and
   * does the actual approve / reject operation.
   * @param taxcard An owned copy of the taxcard that should be shown for approval / reject.
   * @returns A dialog result with "ok" if the taxcard was approved, "reject" if it was rejected
   * and "cancel" if the operation was canceled.
   */
  public approveTaxcardWithDialog(taxcard: Taxcard): Promise<EditDialogResult<Taxcard>> {
    return this.uiHelpers.openApproveTaxcardDialog(taxcard).then((result) => {
      if (result.action === EditDialogKnownActions.Ok) {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
        return this.taxcards.approveOwned(taxcard.id, TaxcardApprovalMethod.Approve).then((approvedCard) => {
          result.item = approvedCard;
          loader.dismiss();
          return result;
        });
      }
      if (result.action === "reject") {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
        return this.taxcards.approveOwned(taxcard.id, TaxcardApprovalMethod.Reject).then((approvedCard) => {
          result.item = approvedCard;
          loader.dismiss();
          return result;
        });
      }
      return result;
    });
  }
}
