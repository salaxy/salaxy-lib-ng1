import * as angular from "angular";

import {
  Calculation, CalculationListItem, CalendarEvent, DateRange, EmploymentListItem, ExternalDialogData,
  Taxcard, TaxCard2019Logic, WorkerAccount, WorkerLogic
} from "@salaxy/core";

import { EditDialogKnownActions, EditDialogResult, ExternalDialogConfig } from "./model";

import { CrudControllerBase } from "../../controllers";
import { Ng1Translations } from "./Ng1Translations";
import { SessionService } from "../SessionService";

/**
 * Provides misc. user interface helper methods that are related to AngularJS.
 * Please note that these methods do NOT connect to API (load/save) to separate UI logic from business logic:
 * The business logic is in separate UiCrudHelpers class.
 */
export class UiHelpers {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$uibModal", "$location", "$rootScope", "$window", "Ng1Translations", "$timeout", "SessionService"];

  /**
   * Creates a new UiHelpers with dependency injection.
   */
  constructor(
    private $uibModal: angular.ui.bootstrap.IModalService,
    private $location: angular.ILocationService,
    private $rootScope: angular.IRootScopeService,
    private $window: angular.IWindowService,

    private translate: Ng1Translations,
    private $timeout: angular.ITimeoutService,
    private sessionService: SessionService,
  ) {
    this.init();
  }

  /**
   * Shows an edit dialog - typically for a list item, but potentially for some other editable item.
   * Alternatively, you may use showDialog() if you wish to specify your own controller (with more logic)
   *
   * @param templateUrl - Template url.
   * @param item - The item that is being edited. The logic will support change tracking and reset on this item.
   * @param logic - Additional logic: Helper functions, metadata etc. that view can use to contruct the UI.
   * @param size - Optional size parameter: sm, lg or ''. Default is empty string (normal size).
   * @param controller Optional controller for the modal instance.
   * The item and logic are passed to controller with dependency injection name "EditDialogParameters".
   * @param options Rarely used dialog options.
   * @param options.template If set, ignores the templateUrl and uses this inline HTML template instead.
   *
   * @returns A promise that resolves to the result from the dialog.
   *
   * @example
   * this.uiHelpers.showDialog("salaxy-components/modals/account/AuthorizedAccountAdd.html", "AccountAuthorizationController")
   *
   */
  public openEditDialog<TItem>(templateUrl: string, item: TItem, logic: any, size: "sm" | "lg" | "" = "",
    controller = "EditDialogController", options: {
      template?: string,
    } = {},
  )
    : Promise<EditDialogResult<TItem>> {
    const current = angular.copy(item);
    const modalOptions: angular.ui.bootstrap.IModalSettings = {
      backdrop: "static",
      ariaLabelledBy: "modal-title",
      ariaDescribedBy: "modal-body",
      template: options.template,
      templateUrl: options.template ? null : templateUrl,
      windowTemplateUrl: "salaxy-components/modals/ui/DialogWindow.html",
      controller,
      controllerAs: "$ctrl",
      size,
      resolve: {
        EditDialogParameters: () => {
          return {
            current,
            logic,
          };
        },
      },
    };
    const modal = this.$uibModal.open(modalOptions);
    return Promise.resolve(modal.result
      .then((modalResult) => {
        const action = (angular.isString(modalResult) ? modalResult : (modalResult || {}).action) || EditDialogKnownActions.Ok;
        const hasChanges = !angular.equals(current, item);
        if (action !== EditDialogKnownActions.Cancel && hasChanges) {
          angular.copy(current, item);
        }
        const result: EditDialogResult<TItem> = {
          action,
          result: modalResult,
          item,
          logic,
          hasChanges,
        };
        return result;
      })
      .catch(() => {
        const result: EditDialogResult<TItem> = {
          action: EditDialogKnownActions.Cancel,
          result: null,
          item,
          logic,
          hasChanges: false,
        };
        return result;
      }));
  }

  private externalWindows: {
    [name: string]: {
      win: any,
      url: string,
      listener: (this: Window, ev: {
        /** The object passed from the other window. */
        data: any,
        /** The origin of the window that sent the message at the time postMessage was called. */
        origin: string,
        /** A reference to the window object that sent the message */
        source: MessageEventSource | null,
      }) => any,
    }
  } = {};

  /**
   * Shows an external dialog that may be in an untrusted domain.
   * Enables cross-site communication using postMessage events.
   * @param name Name of the unique window
   * @param url Url to show in the Window
   * @param item The item that is being edited. The logic will support change tracking and reset on this item.
   * @param config Configuration of the opened window.
   */
  public showExternalDialog<TData>(name: string, url: string, item: TData, config?: ExternalDialogConfig)
    : Promise<ExternalDialogData<TData>> {
    const browserWindow = window;
    config = config || {};

    if (this.externalWindows[name]?.listener) {
      // Cancel the previous promise and unregister event.
      this.externalWindows[name].listener.call(browserWindow, {
        data: {
          action: "cancel",
        },
        origin: null,
        source: null,
      });
      browserWindow.removeEventListener('message', this.externalWindows[name].listener);
    }

    const width = config.width || 600;
    const height = config.height || 700;
    // access to browserWindow.top is blocked if the site is run in an iframe in a cross domain environment
    // have to use current window only
    const y = browserWindow.outerHeight / 2 + browserWindow.screenY - (height / 2);
    const x = browserWindow.outerWidth / 2 + browserWindow.screenX - (width / 2);
    const windowFeatures = `toolbar=no, menubar=no, width=${width}, height=${height}, top=${y}, left=${x}`;

    const hostUrl = new URL(url);
    const popupBaseUrl = `${hostUrl.protocol}//${hostUrl.host}`;
    url += (url.indexOf("?") > 0 ? "&" : "?") + "data=" + encodeURIComponent(btoa(JSON.stringify(item)));
    if (url.indexOf("http") !== 0 ) {
      url += "&access_token=" + this.sessionService.getCurrentToken();
    }

    if (!this.externalWindows[name]?.win || this.externalWindows[name].win.closed || this.externalWindows[name].url !== url) {
      // According to best practice in https://developer.mozilla.org/en-US/docs/Web/API/Window/open
      this.externalWindows[name] = {
        url,
        win: window.open(url, name, windowFeatures),
        listener: null,
      };
    }
    // Focus and check if popup blocked
    try {
      this.externalWindows[name].win.focus();
    }
    catch (e) {
      return this.showAlert("Ponnahdusikkuna estetty selaimen asetuksissa", "Toiminto edellyttää, että sallit ponnahdusikkunan avattavan osoitteeseen " + popupBaseUrl + ".", "Sulje").then(() => {
        return Promise.resolve({
          action: "cancel",
          hasChanges: false,
          item,
        })
      });
    }

    return new Promise<ExternalDialogData<TData>>((resolve) => {
      this.externalWindows[name].listener = (ev: {
        /** The object passed from the other window. */
        data: ExternalDialogData<TData>,
        /** The origin of the window that sent the message at the time postMessage was called. */
        origin: string,
        /** A reference to the window object that sent the message */
        source: MessageEventSource | null,
      }) => {
        // HACK: Check origin/source.
        const action = ev.data.action || "cancel";
        const hasChanges = !angular.equals(ev.data.item, item);
        console.debug("Message received");
        if (action !== EditDialogKnownActions.Cancel && hasChanges) {
          angular.copy(ev.data.item, item);
        }
        resolve({
          action,
          hasChanges,
          item,
        });
      };
      // add the listener for receiving a message from the popup
      browserWindow.addEventListener('message', this.externalWindows[name].listener, { once: true });
    });
  }

  /**
   * Shows a custom dialog using the given controller and templateUrl.
   * Optionally injects the given data into the controller as 'data'.
   * NOTE: There is an alternative - more simple - modal dialog pattern openEditDialog() if you do not need a custom controller or windowTemplateUrl.
   *
   * @param templateUrl - Template url.
   * @param controller  - Controller for the dialog.
   * @param data - Optional data to inject into the controller.
   * @param windowTemplateUrl - Optional template url for the window. Defaults to 'salaxy-components/modals/ui/DialogWindow.html'. If "none", no window is used.
   * @param size - Optional size parameter: sm, lg or ''.
   * @param backdrop - Optional. Controls the presence of a backdrop. Allowed value true, false (no backdrop) 'static' (default) backdrop is present but modal window is not closed when clicking outside of the modal window.
   * @returns A promise that resolves to the result from the dialog.
   *
   * @example
   * this.uiHelpers.showDialog("salaxy-components/modals/account/AuthorizedAccountAdd.html", "AccountAuthorizationController")
   *
   */
  public showDialog(
    templateUrl: string,
    controller?: string | any,
    data?: any,
    windowTemplateUrl?: string,
    size?: "sm" | "lg" | "",
    backdrop: string | boolean = "static",
  ): Promise<any> {

    const $attrs: any = {};
    if (controller instanceof CrudControllerBase) {
      $attrs.onCreateNew = controller.onCreateNew;
      $attrs.onListSelect = controller.onListSelect;
      $attrs.onDelete = controller.onDelete;
    }

    const options: angular.ui.bootstrap.IModalSettings = {
      backdrop,
      ariaLabelledBy: "modal-title",
      ariaDescribedBy: "modal-body",
      templateUrl,
      windowTemplateUrl: windowTemplateUrl || "salaxy-components/modals/ui/DialogWindow.html",
      controller: controller || "ModalGenericDialogController",
      controllerAs: "$ctrl",
      size,
      resolve: {
        data: () => {
          return data;
        },
        $attrs,
      },
    };
    if (options.windowTemplateUrl === "none") {
      delete options.windowTemplateUrl;
    }
    const modal = this.$uibModal.open(options);

    return Promise.resolve(modal.result)
      .then((value) => {
        return value;
      })
      .catch((reason) => {
        if (reason === "backdrop click") {
          return null;
        }
        throw new Error("" + reason);
      });
  }

  /**
   * Shows a customized loading component (spinner) with an optional message.
   * Returns an object with a dismiss function to call for closing the dialog.
   * Note that there is a separate generic directive 'salaxy-loader' for indicating
   * http-traffic.
   *
   * @param heading Translation key for heading text for the loader.
   * Default text is "SALAXY.UI_TERMS.loading", but you may set this to null for just the spinner.
   * @param text Optional translation key for additional text paragraph describing what is happening.
   * @returns An object with dismiss function to close the dialog.
   *
   * @example
   * ```ts
   * const loader = this.uiHelper.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
   * setTimeout(() => {
   *   loader.dismiss();
   * }, 2000);
   * ```
   */
  public showLoading(heading = "SALAXY.UI_TERMS.loading", text?: string): {
    /** Closes the dialog */
    dismiss: () => void,
    /** Changes the heading text after opening */
    setHeading: (heading: string) => void,
    /** Changes the text after opening */
    setText: (text: string) => void,
  } {
    const data = {
      heading,
      text,
    };
    const options: angular.ui.bootstrap.IModalSettings = {
      ariaLabelledBy: "modal-title",
      ariaDescribedBy: "modal-body",
      templateUrl: "salaxy-components/modals/ui/Loading.html",
      controller: "ModalGenericDialogController",
      controllerAs: "$ctrl",
      windowClass: "salaxy-component",
      size: "sm",
      resolve: { data },
    };
    const modal = this.$uibModal.open(options);

    // prevent possibly unhandled rejection errors
    modal.result
      .then(() => {
        // do nothing
      })
      .catch(() => {
        // do nothing
      });

    return {
      dismiss: () => {
        this.$timeout(() => modal.dismiss());
      },
      setHeading: (heading: string) => {
        this.$timeout(() => { data.heading = heading; });
      },
      setText: (text: string) => {
        this.$timeout(() => { data.text = text; });
      },
    };
  }

  /**
   * Shows a confirm dialog. Like window.confirm() in JavaScript.
   * Returns true if a user clicks OK -button. Otherwise (cancel or close) returns false.
   *
   * @param heading Heading text of the confirm question as translation key.
   * @param text Optional additional text paragraph describing what is happening.
   * @param okText Possibility to override the OK button text. The given text is run through translation.
   * @param cancelText Possibility to override the Cancel button text. The given text is run through translation.
   *
   * @returns A promise that resolves to true if user clicks OK, otherwise false.
   *
   * @example
   * ```ts
   * this.uiHelpers
   *     .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.UI_TERMS.sureToDeleteRecord")
   *     .then((result) => { this.uiHelpers.showAlert("result", result); })
   * ```
   */
  public showConfirm(heading: string, text?: string, okText = "SALAXY.UI_TERMS.ok", cancelText = "SALAXY.UI_TERMS.cancel"): Promise<boolean> {
    const options: angular.ui.bootstrap.IModalSettings = {
      ariaLabelledBy: "modal-title",
      ariaDescribedBy: "modal-body",
      templateUrl: "salaxy-components/modals/ui/Confirm.html",
      windowTemplateUrl: "salaxy-components/modals/ui/DialogWindow.html",
      controller: "ModalGenericDialogController",
      controllerAs: "$ctrl",
      resolve: {
        data: {
          heading,
          text,
          okText,
          cancelText,
        },
      },
    };
    const modal = this.$uibModal.open(options);
    return Promise.resolve(modal.result)
      .then((result) => {
        return result;
      })
      .catch(() => {
        return false;
      });
  }

  /**
   * Shows a date range dialog that modifies the date range of the given object.
   *
   * @param target Target object, currently both DateRange and Calculation are supported.
   * Please note that the resulting EditDialogResult is always a DateRange though.
   * @param title Title text for the dialog
   * @param okText Possibility to override the button text. The given text is run through translation.
   * @param cancelText Possibility to override the button text. The given text is run through translation.
   * @returns EditDialogResult with the edited DateRange. Note that this is DateRange even if the object to edit is Calculation.
   */
  public showDateRange(target: DateRange | Calculation, title: string, okText = "SALAXY.UI_TERMS.save", cancelText = "SALAXY.UI_TERMS.cancel"): Promise<EditDialogResult<DateRange>> {
    let dateRange: DateRange;
    const calc = (target as Calculation).info ? (target as Calculation) : null;
    if (calc) {
      dateRange = {
        start: calc.info.workStartDate,
        end: calc.info.workEndDate,
        daysCount: calc.framework.numberOfDays,
      };
    } else {
      dateRange = target as DateRange;
    }
    return this.openEditDialog("salaxy-components/modals/date-range/DateRange.html", dateRange, {
      title,
      okText,
      cancelText,
    }, "sm").then((dialogResult) => {
      if (dialogResult.action === "ok" && calc) {
        // If the editable item was DateRange, those changes are already committed.
        calc.info.workStartDate = dialogResult.item.start;
        calc.info.workEndDate = dialogResult.item.end;
        calc.framework.numberOfDays = dialogResult.item.daysCount;
      }
      return dialogResult;
    });
  }

  /**
   * Shows an alert dialog. Like window.alert() in JavaScript.
   *
   * @param heading Heading text as translation key
   * @param text Optional additional text paragraph describing what is happening.
   * @param okText Possibility to override the button text. The given text is run through translation.
   *
   * @returns A promise with boolean: True if OK button was clicked, false if the dialog is closed / dismissed in another way.
   *
   * @example this.uiHelpers.showAlert("Please note", "There is this thing that we want to tell you.");
   */
  public showAlert(heading: string, text?: string, okText = "SALAXY.UI_TERMS.ok"): Promise<boolean> {
    const options: angular.ui.bootstrap.IModalSettings = {
      ariaLabelledBy: "modal-title",
      ariaDescribedBy: "modal-body",
      templateUrl: "salaxy-components/modals/ui/Alert.html",
      controller: "ModalGenericDialogController",
      controllerAs: "$ctrl",
      windowTemplateUrl: "salaxy-components/modals/ui/DialogWindow.html",
      resolve: {
        data: {
          heading,
          text,
          okText,
        },
      },
    };
    const modal = this.$uibModal.open(options);
    return Promise.resolve(modal.result)
      .then((result) => {
        return result;
      })
      .catch(() => {
        return false;
      });
  }

  /**
   * Shows a dialog for editing an existing worker.
   * NOTE: This method does not Save the Worker: The caller must do it separately.
   * @param worker - Worker Account object to edit in the dialog.
   * @param initialTab - Tab to open.
   * @param buttonTypes "default" shows standard save button,
   * "updateCalc" shows two buttons "ok" for full calc update and "ok-no-rows" update without touching default rows.
   * @returns Dialog result. Note that OK result may be either "ok" or "ok-no-rows".
   * The latter means that for calculation only worker info should be updated, not the default rows.
   */
  public openEditWorkerDialog(
    worker: WorkerAccount,
    initialTab: "default" | "calculations" | "taxcards" | "employment" | "calcRows" | "holidays" | "absences" = null,
    buttonTypes: "default" | "updateCalc" = "default",
  ): Promise<EditDialogResult<WorkerAccount>> {
    return this.openEditDialog(
      "salaxy-components/modals/worker/WorkerEdit.html", worker, {
      initialTab,
      buttonTypes,
      title: this.translate.get("SALAXY.NG1.WorkerDetailsComponent.workerEdit.title"),
    }, "lg");
  }

  /**
   * Opens wizard for creating a new worker in a modal dialog.
   * NOTE: This method does not Save the Worker: The caller must do it separately.
   * Uploaded taxcard attachment file may be saved.
   *
   * @param worker - If specified, modifies an existing worker account instead of creating a new one.
   * Mainly used for testing purposes.
   */
  public openNewWorkerWizard(worker: WorkerAccount = null): Promise<EditDialogResult<{
    /**
     * Worker account that was created (edited in testing) by the wizard.
     * This always exists, if the is "ok".
     */
    worker: WorkerAccount,
    /**
     * The taxcard added as part of the Wizard.
     * Adding taxcard may currently be skipped (kind is lef to undefined). In that case this is null.
     */
    taxcard: Taxcard,
  }>> {
    worker = worker || WorkerLogic.getBlank();
    return this.openEditDialog("salaxy-components/modals/worker/WorkerWizard/index.html", worker, null, "lg", "WorkerWizardController")
      .then((dialogResult) => {
        const result = angular.copy(dialogResult) as EditDialogResult<{
          /** Worker account that was created (edited in testing) by the wizard. */
          worker: WorkerAccount,
          /** The taxcard added as part of the Wizard. */
          taxcard: Taxcard,
        }>;
        result.item = {
          worker: dialogResult.item,
          taxcard: dialogResult.result.taxcard,
        };
        return result;
      });
  }

  /**
   * Opens a new dialog for adding a new taxcard.
   * @param personalId Worker personal id is required.
   */
  public openNewTaxcardDialog(personalId: string): Promise<EditDialogResult<Taxcard>> {
    if (!personalId) {
      throw new Error("Personal ID is required.");
    }
    const newCard: Taxcard = TaxCard2019Logic.getBlank(personalId);
    return this.openEditDialog("salaxy-components/modals/worker/taxcard-new.html", newCard, { personalId }, "lg");
  }

  /**
   * Opens a dialog for approving / rejecting the taxcard. The caller must do the actual approve / reject to server.
   * @param taxcard An owned copy of the taxcard that should be shown for approval / reject.
   * @returns A dialog result with "ok" if the taxcard should be approved,
   * "reject" if user rejected the approval (should be rejected) or
   * "cancel" if the operation was canceled by the user.
   */
  public openApproveTaxcardDialog(taxcard: Taxcard): Promise<EditDialogResult<Taxcard>> {
    return this.openEditDialog("salaxy-components/modals/worker/taxcard-approve.html", taxcard, {}, "lg");
  }

  /**
   * Opens a dialog for editing a calendar event.
   * @param calEvent The calendar event that should be created / edited.
   * @param title Title for the dialog.
   * @param template Template to use in create / editing.
   * Simplified is designed for adding scheduling to Calculation / Payroll.
   * @returns Standard dialog result for calendar event.
   */
  public openEditCalendarEvent(calEvent: CalendarEvent, title: string, template: "default" | "simplified" = "default"): Promise<EditDialogResult<CalendarEvent>> {
    const okText = "SALAXY.UI_TERMS.save";
    const cancelText = "SALAXY.UI_TERMS.cancel";
    const isSimplified = template === "simplified";
    return this.openEditDialog("salaxy-components/modals/workflow/CalendarEventEdit.html", calEvent, { title, okText, cancelText, isSimplified }, "lg");
  }

  /**
   * Opens the list of calculations into a dialog window for selecting a set of them.
   * NOTE: If necessary, add functionality to select only one.
   * @param category Either "paid": Read-only or "draft": Editable.
   * @param title Title for the dialog.
   */
  public openSelectCalcs(category: "paid" | "draft" = "draft", title: string = null): Promise<EditDialogResult<CalculationListItem[]>> {
    const calculations: CalculationListItem[] = [];
    title = title || (category === "paid" ? "Valitse maksetut palkat" : "Valitse luonnokset");
    return this.openEditDialog("salaxy-components/modals/calc/CalcList.html", calculations, { title, category });
  }

  /**
   * Opens the list of employment relations into a dialog window for selecting a set of them.
   * @param title Title for the dialog
   * @returns A set of selected item in dialog result.
   */
  public openSelectEmployments(title: string = null): Promise<EditDialogResult<EmploymentListItem[]>> {
    // HACK: The item now binds to old Worker API (that is really Employment based). Change to the latest Employment API.
    const workerList: EmploymentListItem[] = [];
    title = title || "Valitse työntekijät";
    return this.openEditDialog("salaxy-components/modals/calc/WorkerList.html", workerList, { title });
  }

  /**
   * Caches data to controller: This is needed to avoid $digest loops in AngularJS controllers.
   * NOTE: Default data diff method uses angular.equals(), so it will not recognize difference in properties starting with "$" - most notably OData options.
   * @param controller Controller that is used for caching. A property $salaxyCache is added for caching if not there already.
   * @param cacheKey Name for the object that is being cached. Must be unique within the controller scope.
   * @param dataMethod Method that gets the data that is being cached.
   * @param dataDiffMethod Optional method that is used for determining whether the data has changed.
   * If not specified, the dataMethod is used for diff. Should be used if dataMethod returns a lot of data or if it is resource intensive.
   */
  public cache<T>(controller: angular.IController, cacheKey: string, dataMethod: () => T, dataDiffMethod?: () => any): T {
    if (!controller.$salaxyCache) {
      controller.$salaxyCache = {} as any;
    }
    const cache = controller.$salaxyCache;
    if (dataDiffMethod) {
      const keyValue = dataDiffMethod();
      if (!cache[cacheKey] || !angular.equals(cache[cacheKey].key, keyValue)) {
        cache[cacheKey] = {
          key: keyValue,
          data: dataMethod(),
        };
      }
      return cache[cacheKey].data;
    } else {
      const data = dataMethod();
      if (!angular.equals(cache[cacheKey], data)) {
        cache[cacheKey] = data;
      }
      return cache[cacheKey];
    }
  }

  /**
   * Shows a dialog based on returnUrl parameter from the current url.
   */
  private showReturnUrlDialog() {
    const returnUrlParameters = this.getReturnUrlParameters();
    if (returnUrlParameters) {
      if (returnUrlParameters.returnUrl &&
        (returnUrlParameters.returnUrl.toLowerCase().startsWith("http:") ||
          returnUrlParameters.returnUrl.toLowerCase().startsWith("https:")
        )) {
        this.showDialog(
          "salaxy-components/modals/" + returnUrlParameters.componentTemplate + ".html",
          null).then(() => {
            this.$window.location.assign(returnUrlParameters.returnUrl);
          });
        this.$rootScope.$evalAsync(() => {
          this.$location.url("/");
        });
      } else {
        this.showDialog(
          "salaxy-components/modals/" + returnUrlParameters.componentTemplate + ".html",
          null);
        // async redirect immediately with dialog
        this.$rootScope.$evalAsync(() => {
          this.$location.url(returnUrlParameters.returnUrl);
        });
      }
    }
  }

  private getReturnUrlParameters() {
    const url = this.$location.url();
    if (url.indexOf("/dialog/") !== 0) {
      return null;
    }
    const dialog = url.substring("/dialog/".length, url.indexOf("?"));
    const returnUrl = this.$location.search().returnUrl;
    return {
      componentTemplate: dialog,
      returnUrl,
    };
  }

  private init() {
    this.$rootScope.$on("$locationChangeStart", () => {
      this.showReturnUrlDialog();
    });
  }
}
