﻿import * as angular from "angular";

import { Numeric } from "@salaxy/core";

/**
 * Defines a step in wizard
 */
export interface WizardStep {
  /** A number for the step, typically set by the counter */
  number?: number;

  /** Short title that is displayed with the Wizard buttons */
  title: string;

  /** If true, the step is active/selected. Typically, there is only one step active at any given time. */
  active?: boolean;

  /** If true, the step cannot be clicked/activated */
  disabled?: boolean;

  /** Path to the view that is shown in the wizard */
  view?: string;

  /** Path to the buttons view that is shown in the footer of the wizard */
  buttonsView?: string;

  /** Heading for the step */
  heading?: string;

  /** Intro text at the top of the wizard */
  intro?: string;
}

/**
 * Manages the state and pages of a Wizard that potentially has multiple
 * controllers views etc. in it.
 */
export class WizardService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope"];

  private steps: WizardStep[] = [];
  private activeStep: number = null;

  /**
   * Creates a new instance of WizardService
   *
   * @param $rootScope - Angular root scope. Used for event routing
   */
  constructor(private $rootScope: angular.IRootScopeService, newSteps: WizardStep[]) {
    this.setSteps(newSteps, true);
  }

  /** Gets the Wizard steps as collection */
  public getSteps(): WizardStep[] {
    return this.steps;
  }

  /**
   * Sets the Wizard steps
   *
   * @param newSteps - the new collection of steps for the wizard.
   * @param skipNotify - If true will not send the the Notify event to Subscribed controllers.
   */
  public setSteps(newSteps: WizardStep[] = [], skipNotify = false): void {
    if (angular.equals(newSteps, this.steps)) {
      return;
    }
    for (let n = 0; n < newSteps.length; n++) {
      if (!Numeric.isNumber(newSteps[n].number)) {
        newSteps[n].number = n + 1;
      }
    }
    this.steps = newSteps;
    this.activeStep = newSteps.length > 0 ? 1 : 0;
    if (!skipNotify) {
      this.notify();
    }
  }

  /** Gets the number of the currently active step */
  public get activeStepNumber() {
    return this.activeStep;
  }
  /** Sets the number of the currently active step */
  public set activeStepNumber(stepNumber: number) {
    for (const step of this.steps) {
      step.active = false;
    }
    this.steps[stepNumber - 1].active = true;
    this.activeStep = stepNumber;
    this.notify();
  }

  /**
   * Gets the current step object
   */
  public getCurrentStepObject(): WizardStep {
    return this.activeStep ? this.steps[this.activeStep - 1] : null;
  }

  /**
   * Controllers can subscribe to changes in service data using this method.
   * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
   *
   * @param scope - Controller scope for the subscribing controller (or directive etc.)
   * @param callback - The event listener function. See $on documentation for details
   */
  public subscribe(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
    const handler = this.$rootScope.$on("wizard-service-event", callback);
    scope.$on("$destroy", handler);
  }

  private notify(): void {
    this.$rootScope.$emit("wizard-service-event");
  }
}
