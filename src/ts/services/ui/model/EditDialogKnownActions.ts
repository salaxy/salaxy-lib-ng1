
/**
 * Known dialog actions: Buttons that result in closing the dialog.
 * A dialog action can be a string for special purposes, but if the dialog action is something clerly reuasable, you may add it here.
 */
export enum EditDialogKnownActions {
  /** Primary action of the dialog: Often Save (Insert / Update) */
  Ok = "ok",

  /** Primary action of the dialog: Often Save (Insert / Update). Does not perform any save operation against api. */
  OkNoCommit = "ok-no-save",

  /**
   * Cancel / reset dialog action.
   * Also the close button at the top-right corner, esc keyboard action and clicking outside dialog when available.
   */
  Cancel = "cancel",

  /**
   * Delete item action in editor windows.
   */
  Delete = "delete",

  /**
   * Delete item action in editor windows. Does not perform any delete operation against api.
   */
  DeleteNoCommit = "delete-no-save",
}
