/**
 * Parameters that are passed to an Edit dialog using uiHelpers.openEditDialog()
 * and potentially in the future by other logic.
 */
export class EditDialogParameters<TItem> {

  /** The edited item - published as $ctrl.current within the view. */
  public current: TItem;

  /**
   * Additional logic that can be used to pass small functions and non-item parameters / data to view.
   * Large amounts of logic should be passed as custom controllers.
   * Published as $ctrl.logic within the view.
   */
  public logic: any;
}
