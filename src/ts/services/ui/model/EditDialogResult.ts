import { EditDialogKnownActions } from "./EditDialogKnownActions";

/** Result from a modal dialog */
export class EditDialogResult<TItem> {

  /**
   * Result of the modal is typically either OK or Cancel.
   * Edit dialogs often also have "delete", but you may add custom actions.
   */
  public action: EditDialogKnownActions | string;

  /**
   * Result is the object that Dialog sends back in $close() method.
   * If this is string, it is interpreted as action.
   */
  public result: EditDialogKnownActions | string | {
     /** Action of the result. */
     action?: EditDialogKnownActions | string,
     } | any;

  /**
   * Data that is being edited in the dialog.
   */
  public item: TItem;

  /**
   * The logic part should typically contain functions for the modal view, but in exceptional
   * scenarios you may pass some data back here as well.
   */
  public logic: any;

  /** If true, the item has been edited by the user and should typically be saved. */
  public hasChanges: boolean;
}
