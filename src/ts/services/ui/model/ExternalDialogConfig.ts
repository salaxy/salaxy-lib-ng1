/** Properties / config for displaying an external dialog using window.open(). */
export interface ExternalDialogConfig {

  /** Height (in px) of the window. Default is 700. */
  height?: number

  /** Width (in px) of the window. Default is 600. */
  width?: number;

}