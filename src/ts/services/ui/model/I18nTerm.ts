
/**
 * A term with key and all the langauge versions.
 * Mainly for display purposes.
 */
export interface I18nTerm {

  /** Translation key */
  key: string,

  /** Finnish language text */
  fi?: string,

  /** Swedish language text */
  sv?: string,

  /** English language text */
  en?: string,
}