/**
 * A node in Palkkaus sitemap.
 */
export interface SalaxySitemapNode {
  /** Id for the node. */
  id?: string;
  /** Title. */
  title?: string;
  /** Url. */
  url: string;
  /** Description. */
  decr?: string;
  /** Icon name. */
  icon?: string;
  /** Color code. */
  color?: string;

  /** Possibility to define CSS class strings (space deparated) for page-specific layout rules. */
  cssClass?: string;

  /** Roles for limiting the visibility of the node. */
  roles?: string;
  /** Image url. */
  image?: string;
  /** If true, the item is not visible in navigation */
  hidden?: boolean;

  /** if true, sets container class to container-fluid */
  isFullWidth?: boolean;
  /**
   * Array of child nodes.
   * NOTE: Current implementations may support children only on 2 and 3 levels (section and perhaps first node level).
   */
  children?: SalaxySitemapNode[];
}
