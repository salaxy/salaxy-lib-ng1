import * as angular from "angular";

import { Ajax } from "@salaxy/core";

/**
 * Declaration of salaxy global variable.
 * @ignore
 */
declare let salaxy;

/**
 * User interaction with the current session: UserCredentials, Current Account(s) and Login/Logout.
 */
export class PersonalWebLegacyController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = [
    "AjaxNg1",
    "$sce",
    "$window",
    "$location",
    "$timeout",
  ];

  constructor(
    private ajax: Ajax,

    private $sce: angular.ISCEService,
    private $window: angular.IWindowService,
    private $location: angular.ILocationService,
    private $timeout: angular.ITimeoutService,
  ) {

  }

  /**
   * Implement IController
   */
  public $onInit() {
    //
    this.$window.addEventListener("message", (event) => this.receiveUrl(event), false);
  }

  /** Get Archive page (in iframe) */
  public getArchivePage() {
    const url = `${salaxy.config.wwwServer}/YearlyLegacy/Open?access_token=${this.ajax.getCurrentToken()}`;
    return this.$sce.trustAsResourceUrl(url);
  }

  /** TODO */
  public receiveUrl(event) {
    if (event && event.data && event.data.eventType === "legacyRedirect" && event.data.calcId) {
      this.$timeout(() => { this.$location.path("/calc/details/" + event.data.calcId); });
    }
  }

  /** Get logout url */
  public getLogOutUrl() {
    const url = `${salaxy.config.wwwServer}/security/logout`;
    return url;
  }
}
