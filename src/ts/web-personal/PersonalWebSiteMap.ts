import { SalaxySitemapNode } from "../services";

/** Sitemap for the Palkkaus.fi personal web site (Household and Worker) */
export class PersonalWebSiteMap {
  /** The sitemap */
  public static sitemap: SalaxySitemapNode[] = [
    {
    title: "Etusivu",
    url: "#/",
    id: "home",
    isFullWidth: true,
    // hack to prevent double sidebar when logged in as household
    roles: "household",
    cssClass: "",
  },
  {
    title: "Etusivu",
    url: "#/",
    id: "home",
    roles: "worker",
    isFullWidth: true,
    cssClass: "double-sidebar",
  },
  {
    title: "Kotitalousvähennys 2020",
    url: "#/home/household-deduction",
    roles: "household",
    id: "household-deduction",
  },
  {
    title: "Verokortit",
    url: "#/taxcards",
    roles: "worker",
    id: "taxcards",
  },
  {
    title: "Palkkalaskelmat",
    url: "#/calc",
    id: "calc",
    roles: "worker",
    children: [
      {
        title: "Maksetut",
        url: "#/calc/paid",
      },
      {
        title: "Lähetetyt",
        url: "#/calc/sent",
      },
      {
        title: "Luonnokset",
        url: "#/calc/draft",
      },
      {
        title: "Perutut ja virheet",
        url: "#/calc/error",
        id: "error",
      },
      {
        title: "Uusi laskelma",
        url: "#/calc/details/new",
        id:"new",
        hidden:true,
      },
      {
        title: "Palkkalaskelma",
        url: "#/calc/details/*",
        id:"calc",
        hidden:true,
      },
      {
        title: "Jaa laskelma",
        url: "#/calc/sharing/*",
        id:"calcSharing",
        hidden:true,
      },
    ],
  },
  {
    title: "Laskuri",
    url: "#/calc/details/new",
    id: "calc",
    hidden:true,
    children: [
      {
        title: "Laskelmat",
        url: "#/calc",
        hidden:true,
      },
      {
        title: "Palkkalaskuri",
        url: "#/calc/details/*",
        hidden:true,
        isFullWidth: false,
      },
    ],
  },
  {
    title: "Arkisto",
    url: "#/archive/employer-report",
    id: "archive",
    isFullWidth: false,
    roles: "household",
    children: [
      {
        title: "Työsopimukset",
        url: "#/archive/employment-contracts",
        isFullWidth: false,
      },
      {
        title: "Kotitalousvähennykset",
        url: "#/archive/household-deduction-report",
        id: "householdDeductionReport",
        isFullWidth: false,
      }
    ]
  },
  {
    title: "Arkisto",
    url: "#/archive/worker-report",
    id: "archive",
    isFullWidth: false,
    roles:"worker",
    children: [
      {
        title: "Työsopimukset",
        url: "#/archive/employment-contracts",
        isFullWidth: false,
      },
    ]
  },
  {
    title: "Omat tiedot",
    url: "#/account",
    id: "account",
    children: [
      {
        title: "Tilin tiedot",
        url: "#/account/account-info",
        hidden: true,
      },
    ],
  },
  {
    title: "Ohjeet",
    url: "https://help.palkkaus.fi/hc/fi",
    id: "abc",
    isFullWidth: true,
  },
  ]
}
