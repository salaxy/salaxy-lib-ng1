import * as angular from "angular";

import { AppInsightExceptionTracking, RouteHelperProvider } from "../helpers";
import { TemplateInterceptor } from "../interceptors";
import { UiHelpers } from "../services";
import { PersonalWebLegacyController } from "./PersonalWebLegacyController";
import { PersonalWebSiteMap } from "./PersonalWebSiteMap";

/** The main angular module configuration. */
angular.module("palkkaus.web.personal", ["ngRoute", "salaxy.ng1.components.all"])

  // Set sitemap for navigation
  .constant("SITEMAP", PersonalWebSiteMap.sitemap)
  // Set routes to pages
  .config(["RouteHelperProvider", (routeHelperProvider: RouteHelperProvider) => {
    const personalRoot = "salaxy-components/pages-personal/";
    routeHelperProvider
      .setCustomSectionRoot("salaxy-components/pages-personal")
      .homeDefault("home")
      .defaultSection("home", ["timeline-tester"], "./app/views/ng1/pages-undercon", personalRoot + "home")

      // [], "./app/views/ng1/pages-undercon" is a method for temporarily moving some pages to web where view changes require no build.
      .defaultSection("calc", [], "./app/views/ng1/pages-undercon", personalRoot + "calc")
      .defaultSection("workers", [], "./app/views/ng1/pages-undercon", personalRoot + "workers")
      .defaultSection("account", [], "./app/views/ng1/pages-undercon", personalRoot + "account")
      .defaultSection("archive", [], "./app/views/ng1/pages-undercon", personalRoot + "archive")
      .defaultSection("taxcards", [], "./app/views/ng1/pages-undercon", personalRoot + "taxcards")

      /** Temporary until the household site is shut down (02/2021) */
      .defaultSection("employment", [], "./app/views/ng1/pages-undercon", personalRoot + "employment")
      .defaultSection("calc-household", [], "./app/views/ng1/pages-undercon", personalRoot + "calc")
      .defaultSection("workers-household", [], "./app/views/ng1/pages-undercon", personalRoot + "workers")
      .defaultSection("account-household", [], "./app/views/ng1/pages-undercon", personalRoot + "account")
      .defaultSection("taxcards-household", [], "./app/views/ng1/pages-undercon", personalRoot + "taxcards")

      // Not currenlty in use
      .defaultSection("payroll")
      .defaultSection("messages")
      .defaultSection("reports")

      .otherwiseDefault();
  }])
  .config(AppInsightExceptionTracking)
  .config(["$httpProvider", ($httpProvider: angular.IHttpProvider) => {
    // Clear cached templates in browser every hour
    $httpProvider.interceptors.push(TemplateInterceptor.factory(/views\//, "YYYYMMDDHH"));
  }])
  .controller({PersonalWebLegacyController})

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  .run(["UiHelpers", (uiHelpers: UiHelpers) => {
    // UiHelpers is required for url detection (access token & return url)
  }])
  ;
