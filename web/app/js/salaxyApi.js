﻿"use strict"

var salaxyCore = require("@salaxy/core");

/**
 * Provides static access to salaxyApi classes instantiated with default address etc.
 * Provided for backward comaptibility. NOTE: Not decided whether this will be supported in the future.
 */
var salaxyApi;
(function (salaxyApi) {
  /** Returns the latest API address. E.g. "https://test-api.salaxy.com/v02/api"  */
  function getApiAddress() {
    return salaxyApi.ajax.getApiAddress();
  }
  salaxyApi.getApiAddress = getApiAddress;
  /** Returns the latest Server address that is used as bases to the HTML queries. E.g. "https://test-api.salaxy.com"  */
  function getServerAddress() {
    return salaxyApi.ajax.getServerAddress();
  }


  /**  The raw Ajax-access to the server methods: GET, POST and DELETE with different return types and authentication / error events. */
  salaxyApi.ajax = new salaxyCore.AjaxJQuery();
  /** Provides read-only access to the current user session */
  salaxyApi.session = new salaxyCore.Session(salaxyApi.ajax);
  /** Methods for updating account information */
  salaxyApi.accounts = new salaxyCore.Accounts(salaxyApi.ajax);
  /** Provides CRUD access for authenticated user to access a his/her own calculations */
  salaxyApi.calculator = new salaxyCore.Calculator(salaxyApi.ajax);
  /** Provides CRUD access for authenticated user to access a his/her own calculations */
  salaxyApi.calculations = new salaxyCore.Calculations(salaxyApi.ajax);
  /** Read-only access to CMS Articles and other documentation */
  salaxyApi.cms = new salaxyCore.Cms(salaxyApi.ajax);
  /** Anonymous methods for accessing the Public Profiles (Osaajaprofiilit). */
  // salaxyApi.profiles = new salaxyCore.Profiles(salaxyApi.ajax);
  /** Provides access to HTML reports on calculations */
  salaxyApi.reports = new salaxyCore.Reports(salaxyApi.ajax);

  salaxyApi.files = new salaxyCore.Files(salaxyApi.ajax);

})(salaxyApi || (salaxyApi = {}));