import * as angular from "angular";

import { RouteHelperProvider, SalaxySitemapNode } from "@salaxy/ng1";

import sitemap from "./sitemap.json";
import { AssertionDemoController, SalaxyDemoController, SecurityDemoController } from "./controllers";
import { SalaxyDemo } from "./components";


/**
 * Main Angular application for the Salaxy Blank demo site
 */
export const module = angular.module("Salaxy.demoApp", ["ngRoute", "salaxy.ng1.components.all", "color.picker"])
  .config(["RouteHelperProvider",
    function (routeHelperProvider: RouteHelperProvider) {
      // Helper to get a path to template with defaults
      function getTemplate(section: string, template = "index") {
        return "/app/views/" + section + "/" + template + ".html";
      }
      // Helper to get a path to template with defaults
      function getBootstrapModalTemplate(folder, template) {
        return "salaxy-components/modals/" + folder + "/" + template + ".html";
      }
      routeHelperProvider
        /** System  */
        .when("/", { templateUrl: getTemplate("home") }, "")
        .when("/error", { templateUrl: getTemplate("home", "error") }, "error")

        .setCustomSectionRoot("/app/views/ng1")

        /** Well known main sections that may be accessed outside (e.g. IFrame) */
        // These have been checked
        .defaultSection("reports")

        .defaultSection("welcome", [], null, "home")
        .customSection("articles")
        .customSection("accounting")
        .customSection("account-reset")

        .defaultSection("partners")
        .defaultSection("service")
        // temporary routing for reports
        .defaultSection("earnings-payments")
        .defaultSection("irepr")
        .defaultSection("salary-reports")

        // TODO: Implement new crudItemId based routing
        // .when("/workers/:viewName?/:workerId?", { templateUrl: function (path) { return getTemplate("ng1/workers", path.viewName) } })
        .when("/workflow/:viewName?/:crudItemId*?", { templateUrl: function (path) { return getTemplate("ng1/workflow", path.viewName) } }, "workflow")


        /* TODO: Go through */
        .when("/ng1/form-controls", { templateUrl: getTemplate("ng1/form-controls") }, "ng1")
        .when("/account/authorizationsAndCertificates/", { templateUrl: getTemplate("ng1", "account") }, "account")
        .when("/account/:viewName", { templateUrl: function (path) { return getTemplate("account", path.viewName) } }, "account")

        /* Development-site specific stuff (go through) */
        .when("/todo/:section", { templateUrl: function (path) { return getTemplate("home", "todo") } }, "todo")
        .when("/home/:viewName", { templateUrl: function (path) { return getTemplate("home", path.viewName) } }, "home")
        .when("/api/:viewName", { templateUrl: function (path) { return getTemplate("api", path.viewName) } }, "api")
        .when("/js/:viewName", { templateUrl: function (path) { return getTemplate("js", path.viewName) } }, "js")
        .when("/ng1/:viewName", { templateUrl: function (path) { return getTemplate("ng1", path.viewName) } }, "ng1")
        .when("/other/:viewName", { templateUrl: function (path) { return getTemplate("other", path.viewName) } }, "other")
        .when("/security/:viewName", { templateUrl: function (path) { return getTemplate("security", path.viewName) } }, "security")
        .when("/ui/:viewName", { templateUrl: function (path) { return getTemplate("ui", path.viewName) } }, "ui")
        .when("/docs/@salaxy/:lib/class/:class", { templateUrl: function (path) { return getTemplate("docs", "class") } }, "docs")
        .when("/docs/@salaxy/:lib/interface/:class", { templateUrl: function (path) { return getTemplate("docs", "interface") } }, "docs")
        .when("/docs/@salaxy/:lib", { templateUrl: function (path) { return getTemplate("docs", "typedoc") } }, "docs")
        .when("/docs/:viewName?/:id*?", { templateUrl: function (path) { return getTemplate("docs", path.viewName) } }, "docs")
        .when("/dev/:viewName", { templateUrl: function (path) { return getTemplate("dev", path.viewName) } }, "dev")
        .when("/bootstrap/modals/:section/:template", { templateUrl: function (path) { return getBootstrapModalTemplate(path.section, path.template) } }, "bootstrap")

        /* Catch-all to error page */
        .otherwise({ templateUrl: getTemplate("home", "error404") })

        .commit();
    }
  ])
  .constant<SalaxySitemapNode[]>("SITEMAP", sitemap)
  .run(["SessionService",
    function (sessionService) {
      sessionService.signInErrorUrl = "/home/error-oauth";
    }
  ])
  // Just for location change tracking:
  .run(["OnboardingService", function (onboardingService) { }])
  .run(["Ng1Translations", function (translate) {
    translate.setLanguage("en");
  }])
  .controller("AssertionDemoController", AssertionDemoController)
  .controller("SalaxyDemoController", SalaxyDemoController)
  .controller("SecurityDemoController", SecurityDemoController)
  .component("salaxyDemo", new SalaxyDemo())
  ;
