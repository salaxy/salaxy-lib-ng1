import * as angular from "angular";

import { SalaxyDemoController } from "../controllers";

/**
 * Show a user interface for a plain JavaScript demo
 * TODO: To be replaced by Codepen or similar
 */
export class SalaxyDemo implements angular.IComponentOptions {

    /**
     * The following component properties (attributes in HTML) are bound to the Controller.
     * For detailed functionality, refer to controller implementation
     */
     bindings = {
      title: "@",
      filter: "@",
      folder: "@",
      directive: "@",
      component: "@",
      componentTemplate: "@",
      controller: "@",
      hasJs: "<",
      hasCss: "<",
    }

    /** Uses the SalaxyDemoController */
    public controller = SalaxyDemoController;

    public transclude = true;

    /** Teh template URL */
    public templateUrl = "/app/templates/salaxyDemo.html";
}
