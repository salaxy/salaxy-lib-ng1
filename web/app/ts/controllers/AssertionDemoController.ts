import * as angular from "angular";

import { Configs } from "@salaxy/core";

/** Controller for demonstrating the sso/assertion features. */
export class AssertionDemoController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1", "$scope", "$http", "$timeout", "AuthorizedAccountService", "SessionService", "$sce"];

  /** Model for the assertion demo */
  public model = {

    /** Account type for the certificate request: "current", "authorizingAccount" or "proxy" */
    accountType: null,
    /** Request for fetching the certificate */
    certRequest: {} as any,
    /** Result for fetching the certificate. */
    certResult: null,

    /** Test assertion Json Web Token. */
    testAssertionJwt: null,
    /** Test assertion JWT header. */
    testAssertionHeaderJson: null,
    /** Test assertion JWT payload. */
    testAssertionPayloadJson: null,
    /** Test assertion JWT signature. */
    testAssertionSignature: null,
    /** Certificate creation error */
    certificateCreationError: null,
    /** Test access token request. */
    testRequestJson: null,
    /** Test access token request response. */
    testResponseJson: null,
    /** Authorization header for session request */
    sessionRequestHeader: null,
    /** Response from session request */
    sessionResponseJson: null,
    /** Uploaded certificate file */
    certificateFile: null,

    /** Just JSON to display as-is */
    sampleJson: {
      /** Access token request structure. */
      requestStructureJson: {
        grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
        assertion: "<assertion JWT>"
      },
      /** JWT header structure in assertion. */
      assertionHeaderStructureJson: {
        typ: "JWT",
        alg: "RS256",
        x5t: "<thumbprint for the X.509 certificate>",
        x5c: "array containing one base64 encoded X.509 certificate string"
      },
      /** JWT paylaod structure in assertion. */
      assertionPayloadStructureJson: {
        iss: "<issuing application>",
        sub: "<named account id>",
        aud: "https://test-api.salaxy.com/oauth2/token",
        exp: "<expiration time>"
      },
    }
  };

  /** Model for the Federation demo */
  public fedModel = {
    /** The federation data based on the current user */
    federationData: null,
    /** Certificate bytes as Base64String */
    certificateBytes: null,
    /** Certificate password for signing */
    certificatePwd: null,
    /** Federation data as JWT */
    token: null,

    /** TODO: Needs implementation */
    certificateFile: null,
    /** TODO: Is this needed? Or can we use JSON editor to federationData? */
    customFederationDataJson: null,
  };

  /** The result model depending on the demo */
  public postResult: any = null;

  private authorizingAccountsCache: any;

  constructor(private ajax, private $scope, private $http, private $timeout, private authorizedAccountService, private sessionService, private $sce: angular.ISCEService) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
  }

  /**
   * Gest the URL for the WWW-server
   */
  public getWwwUrl() {
    return Configs.current.wwwServer || "https://test-www.palkkaus.fi";
  }

  /**
   * Gest the URL for the API server we are currently using.
   * @param path Path that is added to the server name. Start the path with "/".
   */
   public getApiUrl(path: string) {
    return this.$sce.trustAsResourceUrl(this.ajax.getServerAddress() + path);
  }

  /** Gets the current account. */
  public getCurrentAccount() {
    return this.sessionService.isAuthenticated ? this.sessionService.getSession().currentAccount : null;
  }

  /** Gets an array of authorizing accounts once loaded from server.  */
  public getAuthorizingAccounts() {
    if (this.authorizedAccountService.getAuthorizingAccounts().length && !this.authorizingAccountsCache) {
      this.authorizingAccountsCache = this.authorizedAccountService.getAuthorizingAccounts().reduce((accumulator, currentValue) => {
        accumulator[currentValue.id] = currentValue.id + " - " + currentValue.avatar.displayName;
        return accumulator;
      }, {});
    }
    return this.authorizingAccountsCache;
  }

  /** For assure company account demo page, post the request. */
  public createCertificate() {
    if (this.model.accountType == "current") {
      this.model.certRequest.subject = this.getCurrentAccount().id;
    }
    this.ajax.postJSON("/v02/api/accounts/sso-demo/create-certificate", this.model.certRequest)
      .then((result) => {
        this.model.certResult = result;
        if (result.token) {
          this.model.testRequestJson = {
            grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
            assertion: result.token
          };
        }
      }).catch((error) => {
        this.model.certResult = {
          error: "HTTP-error: " + error.data || error,
        }
      })
  }

  public getAccessToken() {
    this.ajax.postJSON(this.ajax.getServerAddress() + "/oauth2/token", this.model.testRequestJson)
      .then((result) => {
        this.model.testResponseJson = result;
        if (result.access_token) {
          this.model.sessionRequestHeader = "Authorization: Bearer " + result.access_token;
        }
      }).catch((error) => {
        this.model.testResponseJson = {
          error: "HTTP-error: " + error.data || error,
        }
      });
  }

  public getSession() {
    var model = this.model;
    this.$http({
      method: 'GET',
      url: this.ajax.getServerAddress() + '/v02/api/session/current',
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + model.testResponseJson.access_token,
      },
    }).then(function successCallback(response) {
      model.sessionResponseJson = response;
    }, function errorCallback(response) {
      model.sessionResponseJson = { error: response };
    });
  };

  public uploadCertificateFile(file, isFederation) {
    if (!file) {
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.$timeout(() => {
        // Data url contains mimetype and base64 encoded file
        // Remove mime type
        const results = (reader.result as string).split(",");
        if (results.length > 0) {
          if (isFederation) {
            this.fedModel.certificateBytes = results[results.length - 1];
          } else {
            this.model.certRequest.certificateBytes = results[results.length - 1];
          }
        }
      });
    };
  }

  /**
   * Gets the federation data and/or the token if there is enough information.
   * @param clearCurrent If true, clear the federationData property so that it is regenerated.
   */
  public getFederationData(clearCurrent: boolean) {
    if (clearCurrent && this.fedModel) {
      this.fedModel.federationData = null;
    }
    this.ajax.postJSON("/v02/api/test/federation", this.fedModel)
      .then((result) => {
        this.fedModel = result;
      }).catch((error) => {
        alert("HTTP-error: " + error.data || error);
      })
  };

  public copyCurrentUser() {
    this.fedModel.customFederationDataJson = JSON.stringify(this.fedModel.federationData, null, " ");
  };

}
