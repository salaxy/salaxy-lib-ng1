import * as angular from "angular";

/** Controller for demonstrating the misc security features. */
export class SalaxyDemoController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$http"];

  public title: string;
  
  public folder: string;
  public hasJs = false;
  public hasCss = false;
  public contents: string;
  public showHtml = false;
  public showJs = false;
  public showCss = false;
  public sourceHtml = "Loading...";
  public sourceJs = "Loading...";
  public sourceCss = "Loading...";
  public sourceTemplate = "Loading...";
  public controller: string;
  public component: string;
  public componentTemplate: string;
  public directive: string;
  public filter: string;

  public showTemplate: boolean;

  constructor(private $http: angular.IHttpService) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    if (this.folder) {
      this.$http.get<string>("/app/views/" + this.folder + "/index.html").then((res) => { this.sourceHtml = res.data; });
      if (this.hasJs) {
        this.$http.get<string>("/app/views/" + this.folder + "/script.js").then((res) => { this.sourceJs = res.data; });
      }
      if (this.hasCss) {
        this.$http.get<string>("/app/views/" + this.folder + "/styles.css").then((res) => { this.sourceCss = res.data; });
      }
    }
    if (this.componentTemplate) {
      this.$http.get<string>("/docs/templates/" + this.componentTemplate).then((res) => { this.sourceTemplate = res.data; });
    }
  }

  public toggleSource(type) {
    // TODO: When writing this as Typecript, replce this naive implementation.
    if (type == "js") {
      this.showHtml = false;
      this.showJs = !this.showJs;
      this.showCss = false;
      this.showTemplate = false;
    } else if (type == "css") {
      this.showHtml = false;
      this.showJs = false;
      this.showCss = !this.showCss;
      this.showTemplate = false;
    } else if (type == "template") {
      this.showHtml = false;
      this.showJs = false;
      this.showCss = false;
      this.showTemplate = !this.showTemplate;
    } else {
      this.showHtml = !this.showHtml;
      this.showJs = false;
      this.showCss = false;
      this.showTemplate = false;
    }
  }

  public getSource(type) {
    if (type == "js") {
      return this.sourceJs;
    }
    if (type == "css") {
      return this.sourceCss;
    }
    if (type == "template") {
      return this.sourceTemplate;
    }
    return this.sourceHtml;
  }

  public getUrl() {
    return "/app/views" + this.folder + "/index.html";
  }
  public getFilterUrl() {
    return "#/docs/@salaxy/ng1/class/" + (this.filter || "").toLowerCase().replace(/-/g, "").replace("sxy", "salaxy") + "filter";
  }
  public getDirectiveUrl() {
    return "#/docs/@salaxy/ng1/class/" + (this.directive || "").toLowerCase().replace(/-/g, "") + "directive";
  }
  public getComponentUrl() {
    return "#/docs/@salaxy/ng1/class/" + (this.component || "").toLowerCase().replace(/-/g, "").replace("salaxy", "");
  }
  public getControllerUrl() {
    return "#/docs/@salaxy/ng1/class/" + (this.controller || "").toLowerCase();
  }
}
