import * as angular from "angular";

import { AjaxNg1 } from "@salaxy/ng1";

/** Controller for demonstrating the misc security features. */
export class SecurityDemoController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1", "$scope"];

  /** Model depending on the demo */
  public model: any = {};

  /** The result model depending on the demo */
  public postResult: any = null;

  constructor(private ajax: AjaxNg1, $scope: angular.IScope) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
  }

  /** For assure company account demo page, post the request. */
  public postAssureCompanyAccount() {
    this.postResult = "";
    this.ajax.postJSON("/partner/company", this.model).then((result) => {
        this.postResult = result;
      }).catch((error) => {
        this.postResult = error.data || error;
      })
  };
}
