var salaxyCore = require("@salaxy/core");
var ajax = new salaxyCore.AjaxJQuery();
var calcApi = new salaxyCore.Calculator(ajax);

function sample2Calculate() {
    calcApi.createNew().then(emptyCalcObjectReceived);
}

function emptyCalcObjectReceived(data) {
    data.salary.kind = "hourly";
    data.salary.amount = $('#sample2-amount').val();
    data.salary.price = $('#sample2-price').val() || 10;
    calcApi.recalculate(data).then( recalculationDone);
};

function recalculationDone(data) {
    // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
    // However, we are planning to round to 2 decimals inside API before release, so this should be fine in the release timeframe.
    $('#sample2-result-amount').text(data.salary.amount.toFixed(2));
    $('#sample2-result-price').text(data.salary.price.toFixed(2));
    $('#sample2-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
    $('#sample2-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
    $('#sample2-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
}