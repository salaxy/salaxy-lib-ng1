﻿
function createCalculation() {
    salaxyApi.calculator.calculateSalary(
            "hourly",
            $('#sample1-amount').val(),
            $('#sample1-price').val() || 10,
            null).then(
            function (data) {
                // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
                // However, we are planning to round to 2 decimals inside API before release, so this should be fine in the release timeframe.
                $('#sample1-result-amount').text(data.salary.amount.toFixed(2));
                $('#sample1-result-price').text(data.salary.price.toFixed(2));
                $('#sample1-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
                $('#sample1-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
                $('#sample1-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
                salaxyApi.calculations.save(data).then( function(resp) {
                    $('#sample1-result').text('created calc id:' + resp.id);
                })
            });
}

