﻿function getCalculations() {
    salaxyApi.calculations.getAll().then(
            function (data) {
                $("#get_table > tbody > tr").remove();
                var index;
                for (index = 0; index < data.length; ++index) {
                    $('#get_table > tbody:last').append('<tr><td>' + data[index].id + '</td><td>' + data[index].result.totals.totalBaseSalary + '</td><td>' + data[index].result.totals.totalExpenses + '</td><td>' + data[index].workflow.status + '</td></tr>');
                }
            });
}
