﻿function workerReport() {
    salaxyApi.reports.getReportHtmlById('worker',$('#sample4-id').val()).then( reportDone);
}

function workerReportPage2() {
    salaxyApi.reports.getReportHtmlById('workerpage2',$('#sample4-id').val()).then( reportDone);
}

function employerReport() {
    salaxyApi.reports.getReportHtmlById('employer',$('#sample4-id').val()).then( reportDone);
}

function employerReportPage2() {
    salaxyApi.reports.getReportHtmlById('employerpage2',$('#sample4-id').val()).then( reportDone);
}

function paymentReport() {
    salaxyApi.reports.getReportHtmlById('payments',$('#sample4-id').val()).then( reportDone);
}

function reportDone(html) {
    $('#report').html(html);
}