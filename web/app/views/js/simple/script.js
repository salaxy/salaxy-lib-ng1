import salaxyCore from 'https://cdn.skypack.dev/@salaxy/core';
var ajax = new salaxyCore.AjaxJQuery();
var calcApi = new salaxyCore.Calculator(ajax)

var reportsApi = new salaxyCore.Reports(ajax);

function sample1Calculate() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
      // However, we are planning to round to 2 decimals inside API before release, so this should be fine in the release timeframe.
      $('#sample1-result-amount').text(data.salary.amount.toFixed(2));
      $('#sample1-result-price').text(data.salary.price.toFixed(2));
      $('#sample1-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
      $('#sample1-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
      $('#sample1-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
    });
}

function workerReport() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      reportsApi.getReportHtmlWithPost("worker", data).then(reportDone);
    });
}

function workerReportPage2() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      reportsApi.getReportHtmlWithPost("workerpage2", data).then(reportDone);
    });
}


function employerReport() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      reportsApi.getReportHtmlWithPost("employer", data).then(reportDone);
    });
}

function employerReportPage2() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      reportsApi.getReportHtmlWithPost("employerpage2", data).then(reportDone);
    });
}

function paymentReport() {
  calcApi.calculateSalary(
    "hourly",
    $('#sample1-amount').val(),
    $('#sample1-price').val() || 10,
    null).then(
    function(data) {
      reportsApi.getReportHtmlWithPost("payments", data).then(reportDone);
    });
}

function reportDone(html) {
  $('#report').html(html);
}

window.salaxy.sample1Calculate = sample1Calculate;
window.salaxy.workerReport = workerReport;
window.salaxy.workerReportPage2 = workerReportPage2;
window.salaxy.employerReport = employerReport;
window.salaxy.employerReportPage2 = employerReportPage2;
window.salaxy.paymentReport = paymentReport;