﻿function getAndUpdateCalculation() {
    salaxyApi.calculations.getSingle(
            $('#sample5-id').val()).then(
            function (data) {
                var msg = "Orginal salary: " + data.salary.amount;
                data.salary.amount += 10;
                salaxyApi.calculations.save(data).then( function (resp) {
                    msg += " Current salary: " + resp.salary.amount;
                    $('#sample5-result').text(msg);
                });

            });
}
