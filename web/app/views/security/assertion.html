<div ng-controller="AssertionDemoController as $ctrl">
  <div>
    <h2 class="page-header">Impersonation Flow</h2>
    <p class="lead">
      Impersonation is the process where a user or system (requesting account)
      logs into Salaxy API impersonating as another user (named account).
      Naturally, this happens only if the named account has already given permission (authorization) to such an action.
      The main use case is to facilitate server side applications to run operations on customer data.
    </p>
    <p class="lead">
      In Salaxy, impersonation is done using <strong>Authorization grant messages</strong>:
      The document <a href="https://tools.ietf.org/html/rfc7523" target="_blank">RFC7523 2.1. "Using JWTs as Authorization Grants"</a>
      (<strong>grant_type: urn:ietf:params:oauth:grant-type:jwt-bearer</strong>)
      defines an oAuth2.0 -access token request which can be used for generating an access token for a named account.
    </p>
    <p>
      The Simple Impersonation Flow has the following steps:
    </p>
    <img src="/app/img/elems/assertion.png" class="img-responsive img-thumbnail" />
    <br /><br />
    <ol>
      <li>
        Prerequisites
        <ul>
          <li>Sign up in Salaxy / Palkkaus.fi to aquire a Salaxy account</li>
          <li>Aquire a certificate</li>
        </ul>
      </li>
      <li>
        Client application logins to the partner server.
      </li>
      <li>
        Create a JSON Web Token to be used as an authorization grant
        based on <a href="https://tools.ietf.org/html/rfc7523" target="_blank">RFC7523 2.1. "Using JWTs as Authorization Grants"</a>.
      </li>
      <li>
        Post an access token request to Salaxy using the created JSON Web Token as an authorization grant.
      </li>
      <li>
        The partner server receives the user specific Salaxy access token.
      </li>
      <li>
        The client receives the Salaxy access token at the end of the login process.
      </li>
      <li>
        The client sets the token in the Authorization header (Bearer) and signs in to Salaxy.
        The client continues communication with the Salaxy using the access token.
      </li>
    </ol>
  </div>
  <div salaxy-if-role="anon">
    <h3>Authentication required</h3>
    <div class="alert alert-danger">
      The rest of the page requires authentication.
      Please see <a href="#security/index">Getting started section</a> for more information.
    </div>
  </div>
  <div salaxy-if-role="auth">
    <div>
      <h2>1. Prerequisites</h2>
      <h3>Salaxy account</h3>
      <p>
        Every company, association or person has only one Account in the Salaxy system. This account stores all the paid salaries for employers and received salaries for
        employees. The uniqueness of the account is checked using Finnish Personal ID (Henkilötunnus) or Business ID (Y-tunnus).
        This account may be accessed using several Credentials (e.g. E-mail/PWD, Facebook, Certificate) and the account owner may grant Authorization to another account (e.g.
        to an accountant or lawyer).
        New account can be created by signing up in the Salaxy.
      </p>
      <h3>Certificate</h3>
      <p>
        For step 3. you will also need a Certificate for your account.
        If you do not have a certificate
        <a href="#/settings/authorizations">create one here</a>.
        You need both certificate and its password.
      </p>
      <p>
        The requesting account <strong>must</strong> have <strong>a valid X.509 certificate</strong> for authentication.
        Additionally the account for which the access token is requested (named account) <strong>must</strong> have <strong>authorized the requesting account</strong>.
        If you are impersonating any other account than yourself, you need to ask <strong>the other account</strong> to
        <a href="#/settings/authorizations">authorize your account</a>.
      </p>
    </div>
    <div>
      <h2>2. Client initiation</h2>
      <p>
        Client logins to the partner server and the server side logic starts the process for impersonating the user for Salaxy.
      </p>
    </div>
    <div>
      <h2>3. Create a JSON Web Token to be used as an authorization grant</h2>
      <h3>Json Web Token structure</h3>
      <p>Header</p>
      <pre>{{ $ctrl.model.sampleJson.assertionHeaderStructureJson | json }}</pre>
      <p>Payload</p>
      <pre>{{ $ctrl.model.sampleJson.assertionPayloadStructureJson | json }}</pre>

      <h3>Creating Json Web Token for authorization grant</h3>
      <p>
        The Json Web Token token is generated using the following code (c#).
        Node.js implementation can be based on <a href="https://github.com/auth0/node-jsonwebtoken" target="_blank">https://github.com/auth0/node-jsonwebtoken</a>. A sample
        implementation can be found in the code sample below.
      <pre class="pre-scrollable">
//C# sample
public static string CreateAssertionToken(byte[] certificateBytes, string password, string issuer, DateTime expires, string subject)
{

    //Load certificate
    var certificate = new X509Certificate2(certificateBytes, password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);

    //Make header, add signing certificate
    var header = new JwtHeader(new X509SigningCredentials(certificate));
    header.Add(JwtHeaderParameterNames.X5c, new string[] { Convert.ToBase64String(certificate.RawData) });

    //Make payload
    var payload = new JwtPayload(
    issuer,
    "https://test-api.salaxy.com/oauth2/token",
    new Claim[] { new Claim("sub", subject) },
    null,
    expires);

    //Make token and sign
    var token = new JwtSecurityToken(header, payload);

    //Serialize
    var tokenHandler = new JwtSecurityTokenHandler();
    string jwt = tokenHandler.WriteToken(token);
    return jwt;
}

//TypeScript sample for node.js
import jwt = require("jsonwebtoken");
export class CertificateUtility{

    public static createAssertionToken(thumbprint:string, certificate:Buffer, privateKey:Buffer, issuer:string, expires:Date, subject:string):string{   
        //Read and parse certificate    
        let crtData = certificate.toString();
        let startTag = "-----BEGIN CERTIFICATE-----";
        let endTag = "-----END CERTIFICATE-----";
        let start = crtData.indexOf(startTag) + startTag.length;
        let end = crtData.indexOf(endTag);
        crtData = crtData.substring(start, end).replace(/(?:\r\n|\r|\n)/g, "");

        //Make header
        let buffer = new Buffer(thumbprint,"hex");
        let x5t = buffer.toString("base64");     
        x5t = x5t.replace(/\+/g,'-').replace(/\//g,'_').replace(/=+$/, '');

        let headers = { 
            "algorithm": "RS256",
            "header": {
            "x5t": x5t,
            "x5c": [crtData]
            }
        }

        //Make payload
        let payload = {
            "iss": issuer,
            "sub": subject,
            "aud": "https://test-api.salaxy.com/oauth2/token",
            "exp": Math.floor(expires.getTime() / 1000) 
        }
       
        //Sign certificate
        let token = jwt.sign(payload, privateKey, headers);
        

        return token;
    }
}
</pre>
      </p>
    </div>
    <div class="form-horizontal">

      <fieldset>
        <div class="form-group disabled">
          <label class="control-label col-sm-4" for="certificateFile">Upload X.509 certificate (.pfx file)</label>
          <div class="col-sm-8">
            <button class="btn btn-default" ngf-multiple="false" ngf-select="$ctrl.uploadCertificateFile($file)" name="certificateFile" id="certificateFile"
              class="form-control">Select file...</button>
          </div>
        </div>
        <salaxy-textarea label="...or alternatively paste certificate" name="certificateBytes" maxlength="10000" ng-model="$ctrl.model.certRequest.certificateBytes"
          label="Basic example"></salaxy-textarea>
        <salaxy-input ng-model="$ctrl.model.certRequest.password" s-type="password" name="certificatePassword" label="Certificate Pwd"></salaxy-input>
        <salaxy-input ng-model="$ctrl.model.certRequest.userEmail" name="userEmail" label="User e-mail / user ID"></salaxy-input>
        <salaxy-input-enum
          options="{ current: 'Current account', authorizingAccount: 'One of the accounts that have authorized this account', proxy: 'Two-level: Partner account as a proxy' }"
          label="Target account" name="accountType" ng-model="$ctrl.model.accountType" type="radio" placeholder="Please select account type..." required></salaxy-input-enum>
        <salaxy-form-group name="currentAccount" label="Current account" ng-if="$ctrl.model.accountType == 'current'">
          {{ $ctrl.getCurrentAccount().avatar.displayName }} ({{ $ctrl.getCurrentAccount().id }})
        </salaxy-form-group>
        <salaxy-input-enum ng-if="$ctrl.model.accountType == 'authorizingAccount'" options="$ctrl.getAuthorizingAccounts()" label="Account Id" name="subject"
          ng-model="$ctrl.model.certRequest.subject" type="select" placeholder="Please select account..." disable-cache="true" required></salaxy-input-enum>
        <div ng-if="$ctrl.model.accountType == 'proxy'">
          <p>
            Proxy account (e.g. accounting company) is the account where the current acount {{ $ctrl.getCurrentAccount().avatar.displayName }} ({{ $ctrl.getCurrentAccount().id
            }}) has access rights.
            Proxy account then must have access right to the final account (e.g. employer company).
          </p>
          <salaxy-input-enum options="$ctrl.getAuthorizingAccounts()" label="Proxy account id" name="subject" ng-model="$ctrl.model.certRequest.proxyActor" type="select"
            placeholder="Please select account..." disable-cache="true" required></salaxy-input-enum>
          <salaxy-input ng-model="$ctrl.model.certRequest.subject" name="subject" label="Account id"></salaxy-input>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-4 col-sm-8">
            <button class="btn btn-primary" type="submit" name="Command" ng-click="$ctrl.createCertificate()">Create Json Web Token</button>
          </div>
        </div>
      </fieldset>
      <div class="alert alert-danger" ng-if="$ctrl.model.certResult.error">{{ $ctrl.model.certResult.error }}</div>
      <div class="alert alert-info" ng-if="!$ctrl.model.certResult.token">Post the form with Certificate, Password and Account to show the token here.</div>
      <div ng-if="$ctrl.model.certResult.token">
        <h3>Created Json Web Token</h3>
        <pre style="white-space: pre-wrap">{{ $ctrl.model.certResult.token | json }}</pre>
        <h3>Decoded Json Web Token</h3>
        <p>Header</p>
        <pre style="white-space: pre-wrap">{{ $ctrl.model.certResult.tokenDecoded.header | json }}</pre>
        <p>Payload</p>
        <pre style="white-space: pre-wrap">{{ $ctrl.model.certResult.tokenDecoded.payload | json }}</pre>
        <p>Raw signature</p>
        <pre style="white-space: pre-wrap">{{ $ctrl.model.certResult.tokenDecoded.rawSignature | json }}</pre>
      </div>
    </div>
    <div class="form-horizontal">
      <h2>4. Create and post access token request</h2>

      <h3>Access token request structure</h3>
      <p>
        The request must contain parameters: <strong>grant_type</strong> and <strong>assertion</strong>.
        Assertion parameter contains a signed Json Web Token (JWT).
        The end point for the access token request is <a href="/oauth2/token" target="_blank">/oauth2/token</a>.
      </p>
      <pre>{{ $ctrl.model.sampleJson.requestStructureJson | json }}</pre>

      <h3>Posting access token request</h3>
      <div class="alert alert-info" ng-if="!$ctrl.model.certResult.token">
        Post the form in Step 3. with Certificate, Password and Account to test sending of the access token request here.
      </div>
      <div ng-if="$ctrl.model.certResult.token">
        <p>Created access token request</p>
        <pre style="white-space: pre-wrap">{{$ctrl.model.testRequestJson}}</pre>
        <fieldset>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button class="btn btn-primary" type="submit" name="Command" ng-click="$ctrl.getAccessToken()">Post access token request</button>
            </div>
          </div>
        </fieldset>
        <div class="alert alert-info" ng-if="!$ctrl.model.testResponseJson">Post the access token request to show the response here.</div>
        <div ng-if="$ctrl.model.testResponseJson">
          <p>Access token request response</p>
          <pre style="white-space: pre-wrap" id="accessTokenResponse">{{ $ctrl.model.testResponseJson | json}}</pre>
        </div>
      </div>
    </div>
    <div>
      <h2>5. User specific access token</h2>
      <p>
        The server receives the Salaxy access token, and can use it for impersonation.
      </p>
    </div>
    <div>
      <h2>6. Providing the access token for the client</h2>
      <p>
        In most scenarios, you can use the generic Palkkaus.fi as a client. The site can be customized to your own layout and branding. Alternatively you can create your own
        web site (see point 7. below).
      </p>
      <div sng-if="$ctrl.model.testResponseJson && $ctrl.model.testResponseJson.access_token">
        <p>
          The address in test for SSO is <a ng-href="{{$ctrl.getWwwUrl() + '/company#access_token=' + $ctrl.model.testResponseJson.access_token}}" target="_blank">{{
            $ctrl.getWwwUrl() + '/company#access_token=[token]' }}</a>
        </p>
        <p>
          The address in production is <u>https://www2.palkkaus.fi/company/#access_token=[token]</u> but for that, you will need to create a token with production certificates.
        </p>
      </div>
    </div>
    <div class="form-horizontal">
      <h2>7. Using token from your own client / code</h2>
      <h3>Test request structure</h3>
      <p>The request must contain and Authorization header with value:</p>
      <pre>Authorization: Bearer [access_token]</pre>
      <h3>Testing request against Salaxy API</h3>
      <div class="alert alert-info" ng-if="!$ctrl.model.sessionRequestHeader">
        Post the forms in Step 3. and 4. to test the communication with the Salaxy API.
      </div>
      <div ng-if="$ctrl.model.sessionRequestHeader">
        <p>The end point for the test request is <a href="/v02/api/session/current" target="_blank">/v02/api/session/current</a>.</p>
        <p>Authorization header to test</p>
        <pre style="white-space: pre-wrap">{{ $ctrl.model.sessionRequestHeader }}</pre>
        <fieldset>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button class="btn btn-primary" type="submit" name="Command" ng-click="$ctrl.getSession()">Test request against Salaxy API</button>
            </div>
          </div>
        </fieldset>
        <div class="alert alert-info" ng-if="!$ctrl.model.sessionResponseJson">Post the test request to show the response here.</div>
        <div ng-if="$ctrl.model.sessionResponseJson">
          <p>Test request response</p>
          <pre class="pre-scrollable" style="white-space: pre-wrap" id="testResponse">{{$ctrl.model.sessionResponseJson | json}}</pre>
        </div>
      </div>
    </div>
  </div>
</div>