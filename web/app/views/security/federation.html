<div ng-controller="AssertionDemoController as $ctrl">
  <div>
    <div>
      <h2 class="page-header">Federation Flow for Banks</h2>
      <p class="lead">
        The following federation flow is created for banks and other similar partners as an example
        of a federation message from a trusted partner.
        In this case, the partner acts as a <strong>Federation provider</strong>.
      </p>
      <p class="lead">
        A Federation provider must be a trusted partner who is committed to strong authentication methods
        (TUPAS or similar level) and who has signed a contract with Salaxy.
        In practice this means <strong>banks and payment providers</strong> who follow KYC regulations of Finland.
      </p>
      <p>
        The Simple Federation Flow has the following steps:
      </p>
      <img src="/app/img/elems/federation.png" class="img-responsive img-thumbnail" />
      <br /><br />
      <ol>
        <li>
          Prerequisites
          <ul>
            <li>Federation provider role in Salaxy</li>
            <li>Aquire a certificate</li>
          </ul>
        </li>
        <li>Create the federation message based on Open ID Connect UserInfo Response.</li>
        <li>Create a secure JWT token based on the Federation message using the certificate.</li>
        <li>Send a token to Federation end-point.</li>
        <li>
          The end-point will either Create the Salaxy account<br />
          b) ... or if there is an existing salaxy account, ask permission from the user
        </li>
        <li>
          End-point returns the user browser to <b>Success URL</b> with valid access token<br />
          b) ... or to <b>Cancel URL</b> with error message.
        </li>
        <li>
          Use the given Access token in the API requestes (Bearer token).
        </li>
      </ol>
      <p class="lead">
        <strong>If you are interested in this federation flow, please ask more information from our customer service or sales.</strong>
      </p>
      <h3>Alternative Federation flows</h3>
      <p>
        There are alternative Federation flows and message structures that are partly implemented,
        but are not currently in use by our partners.
        Therefore, we have not yet documented them. Please contact us, if you would rather use one of these Flows:
      </p>
      <p>
        <strong>Full OpenID Connect flow</strong>:
        If the Federation Provider already implements the full
        <a href="http://openid.net/specs/openid-connect-core-1_0.html#Overview" target="_blank">Open ID Connect protocol</a>
        as an OpenID Provider, Salaxy is happy to connect to that Flow.
      </p>
      <p>
        The simple Federation flow described on this page is already based OpenID Connect messages,
        but it is a simplified version without some redirects. This is less programming work for the for the Federation Partner.<br />
      </p>
      <p>
        <strong>SAML</strong> is a more legacy method for doing basically the same thing as with OpenID Connect.
        For us, it is also quite simple to implement as the basic structure is the same:
        We just need to implement different messages in SOAP infrastructure.
        For basic describtion of SAML flow <a href="https://en.wikipedia.org/wiki/SAML_2.0">read the Wikipedia article here</a>.<br />
      </p>
      <p>
        Basically, there are again two ways of implementing SAML for this use case:
      </p>
      <ul>
        <li>Full implementation of one of the SAML SSO profiles: Partner is the Identity Provider, Salaxy is Service provider.</li>
        <li>As with the simple Federation flow described on this page, agree on sending just the Response message (Assertion) with approriate security.</li>
      </ul>
      <p class="lead">
        <strong>If you are interested in these alternative flows, please ask more information from our customer service or sales.</strong>
      </p>
    </div>
    <div>
      <h2 id="point1">1. Prerequisites</h2>
      <h3>Federation provider role</h3>
      <p>
        In the production environment, Federation messages can only be used as authentication and authorization by
        partners who have been marked as <strong>Federation providers</strong>.
      </p>
      <p>
        Federation providers are trusted partners who are committed to strong authentication methods
        (TUPAS or similar level) and who have a signed contract with Salaxy.
        In practice this means banks and payment providers who follow KYC regulations of Finland.
        Salaxy back office marks an individual account as a Federation provider after signing a contract.
      </p>
      <p>
        At the time of writing, in test environment any account can be used for federation,
        but we may change that later without advance notice. Let us know if you plan on using this feature!
      </p>
      <h3>Get a certificate</h3>
      <p>
        For step 3. you will also need a Certificate for the Federation Provider account.
        If you do not have a certificate
        <a href="#/settings/authorizations">create one here</a>.
        You need both certificate and its password.
      </p>
    </div>
    <div>
      <h2 id="point2">2. Create the federation message</h2>
      <p>
        The federation message contains the information about the Company account or Personal account
        the federation provider passes to Salaxy. On conceptual level, the federation message looks like this:<br />
        <img src="/app/img/elems/_OpenIdConnect.png" class="img-responsive img-thumbnail" />
      </p>
      <ul>
        <li>
          The model begins from <strong>OpenIdSupportedClaims</strong> in the left of the picture by describing the
          <strong>End-user</strong> in
          <a href="http://openid.net/specs/openid-connect-basic-1_0.html#StandardClaims" target="_blank">OpenID Supported standard Claims</a>.
          <ul>
            <li>This is the root of the message</li>
            <li><strong>address</strong>: Address is a complex claim (JSON) defined in the spec</li>
          </ul>
        </li>
        <li>
          Salaxy defines three additional claims – all complex claims (JSON):
          <ul>
            <li>
              <strong>company</strong>: When the federated entity is a company, the company data is passed here.
              <ul>
                <li>
                  <strong>address</strong>:
                  Company also contains the Address section, which is modeled in identical format as above.
                </li>
              </ul>
            </li>
            <li><strong>person</strong>: Information about the Person as an Employer, Worker or a signing party.</li>
            <li>
              <strong>app</strong>: The Application claims contains data about the application and session
              <ul>
                <li>Most significant part is Role, which provides the authorization</li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
      <h3>OpenID Connect Standard claims</h3>
      <p>
        The message format is based on OpenID Connect UserInfo Response.
        Here is the full documentation for the standard:
        <a href="http://openid.net/specs/openid-connect-basic-1_0.html#StandardClaims"
          target="_blank">http://openid.net/specs/openid-connect-basic-1_0.html#StandardClaims</a>.<br />
        <strong>Salaxy federation supports these claims:</strong>
      </p>
      <table class="table">
        <thead>
          <tr>
            <th>Member</th>
            <th>Type</th>
            <th>Description (from the spec)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>sub</td>
            <td>string</td>
            <td>Subject - Identifier for the End-User at the Issuer.</td>
          </tr>
          <tr>
            <td>name</td>
            <td>string</td>
            <td>End-User's full name in displayable form</td>
          </tr>
          <tr>
            <td>given_name</td>
            <td>string</td>
            <td>Given name(s) or first name(s) of the End-User.</td>
          </tr>
          <tr>
            <td>family_name</td>
            <td>string</td>
            <td>Surname(s) or last name(s) of the End-User.</td>
          </tr>
          <tr>
            <td>picture</td>
            <td>string</td>
            <td>URL of the End-User's profile picture.</td>
          </tr>
          <tr>
            <td>email</td>
            <td>string</td>
            <td>End-User's preferred e-mail address.</td>
          </tr>
          <tr>
            <td>email_verified</td>
            <td>boolean</td>
            <td>True if the End-User's e-mail address has been verified; otherwise false.</td>
          </tr>
          <tr>
            <td>birthdate</td>
            <td>string</td>
            <td>End-User's birthday if personal ID is not known. For workers only</td>
          </tr>
          <tr>
            <td>locale</td>
            <td>string</td>
            <td>End-User's locale, represented as a BCP47 [RFC5646] language tag.</td>
          </tr>
          <tr>
            <td>phone_number</td>
            <td>string</td>
            <td>End-User's preferred telephone number.</td>
          </tr>
          <tr>
            <td>phone_number_verified</td>
            <td>boolean</td>
            <td>True if the End-User's phone number has been verified; otherwise false.</td>
          </tr>
          <tr>
            <td>address</td>
            <td>JSON object</td>
            <td>
              End-User's preferred postal address.<br />
              <strong>NOTE: <code>country</code> must be "fi" or null, which defaults to "fi".</strong><br />
              <strong>NOTE: </strong> Members <code>formatted</code> and <code>region</code> can be passed, but are currently not used.
            </td>
          </tr>
          <tr>
            <td>updated_at</td>
            <td>number</td>
            <td>Time the End-User's information was last updated (seconds from epoc).</td>
          </tr>
          <tr>
            <td colspan="3">
              <strong>NOTE: The following standard claims can be passed, but they are currently not used:</strong>
              middle_name, nickname, preferred_username, profile, website, gender, zoneinfo.
            </td>
          </tr>
        </tbody>
      </table>
      <h3>Salaxy custom claims</h3>
      <p>
        In addition to the standard claims, the message has three custom claims:
      </p>
      <table class="table">
        <thead>
          <tr>
            <th>Member</th>
            <th>Type</th>
            <th>Description (from the spec)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>https://salaxy.com/federation/person</td>
            <td>JSON object (<a href="#UserInfoPersonAccount">UserInfoPersonAccount</a>)</td>
            <td>
              Salaxy extension to Open ID Connect UserInfo claims for Personal account federation.
            </td>
          </tr>
          <tr>
            <td>https://salaxy.com/federation/app</td>
            <td>JSON object (<a href="#UserInfoApplication">UserInfoApplication</a>)</td>
            <td>
              Salaxy extension to Open ID Connect UserInfo claims for Application data.
            </td>
          </tr>
          <tr>
            <td>https://salaxy.com/federation/company</td>
            <td>JSON object (<a href="#UserInfoCompanyAccount">UserInfoCompanyAccount</a>)</td>
            <td>
              Salaxy extension to Open ID Connect UserInfo claims for Company federation.
            </td>
          </tr>
        </tbody>
      </table>
      <h4 id="UserInfoPersonAccount">UserInfoPersonAccount</h4>
      <table class="table">
        <thead>
          <tr>
            <th>Member</th>
            <th>Type</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>can_sign_company</td>
            <td>bool</td>
            <td>
              If true, the user issuer indicates that the current user has the right to sign in behalf of the company / association.
              In this case the provider must also provide a valid Finnish personal ID in personal_id field.
            </td>
          </tr>
          <tr>
            <td>personal_id</td>
            <td>string</td>
            <td>
              A Finnish Personal ID (Henkilötunnus / HETU) for digital signature.
              If can_sign_company is set to true, the issuer should provide a valid personal id here.
            </td>
          </tr>
        </tbody>
      </table>
      <h4 id="UserInfoApplication">UserInfoApplication</h4>
      <table class="table">
        <thead>
          <tr>
            <th>Member</th>
            <th>Type</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>return_link</td>
            <td>string</td>
            <td>Return link back to the calling application. Token is added to this string.</td>
          </tr>
          <tr>
            <td>cancel_link</td>
            <td>string</td>
            <td>Return link in case of error or user cancel.</td>
          </tr>
          <tr>
            <td>role</td>
            <td>string</td>
            <td>
              Application role that the user has in relation to the account in this session.
              <strong>Currently supported values are: "noAccess" and "owner"</strong>.
              Also "unknown" is technically supported.
              This is the default if role is not set.
              But "unknown" is an error in federation process by design so you should never use it.
            </td>
          </tr>
          <tr>
            <td>custom</td>
            <td>JSON</td>
            <td>
              Application specific data that is sent from the federation provider to the end-application
            </td>
          </tr>
        </tbody>
      </table>
      <h4 id="UserInfoCompanyAccount">UserInfoCompanyAccount</h4>
      <table class="table">
        <thead>
          <tr>
            <th>Member</th>
            <th>Type</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>as_company</td>
            <td>bool</td>
            <td>
              If true, the authorization is made for a company account.
              The root user claims are just for a current user acting in behalf of the company.
              If false, the entire CompanyClaims object will be ignored.
            </td>
          </tr>
          <tr>
            <td>business_id</td>
            <td>string</td>
            <td>
              REQUIRED: The Business ID (Y-tunnus / Business Identity Code) is a code given to businesses and organizations by the PRH or the Tax Administration.
              It consists of seven digits, a dash and a control mark, for example 1234567-8.
              Alternative accepted formats incule international VAT number "FI12345678" or number without the dash: "12345678".
            </td>
          </tr>
          <tr>
            <td>type</td>
            <td>string</td>
            <td>
              One of the Salaxy Finnish company type shortcuts: fiOy, fiTm, fiRy, fiYy
              <ul>
                <li><strong>fiOy</strong>: Osakeyhtiö / Limited liability company</li>
                <li><strong>fiTm</strong>: Yksityinen elinkeinonharjoittaja (a.k.a "toiminimi") / sole proprietor</li>
                <li><strong>fiRy</strong>: Rekisteröity yhdistys / Association</li>
                <li><strong>fiYy</strong>: Other finnish company or association</li>
              </ul>
            </td>
          </tr>
          <tr>
            <td>name</td>
            <td>string</td>
            <td>
              Company name - the owner of the Salaxy account and shown in salary calculations, reports and invoices.
              Typically this is the official full name of the company as in Business registry.
            </td>
          </tr>
          <tr>
            <td>picture</td>
            <td>string</td>
            <td>
              URL of the Company profile picture.
              This URL MUST refer to an image file (for example, a PNG, JPEG, or GIF image file), rather than to a Web page containing an image.
              The URL SHOULD specifically reference a company logo picture (PNG preferred) and it should be a square. If not, it may be converted, cropped and/or extended.
            </td>
          </tr>
          <tr>
            <td>email</td>
            <td>string</td>
            <td>The company's preferred e-mail address in relation to salary payments. See corresponding standard claim for details.</td>
          </tr>
          <tr>
            <td>email_verified</td>
            <td>bool</td>
            <td>
              True if the Company's e-mail address has been verified; otherwise false. See corresponding standard claim for details.
              IMPORTANT: In Salaxy federation context, avoid sending non-confirmed e-mails.
              This may later become an error without being considered a breaking change.
            </td>
          </tr>
          <tr>
            <td>phone_number</td>
            <td>string</td>
            <td>Company's preferred telephone number for salary payment purposes. See corresponding standard claim for details.</td>
          </tr>
          <tr>
            <td>phone_number_verified</td>
            <td>bool</td>
            <td>True if the Company's phone number has been verified; otherwise false. See corresponding standard claim for details.</td>
          </tr>
          <tr>
            <td>address</td>
            <td>JSON object</td>
            <td>Company's preferred postal address in relation to salary payments. See corresponding standard claim for details.</td>
          </tr>
        </tbody>
      </table>
      <h3>Example message</h3>
      <p>
        Here is an example of a federation message.
        Select an account for which you wish to generate it to.
      </p>
      <div salaxy-if-role="anon">
        <h3>Authentication required</h3>
        <div class="alert alert-danger">
          The rest of the page requires authentication.
          Please see <a href="#security/index">Getting started section</a> for more information.
        </div>
      </div>
      <salaxy-spinner salaxy-if-role="init" heading="Loading..." text="Please wait."></salaxy-spinner>
      <div salaxy-if-role="auth" class="form-horizontal">
        <salaxy-input-enum
          options="{ current: 'Current account', authorizingAccount: 'One of the accounts that have authorized this account' }"
          label="Target account" name="accountType" ng-model="$ctrl.accountType" type="radio" placeholder="Please select account type..." required></salaxy-input-enum>
        <salaxy-form-group name="currentAccount" label="Current account" ng-if="$ctrl.accountType == 'current'"
          ng-init="$ctrl.getFederationData(true)">
          <div class="salaxy-read-only-value">{{ $ctrl.getCurrentAccount().avatar.displayName }} ({{ $ctrl.getCurrentAccount().id }})</div>
        </salaxy-form-group>
        <salaxy-input-enum ng-if="$ctrl.accountType == 'authorizingAccount'" options="$ctrl.getAuthorizingAccounts()" label="Account Id" name="subject"
          ng-change="$ctrl.getFederationData(true)"
          ng-model="$ctrl.fedModel.accountId" type="select" placeholder="Please select account..." disable-cache="true" required></salaxy-input-enum>
        <uib-tabset>
          <uib-tab heading="JSON Preview">
            <div ng-if="!$ctrl.fedModel.federationData" class="alert alert-info">
              Please select account to create federation data.
            </div>
            <div ng-if="$ctrl.fedModel.federationData">
              <salaxy-json-formatter json="$ctrl.fedModel.federationData" key="'Federation message for ' + $ctrl.fedModel.accountId" open="2"></salaxy-json-formatter>
            </div>
          </uib-tab>
          <uib-tab heading="Edit fields">
            <div ng-if="!$ctrl.fedModel.federationData" class="alert alert-info">
              Please select account to create federation data.
            </div>
            <div ng-if="$ctrl.fedModel.federationData">
              <fieldset>
                <legend>Modify values in sample data</legend>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.sub" name="sub" label="Account ID (sub)"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData['https://salaxy.com/federation/person'].personal_id" name="personal_id" label="Personal ID (hetu)"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.birthdate" name="birthdate" label="Birthdate"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.given_name" name="given_name" label="Given name"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.family_name" name="family_name" label="Family name"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.name" name="name" label="Name"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.phone_number" name="phone_number" label="Phone"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.email" name="email" label="Email"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData.picture" name="picture" label="Picture"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData['https://salaxy.com/federation/app'].return_link" name="return_link" label="Redirect (return_link)"></salaxy-input>
                <salaxy-input ng-model="$ctrl.fedModel.federationData['https://salaxy.com/federation/app'].cancel_link" name="cancel_link" label="Cancel URL (cancel_link)"></salaxy-input>
              </fieldset>
            </div>
          </uib-tab>
          <uib-tab heading="JSON Text editor">
            <p>Here, you can copy paste a custom federation data. If there is any text below, it overrides the JSON in other tabs.</p>
            <div>
              <button type="button" class="btn btn-default" ng-click="$ctrl.copyCurrentUser()">Copy current user</button>
              <button type="button" class="btn btn-default" ng-click="$ctrl.fedModel.customFederationDataJson = null">Clear: Use example message</button>
            </div>
            <salaxy-textarea maxlength="1000000" name="customFederationDataJson" rows="5" ng-model="$ctrl.fedModel.customFederationDataJson"
              label="User info message"></salaxy-textarea>
          </uib-tab>
        </uib-tabset>
      </div>
    </div>
    <div salaxy-if-role="auth">
      <div>
        <h2 id="point3">3. Create a secure JWT token</h2>
        <p>
          Next, you need to create a secure JWT token based on the Federation message from step 2. using the certificate you got in step 1.
        </p>
        <p>
          The following code block shows how to do this in C#.
          Node.js implementation can be based on
          <a href="https://github.com/auth0/node-jsonwebtoken" target="_blank">https://github.com/auth0/node-jsonwebtoken</a>.
        <pre class="code pre-scrollable">
public string CreateToken(byte[] certificateBytes, string password, OpenIdUserInfoSupportedClaims claimsData)
{
//Load certificate
var certificate = new X509Certificate2(certificateBytes, password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
//Make header, add signing certificate
var header = new JwtHeader(new X509SigningCredentials(certificate));
header.Add(JwtHeaderParameterNames.X5c, new string[] { Convert.ToBase64String( certificate.RawData) });
//Make payload - see below
var payload = GetJwtPayload(claimsData);
//Make token and sign
var token = new JwtSecurityToken(header, payload);
//Serialize
var tokenHandler = new JwtSecurityTokenHandler();
string jwt = tokenHandler.WriteToken(token);
return jwt;
}
public JwtPayload GetJwtPayload(OpenIdUserInfoSupportedClaims claimsData)
{
var payload = new JwtPayload();
payload.AddClaim(new Claim("sub", claimsData.sub));
payload.AddClaim(new Claim("name", claimsData.name));
payload.AddClaim(new Claim("given_name", claimsData.given_name));
payload.AddClaim(new Claim("family_name", claimsData.family_name));
payload.AddClaim(new Claim("picture", claimsData.picture));
payload.AddClaim(new Claim("email", claimsData.email));
payload.Add("email_verified", claimsData.email_verified);                   // Type: boolean (nullable)
payload.AddClaim(new Claim("birthdate", claimsData.birthdate?.ToString("yyyy-MM-dd")));
payload.AddClaim(new Claim("phone_number", claimsData.phone_number));
payload.Add("phone_number_verified", claimsData.phone_number_verified);     // Type: boolean (nullable)
payload.Add("address", claimsData.address);                                 // Type: OpenIdUserInfoAddress
payload.Add("updated_at", claimsData.updated_at);                           // Type: decimal (nullable)
payload.Add("https://salaxy.com/federation/company", claimsData.company);   // Type: UserInfoCompanyAccount
payload.Add("https://salaxy.com/federation/person", claimsData.person);     // Type: UserInfoPersonAccount
payload.Add("https://salaxy.com/federation/app", claimsData.app);           // Type: UserInfoApplication
// NOTE: These standard claims are currently ignored by Salaxy:
//       middle_name, nickname, preferred_username, profile, website, gender, zoneinfo
return payload;
}
</pre>
        </p>
        <div class="form-horizontal">
          <form>
            <fieldset>
              <div class="form-group disabled">
                <label class="control-label col-sm-4" for="certificateFile">Upload X.509 certificate (.pfx file)</label>
                <div class="col-sm-8">
                  <button type="button" class="btn btn-default" ngf-multiple="false" ngf-select="$ctrl.uploadCertificateFile($file, true)" name="certificateFile" id="certificateFile"
                    class="form-control">Select file...</button>
                </div>
              </div>
              <salaxy-textarea maxlength="1000000" name="certificateBytes" rows="5" ng-model="$ctrl.fedModel.certificateBytes" label="...or alternatively paste certificate:"></salaxy-textarea>
              <salaxy-input name="certificatePwd" type="password" ng-model="$ctrl.fedModel.certificatePwd"></salaxy-input>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <button class="btn btn-primary" type="button" ng-click="$ctrl.getFederationData()">Create authorization grant request</button>
                </div>
              </div>
            </fieldset>
          </form>
          <div ng-if="!$ctrl.fedModel.token" class="alert alert-info">Post the form with Certificate and Password to show the token here</div>
          <form ng-if="$ctrl.fedModel.token" method="post" action="{{ $ctrl.getApiUrl('/security/TestOpenIdConnectUserInfo') }}" target="_blank" class="form-horizontal">
            <p>
              You can check the payload of the the token e.g. in
              <a href="https://jwt.io/" target="_blank">https://jwt.io/</a>
              (does not support certificate signature validation)
              ...
              or you can test the token using the form here.
            </p>
            <salaxy-textarea maxlength="1000000" name="token" rows="5" ng-model="$ctrl.fedModel.token" label="JWT token to test"></salaxy-textarea>
            <div class="form-group">
              <div class="col-sm-offset-4 col-sm-8">
                <button class="btn btn-primary" type="submit">Send to test page (new window)</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div>
        <h2 id="point4">4. Send the token to Federation end-point</h2>
        <p>
          Next, you will need to send the token to Salaxy's Federation end-point.
          This can be done either with GET or POST.
        </p>
        <p>The end-point will either:</p>
        <ol>
          <li>Create the Salaxy account and automatically implicitly accept authorization to requesting account</li>
          <li>If there is an existing salaxy account, ask user to explicitly give authorization to requesting account</li>
        </ol>
        <div ng-if="!$ctrl.fedModel.token" class="alert alert-info">Post the form in Step 3. with Certificate and Password to test sending of the token here.</div>
        <form ng-if="$ctrl.fedModel.token" method="post" action="{{ $ctrl.getApiUrl('/security/federation') }}" target="_blank" class="form-horizontal">
          <salaxy-textarea maxlength="1000000" name="token" rows="5" ng-model="$ctrl.fedModel.token" label="JWT token to test"></salaxy-textarea>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button class="btn btn-primary" type="submit">Do federation (new window)</button>
            </div>
          </div>
        </form>
      </div>
      <div>
        <h2 id="point5">5. A salaxy account is created</h2>
        <p>
          Salaxy will check if an account already exists based on either
          Company Business ID (<code>company.business_id</code> when <code>company.as_company=true</code>) or
          Personal ID (<code>person.personal_id</code>).
        </p>
        <p>
          In a normal use-case, <strong>the account is a new one and it is created</strong>.
          The federation provider is automatically granted Authorization to that new account.
        </p>
        <p>
          <strong>If the account already exists</strong>,
          the owner must be asked for permission so that the existing data can be given to the Federation provider.
          Salaxy will show a Permission check screen to the end user.
        </p>
      </div>
      <div>
        <h2 id="point6">6. Browser returns to the calling application</h2>
        <p>
          The user browser will return to your application either to:
        </p>
        <ul>
          <li><b>Success URL</b> with valid access token</li>
          <li>...or <b>Cancel URL</b> with error message.</li>
        </ul>
      </div>
      <div>
        <h2 id="point7">7. Browser JavaScript uses the access token</h2>
        <p>
          The JavaScript receives the access token and sets it to the Bearer token of each request to Salaxy API.
        </p>
        <p>&nbsp;</p>
      </div>
    </div>
  </div>
</div>