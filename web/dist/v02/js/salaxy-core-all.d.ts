declare module "model/oauth2" {
    /** Based on standard https://tools.ietf.org/html/rfc6749 . */
    export interface OAuthMessage {
        /** The client identifier issued to the client during the registration process. */
        client_id?: string | null;
        /** The client secret. The client MAY omit the parameter if the client secret is an empty string. */
        client_secret?: string | null;
        /** The value MUST be one of "code" for requesting an authorization code, "token" for  requesting an access token(implicit grant), or a registered extension value. */
        response_type?: OAuthResponseType | null;
        /** The value of the scope parameter is expressed as a list of space-delimited, case-sensitive strings.The strings are defined by the authorization server. */
        scope?: string | null;
        /** An opaque value used by the client to maintain state between the request and callback.The authorization server includes this value when redirecting the user-agent back to the client. The parameter SHOULD be used for preventing cross-site request forgery. */
        state?: string | null;
        /** After completing its interaction with the resource owner, the authorization server directs the resource owner's user-agent back to the client. */
        redirect_uri?: string | null;
        /** A single ASCII [USASCII] error code. */
        error?: OAuthError | null;
        /** Human-readable ASCII [USASCII] text providing additional information, used to assist the client developer in understanding the error that occurred. */
        error_description?: string | null;
        /** A URI identifying a human-readable web page with information about the error, used to provide the client developer with additional information about the error. */
        error_uri?: string | null;
        /** Requested grant type. */
        grant_type?: OAuthGrantType | null;
        /** The authorization code received from the authorization server. */
        code?: string | null;
        /** The access token issued by the authorization server. */
        access_token?: string | null;
        /** The type of the token issued. */
        token_type?: OAuthTokenType | null;
        /** The lifetime in seconds of the access token.  For example, the value "3600" denotes that the access token will expire in one hour from the time the response was generated. If omitted, the authorization server SHOULD provide the expiration time via other means or document the default value. */
        expires_in?: number | null;
        /** Auth0 connection */
        connection?: string | null;
        /** The resource owner username. */
        username?: string | null;
        /** The resource owner password. */
        password?: string | null;
        /** The refresh token, which can be used to obtain new access tokens. */
        refresh_token?: string | null;
        /** The value of the "assertion" parameter MUST contain a single JWT. */
        assertion?: string | null;
        /** The id token issued by the authorization server. */
        id_token?: string | null;
        /** Salaxy specific onboarding uri. */
        salaxy_onboarding_uri?: string | null;
        /** User interface language. */
        salaxy_language?: string | null;
        /** Users role. */
        salaxy_role?: string | null;
    }
    /** OAuthGrantType enumeration */
    export enum OAuthGrantType {
        Undefined,
        Authorization_code,
        Password,
        Client_credentials,
        Refresh_token,
        JwtBearer
    }
    /** OAuthResponseType enumeration */
    export enum OAuthResponseType {
        Undefined,
        Code,
        Token
    }
    /** OAuthTokenType enumeration */
    export enum OAuthTokenType {
        Undefined,
        Bearer,
        Mac
    }
    /** OAuthError enumeration */
    export enum OAuthError {
        Undefined,
        Invalid_request,
        Unauthorized_client,
        Access_denied,
        Unsupported_response_type,
        Invalid_scope,
        Server_error,
        Temporarily_unavailable
    }
    /** OAuthSalaxyAuthorizeMode enumeration */
    export enum OAuthSalaxyAuthorizeMode {
        Undefined,
        Sign_in,
        Sign_up
    }
}
declare module "model/v01" {
    /** Absences for a single Worker. */
    export interface WorkerAbsences {
        /** Account ID for the worker whose absences are recorded.
    This is required and currently always the same as Id, but this may later change:
    Id may may be something else in the future. */
        workerId: string;
        /** Collection of absence periods. */
        periods?: AbsencePeriod[] | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Period of absence */
    export interface AbsencePeriod {
        /** Period for the absance. */
        period?: DateRange | null;
        /** Cause / type of the absence */
        causeCode?: AbsenceCauseCode | null;
        /** If true, the absence is paid by the employer. */
        isPaid?: boolean | null;
        /** If true, annual leaves are accrued from the absence. If false, these days are deducted from the accrual.
    By default should always follow the IsPaid (in the UI / calling logic), but in exceptional cases, user can set this specifically true or false. */
        isHolidayAccrual?: boolean | null;
        /** Amount of salary that is paid for the absense.
    Currently not in use: Would be used in Incomes Register integration. */
        amount?: number | null;
        /** Additional information as recorded by the Employer. */
        notes?: string | null;
    }
    /** Describes the range of dates from start to end and the number of days in between. */
    export interface DateRange {
        /** Start date of the period */
        start?: string | null;
        /** End date of the period. */
        end?: string | null;
        /** Number of days in the range.
    Depending of the context, this may be mathematical (end - start) or
    explicitly specified (typically working days).
    Typically an integer, and calculations and UI's may choose to round it. */
        daysCount?: number | null;
        /** The collection of days contained in the DateRange if specified by the user.
    Alternative to DaysCount. If set, should also set DaysCount for backward compatibility. */
        days?: string[] | null;
    }
    /** A company account - note that this can also be an association etc. - something that has a Company ID. */
    export interface CompanyAccount {
        /** Information about the verification of the account.
    NOTE that this object may not be present in many method calls - this is because it contains confidential information. */
        identity?: Identity | null;
        /** Majority shareholders (over 25% stake) are stored in this field.
    Ownership is a list of Shareholder objects. */
        readonly ownership?: Ownership | null;
        /** High level company type */
        companyType?: CompanyType | null;
        /** Entity type of the account.
    Type PersonCreatedByEmployer means that the Worker has not yet signed into the system and there may be multiple instances of the same logical person (Official ID) - one for each employer. */
        entityType?: LegalEntityType | null;
        /** If true the object Account is and Employer account - it has the EmployerInfo object */
        isEmployer?: boolean | null;
        /** If true the object Account is and Worker account - it has the WorkerInfo object */
        isWorker?: boolean | null;
        /** If true, the account has been verified. Typically this means a digitally signed contract.
    This means that the account also has an Identity object.
    However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
        isVerified?: boolean | null;
        /** Avatar is the visual representation of the Account. */
        avatar?: Avatar | null;
        /** Contact information for the Account. */
        contact?: Contact | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Identity is the official identity of an actor. This part is not freely editable as changing it requires signature or some other means. Identity is also often fetched from external sources such as govenment registries. */
    export interface Identity {
        /** This is an official identifier of the actor:
    Finnish HETU (Soc. sec. number) for a Person or Y-tunnus (Company ID for a company or association) */
        officialId?: string | null;
        /** First name OR company name */
        firstName?: string | null;
        /** Last name
    Available only to real persons (as opposed to companies). */
        lastName?: string | null;
        /** The latest authorization contract that is the bases for this Identity */
        contract?: AuthorizationContract | null;
        /** Tha bank account number of the Worker - in Iban format. */
        ibanNumber?: string | null;
        /** Indicates whether the principal user of the customer is a politically exposed person or not. */
        isPep?: boolean | null;
        /** Mainly for company accounts, but in special cases for Persons,
    this is the main Contact party and usually the signer for the account. */
        primaryContact?: ContractParty | null;
        /** List of roles which this account is member of. */
        roles?: Role[] | null;
    }
    export interface Ownership {
        /** If true, authorities require tracking the shareholders.
    In Finland, with current legistlation, this is if a shareholder has over 25% of the shares. */
        isRequiredTracking?: boolean | null;
        /** List of shareholders for the company */
        shareHolders?: Shareholder[] | null;
    }
    /** Defines an avatar for an account, profile etc. - mainly the image that should be shown, names and short description */
    export interface Avatar {
        /** Entity type: person/company */
        entityType?: LegalEntityType | null;
        /** First name or company name */
        firstName?: string | null;
        /** Last name, for companies, this should be null */
        lastName?: string | null;
        /** Display name. For a person this is 'FirstName LastName' (auto-created). */
        displayName?: string | null;
        /** Sortable name for ordered lists etc. For a person this is 'LastName, FirstName' (auto-created).
    Depending on the data storage, this may be all-lowercase for easier sorting and search. */
        sortableName?: string | null;
        /** Type of the Avatar picture. */
        pictureType?: AvatarPictureType | null;
        /** Color - currently only used by type Icon */
        color?: string | null;
        /** Initials - currently only used by type Icon */
        initials?: string | null;
        /** URL of the picture if specified as picture (null in a case of type Icon) */
        url?: string | null;
        /** Short description of the user.
    This may be overriden by a context specific value by the business logic. */
        description?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Common contact information object for Person, Company, Location etc. */
    export interface Contact {
        /** Contact e-mail address. Always lower invariant, trimmed and empty string set to null. */
        email?: string | null;
        /** Telephone number */
        telephone?: string | null;
        /** Street address */
        street?: string | null;
        /** Postal code */
        postalCode?: string | null;
        /** City or community (postitoimipaikka) */
        city?: string | null;
        /** This is the ISO code for country.
    Currently, it should always be "fi". */
        countryCode?: string | null;
    }
    /** Contract / authorization document that is the bases for Salaxy acting in behalf of this account */
    export interface AuthorizationContract {
        /** Time when the contract was signed */
        signedAt?: string | null;
        /** If true, there is a valid authorization - the account has been validated. */
        isSigned?: boolean | null;
        /** Type of the authorization */
        authorizationType?: AuthorizationType | null;
        /** A person or other party that authorized the current person. */
        authorizedBy?: Authority | null;
    }
    /** A signing party in a contract. */
    export interface ContractParty {
        /** Identifier for the party: Currently always an account, but may be an external ID later. */
        id?: string | null;
        /** Avatar for the Party.
    Note that this is not automatically fetched by all methods: Especially when IsSelf is true. */
        avatar?: Avatar | null;
        /** Contact information - note that for most signatures, only E-mail and phone number are required. */
        contact?: Contact | null;
        /** True if Profile is the current Authorization.
    If set to true, will set current Authorization as this contract party.
    If set to false AND was previously true, will set the Profile properties null (unless explicitly set to something else) */
        isSelf?: boolean | null;
        /** Official ID if needed in signature */
        officialId?: string | null;
        /** Role of the person - justification for authorization */
        role?: string | null;
        /** True if the person has signed the contract. */
        isSigned?: boolean | null;
        /** Reserved for cases where there are several signees and each has a separate signature document. */
        signatureUrl?: string | null;
        /** Bank account IBAN number */
        iban?: string | null;
        /** Party type (Person or Prospect) */
        contractPartyType?: ContractPartyType | null;
        /** If true, contact is an employer */
        isEmployer?: boolean | null;
    }
    /** Defines a shareholder. At the moment, this is only a name and it is used only if the person has more than 25% of the shares. Expect this object to be developed with, Ownership percent or number of shares, Id or other such link to the actual person etc. */
    export interface Shareholder {
        /** Name of the shareholder. */
        name?: string | null;
    }
    /** A person or other party that authorized the current person. */
    export interface Authority {
        /** Name of the authority. */
        name?: string | null;
        /** Telephone number of the person. This is currently optional.
    Idea is to send SMS invitations in some scenarios. */
        telephone?: string | null;
        /** E-mail of the person. This is currently not used, but is expected to be used in the future.
    So fill it in as it was required unless you have very compelling reasons. */
        email?: string | null;
        /** Finnish Person ID is the key to signature. This is currently always required. */
        personalId?: string | null;
        /** The Palkkaus.fi Account ID. This is optional and should not be set if the ID is the same as the target account / Person. */
        accountId?: string | null;
        /** Date for the authorization. */
        authorizedAt?: string | null;
    }
    /** An account for a Person - can be both Employer and Worker */
    export interface PersonAccount {
        /** Information about the verification of the account.
    NOTE that this object may not be present in many method calls - this is because it contains confidential information. */
        identity?: Identity | null;
        /** Type of the last login - the user role */
        lastLoginAs?: WebSiteUserRole | null;
        /** The worker information in case the person is a Worker */
        workerInfo?: WorkerInfo | null;
        /** Entity type of the account.
    Type PersonCreatedByEmployer means that the Worker has not yet signed into the system and there may be multiple instances of the same logical person (Official ID) - one for each employer. */
        entityType?: LegalEntityType | null;
        /** If true the object Account is and Employer account - it has the EmployerInfo object */
        isEmployer?: boolean | null;
        /** If true the object Account is and Worker account - it has the WorkerInfo object */
        isWorker?: boolean | null;
        /** If true, the account has been verified. Typically this means a digitally signed contract.
    This means that the account also has an Identity object.
    However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
        isVerified?: boolean | null;
        /** Avatar is the visual representation of the Account. */
        avatar?: Avatar | null;
        /** Contact information for the Account. */
        contact?: Contact | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Information directly related to Employment (as Worker) */
    export interface WorkerInfo {
        /** The Tax card that is used by default. */
        defaultTaxCard?: TaxcardSnapshot | null;
    }
    /** Basic information about the taxcard: This object has the infromation printed in SalarySlip etc. but not the full salaries paid to the taxcard etc. */
    export interface TaxcardSnapshot {
        /** Year that the tax card is valid for. Typically this means from February of this year to January of the following. */
        forYear: number;
        /** Validity for the taxcard as expressed in the card.
    Note that the end date may not be reliable if new taxcard has replaced this one. */
        validity?: DateRange | null;
        /** Tax percent as percent. I.e. for 50% set 50, not 0.5. */
        taxPercent?: number | null;
        /** Income up to which the TaxPercent can be used.
    Any income above the limit is taxed with ExcessTaxPercent. */
        incomeLimit?: number | null;
        /** Tax percentage that is used in calculating the widthholding tax for the part of income above the IncomeLimit. */
        taxPercent2?: number | null;
        kind?: TaxcardKind | null;
        /** Uri to the file copy of the tax card.
    Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
        fileUri?: string | null;
        /** Uri to the preview image of the tax card.
    Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
        previewUri?: string | null;
    }
    /** User certificate. */
    export interface Certificate {
        /** Certificate thumbprint. */
        thumbprint?: string | null;
        /** Certificate bytes. */
        bytes?: string | null;
        /** End user description for the certificate. */
        title?: string | null;
        /** Password for issuing a new certificate. Not persisted. */
        password?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Account - either Person or Company - is a juridical entity Paying or receiving salaries, making contracts etc. It may be related to zero, one or multiple UserAccounts/Credentials. */
    export interface AccountBase {
        /** Entity type of the account.
    Type PersonCreatedByEmployer means that the Worker has not yet signed into the system and there may be multiple instances of the same logical person (Official ID) - one for each employer. */
        entityType?: LegalEntityType | null;
        /** If true the object Account is and Employer account - it has the EmployerInfo object */
        isEmployer?: boolean | null;
        /** If true the object Account is and Worker account - it has the WorkerInfo object */
        isWorker?: boolean | null;
        /** If true, the account has been verified. Typically this means a digitally signed contract.
    This means that the account also has an Identity object.
    However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
        isVerified?: boolean | null;
        /** Avatar is the visual representation of the Account. */
        avatar?: Avatar | null;
        /** Contact information for the Account. */
        contact?: Contact | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Represents a user credential in session: This credential is typically based on Claims and is availble without going to database. */
    export interface SessionUserCredential {
        /** The currently selected Palkkaus.fi-account ID.
    This is the owner ID that defines the access to user objects.
    May also be null if account has not been created yet. */
        currentAccountOwnerId?: string | null;
        /** The currently selected Palkkaus.fi-account ID.
    This is the ID that can be used to fetch the account object itself. */
        currentAccountUniqueId?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** Unique identitier for the user ID coming from the Auth0 database:
    '[MethodText]|[ProviderId]', e.g. 'facebook|12345678901234567'
    (Number may contain more characters - not sure for the exact cahracters count) */
        id?: string | null;
        /** Avatar object that contains the visual reresentation of the User account. */
        avatar?: Avatar | null;
        /** E-mail address if provided by the authentication provider. */
        email?: string | null;
        /** The ultimate authentication provider that authenticated the user, e.g. Facebook, Google. */
        authenticationMethod?: AuthenticationMethod | null;
    }
    /** Calculation is the most important model in the Palkkaus.fi API. We suggest you open the separately provided documentation and especially the class diagram to understand the whole model in detail. */
    export interface Calculation {
        /** The employer object for this calculation.
    Typically, you do not need to set this before the actual payment process. */
        employer?: CalcEmployer | null;
        /** The Worker (employee) that has performed the work. */
        worker?: CalcWorker | null;
        /** Information about the workflow and state of the calculation. */
        workflow?: CalcWorkflow | null;
        /** The main salary element.
    Alternatively the salary can be set in the Rows object - this will be the only way to set multiple salary rows (not yet supported). */
        salary?: Salary | null;
        /** Set of rows that make up the salary calculation:
    Expenses, Benefits, deductions etc. Note that side costs are not rows. */
        rows?: UserDefinedRow[] | null;
        /** DEPRECIATED: Usecase will be replacing this functionality in a next major release.
    The Framework Agreement (TES) parameters that affect the framework based side costs etc. of this calculation */
        framework?: TesParameters | null;
        /** A usecase for creating and editing salary calculations.
    In practice, a usecase is either a user interface or a microservice that provides a salary calculation
    and it may be implemented outside Salaxy.
    Provides the possibility to store useacase specific data as part of calculation.
    NOTE: This functionality will replace Framework property in a next major release. */
        usecase?: UsecaseData | null;
        /** The results of the calculation. In the API, this is a read-only object. */
        result?: CalculationResult | null;
        /** Information about the work that is not directly related to calculating salary and expenses. */
        info?: CalcInfo | null;
        /** Sharing of the calculation. This object cannot be modified directly,
    use specialized methods in Calculations service. */
        sharing?: CalculationSharing | null;
        /** Worktime data that is stored as part of Salary calculation:
    work days, absences, holidays and holiday accrual.
    In the future may also contain relevant part of timesheet data (work hours). */
        worktime?: CalcWorktime | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Employer of a calculation */
    export interface CalcEmployer {
        /** Display image information and type of employer.
    NOTE: Recalculate-method retains the values for display purposes if AccountId or IsSelf is not set.
    This is mainly for demo and testing in anonymous mode.
    However, in storage methods, the value is reset based on values of AccountId or IsSelf
    even if they are not set: Anything stored in Avatar will be ignored. */
        avatar?: Avatar | null;
        /** The Palkkaus.fi Account identifier for the Employer. */
        accountId?: string | null;
        /** True if Profile is the current Authorization.
    If set to true, will set current Authorization as this contract party.
    This overrides any value in AccountId, so be sure to switch this back to false if this is switchable.
    Setting to False does not have any immediate effect. */
        isSelf?: boolean | null;
    }
    /** Worker object for the current calculation */
    export interface CalcWorker {
        /** Palkkaus.fi Account Id for the worker.
    Alternatively you can set the Payment data with the required infromation */
        accountId?: string | null;
        /** Data that is needed for salary payment.
    This object may be null if AccountId has been set.
    Also, if AccountId has been set, these properties will be ignored:
    The Worker will decide on payment properties herself. */
        paymentData?: CalcWorkerPaymentData | null;
        /** Display image and properties of the Worker.
    Based on either the AccountId or PaymentData */
        avatar?: Avatar | null;
        /** True if Profile is the currently Account
    If set to true, will set current Account as this contract party.
    This overrides any value in AccountId, so be sure to switch this back to false if this is switchable.
    Setting to False does not have any immediate effect. */
        isSelf?: boolean | null;
        /** Date of birth for the Worker - this affects the side costs.
    Please also set the DateOfBirthAccuracy, if you set this property.
    If PaymentData or AccountId lead to a valid Social security number, it will override any age set here. */
        dateOfBirth?: string | null;
        /** Accuracy of the date of birth
    If PaymentData or AccountId lead to a valid Social security number, it will override any age set here. */
        dateOfBirthAccuracy?: DateOfBirthAccuracy | null;
        /** New Widthholding tax logic that is based on separately stored tax cards. */
        tax?: CalcTax | null;
        /** Tax percent that is used in visualization - this is not necessarily the final tax percent.
    Tax percent as percent. I.e. for 50% set 50, not 0.5.
    If PaymentData or AccountId lead to a valid tax card type and percent, it will override any value set here. */
        taxPercentForVisualization?: number | null;
    }
    /** Defines the properties of salary calculation workflow - typically the workflow from draft to Payment. */
    export interface CalcWorkflow {
        /** Status of the calculation from Draft to PaymentSucceeded */
        status?: CalculationStatus | null;
        /** Time when this calculation was paid (if it was paid) to Palkkaus service. */
        paidAt?: string | null;
        /** Time when the salary was paid by Palkkaus service. */
        salaryPaidAt?: string | null;
        /** The estimated date when the salary is withdrawable by the worker. */
        salaryDate?: string | null;
        /** The requested date for the SalaryDate from the employer. */
        requestedSalaryDate?: string | null;
    }
    /** Defines the main salary that is the bases for the calculation */
    export interface Salary {
        /** Type of the salary being calculated */
        kind: SalaryKind;
        /** Amount of e.g. hours, months for the salary. Default is 1. */
        amount?: number | null;
        /** Price of one unit - e.g. hour, month. */
        price: number;
        /** E.g. hour, month. Typically this parameter can be left empty/null and the unit is set according to type. */
        unit?: string | null;
        /** Optional message for the salary row that is shown in reports. If null, will be set according to type. */
        message?: string | null;
        /** If set to true, will calculate the household deduction. Applies only to natural persons - as oppsed to companies. */
        isHouseholdDeductible?: boolean | null;
        /** If tax IsHouseholdDeductible is set to true, please also select the category (categories).
    Note that "OwnPropety" is a default, it does not need to be set: Only set RelativesProperty if necessary. */
        taxDeductionCategories?: TaxDeductionWorkCategories | null;
    }
    /** This is a user defined calculation row as opposed to CalculationRow that may be either based on a user defined row or generated by the system based on e.g. TES parameters. */
    export interface UserDefinedRow {
        /** Zero based row index that can be used to match the row to result rows or when deleting.
    NOTE: The Row ID is given values on the fly so setting this will not have any effect at the moment.
    It may reorder the entries in later implementations. */
        rowIndex?: number | null;
        /** Logical type of the row */
        rowType?: CalculationRowType | null;
        /** Description text of the row that is shown in reports. If null, will be set according to type. */
        message?: string | null;
        /** Count for the row - default is one */
        count?: number | null;
        /** Price for the row */
        price?: number | null;
        /** Unit for the row. Guessed based on the RowType and count, but you are better of setting it to be sure. */
        unit?: CalculationRowUnit | null;
        /** Identifier in the source system is a key defined by a source system / partner system.
    This is a pass-through string that is passed to the result calculations. */
        sourceId?: string | null;
        /** Usecase specific data */
        data?: {
            [key: string]: any;
        } | null;
    }
    /** Captures the collection of parameters that different Framework agreements (TES) have. */
    export interface TesParameters {
        /** Type of the work framework */
        type?: FrameworkAgreement | null;
        /** Subtype when one framework has several options */
        subType?: TesSubtype | null;
        /** Marker that the payments defined by framework agreement are included in the salary (will not be added by the calculation). */
        isTesIncludedInSalary?: boolean | null;
        /** Number of days may affect calculation of different payments calculated based on a Framework agreement */
        numberOfDays?: number | null;
        /** Travel expences per day */
        dailyTravelExpenses?: number | null;
        /** The amount of kilometers travelled by day - used to define the amount of travel expenses in aomse framework agreements. */
        dailyTravelExpensesKm?: number | null;
        /** Daily expenses amount */
        dailyExpenses?: number | null;
        /** If true will calculate the Finnish child care subsidy */
        isYksityisenHoidonTuki?: boolean | null;
        /** Amount of Finnish child care subsidy */
        yksityisenHoidonTukiAmount?: number | null;
    }
    /** A usecase for creating and editing salary calculations and other . In practice, a usecase is either a user interface or a microservice that provides the bases for the object. Provides the possibility to store useacase specific data as part of object. NOTE: For Calculations, this functionality will replace TesParameters in the 3.0 version. */
    export interface UsecaseData {
        /** Key for the usecase based on which it is resolved.
    Later, this may resolve to a user interface, microservice or a to manifest registering one.
    For now, just use a unique string starting with a domain you own. */
        uri?: string | null;
        /** A short label of that can be shown to the end user. */
        label?: string | null;
        /** Optional description of the use case for the end user.
    Shown in detail views when explaining the logic of the usecase. */
        description?: string | null;
        /** Usecase specific data */
        data?: {
            [key: string]: any;
        } | null;
    }
    export interface CalculationResult {
        /** Calculation totals that are common to both Worker and Employer. */
        totals?: TotalCalculationDTO | null;
        /** Rows that are compatible with Incomes Register (tulorekisteri) process.
    These are available only for calculations paid after 7.2.2019 (TODO: estimation as of writing). */
        irRows?: IrRow[] | null;
        /** The calculation from the Employer point-of-view */
        employerCalc?: EmployerCalculationDTO | null;
        /** The calculation from the Worker point-of-view */
        workerCalc?: WorkerCalculationDTO | null;
        /** The result rows. Note that this collection also contains the main Salary entry as one of the rows. */
        rows?: ResultRow[] | null;
        /** Responsibilities of Employer and parameters that affect those responsibilities. */
        responsibilities?: IEmployerResponsibilities | null;
        /** The validation result for the current calculation.
    Please note that this is not serialized into the data storage. */
        validation?: ApiValidation | null;
    }
    /** Informational object that stores data about the salary calculation / payment that is not directly related to calculating the salary. */
    export interface CalcInfo {
        /** Start date of the work */
        workStartDate?: string | null;
        /** End date of the work */
        workEndDate?: string | null;
        /** Description of the work for reporting purposes. Max 32 chars. */
        workDescription?: string | null;
        /** Message from worker to employer - when worker sends the salary calculation to the employer. */
        workerMessage?: string | null;
        /** Payment reference number in payment service */
        paymentId?: string | null;
        /** Notes related to the payment and other backoffice operations:
    For example the justification why the sum is in calculation vs. Paytrail payment. */
        backofficeNotes?: string | null;
        /** Occupation classification, used at least for Accident insurance purposes, but may be used for other reporting.
    For Finnish Salaries use the Statistics Finland
    "Classification of Occupations 2010" (TK10): https://www.stat.fi/meta/luokitukset/ammatti/017-2018-05-15/index_en.html */
        occupationCode?: string | null;
        /** Calculated pension insurance payment date. */
        pensionPaymentDate?: string | null;
        /** Pension insurance reference number. */
        pensionPaymentRef?: string | null;
        /** Pension insurance payment specifier. */
        pensionPaymentSpecifier?: string | null;
    }
    /** Defines the properties related to sharing and sending of the calculation. */
    export interface CalculationSharing {
        /** Type of sharing for this calculation. */
        type?: SharingLink | null;
        /** Sharing link. */
        link?: string | null;
        /** If true, the sharing is protected by password. */
        hasPassword?: boolean | null;
    }
    /** Worktime data that is stored as part of Salary calculation: work days, absences, holidays and holiday accrual. In the future may also contain relevant part of timesheet data (work hours). */
    export interface CalcWorktime {
        /** Specification for the holiday calculation */
        holidaySpec?: HolidaySpecificationForYear | null;
        /** Parameters related to Monthly salary.
    Could theoretically be used for other long periods, namely Weekly salary, but that is not actively supported or tested. */
        monthly?: CalcMonthlyData | null;
        /** Parameters related to Hourly or Performance based salary.
    Could theoretically be used for other long periods, namely Weekly salary, but that is not actively supported or tested. */
        hourly?: CalcHourlyData | null;
        /** Parameters related to Holiday Bonus. */
        bonus?: CalcHolidayBonus | null;
        /** Number of days of non-paid absences.
    These are deducted from the Monthly salary, not from Hourly salary at the moment. */
        absencesDays?: number | null;
        /** Accruals for each calendar month in this calculation. */
        accruals?: MonthlyHolidayAccrual[] | null;
        /** Number of paid Leave days that is paid in this salary calculation / for this period. */
        leavesDays?: number | null;
        /** Price for a single holiday day */
        leavesDailySalary?: number | null;
        /** Describes the working days in the period.
    Currently, uses the DaysCount, but later the full days array will be taken to use.
    This includes the absence days (divider in AbsencesDaySalary). */
        workDays?: DateRange | null;
    }
    /** The required information for paying the Salary to Worker. */
    export interface CalcWorkerPaymentData {
        /** First name of the person. */
        firstName?: string | null;
        /** Last name / Surname of the person. */
        lastName?: string | null;
        /** Gets the Social security number the way the user set it in input.
    HOWEVER: The getter will change the string to upper invariant/trim and if the last character is "*", it will be replaced by calculated checksum. */
        socialSecurityNumber?: string | null;
        /** Social security number if valid or null */
        readonly socialSecurityNumberValid?: string | null;
        /** IBAN number for the Bank account */
        ibanNumber?: string | null;
        /** Contact e-mail address. */
        email?: string | null;
        /** Telephone number */
        telephone?: string | null;
        /** Tax percent for this payment
    This is whole percent as number. i.e. for 50% set 50, not 0.5. */
        taxPercent?: number | null;
        /** Tax card type */
        taxCardType?: CalcWorkerPaymentDataTaxCardType | null;
    }
    /** Tax card information stored as part of the Calculation object */
    export interface CalcTax {
        /** Identifier for a stored tax card if one is applied to this calculation.
    System updates the TaxCardId automatically in Recalculate / Save if it finds a better candidate
    and also updates the Snapshot. */
        taxcardId?: string | null;
        /** Snapshot of the tax card for salary slip etc. purposes.
    Updated in Recalculate / Save based on TaxCardId */
        snapshot?: TaxcardSnapshot | null;
        /** Possibility to set a fixed widthholding tax amount for this calculation regardless of
    the percentage etc. in the tax card. */
        fixedTaxAmount?: number | null;
        /** The API returns true if there is a valid taxcard for the PaidAt date for this calculation
    or if FixedTaxAmount is set for */
        isValid?: boolean | null;
        /** Calculated widthholding tax. Note that this is an estimation until the calculation is actually paid.
    Also, if the salary is not paid as cash (e.g. fringe benefits), there may not be enough money to make the full deduction. */
        estimatedTax?: number | null;
    }
    /** Calculates the total numbers - the ones that are common to Employer and worker. */
    export interface TotalCalculationDTO {
        /** The total pension payment for this calculation */
        pension?: number | null;
        /** The total unemployment payment sum. */
        unemployment?: number | null;
        /** The total of all calculation rows */
        total?: number | null;
        /** The base salary without overtime and other such additions that are part of gross salary. */
        totalBaseSalary?: number | null;
        /** Gross salary is the salary including additions like overtime, but not benefits or expenses */
        totalGrossSalary?: number | null;
        /** Taxable salary is the salary from the taxation point of view - includes some benefits. */
        totalTaxable?: number | null;
        /** DEPRICATED: This is now the same as TotalPensionInsuranceBase. For other social insurances, you may want to use
    TotalUnemploymentInsuranceBase, TotalHealthInsuranceBase or TotalAccidentInsuranceBase, */
        totalSocialSecurityBase?: number | null;
        /** Base income for calculating the Pension insurance contributions. */
        totalPensionInsuranceBase?: number | null;
        /** Base income for calculating the Unemployment insurance contributions. */
        totalUnemploymentInsuranceBase?: number | null;
        /** Base income for calculating the Health insurance contributions. */
        totalHealthInsuranceBase?: number | null;
        /** Base income for calculating the Accident insurance contributions. */
        totalAccidentInsuranceBase?: number | null;
        /** Tax free expenses. */
        totalExpenses?: number | null;
        /** Total sum of rows that are paid through Salaxy Customer Funds account.
    This is the Gross sum before side costs, deductions etc., but it is not Gross salary as it includes expenses etc. */
        totalPayable?: number | null;
    }
    /** Business level modeling corresponding to WageReportsToIRTypes Transaction. Also contains additional data for accounting and integration purposes. */
    export interface IrRow {
        /** Type of the row - basically describes the source for this row:
    Either Manual (default) or Usecase logic, which may be legacy / API v02 or new / API v03. */
        type?: IrRowSourceType | null;
        /** Description text of the row that is shown in reports. */
        message?: string | null;
        /** Count for the row. Default is one. */
        count?: number | null;
        /** Price for the row. */
        price?: number | null;
        /** Simple multiplication: Count * Price - no other logic. Not used as Input. */
        total?: number | null;
        /** Unit for the row. If specified as Undefined, set by the server-logic based on row type etc. */
        unit?: Unit | null;
        /** Details for the National Incomes registry */
        irData?: IrDetails | null;
        /** Details from Salaxy internal calulcation point-of-view (logic not in IR). */
        calcData?: IrRowCalculationProperties | null;
        /** Earnings period if differnet than the report period.
    Note that this property may be (and by default is) null. */
        earningPeriod?: DateRange | null;
        /** Accounting related data for the row. */
        accounting?: RowAccountingData | null;
        /** Usecase specific data */
        data?: {
            [key: string]: any;
        } | null;
    }
    /** Models the business logic of Employer side of the calculation. For documentation, see the IEmployerCalculation interface. */
    export interface EmployerCalculationDTO {
        /** All side cost: The mandatory side costs + Palkkaus and partner fees */
        allSideCosts?: number | null;
        /** The final cost after household decuction from the tax authorities:
    Add to the payment the deductions (paid by employer) and deduct House hold deduction (later potentially other such subsidies) */
        finalCost?: number | null;
        /** Household deduction that the tax man will reimburse in taxation.
    This is an estimation: There is minimum and maximum limits etc. */
        householdDeduction?: number | null;
        /** The mandatory side costs are Pension (TyEL), Social secuirty and Unemployment insurance. */
        mandatorySideCosts?: number | null;
        /** The Palkkaus.fi fee including potential partner fees. */
        palkkaus?: number | null;
        /** The part of the Pension that Employer pays */
        pension?: number | null;
        /** The social security payment- paid as part of taxation (ennakonpidätys). */
        socialSecurity?: number | null;
        /** Total payment that is Paid to Palkkaus.fi */
        totalPayment?: number | null;
        /** Total payment using pre Insiders register calculation logic.
    DEPRICATED: This is provided for special scenarios and may be removed without warning */
        totalPaymentLegacy?: number | null;
        /** Total salary cost is the paid salary, deductions and side costs, but not expenses. */
        totalSalaryCost?: number | null;
        /** The part of the unemployment insurance that the employer pays */
        unemployment?: number | null;
        /** Total deductions that are made from the payment that is paid to Palkkaus and then to Worker */
        totalDeductions?: number | null;
        /** Union payment if it is deducted from the final figure */
        deductionUnionPayment?: number | null;
        /** Pension payents, when paid directly by the Employer - not by Palkkaus.fi */
        deductionPensionSelfPayment?: number | null;
        /** Unemployment insurance (TVR) payents, when paid directly by the Employer - not by Palkkaus.fi */
        deductionUnemploymentSelfPayment?: number | null;
        /** Salary advance - this part has already been paid. */
        deductionSalaryAdvance?: number | null;
        /** Foreclosure that has already been paid by the employer. */
        deductionForeclosure?: number | null;
        /** Foreclosure that Palkkaus.fi pays in behalf of employer. */
        foreclosureByPalkkaus?: number | null;
        /** Other deductions that are deducted from the net salary. */
        deductionOtherDeductions?: number | null;
    }
    /** The Worker side of the calculation. SalarySlip is largely written based on this calculation. */
    export interface WorkerCalculationDTO {
        /** The benefits as valued by the tax authority. */
        benefits?: number | null;
        /** All the items that are deducted from the salary (this is also the order if there is not enough payable money):
    SalaryAdvance + Tax + Pension + UnemploymentInsurance + UnionPayment + OtherDeductions */
        deductions?: number | null;
        /** The payment that is made to the Worker based on salary after all deductions.
    Does not include expenses. */
        salaryPayment?: number | null;
        /** The salary advance that was paid to the Worker */
        salaryAdvance?: number | null;
        /** The foreclosure that been deducted from the salary by the employer. */
        foreclosure?: number | null;
        /** Ennakonpidätys: The taxes that were deducted from the worker salary. */
        tax?: number | null;
        /** Net salary after tax - before worker foreclosure or deductions of the side costs */
        salaryAfterTax?: number | null;
        /** Net salary after tax and foreclosure. */
        salaryAfterTaxAndForeclosure?: number | null;
        /** Part of the Pension that is taken from the Worker side of the salary. */
        pension?: number | null;
        /** Workers part of the unemployment insurance. */
        unemploymentInsurance?: number | null;
        /** The union payment that is deducted from the Worker salarya s per workers request. */
        unionPayment?: number | null;
        /** Widthholding tax: The original calulated version - there might not be enough money to finally pay this. */
        fullTax?: number | null;
        /** The salary advance that was paid to the Worker
    The original calulated version - there might not be enough money to finally pay this. */
        fullSalaryAdvance?: number | null;
        /** Part of the Pension that is taken from the Worker side of the salary.
    The original calulated version - there might not be anough money to finally pay this. */
        fullPension?: number | null;
        /** Workers part of the unemployment insurance.
    The original calulated version - there might not be anough money to finally pay this. */
        fullUnemploymentInsurance?: number | null;
        /** The union payment that is deducted from the Worker salarya s per workers request. */
        fullUnionPayment?: number | null;
        /** The sum that is at the end paid to the Worker:
    Salary payment + expenses */
        totalWorkerPayment?: number | null;
        /** Total payment using pre Insiders register calculation logic.
    DEPRICATED: This is provided for special scenarios and may be removed without warning */
        totalWorkerPaymentLegacy?: number | null;
        /** The side costs that are deducted from the Worker salary according to law:
    Pension, Unemployment and Union payment. Tax is not included in this number.
    This is not an official number, really used in the charts etc. */
        workerSideCosts?: number | null;
        /** Other deductions that are deducted from the net salary.
    The original calulated version - there might not be enough money to finally deduct this. */
        fullOtherDeductions?: number | null;
        /** Part of the other deductions that are deducted from the net salary. */
        otherDeductions?: number | null;
        /** Prepaid expenses that are deducted from the expenses to be paid to the worker.
    The original calulated version - there might not be enough money to finally deduct this. */
        fullPrepaidExpenses?: number | null;
        /** Part of the prepaid expenses that are deducted from the expenses. */
        prepaidExpenses?: number | null;
    }
    /** Defines a Calculation row in the calculation results */
    export interface ResultRow {
        /** Row index that matches this row to a user defined row (which is zero-based).
    If the row is generated by calculation business logic (e.g. based on framework agreement), the index is -1. */
        userRowIndex?: number | null;
        /** Logical type of the row */
        rowType?: CalculationRowType | null;
        /** Source of the calculation row - affects how the row is handled in editing / recalculation scenarios. */
        rowSource?: CalculationRowSource | null;
        /** Description text of the row that is shown in reports. If null, will be set according to type. */
        message?: string | null;
        /** Count for the row - default is one */
        count?: number | null;
        /** Price for the row */
        price?: number | null;
        /** Unit for the row */
        unit?: CalculationRowUnit | null;
        /** Total for the row - always Price * Count */
        total?: number | null;
        /** Base salary (Peruspalkka) is the monthly salary or total salary without addiotions (overtime, holidays etc.) */
        totalBaseSalary?: number | null;
        /** Gross salary (Bruttopalkka) is "real" salary including the additions (overtime, holidays etc.) but not benefits. */
        totalGrossSalary?: number | null;
        /** Taxable income (Ennakonpidätyksen alainen ansio) is the salary from taxation point-of-view. It is basically the Gross Salary plus benefits. */
        totalTaxable?: number | null;
        /** Social Security Base (Sosiaaliturvan alainen ansio) is typically the same as TotalTaxable,
    but e.g. child care subsidy (yksityisen hoidon tuki) is handled differently. */
        totalSocialSecurityBase?: number | null;
        /** Taxfree expenses compensation */
        totalExpenses?: number | null;
        /** Deductions from the salary: Union payment, Advance, Foreclosure */
        totalDeduction?: number | null;
        /** Identifier in the source system is a key defined by a source system / partner system.
    This is a pass-through string that is kept as-is in the Salaxy system. */
        sourceId?: string | null;
        /** Usecase specific data */
        data?: {
            [key: string]: any;
        } | null;
    }
    /** Parameters that affect the calculation. EmployerResponsibilities is historical name - currentlty to object holds also other properties that are not well described as responsibilities. These based on the EmployerGroups selection or properties of the Employer and Worker snapshots as well as this Calculation */
    export interface IEmployerResponsibilities {
        /** Employer groups that are set on Example calculations.
    These will be replaced by properties from Worker / Employer when these persons are set. */
        employerGroups?: EmployerGroups | null;
        /** If true, this employer is not required to get an accident insurance. */
        readonly noAccidentInsurance?: boolean | null;
        /** Pension is not paid if the salary to this Worker is less than a certain amount in a month (about 60€)
    or worker is less than 17 or more than 64 years old. */
        readonly noPensionPayment?: boolean | null;
        /** If worker is less than 16 or more than 67, social secuiry payment is not paid */
        readonly noSocialSecurity?: boolean | null;
        /** If Worker is less than 17 or more than 64, TVR is not paid.
    Also if the Employer has no accident insurance. */
        readonly noTvr?: boolean | null;
        /** If Employer pays to this Worker max 200 euro, he does not need to even make the reports to tax office.
    He may do so though */
        readonly noTaxResponsibility?: boolean | null;
        /** If Employer pays to this Worker max 1500 euro, he does not need to deduct the witholding tax.
    He may do so though */
        readonly noWithholdingTax?: boolean | null;
        /** The name of the Pension company because this affects the Pension percentage used in the calculation (only in contract situation).
    It also needs to be shown in the reports. */
        readonly pensionCompany?: PensionCompany | null;
        /** If the Worker is from 53 to 62 years old, more of the Pension payment
    is deducted from the Worker side of the salary (less for the Employer).
    NOTE: People from 63 to 67 years pay the same amount as 17-52 years old. */
        readonly pensionOldAgePayment?: boolean | null;
        /** The employer should have a Pension contract (not a temporary employer).
    It does not necessarily mean that the user has a contract yet.
    NOTE: At the moment, this is not used for calculation - only in the Side costs visualizer tool. */
        readonly pensionRegularEmployer?: boolean | null;
        /** If true, the employer has a Pension contract and makes the payments
    in a monthly schedule which means that Pension interest is calculated for the 20th of the next month.
    Currently, this is only enabled to Etera - other companies will follow. */
        readonly isPensionMonthlyContract?: boolean | null;
        /** If true, the employer pays the Unemployment insurance directly to TVR.
    As of writing, this is true for all companies and with calculations starting 1.8.2018,
    false for households or calculations before the date. */
        readonly unemploymentSelfPayment?: boolean | null;
        /** The assumed payment date, which may be te real Payment date if the calculation is paid
    or Today if calculation is a draft.
    In the future, it may also be settable in some way: It is currently settable, by setting the PaidAt,
    but that is not recommended ONLY IN TEST as this may not work in all production scenarios. */
        readonly calculationPaymentDate?: string | null;
        /** The age range that is used in the business logic that determines the employer responsibilities.
    This property replaces the EmployerGroups.AgeRange that was previous get/set.
    This is based on WorkerDateOfBirth and CalculationPaymentDate. */
        readonly pensionAgeRange?: AgeRange | null;
        /** Date of birth for the Worker - this affects the side costs.
    Please also set the DateOfBirthAccuracy, if you set this property.
    If SocialSecurityNumber is set, it will override any value set here. */
        readonly workerDateOfBirth?: string | null;
        /** Accuracy of the date of birth
    If SocialSecurityNumber is set, it will override any value set here. */
        readonly workerDateOfBirthAccuracy?: DateOfBirthAccuracy | null;
        /** Pension calculation type. */
        pensionCalculation?: PensionCalculation | null;
        /** Type of additional income earner data by National Incomer Register classification . If several types apply, all of them should be selected. */
        irIncomeEarnerTypes?: IncomeEarnerType[] | null;
    }
    /** Common base class / interface for data validation. */
    export interface ApiValidation {
        /** If true, the data is valid - no errors. */
        readonly isValid?: boolean | null;
        /** If true, has required fields missing data. */
        readonly hasAllRequiredFields?: boolean | null;
        /** Validation errors on invalid field values.
    Note that required fields missing error messages are not here. Only the invalid values. */
        readonly errors?: ApiValidationError[] | null;
    }
    /** Extends the Holiday specification with the properties of employment relation that are required for a specific year. */
    export interface HolidaySpecificationForYear {
        /** Wage basis for a Holiday Year.
    When this is changed, you need to make a rerun to the previous calculations in the holiday year.
    NOTE: This is not in HolidaySpecification because, the same value is stored in Employment relation. */
        wageBasis?: WageBasis | null;
        /** Employment period for the purposes of holiday calculation (annual leave accrual).
    When this is changed, you need to make a rerun to the previous calculations in the holiday year. */
        employmentPeriod?: DateRange | null;
        /** Defines how the holidays are handled for the worker. */
        code?: HolidayCode | null;
        /** Defines the number of holidays that the worker is entitled per month worked.
    If the value is set to 0 (default), the number is 2 for first year and 2,5 thereafter as defined in the law.
    To actually se the daysPerMonth to zero, use code value other than Permanent14Days or Permanent35Hours. */
        accrualFixed?: number | null;
        /** Holiday compensation as percent of salary (11.5% is 0.115) */
        compensation?: number | null;
        /** Method for paying holiday bonus in this salary calculation. */
        bonusMethod?: HolidayBonusPaymentMethod | null;
        /** Holiday bonus for the worker (lomaraha, lomaltapaluuraha).
    Typically 0.5 for 50%. */
        bonus?: number | null;
    }
    /** Period recalculation parameters that are related to Monthly salary. Could theoretically be used for other long periods, namely Weekly salary, but that is not actively supported or tested. */
    export interface CalcMonthlyData {
        /** If true the Monthly (weekly) calculation is being applied to absences and holidays. */
        isMonthly?: boolean | null;
        /** Monthly salary in holiday and absences calculation.
    Typically, the same as the actual monthly calculation, but may be modified. */
        monthlySalary?: number | null;
        /** Price for a single absence day. Typically based on MonthlySalary, but may be modified. */
        absencesDaySalary?: number | null;
    }
    /** Holiday calculation based on hourly salary OR performance based salaries. May also contain other data related to working hours. */
    export interface CalcHourlyData {
        /** If true, the hourly / performance based calculation is applied. */
        isHourly?: boolean | null;
    }
    /** Holiday Bonus data that is stored into a Salary calculation. */
    export interface CalcHolidayBonus {
        /** Number of yearly leave days. This is for the purposes of calculating the Bonus,
    so this is yearly salary days, not the complete saldo (including StartSaldo). */
        yearlyLeaveDays?: number | null;
        /** Total yearly holiday salary for the purposes of holiday bonus. */
        totalHolidaySalary?: number | null;
        /** The sum of holiday bonus that has already been paid this year.
    The payment is never so big that this would become more than BonusTotalHolidaySalary
    (by this calculation - the limit can be exceeded manually). */
        alreadyPaid?: number | null;
    }
    /** Holiday Accrual for a calendar month in releation to a calculation. */
    export interface MonthlyHolidayAccrual {
        /** First day of month for the holiday month */
        month?: string | null;
        /** Holiday accrual for this month.
    Note that -1 is indication that accrual should not be updated for this particylar month.
    There is no possiblity for negative accrual. */
        daysAccrued?: number | null;
        /** Number of absence days used in the calculation. */
        absencesDays?: number | null;
        /** Work days in determining the accrual for the month in 14 days rule. */
        workDays?: number | null;
        /** Work hours in determining the accrual fot the month in 35 hour rule. */
        workHours?: number | null;
    }
    /** Reporting details to Incomes registry (Tulorekisteri). */
    export interface IrDetails {
        /** The number code in Incomes Register. */
        code?: number | null;
        /** Boolean flags that can be set on transaction. */
        flags?: IrFlags[] | null;
        /** Defines the exceptions to Insurance / Sidecost defaults that are set by the row type. */
        insuranceExceptions?: IrInsuranceExceptions[] | null;
        /** Daily allowance code. */
        dailyAllowance?: AllowanceCode[] | null;
        /** Set this to true if meak benefit is of tax value - false if it si not.
    The value should be null if the row is not mealBenefit. */
        mealBenefitIsTaxValue?: boolean | null;
        /** Provides the additional data needed for car benefit. */
        carBenefit?: CarBenefit | null;
    }
    /** Details from Salaxy internal calulcation point-of-view (logic not in IR). */
    export interface IrRowCalculationProperties {
        /** Grouping for reports etc. */
        grouping?: CalcGroup | null;
        /** Defines the behavior of the row in Salaxy calculation process. */
        behavior?: CalculationFlag[] | null;
    }
    /** Provides row specific data for accounting. */
    export interface RowAccountingData {
        /** Percent for VAT calculation expressed as decimal: 24% is 0.24.
    The price in the root level is assumed being with tax. */
        vatPercent?: number | null;
        /** Price without VAT. */
        priceNoVat?: number | null;
        /** Suggested account number in accounting */
        suggestedAccountNumber?: string | null;
    }
    /** Employer groups that are set on Example calculations. These will be replaced by properties from Worker / Employer */
    export interface EmployerGroups {
        /** Ranges for salary / year will affect the side costs. */
        salaryPerYear?: SalaryPerYearRange | null;
        /** Very small salaries do not require Pension payments. */
        salaryPerMonth?: SalaryPerMonthRange | null;
        /** Very small employers (households) do not required to report to the tax authorities. */
        employerType?: EmployerType | null;
        /** If true, the Employer is a private person.
    Otherwise Company or Unknown. */
        isPrivatePerson?: boolean | null;
        /** Campaign pricing may affect Palkkaus fees.
    This field is set by the system when coming to the payment based on Employer properties (usually campaign code). */
        campaignPricing?: PalkkausCampaignPricing | null;
    }
    /** Validation errors used in API output. */
    export interface ApiValidationError {
        /** Full path to Name of the property / field (Member name in JSON).
    This may be null/empty for type General. */
        key?: string | null;
        /** High level type: Specifies the relation to object and its Member. */
        type: ApiValidationErrorType;
        /** Validation error message in the requested language. */
        msg: string;
        /** Some error providers may have a unique technical code for error or validator */
        code?: string | null;
    }
    export interface CarBenefit {
        carBenefitCode?: CarBenefitCode | null;
        ageGroupCode?: AgeGroupCode | null;
        kilometers?: number | null;
    }
    /** Data that is needed for recalculating holidays and absences to a calculation. */
    export interface RecalculatePeriodRequest {
        /** The salary calculation that should be recalculated. */
        calc?: Calculation | null;
        /** Parameters for adding the rows. */
        parameters?: CalcWorktime | null;
    }
    /** Data that is needed for determening PeriodRecalculationParams and thus at the end recalculating holidays and absences to a calculation. */
    export interface PeriodRecalculationData {
        /** The salary calculation that should be recalculated. */
        calc?: Calculation | null;
        /** Holiday year with holidays that should be added as salary rows.
    If not provided, holidays are not calculated. */
        holidayYear?: HolidayYear | null;
        /** Absences that should be deducted from monthly salary.
    If not provided, absences are not calculated. */
        absences?: WorkerAbsences | null;
    }
    /** Holiday year is in Finland from April to end of march. This is the time from which the holidays for the following summer and year are determined. */
    export interface HolidayYear {
        /** Year as integer. This value is required when inserting and cannot be edited later.
    Period will be set accordingly, or adjusted in case it is set below. */
        year: number;
        /** Salaxy ID of the worker. This value is required when inserting and cannot be edited later.
    WorkerSnapshot is automatically updated upon save. */
        workerId: string;
        /** The holiday period for the year: Should always be 2.5.[Year]-30.4.[Year+1] */
        period?: DateRange | null;
        /** Specifies how the holidays are handled.
    If the Spec.Code is Undefined in Save, the contents is fetched from the current employment relation. */
        spec?: HolidaySpecificationForYear | null;
        /** Accrual of holidays (lomakirjanpito). */
        accrual?: HolidayAccrual | null;
        /** Payments related to Annual Leaves (holidays). */
        leaves?: AnnualLeaves | null;
        /** Basic information for the Worker.
    Automatically updated upon save based on WorkerId.
    Mainly for the listings etc. */
        workerSnapshot?: Avatar | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Yearly log for Holiday accrual. */
    export interface HolidayAccrual {
        /** Holiday accrual entries during the period. */
        months?: HolidayAccrualEntry[] | null;
        /** The period for holiday accrual.
    This is also the period of the holiday year that the Worker is Employed,
    i.e. it may be shorter than HolidayYear Period if the employment relation
    begins or ends within the holiday year. */
        period?: DateRange | null;
        /** Default accrual for months where the Accrual occurs.
    When Holiday year is recalculated, this is determined either from Employment Period or AccrualFixed if set. */
        defaultAccrual?: number | null;
        /** Start saldo from the previous year. */
        startSaldo?: number | null;
        /** Total for the current year. This does not include Start saldo. */
        readonly total?: number | null;
        /** End saldo from the previous year. This is Total + Start Saldo. */
        readonly endSaldo?: number | null;
    }
    /** Data about the annual leaves planned and used during the holiday year. */
    export interface AnnualLeaves {
        /** Planned annual leaves. */
        planned?: AnnualLeave[] | null;
        /** Paid or otherwise handled annual leaves. */
        paid?: AnnualLeavePayment[] | null;
    }
    /** Entry in the Yearly Holiday Accrual list. */
    export interface HolidayAccrualEntry {
        /** Index of the month from 0-11 for easier iteration.
    This is also the real identifier of the month when editing the entries through API.
    Months 4-12 are index 0-8 and month 1-3 are index 9-11 */
        index?: number | null;
        /** First day of month for the holiday month */
        month?: string | null;
        /** Days added to holiday Accrual: Typically days per month. */
        daysAccrued?: number | null;
        /** Source of the holiday accrual. */
        source?: HolidayAccrualSource | null;
        /** Identier of the calculation if Kind */
        calcId?: string | null;
        /** Additional information as recorded by the Employer. */
        notes?: string | null;
    }
    /** A planned annual Leave - not sure whether it will kept or is it just planned */
    export interface AnnualLeave {
        /** Identifies a single leave. */
        id?: string | null;
        /** The planned holiday period. */
        period?: DateRange | null;
        /** Additional information as recorded by the Employer. */
        notes?: string | null;
    }
    /** Payment related to annual leave: holiday salary, bonus and compensation */
    export interface AnnualLeavePayment {
        /** Type of the payment entry */
        kind?: AnnualLeavePaymentKind | null;
        /** The reference date: Typically the Paid at date.
    The items are typically sorted based on this. */
        date?: string | null;
        /** Days of holidays that are paid or compensated */
        holidayDays?: number | null;
        /** Salary that is paid for holidays */
        holidaySalary?: number | null;
        /** Compensation that is paid for holidays not kept. */
        holidayCompensation?: number | null;
        /** Bonus for either Holiday Salary or Compensation. */
        holidayBonus?: number | null;
        /** If the payment is defined by a calculation, stores the calculation ID. */
        calcId?: string | null;
        /** Identifies a single leave. */
        id?: string | null;
        /** The planned holiday period. */
        period?: DateRange | null;
        /** Additional information as recorded by the Employer. */
        notes?: string | null;
    }
    /** Parameters that are changing yearly. This class is made for the change of the year and it is typically updated in December. It is not for recording longer history of numbers. */
    export interface YearlyChangingNumbers {
        /** Year for numbers. */
        readonly year?: number | null;
        /** Returns yearly sidecosts. */
        readonly sideCosts?: YearlySideCosts | null;
        /** Returns holidays for the year. */
        holidays?: Holidays | null;
    }
    /** Encapsulates yearly side costs into one class. */
    export interface YearlySideCosts {
        /** Unemployment insurance payment percent - Employer */
        readonly unemploymentEmployerPercent?: number | null;
        /** Unemployment insurance payment percent - deducted from Worker */
        readonly unemploymentWorkerPercent?: number | null;
        /** Unemployment insurance payment percent for partial owner worker - deducted from Worker */
        readonly unemploymentPartialOwnerWorkerPercent?: number | null;
        /** Pension payment income limit per month */
        readonly tyelIncomeLimit?: number | null;
        /** Pension payment base percentage */
        readonly tyelBasePercent?: number | null;
        /** Pension payment base percentage - deducted from Worker salary */
        readonly tyelWorkerPercent?: number | null;
        /** Pension payment base percentage - deducted from 53-62 year old Worker */
        readonly tyel53Percent?: number | null;
        /** Pension contract employer yearly limit */
        readonly tyelContractEmployerLimit?: number | null;
        /** Social Security payment for employer (SAVA-maksu) */
        readonly socSecPercent?: number | null;
        /** Illness Insurance care percent that is withhold from the Worker. (vakuutetun sairausvakuutusmaksu: päivärahamaksu + sairaanhoitomaksu */
        readonly illnessInsurancePercent?: number | null;
        /** Tax free daily allowance for full day approved by tax authorities. */
        readonly taxFreeDailyAllowance?: number | null;
        /** Tax free daily allowance for half day approved by tax authorities. */
        readonly taxFreeDailyHalfAllowance?: number | null;
        /** Tax free meal allowance approved by tax authorities. */
        readonly taxFreeMealAllowance?: number | null;
        /** Tax free milage allowance approved by tax authorities. */
        readonly taxFreeKmAllowance?: number | null;
        /** Union payment: Rakennusliitto, A (typical) */
        readonly unionPaymentRaksaA?: number | null;
        /** Union payment: Rakennusliitto, AO and TA */
        readonly unionPaymentRaksaAoTa?: number | null;
        /** Steps tax card limit 1 */
        readonly stepsTaxCardLimit1?: number | null;
        /** Steps tax card limit 2 */
        readonly stepsTaxCardLimit2?: number | null;
    }
    export interface HolidayDate {
        date?: string | null;
        holiday?: Holiday | null;
        readonly holidayGroups?: HolidayGroup[] | null;
        localName?: string | null;
        name?: string | null;
    }
    /** Holidays for the entire year. */
    export interface YearlyHolidays {
        /** Year for the holidays. */
        readonly year?: number | null;
        /** Holidays for the year. */
        readonly holidays?: Holidays2 | null;
    }
    /** Finvoice implementation for Payment object. */
    export interface Invoice {
        /** Finvoice header details. */
        header?: InvoiceHeader | null;
        /** Finvoice seller details. */
        seller?: InvoiceSeller | null;
        /** Finvoice buyer details. */
        buyer?: InvoiceParty | null;
        /** Finvoice rows. */
        rows?: InvoiceRow[] | null;
        /** Finvoice raw data in XML or json format. */
        content?: string | null;
        /** Finvoice as Pdf. */
        pdf?: string | null;
    }
    /** Header for the invoice */
    export interface InvoiceHeader {
        /** Invoice number. */
        invoiceNumber?: string | null;
        /** Unique identifier of the Invoice is an RF reference (e.g. RF0710010831) */
        referenceNumber?: string | null;
        /** Invoice Date */
        date?: string | null;
        /** Due date for the invoice. */
        dueDate?: string | null;
        /** Total amount for the invoice excluding VAT. */
        totalExVat?: number | null;
        /** Total amount for the invoice including VAT */
        total?: number | null;
        /** The free text that is displayed / sent with the invoice.
    May have multiple lines. */
        message?: string | null;
    }
    /** Seller in an Invoice: Contains the billing and marketing infromation in addition to InvoiceParty. */
    export interface InvoiceSeller {
        /** Web site URL */
        webUrl?: string | null;
        /** Bank account IBAN number */
        iban?: string | null;
        /** Bank account BIC */
        bic?: string | null;
        /** Finnish Company ID (Y-tunnus). */
        companyId?: string | null;
        /** Organisation name */
        companyName?: string | null;
        /** Contact information */
        contact?: Contact | null;
        /** Contact person Full name */
        contactPersonName?: string | null;
    }
    /** Party in an Invoice: Either Buyer or Seller. */
    export interface InvoiceParty {
        /** Finnish Company ID (Y-tunnus). */
        companyId?: string | null;
        /** Organisation name */
        companyName?: string | null;
        /** Contact information */
        contact?: Contact | null;
        /** Contact person Full name */
        contactPersonName?: string | null;
    }
    /** Row for the invoice */
    export interface InvoiceRow {
        /** Article Identifier is given by seller, EAN etc.
    In the case of Salaxy salaries, this is CalculationRowType. */
        articleId?: string | null;
        /** Name of the product or service.
    In the case of Salaxy salaries, this is language versioned text of CalculationRowType. */
        articleName?: string | null;
        /** Message (RowFreeText in Finvoice) is the message is mainly designed for accounting systems. */
        message?: string | null;
        /** Count (DeliveredQuantity in Finvoice) is the quantity of product or service. */
        count?: number | null;
        /** Price including VAT (UnitPriceVatIncludedAmount, Yksikköhinta verollinen) */
        price?: number | null;
        /** Price of one product or service without Vat (UnitPriceAmount, Yksikköhinta veroton).
    Yksikköhinta voidaan ilmoittaa myös verollisena UnitPriceVatIncludedAmount- elementissä laskutuskäytännön ja verottajan ALV-ohjeistuksen mukaan. */
        priceExVat?: number | null;
        /** Vat percent related to the product or service (RowVatRatePercent, Alv%) */
        vatPercent?: number | null;
        /** Row total excluding VAT (RowVatExcludedAmount, Yhteensä veroton) */
        totalExVat?: number | null;
        /** Row total with VAT (RowAmount, Yhteensä verollinen) */
        total?: number | null;
        /** Suggested account number (RowNormalProposedAccountIdentifier). */
        accountingAccountNumber?: string | null;
        /** Textual code for accounting mapping (RowProposedAccountText, Raportointikoodi).
    In Finvoice: "Accounting information agreed between seller and buyer." */
        accountingCode?: string | null;
    }
    /** Read-only Article object for the API */
    export interface Article {
        /** Title: Shown in te beginning of the text, in the browser window header, in the lists etc. */
        title?: string | null;
        /** The section where the article should appear */
        section?: AbcSection | null;
        /** URL to a picture that should be used in social media sharing */
        picture?: string | null;
        /** Publish date */
        date?: string | null;
        /** Author of the article. */
        author?: Author | null;
        /** File path as in CDN */
        file?: string | null;
        /** Shorter summary (max 140 chars), shown as default
    in Tweets. */
        shortSummary?: string | null;
        /** A slightly longer summary, shown in Facebook, Google+
    and LinkedIn likes/shares. */
        longSummary?: string | null;
        /** Article text and/or markup.
    This is not serialized to XML, but read/written separately to blob storage - typically using MarkDown. */
        html?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Author of an article in the CMS */
    export interface Author {
        /** Person name */
        name?: string | null;
        /** Public image for the author */
        image?: string | null;
        /** Short description text */
        description?: string | null;
        /** Company or organization logo - expect that this may be null. */
        companyLogo?: string | null;
        /** Short description text / label for the organization. */
        companyDescription?: string | null;
        /** Link to a web site - typically of the organization that the author represents */
        companyUrl?: string | null;
        /** Unique key for the object */
        guid?: string | null;
        /** Exact time of last technical save: Not editable by user and will change in imports/exports etc. */
        timestamp?: string | null;
        /** Supports the Cosmos DB migration by serializing the type of the current object as a short type string for querying. */
        type?: string | null;
    }
    /** API EmailMessage */
    export interface EmailMessage {
        /** Message subject */
        subject?: string | null;
        /** Message body */
        body?: string | null;
        /** Message Sender (ID) */
        sentByUser?: string | null;
        /** Message From (email address) */
        from?: string | null;
        /** Message To (email address) */
        to?: string | null;
        /** Message To (ID) */
        receivedByUser?: string | null;
        /** Sender name */
        senderName?: string | null;
        /** Receiver name */
        receiverName?: string | null;
        /** Receiver's message */
        isReceivedMessage?: boolean | null;
        /** Message sent */
        sentAt?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    export interface EmploymentContract {
        employer?: ContractParty | null;
        worker?: ContractParty | null;
        isWorkAtHome?: boolean | null;
        workPlace?: string | null;
        mainWorkTasks?: string | null;
        partTimeReason?: string | null;
        otherPayDescription?: string | null;
        testPeriod?: string | null;
        annualLeave?: string | null;
        benefitsException?: string | null;
        noticeExeption?: string | null;
        otherTerms?: string | null;
        salary?: ContractSalary | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Part of the Employment contract that is relevant to paying the salary */
    export interface ContractSalary {
        /** Starting date for the employment.
    Should always be set for a valid contract. */
        startDate?: string | null;
        /** End date for the employment.
    Only set if the contract is Fixed term. */
        endDate?: string | null;
        worksHoursType?: EmploymentWorkHoursType | null;
        payDay?: string | null;
        salaryType?: EmploymentSalaryType | null;
        salaryAmount?: string | null;
        collectiveContract?: string | null;
        workType?: EmploymentType | null;
        workHours?: string | null;
        salaryCompension?: string | null;
    }
    /** E-salary / E-palkka is a way for a partner to direct an Employer to Salary payment without having access to Employer account. In first instance, E-salary is targeting Household Employers only. */
    export interface ESalaryPayment {
        /** Status of the Payment.
    NOTE: You can only set states Created, Shared and Opened.
    Other states are set by the Salaxy service and cannot be modified throgh API. */
        status?: ESalaryPaymentStatus | null;
        /** Known data for the employer household that is paying the salary.
    It is understood that the caller may not have all the information:
    Palkkaus.fi will ask the rest if necessary. */
        household?: PersonInput | null;
        /** Payment request and how to return to the calling service. */
        request?: PaymentRequestParameters | null;
        /** Provides the validation of the ESalaryPayment object after the object has been saved. */
        validation?: ApiValidation | null;
        /** Calculation that should be paid by the Employer. */
        calc?: Calculation | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    export interface PersonInput {
        /** If true, creates a personal account based on this data.
    Otherwise this is just user data for Company. */
        doCreate?: boolean | null;
        /** Contact information for the user / personal account */
        contact?: Contact | null;
        /** Verification objects for contact verification. */
        contactVerification?: OnboardingContactVerifications | null;
        /** Finnish Personal ID (Henkilötunnus /HETU) */
        personalId?: string | null;
        /** If an object with the given PersonalId exists, it is resolved here: The unique ID is set here by the onboarding service. This field cannot be set. */
        resolvedId?: string | null;
        /** First name or company name */
        firstName?: string | null;
        /** Last name, for companies, this should be null */
        lastName?: string | null;
        /** URL of the picture if specified as picture (null in a case of type Icon) */
        picture?: string | null;
        /** Bank account number for salary payment */
        bankAccountIban?: string | null;
    }
    /** Contains request specific parameters for payment. */
    export interface PaymentRequestParameters {
        /** Return url for successful payment. */
        successUrl?: string | null;
        /** If this URL is set, the payment allows aynchronous payment methods
    such as payment with invoice (viitelasku).
    This means that the payment UI returns in status InProgress and
    payment is finalized later (may take hours, days or even be canceled). */
        paymentStartedUrl?: string | null;
        /** Return url for failed / cancelled payment. */
        cancelUrl?: string | null;
        /** Indicates whether to send emails according to workflow after payment.
    TODO: This should be retrieved from the partner profile etc. */
        sendEmails?: boolean | null;
        /** If true, the target object for the payment (calculation(s), payroll)
    requires further validation before the net salaries can be paid. */
        isFurtherValidationRequired?: boolean | null;
    }
    /** Contact verification object for onboarding. */
    export interface OnboardingContactVerifications {
        /** Verification for email contact. */
        email?: OnboardingContactVerification | null;
        /** Verification for telephone contact. */
        telephone?: OnboardingContactVerification | null;
    }
    /** Contact verification object for single message type. */
    export interface OnboardingContactVerification {
        /** Given pin code. */
        pin?: string | null;
    }
    /** The BlobFile stores metadata about miscellaneous BLOB-files that are stored to the system. For example Receipts and Authorizations. */
    export interface BlobFile {
        id?: string | null;
        /** The logical container - first level folder for the file. */
        container?: BlobRepository | null;
        /** Strongly typed logical type of the file. For user files, this is a sub folder. */
        type?: BlobFileType | null;
        /** File type (mime type etc.) based on the ending of the file name.
    Null for folders. */
        readonly fileType?: FileType | null;
        /** A user friendly name that can be used in listings.
    In the upload scenarios this is usually the original file name when uploaded.
    It is not necessarily unique. No path information */
        userFriendlyName?: string | null;
        /** Technical file name in storage.  In practical terms, this is the relative path
    excluding the container part "/[container]/".
    I.e. this property is without the starting slash "/".
    It must be unique and is used for Uri.
    For user files this is constructed "[Username]/[Type]/[UniqueFileName].[xxx]" */
        filePath?: string | null;
        /** Full unique URI in the storage */
        blobUri?: string | null;
        updatedAt?: string | null;
        /** The length of the file in bytes. */
        length?: number | null;
        /** If true, this is a collection (folder in file system) */
        isCollection?: boolean | null;
        metadata?: BlobFileMetadata | null;
    }
    export interface FileType {
        extension?: string | null;
        mimeType?: string | null;
        isImage?: boolean | null;
        readonly isText?: boolean | null;
    }
    /** Metadata related to blob files as a strongly typed object. This could optionally be extended as a dictionary or dynamic to allow arbitrary metadata. */
    export interface BlobFileMetadata {
        /** Whether the document is digitally signed (or manually checked as being signed).
    At the time of writing, this is used only for AuthorizationPdf documents. */
        isSigned?: boolean | null;
        /** Logical date for the file. E.g. first of the month for a monthly report. */
        logicalDate?: string | null;
        /** Subtype is internal typing within a BlobFileType. This may or may not be a sub-folder.
    There is no enumeration on File level - there may be one under services specific to one BlobFileType. */
        subType?: string | null;
        /** An amount related to document. By Default, this is the gross salary. */
        amount?: number | null;
        /** Avatar object related to the report.
    For example for the salary payments this is the other party (employer or worker) */
        avatar?: Avatar | null;
        /** Identifier of the original calculation etc. business object. */
        originalId?: string | null;
    }
    /** Measurement value. */
    export interface Measurement {
        /** Tag for the measurement. */
        tag?: string | null;
        /** Time when the measurement was recorded. */
        timestamp?: string | null;
        /** Actual measurement value. */
        value?: number | null;
    }
    /** Profile object for either Worker or Employer */
    export interface Profile {
        /** The source of this profile: Palkkaus section or partner site.
    This is a strongly typed object that may be undefined - see SourceId in that case. */
        source?: PartnerSite | null;
        worker?: WorkerProfileProperties | null;
        /** The identifier string of the source. This will always return a distinct value. */
        sourceId?: string | null;
        /** The legal entity type of the profile: Person or Company */
        entityType?: string | null;
        /** Account if known */
        accountId?: string | null;
        /** External id (e.g. member number) */
        externalId?: string | null;
        /** The avatar object: Picture name and other such basic info. */
        avatar?: Avatar | null;
        /** If true, the profile is set to public. */
        isPublic?: boolean | null;
        /** Expiration date for the profile will be unpublished. */
        expiresAt?: string | null;
        /** Last time user updated the profile */
        userUpdatedAt?: string | null;
        /** Other contact information for this profile. */
        contact?: Contact | null;
        job?: JobProfile | null;
        sortableName?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    export interface WorkerProfileProperties {
        dateOfBirth?: string | null;
        dateOfBirthAccuracy?: DateOfBirthAccuracy | null;
        gender?: Gender | null;
        hasTaxCard?: boolean | null;
        hasDriversLisence?: boolean | null;
    }
    export interface JobProfile {
        /** Background image for the profile page */
        backgroundImage?: string | null;
        /** Long description: This field accepts MarkDown, potentially also HTML in the future (at the moment HTML may create an error). */
        description?: string | null;
        /** External links to web sites, social media pages etc. */
        links?: string[] | null;
        /** Period for which the Salary period is set. */
        salaryPeriod?: SalaryPeriod | null;
        /** Minimum salary for the given salary period */
        salaryMin?: number | null;
        /** Max salary for the given salary period */
        salaryMax?: number | null;
        /** Title for the Job */
        jobTitle?: string | null;
        /** Detailed description of the Job */
        jobDetail?: string | null;
        /** Free text for location of work or worker */
        location?: string | null;
        /** A collection of known city IDs.
    Query the interface for city IDs */
        cities?: string[] | null;
        /** A collection of job ID's that this work or worker belongs */
        jobs?: string[] | null;
    }
    /** Provides the data for onboarding process for a company. Onboarding is the process where Salaxy gathers sufficient information to at least create an acccount, often even more so that the user can pay a salaary with the account. */
    export interface Onboarding {
        /** Type of the account to be created. */
        accountType: WebSiteUserRole;
        /** The server-side status of the onboarding process.
    NOTE that there is a more relaxed user interface-level state in ui.state and ui.stateFullText
    that can be used in onboarding user interfaces. */
        status?: OnboardingStatus | null;
        /** Input that is needed for creating a company account. */
        company?: CompanyInput | null;
        /** Input that is needed for creating a personal account.
    This may be
    1) the principal account if the account is created for 1a) Worker or 1b) Household or
    2) creator owner if the account is created for a Company. */
        person?: PersonInput | null;
        /** Possibility to define products.
    These can only be defined to one account as part of the onboarding process:
    If company account is created, the products are always for Company, not for the Person =&gt;
    Person will have default products in that case and they need to be edited in a separate process once the accounts have been created. */
        products?: AccountProducts | null;
        /** Input and post data for digital signature. */
        signature?: SignatureData | null;
        /** Validation status and errors. */
        validation?: AccountValidation | null;
        /** User interface related data: Mainly the state of the UI. */
        ui?: OnboardingUIState | null;
        credentials?: OnboardingCredentials | null;
        /** Custom state object that is guaranteed to survive the round-trip in the onboarding process.
    Calling application to the onboarding may pass here any data that it needs after the onboarding process is ready and user has been authenticated. */
        appState?: {
            [key: string]: string;
        } | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Represents the input for creating a company account. */
    export interface CompanyInput {
        /** If true, creates a company account based on company data. */
        doCreate?: boolean | null;
        /** Official name of the company */
        name: string;
        /** URL to a Logo picture (preferably PNG). This logo should be a square - icon type of logo for avatar. */
        logo?: string | null;
        /** The Business ID (Y-tunnus / Business Identity Code) is a code given to businesses and organizations by the PRH or the Tax Administration.
    It consists of seven digits, a dash and a control mark, for example 1234567-8.
    Alternative accepted formats incule international VAT number "FI12345678" or number without the dash: "12345678". */
        businessId: string;
        /** If an object with the given BusinessID exists, it is resolved here: The unique ID is set here by the onboarding service. This field cannot be set. */
        resolvedId?: string | null;
        /** Identifier at the partner site - typicelly an organization ID. */
        partnerAccountId?: string | null;
        /** Company ownership for KYC */
        ownership?: Ownership | null;
        /** If true, will fetch the data from the Finnish Business Infromation System (YTJ/BIS).
    Fetching is done firstly based on Business ID, if that is not valid then based on name. */
        doYtjUpdate?: boolean | null;
        /** The details of company data as provided by the Finnish Business Infromation System (YTJ/BIS) */
        ytjDetails?: YtjCompany | null;
        /** A search to Finnish Business Infromation System (YTJ/BIS). This search is triggered if you set the RawInput field. */
        ytjSearch?: YtjSearchResult | null;
        /** The legal type of the company, as categorized by Palkkaus / used in generating IBAN numbers. */
        companyType: CompanyType;
        /** Contact information for the company */
        contact?: Contact | null;
        /** List of roles which this account is member of. */
        roles?: Role[] | null;
    }
    /** Provides an overview and the possibility to edit products that are enabled to this account */
    export interface AccountProducts {
        /** The basic services for Salary payment. */
        baseService?: BaseSalaryProduct | null;
        /** Reports and payments to tax authorities. */
        tax?: TaxProduct | null;
        /** Reports and payments to TVR (the unemployment insurance fund of Finland) */
        unemployment?: UnemploymentProduct | null;
        /** Reports ant payments to Pension companies. */
        pension?: PensionProduct | null;
        /** Reports and payments for mandatory employment insurances. */
        insurance?: InsuranceProduct | null;
        /** Bookkeeping reports for salarypayments */
        accounting?: AccountingProduct | null;
        /** Employer health care from the company Heltti */
        healthCareHeltti?: HealthCareHelttiProduct | null;
        /** Pension payments for an entrepreneur */
        pensionEntrepreneur?: PensionEntrepreneurProduct | null;
        /** Defines the pricing partner, service model and pricing model. */
        pricing?: PricingProduct | null;
    }
    export interface SignatureData {
        /** Name of the signing person.
    By default, this is Onboarding Person, but it may be a completely different person. */
        personName?: string | null;
        /** Personal ID of the person, who has the right to sign. */
        personalId?: string | null;
        email?: string | null;
        telephone?: string | null;
        isPep?: boolean | null;
        digitalSignature?: VismaSignPostData | null;
        /** The account is authorized to sign on behalf of the actual account. */
        isProcura?: boolean | null;
    }
    /** Validation errors for the onboarding process */
    export interface AccountValidation {
        /** If true the official ID is technically valid.
    Official ID is either Finnish Personal ID (HETU, Henkilötunnus) or Business ID (Y-tunnus). */
        isOfficialIdValid?: boolean | null;
        /** If false, none of the accounts in ExistingAccounts cannot be accessed by the current user. */
        isAuthorized?: boolean | null;
        /** TODO: NOt implemented yet - should be implemented
    If true, the onboarding object has sufficient data to start the Digital Signature process (Visma Sign).
    Depending on the use case this may or may not be required for committing the data as account data. */
        canStartSign?: boolean | null;
        /** If true, the validation process has been completed. The IsValid may be true or false. */
        isCompleted?: boolean | null;
        /** Existing account based on official id (Personal ID / Business ID). */
        existingAccount?: Avatar | null;
        /** If true, the data is valid - no errors. */
        readonly isValid?: boolean | null;
        /** If true, has required fields missing data. */
        readonly hasAllRequiredFields?: boolean | null;
        /** Validation errors on invalid field values.
    Note that required fields missing error messages are not here. Only the invalid values. */
        readonly errors?: ApiValidationError[] | null;
    }
    /** Stores data for the onboarding user interface. */
    export interface OnboardingUIState {
        /** State of the user interface. Typically an enumeration that stores where the UI was. E.g. Wizard step. */
        state?: string | null;
        /** Full description of the user interface state for admin and analyses purposes. */
        stateFullText?: string | null;
        /** URL where the user should be redirected back to when the onboarding process is complete. */
        successUrl?: string | null;
        /** URL where the user should be redirected back to if the process is canceled or there is an urecoverable error. */
        cancelUrl?: string | null;
        /** Response type to OAuth -request */
        oAuthResponseType?: string | null;
        /** Salaxy worker invitation key. */
        invitationKey?: string | null;
    }
    /** Stores information about the credentials that were used in the Onboarding process. */
    export interface OnboardingCredentials {
        /** Identifier of the credentials (UserAccount Guid in the database). */
        credentialId?: string | null;
        /** The partner that initiated the Onboarding process. */
        partnerId?: string | null;
    }
    /** The details of company data as provided by the Finnish Business Infromation System (YTJ/BIS) */
    export interface YtjCompany {
        /** The Business ID (Y-tunnus / Business Identity Code) is a code given to businesses and organizations by the PRH or the Tax Administration.
    It consists of seven digits, a dash and a control mark, for example 1234567-8.
    Alternative accepted formats incule international VAT number "FI12345678" or number without the dash: "12345678". */
        businessId?: string | null;
        /** Official name of the company.
    Source: Tiedostonimi.Tiedostonimi */
        name?: string | null;
        /** The legal type of the company, as categorized by Palkkaus / used in generating IBAN numbers. */
        companyType?: CompanyType | null;
        /** YTJ's legal type of the company as a string. This is only to be used for display purposes.
    Source: YritysHaku.YritysMuoto */
        companyTypeText?: string | null;
        /** The internal YTJ code number for the company's legal type. */
        ytjCompanyType?: YtjCompanyType | null;
        /** Contact information for the */
        contact?: Contact | null;
        /** Social security number of the contact person in the company, if available. */
        contactPersonPersonalId?: string | null;
        /** Names are not necessarily avaiable. If they are, they may not be reliable. */
        contactPerson?: string | null;
        /** Mobile for or other company phone.
    Difference between Contact.Phone and ContactPhone is that
    this (ContactPhone) is first mobile number, then company general phone if the former was null.
    Contact.Phone is fisrt general phone number and mobile number only if not available. */
        contactPhone?: string | null;
        /** Web site address */
        www?: string | null;
        /** The sector where the company is active. (Toimialatieto.Koodi) */
        sectorOfBusinessCode?: number | null;
        /** Text of the sector where company operates. */
        sectorOfBusinessText?: string | null;
    }
    /** Search result for free text search from the Finnish Business Infromation System (YTJ/BIS). Returns a list of companies without details. */
    export interface YtjSearchResult {
        /** The original search string - without trimming etc. that are done in the actual search. */
        rawInput?: string | null;
        /** If true, search is a success. This may mean that no results were found though. */
        isSuccess?: boolean | null;
        /** If true, the searh is too generic.
    Typically either there was more than 200 results or less than 3 characters in the search string.
    Both of these return an error in the API. */
        isTooGeneric?: boolean | null;
        /** Technical status - for developers / admin. */
        status?: string | null;
        /** A message that can be shown to the end user. */
        userMessage?: string | null;
        /** API quota counter */
        counter?: number | null;
        /** Result of the search. */
        result?: YtjSearchResultCompany[] | null;
    }
    /** First version of the product that integrates the Palkkaus.fi-salary payments to employers accounting. */
    export interface BaseSalaryProduct {
        /** A campaign code that may affect product pricing */
        campaignCode?: string | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** The status text that should give the user.
    Restrict the length to 56 characters. */
        readonly status?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If true, the product is enabled for the current user */
        enabled?: boolean | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Service related to integration, reporting and payments to tax authorities. */
    export interface TaxProduct {
        /** The reference number for the tax authorities */
        paymentReferenceNumber?: string | null;
        /** Defines how the reporting is done for Incomes registry (Tulorekisteri). */
        irReporting?: TaxReportHandling | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** The status text that should give the user.
    Restrict the length to 56 characters. */
        readonly status?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If true, the product is enabled for the current user */
        enabled?: boolean | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Product that helps employer to handle the mandatory unemployment insurance reports and payments. */
    export interface UnemploymentProduct {
        /** If true, the account has paid salaries this year outside Palkkaus.fi.
    This affects the yearly reporting - Palkkaus needs to contact the account.
    Note that this is a shared option with the taxation product. */
        isSalariesPaidThisYear?: boolean | null;
        /** If true, the account has made prepayments or is registered to make prepayments.
    Palkkaus needs to contact the account. */
        isPrepayments?: boolean | null;
        /** If true, the account himself handles the unemployment insurance notifications. */
        isNotificationsSelfHandling?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** The status text that should give the user.
    Restrict the length to 56 characters. */
        readonly status?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If true, the product is enabled for the current user */
        enabled?: boolean | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Pension for Workers: The payments and monthly reporting. */
    export interface PensionProduct {
        readonly status?: string | null;
        /** The use has a pension contract done (as opposed to acting as a temporary employer that pays the pension without a acontract). */
        isPensionContractDone?: boolean | null;
        /** The pension company where the user has a contract. */
        pensionCompany?: PensionCompany | null;
        /** Number for the contract. */
        pensionContractNumber?: string | null;
        /** If true, the user handles the reporting and payments himself/herself. */
        isPensionSelfHandling?: boolean | null;
        /** If true, there is a new pending contract. */
        isPendingContract?: boolean | null;
        /** Enabled property set automatically by IsPensionSelfHandling. */
        enabled?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Employer insurance. Currently, this is the mandatory employer insurance only. */
    export interface InsuranceProduct {
        readonly status?: string | null;
        /** If set to true, the insurance is set to a partner insurance. */
        isPartnerInsurance?: boolean | null;
        /** If true, the user indicates that he/she has already made an insurance contract.
    Should also fill in the InsuranceCompany and InsuranceContractNumber */
        isInsuranceContractDone?: boolean | null;
        /** Company for the current insurance */
        insuranceCompany?: InsuranceCompany | null;
        /** Contract number for the current insurance. */
        insuranceContractNumber?: string | null;
        /** Enabled property set automatically by IsPartnerInsurance. */
        enabled?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** First version of the product that integrates the Palkkaus.fi-salary payments to employers accounting. */
    export interface AccountingProduct {
        readonly status?: string | null;
        accountantName?: string | null;
        accountantEmail?: string | null;
        accountantPhone?: string | null;
        /** Enabled property set automatically by AccountantName, AccountantEmail and AccountantPhone. */
        enabled?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Employer health care from the company Heltti */
    export interface HealthCareHelttiProduct {
        /** Defines the product package */
        package?: HelttiProductPackage | null;
        /** Industry of work */
        industry?: HelttiIndustry | null;
        readonly status?: string | null;
        /** Enabled property set automatically by HelttiProductPackage. */
        enabled?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    export interface PensionEntrepreneurProduct {
        readonly status?: string | null;
        /** If true, user wants to order a YEL-contract. */
        tempContractMarker?: boolean | null;
        /** Enabled property set automatically by TempContractMarker. */
        enabled?: boolean | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Defines the pricing partner, service model and pricing model. */
    export interface PricingProduct {
        /** Current status of pricing. */
        readonly status?: string | null;
        /** Enabled property set automatically if partner set and partner specific pricing model. */
        enabled?: boolean | null;
        /** Defines the pricing partner, service model and pricing model. */
        pricingPartner?: PricingPartner | null;
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If false, it is not bossible to enable this product for the current account / environment.
    Typically, this means that the UI should not show this product.
    E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product. */
        visible?: boolean | null;
        /** Indicates (true-value) if the end user has confirmed the product options.
    If false, the end user has not confirmed the option. */
        visited?: boolean | null;
    }
    /** Data for building the Visma Sign API request form. This is the legacy API v0.2: https://allekirjoitus.visma.fi/download/visma_sign-api.pdf */
    export interface VismaSignPostData {
        /** Palkkaus.fi Customer code to Visma Sign-system */
        customer?: string | null;
        /** Failure address - needs to be non-encrypted, because the error may be in the encryption */
        return_failure?: string | null;
        /** Rest of the data as encrypted JSON. */
        data?: string | null;
        /** Encryption Initialization vector */
        iv?: string | null;
        /** Specifies the Tupas authentication service (e.g. 'osuuspankki', 'nordea').
    If not specified, Visma Sign will show its own selection screen. */
        auth_service?: string | null;
        /** Document URL for preview purposes.
    To Visma Sign, this is sent within the encrypted data field. */
        documentUrl?: string | null;
    }
    /** Search result list level of detail of companies coming from YTJ. */
    export interface YtjSearchResultCompany {
        /** The Business ID (Y-tunnus / Business Identity Code) is a code given to businesses and organizations by the PRH or the Tax Administration.
    It consists of seven digits, a dash and a control mark, for example 1234567-8.
    Alternative accepted formats incule international VAT number "FI12345678" or number without the dash: "12345678". */
        businessId?: string | null;
        /** Official name of the company. */
        name?: string | null;
        /** Numeric ID ("Yritysavain") */
        id?: number | null;
        /** Type of the company. This is a free text type. Not necessary parseable to company type enumeration in details. */
        type?: string | null;
    }
    /** Defines the partner, service and pricing model for the customer. */
    export interface PricingPartner {
        /** Partner account id. */
        accountId?: string | null;
        /** Pricing model by the partner. */
        pricingModel?: PricingModel | null;
        /** Fee for one calculation, which is visible and paid by customer.
    This is not relevant if the PricingModel is PalkkausFee */
        fixedFee?: number | null;
        /** Service model by the partner. */
        serviceModel?: ServiceModel | null;
    }
    /** Request to assure that a company account exists: Either an account is returned or an account stub (non-verified) will be created. */
    export interface AssureCompanyAccountRequest {
        /** Company number in the the trade register (Y-tunnus).
    Format is "1234567-8" - 7 numbers, one control character, with or without the dash.
    Also, the international VAT format is allowed: FI12345678 */
        officialId: string;
        /** The account ID at the partner site.
    If present, this ID is used to restrict the authorization to one account at partner site only. */
        partnerAccountId?: string | null;
        /** Contact e-mail */
        email?: string | null;
        /** Telephone number for the contact person */
        telephone?: string | null;
        /** First name of the contact person */
        contactFirstName?: string | null;
        /** Last name of the contact person */
        contactLastName?: string | null;
    }
    /** Information about a Customer account for a Partner */
    export interface PartnerCompanyAccountInfo {
        /** Account Id */
        accountId?: string | null;
        /** Account Id fomratted for dispaly to end user */
        accountIdFormatted?: string | null;
        /** Avatar for display information */
        avatar?: Avatar | null;
        /** If true, the account has a verified owner. */
        isVerified?: boolean | null;
        /** If true, the current user (the partner) is authorized to access the account */
        isAuthorized?: boolean | null;
        /** The partner account ID to which the access is currently restricted. */
        partnerAccountId?: string | null;
    }
    /** Account data that is relevant for a Worker. */
    export interface WorkerAccount {
        /** Tha bank account number of the Worker - in Iban format. */
        ibanNumber?: string | null;
        /** If created (and still owned) by an employer, this is the AccountOwnerId of the employer who created this account. */
        employerId?: string | null;
        /** The Person ID of the worker if known. */
        officialPersonId?: string | null;
        /** Age range that is relevant to workers salary calculation and/or employment.
    Set this especially if OfficialPersonId is not set.
    If OfficialPersonId is valid, this property is always set based on it. */
        ageRange?: AgeRange | null;
        /** Information needed when salary is paid to this account and other such stuff about the account as a worker. */
        workerInfo?: WorkerInfo | null;
        /** Current Employment Relation: The data that is owned by the Employer about the Worker
    even if the Worker has registered and created an account.
    If employer has several Employment Relations, this is the one that is marked as Primary. */
        employment?: WorkerAccountEmployment | null;
        /** Provides validation errors currently only in Save operations,
    but potentially in the future also in detail/list views. */
        validation?: ApiValidation | null;
        /** Entity type of the account.
    Type PersonCreatedByEmployer means that the Worker has not yet signed into the system and there may be multiple instances of the same logical person (Official ID) - one for each employer. */
        entityType?: LegalEntityType | null;
        /** If true the object Account is and Employer account - it has the EmployerInfo object */
        isEmployer?: boolean | null;
        /** If true the object Account is and Worker account - it has the WorkerInfo object */
        isWorker?: boolean | null;
        /** If true, the account has been verified. Typically this means a digitally signed contract.
    This means that the account also has an Identity object.
    However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
        isVerified?: boolean | null;
        /** Avatar is the visual representation of the Account. */
        avatar?: Avatar | null;
        /** Contact information for the Account. */
        contact?: Contact | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Employment Relation as it is shown as part of WorkerAccount. */
    export interface WorkerAccountEmployment {
        pensionCalculation?: PensionCalculation | null;
        /** Type of the employment relation */
        type?: EmploymentRelationType | null;
        /** Type of additional income earner data by National Incomer Register classification . If several types apply, all of them should be selected. */
        irIncomeEarnerTypes?: IncomeEarnerType[] | null;
        /** Describes the work that is being performed in this Employment Relation. */
        work?: WorkDescription | null;
        /** Defines how the holidays are specified to this Worker. */
        holidays?: HolidaySpecification | null;
        /** Usecase is data related to the source and processing of the Employment data outside the Salaxy backend service / system.
    Typically, Usecase data would be processed by an external system that has originally created the
    Employment Relation (Worker / Employee in many systems). E.g. expenses system, hourly reporting system. */
        usecase?: UsecaseData | null;
        /** Indicates client version.
    This is required for solving backward compatibility issues during the transition period to new versions.
    This property is never persisted to any storage.
    TODO: consider moving this to upper level. */
        clientVersion?: string | null;
        /** Start date of the Employment Relation. */
        startDate?: string | null;
        /** End date of the Employment Relation. */
        endDate?: string | null;
        /** Duration of the employment. If true, the employment relation is for a fixed term.
    Otherwise (which is the default) the employment relation continues until further notice. */
        isFixedTerm?: boolean | null;
        /** Indicates that the employment relation has been terminated.
    Saving with this status also makes the employment relation Archived. */
        isTerminated?: boolean | null;
        /** If true, the worker is the same person as the actual signed owner of the account. */
        isAccountOwner?: boolean | null;
    }
    /** Describes the work that is beign performed - mainly for Employment relation purposes. */
    export interface WorkDescription {
        /** Free text description of the work that is being performed. */
        description?: string | null;
        /** Occupation classification, used at least for Accident insurance purposes, but may be used for other reporting.
    For Finnish Salaries use the Statistics Finland
    "Classification of Occupations 2010" (TK10): https://www.stat.fi/meta/luokitukset/ammatti/017-2018-05-15/index_en.html */
        occupationCode?: string | null;
        /** Contains default calculation rows for the worker.
    Typically used for setting for example monthly salary or hourly salary price for workers. */
        salaryDefaults?: UserDefinedRow[] | null;
        /** Default bases for wage. */
        wageBasis?: WageBasis | null;
    }
    /** Specifies how the holidays are handled for this Worker. */
    export interface HolidaySpecification {
        /** Defines how the holidays are handled for the worker. */
        code?: HolidayCode | null;
        /** Defines the number of holidays that the worker is entitled per month worked.
    If the value is set to 0 (default), the number is 2 for first year and 2,5 thereafter as defined in the law.
    To actually se the daysPerMonth to zero, use code value other than Permanent14Days or Permanent35Hours. */
        accrualFixed?: number | null;
        /** Holiday compensation as percent of salary (11.5% is 0.115) */
        compensation?: number | null;
        /** Method for paying holiday bonus in this salary calculation. */
        bonusMethod?: HolidayBonusPaymentMethod | null;
        /** Holiday bonus for the worker (lomaraha, lomaltapaluuraha).
    Typically 0.5 for 50%. */
        bonus?: number | null;
    }
    /** Contains order data for Varma pensions (TyEL, YEL) and required operations. */
    export interface VarmaPensionOrder {
        /** Orderer information. */
        orderer?: InvoiceParty | null;
        /** Action for the order. */
        action?: VarmaPensionOrderAction | null;
        /** Validation information. */
        validation?: ApiValidation | null;
        /** YEL data for the order */
        yel?: VarmaPensionOrderYel | null;
        /** TyEL data for the order */
        tyel?: VarmaPensionOrderTyel | null;
    }
    /** Varma pension order YEL details. */
    export interface VarmaPensionOrderYel {
        /** Personal id number. */
        personalId?: string | null;
        /** Payer definition. */
        payer?: VarmaPensionOrderYelPayer2 | null;
        /** If true, indicates that existing contract will be moved to Varma. */
        isMove?: boolean | null;
        /** Estimated yearly income. */
        yearlyIncome?: number | null;
        /** Start date for the new pension. */
        startDate?: string | null;
        /** Movement timing. (Q1,Q2,Q3 or Q4, or as soon as possible) */
        moveMoment?: string | null;
        /** Indicates if the existing yearly income estimate will be changed. */
        isYearlyIncomeChange?: boolean | null;
    }
    /** Varma pension order TyEL details. */
    export interface VarmaPensionOrderTyel {
        /** Personal id number. */
        personalId?: string | null;
        /** If true, indicates that existing contract will be moved to Varma. */
        isMove?: boolean | null;
        /** Estimated yerly income. */
        yearlyIncome?: number | null;
        /** Start year for the new order. */
        startYear?: string | null;
        /** Start month for the new order. */
        startMonth?: string | null;
        /** Movement timing. (Q1,Q2,Q3 or Q4, or as soon as possible) */
        moveMoment?: string | null;
    }
    export interface IfInsuranceOrder {
        /** Company for the current insurance */
        insuranceCompany?: InsuranceCompany | null;
        /** Contract number for the current insurance. */
        insuranceContractNumber?: string | null;
        /** If true, indicates that existing contract will be moved to If. */
        isMove?: boolean | null;
        /** Occupation code. */
        occupationCode?: string | null;
        /** Estimated yearly income. */
        yearlyIncome?: number | null;
        /** Start date for the new pension. */
        startDate?: string | null;
        /** Validation information. */
        validation?: ApiValidation | null;
    }
    /** Payment object requested to be paid for completing the underlying business objects. */
    export interface Payment {
        /** Payer as it would be on the invoice */
        payer?: Payer | null;
        /** Recipient as it would be on the invoice */
        recipient?: Recipient | null;
        /** Amount requested to be paid. */
        amount?: number | null;
        /** Bank reference number/string. Will be generated by the system automatically. */
        reference?: string | null;
        /** Message for the payment. */
        message?: string | null;
        /** Due date for the payment. */
        dueDate?: string | null;
        /** Status of the payment. */
        status?: PaymentStatus | null;
        /** Status text giving more information about the status. */
        statusText?: string | null;
        /** List of business objects (calculations etc.) on which the payment is based on. */
        businessObjects?: BusinessObject[] | null;
        /** Request specific payment parameters. */
        request?: PaymentRequestParameters | null;
        /** Payment method. */
        method?: PaymentMethod | null;
        /** Payment type. */
        type?: BankPaymentType | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** The paying party */
    export interface Payer {
        fullName?: string | null;
        accountIBAN?: string | null;
        bic?: string | null;
        /** This field is mandatory for SEPA payment scenario. */
        bankPartyIdentification?: string | null;
        contactInfo?: Contact | null;
        avatar?: Avatar | null;
    }
    export interface Recipient {
        fullName?: string | null;
        accountIBAN?: string | null;
        bic?: string | null;
        /** This field is mandatory for SEPA payment scenario. */
        bankPartyIdentification?: string | null;
        contactInfo?: Contact | null;
        avatar?: Avatar | null;
    }
    /** Business object in the Salaxy repository */
    export interface BusinessObject {
        /** Optional name for the business object. */
        name?: string | null;
        /** Id of the business object */
        id?: string | null;
        /** Partition key of the business object */
        owner?: string | null;
        /** Business object type */
        type?: string | null;
        /** Optional property for object instance. */
        object?: any | null;
    }
    /** Indicates both the full and the remaining payable worker net salary amount in the calculation. */
    export interface InstantPaymentSaldo {
        /** The full worker net salary amount in the calculation. */
        full?: number | null;
        /** Current instantly payable amount of the worker net salary which is remaining in the calculation. */
        payable?: number | null;
        /** Supported payment methods for the payment. */
        methods?: PaymentMethod[] | null;
    }
    /** Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization. Typical usecase is that a a company has e.g.a monthly salary list that is paid at the end of month. For next month, a copy is then made from the latest list and the copy is potentially modified with the changes of that particular month. Payroll can also be started from scratch either by just writing salaries from e.g. an e-mail or by uploading an Excel sheet. */
    export interface Payroll {
        /** General information / metadata about the Payroll instance. */
        info?: PayrollInfo | null;
        /** Rows that make the definition of the Payroll. */
        rows?: PayrollRow[] | null;
        /** Status for the payroll and its calculations in the payment Workflow.
    Yo may only set the value to/from Draft and Template.
    Other values are set by the system and may not be changed through the API. */
        status?: PayrollStatus | null;
        /** Results of the payroll calculations.
    These may be intermediate results (estimations) or based on actual paid calculations. */
        result?: Calculation[] | null;
        /** When saved or validated, provides a validation object for each calculation:
    Whether the calculations are ready for payment. */
        validations?: ApiListItemValidation[] | null;
        /** Snapshot of the Payroll and its calculations when it was last recalculated. */
        snapshot?: PayrollSnapshot | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** General information / metadata about the Payroll instance. */
    export interface PayrollInfo {
        /** Title that can be specified by the end user. */
        title?: string | null;
        /** Paid at date for the payroll. (Date of the employer's payment). */
        paymentDate?: string | null;
        /** The estimated date when the salary is withdrawable by the worker. */
        salaryDate?: string | null;
        /** The requested date for the SalaryDate from the employer. */
        requestedSalaryDate?: string | null;
        /** The reference number of the payment. */
        paymentId?: string | null;
        /** List of calculations generated from the payroll. */
        calculations?: string[] | null;
        /** Period (as dates, not times) for the Payroll.
    Default for the rows. */
        period?: DateRange | null;
    }
    /** Definition row for Payroll */
    export interface PayrollRow {
        /** Worker account ID of the row.
    When Salary calculations are created, the rows are grouped based on this ID. */
        workerAccountId?: string | null;
        /** Flag for Calculation handling in payrolls.
    The flag is typically taken only from the first row of one WorkerAccountId.
    For now, used for marking Entrepreneur Pension (YEL), but may be used to marked other handling.
    At that point, may be changed to Flag (comma separted multi-enumeration). */
        handling?: PayrollCalculationHandling | null;
        /** Logical type of the row */
        rowType?: CalculationRowType | null;
        /** Description text of the row that is shown in reports. If null, will be set according to type. */
        message?: string | null;
        /** Count for the row - default is one */
        count?: number | null;
        /** Price for the row */
        price?: number | null;
        /** Unit for the row. Guessed based on the RowType and count, but you are better of setting it to be sure. */
        unit?: CalculationRowUnit | null;
        /** Possibility to override the Salary Calculation Period for one Worker.
    If null (default), uses the Payroll Period. */
        period?: DateRange | null;
        /** Identifier in the source system is a key defined by a source system / partner system.
    This is a pass-through string that is passed to the result calculations. */
        sourceId?: string | null;
    }
    /** ApiValidation object that is specialized for list items. Provides if and index properties for connecting a validation to a specific item in the list */
    export interface ApiListItemValidation {
        /** Identifier of the item this validation is for.
    Identifiers are not always available (if the item has not been saved), so index is a more generic approach. */
        id?: string | null;
        /** Zero based index of the item this validation is for. */
        index?: number | null;
        /** If true, the data is valid - no errors. */
        readonly isValid?: boolean | null;
        /** If true, has required fields missing data. */
        readonly hasAllRequiredFields?: boolean | null;
        /** Validation errors on invalid field values.
    Note that required fields missing error messages are not here. Only the invalid values. */
        readonly errors?: ApiValidationError[] | null;
    }
    /** Provides Snapshot of the Payroll: In the case of non-paid Payrolls, this is the latest save/validation. In the case of paid Payrolls, this is the status of the Payroll when it was paid. */
    export interface PayrollSnapshot {
        /** Date that the snapshot was created. */
        date?: string | null;
        /** Count of calculations in this Payroll */
        calcCount?: number | null;
        /** Total gross salary */
        totalGrossSalary?: number | null;
        /** Total payment for the Payroll. */
        totalPayment?: number | null;
        /** Total Palkkaus.fi fees for the Payroll */
        fees?: number | null;
        /** If true, all calculations have been validated and they are valid for payment.
    If false, validation has not been done or some calculations are not valid for payment. */
        isReadyForPayment?: boolean | null;
    }
    export interface PayrollListItem {
        /** Input that defines the Payroll. */
        input?: PayrollInput | null;
        /** General summary information about the Payroll and when it was las processed. */
        info?: Payroll03Info | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Input that user defines about the Payroll */
    export interface PayrollInput {
        /** Title that can be specified by the end user. */
        title?: string | null;
        /** The Default Salary period for the Payroll.
    This may be overriden for an individual calculation. */
        defaultPeriod?: DateRange | null;
        /** List of calculations that are attached to this calculation. */
        calculations?: string[] | null;
        /** If set to true, the Payroll is treated as a template - not as an individual payroll that should be paid directly. */
        isTemplate?: boolean | null;
    }
    /** Provides Read-only information about the Payroll In the case of non-paid Payrolls, this is the latest save/validation. In the case of paid Payrolls, this is the status of the Payroll when it was paid. */
    export interface Payroll03Info {
        /** Date that the Info was las updated. */
        date?: string | null;
        /** Count of calculations in this Payroll */
        calcCount?: number | null;
        /** Total gross salary */
        totalGrossSalary?: number | null;
        /** Total payment for the Payroll. */
        totalPayment?: number | null;
        /** Total Palkkaus.fi fees for the Payroll */
        fees?: number | null;
        /** If true, all calculations have been validated and they are valid for payment.
    If false, validation has not been done or some calculations are not valid for payment. */
        isReadyForPayment?: boolean | null;
        /** Status for the payroll and its calculations in the payment Workflow. */
        status?: PayrollStatus | null;
        /** Salary payment date - currently only filled after payment, but may later be used to specify future payment date. */
        paymentDate?: string | null;
        /** The reference number of the payment after payment is made. */
        paymentId?: string | null;
    }
    /** New version 03 of Payroll. Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization. Typical usecase is that a a company has e.g. a monthly salary list that is paid at the end of month. For next month, a copy is then made from the latest list and the copy is potentially modified with the changes of that particular month. */
    export interface PayrollDetails {
        /** Snapshots of the calculations based on the ID's specified in Info */
        calcs?: Calculation[] | null;
        /** When saved or validated, provides a validation object for each calculation:
    Whether the calculations are ready for payment. */
        validations?: ApiListItemValidation[] | null;
        /** Input that defines the Payroll. */
        input?: PayrollInput | null;
        /** General summary information about the Payroll and when it was las processed. */
        info?: Payroll03Info | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    export interface City {
        id?: string | null;
        name?: string | null;
        nameLang2?: string | null;
        grouping?: string | null;
        count?: number | null;
    }
    /** A job for a Public Profile (Osaajaprofiili) or later also for Job ads. */
    export interface ProfileJob {
        id?: string | null;
        name?: string | null;
        category?: string | null;
        picture?: string | null;
        synonyms?: string[] | null;
        /** Context-specific count that can show e.g. Profiles found for this Job */
        count?: number | null;
    }
    /** Category of Profile Jobs */
    export interface ProfileJobCategory {
        /** Unique ID of the Category */
        id?: string | null;
        /** Name / title of the Category */
        name?: string | null;
        /** Backround picture for the Category */
        picture?: string | null;
        /** Context-specific count that can show e.g. Profiles found for this Category */
        count?: number | null;
    }
    /** Profile search result for the API */
    export interface ApiProfileSearchResult {
        cities?: City[] | null;
        jobs?: ProfileJob[] | null;
        jobCategories?: ProfileJobCategory[] | null;
        /** Search text that was input by the user */
        searchText?: string | null;
        /** Search text as it is sent to the search provider:
    Contains provider specific wildcards etc. */
        searchTextTechnical?: string | null;
        /** Sort string */
        sort?: string | null;
        /** Actual search result - list of items */
        results?: ApiProfileSearchResultProfile[] | null;
        /** Dynamic for search facets if faceting is being used.
    NOTE: Facet names are lower camel case - using the JavaScript naming conventions as in the original JSON. */
        facets?: any | null;
        /** Total count returned by the search service.
    This may be more than the count of Result list */
        totalCount?: number | null;
    }
    export interface ApiProfileSearchResultProfile {
        id?: string | null;
        avatar?: Avatar | null;
        jobs?: string[] | null;
        jobCategories?: string[] | null;
        cities?: string[] | null;
    }
    /** Describes a logical generated report. This is a report instance e.g. a monthly report for January 2017. It may contain several files of different types: PDF, preview PNG, HTML etc. */
    export interface Report {
        /** Short title for the report that can be shown to the end user. */
        title?: string | null;
        /** Report date: For monthly and yearly reports this is the first day of the year/month.
    For ad-hoc reports, this is the start date of the period. */
        reportDate?: string | null;
        /** Type of the report */
        type?: ReportType | null;
        /** PDF report for viewing and printing. This is typically the main report.
    For preview and demo, there is typically only HTML version - no PDF. */
        pdfUrl?: string | null;
        /** Thumbnail image for the report.
    For preview and demo data this is typically a fixed image - the thumbnail is only generated when PDF is generated. */
        thumbnail?: string | null;
        /** HTML for the report. Use salaxy-rpt.css for formatting.
    For preview and demo, this is the format to show to the user - PDF is not generated. */
        htmlUrl?: string | null;
        /** Some reports are also available in JSON format, from which you can create lists, CSV, Excel files etc. */
        jsonUrl?: string | null;
        /** If true, the report is in preview state, typically in the middle of the month.
    This means that the data itself is production, its just not final. */
        isPreview?: boolean | null;
        /** If true, the report is created using demo data - should be used only in the beginning when the user has not paid any salaries.
    This may be e.g. calculations in draft stage or completely invented demo data such as example workers.
    Demo data should not be confused to test database (test-api.palkkaus.fi / test-api.salaxy.com):
    Paid calculations in test database are production data. */
        isDemoData?: boolean | null;
        /** Time when the PDF, HTML or JSON URL's expire IF they are generated in a way that
    they are callable without authentication for a specified time period.
    This property is null if the URL requires authentication. */
        urlsExpireAt?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** An accounting report for an employer for a specific period (typically month or year). */
    export interface AccountingReport {
        /** Finnish Business Identifier (Y-tunnus) for the company. */
        businessId?: string | null;
        /** Employer avatar */
        employer?: Avatar | null;
        /** Contact information for the report. Typically the employer, but may be something else.
    By desing, the contact information is not filled in for Private Person employers. */
        contact?: Contact | null;
        /** Start date for the report */
        startDate?: string | null;
        /** End date for the report */
        endDate?: string | null;
        /** The total sum information for the report - this is the main payload of the report */
        result?: CalculationResult | null;
        /** Individual calculations based on which the report is made.
    Depending on the request, this may be filled in or null (as opposed to empty list). */
        calculations?: Calculation[] | null;
    }
    /** Contains calculation data structured for easy rendering in reports. */
    export interface CalculationReport {
        /** Calculation report type. */
        reportType?: ReportType | null;
        /** Report header section. */
        header?: CalculationReportHeader | null;
        /** Report row groups. */
        rowGroups?: ICalculationReportRowGroup[] | null;
        /** Validation messages for the report.
    Contains for example deduction warnings. */
        validationMessages?: CalculationReportText[] | null;
        /** Returns true if the report has validation messages. */
        readonly hasValidationMessages?: boolean | null;
        /** Additional notes for the report. */
        notes?: CalculationReportText[] | null;
        /** Returns true if the report has notes. */
        readonly hasNotes?: boolean | null;
    }
    /** Report header information for the report. Contains employer and worker details as well as further info about the work. */
    export interface CalculationReportHeader {
        /** Employer name and contact details. */
        employer?: CalculationReportEmployer | null;
        /** Worker name and contact details. */
        worker?: CalculationReportWorker | null;
        /** Start date of the work. */
        workStartDate?: string | null;
        /** End date of the work. */
        workEndDate?: string | null;
        /** Description of the work for reporting purposes. */
        workDescription?: string | null;
        /** The day when the total amount was paid by Employer to Palkkaus.fi */
        paidAt?: string | null;
        /** The estimated date for the salary in worker. */
        salaryDate?: string | null;
        /** The actual date for the salary in worker. */
        requestedSalaryDate?: string | null;
        /** Number of days may affect calculation of different payments calculated based on a Framework agreement. */
        numberOfDays?: number | null;
        /** If true, the worker has not given any tax card, 60% will be taken. */
        workerHasNoTaxcard?: boolean | null;
        /** Calculated tax percent. Please note this is a number between 0-1. */
        taxPercent?: number | null;
        /** Tax percent in the tax card. */
        taxcardTaxPercent?: number | null;
        /** Additional tax percent in the tax card. */
        taxcardTaxPercent2?: number | null;
    }
    /** Interface for report row group. */
    export interface ICalculationReportRowGroup {
        /** Group total. */
        total?: number | null;
    }
    /** Generic text object for the report. Contains heading, text and optional interpolation parameters. */
    export interface CalculationReportText {
        /** Heading for the text. */
        heading?: string | null;
        /** The actual content for the text. */
        text?: string | null;
        /** Parameters to be interpolated in the text. */
        parameters?: {
            [key: string]: any;
        } | null;
    }
    /** Employer name and contact details for reports. */
    export interface CalculationReportEmployer {
        /** Finnish Business Identifier (Y-tunnus) for the company. */
        businessId?: string | null;
        /** Employer avatar. */
        avatar?: Avatar | null;
        /** Employer contact information. */
        contact?: Contact | null;
    }
    /** Worker name and contact details for reports. */
    export interface CalculationReportWorker {
        /** The Person ID of the worker if known. */
        socialSecurityNumberValid?: string | null;
        /** Bank account number. */
        ibanNumber?: string | null;
        /** Employer avatar. */
        avatar?: Avatar | null;
        /** Worker contact information. */
        contact?: Contact | null;
    }
    /** Contains accounting rows for one or many calculations. */
    export interface AccountingReportTable {
        /** All rows in the report table. */
        rows?: AccountingReportRow[] | null;
        /** Accounting report table type. */
        tableType?: AccountingReportTableType | null;
        /** Finnish Business Identifier (Y-tunnus) for the company. */
        businessId?: string | null;
        /** Employer avatar */
        employer?: Avatar | null;
        /** Contact information for the report. Typically the employer, but may be something else.
    By desing, the contact information is not filled in for Private Person employers. */
        contact?: Contact | null;
        /** Period for the report. */
        period?: DateRange | null;
        /** Summary containing relevant numbers for the report. For example,
    number of calculations, gross and net amounts, payment amounts and Palkkaus fee. */
        summary?: AccountingReportTableSummary | null;
    }
    /** Accounting report table row. */
    export interface AccountingReportRow {
        /** Row type. Default is the Booking. */
        rowType?: AccountingReportRowType | null;
        /** Amount for the row: May be positive or negative(depending on the depet/credit and account type). */
        amount?: number | null;
        /** Percentage of the value added tax that is included in the amount. */
        vatPercent?: number | null;
        /** Proposed account number for the row. */
        accountNumber?: string | null;
        /** Proposed account text for the row. */
        accountText?: string | null;
        /** Salaxy calculation row type. */
        code?: AccountingReportRowCode | null;
        /** Salaxy calculation result row type. */
        resultCode?: AccountingReportRowResultCode | null;
        /** Returns product code. (Code or ResultCode in this order). */
        readonly productCode?: string | null;
        /** Returns product name. (Code or ResultCode in this order). */
        readonly productName?: string | null;
        /** Text for the booking entry / header. */
        text?: string | null;
        /** Space separated set of class names for CSS styling purposes. */
        styles?: string | null;
        /** Count of units. Default is 1. */
        count?: number | null;
        /** Price for the unit. */
        price?: number | null;
        /** Unit identifier; */
        unit?: string | null;
        /** Additional information for the row. */
        additionalInformation?: string | null;
    }
    /** Summary of the accounting report. Contains main figures: totals and workers and employer total figures. */
    export interface AccountingReportTableSummary {
        /** The total number of calculations included in the report. */
        count?: number | null;
        /** Calculation totals that are common to both Worker and Employer. */
        totals?: TotalCalculationDTO | null;
        /** The calculation from the Employer point-of-view */
        employerCalc?: EmployerCalculationDTO | null;
        /** The calculation from the Worker point-of-view */
        workerCalc?: WorkerCalculationDTO | null;
    }
    /** Passes the data about the authorization that is the bases for callin the services that require user authorization. */
    export interface UserSession {
        /** If false, the current user needs to be authenticated or authenticated user does not have acces to the system. */
        isAuthorized?: boolean | null;
        /** If false, the current user does not have a credential in the system =&gt; need to redirect to login */
        hasCredential?: boolean | null;
        /** If false, the current user does not have a current account.
    If HasCredential is true, this means that the user should be redirected to Account creation wizard. */
        hasCurrentAccount?: boolean | null;
        /** Authorization status */
        status?: AuthorizationStatus | null;
        /** Avatar that should be shown for the current session.
    Typically CurrentAccount.Avatar, but could theoretically be something different. */
        avatar?: Avatar | null;
        /** The current user credentials. */
        currentCredential?: SessionUserCredential | null;
        /** The current Palkkaus.fi-account that the user acting as */
        currentAccount?: IVerifiedAccount | null;
        /** The site from which the API call is being made. */
        requestingPartnerSite?: PartnerSite | null;
        /** Contains html for system alert. */
        systemAlertHtml?: string | null;
    }
    /** An account that is verified. Typically this means a digitally signed contract. This means that the account also has an Identity object. However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
    export interface IVerifiedAccount {
        /** Information about the verification of the account.
    NOTE that this object may not be present in many method calls - this is because it contains confidential information. */
        identity?: Identity | null;
        /** If true, the account has been verified. Typically this means a digitally signed contract.
    This means that the account also has an Identity object.
    However, as the Identity object contains confidential information, it is not necessarily present in all method calls. */
        readonly isVerified?: boolean | null;
        /** Avatar is the visual representation of the Account. */
        avatar?: Avatar | null;
        /** Contact information for the Account. */
        contact?: Contact | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** API SmsMessage */
    export interface SmsMessage {
        /** Message body */
        body?: string | null;
        /** Message Sender (ID) */
        sentByUser?: string | null;
        /** Message From (characters or numbers) */
        from?: string | null;
        /** Message To (phone) */
        to?: string | null;
        /** Message To (ID) */
        receivedByUser?: string | null;
        /** Sender name */
        senderName?: string | null;
        /** Receiver name */
        receiverName?: string | null;
        /** Receiver's message */
        isReceivedMessage?: boolean | null;
        /** Message sent */
        sentAt?: string | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Provides the results of the tax card calculation and business logic. */
    export interface TaxCardResult {
        /** Sum of income for this year based on the items marked on the tax card. */
        yearlyPreviousIncome?: number | null;
        /** Provides a decimal number for a recommended widthholding tax IF it is possible to calculate that based on given information.
    Null, if the information is not sufficient to give a recommendation. */
        tax?: number | null;
    }
    /** Full tax card that is stored in database and that is used in tax card percentage calculations. Also contains the history etc. */
    export interface Taxcard {
        /** The Worker, whose salaries are paid based on this tax card. */
        worker?: Avatar | null;
        /** The input that is coming from the Worker / Tax authorities */
        card: TaxcardInput;
        /** Calculations that have been paid to this tax card. */
        incomeLog?: TaxCardIncome[] | null;
        /** Identifier of the object. */
        id?: string | null;
        /** The date when the object was created. */
        createdAt?: string | null;
        /** The time when the object was last updated.
    Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
        updatedAt?: string | null;
        /** Owner ID for this data */
        owner?: string | null;
        /** Indication that for the currently logged-in account, the data is generally read-only. */
        isReadOnly?: boolean | null;
        /** Partner service that has be used in modifying the object. */
        partner?: string | null;
    }
    /** Input from user for a tax card. Contains the information of Snapshot plus additionally, the */
    export interface TaxcardInput {
        /** The personal ID as written in the tax card. */
        personalId: string;
        /** Tax card approval state */
        state?: TaxcardState | null;
        /** Year that the tax card is valid for. Typically this means from February of this year to January of the following. */
        forYear: number;
        /** Validity for the taxcard as expressed in the card.
    Note that the end date may not be reliable if new taxcard has replaced this one. */
        validity?: DateRange | null;
        /** Tax percent as percent. I.e. for 50% set 50, not 0.5. */
        taxPercent?: number | null;
        /** Income up to which the TaxPercent can be used.
    Any income above the limit is taxed with ExcessTaxPercent. */
        incomeLimit?: number | null;
        /** Tax percentage that is used in calculating the widthholding tax for the part of income above the IncomeLimit. */
        taxPercent2?: number | null;
        kind?: TaxcardKind | null;
        /** Uri to the file copy of the tax card.
    Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
        fileUri?: string | null;
        /** Uri to the preview image of the tax card.
    Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
        previewUri?: string | null;
    }
    /** Defines income that is recorded in a Taxcard: A paid salary calulation, income from previous employers or other salaries that affect the income calculation. */
    export interface TaxCardIncome {
        /** Type of income */
        type?: TaxCardIncomeType | null;
        /** Calculation id in the Salaxy database.
    If the calculation is not in the Salaxy Database, leave this null. */
        id?: string | null;
        /** A short description of the paid calculation.
    This is a mandatory field if the Id is null. */
        description?: string | null;
        /** Beginning of the salary payment period from taxation point-of-view.
    This is typically the same as CalcInfo.WorkStartDate, but may be different in edge cases. */
        startDate?: string | null;
        /** End of the salary payment period from taxation point-of-view.
    This is typically the same as CalcInfo.WorkEndDate, but may be different in edge cases. */
        endDate?: string | null;
        /** The paidAt date which tells mainly to which year the payment should be attributed.
    This is reliable only if the salary is paid by Salaxy (TaxCardIncomeType.SalaxyCalculation).
    In other cases it is just the reporting date. */
        paidAt?: string | null;
        /** Taxable income from this calculation */
        income?: number | null;
        /** Widthholding tax in euro */
        tax?: number | null;
    }
    /** Test object with value properties. */
    export interface TestValues {
        /** Test integer. */
        intValue?: number | null;
        /** Test double. */
        doubleValue?: number | null;
        /** Test decimal. */
        decimalValue?: number | null;
        /** Test string. */
        stringValue?: string | null;
        /** Test DateTime. */
        dateTimeValue?: string | null;
        /** Test bool. */
        boolValue?: boolean | null;
        /** Test object. */
        objectValue?: any | null;
        /** Test array */
        arrayValue?: any[] | null;
        /** Test enum */
        enumValue?: TestValuesEnumValue | null;
    }
    /** Object containing essential details of the test account */
    export interface TestAccountInfo {
        /** Bearer token for authentication. */
        token?: string | null;
        /** The current user credentials. */
        userCredential?: SessionUserCredential | null;
    }
    /** Handles the Calculation of CalculationRowType.CarBenefitUsecase to IrRows */
    export interface CarBenefitUsecase {
        /** Age group as in A,B,C or I for international. */
        ageGroup?: AgeGroupCode | null;
        /** If true, the calculation is kilometers based. */
        isKilometersBased?: boolean | null;
        /** Number of kilometers is filled in only if that type of benefit calculation is used. */
        kilometers?: number | null;
        /** Deduction from salary */
        deduction?: number | null;
        /** Primary subtyping of the usecase. */
        kind?: CarBenefitKind | null;
    }
    /** Handles the Calculation of CalculationRowType.DailyAllowance to IrRows */
    export interface DailyAllowanceUsecase {
        /** Primary subtyping of the usecase. */
        kind?: DailyAllowanceKind | null;
    }
    /** Handles the Calculation of CalculationRowType.MealBenefit to IrRows */
    export interface MealBenefitUsecase {
        /** Deduction from salary */
        deduction?: number | null;
        /** The taxable price calculated by the server. */
        taxablePrice?: number | null;
        /** If true, the deduction from Worker salary corresponds to taxable value. */
        isTaxValue?: boolean | null;
        /** Primary subtyping of the usecase. */
        kind?: MealBenefitKind | null;
    }
    /** Handles the Calculation of CalculationRowType.NonProfitOrg to IrRows */
    export interface NonProfitOrgUsecase {
        /** Primary subtyping of the usecase. */
        kind?: NonProfitOrgKind | null;
    }
    /** Handles the Calculation of CalculationRowType.SubsidisedCommute to IrRows */
    export interface SubsidisedCommuteUsecase {
        /** Deduction from salary */
        deduction?: number | null;
        /** Number of periods if Kind is PeriodicalDeduction.
    Otherwise this value is null. */
        periodDivider?: number | null;
        /** Primary subtyping of the usecase. */
        kind?: SubsidisedCommuteKind | null;
    }
    /** Handles the Calculation of CalculationRowType.UnionPayment to IrRows */
    export interface UnionPaymentUsecase {
        /** If true, deduction is done in Salaxy backoffice, not directly by employer.
    As of writing, this is only done for Rakennusliitto / Construction. */
        isDeductedAtBackoffice?: boolean | null;
        /** If true, the value is a fixed sum: the price should not be updated based on salary and
    the count should not be updated based on percentage. */
        isFixedSum?: boolean | null;
        /** Primary subtyping of the usecase. */
        kind?: UnionPaymentKind | null;
    }
    /** The feedback from the account holder at the end-of-year. Possibility to feed in changes, corrections and salaries that are paid outside the Salaxy system. */
    export interface YearlyFeedback {
        /** List of Workers for which salaries have been paid during the year. */
        workerSummaries?: WorkerSummary[] | null;
        /** Avatar for the employer of display purposes in reports etc. */
        employerAvatar?: Avatar | null;
        /** Account ID for the employer. */
        employerId?: string | null;
        /** Year for the feedback */
        year?: number | null;
        /** The satus that the account holder gives to yearly material. */
        userStatus?: YearEndUserFeedback | null;
        /** Free text from the account holder to Salaxy backoffice. */
        userFeedbackText?: string | null;
        /** Time when the feedback was given. */
        userFeedbackTime?: string | null;
        /** Status for administrator check. */
        adminStatus?: YearEndAdminCheck | null;
        /** Short notice for backoffice purposes. This is the text visible in the lists. */
        adminShortNote?: string | null;
        /** Full description of yearly process status for backoffice pusposes. Shown in backoffice detail views. */
        adminNotes?: string | null;
        /** If true, Salaxy should report the salaries to Insurance company.
    Currently, this is available only for Companies and for If insurance company only. */
        sendInsuranceReport?: boolean | null;
        /** Bank IBAN number for yearly refunding of extra customer assets.
    READONLY: Note that once set to a valid IBAN, this number is read-only and must be changed through customer service or onboarding (requires authentication). */
        bankIban?: string | null;
        /** Yearly feedback data that is specific to household customers.
    Main part is the information realted to household deduction and how it can be divided between spouses. */
        household?: HouseholdYearlyFeedback | null;
        /** Temporary user interface helper for external workers =&gt; The final implementation will add workers to WorkerSummaries. */
        externalWorkerSalaries?: number | null;
        /** Temporary user interface helper for external workers (insurance) =&gt; The final implementation will add workers to WorkerSummaries. */
        externalWorkerInsuranceWork?: string | null;
    }
    /** Year end summary for one Worker account. */
    export interface WorkerSummary {
        /** Worker account to which the salaries have been paid. */
        worker?: WorkerAccount | null;
        /** Total of Calculations and Corrections as calculatied by the server side logic. */
        summary?: CalculationSummary | null;
        /** Calculations that have been paid through Salaxy system. */
        calculations?: CalculationSummary[] | null;
        /** Corrections that are made to existing calculations as well as
    salary calculations that are paid outside Salaxy. */
        corrections?: CalculationSummary[] | null;
        /** Type of work for insurance purposes when set on worker level, not for each calculation separately. */
        insuranceWork?: string | null;
    }
    /** Yearly feedback data that is specific to household customers. Main part is the information realted to household deduction and how it can be divided between spouses. */
    export interface HouseholdYearlyFeedback {
        /** Amount of accident insurance paid during this year as reported by the user
    This is relevant for households for Household deduction:
    This amount can be deducted from the yearly salaries and needs to be reported to tax authorities. */
        insuranceAmount?: number | null;
        /** Amount of accident insurance paid during this year from parner insurance (in Palkkaus.fi records).
    This is relevant for households for Household deduction:
    This amount can be deducted from the yearly salaries and needs to be reported to tax authorities. */
        insuranceAmountFromAccount?: number | null;
        /** Name of the spouse with who the household deduction is split */
        spouseName?: string | null;
        /** Finnish Personal ID of the spouse with who the household deduction is split */
        spousePersonalId?: string | null;
    }
    /** Summarises from a large calculation object the relevant data for yearend feedback. */
    export interface CalculationSummary {
        /** Type of Calculation summary */
        type?: YearEndFeedbackCalculationType | null;
        /** Identifier of the calculation or correction. */
        calculationId?: string | null;
        /** Date when the calculation was paid or where the correction should be set. */
        paidAt?: string | null;
        /** If true the original calculation was marked as deductible (household deduction). */
        originalIsDeductible?: boolean | null;
        /** If true the the calculation is currently marked as deductible (household deduction).
    Default value is OriginalIsDeductible. */
        selectedIsDeductible?: boolean | null;
        /** Taxable salary of the calculation */
        salary?: number | null;
        /** The mandatory side costs for taxation purposes */
        sideCosts?: number | null;
        /** Categories for household deduction.
    UI implementors should force this enumeration being a single value although historically flags were supported. */
        taxDeductionCategories?: TaxDeductionWorkCategories | null;
        /** Total gross salary */
        grossSalary?: number | null;
        /** Widthholding tax deducted from the Worker */
        withholdingTax?: number | null;
        /** Pension and unemployment insurance deductions from the Worker */
        socialInsuranceDeductionsFromEmployee?: number | null;
        /** Phone benefit in euros */
        phoneBenefit?: number | null;
        /** Compensations is daily allowance (including half days) plus meals compensation. */
        compensations?: number | null;
        /** Benefits excluding phone benefit: Accomodation, Meals and other */
        benefitsInKindWithoutPhone?: number | null;
        /** Milage compensation includes MilageOwnCar, Daily milage as well as other milage */
        milageCompensation?: number | null;
        /** Message related to this row. Mainly for backoffice purposes. */
        message?: string | null;
        /** Type of work for insurance purposes. */
        insuranceWork?: string | null;
        /** Type of salary from Pension and Taxation point-of-view */
        salaryType?: SalaryType | null;
    }
    /** AbcSection enumeration */
    export enum AbcSection {
        Undefined = "undefined",
        DoNotShow = "doNotShow",
        Employment = "employment",
        Insurance = "insurance",
        Salary = "salary",
        Examples = "examples",
        PalkkausGeneral = "palkkausGeneral",
        PalkkausInstructions = "palkkausInstructions",
        Blog = "blog",
        Press = "press",
        PersonEmployer = "personEmployer",
        HouseholdEmployer = "householdEmployer",
        Worker = "worker",
        Employee = "employee",
        Entrepreneur = "entrepreneur",
        Association = "association",
        BusinessOwner = "businessOwner",
        ProductLongDescription = "productLongDescription",
        DocumentTemplates = "documentTemplates",
        InstructionsAndExamples = "instructionsAndExamples"
    }
    /** AbsenceCauseCode enumeration */
    export enum AbsenceCauseCode {
        UnpaidLeave = "unpaidLeave",
        PersonalReason = "personalReason",
        Illness = "illness",
        PartTimeSickLeave = "partTimeSickLeave",
        ParentalLeave = "parentalLeave",
        SpecialMaternityLeave = "specialMaternityLeave",
        Rehabilitation = "rehabilitation",
        ChildIllness = "childIllness",
        PartTimeChildCareLeave = "partTimeChildCareLeave",
        Training = "training",
        JobAlternationLeave = "jobAlternationLeave",
        StudyLeave = "studyLeave",
        IndustrialAction = "industrialAction",
        InterruptionInWorkProvision = "interruptionInWorkProvision",
        LeaveOfAbsence = "leaveOfAbsence",
        MilitaryRefresherTraining = "militaryRefresherTraining",
        MilitaryService = "militaryService",
        LayOff = "layOff",
        ChildCareLeave = "childCareLeave",
        MidWeekHoliday = "midWeekHoliday",
        AccruedHoliday = "accruedHoliday",
        OccupationalAccident = "occupationalAccident",
        AnnualLeave = "annualLeave",
        PartTimeAbsenceDueToRehabilitation = "partTimeAbsenceDueToRehabilitation",
        Other = "other"
    }
    /** AccountingReportRowType enumeration */
    export enum AccountingReportRowType {
        Debit = "debit",
        Credit = "credit",
        Total = "total",
        GroupHeader = "groupHeader",
        GroupTotal = "groupTotal",
        ChildRow = "childRow"
    }
    /** AccountingReportTableType enumeration */
    export enum AccountingReportTableType {
        Classic = "classic",
        Simple = "simple"
    }
    /** AgeGroupCode enumeration */
    export enum AgeGroupCode {
        A = "a",
        B = "b",
        C = "c",
        U = "u"
    }
    /** AgeRange enumeration */
    export enum AgeRange {
        Unknown = "unknown",
        Age_15 = "age_15",
        Age16 = "age16",
        Age17 = "age17",
        Age18_52 = "age18_52",
        Age53_62 = "age53_62",
        Age63_64 = "age63_64",
        Age65_67 = "age65_67",
        Age68AndOVer = "age68AndOVer"
    }
    /** AllowanceCode enumeration */
    export enum AllowanceCode {
        MealAllowance = "mealAllowance",
        PartialDailyAllowance = "partialDailyAllowance",
        FullDailyAllowance = "fullDailyAllowance",
        InternationalDailyAllowance = "internationalDailyAllowance"
    }
    /** AnnualLeavePaymentKind enumeration */
    export enum AnnualLeavePaymentKind {
        Undefined = "undefined",
        Planned = "planned",
        ManualSalary = "manualSalary",
        ManualCompensation = "manualCompensation",
        ManualBonus = "manualBonus",
        PaidCalc = "paidCalc",
        DraftCalc = "draftCalc"
    }
    /** ApiTestErrorType enumeration */
    export enum ApiTestErrorType {
        Default = "default",
        UserFriendly = "userFriendly"
    }
    /** ApiValidationErrorType enumeration */
    export enum ApiValidationErrorType {
        General = "general",
        Required = "required",
        Invalid = "invalid",
        Warning = "warning"
    }
    /** AuthenticationMethod enumeration */
    export enum AuthenticationMethod {
        Undefined = "undefined",
        EmailPwdLocal = "emailPwdLocal",
        Facebook = "facebook",
        Google = "google",
        Microsoft = "microsoft",
        LinkedIn = "linkedIn",
        Auth0Database = "auth0Database",
        X509 = "x509",
        Salaxy = "salaxy",
        Test = "test",
        InternalLocalhost = "internalLocalhost"
    }
    /** AuthorizationStatus enumeration */
    export enum AuthorizationStatus {
        Undefined = "undefined",
        OK = "oK",
        AccessDenied = "accessDenied",
        ExpiredOauthToken = "expiredOauthToken",
        InvalidOAuthToken = "invalidOAuthToken",
        NoOauthToken = "noOauthToken"
    }
    /** AuthorizationType enumeration */
    export enum AuthorizationType {
        None = "none",
        EmployerAuthorization = "employerAuthorization",
        WorkerContract = "workerContract",
        CompanyContract = "companyContract",
        Manual = "manual",
        Temporary = "temporary"
    }
    /** AvatarPictureType enumeration */
    export enum AvatarPictureType {
        Icon = "icon",
        Uploaded = "uploaded",
        Gravatar = "gravatar"
    }
    /** BankPaymentType enumeration */
    export enum BankPaymentType {
        Unknown = "unknown",
        Error = "error",
        PaytrailBatch = "paytrailBatch",
        NetvisorPayment = "netvisorPayment",
        SalaryGrossPayment = "salaryGrossPayment",
        RefundPayment = "refundPayment",
        PartialRefundPayment = "partialRefundPayment",
        InsuranceGrossPayment = "insuranceGrossPayment",
        NetSalary = "netSalary",
        SalaryAdvance = "salaryAdvance",
        TaxAccount = "taxAccount",
        Tvr = "tvr",
        TvrRfnd = "tvrRfnd",
        PalkkausFees = "palkkausFees",
        BankFees = "bankFees",
        PaytrailFees = "paytrailFees",
        InsuranceLahiTapiola = "insuranceLahiTapiola",
        Tyel = "tyel",
        TyelEteraContract = "tyelEteraContract",
        TyelEteraTemp = "tyelEteraTemp",
        TyelEteraUnknown = "tyelEteraUnknown",
        TyelEloContract = "tyelEloContract",
        TyelEloTemp = "tyelEloTemp",
        TyelEloInitialImport = "tyelEloInitialImport",
        TyelVarmaContract = "tyelVarmaContract",
        TyelVarmaTemp = "tyelVarmaTemp",
        TyelIlmarinenContract = "tyelIlmarinenContract",
        TyelIlmarinenTemp = "tyelIlmarinenTemp",
        Union = "union",
        UnionRaksa = "unionRaksa",
        SaldoYearBegin = "saldoYearBegin",
        SaldoYearEnd = "saldoYearEnd"
    }
    /** BenefitCode enumeration */
    export enum BenefitCode {
        AccommodationBenefit = "accommodationBenefit",
        TelephoneBenefit = "telephoneBenefit",
        MealBenefit = "mealBenefit",
        OtherBenefits = "otherBenefits"
    }
    /** BlobFileType enumeration */
    export enum BlobFileType {
        Unknown = "unknown",
        AuthorizationPdf = "authorizationPdf",
        Avatar = "avatar",
        TaxCard = "taxCard",
        ExpensesReceipt = "expensesReceipt",
        Raksaloki = "raksaloki",
        Template = "template",
        EInvoice = "eInvoice",
        MonthlyReport = "monthlyReport",
        YearlyReport = "yearlyReport",
        CalcReport = "calcReport"
    }
    /** BlobRepository enumeration */
    export enum BlobRepository {
        UserFiles = "userFiles",
        VersionHistory = "versionHistory",
        SystemFiles = "systemFiles",
        Payload = "payload",
        Reports = "reports",
        CdnImages = "cdnImages",
        GitContent = "gitContent",
        FileSystemContent = "fileSystemContent"
    }
    /** CalcGroup enumeration */
    export enum CalcGroup {
        Undefined = "undefined",
        BaseSalary = "baseSalary",
        SalaryAdditions = "salaryAdditions",
        Benefits = "benefits",
        Expenses = "expenses",
        Deductions = "deductions",
        OtherNoPayment = "otherNoPayment",
        Totals = "totals",
        Disabled = "disabled"
    }
    /** CalculationFlag enumeration */
    export enum CalculationFlag {
        Exclude = "exclude",
        PensionInsurance = "pensionInsurance",
        AccidentInsurance = "accidentInsurance",
        UnemploymentInsurance = "unemploymentInsurance",
        HealthInsurance = "healthInsurance",
        InsurancesDeduction = "insurancesDeduction",
        NoTax = "noTax",
        Tax = "tax",
        TaxDeduction = "taxDeduction",
        CfNoPayment = "cfNoPayment",
        CfPayment = "cfPayment",
        CfDeduction = "cfDeduction",
        CfDeductionAtSalaxy = "cfDeductionAtSalaxy"
    }
    /** CalculationResultRowType enumeration */
    export enum CalculationResultRowType {
        Undefined = "undefined",
        TotalBenefits = "totalBenefits",
        TotalExpenses = "totalExpenses",
        TotalSocialSecurityEmployer = "totalSocialSecurityEmployer",
        TotalSocialSecurityEmployerDebt = "totalSocialSecurityEmployerDebt",
        TotalSocialSecurityEmployerPayment = "totalSocialSecurityEmployerPayment",
        TotalPension = "totalPension",
        TotalPensionPayment = "totalPensionPayment",
        TotalPensionEmployer = "totalPensionEmployer",
        TotalPensionEmployerDebt = "totalPensionEmployerDebt",
        TotalPensionWorker = "totalPensionWorker",
        TotalUnemployment = "totalUnemployment",
        TotalUnemploymentPayment = "totalUnemploymentPayment",
        TotalUnemploymentEmployer = "totalUnemploymentEmployer",
        TotalUnemploymentEmployerDebt = "totalUnemploymentEmployerDebt",
        TotalUnemploymentWorker = "totalUnemploymentWorker",
        TotalPalkkaus = "totalPalkkaus",
        TotalSalary = "totalSalary",
        TotalTax = "totalTax",
        TotalTaxPayment = "totalTaxPayment",
        TotalPayment = "totalPayment",
        TotalWorkerPayment = "totalWorkerPayment"
    }
    /** CalculationRowSource enumeration */
    export enum CalculationRowSource {
        SalaryPage = "salaryPage",
        TesSelection = "tesSelection",
        ManualRow = "manualRow"
    }
    /** CalculationRowType enumeration */
    export enum CalculationRowType {
        Unknown = "unknown",
        Salary = "salary",
        HourlySalary = "hourlySalary",
        MonthlySalary = "monthlySalary",
        TotalWorkerPayment = "totalWorkerPayment",
        TotalEmployerPayment = "totalEmployerPayment",
        Compensation = "compensation",
        Overtime = "overtime",
        TesWorktimeShortening = "tesWorktimeShortening",
        EveningAddition = "eveningAddition",
        NightimeAddition = "nightimeAddition",
        SaturdayAddition = "saturdayAddition",
        SundayWork = "sundayWork",
        OtherAdditions = "otherAdditions",
        AccomodationBenefit = "accomodationBenefit",
        MealBenefit = "mealBenefit",
        PhoneBenefit = "phoneBenefit",
        CarBenefit = "carBenefit",
        OtherBenefit = "otherBenefit",
        HolidayCompensation = "holidayCompensation",
        HolidayBonus = "holidayBonus",
        HolidaySalary = "holidaySalary",
        DailyAllowance = "dailyAllowance",
        DailyAllowanceHalf = "dailyAllowanceHalf",
        MealCompensation = "mealCompensation",
        MilageOwnCar = "milageOwnCar",
        ToolCompensation = "toolCompensation",
        Expenses = "expenses",
        MilageDaily = "milageDaily",
        MilageOther = "milageOther",
        UnionPayment = "unionPayment",
        Foreclosure = "foreclosure",
        Advance = "advance",
        ForeclosureByPalkkaus = "foreclosureByPalkkaus",
        PrepaidExpenses = "prepaidExpenses",
        OtherDeductions = "otherDeductions",
        ChildCareSubsidy = "childCareSubsidy",
        ChainsawReduction = "chainsawReduction",
        NonProfitOrg = "nonProfitOrg",
        SubsidisedCommute = "subsidisedCommute",
        Totals = "totals"
    }
    /** CalculationRowUnit enumeration */
    export enum CalculationRowUnit {
        Undefined = "undefined",
        Count = "count",
        Percent = "percent",
        Days = "days",
        Kilometers = "kilometers",
        Hours = "hours"
    }
    /** CalculationStatus enumeration */
    export enum CalculationStatus {
        Draft = "draft",
        PaymentStarted = "paymentStarted",
        PaymentSucceeded = "paymentSucceeded",
        PaymentCanceled = "paymentCanceled",
        PaymentError = "paymentError",
        PaymentWorkerCopy = "paymentWorkerCopy",
        WorkerRequested = "workerRequested",
        WorkerRequestAccepted = "workerRequestAccepted",
        WorkerRequestDeclined = "workerRequestDeclined",
        PaymentRefunded = "paymentRefunded",
        ExternalPaymentStarted = "externalPaymentStarted",
        ExternalPaymentSucceeded = "externalPaymentSucceeded",
        ExternalPaymentCanceled = "externalPaymentCanceled",
        ExternalPaymentError = "externalPaymentError",
        Template = "template"
    }
    /** CarBenefitCode enumeration */
    export enum CarBenefitCode {
        LimitedCarBenefit = "limitedCarBenefit",
        FullCarBenefit = "fullCarBenefit"
    }
    /** CompanyType enumeration */
    export enum CompanyType {
        Unknown = "unknown",
        FiOy = "fiOy",
        FiTm = "fiTm",
        FiRy = "fiRy",
        FiYy = "fiYy"
    }
    /** ContractPartyType enumeration */
    export enum ContractPartyType {
        Person = "person",
        CustomContact = "customContact"
    }
    /** EmploymentRelationType enumeration */
    export enum EmploymentRelationType {
        Undefined = "undefined",
        Salary = "salary",
        HourlySalary = "hourlySalary",
        MonthlySalary = "monthlySalary",
        Compensation = "compensation",
        BoardMember = "boardMember",
        Entrepreneur = "entrepreneur",
        Farmer = "farmer",
        EmployedByStateEmploymentFund = "employedByStateEmploymentFund",
        KeyEmployee = "keyEmployee",
        Athlete = "athlete",
        PerformingArtist = "performingArtist"
    }
    /** EmploymentSalaryType enumeration */
    export enum EmploymentSalaryType {
        Unknown = "unknown",
        HourlyPay = "hourlyPay",
        MontlyPay = "montlyPay",
        OtherPay = "otherPay"
    }
    /** EmploymentType enumeration */
    export enum EmploymentType {
        Unknown = "unknown",
        Job = "job",
        PartTime = "partTime",
        FullTime = "fullTime"
    }
    /** EmploymentWorkHoursType enumeration */
    export enum EmploymentWorkHoursType {
        HoursPerDay = "hoursPerDay",
        HoursPerWeek = "hoursPerWeek",
        HourPerTwoWeeks = "hourPerTwoWeeks",
        HourPerThreeWeeks = "hourPerThreeWeeks"
    }
    /** ESalaryPaymentStatus enumeration */
    export enum ESalaryPaymentStatus {
        Created = "created",
        Shared = "shared",
        Opened = "opened",
        EmployerLinked = "employerLinked",
        CalculationCreated = "calculationCreated",
        PaymentStarted = "paymentStarted",
        PaymentSucceeded = "paymentSucceeded",
        PaymentCanceled = "paymentCanceled"
    }
    /** DateOfBirthAccuracy enumeration */
    export enum DateOfBirthAccuracy {
        Assumption = "assumption",
        AgeBased = "ageBased",
        AgeGroupBased = "ageGroupBased",
        MonthCorrect = "monthCorrect",
        Exact = "exact",
        Verified = "verified"
    }
    /** EmployerType enumeration */
    export enum EmployerType {
        Unknown = "unknown",
        MiniEmployer = "miniEmployer",
        SmallEmployer = "smallEmployer",
        FullTimeEmployer = "fullTimeEmployer",
        TyelOwnContractEmployer = "tyelOwnContractEmployer"
    }
    /** FrameworkAgreement enumeration */
    export enum FrameworkAgreement {
        NotDefined = "notDefined",
        Construction = "construction",
        Mll = "mll",
        ChildCare = "childCare",
        Cleaning = "cleaning",
        SantaClaus = "santaClaus",
        Entrepreneur = "entrepreneur",
        Other = "other"
    }
    /** Gender enumeration */
    export enum Gender {
        Unknown = "unknown",
        Male = "male",
        Female = "female"
    }
    /** HelttiIndustry enumeration */
    export enum HelttiIndustry {
        NotDefined = "notDefined",
        I = "i",
        J = "j",
        K = "k",
        M = "m",
        O = "o",
        N = "n",
        P = "p",
        Q = "q",
        R = "r",
        S = "s",
        Other = "other"
    }
    /** HelttiProductPackage enumeration */
    export enum HelttiProductPackage {
        NotDefined = "notDefined",
        Small = "small",
        Medium = "medium",
        Large = "large"
    }
    /** IncomeEarnerType enumeration */
    export enum IncomeEarnerType {
        EmployedByStateEmploymentFund = "employedByStateEmploymentFund",
        JointOwnerWithPayer = "jointOwnerWithPayer",
        PartialOwner = "partialOwner",
        KeyEmployee = "keyEmployee",
        LeasedEmployeeLivingAbroad = "leasedEmployeeLivingAbroad",
        PersonWorkingInFrontierDistrict = "personWorkingInFrontierDistrict",
        PersonWorkingAbroad = "personWorkingAbroad",
        Athlete = "athlete",
        PerformingArtist = "performingArtist",
        RestrictedPeriodInFinland = "restrictedPeriodInFinland",
        NetOfTaxContract = "netOfTaxContract",
        Organization = "organization",
        PersonWorkingOnAlandFerry = "personWorkingOnAlandFerry"
    }
    /** Holiday enumeration */
    export enum Holiday {
        NewYearsDay = "newYearsDay",
        Epiphany = "epiphany",
        GoodFriday = "goodFriday",
        EasterSunday = "easterSunday",
        EasterSaturday = "easterSaturday",
        EasterMonday = "easterMonday",
        MayDay = "mayDay",
        AscensionDay = "ascensionDay",
        Pentecost = "pentecost",
        MidsummerEve = "midsummerEve",
        MidsummerDay = "midsummerDay",
        AllSaintsDay = "allSaintsDay",
        IndependenceDay = "independenceDay",
        ChristmasEve = "christmasEve",
        ChristmasDay = "christmasDay",
        StStephensDay = "stStephensDay"
    }
    /** HolidayAccrualSource enumeration */
    export enum HolidayAccrualSource {
        Initial = "initial",
        Manual = "manual",
        CalcDraft = "calcDraft",
        CalcPaid = "calcPaid"
    }
    /** HolidayBonusPaymentMethod enumeration */
    export enum HolidayBonusPaymentMethod {
        None = "none",
        PayForHolidaySalary = "payForHolidaySalary",
        PaySummerBonus = "paySummerBonus",
        Pay24Days = "pay24Days",
        PayAllBonus = "payAllBonus"
    }
    /** HolidayCode enumeration */
    export enum HolidayCode {
        Undefined = "undefined",
        Permanent14Days = "permanent14Days",
        Permanent35Hours = "permanent35Hours",
        TemporaryTimeOff = "temporaryTimeOff",
        HolidayCompensation = "holidayCompensation",
        HolidayCompensationIncluded = "holidayCompensationIncluded",
        NoHolidays = "noHolidays"
    }
    /** HolidayGroup enumeration */
    export enum HolidayGroup {
        Official = "official",
        NonBanking = "nonBanking",
        Holiday = "holiday"
    }
    /** InsuranceCompany enumeration */
    export enum InsuranceCompany {
        None = "none",
        LähiTapiola = "l\u00E4hiTapiola",
        Pohjola = "pohjola",
        If = "if",
        Fennia = "fennia",
        AVakuutus = "aVakuutus",
        Aktia = "aktia",
        Pohjantähti = "pohjant\u00E4hti",
        Tryg = "tryg",
        Ålands = "\u00E5lands",
        Turva = "turva",
        Redarnas = "redarnas",
        Folksam = "folksam",
        Alandia = "alandia",
        Other = "other",
        Pending = "pending"
    }
    /** IrFlags enumeration */
    export enum IrFlags {
        NoMoney = "noMoney",
        OneOff = "oneOff",
        UnjustEnrichment = "unjustEnrichment"
    }
    /** IrInsuranceExceptions enumeration */
    export enum IrInsuranceExceptions {
        IncludeAll = "includeAll",
        IncludePension = "includePension",
        IncludeHealthInsurance = "includeHealthInsurance",
        IncludeUnemployment = "includeUnemployment",
        IncludeAccidentInsurance = "includeAccidentInsurance",
        ExcludeAll = "excludeAll",
        ExcludePension = "excludePension",
        ExcludeHealthInsurance = "excludeHealthInsurance",
        ExcludeUnemployment = "excludeUnemployment",
        ExcludeAccidentInsurance = "excludeAccidentInsurance"
    }
    /** IrRowSourceType enumeration */
    export enum IrRowSourceType {
        Manual = "manual",
        Usecase = "usecase",
        UsecaseV02 = "usecaseV02"
    }
    /** LegalEntityType enumeration */
    export enum LegalEntityType {
        Undefined = "undefined",
        Person = "person",
        Company = "company",
        PersonCreatedByEmployer = "personCreatedByEmployer",
        Partner = "partner"
    }
    /** LegacyTaxcardType enumeration */
    export enum LegacyTaxcardType {
        Example = "example",
        NoTaxCard = "noTaxCard",
        SideIncome = "sideIncome",
        NoTaxPrepayment = "noTaxPrepayment",
        TaxCardA = "taxCardA",
        TaxCardB = "taxCardB",
        Freelancer = "freelancer",
        Steps = "steps",
        Others = "others"
    }
    /** MessageType enumeration */
    export enum MessageType {
        Email = "email",
        Sms = "sms"
    }
    /** OnboardingStatus enumeration */
    export enum OnboardingStatus {
        Created = "created",
        Open = "open",
        Done = "done",
        Expired = "expired"
    }
    /** PalkkausCampaignPricing enumeration */
    export enum PalkkausCampaignPricing {
        None = "none",
        CampaignFirstUse = "campaignFirstUse",
        CampaignAfterFirstUse = "campaignAfterFirstUse",
        CompanyFreeTrial = "companyFreeTrial"
    }
    /** PartnerSite enumeration */
    export enum PartnerSite {
        Unknown = "unknown",
        Palkkaus = "palkkaus",
        PalkkausIdentified = "palkkausIdentified",
        PalkkausEmployerFillIn = "palkkausEmployerFillIn",
        Example = "example",
        Dev = "dev",
        RefugeeJobs = "refugeeJobs",
        Talkkarit = "talkkarit",
        Raksa = "raksa",
        Palkkaapakolainen = "palkkaapakolainen",
        CareDotCom = "careDotCom",
        Duunitori = "duunitori",
        Mol = "mol"
    }
    /** PaymentMethod enumeration */
    export enum PaymentMethod {
        Undefined = "undefined",
        Sepa = "sepa",
        Siirto = "siirto"
    }
    /** PaymentStatus enumeration */
    export enum PaymentStatus {
        New = "new",
        SentToBank = "sentToBank",
        BankTechApproval = "bankTechApproval",
        BankDelivered = "bankDelivered",
        BankPartialError = "bankPartialError",
        BankError = "bankError",
        Paid = "paid",
        Unknown = "unknown",
        Cancelled = "cancelled"
    }
    /** PayrollCalculationHandling enumeration */
    export enum PayrollCalculationHandling {
        Default = "default",
        EntrepreneurPension = "entrepreneurPension"
    }
    /** PayrollStatus enumeration */
    export enum PayrollStatus {
        Draft = "draft",
        PaymentStarted = "paymentStarted",
        PaymentSucceeded = "paymentSucceeded",
        PaymentCanceled = "paymentCanceled",
        PaymentError = "paymentError",
        Template = "template"
    }
    /** PensionCalculation enumeration */
    export enum PensionCalculation {
        Undefined = "undefined",
        Employee = "employee",
        Entrepreneur = "entrepreneur",
        Farmer = "farmer",
        PartialOwner = "partialOwner",
        Athlete = "athlete",
        Compensation = "compensation",
        BoardRemuneration = "boardRemuneration"
    }
    /** PensionCompany enumeration */
    export enum PensionCompany {
        None = "none",
        Etera = "etera",
        Ilmarinen = "ilmarinen",
        Elo = "elo",
        PensionsAlandia = "pensionsAlandia",
        Varma = "varma",
        Veritas = "veritas",
        Apteekkien = "apteekkien",
        Verso = "verso",
        Other = "other"
    }
    /** PeriodType enumeration */
    export enum PeriodType {
        Month = "month",
        Quarter = "quarter",
        Year = "year",
        Custom = "custom"
    }
    /** PricingModel enumeration */
    export enum PricingModel {
        PalkkausFee = "palkkausFee",
        NoFee = "noFee",
        FixedFee = "fixedFee"
    }
    /** ProductListFilter enumeration */
    export enum ProductListFilter {
        Available = "available",
        All = "all"
    }
    /** ReportCategory enumeration */
    export enum ReportCategory {
        All = "all",
        Monthly = "monthly",
        Yearly = "yearly"
    }
    /** ReportType enumeration */
    export enum ReportType {
        Undefined = "undefined",
        Example = "example",
        MonthlyDetails = "monthlyDetails",
        TaxMonthly4001 = "taxMonthly4001",
        MonthlyPension = "monthlyPension",
        MonthlyLiikekirjuri = "monthlyLiikekirjuri",
        MonthlyRapko = "monthlyRapko",
        YearlyDetails = "yearlyDetails",
        YearEndReport = "yearEndReport",
        YearlyWorkerSummary = "yearlyWorkerSummary",
        TaxYearly7801 = "taxYearly7801",
        Unemployment = "unemployment",
        Insurance = "insurance",
        HouseholdDeduction = "householdDeduction",
        TaxHouseholdDeduction14B = "taxHouseholdDeduction14B",
        TaxHouseholdDeduction14BSpouseA = "taxHouseholdDeduction14BSpouseA",
        TaxHouseholdDeduction14BSpouseB = "taxHouseholdDeduction14BSpouseB",
        SalarySlip = "salarySlip",
        SalarySlipPaid = "salarySlipPaid",
        EmployerReport = "employerReport",
        PaymentReport = "paymentReport",
        EarningsPaymentReport = "earningsPaymentReport",
        EmployersSeparateReport = "employersSeparateReport",
        EmploymentContract = "employmentContract"
    }
    /** Role enumeration */
    export enum Role {
        Undefined = "undefined",
        Accountant = "accountant",
        TrustedPartner = "trustedPartner",
        AccountantCandidate = "accountantCandidate"
    }
    /** SalaryKind enumeration */
    export enum SalaryKind {
        Undefined = "undefined",
        FixedSalary = "fixedSalary",
        HourlySalary = "hourlySalary",
        MonthlySalary = "monthlySalary",
        Compensation = "compensation",
        TotalWorkerPayment = "totalWorkerPayment",
        TotalEmployerPayment = "totalEmployerPayment"
    }
    /** SalaryType enumeration */
    export enum SalaryType {
        Tyel = "tyel",
        Yel = "yel",
        Compensation = "compensation"
    }
    /** SalaryPeriod enumeration */
    export enum SalaryPeriod {
        NotDefined = "notDefined",
        Hourly = "hourly",
        Monthly = "monthly",
        Fixed = "fixed"
    }
    /** SalaryPerMonthRange enumeration */
    export enum SalaryPerMonthRange {
        Unknown = "unknown",
        NoPensionRequired = "noPensionRequired",
        Normal = "normal"
    }
    /** SalaryPerYearRange enumeration */
    export enum SalaryPerYearRange {
        ZeroSalary = "zeroSalary",
        NoReportingNecessary = "noReportingNecessary",
        NoMonthlyReporting = "noMonthlyReporting",
        Normal = "normal"
    }
    /** ServiceModel enumeration */
    export enum ServiceModel {
        Shared = "shared",
        PartnerOnly = "partnerOnly"
    }
    /** SharingLink enumeration */
    export enum SharingLink {
        NoSharing = "noSharing",
        Unguessable = "unguessable",
        Short = "short"
    }
    /** TaxCardIncomeType enumeration */
    export enum TaxCardIncomeType {
        Unknown = "unknown",
        SalaxyCalculation = "salaxyCalculation",
        PreviousEmployerSalaries = "previousEmployerSalaries",
        ExternalSalaries = "externalSalaries"
    }
    /** TaxcardState enumeration */
    export enum TaxcardState {
        New = "new",
        Waiting = "waiting",
        Approved = "approved",
        Rejected = "rejected",
        EmployerAdded = "employerAdded",
        VerifiedVero = "verifiedVero",
        Replaced = "replaced",
        Expired = "expired"
    }
    /** TaxcardKind enumeration */
    export enum TaxcardKind {
        Undefined = "undefined",
        NoTaxCard = "noTaxCard",
        DefaultYearly = "defaultYearly",
        Replacement = "replacement",
        Others = "others",
        Historical = "historical"
    }
    /** TaxDeductionWorkCategories enumeration */
    export enum TaxDeductionWorkCategories {
        None = "none",
        Householdwork = "householdwork",
        Carework = "carework",
        HomeImprovement = "homeImprovement",
        OwnPropety = "ownPropety",
        RelativesProperty = "relativesProperty"
    }
    /** TaxReportHandling enumeration */
    export enum TaxReportHandling {
        Default = "default",
        NoZeroSalaryReport = "noZeroSalaryReport",
        NoMonthlyReport = "noMonthlyReport"
    }
    /** TesSubtype enumeration */
    export enum TesSubtype {
        NotSelected = "notSelected",
        ConstructionCarpenter = "constructionCarpenter",
        ConstructionFloor = "constructionFloor",
        ConstructionOther = "constructionOther",
        ConstructionFreeContract = "constructionFreeContract"
    }
    /** TestEnum enumeration */
    export enum TestEnum {
        ValueZero = "valueZero",
        ValueOne = "valueOne",
        ValueTwo = "valueTwo",
        ValueThree = "valueThree",
        ValueFour = "valueFour",
        ValueFive = "valueFive",
        ValueSix = "valueSix",
        ValueSeven = "valueSeven",
        ValueEight = "valueEight",
        ValueNine = "valueNine",
        ValueTen = "valueTen"
    }
    /** UnionPaymentType enumeration */
    export enum UnionPaymentType {
        NotSelected = "notSelected",
        RaksaNormal = "raksaNormal",
        RaksaUnemploymentOnly = "raksaUnemploymentOnly",
        Other = "other"
    }
    /** Unit enumeration */
    export enum Unit {
        Undefined = "undefined",
        Hours = "hours",
        Days = "days",
        Weeks = "weeks",
        Period = "period",
        One = "one",
        Count = "count",
        Percent = "percent",
        Kilometers = "kilometers",
        Euro = "euro"
    }
    /** VarmaPensionOrderAction enumeration */
    export enum VarmaPensionOrderAction {
        Undefined = "undefined",
        NewYel = "newYel",
        NewTyel = "newTyel",
        Move = "move"
    }
    /** VarmaPensionOrderYelPayer enumeration */
    export enum VarmaPensionOrderYelPayer {
        Undefined = "undefined",
        Entrepreneur = "entrepreneur",
        Company = "company"
    }
    /** WageBasis enumeration */
    export enum WageBasis {
        Undefined = "undefined",
        Monthly = "monthly",
        Hourly = "hourly",
        PerformanceBased = "performanceBased",
        Other = "other"
    }
    /** WebSiteUserRole enumeration */
    export enum WebSiteUserRole {
        None = "none",
        Household = "household",
        Worker = "worker",
        Company = "company"
    }
    /** YearEndAdminCheck enumeration */
    export enum YearEndAdminCheck {
        NotChecked = "notChecked",
        Ok = "ok",
        OkWithWarning = "okWithWarning",
        Errors = "errors"
    }
    /** YearEndFeedbackCalculationType enumeration */
    export enum YearEndFeedbackCalculationType {
        Unknown = "unknown",
        SalaxyPayment = "salaxyPayment",
        External = "external",
        Correction = "correction"
    }
    /** YearEndUserFeedback enumeration */
    export enum YearEndUserFeedback {
        NotChecked = "notChecked",
        Ok = "ok",
        OkWithModifications = "okWithModifications",
        WillHandleMyself = "willHandleMyself",
        NoYearlyReports = "noYearlyReports"
    }
    /** YtjCompanyType enumeration */
    export enum YtjCompanyType {
        NoCompanyType = "noCompanyType",
        HousingCooperative = "housingCooperative",
        Condominium = "condominium",
        AsoAssociation = "asoAssociation",
        OpenCompany = "openCompany",
        Association = "association",
        HypoAssociation = "hypoAssociation",
        Ky = "ky",
        Osuuskunta = "osuuskunta",
        CooperativeBank = "cooperativeBank",
        Oy = "oy",
        Foundation = "foundation",
        SavingsBank = "savingsBank",
        FinancialAssociation = "financialAssociation",
        StateEstablishment = "stateEstablishment",
        InsuranceAssociation = "insuranceAssociation",
        PrivateEntrepreneur = "privateEntrepreneur",
        OtherAssociation = "otherAssociation",
        SpecialPurposeAssociation = "specialPurposeAssociation",
        ForestCareAssociation = "forestCareAssociation",
        OtherFinancialAssociation = "otherFinancialAssociation",
        MutualInsuranceAssociation = "mutualInsuranceAssociation",
        MunicipalEstablishment = "municipalEstablishment",
        FederationOfMunicipalitiesEstablishment = "federationOfMunicipalitiesEstablishment",
        AlandFederation = "alandFederation",
        EuropeanCooperative = "europeanCooperative",
        EuropeanCooperativeBank = "europeanCooperativeBank",
        ReindeerHerdingCooperative = "reindeerHerdingCooperative",
        Unknown = "unknown"
    }
    /** CarBenefitKind enumeration */
    export enum CarBenefitKind {
        Undefined = "undefined",
        LimitedCarBenefit = "limitedCarBenefit",
        FullCarBenefit = "fullCarBenefit"
    }
    /** UnionPaymentKind enumeration */
    export enum UnionPaymentKind {
        Undefined = "undefined",
        RaksaNormal = "raksaNormal",
        RaksaUnemploymentOnly = "raksaUnemploymentOnly",
        Other = "other"
    }
    /** NonProfitOrgKind enumeration */
    export enum NonProfitOrgKind {
        Undefined = "undefined",
        KilometreAllowance = "kilometreAllowance",
        DailyAllowance = "dailyAllowance",
        AccomodationAllowance = "accomodationAllowance"
    }
    /** SubsidisedCommuteKind enumeration */
    export enum SubsidisedCommuteKind {
        Undefined = "undefined",
        NoDeduction = "noDeduction",
        SingleDeduction = "singleDeduction",
        PeriodicalDeduction = "periodicalDeduction"
    }
    /** MealBenefitKind enumeration */
    export enum MealBenefitKind {
        Undefined = "undefined",
        CateringContract = "cateringContract",
        MealTicket = "mealTicket",
        TaxableAmount = "taxableAmount",
        MealAllowance = "mealAllowance",
        Institute = "institute",
        Teacher = "teacher",
        RestaurantWorker = "restaurantWorker"
    }
    /** DailyAllowanceKind enumeration */
    export enum DailyAllowanceKind {
        Undefined = "undefined",
        FullDailyAllowance = "fullDailyAllowance",
        PartialDailyAllowance = "partialDailyAllowance",
        InternationalDailyAllowance = "internationalDailyAllowance",
        MealAllowance = "mealAllowance"
    }
    export enum Filter {
        Undefined = "undefined",
        DoNotShow = "doNotShow",
        Employment = "employment",
        Insurance = "insurance",
        Salary = "salary",
        Examples = "examples",
        PalkkausGeneral = "palkkausGeneral",
        PalkkausInstructions = "palkkausInstructions",
        Blog = "blog",
        Press = "press",
        PersonEmployer = "personEmployer",
        HouseholdEmployer = "householdEmployer",
        Worker = "worker",
        Employee = "employee",
        Entrepreneur = "entrepreneur",
        Association = "association",
        BusinessOwner = "businessOwner",
        ProductLongDescription = "productLongDescription",
        DocumentTemplates = "documentTemplates",
        InstructionsAndExamples = "instructionsAndExamples"
    }
    export enum Status {
        New = "new",
        SentToBank = "sentToBank",
        BankTechApproval = "bankTechApproval",
        BankDelivered = "bankDelivered",
        BankPartialError = "bankPartialError",
        BankError = "bankError",
        Paid = "paid",
        Unknown = "unknown",
        Cancelled = "cancelled"
    }
    /** Minimum status level for the calculations: draft-paymentStarted-paymentSucceeded. The default level is paymentSucceeded. */
    export enum MinStatus {
        Draft = "draft",
        PaymentStarted = "paymentStarted",
        PaymentSucceeded = "paymentSucceeded",
        PaymentCanceled = "paymentCanceled",
        PaymentError = "paymentError",
        PaymentWorkerCopy = "paymentWorkerCopy",
        WorkerRequested = "workerRequested",
        WorkerRequestAccepted = "workerRequestAccepted",
        WorkerRequestDeclined = "workerRequestDeclined",
        PaymentRefunded = "paymentRefunded",
        ExternalPaymentStarted = "externalPaymentStarted",
        ExternalPaymentSucceeded = "externalPaymentSucceeded",
        ExternalPaymentCanceled = "externalPaymentCanceled",
        ExternalPaymentError = "externalPaymentError",
        Template = "template"
    }
    export enum CalcWorkerPaymentDataTaxCardType {
        Example = "example",
        NoTaxCard = "noTaxCard",
        SideIncome = "sideIncome",
        NoTaxPrepayment = "noTaxPrepayment",
        TaxCardA = "taxCardA",
        TaxCardB = "taxCardB",
        Freelancer = "freelancer",
        Steps = "steps",
        Others = "others"
    }
    export interface Holidays {
        newYearsDay?: HolidayDate | null;
        epiphany?: HolidayDate | null;
        goodFriday?: HolidayDate | null;
        easterSunday?: HolidayDate | null;
        easterSaturday?: HolidayDate | null;
        easterMonday?: HolidayDate | null;
        mayDay?: HolidayDate | null;
        ascensionDay?: HolidayDate | null;
        pentecost?: HolidayDate | null;
        midsummerEve?: HolidayDate | null;
        midsummerDay?: HolidayDate | null;
        allSaintsDay?: HolidayDate | null;
        independenceDay?: HolidayDate | null;
        christmasEve?: HolidayDate | null;
        christmasDay?: HolidayDate | null;
        stStephensDay?: HolidayDate | null;
    }
    export interface Holidays2 {
        newYearsDay?: HolidayDate | null;
        epiphany?: HolidayDate | null;
        goodFriday?: HolidayDate | null;
        easterSunday?: HolidayDate | null;
        easterSaturday?: HolidayDate | null;
        easterMonday?: HolidayDate | null;
        mayDay?: HolidayDate | null;
        ascensionDay?: HolidayDate | null;
        pentecost?: HolidayDate | null;
        midsummerEve?: HolidayDate | null;
        midsummerDay?: HolidayDate | null;
        allSaintsDay?: HolidayDate | null;
        independenceDay?: HolidayDate | null;
        christmasEve?: HolidayDate | null;
        christmasDay?: HolidayDate | null;
        stStephensDay?: HolidayDate | null;
    }
    export enum VarmaPensionOrderYelPayer2 {
        Undefined = "undefined",
        Entrepreneur = "entrepreneur",
        Company = "company"
    }
    export enum AccountingReportRowCode {
        Unknown = "unknown",
        Totals = "totals",
        Salary = "salary",
        HourlySalary = "hourlySalary",
        MonthlySalary = "monthlySalary",
        TotalWorkerPayment = "totalWorkerPayment",
        TotalEmployerPayment = "totalEmployerPayment",
        Compensation = "compensation",
        Overtime = "overtime",
        TesWorktimeShortening = "tesWorktimeShortening",
        EveningAddition = "eveningAddition",
        NightimeAddition = "nightimeAddition",
        SaturdayAddition = "saturdayAddition",
        SundayWork = "sundayWork",
        OtherAdditions = "otherAdditions",
        AccomodationBenefit = "accomodationBenefit",
        MealBenefit = "mealBenefit",
        PhoneBenefit = "phoneBenefit",
        CarBenefit = "carBenefit",
        OtherBenefit = "otherBenefit",
        HolidayCompensation = "holidayCompensation",
        HolidayBonus = "holidayBonus",
        HolidaySalary = "holidaySalary",
        DailyAllowance = "dailyAllowance",
        DailyAllowanceHalf = "dailyAllowanceHalf",
        MealCompensation = "mealCompensation",
        MilageOwnCar = "milageOwnCar",
        ToolCompensation = "toolCompensation",
        Expenses = "expenses",
        MilageDaily = "milageDaily",
        MilageOther = "milageOther",
        UnionPayment = "unionPayment",
        Foreclosure = "foreclosure",
        Advance = "advance",
        ForeclosureByPalkkaus = "foreclosureByPalkkaus",
        PrepaidExpenses = "prepaidExpenses",
        OtherDeductions = "otherDeductions",
        ChildCareSubsidy = "childCareSubsidy",
        ChainsawReduction = "chainsawReduction",
        NonProfitOrg = "nonProfitOrg",
        SubsidisedCommute = "subsidisedCommute"
    }
    export enum AccountingReportRowResultCode {
        Undefined = "undefined",
        TotalBenefits = "totalBenefits",
        TotalExpenses = "totalExpenses",
        TotalSocialSecurityEmployer = "totalSocialSecurityEmployer",
        TotalSocialSecurityEmployerDebt = "totalSocialSecurityEmployerDebt",
        TotalSocialSecurityEmployerPayment = "totalSocialSecurityEmployerPayment",
        TotalPension = "totalPension",
        TotalPensionPayment = "totalPensionPayment",
        TotalPensionEmployer = "totalPensionEmployer",
        TotalPensionEmployerDebt = "totalPensionEmployerDebt",
        TotalPensionWorker = "totalPensionWorker",
        TotalUnemployment = "totalUnemployment",
        TotalUnemploymentPayment = "totalUnemploymentPayment",
        TotalUnemploymentEmployer = "totalUnemploymentEmployer",
        TotalUnemploymentEmployerDebt = "totalUnemploymentEmployerDebt",
        TotalUnemploymentWorker = "totalUnemploymentWorker",
        TotalPalkkaus = "totalPalkkaus",
        TotalSalary = "totalSalary",
        TotalTax = "totalTax",
        TotalTaxPayment = "totalTaxPayment",
        TotalPayment = "totalPayment",
        TotalWorkerPayment = "totalWorkerPayment"
    }
    export enum TestValuesEnumValue {
        ValueZero = "valueZero",
        ValueOne = "valueOne",
        ValueTwo = "valueTwo",
        ValueThree = "valueThree",
        ValueFour = "valueFour",
        ValueFive = "valueFive",
        ValueSix = "valueSix",
        ValueSeven = "valueSeven",
        ValueEight = "valueEight",
        ValueNine = "valueNine",
        ValueTen = "valueTen"
    }
    export interface FileParameter {
        data: any;
        fileName: string;
    }
}
declare module "model/nir" {
    export module nir {
        /** Object that stores National Incomes Register Earnings Payment Reports. */
        interface EarningsPayment {
            /** Delivery data from IR Wage Report. */
            deliveryData?: DeliveryData | null;
            /** Salaxy data that can't be stored in Earnings Payment Report */
            info?: EarningsPaymentExtensions | null;
            /** Validation for the Earnings Payment Report */
            validation?: ApiValidation | null;
            /** Status of the Earnings Payment Report. */
            status?: EarningsPaymentStatus | null;
            /** Identifier of the object. */
            id?: string | null;
            /** The date when the object was created. */
            createdAt?: string | null;
            /** The time when the object was last updated.
        Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
            updatedAt?: string | null;
            /** Owner ID for this data */
            owner?: string | null;
            /** Indication that for the currently logged-in account, the data is generally read-only. */
            isReadOnly?: boolean | null;
            /** Partner service that has be used in modifying the object. */
            partner?: string | null;
        }
        interface DeliveryData {
            timestamp?: string | null;
            source?: string | null;
            deliveryDataType?: DeliveryDataType | null;
            deliveryId?: string | null;
            faultyControl?: FaultyControl | null;
            productionEnvironment?: boolean | null;
            deliveryDataOwner?: Id | null;
            deliveryDataCreator?: Id | null;
            deliveryDataSender?: Id | null;
            paymentPeriod?: PaymentPeriod | null;
            contactPersons?: ContactPerson[] | null;
            payer?: Payer | null;
            reports?: Report[] | null;
        }
        /** Salaxy data that can't be stored in earnings payment report */
        interface EarningsPaymentExtensions {
            /** Worker tax card */
            workerTaxCard?: TaxCardSnapshot | null;
            /** Calculation used to generate this WageReport */
            calculationId?: string | null;
            /** Employer's Id */
            employerId?: string | null;
            /** Worker's Id */
            workerId?: string | null;
        }
        /** Common base class / interface for data validation. */
        interface ApiValidation {
            /** If true, the data is valid - no errors. */
            isValid?: boolean | null;
            /** If true, has required fields missing data. */
            hasAllRequiredFields?: boolean | null;
            /** Validation errors on invalid field values.
        Note that required fields missing error messages are not here. Only the invalid values. */
            errors?: ApiValidationError[] | null;
        }
        interface Id {
            type?: IdType | null;
            code?: string | null;
            countryCode?: string | null;
            countryName?: string | null;
        }
        interface PaymentPeriod {
            paymentDate?: string | null;
            startDate?: string | null;
            endDate?: string | null;
        }
        interface ContactPerson {
            name?: string | null;
            telephone?: string | null;
            email?: string | null;
            responsibilityCode?: ResponsibilityCode | null;
        }
        interface Payer {
            payerIds?: Id[] | null;
            payerBasic?: PayerBasic | null;
            address?: Address | null;
            subOrgs?: SubOrg[] | null;
            payerOther?: PayerOther | null;
            substitutePayer?: SubstitutePayer | null;
        }
        interface Report {
            reportData?: ReportData | null;
            incomeEarner?: IncomeEarner | null;
            transactions?: Transaction[] | null;
        }
        /** Basic information about the taxcard: This object has the infromation printed in SalarySlip etc. but not the full salaries paid to the taxcard etc. */
        interface TaxCardSnapshot {
            /** Year that the tax card is valid for. Typically this means from February of this year to January of the following. */
            forYear: number;
            /** Issue date off the tax card. */
            date?: string | null;
            /** Tax percent as percent. I.e. for 50% set 50, not 0.5. */
            taxPercent?: number | null;
            /** Income up to which the TaxPercent can be used.
        Any income above the limit is taxed with ExcessTaxPercent. */
            incomeLimit?: number | null;
            /** Tax percentage that is used in calculating the widthholding tax for the part of income above the IncomeLimit. */
            taxPercent2?: number | null;
            /** Type of tax card. */
            type?: TaxCardSnapshotType | null;
            /** Uri to the file copy of the tax card.
        Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
            fileUri?: string | null;
            /** Uri to the preview image of the tax card.
        Set by the system as part of the upload process, do not change in the API though it may be technically possible. This may have unintended results. */
            previewUri?: string | null;
        }
        /** Validation errors used in API output. */
        interface ApiValidationError {
            /** Full path to Name of the property / field (Member name in JSON).
        This may be null/empty for type General. */
            key?: string | null;
            /** High level type: Specifies the relation to object and its Member. */
            type: ApiValidationErrorType;
            /** Validation error message in the requested language. */
            msg: string;
            /** Some error providers may have a unique technical code for error or validator */
            code?: string | null;
        }
        interface PayerBasic {
            missingId?: boolean | null;
            companyName?: string | null;
            lastName?: string | null;
            firstName?: string | null;
            birthDate?: string | null;
            language?: Language | null;
        }
        interface Address {
            co?: string | null;
            street?: string | null;
            poBox?: string | null;
            postalCode?: string | null;
            postOffice?: string | null;
            countryCode?: string | null;
            countryName?: string | null;
        }
        interface SubOrg {
            type?: number | null;
            code?: string | null;
        }
        interface PayerOther {
            payerTypes?: PayerType[] | null;
        }
        interface SubstitutePayer {
            acts?: boolean | null;
            employerId?: Id | null;
            employerName?: string | null;
            wageSec?: boolean | null;
        }
        interface ReportData {
            actionCode?: ActionCode | null;
            irReportId?: string | null;
            reportId?: string | null;
            reportVersion?: number | null;
        }
        interface IncomeEarner {
            incomeEarnerIds?: Id[] | null;
            incomeEarnerBasic?: IncomeEarnerBasic | null;
            addresses?: TypedAddress[] | null;
            subOrgs?: SubOrg[] | null;
            employment?: Employment | null;
            professions?: Profession[] | null;
            employmentRegs?: EmploymentReg[] | null;
            placeOfBusiness?: PlaceOfBusiness | null;
            pensionInsurance?: PensionInsurance | null;
            accidentInsurance?: AccidentInsurance | null;
            insuranceExceptions?: ExceptionCode[] | null;
            incomeEarnerOther?: IncomeEarnerOther | null;
        }
        interface Transaction {
            transactionBasic?: TransactionBasic | null;
            insuranceData?: TransactionInclusion[] | null;
            earningPeriods?: EarningPeriod[] | null;
            unitWages?: UnitWage[] | null;
            carBenefit?: CarBenefit | null;
            mealBenefit?: MealBenefit | null;
            otherBenefit?: BenefitCode[] | null;
            recoveryData?: RecoveryData | null;
            dailyAllowance?: AllowanceCode[] | null;
            kmAllowance?: KmAllowance | null;
        }
        interface IncomeEarnerBasic {
            missingId?: boolean | null;
            companyName?: string | null;
            lastName?: string | null;
            firstName?: string | null;
            birthDate?: string | null;
            gender?: Gender | null;
        }
        interface TypedAddress {
            addressType?: AddressType | null;
            co?: string | null;
            street?: string | null;
            poBox?: string | null;
            postalCode?: string | null;
            postOffice?: string | null;
            countryCode?: string | null;
            countryName?: string | null;
        }
        interface Employment {
            employed?: boolean | null;
            employmentCode?: EmploymentCode | null;
            termCode?: TermCode | null;
            partTime?: number | null;
            hoursPerWeek?: number | null;
            paymentTypes?: PaymentType[] | null;
            employmentPeriods?: Period[] | null;
            employmentEndings?: EmploymentEnding[] | null;
        }
        interface Profession {
            type?: ProfessionType | null;
            code?: string | null;
            title?: string | null;
        }
        interface EmploymentReg {
            type?: number | null;
            code?: string | null;
        }
        interface PlaceOfBusiness {
            code?: string | null;
            street?: string | null;
            postalCode?: string | null;
            postOffice?: string | null;
            countryCode?: string | null;
            countryName?: string | null;
        }
        interface PensionInsurance {
            pensionActCode?: PensionActCode | null;
            pensionProvIdCode?: PensionProvIdCode | null;
            pensionPolicyNo?: string | null;
        }
        interface AccidentInsurance {
            accInsProvId?: Id | null;
            accInsPolicyNo?: string | null;
        }
        interface IncomeEarnerOther {
            cbaCode?: number | null;
            incomeEarnerTypes?: number[] | null;
            payments?: Payment[] | null;
        }
        interface TransactionBasic {
            transactionCode?: number | null;
            amount?: number | null;
            noMoney?: boolean | null;
            oneOff?: boolean | null;
            unjustEnrichment?: boolean | null;
            recovery?: boolean | null;
        }
        interface TransactionInclusion {
            insuranceCode?: InsuranceCode | null;
            included?: boolean | null;
        }
        interface EarningPeriod {
            startDate?: string | null;
            endDate?: string | null;
        }
        interface UnitWage {
            unitPrice?: number | null;
            unitAmount?: number | null;
            unitCode?: WageUnitCode | null;
        }
        interface CarBenefit {
            carBenefitCode?: CarBenefitCode | null;
            ageGroupCode?: AgeGroupCode | null;
            kilometers?: number | null;
        }
        interface MealBenefit {
            taxValue?: boolean | null;
        }
        interface RecoveryData {
            recoveryDate?: string | null;
            withhold?: number | null;
            origPaymentPeriods?: OrigPaymentPeriod[] | null;
        }
        interface KmAllowance {
            kilometers?: number | null;
        }
        interface Period {
            startDate?: string | null;
            endDate?: string | null;
        }
        interface EmploymentEnding {
            type?: EmploymentEndingType | null;
            code?: string | null;
        }
        interface Payment {
            paymentType?: RefPaymentType | null;
            paymentRef?: string | null;
            paymentSpecifier?: string | null;
        }
        interface OrigPaymentPeriod {
            paymentDate?: string | null;
            startDate?: string | null;
            endDate?: string | null;
        }
        /** Vero.fi income types and control rules */
        interface IncomeTypeCode {
            code?: number | null;
            nameEn?: string | null;
            nameFi?: string | null;
            nameSv?: string | null;
            pensionInsurance?: boolean | null;
            accidentInsurance?: boolean | null;
            unemploymentInsurance?: boolean | null;
            healthInsurance?: boolean | null;
            insuranceInformationAllowed?: boolean | null;
            transactionCode?: TransactionCode | null;
        }
        /** ActionCode enumeration */
        enum ActionCode {
            NewReport = "newReport",
            ReplacementReport = "replacementReport"
        }
        /** AddressType enumeration */
        enum AddressType {
            AddressInHomeCountry = "addressInHomeCountry",
            AddressInTheCountryOfWork = "addressInTheCountryOfWork"
        }
        /** AgeGroupCode enumeration */
        enum AgeGroupCode {
            A = "a",
            B = "b",
            C = "c",
            U = "u"
        }
        /** AllowanceCode enumeration */
        enum AllowanceCode {
            MealAllowance = "mealAllowance",
            PartialDailyAllowance = "partialDailyAllowance",
            FullDailyAllowance = "fullDailyAllowance",
            InternationalDailyAllowance = "internationalDailyAllowance"
        }
        /** BenefitCode enumeration */
        enum BenefitCode {
            AccommodationBenefit = "accommodationBenefit",
            TelephoneBenefit = "telephoneBenefit",
            MealBenefit = "mealBenefit",
            OtherBenefits = "otherBenefits"
        }
        /** CarBenefitCode enumeration */
        enum CarBenefitCode {
            LimitedCarBenefit = "limitedCarBenefit",
            FullCarBenefit = "fullCarBenefit"
        }
        /** DeliveryDataType enumeration */
        enum DeliveryDataType {
            EarningsPaymentReports = "earningsPaymentReports"
        }
        /** EmploymentCode enumeration */
        enum EmploymentCode {
            FullTime = "fullTime",
            PartTime = "partTime",
            NotAvailable = "notAvailable"
        }
        /** EmploymentEndingCode enumeration */
        enum EmploymentEndingCode {
            EmployeesResignation = "employeesResignation",
            Other = "other",
            TerminationGroundsRelatedToEmployeesPerson = "terminationGroundsRelatedToEmployeesPerson",
            FinancialAndProductionRelatedGrounds = "financialAndProductionRelatedGrounds",
            JointAgreement = "jointAgreement",
            Retirement = "retirement",
            TerminationOfaFixedTermEmployment = "terminationOfaFixedTermEmployment"
        }
        /** EmploymentEndingType enumeration */
        enum EmploymentEndingType {
            KevaCodes = "kevaCodes",
            BankOfFinlandCodes = "bankOfFinlandCodes",
            IncomesRegisterCodes = "incomesRegisterCodes"
        }
        /** ExceptionCode enumeration */
        enum ExceptionCode {
            NoObligationToSocialInsurance = "noObligationToSocialInsurance",
            NoObligationToHealthInsurance = "noObligationToHealthInsurance",
            NoObligationToEarningsRelatedPensionInsurance = "noObligationToEarningsRelatedPensionInsurance",
            NoObligationToAccidentAndOccupationalDiseaseInsurance = "noObligationToAccidentAndOccupationalDiseaseInsurance",
            NoObligationToUnemploymentInsurance = "noObligationToUnemploymentInsurance",
            NotSubjectToSocialInsurance = "notSubjectToSocialInsurance",
            NotSubjectToEarningsRelatedPensionInsurance = "notSubjectToEarningsRelatedPensionInsurance",
            NotSubjectToAccidentAndOccupationalDiseaseInsurance = "notSubjectToAccidentAndOccupationalDiseaseInsurance",
            NotSubjectToUnemploymentInsurance = "notSubjectToUnemploymentInsurance",
            NotSubjectToHealthInsurance = "notSubjectToHealthInsurance",
            VoluntaryEarningsRelatedPensionInsurance = "voluntaryEarningsRelatedPensionInsurance"
        }
        /** FaultyControl enumeration */
        enum FaultyControl {
            OnlyInvalidItems = "onlyInvalidItems",
            EntireRecordIsRejected = "entireRecordIsRejected"
        }
        /** Gender enumeration */
        enum Gender {
            Female = "female",
            Male = "male"
        }
        /** IdType enumeration */
        enum IdType {
            BusinessId = "businessId",
            PersonalIdentificationNumber = "personalIdentificationNumber",
            Vat = "vat",
            Giin = "giin",
            TaxIdentificationNumber = "taxIdentificationNumber",
            FinnishTradeRegistrationNumber = "finnishTradeRegistrationNumber",
            ForeignBusinessRegistrationNumber = "foreignBusinessRegistrationNumber",
            ForeignPersonalIdentificationNumber = "foreignPersonalIdentificationNumber",
            Other = "other"
        }
        /** InsuranceCode enumeration */
        enum InsuranceCode {
            SubjectToSocialInsuranceContributions = "subjectToSocialInsuranceContributions",
            SubjectToEarningsRelatedPensionInsuranceContribution = "subjectToEarningsRelatedPensionInsuranceContribution",
            SubjectToHealthInsuranceContribution = "subjectToHealthInsuranceContribution",
            SubjectToUnemploymentInsuranceContribution = "subjectToUnemploymentInsuranceContribution",
            SubjectToAccidentInsuranceAndOccupationalDiseaseInsuranceContribution = "subjectToAccidentInsuranceAndOccupationalDiseaseInsuranceContribution"
        }
        /** Language enumeration */
        enum Language {
            Finnish = "finnish",
            Swedish = "swedish",
            English = "english"
        }
        /** PayerType enumeration */
        enum PayerType {
            PublicSector = "publicSector",
            Household = "household",
            TemporaryEmployer = "temporaryEmployer",
            ForeignEmployer = "foreignEmployer",
            State = "state",
            UnincorporatedStateEnterpriseOrGovernmentalInstitution = "unincorporatedStateEnterpriseOrGovernmentalInstitution",
            SpecialisedAgency = "specialisedAgency",
            ForeignGroupCompany = "foreignGroupCompany"
        }
        /** PaymentType enumeration */
        enum PaymentType {
            Monthly = "monthly",
            Hourly = "hourly",
            ContractPay = "contractPay"
        }
        /** PensionActCode enumeration */
        enum PensionActCode {
            EmployeesEarningsRelatedPensionInsurance = "employeesEarningsRelatedPensionInsurance",
            PensionInsuranceForFarmers = "pensionInsuranceForFarmers",
            PensionInsuranceForTheSelfEmployed = "pensionInsuranceForTheSelfEmployed"
        }
        /** PensionProvIdCode enumeration */
        enum PensionProvIdCode {
            None = "none",
            Alandia = "alandia",
            Ilmarinen = "ilmarinen",
            Elo = "elo",
            Varma = "varma",
            Veritas = "veritas",
            Etera = "etera",
            Apteekkien = "apteekkien",
            Verso = "verso"
        }
        /** ProfessionType enumeration */
        enum ProfessionType {
            StatisticsFinland = "statisticsFinland",
            Keva = "keva",
            BankOfFinland = "bankOfFinland",
            Trafi = "trafi"
        }
        /** RefPaymentType enumeration */
        enum RefPaymentType {
            EarningsRelatedPensionProvider = "earningsRelatedPensionProvider"
        }
        /** RemunerationCode enumeration */
        enum RemunerationCode {
            DailyAllowance = "dailyAllowance",
            AccommodationBenefit = "accommodationBenefit",
            CarBenefit = "carBenefit",
            OtherBenefit = "otherBenefit"
        }
        /** ResponsibilityCode enumeration */
        enum ResponsibilityCode {
            ContentIssues = "contentIssues",
            TechnicalIssues = "technicalIssues"
        }
        /** TermCode enumeration */
        enum TermCode {
            UntilFurtherNotice = "untilFurtherNotice",
            FixedTerm = "fixedTerm"
        }
        /** TransactionCode enumeration */
        enum TransactionCode {
            Unknown = "unknown",
            Ignored = "ignored",
            TimeRatePay = "timeRatePay",
            EveningWorkCompensation = "eveningWorkCompensation",
            EveningShiftAllowance = "eveningShiftAllowance",
            SaturdayPay = "saturdayPay",
            HolidayBonus = "holidayBonus",
            OtherCompensation = "otherCompensation",
            SundayWorkCompensation = "sundayWorkCompensation",
            CompensationForAccruedTimeOff = "compensationForAccruedTimeOff",
            ContractPay = "contractPay",
            AnnualHolidayCompensation = "annualHolidayCompensation",
            OverTimeCompensation = "overTimeCompensation",
            NightWorkAllowance = "nightWorkAllowance",
            NightShiftCompensation = "nightShiftCompensation",
            AccommodationBenefit = "accommodationBenefit",
            MealAllowance = "mealAllowance",
            CarBenefit = "carBenefit",
            KilometreAllowanceTaxExempt = "kilometreAllowanceTaxExempt",
            OtherTaxableBenefitForEmployees = "otherTaxableBenefitForEmployees",
            OtherFringeBenefit = "otherFringeBenefit",
            TelephoneBenefit = "telephoneBenefit",
            DailyAllowance = "dailyAllowance",
            MealBenefit = "mealBenefit",
            NonWageCompensationForWork = "nonWageCompensationForWork",
            TaxableReimburementOfExpenses = "taxableReimburementOfExpenses",
            PrivateDayCareAllowanceWages = "privateDayCareAllowanceWages",
            PrivateDayCareAllowanceNonWage = "privateDayCareAllowanceNonWage",
            WithholdingTax = "withholdingTax",
            WagesPaid = "wagesPaid",
            OtherItemDeductibleFromNetWage = "otherItemDeductibleFromNetWage",
            NetWage = "netWage",
            EmployeesPensionInsuranceContribution = "employeesPensionInsuranceContribution",
            EmployeesUnemploymentInsuranceContribution = "employeesUnemploymentInsuranceContribution",
            Distraint = "distraint",
            DeductionBeforeWithholding = "deductionBeforeWithholding",
            WagesPaidBySubstitutePayerIncludingSocialInsurance = "wagesPaidBySubstitutePayerIncludingSocialInsurance",
            WagesPaidBySubstitutePayerIncludingEarningsRelatedPensionInsurance = "wagesPaidBySubstitutePayerIncludingEarningsRelatedPensionInsurance",
            WagesPaidBySubstitutePayerIncludingUnemploymentInsurance = "wagesPaidBySubstitutePayerIncludingUnemploymentInsurance",
            WagesPaidBySubstitutePayerIncludingAccidentAndOccupationalDiseaseInsurance = "wagesPaidBySubstitutePayerIncludingAccidentAndOccupationalDiseaseInsurance",
            WagesPaidBySubstitutePayerIncludingHealthInsurance = "wagesPaidBySubstitutePayerIncludingHealthInsurance",
            KilometreAllowanceTaxable = "kilometreAllowanceTaxable",
            LectureFee = "lectureFee",
            KilometreAllowancePaidByNonProfitOrganisation = "kilometreAllowancePaidByNonProfitOrganisation"
        }
        /** WageUnitCode enumeration */
        enum WageUnitCode {
            Hour = "hour",
            Day = "day",
            Week = "week",
            Period = "period"
        }
        enum EarningsPaymentStatus {
            New = "new",
            Succeeded = "succeeded",
            Canceled = "canceled",
            Error = "error"
        }
        enum TaxCardSnapshotType {
            Example = "example",
            NoTaxCard = "noTaxCard",
            SideIncome = "sideIncome",
            NoTaxPrepayment = "noTaxPrepayment",
            TaxCardA = "taxCardA",
            TaxCardB = "taxCardB",
            Freelancer = "freelancer",
            Steps = "steps",
            Others = "others"
        }
        enum ApiValidationErrorType {
            General = "general",
            Required = "required",
            Invalid = "invalid"
        }
    }
}
declare module "model/index" {
    export * from "model/oauth2";
    export * from "model/v01";
    export * from "model/nir";
}
declare module "api/Ajax" {
    /**
     * Abstracts the interface for Http / Ajax functionality that is required by Salaxy core classes.
     * This interface is implemented in this project for jQuery and Node.
     * Additional implementations exists for Angular.js (salaxy-lib-ng1) and Angular.io / ngx (salaxy-lib-ng).
     * You may also create your own implementation for the framework or http component that you want to support.
     */
    export interface Ajax {
        /**
         * The server address - root of the server. This is settable field.
         * Will probably be changed to a configuration object in the final version.
         */
        serverAddress: string;
        /**
         * By default credentials are not used in http-calls.
         * Enable credentials with this flag (force disabled when the token is set).
         */
        useCredentials: boolean;
        /**
         * Sets Salaxy Bearer token for this http context.
         *
         * @param token - Salaxy JWT token.
         */
        setCurrentToken(token: string): any;
        /** Gets the current Salaxy Bearer token */
        getCurrentToken(): string;
        /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
        getServerAddress(): string;
        /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
        getApiAddress(): string;
        /**
         * Gets a JSON-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        getJSON(method: string): Promise<any>;
        /**
         * Gets a HTML-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
         */
        getHTML(method: string): Promise<string>;
        /**
         * POSTS data to server and receives back a JSON-message.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postJSON(method: string, data: any): Promise<any>;
        /**
         * POSTS data to server and receives back HTML.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postHTML(method: string, data: any): Promise<string>;
        /**
         * Sends a DELETE-message to server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        remove(method: string): Promise<any>;
    }
}
declare module "api/CrudApiBase" {
    import { Ajax } from "api/Ajax";
    /** Base class for CRUD services. */
    export abstract class CrudApiBase<T> {
        protected ajax: Ajax;
        /**
         * Deriving classes should define the baseURl for the method. E.g. "/calculations"
         * Ajax implementations will add the beginning (e.g. "https://test-api.salaxy.com/api/v02")
         */
        protected abstract baseUrl: string;
        /** Constructor creates a new CRUD api with given Ajax-implementation */
        constructor(ajax: Ajax);
        /**
         * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
         * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
         * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
         * Strings, dates and booleans may be null (false if bool), but typically just undefined.
         */
        abstract getBlank(): T;
        /**
         * Gets all the items of a given type.
         * @returns A Promise with result data array.
         */
        getAll(): Promise<T[]>;
        /**
         * Gets a single item based on identier
         * @param id - Unique identifier for the object
         * @returns A Promise with result data.
         */
        getSingle(id: string): Promise<T>;
        /**
         * Deletes an single item from the sotrage
         * @param id - Unique identifier for the object
         * @returns A Promise with result data "Object deleted".
         */
        delete(id: string): Promise<string>;
        /**
         * Saves an item to the storage.
         * If id is null, this is add/insert. If id exists, this is update.
         * @param itemToSave - The item that is updated/inserted.
         * @returns A Promise with result data as saved to the storage (contains id, createAt, owner etc.).
         */
        save(itemToSave: any): Promise<T>;
    }
}
declare module "api/Absences" {
    import { WorkerAbsences } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for Absences of a Worker
     */
    export class Absences extends CrudApiBase<WorkerAbsences> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(workerId?: string): WorkerAbsences;
        /**
         * Gets the Absences for a specific Worker.
         * @returns A Promise with result data array.
         */
        getForWorker(workerId: string): Promise<WorkerAbsences>;
        /**
         * Gets the Absences for the given ids.
         * @param ids Identifiers for the Absences objects.
         * @returns A Promise with result data array.
         */
        getMulti(ids: string[]): Promise<WorkerAbsences[]>;
    }
}
declare module "logic/import/ImportResult" {
    import { ApiValidationError } from "model/index";
    /** Result for importing data (CSV, Excel copy-paste etc.) */
    export interface ImportResult {
        /** Number of ignored lines (empty line or rem/annotated) in the import. */
        ignoredLines: number;
        /** Headers for the import: These are also the properties for the JSON */
        headers: string[];
        /** Reault data is an array of objects with properties specified as headers */
        data: any[];
        /** Errors as validation errors. */
        errors: ApiValidationError[];
    }
}
declare module "logic/import/ImportLogic" {
    import { ImportResult } from "logic/import/ImportResult";
    /** Methods for importing data into Salaxy system. */
    export class ImportLogic {
        /**
         * Converts a tab-separated values text to raw JSON.
         * Used to converting data copy-pasted from Excel to JSON.
         * @param inputText The TSV string that should be imported.
         * @returns ImportResult object with the data and other realted information.
         */
        static tsvToJson(inputText: string): ImportResult;
    }
}
declare module "logic/import/index" {
    export * from "logic/import/ImportLogic";
    export * from "logic/import/ImportResult";
}
declare module "logic/model/AppStatus" {
    /**
     * Supports the UserSession object by providing additional information about the status
     * of fetching the session, data, expiration etc.
     */
    export interface AppStatus {
        /** If true, the session check from server is in progress */
        sessionCheckInProgress: boolean;
        /**
         * If true, the Init-data from the server has been loaded.
         * For different environments, this may signal different things, but currently it is Calculations and Workers.
         * Used for showing loader animation in Application loading / Session reload.
         */
        dataLoaded: boolean;
        /**
         * PLACEHOLDER: Indicates that the session has expired (as opposed to not checked yet).
         * This property has not yet been implemented.
         */
        sessionExpired?: boolean;
    }
}
declare module "logic/model/CalculationRowCategory" {
    /**
     * Primary categorization of Calculation row types.
     */
    export enum CalculationRowCategory {
        Salary = "salary",
        SalaryAdditions = "salaryAdditions",
        Benefits = "benefits",
        Expenses = "expenses",
        Deductions = "deductions",
        Other = "other"
    }
}
declare module "logic/model/CalcRowConfig" {
    import { CalculationRowUnit } from "model/index";
    import { CalculationRowCategory } from "logic/model/CalculationRowCategory";
    /**
     * Configuration for user interface of a Calculation row.
     */
    export interface CalcRowConfig {
        /** Name / unique identifier of the row type. */
        name: string;
        /** Order number in the lists */
        order: number;
        /** Label for the row type */
        label: string;
        /** Description for the row type */
        descr: string;
        /** More info HTML */
        moreInfo: string;
        /** Categorization of the row types. */
        category: CalculationRowCategory;
        /**
         * Favorite row types and their sort order.
         * This is currently fixed, but may later be based on user profile (e.g. most used types).
         */
        favorite: number | null;
        /** Color is based on the category */
        color: string;
        /** Icon short text is 1-5 characters suitable for icon.
         * Has 5 characters only if very narrow, e.g. "1" and comma - typically 2-3 chars.
         */
        iconText: string;
        /** Price input */
        price: {
            /** Label for the input */
            label: string;
            /** Default value */
            default: number;
            /** Type of the input control to use */
            input: "number" | "hidden" | "readOnly";
        };
        /** Amount input */
        amount: {
            /** Label for the input */
            label: string;
            /** Default value */
            default: number;
            /** Type of the input control to use */
            input: "number" | "hidden" | "readOnly";
            /** Code for the unit. */
            unit: CalculationRowUnit;
        };
        /** The total input or read-only field. */
        total: {
            /** Type of the input control to use */
            input: "number" | "hidden" | "readOnly";
        };
    }
}
declare module "logic/model/CalculationRowCategories" {
    import { CalculationRowType } from "model/v01";
    /** Categorization / grouping for CalculationRowType */
    export class CalculationRowCategories {
        /** Base salary types: the ones that you define directly */
        static baseSalary: CalculationRowType[];
        /** Additions to base salary - typically a percentage of base salary */
        static salaryAdditions: CalculationRowType[];
        /** All salary types: baseSalary and salaryAdditions together */
        static salary: CalculationRowType[];
        /** Benefits: Not paid as money but taxable income */
        static benefits: CalculationRowType[];
        /** Expenses and other non-taxed compensations */
        static expenses: CalculationRowType[];
        /** Deductions */
        static deductions: CalculationRowType[];
        /** Holidays */
        static holidays: CalculationRowType[];
        /**
         * Items in the section called "expenses". Besides expenses, contains also benefits.
         */
        static expensesSection: CalculationRowType[];
    }
}
declare module "logic/model/IncomeTypeMetadata" {
    /** Describes the properties and behavior of the Income types in Incomes register. */
    export interface IncomeTypeMetadata {
        /** Code in incomes register. Language versioned. */
        code: number;
        /** Language versioned label for the income type. */
        label: string;
        /**
         * Description of the row.
         * Note that this is long text with line feeds / Markdown
         * Text is currently always in Finnish.
         */
        description: string;
        /**
         * Description of how the row is taken into widthholding tax
         * and how insurance payments (pension, unemployment, accident and health care) are made.
         * Note that this is long text with line feeds / Markdown
         * Text is currently always in Finnish.
         */
        taxAndSidecostsDescr: string;
        /** If true, teh row may have a negative value. */
        isNegativeSupported: boolean;
        /** If true Pension payments are made for this income type by default. */
        pensionInsurance: boolean;
        /** If true Accident insurance payments are made for this income type by default. */
        accidentInsurance: boolean;
        /** If true Unemployment insurance payments are made for this income type by default. */
        unemploymentInsurance: boolean;
        /** If true Health insurance payments are made for this income type by default. */
        healthInsurance: boolean;
        /** If true the default values for insurances may be overriden for a single row. */
        insuranceInformationAllowed: boolean;
        /** Describes the behavior of this row in calculating widthholding tax. */
        taxDefault: number;
        /** If false, the row type is not supported in this version */
        isSupported: boolean;
        /** When isSupported is false, there is a describtive text that tells why the type is not supported. Text is always in Finnish. */
        notSupportedError: any;
        /** Default behavior for payment through Salaxy Customer Funds account */
        paymentDefault: number;
        /** Grouping that is used in reporting etc. */
        calcGrouping: string;
    }
}
declare module "logic/model/Occupation" {
    /**
     * Occupation according to Statistics Finland.
     * Used for insurance pricing and reporting.
     */
    export interface Occupation {
        /** Salaxy identifier for the Occupation. Begins with code, but may have a suffix. */
        id?: string | null;
        /** Statistics Finland code. There may be duplicates with different id. */
        code?: string | null;
        /** Language for the entry */
        lang?: "fi" | "en" | "sv" | null;
        /** Label */
        label?: string | null;
        /** Lower case text that contains the id, label and potentially other text that should match in searches. */
        search?: string | null;
        /**
         * Free text
         * TODO: Do we need to expose this? Why?
         */
        freeText?: string | null;
        /** Keywords for searching etc. */
        keyword?: string | null;
    }
}
declare module "logic/model/Product" {
    /**
     * Base inteface for Products.
     * The common properties for all Products: BaseSalaryProduct, TaxProduct, UnemploymentProduct etc.
     */
    export interface Product {
        /** Identifier for the product */
        readonly id?: string | null;
        /** The main short title for the product / service */
        readonly title?: string | null;
        /** The status text that should give the user. Restrict the length to 56 characters. */
        readonly status?: string | null;
        /** One paragraph description text */
        readonly description?: string | null;
        /** The logo image for the product. */
        readonly img?: string | null;
        /** Identifier for the main product desription article in the Palkkaus.fi CMS */
        readonly articleId?: string | null;
        /** If true, the product is enabled for the current user */
        enabled?: boolean | null;
        /**
         * If false, it is not bossible to enable this product for the current account / environment.
         * Typically, this means that the UI should not show this product.
         * E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product.
         */
        visible?: boolean | null;
    }
}
declare module "logic/model/SystemRole" {
    /**
     * Enumerates the system roles:
     * The roles that are not returned from server as account roles but
     * rather as derived from properties of the current session.
     */
    export enum SystemRole {
        /** Session check is in progress: The role of the user is unknown. */
        Unknown = "unknown",
        /** User is anonymous: The session has been checked and there is no token, token has expired or the token is invalid. */
        Anon = "anon",
        /** User is authenticated either as Company or Person */
        Auth = "auth",
        /**
         * Initialization is in progress:
         * Init includes Session check (SystemRole Unknown) as well as
         * loading the most important data: Currently list of Workers.
         */
        Init = "init",
        /** User is in Worker role.  */
        Worker = "worker",
        /** User is in Employer role: Either Household or Company. */
        Employer = "employer",
        /** User is Household (Employer) */
        Household = "household",
        /** User is Company (Employer) */
        Company = "company",
        /** User is a Person: Employer and/or Worker (not Company) */
        Person = "person",
        /** Hidden always returns false. Used to hide items in navigation. */
        Hidden = "hidden",
        /** System is running with test data */
        Test = "test",
        /** System is runnin with production data */
        Prod = "prod",
        /** Finnish personal company ("Yksityinen elinkeinonharjoittaja"). */
        FiTm = "fiTm",
        /** Finnish Limited liability company ("Osakeyhtiö"). */
        FiOy = "fiOy",
        /** Finnish Association ("yhdistys") */
        FiRy = "fiRy",
        /**
         * Any user ("any" or "*") always returns true.
         * Note that in most scenarios also no roles ([], "" or null) are equivalent to Any.
         */
        Any = "any"
    }
}
declare module "logic/model/index" {
    export * from "logic/model/AppStatus";
    export * from "logic/model/CalcRowConfig";
    export * from "logic/model/CalculationRowCategories";
    export * from "logic/model/CalculationRowCategory";
    export * from "logic/model/IncomeTypeMetadata";
    export * from "logic/model/Occupation";
    export * from "logic/model/Product";
    export * from "logic/model/SystemRole";
}
declare module "logic/usecases/Usecase" {
    /** Use case is a user interfaces for defining a framework */
    export interface Usecase {
        /** Unique identifier for the item in this user interface tree */
        id: string;
        /**
         * Key for the usecase based on which it is resolved.
         * Later, this will resolve to a microservice.
         */
        uri?: string;
        /** Label in the list / tree selection */
        label: string;
        /** Description of the use case (mainly for the lists) */
        descr?: string;
        /** Path to the main icon (typically an SVG ), if null the default may be set by the group */
        icon?: string;
        /** Path to a sub icon (typically a PNG), if null the default may be set by the group  */
        badge?: string;
        /** If true, the selection is deductible in household taxation */
        isHouseholdDeductible?: boolean;
        /** Occupation code for insurance purposes. */
        occupation?: string;
        /** Salaxy Framework subtype */
        subType?: string;
    }
}
declare module "logic/usecases/UsecaseGroup" {
    import { Usecase } from "logic/usecases/Usecase";
    /** Group of usecases (user interfaces for defining a framework) */
    export interface UsecaseGroup {
        /** Identifier for the group */
        id: string;
        /**
         * Key for the usecase based on which it is resolved.
         * Later, this will resolve to a microservice.
         */
        uri?: string;
        /** Label in the list / tree selection */
        label: string;
        /** Description of the use case group (mainly for the lists) */
        descr: string;
        /** Path to the main icon (typically an SVG ) */
        icon: string;
        /** Path to a sub icon (typically a PNG)  */
        badge?: string;
        /** Collection of use cases - in the future, may also support sub groups. */
        children?: Usecase[];
    }
}
declare module "codegen/accountProductDefaults" {
    /** Codegen namespace is interfaces and data that is generated in Salaxy server */
    /**
     * Generated products data from the server.
     * To update, go to: https://test-api.salaxy.com/v02/api/accounts/products/anon
     * @ignore
     * TODO: Add language versioning to the server side and generation.
     * TODO: Can we package this entire namespace in a way that it is hidden to be internal only?
     */
    export const accountProductDefaults: any;
}
declare module "codegen/enumerations" {
    /**
     * This data is generated from
     * ****************************************************
     * DEVELOPERS NOTE: COPY from https://test-api.salaxy.com/docs/EnumJson / http://localhost:82/docs/EnumJson
     * DO NOT EDIT DIRECTLY
     * @ignore
     */
    export const generatedEnumData: {
        "name": string;
        "description": any;
        "values": {
            "order": number;
            "name": string;
        }[];
    }[];
}
declare module "codegen/householdRows" {
    /** Codegen namespace is interfaces and data that is generated in Salaxy server */
    /**
     * Generated in an an excel file
     * @ignore
     * TODO: Can we package this entire namespace in a way that it is hidden to be internal only?
     */
    export const housholdRows: ({
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault: number;
        amountInput: string;
        totalInput: string;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault: number;
        unit: string;
        amountInput?: undefined;
        totalInput?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        amountDefault: number;
        unit: string;
        amountInput?: undefined;
        totalInput?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        unit: string;
        Internal: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        favorites?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        unit?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault: number;
        amountInput?: undefined;
        totalInput?: undefined;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault: number;
        priceDefault: number;
        amountInput?: undefined;
        totalInput?: undefined;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountDefault: number;
        amountInput: string;
        totalInput?: undefined;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        unit: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        priceDefault: number;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        unit?: undefined;
        Internal?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        priceDefault: number;
        unit: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        priceDefault: number;
        unit: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        Internal?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        unit: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        priceInput: string;
        unit: string;
        amountDefault?: undefined;
        amountInput?: undefined;
        totalInput?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        amountInput: string;
        totalInput: string;
        amountDefault?: undefined;
        unit?: undefined;
        favorites?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    } | {
        order: number;
        id: string;
        category: string;
        color: string;
        favorites: number;
        amountInput: string;
        totalInput: string;
        amountDefault?: undefined;
        unit?: undefined;
        Internal?: undefined;
        priceDefault?: undefined;
        priceInput?: undefined;
    })[];
}
declare module "codegen/householdUseCaseTree" {
    /**
     * Generated products data from the server.
     * @ignore
     * TODO: Add language versioning to an Excel file and implement code generation.
     */
    export const householdUsecaseTree: ({
        id: string;
        label: string;
        descr: string;
        icon: string;
        children: ({
            id: string;
            uri: string;
            label: string;
            descr: string;
            isHouseholdDeductible: boolean;
            icon: string;
            badge: string;
            occupation: string;
            isChildcareSubsidy?: undefined;
        } | {
            id: string;
            uri: string;
            label: string;
            descr: string;
            isChildcareSubsidy: boolean;
            icon: string;
            badge: string;
            occupation: string;
            isHouseholdDeductible?: undefined;
        } | {
            id: string;
            uri: string;
            label: string;
            descr: string;
            isHouseholdDeductible: boolean;
            icon: string;
            occupation: string;
            badge?: undefined;
            isChildcareSubsidy?: undefined;
        })[];
        uri?: undefined;
    } | {
        id: string;
        label: string;
        descr: string;
        uri: string;
        icon: string;
        children: ({
            id: string;
            label: string;
            descr: string;
            icon: string;
            subType: string;
            occupation: string;
            isHouseholdDeductible?: undefined;
        } | {
            id: string;
            label: string;
            descr: string;
            icon: string;
            subType: string;
            isHouseholdDeductible: boolean;
            occupation: string;
        })[];
    } | {
        id: string;
        label: string;
        descr: string;
        uri: string;
        icon: string;
        children: ({
            id: string;
            label: string;
            descr: string;
            isHouseholdDeductible: boolean;
            occupation: string;
            isContractLessThanYear: boolean;
            icon?: undefined;
        } | {
            id: string;
            label: string;
            descr: string;
            icon: string;
            isHouseholdDeductible: boolean;
            occupation: string;
            isContractLessThanYear: boolean;
        })[];
    } | {
        id: string;
        label: string;
        descr: string;
        uri: string;
        icon: string;
        children: ({
            id: string;
            label: string;
            isHouseholdDeductible: boolean;
            occupation: string;
            icon?: undefined;
        } | {
            id: string;
            label: string;
            occupation: string;
            isHouseholdDeductible?: undefined;
            icon?: undefined;
        } | {
            id: string;
            label: string;
            icon: string;
            occupation: string;
            isHouseholdDeductible?: undefined;
        } | {
            id: string;
            label: string;
            isHouseholdDeductible: boolean;
            icon: string;
            occupation: string;
        })[];
    })[];
}
declare module "codegen/vismaSignOriginalMethods" {
    /**
     * Authentication methods for VismaSign.
     * This is the raw list of methods as returned by https://www.onnistuu.fi/api/v1/auth/methods
     * We will override most of these images as the ones below are pretty outdated.
     * @ignore
     */
    export const vismaSignOriginalMethods: {
        "identifier": string;
        "name": string;
        "image": string;
    }[];
}
declare module "codegen/incomeTypeCodes" {
    /**
     * Income types code-generated from the server.
     *
     * @generator https://test-api.salaxy.com/nir/incomeTypeCodes
     * Fetch this data from https://test-api.salaxy.com/nir/incomeTypeCodes
     * There is no automatic fetching because the data updated quite seldomly
     */
    export const incomeTypeCodes: ({
        "code": number;
        "nameEn": string;
        "nameFi": string;
        "nameSv": string;
        "isNegativeSupported": boolean;
        "pensionInsurance": boolean;
        "accidentInsurance": boolean;
        "unemploymentInsurance": boolean;
        "healthInsurance": boolean;
        "insuranceInformationAllowed": boolean;
        "taxDefault": number;
        "transactionCode": any;
        "notSupportedError": string;
        "description": string;
        "taxAndSidecostsDescr": string;
        "isSupported": boolean;
        "paymentDefault": number;
        "usecases": any[];
        "calcGrouping": string;
    } | {
        "code": number;
        "nameEn": string;
        "nameFi": string;
        "nameSv": string;
        "isNegativeSupported": boolean;
        "pensionInsurance": boolean;
        "accidentInsurance": boolean;
        "unemploymentInsurance": boolean;
        "healthInsurance": boolean;
        "insuranceInformationAllowed": boolean;
        "taxDefault": number;
        "transactionCode": string;
        "notSupportedError": any;
        "description": string;
        "taxAndSidecostsDescr": string;
        "isSupported": boolean;
        "paymentDefault": number;
        "usecases": string[];
        "calcGrouping": string;
    })[];
}
declare module "codegen/yearlyChangingNumbersYears" {
    import { YearlyChangingNumbers } from "model/index";
    /**
     * Yearly changing numbers. Contains side cost percentages.
     *
     */
    export const yearlyChangingNumbersYears: YearlyChangingNumbers[];
}
declare module "codegen/yearlyHolidaysYears" {
    import { YearlyHolidays } from "model/index";
    /**
     * Yearly holidays.
     */
    export const yearlyHolidaysYears: YearlyHolidays[];
}
declare module "codegen/index" {
    export * from "codegen/accountProductDefaults";
    export * from "codegen/enumerations";
    export * from "codegen/householdRows";
    export * from "codegen/householdUseCaseTree";
    export * from "codegen/vismaSignOriginalMethods";
    export * from "codegen/incomeTypeCodes";
    export * from "codegen/yearlyChangingNumbersYears";
    export * from "codegen/yearlyHolidaysYears";
}
declare module "util/Arrays" {
    /**
     * Helpers for handling arrays.
     */
    export class Arrays {
        /**
         * Takes in an array or string or null and always returns an array.
         * If the incoming value is a string, splits it by commas. This is especaially useful for HTML attributes.
         * Note that null (or other falsy objects) are returned as an empty array.
         *
         * @param arrayCandidate - A string array or string that is split by comma.
         */
        static assureArray(arrayCandidate: string | string[]): string[];
        /**
         * Calculates a sum of array items.
         * Null items will be added as zero.
         * @param array Array to sum
         * @param selector Selector for a number to sum.
         */
        static sum<T>(array: T[], selector: (item: T) => number): number;
    }
}
declare module "util/Barcodes" {
    /**
     * Helpers for generating barcode values.
     * Using format Code 128 (Uniform Symbology Specification)
     * with Modulo 103 algorithm for generating checksum.
     *
     * @see http://www.finanssiala.fi/maksujenvalitys/dokumentit/Bank_bar_code_guide.pdf
     * @see http://www.finanssiala.fi/maksujenvalitys/dokumentit/Pankkiviivakoodi-opas.pdf
     */
    export class Barcodes {
        /**
         * Creates a value for barcode labels. Does not calculate the checksum by default.
         * Most libraries, which generate barcode labels, add the checksum and start and stop sequences automatically,
         * and those are not needed to be calculated externally.
         *
         * @param iban - IBAN bank account number. Must be a Finnish IBAN. Can have whitespace.
         * @param ref - Reference number as a string. Either fully numeric (max len 20), or starting with RF (max len 2+23).
         *        Can have whitespace.
         * @param dueDate - Date as string in DD.MM.YYYY format. False fills values with zeros.
         * @param euroAmount - Euro amount. If string, e.g. 3030,33€, is converted to cents amount 303033.
         *        Alternative can be passed cents amount as number.
         * @param includeChecksum - Calculates and appends the checksum to the end of the barcode value.
         *
         * @returns The barcode value (without start and end characters). The checksum is only calculated and appended to the value if the includeChecksum-parameter is true.
         */
        static getValue(iban: string, ref: string, dueDate: string | false, euroAmount: number | string, includeChecksum?: boolean): string;
        /**
         * Takes in 54 characters string value and calculates modulo 103 value.
         *
         * @param val - 54 characters long string of numbers
         *
         * @returns A Number within range of 0-99. Reminder of dividing the sum total of weighted value pairs with 103.
         */
        static modulo103(val: string): number;
    }
}
declare module "util/Cookies" {
    /**
     * Namespace salaxy.util contains utility classes that are not related to Salaxy business logic,
     * but to general functionality: JavaScript programming, browsers etc.
     *
     * All implementation should be independent from the framework (Angular 1, Angular, jQuery etc.).
     * Only the following dependencies are accepted:
     *
     * - Moment.js (not currently added)
     * - (New dependencies must be approved by Matti & Olli)
     */
    /**
     * Very simple cookie implementation - at the moment for token storage only.
     * Expand if needed for other use cases (see todo section in the docs).
     *
     * @todo Expand this class as necessary e.g. something like this: https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework
     * or this https://github.com/Booyanach/cookie-wrapper ... which unfortunately cannot be used at the moment, because they are GPL license.
     */
    export class Cookies {
        /**
         * Gets a cookie value by a key
         * @param cname - CName / key to look for.
         */
        get(key: string): string;
        /**
         * Sets a cookie value for the specified CName
         * @param cname - CName for the cookie value. Name is uri encoded as a fallback, but generally, you should make sure this is a valid cname.
         * @param value - Value to set for the cookie. Value is uri encoded.
         * @param expirationDays - Days until the cookie expires.
         * Set this to null or 0 to not set the expiration date at all.
         * This defaults the behavior where cookie is deleted when browser is closed.
         */
        setCookie(cname: string, value: string, expirationDays?: number): void;
    }
}
declare module "util/Years" {
    import { YearlyChangingNumbers } from "model/index";
    import { Holidays } from "model/index";
    /**
     * Helper for yearly changing sidecost percentages and holidays.
     */
    export class Years {
        /**
         * Gets salary calculation parameters that are changing yearly and holidays of the year.
         * The method is designed for the end-of-year and as such it only supports 2 years:
         * the current / previous year (from Jan to Nov) OR current / next year (in approx. December).
         *
         * @param forDate - Dately object (JavaScript date, ISO string formatted date, or moment) for which the numbers are fetched.
         *
         * @returns The yearly changing numbers if year supported. Error if year not supported.
         */
        static getYearlyChangingNumbers(forYear: any): YearlyChangingNumbers;
        /**
         * Gets holidays for the given year.
         *
         * @param forDate - Dately object (JavaScript date, ISO string formatted date, or moment) for which the holidays are fetched.
         *
         * @returns The holidays for the given year.
         */
        static getYearlyHolidays(forYear: any): Holidays;
    }
}
declare module "util/Dates" {
    import * as momentImport from "moment";
    import { DateRange, HolidayGroup } from "model/index";
    /**
     * Object that can be understood as date:
     *
     * - String is an ISO string "yyyy-mm-dd" without any time / timezone information.
     *   This is the default Date representation in Salaxy library and should typically be used.
     * - Date is a JavaScript date object
     * - Number is (as in Date constructor) a Unix time stamp - number of milliseconds since January 1, 1970, 00:00:00 UTC (the Unix epoch)
     * - Array of numbers is (as in Date constructor) [year, monthIndex, day].
     * - MomentJS object
     * - Special string "today" will set the value as today.
     *
     * If the object cannot be understood as date it will typically be converted to null. Sometimes this may also be an error.
     */
    export type DatelyObject = string | Date | number | number[] | momentImport.Moment | "today";
    /**
     * Date parsing, formatting and other operations within the Salaxy framework.
     * We use ISO 8601 as our Date format even inside JavaScript objects.
     * This is because of the following reasons:
     *
     * 1. Generally, we do not want to use JavaScript Date because it presents so many problems.
     * 2. However, we do not want to force our library consumers using Moment (or some other Date/Time library).
     * 3. We store dates in JSON as ISO 8601 date strings - this is universally the best practice
     * 4. ISO 8601 provides a way to present a date without time and most importantly timezone information
     * 5. ISO 8601 date also works best for most UI components (because of similar reasons as above).
     *
     * Implementation uses MomentJS, but we should avoid using MomentJS outside this library as much as possible.
     * This library is only for Dates. Time / DateTime functionality should be in separate library if necessary.
     */
    export class Dates {
        /**
         * Gets an object and returns it as the date format that is stored in the data structure with ISO 8601 format.
         *
         * @param datelyObject - Object that should be converted to Salaxy date: A string, JS Date object, Moment etc.
         *
         * @returns The date in simplest possible ISO 8601 compliant format "YYYY-MM-DD", null if empty, not valid date etc.
         */
        static asDate(datelyObject: DatelyObject): string;
        /**
         * Gets a date based on year, month and day (1-based).
         * NOTE: Unlike Date() constructor or Moment() constructor, all numbers are 1-based:
         * (2017, 2, 2) results to "2017-02-02" and not "2017-01-01T22:00:00.000Z" like in those constructors.
         *
         * @param year Year component of the date or "today" for today's year.
         * @param month Month component of the date (1-12) or "today" for today's month.
         * @param day Day of month component of the date (1-31) or "today" for today's day of month.
         */
        static getDate(year: number | "today", month: number | "today", day: number | "today"): string;
        /**
         * Converts the date to a JavaScript Date. Please note, that also time part is returned.
         * @param  datelyObject - Object that should be converted: A string, JS Date object, Moment etc.
         *
         * @returns JavaScript date corresponding to input or null.
         */
        static asJSDate(datelyObject: DatelyObject): Date;
        /**
         * Checks whether a string is in ISO format.
         *
         * @param isoDate Value that is evaluated.
         * @param required If set to true, the value is required. If required is null the false will be returned.
         *
         * @returns True if the given date is in the correct format.
         */
        static isValidDateTime(isoDate: string, required?: boolean): boolean;
        /** Get the date of today as ISO 8601 string format (YYYY-MM-DD)  */
        static getToday(): string;
        /** Get the date of today as moment object. */
        static getTodayMoment(): momentImport.Moment;
        /**
         * Gets the dately object as a Moment. If empty, not valid etc., returns null.
         * If the object is already a returns the given objectg (no clone).
         *
         * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
         * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
         *
         *  @return A Moment object or null.
         */
        static asMoment(datelyObject: DatelyObject, allowInvalid?: boolean): momentImport.Moment;
        /**
         * Gets the dately object as a Moment. If empty, not valid etc., returns null.
         * If the object is already a Moment, clones it.
         *
         * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
         * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
         *
         *  @return A Moment object or null.
         */
        static getMoment(datelyObject: DatelyObject, allowInvalid?: boolean): momentImport.Moment;
        /**
         * Returns the duration  object. If parameter is empty or null, defaults to Today (not Now).
         *
         * @param startDatelyObject Duration start that is parsed using getMoment().
         * Null before conversion defaults to today. Invalid objects result to invalid duration.
         * @param endDatelyObject Duration start that is parsed using getMoment().
         * Null before conversion defaults to today. Invalid objects result to invalid duration.
         *
         * @returns A Duration object, which may also be invalid
         */
        static getDuration(startDatelyObject: DatelyObject, endDatelyObject: DatelyObject): momentImport.Duration;
        /**
         * Gets a date range based on start and end dates.
         * Days count is calculated using getWorkdays() (Add parameter if you need other day count methods).
         *
         * @param start DateRange start that should be converted to Salaxy date: A string, JS Date object, Moment etc.
         * @param end DateRange end that should be converted to Salaxy date: A string, JS Date object, Moment etc.
         *
         * @returns DateRange based on start and end dates.
         */
        static getDateRange(start: DatelyObject, end: DatelyObject): DateRange;
        /**
         * Gets the workdays between two dates including both days.
         * Returns empty array if there is no start or if end is before start.
         * Returns 1 day if there is no end: we assume that the user selected just one day.
         *
         * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
         * @param endDate Corresponding end date.
         * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
         *
         * @returns The work days between the two dates (inclusive).
         */
        static getWorkdays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup?: HolidayGroup): string[];
        /**
         * Gets the vacation days (6 day week Mon-Sat) between two dates including both days.
         * Returns empty array if there is no start or if end is before start.
         * Returns 1 day if there is no end: we assume that the user selected just one day.
         *
         * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
         * @param endDate Corresponding end date.
         * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
         *
         * @returns The work days between the two dates (inclusive).
         */
        static getVacationDays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup?: HolidayGroup): string[];
        /**
         * Output one formatted date, or a formatted range of two dates.
         *
         * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.). Can be an empty value if endDate is not empty.
         * @param endDate Corresponding end date. Can be an empty value, in which case only start date will output.
         *
         * @returns A Simple date string e.g. "12.11.2019". Or a date range string with possible formats:
         *         "12. - 25.11.2019", "12.11. - 02.12.2019", "12.11.2019 - 12.01.2020", or "- 12.01.2020".
         */
        static getFormattedDate(startDate: DatelyObject, endDate?: DatelyObject): string;
        /**
         * Formats a dately object using MomentJs formatting:
         * e.g. "DD.MM.YYYY"
         * https://momentjs.com/docs/#/displaying/format/
         */
        static format(date: DatelyObject, format: string, nullValue?: string): string;
        /**
         * Gets the month as 1-based number: 1-12.
         *
         * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
         * Special string "today" can be used for fetching today's date.
         *
         * @returns Month between 1-12 or null if the object is not a dately object.
         */
        static getMonth(datelyObject: DatelyObject): number;
        /**
         * Gets the year of a date or today.
         *
         * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
         * Special string "today" can be used for fetching today's date.
         *
         * @returns Year, e.g. 2019 or null if the object is not a dately object.
         */
        static getYear(datelyObject: DatelyObject): number;
        /**
         * Returns true if the given dately object is a holiday.
         * Please note that the weekend days are not by default holidays
         *
         * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
         * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
         *
         * @returns True, if the given dately object is a holiday.
         */
        static isHoliday(datelyObject: DatelyObject, holidayGroup?: HolidayGroup): boolean;
        /**
         * Returns true if the given dately object is a working day (5 day week).
         * The day must not be Saturday or Sunday or not any day in the given holiday categoria.
         *
         * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
         * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
         *
         * @returns True, if the given dately object is a working day.
         */
        static isWorkday(datelyObject: DatelyObject, holidayGroup?: HolidayGroup): boolean;
        /**
         * Returns true if the given dately object is a vacation day (6 day week).
         * The day must not be Sunday or not any day in the given holiday categoria.
         *
         * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
         * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
         *
         * @returns True, if the given dately object is a working day.
         */
        static isVacationday(datelyObject: DatelyObject, holidayGroup?: HolidayGroup): boolean;
        /**
         * Adds given amount of working days to given day.
         *
         * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
         * @param days - Working days to add (positive or negative number).
         * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
         *
         * @returns New working day with amount of given working days ahead.
         */
        static addWorkdays(datelyObject: DatelyObject, days: number, holidayGroup?: HolidayGroup): string;
    }
}
declare module "util/DeepEqual" {
    /**
     * Utility class for comparing objects.
     * The implementation is based on fast-deep-equal: https://github.com/epoberezkin/fast-deep-equal
     * Modified to static method and re-written in typescript.
     */
    export class DeepEqual {
        /**
         * Compares objects a and b.
         * Returns true if the objects are equal using value-to-value comparison.
         *
         * @param a - Any object.
         * @param b - Any object to compare with.
         */
        static equal(a: any, b: any): boolean;
        private static isArray;
        private static keyList;
        private static hasProp;
    }
}
declare module "util/Numeric" {
    /**
     * Helpers for handling numbers: Mathematical operations, parsing and formatting numbers in our supported languages etc.
     */
    export class Numeric {
        /**
         * Parses a user input to a float taking into consideration the Finnish culture.
         * Main case is that decimal separator is usually a comma (,).
         * If there is both comma (,) and point (.) in the same string, NaN is returned.
         * Whitespaces are removed and if string contains commas they are replaced with points before conversion to Number.
         *
         * @param numCandidate - text that should be converted to a number if possible.
         */
        static parseNumber(numCandidate: string | number): number;
        /**
         * Formats a number as a euro price. Uses comma as decimal separator.
         * @param value - Numeric value to format
         * @param addSuffix - If true (default), adds the €-suffix.
         * @param nullChar The character or string for null or Nan value, the default is '-'.
         */
        static formatPrice(value: any, addSuffix?: boolean, nullChar?: string): string;
        /**
         * Rounds a JavaScript number in a reliable way.
         *
         * @param value Number that is rounded. If null given, returns null.
         * @param decimals Number of decimals after the decimal separator. Default is 2 as in money.
         * @returns Number properly rounded, Null/undefined as null, Non-numeric values return Number.NaN
         */
        static round(value: number, decimals?: number): number;
        /**
         * Fixes floating point problems in typical number.toString() scenarios.
         * E.g. `console.debug((0.07 * 100).toString());` returns "7.000000000000001".
         * Null/undefined returns empty string, numbers fixed and otherwise toString() is called.
         * @param value Number to show as string.
         * @returns Number rounded to specified number of decimals. Null/undefined returns empty string.
         * @example
         * console.debug(Numeric.toString(0.07 * 100)); // returns "7"
         * console.debug((0.07 * 100).toString());      // returns "7.000000000000001"
         */
        static toString(value: number): string;
        /**
         * Fixes floating point problems in typical number.toFixed() scenarios.
         * E.g. `console.debug((1.005).toFixed(2));` returns "1.00"
         * @param value Number to show as string. Supports null/undefined, but otherwise should be a number.
         * @param fractionDigits Number of digits after the decimal point. Must be in the range 0 - 20, inclusive.
         * @returns Number rounded to specified number of decimals. Null/undefined returns empty string.
         * @example
         * console.debug(Numeric.toFixed(1.005, 2)) // returns "1.01"
         * console.debug((1.005).toFixed(2))        // returns 1.00
         */
        static toFixed(value: number, fractionDigits?: number): string;
        /**
         * Rounds a JavaScript number in a reliable way in percentage format.
         *
         * @param value Number that is rounded.
         * @param decimals Number of decimals after the decimal separator. Default is 2 as in money.
         * If decimals is null, no rounding is done.
         * @param addSuffix If true, adds '%' sign at the end.
         * @param nullChar The character or string for null or Nan value, the default is '-'.
         */
        static formatPercent(value: any, decimals?: number, addSuffix?: boolean, nullChar?: string): string;
        /**
         * Rounds a JavaScript number and formats it using comma as a separator and optional suffix.
         *
         * @param value Number that is rounded.
         * @param decimals Number of decimals after the decimal separator. Default is 2 as in money. If decimals is null, no rounding is done.
         * @param suffix The suffix to add to the number.
         * @param nullChar The character or string for null or Nan value, the default is '-'.
         */
        static formatNumber(value: any, decimals?: number, suffix?: string, nullChar?: string): string;
        /**
         * Checks if the given object is a number.
         * @param numberCandidate Object to check
         */
        static isNumber(numberCandidate: any): boolean;
    }
}
declare module "util/Objects" {
    /**
     * Helpers for performing operations with objects
     */
    export class Objects {
        /**
         * Uses JSON Serialization / Deserialization to deep clone an object.
         * @param value Object to clone
         * @returns A cloned object.
         */
        static cloneJson<T>(value: T): T;
        /**
         * Extends one object with another using Intersection types.
         * https://www.typescriptlang.org/docs/handbook/advanced-types.html
         * @param first First object.
         * @param second Second object.
         */
        static extend<T, U>(first: T, second: U): T & U;
    }
}
declare module "util/Validation" {
    import { PensionCompany } from "model/index";
    /**
     * Provides base methods for validation.
     * These are typically used in user interface validator components.
     */
    export class Validation {
        /**
         * Returns true if the given value is a valid Finnish Personal ID (HETU / SSN).
         * Checks the format and checksum.
         * @param value Candidate for Finnish Personal ID.
         */
        static isPersonalIdFi(value: string): boolean;
        /**
         * Returns true if the value is a valid e-mail.
         * Currently uses a simple regex (source?).
         * @param value E-mail candidate.
         */
        static isEmail(value: string): boolean;
        /**
         * Returns true if the value is a valid mobile phone number: 5-16 numbers and spaces (5-18 if there is a "+" in the beginning).
         * Currently uses a naive regex, but we could extend this to take into account the regional differences.
         * @param value Mobile phone number candidate.
         */
        static isMobilePhone(value: string): boolean;
        /**
         * Returns true if the value is a valid currency amount with maximum two decimals.
         * @param value Currency amount candidate
         */
        static isCurrency(value: string): boolean;
        /**
         * Returns true if the value is a valid number.
         * @param value Number candidate
         */
        static isNumber(value: string): boolean;
        /**
         * Returns true if the value is a valid integer.
         * @param value Number candidate
         */
        static isInteger(value: string): boolean;
        /**
         * Returns true if the value is a valid tax percent amount with maximum one decimal.
         * @param value Tax percent candidate
         */
        static isTaxPercent(value: string): boolean;
        /**
         * Returns true if the value is a valid SMS verification code.
         * SMS verification code is 6 digits.
         * @param value Verification code candidate.
         */
        static isSmsVerificationCode(value: string): boolean;
        /** Returns true if the value is a valid Finnish Postal code: 5 numbers. */
        static isPostalCodeFi(value: string): boolean;
        /**
         * Returns true if the value is a valid Finnish Company ID. If the format is not correct, will return false.
         * @param companyIdCandidate String to validate / format
         * @param allowStarAsChecksum If true, allows the checksum character (the last char) to be an asterix (*).
         * @returns boolean
         */
        static formatCompanyIdFi(companyIdCandidate: string, allowStarAsChecksum?: boolean): boolean;
        /**
         * Returns true if the value is a valid IBAN number.
         * Checks for the country specific length as well as checksum.
         * @param value IBAN number number candidate.
         */
        static isIban(value: string): boolean;
        /**
         * Returns PensionCompany for the given pension contract number.
         * @param value - Pension contract number.
         */
        static isPensionContractNumber(value: string): PensionCompany;
        /**
         * Returns PensionCompany for the given *temporary* pension contract number.
         * @param value - TemporaryPension contract number.
         */
        static isTemporaryPensionContractNumber(value: string): PensionCompany;
        private static mod97;
    }
}
declare module "util/Iban" {
    /**
     * Helpers for handling Iban bank account numbers
     */
    export class Iban {
        /**
         * Format an Iban number in 4 digits segments and trim out other than alphanum characters.
         * @param value - String value to format
         * @returns string value
         */
        static formatIban(value: string): string;
    }
}
declare module "util/Brand" {
    /**
     * Utility for accessing brand specific style definitions.
     */
    export class Brand {
        /**
         * Returns the brand color defined as a global variable.
         *
         * @param color - Named brand color to get.
         * @param format - Color format: hex like '#289548', rgb like 'rgb(40, 149, 72)' or rgba like 'rgba(40, 149, 72, 1.0'. Default value is hex.
         */
        static getBrandColor(color: "primary" | "success" | "info" | "warning" | "danger", format?: "hex" | "rgb" | "rgba"): string;
        /**
         * Returns the css global style variable.
         * E.g. --salaxy-brand-primary
         * @param variableName - Variable name.
         */
        static getStylePropertyValue(variableName: string): string;
        /**
         * Returns the hex color value as rgb css function.
         * For example:  value '#289548' as 'rgba(40, 149, 72)'
         *
         * @param hexValue - Hex value.
         */
        static hex2rgb(hexValue: string): string;
        /**
         * Returns the hex color value as rgba css function.
         * For example:  value '#289548ff' as 'rgba(40, 149, 72, 1)'
         *
         * @param hexValue - Hex value.
         */
        static hex2rgba(hexValue: string): string;
        /**
         * Returns the rgb value as hex string.
         * For example: 40, 149, 72 as #289548.
         *
         * @param r - red parameter.
         * @param g - green parameter.
         * @param b - blue parameter.
         */
        static rgb2hex(r: number, g: number, b: number): string;
        /**
         * Returns the rgba value as hex string.
         * For example: 40, 149, 72, 1 as #289548ff.
         *
         * @param r - red parameter.
         * @param g - green parameter.
         * @param b - blue parameter.
         * @param a - alpha parameter.
         */
        static rgba2hex(r: number, g: number, b: number, a: number): string;
        private static hex2rgbaValues;
    }
}
declare module "util/Config" {
    /**
     * Environment specific configuration for Salaxy API's and JavaScript in general
     */
    export interface Config {
        /** The base address of the Salaxy http API server, e.g. https://secure.salaxy.com */
        apiServer?: string | null;
        /** The base address of the Palkkaus web site, e.g. https://www.palkkaus.fi */
        wwwServer?: string | null;
        /** A flag indicating if the current configuration is for test environment only. If true, the site is for testing only. */
        isTestData?: boolean | null;
        /** A flag indicating if the site sets a Salaxy cookie which persists over web sessions. If true, the cookie will be created. */
        useCookie?: boolean | null;
    }
}
declare module "util/index" {
    export * from "util/Arrays";
    export * from "util/Barcodes";
    export * from "util/Cookies";
    export * from "util/Dates";
    export * from "util/DeepEqual";
    export * from "util/Numeric";
    export * from "util/Objects";
    export * from "util/Validation";
    export * from "util/Iban";
    export * from "util/Brand";
    export * from "util/Config";
    export * from "util/Years";
}
declare module "i18n/dictionary" {
    /** Internationalization dictionary
    @ignore */
    export const dictionary: any;
}
declare module "logic/Translations" {
    /**
     * Provides universal i18n methods:
     * The core JavaScript code uses this to determine the UI texts and potentially
     * formatting. The frameworks on top of core (Angular, NG1) may have their own
     * frameworks and/or they may integrate to this core i18n.
     */
    export class Translations {
        /**
         * Translates given key to the current language.
         * @param key - Key for the text.
         * @param interpolateParams - Variables for the interpolation.
         * @returns The translated value in current language or if none is found the key.
         */
        static get(key: string, interpolateParams?: object): string;
        /**
         * Translates given key to the current language.
         * @param key - Key for the text.
         * @param defaultValue - The default value that is returned if the key is not found. Null is the default.
         * @returns The translated value in current language or if none is found the key.
         */
        static getWithDefault(key: string, defaultValue?: any): string;
        /**
         * Adds a dictionary.
         * @param lang - ISO language code for the dictionary to be added: fi, en or sv.
         * @param translations - A translations object (json).
         */
        static addDictionary(lang: string, translations: object): void;
        /**
         * Add all dictionaries from a single language object. This object must have language codes as first level keys.
         * @param translations - A translations object (json).
         */
        static addDictionaries(translations: object): void;
        /**
         * Sets the Salaxy user interface language.
         * @param lang - One of the supported languages: "fi", "en" or "sv".
         */
        static setLanguage(lang: string): void;
        /**
         * Gets the current user interface language
         * @returns One of the supported languages: "fi", "en" or "sv".
         */
        static getLanguage(): string;
        /** Current language. */
        private static lang;
        /** Current dictionary object. */
        private static dictionary;
    }
}
declare module "logic/CalculatorLogic" {
    import { Avatar, Calculation, CalculationRowType, ResultRow, WorkerAccount } from "model/index";
    /**
     * Provides business logic related to building a salary calculator.
     */
    export class CalculatorLogic {
        /**
         * Sets the Worker in Calculation
         * @param calc Calculation to set the worker
         * @param workerId Worker ID (for the case where the Worker is null).
         * @param worker Worker for properties.
         */
        static setWorker(calc: Calculation, workerId: string, worker: WorkerAccount): void;
        /**
         * Gets a new blank object with default values, suitable for UI binding.
         * This is a synchronous method - does not go to the server.
         */
        static getBlank(): Calculation;
        /**
         * Returns true if calculation totals should be shown:
         * If there are any such rows that the calculation result makes sense.
         */
        static shouldShowTotals(calc: Calculation): boolean;
        /**
         * Get a text that summarizes the start- and end dates as well as the number of work days.
         */
        static getDatesSummary(calc: Calculation): string;
        /**
         * Gets the Avatar of other party in a calculation:
         * Employer or Worker
         */
        static getOtherParty(calc: Calculation, currentAvatar: Avatar): Avatar;
        /**
         * Gets a total sum for a row category from calculation results.
         * @param calc - Calculation object
         * @param category - Row category (collection of row types)
         */
        static getTotalForCategory(calc: Calculation, category: "baseSalary" | "salaryAdditions" | "salary" | "benefits" | "expenses" | "deductions" | "holidays" | "expensesSection"): number;
        /**
         * Gets a total sum for a row category from calculation results.
         * @param calc - Calculation object
         * @param types - Array of row types that are included in the result
         */
        static getTotalForRowsByType(calc: Calculation, ...types: CalculationRowType[]): number;
        /**
         * Filter the result rows of a calculation based on the type
         * @param calc - Calculation object
         * @param types - Array of row types that are included in the result
         */
        static getResultRowsByType(calc: Calculation, ...types: CalculationRowType[]): ResultRow[];
        /**
         * Filter the result rows of a calculation based on a row category
         * @param calc - Calculation object
         * @param category - Row category (collection of row types)
         */
        static getResultRowsByCategory(calc: Calculation, category: "baseSalary" | "salaryAdditions" | "salary" | "benefits" | "expenses" | "deductions" | "holidays" | "expensesSection"): ResultRow[];
    }
}
declare module "logic/usecases/UsecaseChildCare" {
    import { Usecase } from "logic/usecases/Usecase";
    /** Defines the usecase data for childCare including the MLL childCare */
    export interface UsecaseChildCare extends Usecase {
        /**
         * If true, the work is done on Sunday or public holiday.
         * This means that the salary is increased by 100% according to Finnish law
         */
        isSunday: boolean;
        /**
         * If true, a custom price is used instead of the recommended price.
         * Currently, this only affects the MLL case as the normal childCare price is always set by the user.
         */
        useCustomPrice: boolean;
        /** If true, the salary calculation includes child care subsidy paid by the government / community. */
        isChildcareSubsidy?: boolean;
        /** Amount for the child care subsidy paid by the government / community. */
        subsidyAmount: number;
    }
}
declare module "logic/usecases/UsecaseCleaning" {
    import { Usecase } from "logic/usecases/Usecase";
    /** Defines the usecase data for cleaning */
    export interface UsecaseCleaning extends Usecase {
        /**
         * If salaries are compensated (when holidays are not kept, isFullTime=false),
         * the percentage is different depending on whether the person has been working for a full year or not.
         */
        isContractLessThanYear: boolean;
        /**
         * If true, this is a full time contract in the respect of holidays:
         * Holidays are kept during summer and salary is paid for these holidays.
         */
        isFullTime: boolean;
        /**
         * If worktype is "other", the user should describe the work.
         * These descriptions may be used in insurance reports generation.
         */
        workTypeOtherMessage: boolean;
    }
}
declare module "logic/usecases/UsecaseConstruction" {
    import { Usecase } from "logic/usecases/Usecase";
    /** Defines the usecase data for construction */
    export interface UsecaseConstruction extends Usecase {
        /**
         * If worktype is "other", the user should describe the work.
         * These descriptions may be used in insurance reports generation.
         */
        workTypeOtherMessage: boolean;
        /** If true, the TES additions are included in the agreed salary. */
        isTesIncludedInSalary: boolean;
        /**
         * If true, the expenses are agreed in a bispoken way.
         * Default is that expenses are based on Framework agreement (TES) tables.
         */
        isExpensesCustom: boolean;
        /** Daily distance (km) from worker home to construction site. */
        dailyTravelExpensesKm: number;
        /** Daily expenses (työkalukorvaus) if isExpensesCustom=true  */
        dailyExpenses: number;
        /** Daily travel expenses if isExpensesCustom=true  */
        dailyTravelExpenses: number;
    }
}
declare module "logic/usecases/UsecasesLogic" {
    import { Calculation } from "model/index";
    import { Usecase } from "logic/usecases/Usecase";
    import { UsecaseGroup } from "logic/usecases/UsecaseGroup";
    /**
     * Provides business logic for Calculation usecases:
     * Salary framework contracts ("työehtosopimus") and other salary recommendations
     * that are used as bases for salary calculation.
     */
    export class UsecasesLogic {
        /**
         * Known use cases in the first implementation.
         * These implementations replace the olf Calc.Framework.Type -based solution that is not very extensible.
         * The naming is still based on the old system for easier compatibility.
         */
        static readonly knownUseCases: {
            /** Usecase has not been defined. Note that value is null, not "https://secure.salaxy.com/v02/api/usecases/notDefined" */
            notDefined: any;
            /** Generic child care usecase */
            childCare: string;
            /** Cleaning */
            cleaning: string;
            /** Construction (Raksa) */
            construction: string;
            /** NOT IMPLEMENTED: Entrepreneur use case - not implemented for this release. */
            entrepreneur: string;
            /** Child care according to MLL recommendations. */
            mll: string;
            /** NOT IMPLEMENTED: Santa Claus for Christmas campaigns - not implemented for this release. */
            santaClaus: string;
            /** Generic household salary payment usecase user interface. */
            other: string;
        };
        /**
         * Finds a parent group of a given usecase node
         * @param role - Role for which the use cases are filtered.
         * Currently, only "household" is supported, but other roles will follow.
         * @param usecase - The usecase whose parent is fetched.
         * If you pass a group here, the function will return null which will typically mean the tree root in the user interface.
         * @example
         * // Creates a simple calculation
         * const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
         * UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
         * this.context.calculations.recalculateCurrent((calc) => {
         *     calc.worker.paymentData.telephone = "+358401234567";
         * });
         */
        static findUsecaseById(role: "household", usecaseId: any): Usecase;
        /**
         * Finds a group based on group id or usecase id of a child.
         * @param role - Role for which the use cases are filtered.
         * Currently, only "household" is supported, but other roles will follow.
         * @param useCaseOrGroupId - Identifier of the group or usecase.
         * If id is a usecase id, its parent is returned.
         * @example
         * // Returns a parent of a usecase or null if the selectedItem is a group.
         * const group = UsecasesLogic.findGroupById("household", this.selectedItem.id);
         * if (group.id === this.selectedItem.id) {
         *     return null;
         * }
         * return group;
         */
        static findGroupById(role: "household", useCaseOrGroupId: string): UsecaseGroup;
        /**
         * Gets a tree of usecases (user interfaces for defining a framework) for a given role.
         * @param role - Role for which the use cases are filtered.
         * Currently, only "household" is supported, but other roles will follow.
         */
        static getUsecaseTree(role: "household"): UsecaseGroup[];
        /**
         * Abstract the getting of the use case for the calculation.
         * We are expecting changes to the usecase implementation.
         * @param calc Calculation into which the usecase data is stored.
         */
        static getUsecaseData(calc: Calculation): Usecase;
        /**
         * Sets calculation properties based on selected usecase.
         * @param calc Calculation to set
         * @param usecase Usecase to apply.
         * @example
         * // Creates a simple calculation
         * const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
         * UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
         * this.context.calculations.recalculateCurrent((calc) => {
         *     calc.worker.paymentData.telephone = "+358401234567";
         * });
         */
        static setUsecase(calc: Calculation, usecase: Usecase): Usecase;
        /** HACK: Get periodText flag until we are able to implement the real ngModel binding to sxy-period control. */
        static getPeriodText(calc: Calculation): string;
        /** HACK: Set periodText flag until we are able to implement the real ngModel binding to sxy-period control. */
        static setPeriodText(calc: Calculation, value: string): void;
        /**
         * Applies the usecase to a calculation, especially to the old Framework object.
         * Should be called before sending the calculation to the server (recalculate/save).
         * @param calc Calculation where usecase is set to calc.data.usecase.
         */
        static applyUseCase(calc: Calculation): void;
        /** Applies the constructions salary calculation usecase */
        static applyUsecaseConstruction(calc: Calculation): void;
        /** Applies the childCare salary calculation usecase */
        static applyUsecaseChildCare(calc: Calculation): void;
        /** Applies the cleaning salary calculation usecase */
        static applyUsecaseCleaning(calc: Calculation): void;
        /** Applies the MLL salary calculation usecase */
        static applyUsecaseMll(calc: Calculation): void;
        private static applyUsecaseSantaClaus;
        private static applyCommonUsecaseProperties;
        /**
         * Gets a usecase for calculations that were created before the
         * new user interface and usecase model was stored on the server.
         * @param calc Original calculation.
         */
        private static getUsecaseBackwardCompatibility;
        private static translate;
    }
}
declare module "logic/usecases/index" {
    export * from "logic/usecases/Usecase";
    export * from "logic/usecases/UsecaseGroup";
    export * from "logic/usecases/UsecasesLogic";
    export * from "logic/usecases/UsecaseChildCare";
    export * from "logic/usecases/UsecaseCleaning";
    export * from "logic/usecases/UsecaseConstruction";
}
declare module "logic/CalcRowsLogic" {
    import { CalculationRowType, CalculationRowUnit, Unit } from "model/index";
    import { CalcRowConfig, CalculationRowCategory } from "logic/model/index";
    /**
     * Provides business logic for Calculation rows:
     * How to create user interfaces, reports etc. based on them and their categories.
     */
    export class CalcRowsLogic {
        role: "household" | "company";
        /**
         * Gets a single UI configuration for Calculation row.
         * @param type Type for which the configuration is fetched.
         */
        static getRowConfig(type: CalculationRowType): CalcRowConfig;
        /**
         * Gets an indicator string (1-2 characters) for a unit.
         * @param unit Unit that should be described.
         * @returns Given unit indicator or null if none is matched (convert to empty string if necessary).
         */
        static getUnitIndicator(unit: CalculationRowUnit | Unit | string): "h" | "km" | "€" | "%" | "pv" | "kpl" | "vko" | "jakso";
        /**
         * Creates a new CalcRowsLogic helper
         * @param role Role for which the rows are fetched.
         */
        constructor(role?: "household" | "company");
        /**
         * Gets the row types by category.
         */
        getRowTypesByCategory(category: CalculationRowCategory): CalculationRowType[];
        /**
         * Gets the row configurations by category(ies).
         * @param categories List of categories to filter by.
         */
        getRowConfigsByCategory(categories: CalculationRowCategory[]): CalcRowConfig[];
        /**
         * Gets the row configurations based on a list of row types.
         * @param types List of types to filter by.
         */
        getRowConfigsByType(types: CalculationRowType[]): CalcRowConfig[];
        /**
         * Gets a single UI configuration for Calculation row.
         * This method filters for user role. Use static method to return any row configuration (also faster).
         * @param type Type for which the configuration is fetched.
         */
        getRowConfig(type: CalculationRowType): CalcRowConfig;
        /**
         * Gets all the UI configuration objects for calculation rows.
         */
        getRowConfigs(): CalcRowConfig[];
        /** Raw classification of row types to primary categories. */
        static rowsByCategory: {
            /** Base salary types: the ones that you define directly */
            salary: CalculationRowType[];
            /** Additions to base salary - typically a percentage of base salary */
            salaryAdditions: CalculationRowType[];
            /** Benefits: Not paid as money but taxable income */
            benefits: CalculationRowType[];
            /** Expenses and other non-taxed compensations */
            expenses: CalculationRowType[];
            /** Holidays */
            holidays: CalculationRowType[];
            /** Deductions */
            deductions: CalculationRowType[];
            /** Types that are used only as temporary / input types in Calculations - not in the results. */
            salaryCalc: CalculationRowType[];
            /** Other row types */
            other: CalculationRowType[];
        };
        /** Converts a raw excel generated row configuration to language-versioned JSON object. */
        private static getRowConfigInternal;
    }
}
declare module "logic/CalculationResultLogic" {
    import { CalculationResult } from "model/index";
    /** Extension methods for CalculationResult */
    export class CalculationResultLogic {
        /**
         * Adds Calculation Results together and returns a new CalculationResult
         * @param calculationResults CalculationResults to add.
         * @returns A new CalculationResult.
         */
        static add(calculationResults: CalculationResult[]): CalculationResult;
        private static addResults;
        private static addTotals;
        private static addWorkerCalc;
        private static addEmployerCalc;
        private static safeNum;
        private static safeArr;
        private static safeObj;
    }
}
declare module "logic/ContractLogic" {
    import { Avatar, CompanyAccount, ContractParty, EmploymentContract, PersonAccount } from "model/index";
    /**
     * Provides business logic related to displaying / editing an employment contract.
     */
    export class ContractLogic {
        /**
         * Creates a new contract for the current account.
         * The current account is set as Employer or Worker in this contract.
         * @param isWorker If true, current account is in role Worker.
         * If false, the current account is Employer.
         * @param account The current account object (Person or Company).
         */
        static getBlankForCurrentAccount(isWorker: boolean, account: PersonAccount | CompanyAccount): EmploymentContract;
        /**
         * Gets a new blank object with default values, suitable for UI binding.
         */
        static getBlank(): EmploymentContract;
        /**
         * Gets the Avatar of other party in a contract:
         * Employer or Worker
         */
        static getOtherParty(contract: EmploymentContract, currentAvatar: Avatar): Avatar;
        /** Gets a contract party from an Account object: Person or Company */
        static getContractParty(person: PersonAccount | CompanyAccount): ContractParty;
        /** Constructs text for price per (such as 15€/h or 2000€/kk) */
        static constructPricePerText(formattedPrice: any, salaryType: any): any;
        /** Constructs text for term date */
        static constructTermDateText(workType: any, startDate: any, endDate: any): string;
        /** Constructs text salary work hours */
        static constructSalaryWorkHoursText(worksHoursType: any, workHours: any): any;
        /** get full address as two lines or display a human-readable default value
         * @param target Any object that has an contact property
         */
        static constructAddressText(party: ContractParty): string;
    }
}
declare module "logic/ContractPartyLogic" {
    import { ContractParty, WorkerAccount } from "model/index";
    /**
     * Provides business logic related to displaying / editing Contact objects
     */
    export class ContractPartyLogic {
        /**
         * Gets a new blank Contact with default values, suitable for UI binding.
         */
        static getBlank(): ContractParty;
        /** Converts a WorkerAccount object to a ContractParty object. */
        static getFromWorker(worker: WorkerAccount): ContractParty;
        /** Gets sample contacts for demonstration user interfaces. */
        static getSampleContacts(): ContractParty[];
    }
}
declare module "logic/EnumerationsLogic" {
    /**
     * Provides metadata and other helpers for enumerations.
     */
    export class EnumerationsLogic {
        /**
         * Gets a label text for an enumeration value
         * @param enumType - Name of the Type of the enumeration.
         * @param enumValue - Value for which the label is fetched.
         */
        static getEnumLabel(enumType: string, enumValue: string | any): any;
        /**
         * Gets a description text for an enumeration value
         * @param enumType - Name of the Type of the enumeration.
         * @param enumValue - Value for which the description is fetched.
         */
        static getEnumDescr(enumType: string, enumValue: string | any): any;
        /**
         * Gets a very short user interface text for an enumeration.
         * Currently only suppotrted for Unit, but we expect more of these.
         * If the texts will be language-versioned, that logic will be here as well.
         * @param enumType - Name of the Type of the enumeration.
         * @param enumValue - Value for which the text is fetched.
         */
        static getEnumShortText(enumType: string, enumValue: string | any): any;
        /**
         * Gets the metadata (texts etc.) for an enumeration value.
         * @param enumType - Name of the Type of the enumeration.
         * @param enumValue - Value for which the label is fetched.
         */
        static getEnumValueMetadata(enumType: string, enumValue: string): {
            type: string;
            name: string;
            order: number;
            label: any;
            descr: any;
        };
        /**
         * Gets the metadata (texts etc.) for an enumeration and its values.
         * @param enumType - Name of the Type of the enumeration.
         */
        static getEnumMetadata(enumType: string): {
            name: string;
            descr: any;
            values: {
                type: string;
                name: string;
                order: number;
                label: any;
                descr: any;
            }[];
        };
        /**
         * Place to override generatedEnumData.
         * TODO: These texts should be moved to C# code server-side and removed from here.
         */
        private static enumDataOverride;
        private static lowerCamelCase;
        private static upperCamelCase;
    }
}
declare module "logic/HolidaysLogic" {
    import { AbsenceCauseCode, AnnualLeavePayment, Avatar, HolidaySpecification, HolidayYear, WorkerAccountEmployment } from "model/index";
    /** Logic for Holiday years and other holiday functionality. */
    export class HolidaysLogic {
        /**
         * Gets the multiplier that is used in Average Daily Salary calculation (keskipäiväpalkka).
         * @param holidays Number of holidays for the year.
         */
        static getDailySalaryMultiplier(holidays: number): number;
        /**
         * Gets a new blank holiday year.
         * @param year Holiday accrual year (e.g. 2019) or 0 for current year and -1 for previous year (from -10 to 10).
         * Current year is always from 1.4. until 31.3. e.g. year 2019 is 1.4.2019 until 31.3.2020.
         * The main part of the holidays from holiday accrual year 2019 are kept on summer 2020.
         */
        static getBlank(year?: number): HolidayYear;
        /** Gets a default specification for a Worker account */
        static getDefaultHolidaySpec(employment: WorkerAccountEmployment): HolidaySpecification;
        /** Gets the salary payable type or default type if both are possible. */
        static getSalaryPayableType(absenseCauseCode: AbsenceCauseCode): "defaultPaid" | "defaultUnpaid" | "both" | "paid" | "unpaid";
        /** Gets different total days calculation of planned leaves. */
        static getPlannedLeavesCount(holidayYear: HolidayYear, type?: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart"): number;
        /** Gets a calculated total or saldo row for paid calculations */
        static getPaidHolidaysCalculation(holidayYear: HolidayYear, type?: "startSaldo" | "total" | "endSaldo", dailyHolidaySalary?: any): AnnualLeavePayment;
        /** Gets a display avatar for a paid calculation */
        static getPaidCalculationAvatar(type: "row" | "startSaldo" | "total" | "endSaldo", row: AnnualLeavePayment): Avatar;
    }
}
declare module "logic/IncomeTypesLogic" {
    import { IncomeTypeMetadata } from "logic/model/index";
    /**
     * Provides business logic for Income types in Incomes Registry (Tulorekisteri).
     */
    export class IncomeTypesLogic {
        /** Cached language-versioned list of all types - cached version of getAll() */
        static allTypes: IncomeTypeMetadata[];
        /**
         * Cached language-versioned list of supported types.
         * This is the list of types that you typically use.
         */
        static supportedTypes: IncomeTypeMetadata[];
        /**
         * Searches the income type texts (supportedTypes) using the current language version.
         * Returns all types if the search string is less than 2 chars.
         * @param searchString Search text - will be used as case-insensitive. Will be trimmed. Minimum length 2.
         */
        static search(searchString: string): IncomeTypeMetadata[];
        /**
         * Gets all codes in Incomes register reporting.
         * This also includes the Salaxy internal codes eg. 1: "Ignored" / "Not reported"
         * For normal language-versioned behavior use cached allTypes field.
         * @param language Language for the label.
         */
        static getAll(language?: "fi" | "en" | "sv" | null): IncomeTypeMetadata[];
        /**
         * Gets the original codes in Incomes register.
         * This does not include virtual codes, eg. 1: "Ignored" / "Not reported"
         * For normal language-versioned behavior use cached originalTypes field.
         * @param language Language for the label. Typically use null for automatic language versioning.
         */
        static getOriginal(language?: "fi" | "en" | "sv" | null): IncomeTypeMetadata[];
    }
}
declare module "i18n/occupationcodes" {
    /** @ignore */
    export const occupationsData: {
        "id": string;
        "code": string;
        "labelFi": string;
        "labelEn": string;
        "labelSv": string;
        "searchFi": string;
        "searchEn": string;
        "searchSv": string;
        "freeTextFi": string;
        "freeTextEn": string;
        "freeTextSv": string;
        "keyword": string;
    }[];
}
declare module "logic/Occupations" {
    import { Occupation } from "logic/model/index";
    /**
     * Helper for Statistics Finland Occupation codes and texts related to them.
     * Occupation is used for insurance pricing and reporting..
     */
    export class Occupations {
        /** Gets all occupations for all languages. */
        static getAllForAllLanguages(): Occupation[];
        /**
         * Gets all the items of a given type.
         * @returns All occupation entries
         */
        static getAll(language?: "fi" | "en" | "sv" | null): Occupation[];
        /**
         * Searches the occupations.
         * @param searchString String to search for. Typically user input.
         * @param language Language to search for.
         */
        static search(searchString: string, language?: "fi" | "en" | "sv" | null): Occupation[];
        /**
         * Gets a single occupation based on the original Statistics Finland code.
         * @param code - Statistics finland 5-number code.
         * Note that this is different than the Salaxy ID stored in Calculation.
         * Salaxy ID may specify additional suffix separated by dash. For Salaxy ID, use getById() instead.
         * @returns Occupation metadata or null if not found.
         */
        static getByCode(code: string): Occupation;
        /**
         * Gets a single occupation based on the its Salaxy ID, which is derived from the original  Statistics Finland code.
         * @param id - Salaxy ID for the occupation.
         * This is the id stored in the calculation, but not the one that is reported to Incomes Register.
         * @returns Occupation metadata or null if not found.
         */
        static getById(id: string): Occupation;
        /**
         * Fetches a list of occupations either based on list of IDs or a known keyword.
         * @param idListOrKeyword Either a comma separated list of ID's
         * or a known keyword: "household" and "company" currently supported.
         */
        static getByIds(idListOrKeyword: string): Occupation[];
        private static data;
    }
}
declare module "logic/OnboardingLogic" {
    /**
     * Business logic relating to Onboarding process and Digital signature.
     */
    export class OnboardingLogic {
        /** Gets the authentication methods for digital signature. */
        static getTupasMethods(): {
            id: string;
            title: string;
            img: string;
            isPopular: boolean;
        }[];
    }
}
declare module "logic/ProductsLogic" {
    import { AccountProducts } from "model/index";
    import { Product } from "logic/model/index";
    /**
     * Framework independendent business logic methods and helpers for Products.
     */
    export class ProductsLogic {
        /** Gets the default products for a given role */
        static getDefault(role: "household" | "company" | "worker"): AccountProducts;
        /**
         * Gets a product based on id.
         * @param accountProducts The account products object containing all the products.
         * @param id Product id to fetch for - this is the name of the property key in accountProducts and id property of the value.
         * @param defaultValue Default value to return if accountProducts or id is falsy or id is not found or object returned by id is falsy.
         */
        static getProductById(accountProducts: AccountProducts, id: string, defaultValue?: any): Product;
        /**
         * Gets a product of a given type.
         * @param typeName Name of the type (the asme as generic typ T).
         */
        static getProductByType<T extends Product>(accountProducts: AccountProducts, typeName: string, defaultValue?: any): T;
        /** Gets all the products for anonymous user (everything visible) */
        static getAllProducts(): AccountProducts;
        private static getAccountProductDefaults;
    }
}
declare module "logic/ReportsLogic" {
    import { calcReportType } from "api/index";
    import { ReportType } from "model/index";
    /**
     * Salaxy business logic helpers. Mainly helper classes with static helper methods.
     *
     * All implementation should be independent from the framework (Angular 1, Angular, jQuery etc.).
     * Only the following dependencies are accepted:
     *
     * - Moment.js (not currently added)
     * - (New dependencies must be approved by Matti & Olli)
     */
    /**
     * Framework independendent business logic methods and helpers for Reporting.
     */
    export class ReportsLogic {
        /**
         * Gets a link URL for a yearly report. This is a relative link from the servr root (e.g. "/ReportHtml/YearEndReportJson?year=2017").
         * Typically the environment specific implementation needs to figure out the server address and token.
         * @param type - Type of the report must be one of the yearly reports
         * @param year - Year for the report
         * @param token - Token for authentication. Adds "access_token" to the query string.
         * This is typically required from the environment specific implementation,
         * but in exceptional cases you may use some other authentication mechanism and thus leave this null.
         * @param id - Worker ID for those reports that are specific to one Worker.
         * @param id2 - Second Worker ID for those reports that have two Workers in one report
         */
        static getYearlyReportUrl(type: ReportType, year: number, token?: string, id?: string, id2?: string): any;
        /**
         * Gets a URL for a calculation report. This is a relative link from the servr root (e.g. "/ReportHtml/SalarySlip/CALC_ID_HERE").
         * Typically the environment specific implementation needs to figure out the server address and token.
         * @param reportType - Type of report
         * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
         * @param token - Token for authentication. Adds "access_token" to the query string.
         * This is typically required from the environment specific implementation,
         * but in exceptional cases you may use some other authentication mechanism and thus leave this null.
         */
        static getCalcReportUrl(reportType: calcReportType, calcId: string, token?: string): string;
        /** Gets metadata about the supported report types */
        static getReportTypes(): {
            id: string;
            title: string;
            description: string;
            roles: string;
            category: string;
        }[];
    }
}
declare module "logic/TaxCard2019Logic" {
    import { PersonAccount, Taxcard, TaxcardKind, WorkerAccount } from "model/index";
    /**
     * Provides business logic related to TaxCards:
     * The Finnish way of determining the widtholding tax that Employer should deduct from Worker salary.
     */
    export class TaxCard2019Logic {
        /**
         * Gets a new blank taxcard with default values, suitable for UI binding.
         *
         * @param worker - Sets the worker as the given account.
         * @param kind - If specified, sets the type and defaults according to the type
         */
        static getBlank(worker?: PersonAccount | WorkerAccount, kind?: TaxcardKind): Taxcard;
        /**
         * Gets the tax card year for a specific salary date.
         * For January, this is the previous year.
         * @param salaryDate Salary date. If null, today is used.
         */
        static getTaxCardYear(salaryDate?: string): number;
        /**
         * Checks if the taxcard is valid for teh salary date.
         * @param card Taxcard to check.
         * @param salaryDate Salary date. If null, today is used.
         */
        static isTaxCardValid(card: Taxcard, salaryDate?: string): boolean;
        /**
         * Gets the best tax card for the specified Worker account and for the specified salary date.
         */
        static getBestTaxCard(taxcards: Taxcard[], salaryDate?: string): Taxcard;
        /**
         * Sorts an array of taxcards to ascending order (latest last).
         * See taxCardSorter for logic.
         */
        static sortAsc(taxcards: Taxcard[]): Taxcard[];
        /**
         * Sorts an array of taxcards to descending order (latest first).
         * See taxCardSorter for logic.
         */
        static sortDesc(taxcards: Taxcard[]): Taxcard[];
        /**
         * Sorter for taxcards.
         * First sorts based on card date, then based on updatedAt.
         */
        static taxCardSorter(ob1: Taxcard, ob2: Taxcard): number;
        /** Gets the current total income that has been registered for a given taxcard. */
        static getTotalIncome(card: Taxcard): number;
        /**
         * Gets information about the the income limit and real percent.
         * Note that this method calculates the real percent i.e.
         * for examples and no tax cards 60%. If you wish to see the example percent here (23%),
         * catch it separately.
         * @param taxcard Card to analyze
         */
        static getIncomeLimitInfo(taxcard: Taxcard): {
            type: TaxcardKind;
            percent: number;
            hasLimit: boolean;
            fractionOfLimit: number;
        };
    }
}
declare module "logic/WorkerLogic" {
    import { AvatarPictureType, EmploymentRelationType, LegalEntityType, WorkerAccount } from "model/index";
    /**
     * Provides business logic related to Worker accounts: Workers as Employers see them.
     */
    export class WorkerLogic {
        /**
         * Gets a new blank worker account with default values, suitable for UI binding.
         */
        static getBlank(): WorkerAccount;
        /**
         * Creates a new avatar object based on firstName and lastName.
         * This simulates how server will eventually create an avatar object for a worker.
         * @param firstName First name of the person.
         * @param lastName Last name of the person.
         * @param color Avatar color. If not set, this will later be random generated (in this implementation, a static grayish color).
         */
        static createAvatar(firstName: string, lastName: string, color?: any): {
            color: any;
            displayName: string;
            firstName: string;
            entityType: LegalEntityType;
            initials: string;
            lastName: string;
            pictureType: AvatarPictureType;
            sortableName: string;
        };
        /**
         * Capitalizes the first character of a string.
         * The method is null-safe: Returns null if null, empty if empty.
         * @param text Text for which the first letter is capitalized.
         */
        static capitalizeFirstLetter(text: string): string;
        /**
         * Gets a description text for a WorkerAccount.
         * @param worker Worker account to describe.
         */
        static getDescription(worker: WorkerAccount): any;
        /** Sample workers for anonymous demo user interfaces. */
        static getSampleWorkers(): WorkerAccount[];
        /**
         * Sets default values for the worker based on employment type.
         * @param worker - Worker to which default values are set.
         */
        static setEmploymentDefaultValues(worker: WorkerAccount): void;
        /**
         * Returns options object for PensionCalculation selection by employment relation type.
         * @param employmentRelationType Employment relation type of the worker.
         */
        static getPensionCalculationOptions(employmentRelationType: EmploymentRelationType): Array<{
            value: any;
            text: string;
            title?: string;
        }>;
    }
}
declare module "logic/RoleLogic" {
    import { Role, UserSession } from "model/index";
    import { AppStatus, SystemRole } from "logic/model/index";
    /**
     * Provides business logic for role membership rules.
     */
    export class RoleLogic {
        /**
         * Checks whether the user is in a given role
         * @param session - Session of the current account.
         * @param appStatus - Additional information about the session and application status.
         * @param role - One of the known roles or role from server.
         * You can also use exclamation mark for negative (e.g.)
         * @returns Boolean whether user is in the given role
         */
        static isInRole(session: UserSession, appStatus: AppStatus, role: SystemRole | Role | string): boolean;
        /**
         * Returns true if the current account is a member of all of the given roles.
         * Returns true if the roles is null or an empty array or and an empty string.
         * @param session - Session of the current account.
         * @param appStatus - Additional information about the session and application status.
         * @param roles - Array of role values.
         */
        static isInAllRoles(session: UserSession, appStatus: AppStatus, roles: Array<SystemRole | Role | string>): boolean;
        /**
         * Returns true if the current account is a member of some of the given roles.
         * Returns true if the roles is null or an empty array or an empty string.
         * @param session - Session of the current account.
         * @param roles - Array of role values.
         */
        static isInSomeRole(session: UserSession, appStatus: AppStatus, roles: Array<SystemRole | Role | string>): boolean;
    }
}
declare module "logic/SalaryDateLogic" {
    /**
     * Provides business logic for calculating salary date.
     */
    export class SalaryDateLogic {
        /**
         * Returns true, if the given date is a valid Salary date for given gross salary payment date.
         *
         * @param salaryDate - Salary date (the date when the salary is in worker).
         * @param iban - Bank account number of the worker.
         * @param paidAt - Date for the gross salary payment.
         * @returns - True if the given dates are not conflicting (salary date is possible for given paid at date).
         */
        static isValidSalaryDate(salaryDate: any, iban?: string, paidAt?: any): boolean;
        /**
         * Calculates the estimated salary date (the date when the salary is in worker) for the given gross salary payment date.
         *
         * @param iban  - The iban number of the worker.
         * @param paidAt - Date for the salary gross payment.
         * @param isRequestedSalaryDateSet - Flag to indicate, if the requested salary date is set.
         */
        static CalculateSalaryDate(iban?: string, paidAt?: any, isRequestedSalaryDateSet?: any): string;
        /**
         * Required days to process the incoming payment to worker net payment.
         */
        private static daysCustomerServiceProcessing;
        /**
         * Payment delay if the bank is not a big bank.
         */
        private static daysNonBigBankPaymentDelay;
        /**
         * Payment delay if the  payment takes place outside bank hours.
         */
        private static daysLatePaymentDelay;
        /**
         * Delay needed for the payment transaction date setting for salary.
         */
        private static daysPaymentDueDateDelay;
        /**
         * Maximum time from now to salary date.
         */
        private static daysToSalaryDate;
        private static getWorkdayMoment;
        private static isBigBank;
    }
}
declare module "logic/index" {
    export * from "logic/import/index";
    export * from "logic/model/index";
    export * from "logic/usecases/index";
    export * from "logic/CalcRowsLogic";
    export * from "logic/CalculationResultLogic";
    export * from "logic/CalculatorLogic";
    export * from "logic/ContractLogic";
    export * from "logic/ContractPartyLogic";
    export * from "logic/EnumerationsLogic";
    export * from "logic/HolidaysLogic";
    export * from "logic/IncomeTypesLogic";
    export * from "logic/Occupations";
    export * from "logic/OnboardingLogic";
    export * from "logic/ProductsLogic";
    export * from "logic/ReportsLogic";
    export * from "logic/TaxCard2019Logic";
    export * from "logic/Translations";
    export * from "logic/WorkerLogic";
    export * from "logic/RoleLogic";
    export * from "logic/SalaryDateLogic";
}
declare module "api/Workers" {
    import { WorkerAccount } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access to Workers (employees) created by the employer
     * or to whom the employer has paid salaries.
     */
    export class Workers extends CrudApiBase<WorkerAccount> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): WorkerAccount;
        /** Saves the Worker account ot the storage */
        save(itemToSave: WorkerAccount): Promise<WorkerAccount>;
    }
}
declare module "api/Accounts" {
    import { AccountProducts, CompanyAccount, MessageType, PersonAccount, WorkerAccount } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Methods for updating account information
     */
    export class Accounts {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets the current company account if the current account is a company.
         * NOTE: Typically the account data is fetched in Session check => You do not need to fetch it separately.
         *
         * @returns A Promise with result data (Company account or null).
         */
        getCompany(): Promise<CompanyAccount>;
        /**
         * Gets the current Person account:
         * Either the current account or the Primary contact person account if the current account is a company.
         * NOTE: Typically the current account data is fetched in Session check => You do not need to fetch it separately.
         * => Only use this to fetch the Primary contact person account if the current account is a company.
         *
         * @returns A Promise with result data (Personal account).
         */
        getPerson(): Promise<PersonAccount>;
        /**
         * Gets the current set of products for this account
         *
         * @returns A Promise with result data (AccountProducts).
         */
        getProducts(): Promise<AccountProducts>;
        /**
         * Updates the current set of products for this account
         *
         * @param products - The products object with modified products. You may leave unmodified products as null - they will not be set as null.
         *
         * @returns A Promise with result data (updated products).
         */
        updateProducts(products: AccountProducts): Promise<AccountProducts>;
        /**
         * OBSOLETE: Use separate Workers api instead.
         * @ignore
         */
        getWorkers(): Promise<WorkerAccount[]>;
        /**
         * OBSOLETE: Use separate Workers api instead.
         * @ignore
         */
        updateWorker(worker: any): Promise<WorkerAccount>;
        /**
         * OBSOLETE: Use separate Workers api instead.
         * @ignore
         */
        deleteWorker(accountId: string): Promise<string>;
        /**
         * OBSOLETE: Use separate Workers api instead.
         * @deprecated Returns the url where to post the tax card file
         * @ignore
         */
        getWorkerTaxCardUploadUrl(account: WorkerAccount): string;
        /**
         * OBSOLETE: Use separate Workers api instead.
         * @deprecated Returns the url where to get the tax card file
         * @ignore
         */
        getWorkerTaxCardDownloadUrl(account: WorkerAccount): string;
        /**
         * Switches the current web site usage role.
         *
         * @param role - household or worker. You cannot change to company at the moment using this method.
         *
         * @returns A Promise with result data (new role).
         */
        switchRole(role: "worker" | "household"): Promise<"worker" | "household">;
        /**
         * Sends a verification.
         *
         * @param type - Verification type.
         * @param address - Verification address.
         *
         * @returns A Promise with result data (true if succeeded).
         */
        sendVerification(type: MessageType, address: string): Promise<boolean>;
        /**
         * Confirms a verification.
         *
         * @param type - Verification type.
         * @param pin - Verification pin code.
         *
         * @returns A Promise with result data (true/false).
         */
        confirmVerification(type: MessageType, pin: string): Promise<boolean>;
    }
}
declare module "api/AjaxJQuery" {
    import { Ajax } from "api/Ajax";
    /**
     * Provides wrapper methods for communicating with the Palkkaus.fi API.
     * The raw Ajax-access to the server methods: GET, POST and DELETE
     * with different return types and authentication / error events.
     * This is the JQuery based ajax implementation.
     */
    export class AjaxJQuery implements Ajax {
        /**
         * By default credentials are not used in http-calls.
         * Enable credentials with this flag (force disabled when the token is set).
         */
        useCredentials: boolean;
        /**
         * The server address - root of the server. This is settable field.
         * Will probably be changed to a configuration object in the final version.
         */
        serverAddress: string;
        private token;
        /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
        getServerAddress(): string;
        /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
        getApiAddress(): string;
        /**
         * Gets a JSON-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        getJSON(method: string): Promise<any>;
        /**
         * Gets a HTML-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
         */
        getHTML(method: string): Promise<string>;
        /**
         * POSTS data to server and receives back a JSON-message.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postJSON(method: string, data: any): Promise<any>;
        /**
         * POSTS data to server and receives back HTML.
         *
         * @param method - The API method is the url starting from api version, e.g. '/v02/api'. E.g. '/calculator/new'
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postHTML(method: string, data: any): Promise<string>;
        /**
         * Sends a DELETE-message to server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        remove(method: string): Promise<any>;
        /**
         * Gets the current token.
         * Will check the salaxy-token cookie if the token is persisted there
         */
        getCurrentToken(): string;
        /**
         * Sets the current token. The token is set to cookie called "salaxy-token" or
         * if the HTML page is running from local computer, it is set to local storage.
         *
         * @param token - the authentication token to store.
         */
        setCurrentToken(token: string): void;
        /**
         * Implements the OAuth2 "Resource Owner Password Credentials Grant" flow (RFC6749 4.3).
         * This method is not typically used in production web sites - use Implicit Grant instead for client side JavScript (SPAs).
         * However, it is very useful in development, testing, trusted helpers and server-side scenarios.
         */
        resourceOwnerLogin(username: string, password: string): any;
        private getJQuery;
        private getCookie;
        /** If missing, append the API server address to the given url method string */
        private getUrl;
    }
}
declare module "api/AjaxNode" {
    import { Ajax } from "api/Ajax";
    /**
     * Provides wrapper methods for communicating with the Palkkaus.fi API
     * The request-promise access to the server methods: GET, POST and DELETE
     * with different return types and authentication / error events.
     * This is implementation is the default for server-side running of the Salaxy library code.
     */
    export class AjaxNode implements Ajax {
        /**
         * By default credentials are not used in http-calls.
         * Enable credentials with this flag (force disabled when the token is set).
         */
        useCredentials: boolean;
        /**
         * The server address - root of the server. This is settable field.
         * Will probably be changed to a configuration object in the final version.
         */
        serverAddress: string;
        private rp;
        private token;
        /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
        getServerAddress(): string;
        /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
        getApiAddress(): string;
        /**
         * Gets a JSON-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        getJSON(method: string): Promise<any>;
        /**
         * Gets a HTML-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
         */
        getHTML(method: string): Promise<string>;
        /**
         * POSTS data to server and receives back a JSON-message.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        postJSON(method: string, data: any): Promise<any>;
        /**
         * POSTS data to server and receives back HTML.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        postHTML(method: string, data: any): Promise<string>;
        /**
         * Sends a DELETE-message to server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        remove(method: string): Promise<any>;
        /**
         * Returns current JWT token.
         */
        getCurrentToken(): string;
        /**
         * Sets token.
         * @param token - JWT token.
         */
        setCurrentToken(token: string): void;
        /** If missing, append the API server address to the given url method string */
        private getUrl;
    }
}
declare module "api/AjaxXHR" {
    import { Ajax } from "api/Ajax";
    /**
     * Provides wrapper methods for communicating with the Palkkaus.fi API.
     * The raw Ajax-access to the server methods: GET, POST and DELETE
     * with different return types and authentication / error events.
     * This is the XMLHttpRequest based Ajax implementation.
     */
    export class AjaxXHR implements Ajax {
        /**
         * By default credentials are not used in http-calls.
         * Enable credentials with this flag (force disabled when the token is set).
         */
        useCredentials: boolean;
        /**
         * The server address - root of the server. This is settable field.
         * Will probably be changed to a configuration object in the final version.
         */
        serverAddress: string;
        private token;
        /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
        getApiAddress(): string;
        /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
        getServerAddress(): string;
        /**
         * Gets a JSON-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        getJSON(method: string): Promise<any>;
        /**
         * Gets a HTML-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
         */
        getHTML(method: string): Promise<string>;
        /**
         * POSTS data to server and receives back a JSON-message.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        postJSON(method: string, data: any): Promise<any>;
        /**
         * POSTS data to server and receives back HTML.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        postHTML(method: string, data: any): Promise<string>;
        /**
         * Sends a DELETE-message to server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
         *
         * @returns A Promise with result. Standard Promise rejection to be used for error handling.
         */
        remove(method: string): Promise<any>;
        /**
         * Gets the current token.
         * Will check the salaxy-token cookie if the token is persisted there
         */
        getCurrentToken(): string;
        /**
         * Sets the current token. The token is set to cookie called "salaxy-token" or
         * if the HTML page is running from local computer, it is set to local storage.
         *
         * @param token - the authentication token to store.
         */
        setCurrentToken(token: string): void;
        private getCookie;
        /** If missing, append the API server address to the given url method string */
        private getUrl;
        private createCORSRequest;
    }
}
declare module "api/AuthorizedAccounts" {
    import { Avatar, CompanyAccount, PersonAccount } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own authorized accounts
     */
    export class AuthorizedAccounts extends CrudApiBase<Avatar> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /**
         * Returns an empty avatar without any values.
         */
        getBlank(): Avatar;
        /**
         * Lists all accounts on behalf of which this account has been authorized to act.
         *
         * @returns A Promise with result data (list of account objects).
         */
        getAuthorizingAccounts(): Promise<(PersonAccount | CompanyAccount)[]>;
    }
}
declare module "api/Calculations" {
    import { Calculation } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own calculations
     */
    export class Calculations extends CrudApiBase<Calculation> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): Calculation;
        /**
         * OBSOLETE: Use getSingle() instead.
         * @ignore
         */
        get(id: string): Promise<Calculation>;
        /**
         * OBSOLETE: Use delete() instead.
         * @ignore
         */
        deleteCalc(id: string): Promise<string>;
        /**
         * OBSOLETE: Use save() instead.
         * @ignore
         */
        createOrUpdate(calc: Calculation): Promise<Calculation>;
        /**
         * Gets the Calculation objects for the given ids.
         * @param ids Identifiers for the Calculation objects.
         * @returns A Promise with result data array.
         */
        getMulti(ids: string[]): Promise<Calculation[]>;
    }
}
declare module "api/Calculator" {
    import { AccountingReportTableType, Calculation, CalculationRowType, CalcWorktime, HolidayYear, SalaryKind, UnionPaymentType, WorkerAbsences, YearlyChangingNumbers } from "model/index";
    import { nir } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Calculator is the main module for creating and modifying salary calculations.
     * The module can be used completely anonymously without any authentication.
     */
    export class Calculator {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * OBSOLETE: use CalculatorLogic.getBlank() instead.
         * @deprecated Factory method to create a new calculation on server-side.
         * Typically, you do not need to call server-side factory methods in JavaScript.
         * Instead, you can use client-side factory methods under logic. This saves you one round-trip to server.
         * @ignore
         */
        createNew(): Promise<Calculation>;
        /**
         * Creates a new calucation with simple salary
         *
         * @param kind The type of the salary.
         * @param [amount=1] The amount of typically hours (could be months etc.). Defaults to 1 if left as null.
         * @param price The price of one unit of amount.
         * @param message Message text for end user - used in salary calculation
         * @param calc The calculation object that should be updated. This is optional - often null, but can be set if you are updating an existing calculation.
         *
         * @returns A Promise with the recalculated Calculation.
         */
        calculateSalary(kind: SalaryKind, amount: number, price: number, message: string, calc?: any): Promise<Calculation>;
        /**
         * Creates a new calculation with the Worker as a specified Person
         *
         * @param calc The calculation object that should be updated.
         * @param profileId The identifier of the Profile that should be set as Worker.
         *
         * @returns A Promise with the recalculated Calculation.
         */
        workerSet(calc: Calculation, profileId: string): Promise<Calculation>;
        /**
         * Sets a household deduction to a calculation
         *
         * @param calc The calculation object to which household deduction is added
         * @param setHouseHoldDeductionTo Boolean to which household
         *
         * @returns A Promise with the recalculated Calculation.
         */
        setTaxDeduction(calc: Calculation, setHouseHoldDeductionTo: boolean): Promise<Calculation>;
        /**
         * Takes in a calculation - typically with modified parameters - and recalculates it.
         *
         * @param calc The salary calculation that should be recalculated
         *
         * @returns A Promise with the recalculated Calculation.
         */
        recalculate(calc: Calculation): Promise<Calculation>;
        /**
         * Recalculates a calculation taking into account holiday year and absences from the worktime property.
         * This is an anonymous method that can be used for unit testing: Does not read / write from database.
         * @param calc The salary calculation that should be recalculated.
         * Worktime property should contian the bases for the recalculation.
         *
         * @returns A Promise with the recalculated Calculation.
         */
        recalculateWorktime(calc: Calculation): Promise<Calculation>;
        /**
         * Gets the worktime data for Period recalculation from raw calc, holidays andabsences data.
         * This is an anonymous method that can be used for unit testing: Does not read / write from database.
         *
         * @param calc The salary calculation that should be recalculated.
         * @param holidayYear Holiday year with holidays that should be added as salary rows. If not provided, holidays are not calculated.
         * @param absences Absences that should be deducted from monthly salary. If not provided, absences are not calculated.
         *
         * @returns A Promise with Worktime data.
         */
        getWorktimeData(calc: Calculation, holidayYear?: HolidayYear, absences?: WorkerAbsences): Promise<CalcWorktime>;
        /**
         * Sets a unionPayment to a calculation
         *
         * @param calc The calculation object to which union payment is added
         * @param paymentType Type of union payment, options are 'notSelected', 'raksaNormal', 'raksaUnemploymentOnly' and 'other'
         * @param unionPaymentPercent How many percents the union payment is from the salary
         *
         * @returns A Promise with the recalculated Calculation.
         */
        setUnionPayment(calc: Calculation, paymentType: UnionPaymentType, unionPaymentPercent: number): Promise<Calculation>;
        /**
         * Adds a calculation row: An expenses, benefits or other such row and returns the recalculated calculation with full data.
         *
         * @param calc The salary calculation that should be modified.
         * @param rowType Type of the calculation row that should be added.
         * @param message Message text for end user - used in salary calculation
         * @param price Price for the row (e.g. €/h, €/km, €/month. Default is 0.
         * @param count Count of the items - default is 1.
         *
         * @returns A Promise with the recalculated Calculation.
         */
        addRow(calc: Calculation, rowType: CalculationRowType, message: string, price: number, count: number): Promise<Calculation>;
        /**
         * Deletes a calculation row: Expenses, benefits etc. item and returns the recalculated calculation with full data.
         *
         * @param calc The salary calculation that should be modified.
         * @param rowIndex Zero based row index of the row that should be deleted.
         *
         * @returns A Promise with the recalculated Calculation.
         */
        deleteRow(calc: Calculation, rowIndex: number): Promise<Calculation>;
        /**
         * Gets salary calculation parameters that are changing yearly.
         * The method is designed for the end-of-year and as such it only supports 2 years:
         * the current / previous year (from Jan to Nov) OR current / next year (in approx. December).
         *
         * @param forDate Date for which the numbers are fetched.
         *
         * @returns The yearly changing numbers if year supported. Error if year not supported.
         */
        getYearlyChangingNumbers(forYear: Date | string): YearlyChangingNumbers;
        /**
         * Returns a new Finvoice object for the given calculations.
         * @param calculations Calculations for the Finvoice.
         * @param tableType Accounting table type for the generated Finvoice.
         * @ignore experimental
         */
        getFinvoice(calculations: Calculation[], tableType?: AccountingReportTableType): Promise<any>;
        /**
         * Returns a new  earnings payment report content for the given calculation.
         * @param calculation Calculation for the earnings payment report generation.
         * @ignore experimental
         */
        getEarningsPayment(calculation: Calculation): Promise<nir.EarningsPayment>;
    }
}
declare module "api/Certificates" {
    import { Certificate } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own Certificates
     */
    export class Certificates extends CrudApiBase<Certificate> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /**
         * Returns an empty certificate without any values.
         */
        getBlank(): Certificate;
    }
}
declare module "api/Cms" {
    import { AbcSection, Article } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Read-only access to CMS Articles and other documentation
     */
    export class Cms {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets articles from the server
         *
         * @param filter is one of the ABC-section categories. Use null to list all the articles.
         *
         * @returns A Promise with result data (Article[]).
         */
        getArticles(filter: AbcSection): Promise<Article[]>;
        /**
         * Gets an article from the server
         *
         * @param id Identifier for the article
         * @param articleLinkPattern Optional pattern for CMS links from one article to another.
         *        The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         *        The caller can change this to direct internal links to appropriate page template.
         *
         * @returns A Promise with result data (Article).
         */
        getArticle(id: string, articleLinkPattern?: string): Promise<Article>;
    }
}
declare module "api/Contacts" {
    import { ContractParty } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own Contacts
     */
    export class Contacts extends CrudApiBase<ContractParty> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): ContractParty;
        /**
         * Gets all the ContractParty Worker objects that the current account can access
         *
         * @returns A Promise with result data (array of ContractParty).
         */
        getWorkers(): Promise<ContractParty[]>;
        /**
         * Gets all the ContractParty Employer objects that the current account can access
         *
         * @returns A Promise with result data (array of ContractParty).
         */
        getEmployers(): Promise<ContractParty[]>;
    }
}
declare module "api/Contracts" {
    import { EmploymentContract } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own Employment Contracts
     */
    export class Contracts extends CrudApiBase<EmploymentContract> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): EmploymentContract;
        /**
         * Gets a contract preview for a given contract ID (contract must be saved).
         *
         * @param contractId - The Identifier of the contract
         *
         * @returns A Promise with preview HTML.
         */
        getPreviewById(contractId: any): Promise<string>;
        /**
         * Gets a contract preview for a given contract.
         *
         * @param contract - The Contract for which the contract is rendered
         *
         * @returns A Promise with preview HTML.
         */
        getPreview(contract: any): Promise<string>;
    }
}
declare module "api/EmailMessages" {
    import { EmailMessage } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own EmailMessages
     */
    export class EmailMessages extends CrudApiBase<EmailMessage> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): EmailMessage;
    }
}
declare module "api/Files" {
    import { BlobFile, BlobFileType } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Provides access to user files (of the authenticated user).
     */
    export class Files {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets a list of files - filtered by a type
         *
         * @param type: Logical file type.
         *
         * @returns A Promise with result data (list of files).
         */
        filesOfType(type: BlobFileType): Promise<BlobFile[]>;
    }
}
declare module "api/OAuth2" {
    import { OAuthMessage } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Methods for oauth2 communication.
     */
    export class OAuth2 {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Token endpoint.
         *
         * @param msg - OAuth 2.0 authorization request.
         *
         * @returns A Promise with OAuthMessage.
         */
        token(msg: OAuthMessage): Promise<OAuthMessage>;
        /**
         * Gets a federation message for the currently signed-in account as OpenID Connect UserInfo Response
         * (http://openid.net/specs/openid-connect-core-1_0.html#UserInfo).
         * This message can be used by trusted authentication providers (mainly Banks) to federate authentication and authorization information to Salaxy.
         * In such a flow this service end-point provides an example that could be used in testing.
         *
         * @returns A Promise with federation message {OpenIdUserInfoSupportedClaims}.
         */
        getFederationMessage(): Promise<any>;
    }
}
declare module "api/Onboardings" {
    import { Onboarding } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Provides functionality for Onboarding:
     * The process where the user becomes a user of Salaxy service and an account is created for him / her / the company.
     */
    export class Onboardings {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets a single Onboarding object based on identier
         *
         * @param id - Unique identifier for the object
         *
         * @returns A Promise with result data (Onboarding).
         */
        getOnboarding(id: string): Promise<Onboarding>;
        /**
         * Gets a single Onboarding object for authorizing account
         *
         * @param accountId - Account id of the authorizing account.
         *
         * @returns A Promise with result data (Onboarding).
         */
        getOnboardingForAccount(accountId: string): Promise<Onboarding>;
        /**
         * Creates or updates an Onboarding.
         *
         * @param onboarding - The Onboarding object that is updated.
         *
         * @returns A Promise with result data (saved Onboarding).
         */
        saveOnboarding(onboarding: Onboarding): Promise<Onboarding>;
        /**
         * Deletes an onboarding object from the storage
         *
         * @param id - Unique identifier for the object
         *
         * @returns A Promise with result data "Object deleted".
         */
        deleteOnboarding(id: string): Promise<string>;
        /**
         * Sends a new pin code to the telephone number to be verified in onboarding.
         *
         * @param onboarding - The Onboarding object which contains the telephone number.
         *
         * @returns A Promise with result data (true if sending succeeded).
         */
        sendSmsVerificationPin(onboarding: Onboarding): Promise<boolean>;
        /**
         * Commits onboarding and creates/updates the account.
         *
         * @param onboarding - The Onboarding object that is to be committed..
         *
         * @returns A Promise with result data (committed Onboarding).
         */
        commitOnboarding(onboarding: Onboarding): Promise<Onboarding>;
    }
}
declare module "api/Payments" {
    import { AccountingReportTableType, InstantPaymentSaldo, Payment, PaymentMethod, PaymentStatus } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Methods for payment creation and processing.
     */
    export class Payments extends CrudApiBase<Payment> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): Payment;
        /**
         * List payments for the current user.
         *
         * @param status - Payment status filter. If null or empty, returns all statuses.
         *
         * @returns A Promise with result data (list of payments).
         */
        getPayments(status: PaymentStatus[]): Promise<Payment[]>;
        /**
         * Gets a single payment object based on identifier.
         *
         * @param id - Unique identifier for the payment.
         *
         * @returns A Promise with result data (Payment).
         */
        getPayment(id: string): Promise<Payment>;
        /**
         * Deletes a payment.
         *
         * @param id - Unique identifier for the payment.
         *
         * @returns A Promise with result data "Object deleted".
         */
        deletePayment(id: string): Promise<string>;
        /**
         * OBSOLETE
         * @deprecated Creates a new SalaryGrossPayment (startPayment will replace this method).
         * @ignore
         *
         * @param calculationIds - Array of calculation ids.
         * @param successUrl - Return address after successful payment
         * @param cancelUrl - Return address after cancelled payment
         * @param sendEmails - Boolean indicating whether to send emails in the workflow
         *
         * @returns A Promise with result data (created Payment).
         */
        createSalaryGrossPayment(calculationIds: string[], successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payment>;
        /**
         * Creates/updates the SalaryGrossPayment.
         * If a payment for the same calculation ids exist, it will be updated and no new payment
         * is created.
         *
         * @param calculationIds - Array of calculation ids.
         * @param paymentMethod - Payment method.
         * @param successUrl - Return address after successful payment
         * @param cancelUrl - Return address after cancelled payment
         * @param sendEmails - Boolean indicating whether to send emails in the workflow
         *
         * @returns A Promise with result data (created Payment).
         */
        startPayment(calculationIds: string[], paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payment>;
        /**
         * Creates a new InstantPayment.
         *
         * @param calculationId - Calculation (id) which the instant payment is requested for.
         * @param method - Payment method.
         * @param sum - The amount of the advance payment.
         *
         * @returns A Promise with result data (created Payment).
         */
        createInstantPayment(calculationId: string, method: PaymentMethod, sum: number): Promise<Payment>;
        /**
         * Returns the full and the remaining payable worker net salary amount in the calculation.
         *
         * @param calculationId - Calculation (id) which the saldo is requested for.
         *
         * @returns A Promise with result data (InstantPaymentSaldo).
         */
        getInstantPaymentSaldo(calculationId: string): Promise<InstantPaymentSaldo>;
        /**
         * Gets a new finvoice object for the given payment.
         * @param id - The payment id which the finvoice will be returned for.
         * @param tableType - Accounting table type for the generated Finvoice.
         * @ignore - experimental
         */
        getFinvoice(id: string, tableType?: AccountingReportTableType): Promise<any>;
        /**
         * Sends the finvoice object to operator.
         * @param finvocie - Finvoice for the delivery.
         *
         * @returns Finvoice which was sent.
         * @ignore - experimental
         */
        sendFinvoice(finvoice: any): Promise<any>;
        /**
         * Returns the url for starting the payment process for the payroll.
         * @param calculationIds - Array of calculation ids.
         * @param paymentMethod - Payment method.
         * @param successUrl - Return address after successful payment
         * @param cancelUrl - Return address after cancelled payment
         * @param sendEmails - Boolean indicating whether to send emails in the workflow
         * @returns The url to the Paytrail payment page.
         */
        getPaytrailPaymentUrl(calculationIds: string[], paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): string;
    }
}
declare module "api/Profiles" {
    import { ApiProfileSearchResult, City, Contact, Profile, ProfileJob, ProfileJobCategory } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Anonymous methods for accessing the Public Profiles (Osaajaprofiilit).
     * This is read-only access to third-party sites.
     * For modifying and non-public profiles, use MyProfiles autenticated service instead.
     */
    export class Profiles {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets the Cities in Finland. Use the key-property when searching for Profiles
         *
         * @returns A Promise with result data (list of cities).
         */
        getCities(): Promise<City[]>;
        /**
         * Gets the Jobs that are defined for Profiles. Use the id-property when searching for Profiles
         * This method may later provide different Job categorizations for different purposes (e.g. different partners).
         *
         * @returns A Promise with result data (list of jobs).
         */
        getJobs(): Promise<ProfileJob[]>;
        /**
         * Gets the Job Categories that are defined for Profiles. Use the id-property when searching for Profiles
         * This method may later provide different Job categorizations for different purposes (e.g. different partners).
         *
         * @returns A Promise with result data (list of categories).
         */
        getCategories(): Promise<ProfileJobCategory[]>;
        /**
         * Gets a single profile - including the details.
         * Note that this unauthenticated method does not show contact details.
         *
         * @param id - The identifier of the profile.
         *
         * @returns A Promise with result data (Profile).
         */
        getProfile(id: string): Promise<Profile>;
        /**
         * Search method for the public profiles.
         *
         * @param city - Filter by city key
         * @param job - Filter by Job key
         * @param category - Filter by job category key
         * @param searchText - Filter by search text
         *
         * @returns A Promise with result data (search results object).
         */
        search(city: string, job: string, category: string, searchText: string): Promise<ApiProfileSearchResult>;
        /**
         * Gets the profile for the current authenticated user
         *
         * @returns A Promise with result data (Profile).
         */
        getMyProfile(): Promise<Profile>;
        /**
         * Updates user's profile or creates a new one if none exists.
         *
         * @param profile - The profile information that should be updated
         *
         * @returns A Promise with result data (Profile).
         */
        saveMyProfile(profile: Profile): Promise<Profile>;
        /**
         * Returns the url where to post the avatar image file
         */
        getAvatarImageUploadUrl(): string;
        /**
         * Gets a contact for a single profile.
         *
         * @param id - The identifier of the profile.
         *
         * @returns A Promise with result data (Contact).
         */
        getContact(id: string): Promise<Contact>;
    }
}
declare module "api/Reports" {
    import { AccountingReport, AccountingReportTable, AccountingReportTableType, Calculation, PeriodType, Report, ReportType } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Report types that are available for calculations as stand-alone (typically pop-up dialog)
     */
    export type calcReportType = "salarySlip" | "employerReport" | "paymentReport";
    /**
     * Report partials that are available as HTML for developers to create their own
     * reporting views
     */
    export type reportPartial = "employer" | "employerPage2" | "worker" | "workerPage2" | "payments";
    /**
     * Provides access to HTML reports on calculations
     */
    export class Reports {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Gets a list of generated HTML/PDF/JSON reports by type.
         *
         * @param reportType - Type of reports to fetch.
         *
         * @returns A Promise with result data (array of reports).
         */
        getReportsByType(reportType: ReportType): Promise<Report[]>;
        /**
         * Gets a single generated HTML/PDF/JSON report by type and id.
         *
         * @param reportType - Type of the report to fetch.
         * @param id - Id of the report to fetch.
         * @param wait - Optional flag to indicate whether to wait for the report to be generated.
         *
         * @returns A Promise with result data (report).
         */
        getReport(reportType: ReportType, id: string, wait: boolean): Promise<Report>;
        /**
         * Gets the monthly / yearly accounting data for the current account.
         *
         * @param refDate - Reference date (first day of period).
         * @param includeCalculations - If true, includes the individual calculations.
         * This makes the resulting file larger, but is required for many detailed calculations.
         * @param forEntireYear - If true, fetches the data for the whole year.
         * Default is one month.
         * @returns A Promise with result data (Raw data for accounting purposes).
         */
        getAccountingData(refDate: string, includeCalculations?: boolean, forEntireYear?: boolean): Promise<AccountingReport>;
        /**
         * Gets the accounting report based on given set of calculations and Current user.
         * ANON: This method can be accessed anonymously for testing / development purposes.
         * In this case, last valid calculation employer is set as employer avatar,
         * but other employer information (BusinessId, Contact)  is not filled in.
         *
         * @param calculations - Calculations that are the bases for the report.
         * @returns A Promise with result data (AccountingReport).
         */
        getAccountingDataForCalcs(calculations: Calculation[]): Promise<AccountingReport>;
        /**
         * Gets an HTML report based on Calculation ID.
         *
         * @param reportType - Type of the report to fetch. See the HtmlReportType enumeration for possible values.
         * @param calculationId - GUID for the calculation
         *
         * @returns A Promise with result HTML.
         */
        getReportHtmlById(reportType: reportPartial, calculationId: string): Promise<string>;
        /**
         * Gets an HTML report based on posted Calculation object.
         *
         * @param reportType - Type of the report to fetch. See the HtmlReportType enumeration for possible values.
         * @param calculation - The calculation object that is reported.
         *
         * @returns A Promise with result HTML.
         */
        getReportHtmlWithPost(reportType: reportPartial, calculation: any): Promise<string>;
        /**
         * Gets the monthly / quarterly/ yearly accounting data for the current account.
         *
         * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
         * @param tableType  - Accounting table type.
         * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
         * @param endDate - End date for the period. Required only for the custom period.
         * @returns A Promise with result data (Raw data for accounting purposes).
         */
        getAccountingReportTableForPeriod(refDate: string, tableType?: AccountingReportTableType, periodType?: PeriodType, endDate?: string): Promise<AccountingReportTable>;
        /**
         * Gets the accounting report based on given set of calculations.
         *
         * @param calculationIds - Calculations that are the bases for the report.
         * @param tableType  - Accounting table type.
         * @returns Account report based on the calculations.
         * @ignore - experimental
         */
        getAccountingReportTableForCalculationIds(calculationIds: string[], tableType?: AccountingReportTableType): Promise<AccountingReportTable>;
        /**
         * Gets the accounting report based on given set of calculations.
         *
         * @param calculations - Calculations that are the bases for the report.
         * @param tableType  - Accounting table type.
         * @returns Account report based on the calculations.
         * @ignore - experimental
         */
        getAccountingReportTableForCalculations(calculations: Calculation[], tableType?: AccountingReportTableType): Promise<AccountingReportTable>;
    }
}
declare module "api/Session" {
    import { AccountBase } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Provides read-only access to the current user session
     */
    export class Session {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Checks whether the current browser session is authorized.
         *
         * @returns A Promise with result data (boolean).
         */
        isAuthorized(): Promise<boolean>;
        /**
         * Gets the current session information or null if there is no active session.
         * UserSession data is the Authorization object that provides information about the current session. Null, if there is no session.
         *
         * @returns A Promise with result data (UserSession).
         */
        getSession(): Promise<AccountBase>;
        /**
         * Returns server address
         */
        getServerAddress(): string;
    }
}
declare module "api/SmsMessages" {
    import { SmsMessage } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own SmsMessage
     */
    export class SmsMessages extends CrudApiBase<SmsMessage> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): SmsMessage;
    }
}
declare module "api/Taxcards" {
    import { Taxcard, TaxCardResult } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD functionality as well as business logic for storing and using Finnish Taxcards and calculating Withholding tax.
     */
    export class Taxcards extends CrudApiBase<Taxcard> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): Taxcard;
        /**
         * Calculates the widhholding tax based on a given tax card.
         *
         * @param id - Identifier of the tax card.
         * @param income - New salary or other income, for which the widthholding tax is calculated.
         * @param endDate - End date for the salary payment period (salary that is calulated now). This is significant for the validity and cumulation of the tax card.
         *
         * @returns A Promise with result data (TaxCardResult).
         */
        calculateWithId(id: string, income: number, endDate: Date): Promise<TaxCardResult>;
        /**
         * Calculates the widhholding tax based on a given tax card
         *
         * @param income - New salary or other income, for which the widthholding tax is calculated.
         * @param taxCard - Tax card based on which the widthholding tax is calculated. This method will not connect to database in any way so the taxcard may or may not be stored.
         * @param endDate - End date for the salary payment period (salary that is calulated now). This is significant for the validity and cumulation of the tax card.
         *
         * @returns A Promise with result data (TaxCardResult).
         */
        calculate(income: number, taxCard: Taxcard, endDate: Date): Promise<TaxCardResult>;
        /**
         * Gets the current / latest Taxcard for a person based on Personal ID (HETU)
         *
         * @param personalId - Personal ID of the Worker. Note, that this is Finnish Personal ID (HeTu/SoTu), not a Salaxy ID.
         *
         * @returns A Promise with result data (Taxcard).
         */
        getCurrentTaxCard(personalId: string): Promise<Taxcard>;
        /**
         * Returns the url where to post the tax card file
         */
        getTaxCardUploadUrl(): string;
        /**
         * Returns the url where to get the tax card file
         */
        getTaxCardDownloadUrl(taxCard: Taxcard): string;
        /**
         * Returns the url where to get the tax card preview image
         */
        getTaxCardPreviewUrl(taxCard: Taxcard): string;
        /**
         * Gets the Taxcard objects for the given ids.
         * @param ids Identifiers for the Taxcard objects.
         * @returns A Promise with result data array.
         */
        getMulti(ids: string[]): Promise<Taxcard[]>;
    }
}
declare module "api/Test" {
    import { ApiTestErrorType, Onboarding, TestAccountInfo, TestValues } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * Methods for testing the API.
     */
    export class Test {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Basic hello world message. Echoes the given message back.
         *
         * @param message - Message.
         *
         * @returns A Promise with result data (message).
         */
        hello(message: string): Promise<string>;
        /**
         * Test a long lasting request.
         *
         * @param seconds - Delay in seconds.
         *
         * @returns A Promise with result data (Delay in seconds).
         */
        delay(seconds: number): Promise<number>;
        /**
         * Test JSON serialization. Echoes a given object either in success callback or in the promise.
         *
         * @param testValues - TestValues object.
         *
         * @returns A Promise with result.
         */
        values(testValues: TestValues): Promise<TestValues>;
        /**
         * Creates a test account and returns test account details.
         *
         * @param testValues - Onboarding object.
         *
         * @returns A Promise with result data (test accounts).
         */
        createTestAccount(onboarding: Onboarding): Promise<TestAccountInfo>;
        /**
         * Deletes a test account and related objects
         *
         * @param accountId - Account id to delete.
         *
         * @returns A Promise with result data "Object deleted".
         */
        deleteTestAccount(accountId: string): Promise<string>;
        /**
         * Test methods that throws an exception on server side.
         *
         * @param errorType Type of error
         * @param success Function that would theoretically be called if the call was not an error. Will never be called.
         *
         * @returns A Promise if success parameter is null. For throwException,
         *        the promise is always rejected after the error is thrown in the server.
         */
        throwException(errorType: ApiTestErrorType): Promise<string>;
        /**
         * Delete the current account data and credentials (including Auth0 user).
         * Can be called only in a test environment.
         */
        deleteCurrentAccount(): Promise<string>;
        /**
         * Remove all calculations, workers etc. user objects except products and signature from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountData(): Promise<string>;
        /**
         * Remove all calculations, payrolls and payments from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountCalculations(): Promise<string>;
        /**
         * Remove workers including calculations, employment contracts and tax cards from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountWorkers(): Promise<string>;
        /**
         * Remove all holiday year from all workers. Does not touch the default values of holidays in Worker Employment relation.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountHolidays(): Promise<string>;
        /**
         * Remove pension and insurance from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountPensionAndInsurance(): Promise<string>;
        /**
         * Remove all products from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountProducts(): Promise<string>;
        /**
         * Remove the signature from the account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountSignature(): Promise<string>;
        /**
         * Delete all empty accounts (company or worker) created by this account.
         * Can be called only in a test environment.
         */
        deleteCurrentAccountAuthorizingAccounts(): Promise<string>;
    }
}
declare module "api/Client" {
    import { Accounts } from "api/Accounts";
    import { Ajax } from "api/Ajax";
    import { Calculations } from "api/Calculations";
    import { Calculator } from "api/Calculator";
    import { Cms } from "api/Cms";
    import { Contacts } from "api/Contacts";
    import { Contracts } from "api/Contracts";
    import { EmailMessages } from "api/EmailMessages";
    import { Files } from "api/Files";
    import { OAuth2 } from "api/OAuth2";
    import { Onboardings } from "api/Onboardings";
    import { Payments } from "api/Payments";
    import { Profiles } from "api/Profiles";
    import { Reports } from "api/Reports";
    import { Session } from "api/Session";
    import { Taxcards } from "api/Taxcards";
    import { Test } from "api/Test";
    import { Workers } from "api/Workers";
    /**
     * Client to Salaxy API.
     * Abstracts the raw REST api to async methods that are easier to call from TypeScript / JavaScript.
     * Differnet platforms (NG1, NG, jQuery etc.) create derived Clients typically at least by injecting their own Ajax implementation,
     * potentially also by overriding other methods.
     */
    export abstract class Client {
        private _ajax;
        private _accountsApi;
        private _calculationsApi;
        private _calculatorApi;
        private _reportsApi;
        private _filesApi;
        private _sessionApi;
        private _cmsApi;
        private _profilesApi;
        private _paymentsApi;
        private _oAuth2Api;
        private _testApi;
        private _taxcardsApi;
        private _onboardingsApi;
        private _emailMessagesApi;
        private _smsMessagesApi;
        private _contactsApi;
        private _contractsApi;
        private _workersApi;
        constructor(ajaxImpl: Ajax);
        /** The platform (NG1, NG, jQuery etc.) dependent class that performs the actual Ajax calls */
        readonly ajax: Ajax;
        /** Gets the Accounts API: Access to current account, its products and workers */
        readonly accountsApi: Accounts;
        /** Gets the Calculations API: CRUD operations on calculations */
        readonly calculationsApi: Calculations;
        /** Gets the Calculator API: Anonymous salary calculation methods */
        readonly calculatorApi: Calculator;
        /** Gets the Reports API: Viewing and generating reports in different formats */
        readonly reportsApi: Reports;
        /** Gets the Files API: Files stored by the user, thumbnails of pictures etc. */
        readonly filesApi: Files;
        /** Gets the Session API: Access to current session, user and roles */
        readonly sessionApi: Session;
        /** Gets the Content Management API: Articles about salary payment and employment in general. */
        readonly cmsApi: Cms;
        /** Gets the Profiles API: Worker profile (Osaajaprofiili) */
        readonly profilesApi: Profiles;
        /** Gets the Payments API for paying salaries using different methods */
        readonly paymentsApi: Payments;
        /** Gets the OAuth2 API: Authentication methods */
        readonly oAuth2Api: OAuth2;
        /** Gets the Test API: Testing different functionalities of the API and system in general. */
        readonly testApi: Test;
        /** Gets the Tax cards API */
        readonly taxcardsApi: Taxcards;
        /** Gets the Workers API */
        readonly workersApi: Workers;
        /** Gets the Onboarding API for creating a new user */
        readonly onboardingsApi: Onboardings;
        /** Gets the E-mail messages API for communicating with customer services and other users / accounts. */
        readonly emailMessagesApi: EmailMessages;
        /** Gets the SMS API, mainly for receiving SMS messages. */
        readonly smsMessagesApi: any;
        /** Gets the Contacts API: Contacts related to employment and salary payment. */
        readonly contactsApi: Contacts;
        /** Gets the API for Employment contracts. */
        readonly contractsApi: Contracts;
    }
}
declare module "api/Credentials" {
    import { SessionUserCredential } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access all credentials which can access this account.
     */
    export class Credentials extends CrudApiBase<SessionUserCredential> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /**
         * Returns an empty credential without any values.
         */
        getBlank(): SessionUserCredential;
    }
}
declare module "api/EarningsPayments" {
    import { nir } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own Earnings Payments objects
     */
    export class EarningsPayments extends CrudApiBase<nir.EarningsPayment> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: any;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): nir.EarningsPayment;
        /**
         * Validates an Earnings Payment object for delivery.
         * @param earningsPaymentToValidate Earnings payment that should be validated.
         * @returns The Earnings Payment object with validation.
         */
        validate(earningsPaymentToValidate: nir.EarningsPayment): Promise<nir.EarningsPayment>;
    }
}
declare module "api/ESalaryPayments" {
    import { ESalaryPayment } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own E-salary payments
     */
    export class ESalaryPayments extends CrudApiBase<ESalaryPayment> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): ESalaryPayment;
    }
}
declare module "api/HolidayYears" {
    import { HolidayYear } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for Holiday Years of a Worker
     */
    export class HolidayYears extends CrudApiBase<HolidayYear> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): HolidayYear;
        /**
         * Initializes (or re-creates) the holiday years for a Worker.
         * @param workerId Identifier of the Worker to initialize
         * @returns All the holiday years (for other Workers as well) as saved into the database.
         */
        initWorker(workerId: string): Promise<HolidayYear[]>;
        /**
         * Gets the Holiday Year objects for a specific Worker.
         * @returns A Promise with result data array.
         */
        getForWorker(workerId: string): Promise<HolidayYear[]>;
        /**
         * Gets the Holiday Year objects for the given ids.
         * @param ids Identifiers for the Holiday Year objects.
         * @returns A Promise with result data array.
         */
        getMulti(ids: string[]): Promise<HolidayYear[]>;
    }
}
declare module "api/PartnerServices" {
    import { IfInsuranceOrder, VarmaPensionOrder } from "model/index";
    import { Ajax } from "api/Ajax";
    /**
     * API for partner services, like pension, insurance and health care.
     */
    export class PartnerServices {
        private ajax;
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(ajax: Ajax);
        /**
         * Returns a new Varma pension order with default values for the current account.
         */
        getNewVarmaPensionOrder(): Promise<VarmaPensionOrder>;
        /**
         * Validate the given Varma pension order in the server.
         */
        validateVarmaPensionOrder(order: VarmaPensionOrder): Promise<VarmaPensionOrder>;
        /**
         * Send a new Varma pension order for later processing.
         */
        sendVarmaPensionOrder(order: VarmaPensionOrder): Promise<VarmaPensionOrder>;
        /**
         * Returns a new If insurance order with default values for the current account.
         */
        getNewIfInsuranceOrder(): Promise<IfInsuranceOrder>;
        /**
         * Validate the given If insurance order in the server.
         */
        validateIfInsuranceOrder(order: IfInsuranceOrder): Promise<IfInsuranceOrder>;
        /**
         * Send a new If insurance order for later processing.
         */
        sendIfInsuranceOrder(order: IfInsuranceOrder): Promise<IfInsuranceOrder>;
    }
}
declare module "api/Payrolls" {
    import { AccountingReportTableType, PaymentMethod, Payroll } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Provides CRUD access for authenticated user to access a his/her own Payroll objects
     */
    export class Payrolls extends CrudApiBase<Payroll> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
        getBlank(): Payroll;
        /**
         * Validates a Payroll object for payment.
         * @param payrollToValidate Payroll that should be validated.
         * @returns The Payroll object with Calculations added to the Result object and Validated.
         */
        validate(payrollToValidate: Payroll): Promise<Payroll>;
        /**
         * Starts the payment process for the payroll.
         * Creates and saves the calculations as well as the new payment object.
         * Updates the status of calculations and payroll to paymentStarted.
         * @param payroll Payroll which is going to be paid.
         * @param paymentMethod - Payment method.
         * @param successUrl - Return address after successful payment
         * @param cancelUrl - Return address after cancelled payment
         * @param sendEmails - Boolean indicating whether to send emails in the workflow
         * @returns The Payroll object with payment id.
         */
        startPayment(payroll: Payroll, paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payroll>;
        /**
         * Cancels the payment process for the payroll.
         * Deletes the saved calculations as well as the payment object.
         * Updates the status of payroll to paymentCanceled.
         * @param payroll The id of the payroll which is going to be cancelled (payment).
         * @returns The Payroll object.
         */
        cancelPayment(id: string): Promise<Payroll>;
        /**
         * Returns a new Finvoice object for the given payroll.
         * @param payroll - Payroll for the Finvoice.
         * @param tableType - Accounting table type for the generated Finvoice.
         * @ignore - experimental
         */
        getFinvoice(payroll: Payroll, tableType?: AccountingReportTableType): Promise<any>;
        /**
         * Returns the url for starting the payment process for the payroll.
         * @param payrollId Payroll which is going to be paid.
         * @param paymentMethod - Payment method.
         * @param successUrl - Return address after successful payment
         * @param cancelUrl - Return address after cancelled payment
         * @param sendEmails - Boolean indicating whether to send emails in the workflow
         * @returns The url to the Paytrail payment page.
         */
        getPaytrailPaymentUrl(payrollId: string, paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): string;
    }
}
declare module "api/YearEnd" {
    import { YearlyFeedback } from "model/index";
    import { Ajax } from "api/Ajax";
    import { CrudApiBase } from "api/CrudApiBase";
    /**
     * Methods for getting and saving yearly feedback data.
     */
    export class YearEnd extends CrudApiBase<YearlyFeedback> {
        /**
         * For NG1-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base URL for details etc. */
        protected baseUrl: string;
        constructor(ajax: Ajax);
        /**
         * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
         * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
         * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
         * Strings, dates and booleans may be null (false if bool), but typically just undefined.
         */
        getBlank(): YearlyFeedback;
        /**
         * Gets a single item based on identier
         * @param id - Unique identifier for the object
         * @returns A Promise with result data.
         */
        getSingle(id: string): Promise<YearlyFeedback>;
        /**
         * Deletes an single item from the sotrage
         * @param id - Unique identifier for the object
         * @returns A Promise with result data "Object deleted".
         */
        delete(id: string): Promise<string>;
    }
}
declare module "api/index" {
    export * from "api/Absences";
    export * from "api/Accounts";
    export * from "api/Ajax";
    export * from "api/AjaxJQuery";
    export * from "api/AjaxNode";
    export * from "api/AjaxXHR";
    export * from "api/AuthorizedAccounts";
    export * from "api/Calculations";
    export * from "api/Calculator";
    export * from "api/Certificates";
    export * from "api/Client";
    export * from "api/Cms";
    export * from "api/Contacts";
    export * from "api/Contracts";
    export * from "api/Credentials";
    export * from "api/CrudApiBase";
    export * from "api/EarningsPayments";
    export * from "api/EmailMessages";
    export * from "api/ESalaryPayments";
    export * from "api/Files";
    export * from "api/HolidayYears";
    export * from "api/OAuth2";
    export * from "api/Onboardings";
    export * from "api/PartnerServices";
    export * from "api/Payments";
    export * from "api/Payrolls";
    export * from "api/Profiles";
    export * from "api/Reports";
    export * from "api/Session";
    export * from "api/SmsMessages";
    export * from "api/Taxcards";
    export * from "api/Test";
    export * from "api/Workers";
    export * from "api/YearEnd";
}
declare module "i18n/index" {
    export * from "i18n/dictionary";
}
declare module "namespace/exports" {
    export * from "api/index";
    export * from "codegen/index";
    export * from "i18n/index";
    export * from "logic/index";
    export * from "util/index";
    export * from "model/index";
}
declare module "namespace/namespace" {
    import * as core from "namespace/exports";
    global {
        namespace salaxy {
            namespace api {
                export import Accounts = core.Accounts;
                export import Ajax = core.Ajax;
                export import AjaxJQuery = core.AjaxJQuery;
                export import AjaxNode = core.AjaxNode;
                export import AjaxXHR = core.AjaxXHR;
                export import AuthorizedAccounts = core.AuthorizedAccounts;
                export import Calculations = core.Calculations;
                export import Calculator = core.Calculator;
                export import Certificates = core.Certificates;
                export import Client = core.Client;
                export import Cms = core.Cms;
                export import Contacts = core.Contacts;
                export import Contracts = core.Contracts;
                export import Credentials = core.Credentials;
                export import EmailMessages = core.EmailMessages;
                export import ESalaryPayments = core.ESalaryPayments;
                export import Files = core.Files;
                export import OAuth2 = core.OAuth2;
                export import Onboardings = core.Onboardings;
                export import Payments = core.Payments;
                export import Payrolls = core.Payrolls;
                export import Profiles = core.Profiles;
                export import Reports = core.Reports;
                export import Session = core.Session;
                export import SmsMessages = core.SmsMessages;
                export import Taxcards = core.Taxcards;
                export import Test = core.Test;
                export import Workers = core.Workers;
                export import calcReportType = core.calcReportType;
                export import reportPartial = core.reportPartial;
            }
            namespace codegen {
                export import accountProductDefaults = core.accountProductDefaults;
                export import generatedEnumData = core.generatedEnumData;
                export import housholdRows = core.housholdRows;
                export import householdUsecaseTree = core.householdUsecaseTree;
            }
            namespace logic {
                export import CalculatorLogic = core.CalculatorLogic;
                export import ContractLogic = core.ContractLogic;
                export import ContractPartyLogic = core.ContractPartyLogic;
                export import EnumerationsLogic = core.EnumerationsLogic;
                export import ImportLogic = core.ImportLogic;
                export import Occupations = core.Occupations;
                export import OnboardingLogic = core.OnboardingLogic;
                export import ProductsLogic = core.ProductsLogic;
                export import ReportsLogic = core.ReportsLogic;
                export import RoleLogic = core.RoleLogic;
                export import TaxCard2019Logic = core.TaxCard2019Logic;
                export import Translations = core.Translations;
                export import WorkerLogic = core.WorkerLogic;
                namespace model {
                    export import AppStatus = core.AppStatus;
                    export import CalcRowConfig = core.CalcRowConfig;
                    export import CalculationRowCategories = core.CalculationRowCategories;
                    export import CalculationRowCategory = core.CalculationRowCategory;
                    export import Occupation = core.Occupation;
                    export import Product = core.Product;
                    export import SystemRole = core.SystemRole;
                }
                namespace usecases {
                    export import Usecase = core.Usecase;
                    export import UsecaseChildCare = core.UsecaseChildCare;
                    export import UsecaseCleaning = core.UsecaseCleaning;
                    export import UsecaseConstruction = core.UsecaseConstruction;
                    export import UsecaseGroup = core.UsecaseGroup;
                    export import UsecasesLogic = core.UsecasesLogic;
                }
            }
            namespace util {
                export import Arrays = core.Arrays;
                export import Brand = core.Brand;
                export import Cookies = core.Cookies;
                export import Dates = core.Dates;
                export import DeepEqual = core.DeepEqual;
                export import Numeric = core.Numeric;
                export import Objects = core.Objects;
                export import Validation = core.Validation;
                export import Config = core.Config;
            }
            namespace model {
                export import dictionary = core.dictionary;
                export import OAuthMessage = core.OAuthMessage;
                export import OAuthGrantType = core.OAuthGrantType;
                export import OAuthResponseType = core.OAuthResponseType;
                export import OAuthTokenType = core.OAuthTokenType;
                export import OAuthError = core.OAuthError;
                export import OAuthSalaxyAuthorizeMode = core.OAuthSalaxyAuthorizeMode;
                export import AbcSection = core.AbcSection;
                export import AccountBase = core.AccountBase;
                export import AccountingProduct = core.AccountingProduct;
                export import AccountingReport = core.AccountingReport;
                export import AccountProducts = core.AccountProducts;
                export import AccountValidation = core.AccountValidation;
                export import AgeRange = core.AgeRange;
                export import ApiListItemValidation = core.ApiListItemValidation;
                export import ApiProfileSearchResult = core.ApiProfileSearchResult;
                export import ApiProfileSearchResultProfile = core.ApiProfileSearchResultProfile;
                export import ApiTestErrorType = core.ApiTestErrorType;
                export import ApiValidation = core.ApiValidation;
                export import ApiValidationError = core.ApiValidationError;
                export import ApiValidationErrorType = core.ApiValidationErrorType;
                export import Article = core.Article;
                export import AssureCompanyAccountRequest = core.AssureCompanyAccountRequest;
                export import AuthenticationMethod = core.AuthenticationMethod;
                export import Author = core.Author;
                export import AuthorizationContract = core.AuthorizationContract;
                export import AuthorizationStatus = core.AuthorizationStatus;
                export import AuthorizationType = core.AuthorizationType;
                export import Avatar = core.Avatar;
                export import AvatarPictureType = core.AvatarPictureType;
                export import BaseSalaryProduct = core.BaseSalaryProduct;
                export import BlobFile = core.BlobFile;
                export import BlobFileMetadata = core.BlobFileMetadata;
                export import BlobFileType = core.BlobFileType;
                export import BlobRepository = core.BlobRepository;
                export import BusinessObject = core.BusinessObject;
                export import CalcEmployer = core.CalcEmployer;
                export import CalcInfo = core.CalcInfo;
                export import CalcTax = core.CalcTax;
                export import Calculation = core.Calculation;
                export import CalculationResult = core.CalculationResult;
                export import CalculationRowSource = core.CalculationRowSource;
                export import CalculationRowType = core.CalculationRowType;
                export import CalculationRowUnit = core.CalculationRowUnit;
                export import CalculationSharing = core.CalculationSharing;
                export import CalculationStatus = core.CalculationStatus;
                export import CalcWorker = core.CalcWorker;
                export import CalcWorkerPaymentData = core.CalcWorkerPaymentData;
                export import CalcWorkflow = core.CalcWorkflow;
                export import CarBenefit = core.CarBenefit;
                export import Certificate = core.Certificate;
                export import City = core.City;
                export import CompanyAccount = core.CompanyAccount;
                export import CompanyInput = core.CompanyInput;
                export import CompanyType = core.CompanyType;
                export import Contact = core.Contact;
                export import ContractParty = core.ContractParty;
                export import ContractPartyType = core.ContractPartyType;
                export import ContractSalary = core.ContractSalary;
                export import DateOfBirthAccuracy = core.DateOfBirthAccuracy;
                export import DateRange = core.DateRange;
                export import EmailMessage = core.EmailMessage;
                export import EmploymentContract = core.EmploymentContract;
                export import EmploymentSalaryType = core.EmploymentSalaryType;
                export import EmploymentType = core.EmploymentType;
                export import EmploymentWorkHoursType = core.EmploymentWorkHoursType;
                export import EmployerGroups = core.EmployerGroups;
                export import EmployerCalculationDTO = core.EmployerCalculationDTO;
                export import EmployerType = core.EmployerType;
                export import ESalaryPayment = core.ESalaryPayment;
                export import FileType = core.FileType;
                export import FrameworkAgreement = core.FrameworkAgreement;
                export import Gender = core.Gender;
                export import HealthCareHelttiProduct = core.HealthCareHelttiProduct;
                export import HelttiIndustry = core.HelttiIndustry;
                export import HelttiProductPackage = core.HelttiProductPackage;
                export import Identity = core.Identity;
                export import IEmployerResponsibilities = core.IEmployerResponsibilities;
                export import InstantPaymentSaldo = core.InstantPaymentSaldo;
                export import InsuranceCompany = core.InsuranceCompany;
                export import InsuranceProduct = core.InsuranceProduct;
                export import IrDetails = core.IrDetails;
                export import IrRow = core.IrRow;
                export import IrRowCalculationProperties = core.IrRowCalculationProperties;
                export import IVerifiedAccount = core.IVerifiedAccount;
                export import JobProfile = core.JobProfile;
                export import LegalEntityType = core.LegalEntityType;
                export import MessageType = core.MessageType;
                export import Onboarding = core.Onboarding;
                export import OnboardingCredentials = core.OnboardingCredentials;
                export import OnboardingStatus = core.OnboardingStatus;
                export import OnboardingUIState = core.OnboardingUIState;
                export import Ownership = core.Ownership;
                export import PalkkausCampaignPricing = core.PalkkausCampaignPricing;
                export import PartnerCompanyAccountInfo = core.PartnerCompanyAccountInfo;
                export import PartnerSite = core.PartnerSite;
                export import Payer = core.Payer;
                export import Payment = core.Payment;
                export import Payroll = core.Payroll;
                export import PayrollInfo = core.PayrollInfo;
                export import PayrollRow = core.PayrollRow;
                export import PayrollStatus = core.PayrollStatus;
                export import PaymentMethod = core.PaymentMethod;
                export import PaymentRequestParameters = core.PaymentRequestParameters;
                export import PaymentStatus = core.PaymentStatus;
                export import PensionCompany = core.PensionCompany;
                export import PensionEntrepreneurProduct = core.PensionEntrepreneurProduct;
                export import PensionProduct = core.PensionProduct;
                export import PersonAccount = core.PersonAccount;
                export import PersonInput = core.PersonInput;
                export import Profile = core.Profile;
                export import ProfileJob = core.ProfileJob;
                export import ProfileJobCategory = core.ProfileJobCategory;
                export import Recipient = core.Recipient;
                export import Report = core.Report;
                export import ReportCategory = core.ReportCategory;
                export import ReportType = core.ReportType;
                export import ResultRow = core.ResultRow;
                export import Role = core.Role;
                export import RowAccountingData = core.RowAccountingData;
                export import Salary = core.Salary;
                export import SalaryKind = core.SalaryKind;
                export import SalaryPeriod = core.SalaryPeriod;
                export import SalaryPerMonthRange = core.SalaryPerMonthRange;
                export import SalaryPerYearRange = core.SalaryPerYearRange;
                export import SessionUserCredential = core.SessionUserCredential;
                export import Shareholder = core.Shareholder;
                export import SignatureData = core.SignatureData;
                export import SmsMessage = core.SmsMessage;
                export import Taxcard = core.Taxcard;
                export import TaxCardIncome = core.TaxCardIncome;
                export import TaxCardIncomeType = core.TaxCardIncomeType;
                export import TaxcardInput = core.TaxcardInput;
                export import TaxCardResult = core.TaxCardResult;
                export import TaxcardSnapshot = core.TaxcardSnapshot;
                export import TaxcardKind = core.TaxcardKind;
                export import LegacyTaxcardType = core.LegacyTaxcardType;
                export import TaxDeductionWorkCategories = core.TaxDeductionWorkCategories;
                export import TaxProduct = core.TaxProduct;
                export import TesParameters = core.TesParameters;
                export import TesSubtype = core.TesSubtype;
                export import TestAccountInfo = core.TestAccountInfo;
                export import TestEnum = core.TestEnum;
                export import TestValues = core.TestValues;
                export import TotalCalculationDTO = core.TotalCalculationDTO;
                export import UnemploymentProduct = core.UnemploymentProduct;
                export import UnionPaymentType = core.UnionPaymentType;
                export import UserDefinedRow = core.UserDefinedRow;
                export import UserSession = core.UserSession;
                export import WebSiteUserRole = core.WebSiteUserRole;
                export import VismaSignPostData = core.VismaSignPostData;
                export import WorkerAccount = core.WorkerAccount;
                export import WorkerCalculationDTO = core.WorkerCalculationDTO;
                export import WorkerInfo = core.WorkerInfo;
                export import WorkerProfileProperties = core.WorkerProfileProperties;
                export import YearlyChangingNumbers = core.YearlyChangingNumbers;
                export import YtjCompany = core.YtjCompany;
                export import YtjCompanyType = core.YtjCompanyType;
                export import YtjSearchResult = core.YtjSearchResult;
                export import YtjSearchResultCompany = core.YtjSearchResultCompany;
            }
        }
    }
    export namespace salaxy {
        namespace api {
            export import Accounts = core.Accounts;
            export import Ajax = core.Ajax;
            export import AjaxJQuery = core.AjaxJQuery;
            export import AjaxNode = core.AjaxNode;
            export import AjaxXHR = core.AjaxXHR;
            export import AuthorizedAccounts = core.AuthorizedAccounts;
            export import Calculations = core.Calculations;
            export import Calculator = core.Calculator;
            export import Certificates = core.Certificates;
            export import Client = core.Client;
            export import Cms = core.Cms;
            export import Contacts = core.Contacts;
            export import Contracts = core.Contracts;
            export import Credentials = core.Credentials;
            export import EmailMessages = core.EmailMessages;
            export import ESalaryPayments = core.ESalaryPayments;
            export import Files = core.Files;
            export import OAuth2 = core.OAuth2;
            export import Onboardings = core.Onboardings;
            export import Payments = core.Payments;
            export import Payrolls = core.Payrolls;
            export import Profiles = core.Profiles;
            export import Reports = core.Reports;
            export import Session = core.Session;
            export import SmsMessages = core.SmsMessages;
            export import Taxcards = core.Taxcards;
            export import Test = core.Test;
            export import Workers = core.Workers;
            export import calcReportType = core.calcReportType;
            export import reportPartial = core.reportPartial;
        }
        namespace codegen {
            export import accountProductDefaults = core.accountProductDefaults;
            export import generatedEnumData = core.generatedEnumData;
            export import housholdRows = core.housholdRows;
            export import householdUsecaseTree = core.householdUsecaseTree;
        }
        namespace logic {
            export import CalculatorLogic = core.CalculatorLogic;
            export import ContractLogic = core.ContractLogic;
            export import ContractPartyLogic = core.ContractPartyLogic;
            export import EnumerationsLogic = core.EnumerationsLogic;
            export import ImportLogic = core.ImportLogic;
            export import Occupations = core.Occupations;
            export import OnboardingLogic = core.OnboardingLogic;
            export import ProductsLogic = core.ProductsLogic;
            export import ReportsLogic = core.ReportsLogic;
            export import RoleLogic = core.RoleLogic;
            export import TaxCard2019Logic = core.TaxCard2019Logic;
            export import Translations = core.Translations;
            export import WorkerLogic = core.WorkerLogic;
            namespace model {
                export import AppStatus = core.AppStatus;
                export import CalcRowConfig = core.CalcRowConfig;
                export import CalculationRowCategories = core.CalculationRowCategories;
                export import CalculationRowCategory = core.CalculationRowCategory;
                export import Occupation = core.Occupation;
                export import Product = core.Product;
                export import SystemRole = core.SystemRole;
            }
            namespace usecases {
                export import Usecase = core.Usecase;
                export import UsecaseChildCare = core.UsecaseChildCare;
                export import UsecaseCleaning = core.UsecaseCleaning;
                export import UsecaseConstruction = core.UsecaseConstruction;
                export import UsecaseGroup = core.UsecaseGroup;
                export import UsecasesLogic = core.UsecasesLogic;
            }
        }
        namespace util {
            export import Arrays = core.Arrays;
            export import Brand = core.Brand;
            export import Cookies = core.Cookies;
            export import Dates = core.Dates;
            export import DeepEqual = core.DeepEqual;
            export import Numeric = core.Numeric;
            export import Objects = core.Objects;
            export import Validation = core.Validation;
            export import Config = core.Config;
        }
        namespace model {
            export import dictionary = core.dictionary;
            export import OAuthMessage = core.OAuthMessage;
            export import OAuthGrantType = core.OAuthGrantType;
            export import OAuthResponseType = core.OAuthResponseType;
            export import OAuthTokenType = core.OAuthTokenType;
            export import OAuthError = core.OAuthError;
            export import OAuthSalaxyAuthorizeMode = core.OAuthSalaxyAuthorizeMode;
            export import AbcSection = core.AbcSection;
            export import AccountBase = core.AccountBase;
            export import AccountingProduct = core.AccountingProduct;
            export import AccountingReport = core.AccountingReport;
            export import AccountProducts = core.AccountProducts;
            export import AccountValidation = core.AccountValidation;
            export import AgeRange = core.AgeRange;
            export import ApiListItemValidation = core.ApiListItemValidation;
            export import ApiProfileSearchResult = core.ApiProfileSearchResult;
            export import ApiProfileSearchResultProfile = core.ApiProfileSearchResultProfile;
            export import ApiTestErrorType = core.ApiTestErrorType;
            export import ApiValidation = core.ApiValidation;
            export import ApiValidationError = core.ApiValidationError;
            export import ApiValidationErrorType = core.ApiValidationErrorType;
            export import Article = core.Article;
            export import AssureCompanyAccountRequest = core.AssureCompanyAccountRequest;
            export import AuthenticationMethod = core.AuthenticationMethod;
            export import Author = core.Author;
            export import AuthorizationContract = core.AuthorizationContract;
            export import AuthorizationStatus = core.AuthorizationStatus;
            export import AuthorizationType = core.AuthorizationType;
            export import Avatar = core.Avatar;
            export import AvatarPictureType = core.AvatarPictureType;
            export import BaseSalaryProduct = core.BaseSalaryProduct;
            export import BlobFile = core.BlobFile;
            export import BlobFileMetadata = core.BlobFileMetadata;
            export import BlobFileType = core.BlobFileType;
            export import BlobRepository = core.BlobRepository;
            export import BusinessObject = core.BusinessObject;
            export import CalcEmployer = core.CalcEmployer;
            export import CalcInfo = core.CalcInfo;
            export import CalcTax = core.CalcTax;
            export import Calculation = core.Calculation;
            export import CalculationResult = core.CalculationResult;
            export import CalculationRowSource = core.CalculationRowSource;
            export import CalculationRowType = core.CalculationRowType;
            export import CalculationRowUnit = core.CalculationRowUnit;
            export import CalculationSharing = core.CalculationSharing;
            export import CalculationStatus = core.CalculationStatus;
            export import CalcWorker = core.CalcWorker;
            export import CalcWorkerPaymentData = core.CalcWorkerPaymentData;
            export import CalcWorkflow = core.CalcWorkflow;
            export import CarBenefit = core.CarBenefit;
            export import Certificate = core.Certificate;
            export import City = core.City;
            export import CompanyAccount = core.CompanyAccount;
            export import CompanyInput = core.CompanyInput;
            export import CompanyType = core.CompanyType;
            export import Contact = core.Contact;
            export import ContractParty = core.ContractParty;
            export import ContractPartyType = core.ContractPartyType;
            export import ContractSalary = core.ContractSalary;
            export import DateOfBirthAccuracy = core.DateOfBirthAccuracy;
            export import DateRange = core.DateRange;
            export import EmailMessage = core.EmailMessage;
            export import EmploymentContract = core.EmploymentContract;
            export import EmploymentSalaryType = core.EmploymentSalaryType;
            export import EmploymentType = core.EmploymentType;
            export import EmploymentWorkHoursType = core.EmploymentWorkHoursType;
            export import EmployerCalculationDTO = core.EmployerCalculationDTO;
            export import EmployerGroups = core.EmployerGroups;
            export import EmployerType = core.EmployerType;
            export import ESalaryPayment = core.ESalaryPayment;
            export import FileType = core.FileType;
            export import FrameworkAgreement = core.FrameworkAgreement;
            export import Gender = core.Gender;
            export import HealthCareHelttiProduct = core.HealthCareHelttiProduct;
            export import HelttiIndustry = core.HelttiIndustry;
            export import HelttiProductPackage = core.HelttiProductPackage;
            export import Identity = core.Identity;
            export import IEmployerResponsibilities = core.IEmployerResponsibilities;
            export import InsuranceCompany = core.InsuranceCompany;
            export import InstantPaymentSaldo = core.InstantPaymentSaldo;
            export import InsuranceProduct = core.InsuranceProduct;
            export import IrDetails = core.IrDetails;
            export import IrRow = core.IrRow;
            export import IrRowCalculationProperties = core.IrRowCalculationProperties;
            export import IVerifiedAccount = core.IVerifiedAccount;
            export import JobProfile = core.JobProfile;
            export import LegalEntityType = core.LegalEntityType;
            export import MessageType = core.MessageType;
            export import Onboarding = core.Onboarding;
            export import OnboardingCredentials = core.OnboardingCredentials;
            export import OnboardingStatus = core.OnboardingStatus;
            export import OnboardingUIState = core.OnboardingUIState;
            export import Ownership = core.Ownership;
            export import PalkkausCampaignPricing = core.PalkkausCampaignPricing;
            export import PartnerCompanyAccountInfo = core.PartnerCompanyAccountInfo;
            export import PartnerSite = core.PartnerSite;
            export import Payer = core.Payer;
            export import Payment = core.Payment;
            export import Payroll = core.Payroll;
            export import PayrollInfo = core.PayrollInfo;
            export import PayrollRow = core.PayrollRow;
            export import PayrollStatus = core.PayrollStatus;
            export import PaymentMethod = core.PaymentMethod;
            export import PaymentRequestParameters = core.PaymentRequestParameters;
            export import PaymentStatus = core.PaymentStatus;
            export import PensionCompany = core.PensionCompany;
            export import PensionEntrepreneurProduct = core.PensionEntrepreneurProduct;
            export import PensionProduct = core.PensionProduct;
            export import PersonAccount = core.PersonAccount;
            export import PersonInput = core.PersonInput;
            export import Profile = core.Profile;
            export import ProfileJob = core.ProfileJob;
            export import ProfileJobCategory = core.ProfileJobCategory;
            export import Recipient = core.Recipient;
            export import Report = core.Report;
            export import ReportCategory = core.ReportCategory;
            export import ReportType = core.ReportType;
            export import ResultRow = core.ResultRow;
            export import Role = core.Role;
            export import RowAccountingData = core.RowAccountingData;
            export import Salary = core.Salary;
            export import SalaryKind = core.SalaryKind;
            export import SalaryPeriod = core.SalaryPeriod;
            export import SalaryPerMonthRange = core.SalaryPerMonthRange;
            export import SalaryPerYearRange = core.SalaryPerYearRange;
            export import SessionUserCredential = core.SessionUserCredential;
            export import Shareholder = core.Shareholder;
            export import SignatureData = core.SignatureData;
            export import SmsMessage = core.SmsMessage;
            export import Taxcard = core.Taxcard;
            export import TaxCardIncome = core.TaxCardIncome;
            export import TaxCardIncomeType = core.TaxCardIncomeType;
            export import TaxcardInput = core.TaxcardInput;
            export import TaxCardResult = core.TaxCardResult;
            export import TaxcardSnapshot = core.TaxcardSnapshot;
            export import TaxcardKind = core.TaxcardKind;
            export import LegacyTaxcardType = core.LegacyTaxcardType;
            export import TaxDeductionWorkCategories = core.TaxDeductionWorkCategories;
            export import TaxProduct = core.TaxProduct;
            export import TesParameters = core.TesParameters;
            export import TesSubtype = core.TesSubtype;
            export import TestAccountInfo = core.TestAccountInfo;
            export import TestEnum = core.TestEnum;
            export import TestValues = core.TestValues;
            export import TotalCalculationDTO = core.TotalCalculationDTO;
            export import UnemploymentProduct = core.UnemploymentProduct;
            export import UnionPaymentType = core.UnionPaymentType;
            export import UserDefinedRow = core.UserDefinedRow;
            export import UserSession = core.UserSession;
            export import WebSiteUserRole = core.WebSiteUserRole;
            export import VismaSignPostData = core.VismaSignPostData;
            export import WorkerAccount = core.WorkerAccount;
            export import WorkerCalculationDTO = core.WorkerCalculationDTO;
            export import WorkerInfo = core.WorkerInfo;
            export import WorkerProfileProperties = core.WorkerProfileProperties;
            export import YearlyChangingNumbers = core.YearlyChangingNumbers;
            export import YtjCompany = core.YtjCompany;
            export import YtjCompanyType = core.YtjCompanyType;
            export import YtjSearchResult = core.YtjSearchResult;
            export import YtjSearchResultCompany = core.YtjSearchResultCompany;
        }
    }
}
declare module "namespace/index" {
    export * from "namespace/namespace";
}
declare module "testing/TestHelperConfig" {
    /**
     * Configuration for the test helper:
     * Which server to contact, the credentials etc.
     */
    export interface TestHelperConfig {
        /**
         * Server to connect to.
         * Default / recommended is "auto", which currently points to test,
         * but may later be directed to a separte unit test environment.
         */
        server?: "auto" | "test" | "prod" | "localhost";
        /**
         * Possibility to set the server URL and port explicitly.
         * This setting overrides the server property, but is typically null: Used only in special situations.
         */
        serverUrl?: string;
        /** Username that is used to authenticate the connection. */
        username?: string;
        /** Password that is used to authenticate the connection. */
        password?: string;
        /**
         * Test timeout in milliseconds. Used for calling Mocha.ISuiteCallbackContext.timeout() if provided.
         * The default is currently 30000, but it may be changed without it being a breaking change, so configure yout own, if you need a specific value.
         */
        timeout?: number;
    }
}
declare module "testing/TestHelper" {
    import { Accounts, AjaxNode, Calculations, Calculator, ESalaryPayments, Payments, Reports, Taxcards, Test } from "api/index";
    import { OAuthMessage } from "model/index";
    import { TestHelperConfig } from "testing/TestHelperConfig";
    /**
     * Helpers for creating and runing tests in Node (server) environment.
     * Unit tests, System tests and demonstrations written in test format.
     * Tested and designed for Mocha / Chai, but should apply to other frameworks as well.
     */
    export class TestHelper {
        /**
         * Ajax instance that is used in this test session.
         * Before using, initialize using one of the init-methods: Typically either intiAnon() or iniTestUser();
         */
        ajax: AjaxNode;
        /** Configuration for the ajax connection */
        config: TestHelperConfig;
        /**
         * Creates a new TestHelper with server configured, but not authenticated (call authenticate() to do that).
         * Anonymous mode is enough for many unit testing scenarios but not for e.g. CRUD operations.
         * We do not create an authenticated connection in constructor as a best practice.
         */
        constructor(config?: TestHelperConfig, suiteCallBackContext?: any);
        /**
         * Initializes the authenticated connection to the test server according to config.
         * Before calling this method, the ajax connection is anonymous.
         * Note that in Mocha / Chai, you can attach the method directly to before(), see examples below.
         *
         * @example
         * // Mocha / Chai testing
         * describe("Calculations basic CRUD operations.", function() {
         *             const testHelper = new TestHelper(config, this);
         *             before("Authenticate", testHelper.authenticate);
         *
         * @example
         * const testHelper = new TestHelper(config);
         * testHelper.authenticate().then(() => {
         *             // Some other initialization code here.
         * });
         */
        authenticate: () => Promise<OAuthMessage>;
        /** Shortcut to get the Salaxy OpenApi wrappers with the initialized ajax. */
        readonly api: {
            test: Test;
            accounts: Accounts;
            calculator: Calculator;
            calculations: Calculations;
            eSalaryPayments: ESalaryPayments;
            taxcards: Taxcards;
            reports: Reports;
            payments: Payments;
        };
        private getServer;
    }
}
declare module "testing/index" {
    export * from "testing/TestHelper";
    export * from "testing/TestHelperConfig";
}
declare module "index" {
    export * from "api/index";
    export * from "codegen/index";
    export * from "i18n/index";
    export * from "logic/index";
    export * from "util/index";
    export * from "model/index";
    export * from "namespace/index";
    export * from "testing/index";
}
