/// <reference types="angular" />
/// <reference types="angular-translate" />
/// <reference types="angular-ui-bootstrap" />
declare module "ajax/AjaxNg1" {
    import { IQService } from "angular";
    import { Ajax } from "@salaxy/core";
    /**
     * The $http access to the server methods: GET, POST and DELETE
     * with different return types and authentication / error events.
     */
    export class AjaxNg1 implements Ajax {
        private $http;
        private $q;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Alert service for ajax. */
        static alertService: any;
        /**
         * By default (true) the token is set to salaxy-token -cookie.
         * Disable cookies with this flag.
         */
        useCookie: boolean;
        /**
         * By default credentials are not used in http-calls.
         * Enable credentials with this flag.
         */
        useCredentials: boolean;
        /**
         * The server address - root of the server
         * This is settable field. Will probably be changed to a configuration object in the final version.
         */
        serverAddress: string;
        private token;
        /**
         * Creates a new instance of AjaxNg1
         *
         * @param {angular.IHttpService} $http - Angular http service
         */
        constructor($http: angular.IHttpService, $q: IQService);
        /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
        getApiAddress(): string;
        /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
        getServerAddress(): string;
        /** Gets a JSON-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new'.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        getJSON(method: string): Promise<any>;
        /**
         * Gets a HTML-message from server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new'.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        getHTML(method: string): Promise<string>;
        /**
         * POSTS data to server and receives back a JSON-message.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new'.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postJSON(method: string, data: any): Promise<any>;
        /**
         * POSTS data to server and receives back HTML.
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new'.
         * @param data - The data that is posted to the server.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        postHTML(method: string, data: any): Promise<string>;
        /**
         * Sends a DELETE-message to server using the API
         *
         * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
         *        and starts with a forward slash, e.g. '/calculator/new'.
         *
         * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
         */
        remove(method: string): Promise<any>;
        /**
         * Gets the current token.
         * Will check the salaxy-token cookie if the token is persisted there
         */
        getCurrentToken(): string;
        /**
         * Sets the current token. Persists it to cookie until the browser window
         *
         * @param token - the authentication token to persist.
         */
        setCurrentToken(token: string): void;
        /** If missing, append the API server address to the given url method string */
        private getUrl;
        private handleError;
    }
}
declare module "ajax/index" {
    export * from "ajax/AjaxNg1";
}
declare module "services/ui/AlertService" {
    /**
     * Handles error, warning and debug alerts and other such error handling situations.
     * Default functionality is to find #salaxy-alert-container and insert a bootstrap alert box into that container.
     */
    export class AlertService {
        private $rootScope;
        private $timeout;
        private $sce;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Default duration for alerts to be visible */
        duration: number;
        /** Service level counter of alerts. Used for identifier. */
        private alertCounter;
        private alerts;
        private eventPrefix;
        /** Dependency injection etc. */
        constructor($rootScope: angular.IRootScopeService, $timeout: angular.ITimeoutService, $sce: angular.ISCEService);
        /**
         * Adds an error message
         * @param message
         */
        addError(message: string): void;
        /**
         * Adds a warning message
         * @param message
         */
        addWarning(message: string): void;
        /**
         * Adds a success message
         * @param message
         */
        addSuccess(message: string): void;
        /**
         * Adds an info message
         * @param message
         */
        addInfo(message: string): void;
        /**
         * Last message - consider this a temporary implementation of the custom alert handling
         *
         * @returns Last message
         */
        getLastMessage(): string;
        /**
         * Last alertType is bootstrap type: info (default), success, warning or danger.
         * Consider this a temporary implementation of the custom alert handling
         *
         * @returns Last alert type
         */
        getLastAlertType(): string;
        /**
         * INTERNAL ONLY: This functionality is under consideration.
         * We may not support it in the future and we may remove it without it being a breaking change.
         *
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @ignore
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        onChange(scope: any, callback: any): void;
        /**
         * Adds an alert message
         *
         * @param message - A message text for the Alert. Preferably a short text so that it fits to the pull-righ box.
         * @param alertType - Bootstrap color code for the alert info (default), success, warning or danger
         */
        private addAlert;
        private renderAlerts;
        private notify;
        private init;
    }
}
declare module "services/ui/EditDialogKnownActions" {
    /**
     * Known dialog actions: Buttons that result in closing the dialog.
     * A dialog action can be a string for special purposes, but if the dialog action is something clerly reuasable, you may add it here.
     */
    export enum EditDialogKnownActions {
        /** Primary action of the dialog: Often Save (Insert / Update) */
        Ok = "ok",
        /** Primary action of the dialog: Often Save (Insert / Update). Does not perform any save operation against api. */
        OkNoCommit = "ok-no-save",
        /**
         * Cancel / reset dialog action.
         * Also the close button at the top-right corner, esc keyboard action and clicking outside dialog when available.
         */
        Cancel = "cancel",
        /**
         * Delete item action in editor windows.
         */
        Delete = "delete",
        /**
         * Delete item action in editor windows. Does not perform any delete operation against api.
         */
        DeleteNoCommit = "delete-no-save"
    }
}
declare module "services/ui/EditDialogResult" {
    import { EditDialogKnownActions } from "services/ui/EditDialogKnownActions";
    /** Result from a modal dialog */
    export class EditDialogResult<TItem> {
        /**
         * Result of the modal is typically either OK or Cancel.
         * Edit dialogs often also have "delete", but you may add custom actions.
         */
        action: EditDialogKnownActions | string;
        /**
         * Result is the object that Dialog sends back in $close() method.
         * If this is string, it is interpreted as action.
         */
        result: string | {
            /** Action of the result. */
            action?: string;
        } | any;
        /**
         * Data that is being edited in the dialog.
         */
        item: TItem;
        /**
         * The logic part should typically contain functions for the modal view, but in exceptional cenarios you may pass some data back here as well.
         */
        logic: any;
        /** If true, the item has been edited by the user and should typically be saved. */
        hasChanges: boolean;
    }
}
declare module "services/SessionService" {
    import * as angular from "angular";
    import { AccountBase, Accounts, Ajax, Avatar, CompanyAccount, PersonAccount, Role, Session, SystemRole, UserSession } from "@salaxy/core";
    /**
     * Helps in managing the login process and provides information of the current session
     */
    export class SessionService {
        private $rootScope;
        private sessionApi;
        private accountApi;
        private $window;
        private $location;
        private ajax;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, the user is authenticated */
        isAuthenticated: boolean;
        /** If true, the session has been checked from the server */
        isSessionChecked: boolean;
        /** If true, the session call is progressing  */
        isSessionChecking: boolean;
        /**
         * Temporary field to store the info that the data is loaded.
         * Currently stored by WorkerService only => Will be developed further.
         */
        isDataLoaded: boolean;
        /** Avatar to show in the login screen */
        avatar: Avatar;
        /** The full session objcet */
        session: UserSession;
        /** The person account object if the currently selected account is a person */
        personAccount: PersonAccount;
        /** The company account object if the currently selected account is a company */
        companyAccount: CompanyAccount;
        /** Sign in error */
        signInError: string;
        /** Sign in error description */
        signInErrorDescription: string;
        /** Sign in error page url */
        signInErrorUrl: string;
        /** Partner site */
        partnerSite: string;
        /**
         * Creates a new instance of Profiles
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param sessionApi - The Session API that is used to communicating with the server.
         * @param accountApi - The Accounts API that is used to communicating with the server.
         * @param $window - Angular window service
         * @param $location - Angular location service
         * @param AjaxNg1 - Ajax angular component
         */
        constructor($rootScope: angular.IRootScopeService, sessionApi: Session, accountApi: Accounts, $window: angular.IWindowService, $location: angular.ILocationService, ajax: Ajax);
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        /**
         * Services can subscribe to this event to be notified when it is known that there is an authenticated session
         * (as opposed to anonymous session). Uses the same pattern as subscribe.
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        onAuthenticatedSession(scope: any, callback: any): void;
        /** If true, the user is authenticated */
        getIsAuthenticated(): boolean;
        /** If true, the session has been checked from the server */
        getIsSessionChecked(): boolean;
        /** If true, the session call is progressing */
        getIsSessionChecking(): boolean;
        /** Avatar to show in the login screen */
        getAvatar(): Avatar;
        /** The full session objcet */
        getSession(): UserSession;
        /** Gets either the Company account or PErson account depending of the type of the current account. */
        getCurrentAccount(): AccountBase;
        /** Get the person account object if the currently selected account is a person */
        getPersonAccount(): PersonAccount;
        /** The company account object if the currently selected account is a company */
        getCompanyAccount(): CompanyAccount;
        /** Gets the address for the API server where the session is connected */
        getServerAddress(): string;
        /**
         * Returns current access token
         */
        getCurrentToken(): string;
        /**
         * Checks whether the user is in a given role
         * @param role - One of the known roles or role from server.
         * You can also use exclamation mark for negative (e.g. "!test")
         * @returns True if user is in the given role,
         * or if given roles is null/empty.
         */
        isInRole(role: SystemRole | Role | string): boolean;
        /**
         * Checks if the user is in ANY of the roles.
         * @param accountRoles - Array of roles or comma separated string containing role names.
         * @returns True if user is in one of the given roles,
         * or if given roles is null or an empty array.
         */
        isInSomeRole(commaSeparatedRolesList: Array<SystemRole | Role | string> | string): any;
        /**
         * Tests if the current account has ALL the given roles.
         * @param accountRoles - Array of roles.
         * @returns Returns true if the account has all the given roles,
         * or if given roles is null or an empty array.
         */
        isInAllRoles(accountRoles: Array<SystemRole | Role | string>): boolean;
        /** Posts a message to the parent window. The message is sent as salaxySessionEvent  */
        postMessageToParent(message: string): void;
        /**
         * Checks the current session from the server if user has token (either in-memory or in cookie)
         *
         * @returns A Promise with result data (UserSession)
         */
        checkSession(): Promise<UserSession>;
        /**
         * Switches the current web site usage role and refreshes the session.
         * @param role - household or worker.
         * @returns A Promise with result data (new role as string)
         */
        switchRole(role: "household" | "worker"): Promise<"household" | "worker">;
        /**
         * Sends the browser to standard Sign-in page on Salaxy API server.
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        signIn(redirectUrl?: string, role?: string, partnerSite?: string): void;
        /**
         * Sends the browser to standard signUp / register page on Salaxy API server.
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        register(redirectUrl?: string, role?: string, partnerSite?: string): void;
        /**
         * Sends the user to the Sign-out page
         *
         * @param redirectUrl - URL where user is redirected after log out.
         *          Must be absolute URL. Default is the root of the current server.
         */
        signOut(redirectUrl?: string): void;
        /** Returns true
         * if the current authenticated user has signed the contract.
         */
        checkAccountVerification(): boolean;
        private notify;
        private notifyAuthenticatedSession;
        /**
         * Sends the browser to standard Sign-in page on Salaxy API server.
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login.
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users.
         * @param mode - Salaxy login mode: "signIn" or "signUp".
         */
        private authorize;
        private base64Unescape;
        private parseTokenPayload;
        private readTokenFromUrl;
        private checkSessionFromUrl;
        private init;
        private getAppStatus;
    }
}
declare module "services/ui/NaviService" {
    import * as angular from "angular";
    import { SessionService } from "services/SessionService";
    /**
     * First level node in Palkkaus sitemap is called a "Section".
     * Each section must have an id and may have children.
     */
    export interface SalaxySitemapSection extends SalaxySitemapNode {
        /** Id for the node. */
        id: string;
        /** Array of child nodes. */
        children: SalaxySitemapNode[];
    }
    /**
     * A node in Palkkaus sitemap.
     * In this implementation children are not supported for normal nodes (as opposed to sections).
     * This makes the sitemap maximum 2 levels deep.
     * It  may be changed in the future if deeper hierarchies are needed.
     */
    export interface SalaxySitemapNode {
        /** Title. */
        title: string;
        /** Url. */
        url: string;
        /** Description. */
        decr?: string;
        /** Icon name. */
        icon?: string;
        /** Color code. */
        color?: string;
        /** Roles for limiting the visibility of the node. */
        roles?: string;
        /** Image url. */
        image?: string;
    }
    /**
     * Helper service to generate navigation controls:
     * Top- and side-menus, paths and controls that show the current title.
     * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
     * NOTE: This is just an optional helper to make creating simple demo sites easier.
     * There is no need to use NaviService, NaviController or components in your custom site!
     * You can use something completely different.
     */
    export class NaviService {
        private $rootScope;
        private $location;
        private SITEMAP;
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Section where the current node is located. */
        currentSection: SalaxySitemapSection;
        /** Currently selected sitemap node. */
        currentNode: SalaxySitemapNode;
        /** Title of the section (first level in the sitemap) */
        sectionTitle: string;
        /** Title of the current sitemap node */
        title: string;
        /**
         * True if the navigation (e.g. left sidebar) is open.
         * Typically, this property is used only in narrow (mobile) view, otherwise the sidebar is always shown.
         */
        isNaviOpen: boolean;
        /**
         * True if the secondary sidebar (e.g. right sidebar) is open.
         */
        isSidebarOpen: boolean;
        /** Sitemap that describes the structure of this application */
        private sitemap;
        /** Sitemap that describes the structure of this application */
        private sitemapInRole;
        /**
         * Creates a new instance of CmsService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param $location - Angular $location service.
         * @param SITEMAP - A sitemap object that defines the structure for this web site.
         * @param sessionService - Salaxy sessionService.
         */
        constructor($rootScope: angular.IRootScopeService, $location: angular.ILocationService, SITEMAP: any, sessionService: SessionService);
        /**
         * Gets the current sitemap.
         *
         * @param inRole - Default false, indicates whether sitemap should be filtered by roles.
         *
         * @returns SitemapInRole if Role given, else Sitemap (SalaxySitemapSection[])
         *
         */
        getSitemap(inRole?: boolean): SalaxySitemapSection[];
        /** Determines if a given node is the current node */
        isCurrent(siteMapNode: SalaxySitemapNode): boolean;
        /** Determines if a given section is the current section */
        isCurrentSection(section: SalaxySitemapNode): boolean;
        /**
         * Get sitemap node using path.
         *
         * @param path - Page URL path for the node.
         *
         * @returns - Node if exists, otherwise null.
         */
        getNodeByPath(path: string): SalaxySitemapNode | null;
        /**
         * Filters the sitemap using the role-restrictions in the sitemap
         */
        updateSitemapInRole(): void;
        /**
         * Evaluates wildcard string.
         *
         * @param nodeUrl - URL in the sitemap file (e.g. "filename.html#/this/is/nodeUrl"). From this, we take only the hash part ("#/this/is/nodeUrl").
         * @param pathUrl - URL to evaluate - this is the hash path (e.g. "#/this/is/pathUrl")
         *
         * @returns Boolean whether the path url resides within the given node url.
         */
        private checkUrl;
        /**
         * Evaluates wildcard string
         *
         * @param rule - Wildcard rule
         * @param str - String to evaluate
         *
         * @returns Boolean whether the string matches the given wildcard rule.
         */
        private wildCardMatch;
        private resolveCurrentNode;
        /**
         * Filters our sections from given array using role criteria
         * @param sections - Array of sections
         * @returns Filtered list of sitemap sections.
         */
        private filterSitemapSectionsByRole;
    }
}
declare module "i18n/dictionary" {
    /** Internationalization dictionary
    @ignore */
    export const dictionary: any;
}
declare module "services/ui/Ng1Translations" {
    import * as angular from "angular";
    /**
     * Translation service for Ng1.
     * Provides basic methods to utilize i18n translations both in code and html (filter).
     * Please use always this class for translations instead of any other framework, which can be utilized through this class.
     */
    export class Ng1Translations {
        private $translate;
        private $translateProvider;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * @param $translate - Translate service.
         * @param $translateProvider - Translate provider (reference value).
         */
        constructor($translate: angular.translate.ITranslateService, $translateProvider: angular.translate.ITranslateProvider);
        /**
         * Gets the translated value of a key (or an array of keys) synchronously
         * Main point here is that we do not use the async promise infrastructure
         * of ngx-translate as it is obsolete for us.  The translations must be
         * loaded as the app loads: It does not make sense to allow loading them
         * from server asyncronously.
         *
         * @param key
         * @param interpolateParams
         *
         * @returns The translated key, or an object of translated keys
         */
        get(key: string | string[], interpolateParams?: object): any;
        /**
         * Adds a dictionary to the current module. The dictionary translations
         * are merged to any existing translations already in the module.
         *
         * @param lang - ISO language code for the dictionary to be added: fi, en or sv
         * @param translations - A translations object (json)
         */
        addDictionary(lang: string, translations: object): void;
        /**
         * Add all dictionaries from a single language object.
         * This object must have language codes as first level keys. The dictionary translations
         * are merged to any existing translations already in the module.
         *
         * @param translations - A translations object (json)
         */
        addDictionaries(translations: object): void;
        /**
         * Set the language of the current user interface to the given language.
         * Should be called at least once at the beginning of the App.
         *
         * @param lang - ISO language code for the language of the UI: fi (default), en or sv
         *
         * @returns Promise object with loaded language file data or string of the currently used language.
         */
        setLanguage(lang: string): angular.IPromise<string>;
        /**
         * Returns current language.
         *
         * @returns Promise object with loaded language file data or string of the currently used language.
         */
        getLanguage(): string;
        /**
         * Adds default language  and default dictionaries.
         *
         * @param lang - Default language for dictionaries.
         */
        private addDefaultDictionaries;
        private init;
    }
}
declare module "services/IService" {
    /** Defines the repository interface for Salaxy CRUD objects */
    export interface IService<T> {
        /** List of items in the repository */
        list: T[];
        /** Currently selected item in the repository */
        current: T;
        /**
         * Creates a copy of a given item.
         * This method does not yet set the item as current.
         * @param copySource Item to copy as new.
         */
        copyAsNew(copySource: T): T;
        /**
         * Gets a new blank object with default values, suitable for UI binding.
         * This is a synchronous method - does not go to the server and it is not 100% reliable in that way.
         * However, it shoud provide a basic object, good enough for most views.
         */
        getBlank?(): T;
        /** Reloads the list from the server - called e.g. after delete and add new */
        reloadList(): Promise<T[]>;
        /**
         * Sets the current repository item.
         * If not loaded, starts loading from the server to the current.
         * @param id - Identifier of the calculation
         */
        setCurrentId(id: string): void;
        /**
         * Sets the current calculation
         * @param item - Item to set as current
         */
        setCurrent(item: T): void;
        /**
         * Creates a new item and sets it as current for editing and saving.
         * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
         */
        newCurrent?(): void;
        /** Saves changes to the current item. */
        saveCurrent(): Promise<T>;
        /**
         * Adds or updates a given item.
         * Operation is determined based on id: null/''/'new' adds, other string values update.
         */
        save(item: T): Promise<T>;
        /** Deletes the given item from repository if possible */
        delete?(id: any): Promise<string>;
        /**
         * Subscribe to changes with given callback.
         */
        onChange(scope: any, callback: any): void;
    }
}
declare module "services/BaseService" {
    import * as angular from "angular";
    import { CrudApiBase } from "@salaxy/core";
    import { IService } from "services/IService";
    import { SessionService } from "services/SessionService";
    /**
     * Base class for Angular 1.5 (NG1) CRUD services in Salaxy environment.
     */
    export abstract class BaseService<T> implements IService<T> {
        private $rootScope;
        protected sessionService: SessionService;
        private api;
        /**
         * List of items loaded to the service.
         * Empty list if items are still been loaded.
         */
        list: T[];
        /** Currently selected item. */
        current: T;
        /** Set to true when the initial data hase been loaded from the server (not the anonymous dummy data). */
        isInitialDataLoaded: boolean;
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected abstract eventPrefix: any;
        /**
         * @param $rootScope - Angular root scope. Used for event routing
         * @param sessionService - Salaxy session service. Used for getting the information whether the use is authenticated or not.
         * @param api - Api class that is the bases for the service. Typically from @salaxy/core project.
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, api: CrudApiBase<T>);
        /**
         * Reloads the list from the server - called in init and e.g. after Delete and Add new
         *
         * @returns A Promise with result data
         */
        reloadList(): Promise<T[]>;
        /**
         * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
         *
         * @returns A Blank item with default values.
         */
        getBlank(): T;
        /**
         * Get the identifier of the current item, null if none is selected.
         *
         * @returns The identifier of current item, or null if not set
         */
        getCurrentId(): string | null;
        /**
         * Set the current item by id.
         * If id parameter is "new", the item is set to a new blank item.
         * If id paramter is null, the current item is set to null.
         *
         * @param id - The Identifier for the item to set as current
         */
        setCurrentId(id: "new" | string): void;
        /**
         * Set the current item to given item
         *
         * @param item - The item to set as current
         */
        setCurrent(item: T): void;
        /**
         * Sets the current item to a new blank item.
         */
        newCurrent(): void;
        /**
         * Saves the current item to database.
         *
         * @returns A Promise with result data: The current object after round-trip to server.
         */
        saveCurrent(): Promise<T>;
        /**
         * Saves the given item to database.
         *
         * @param item - Item to be saved
         *
         * @returns A Promise with the item after the round-trip to server.
         */
        save(item: T): Promise<T>;
        /**
         * Deletes the specified item.
         *
         * @param id - The identifier for the item to be deleted
         *
         * @returns A Promise with result data ("Object deleted")
         */
        delete(id: string): Promise<string>;
        /**
         * Creates a copy of a given item.
         * This method does not yet set the item as current.
         * @param copySource Item to copy as new.
         */
        copyAsNew(copySource: T): T;
        /**
         * INTERNAL ONLY: This functionality is under consideration.
         * We may not support it in the future and we may remove it without it being a breaking change.
         *
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         *
         * @ignore
         */
        onChange(scope: any, callback: any): void;
        /** INTERNAL ONLY: Emits the service event (typically list reload). This functionality may be dropped in future versions without warning. */
        protected notify(): void;
        private init;
    }
}
declare module "controllers/CrudControllerBase" {
    import { IService, UiHelpers } from "services/index";
    /**
     * Base class for CRUD controllers.
     * Implements the common UI functionality that is possible based on BaseService<T>.
     */
    export class CrudControllerBase<T> implements angular.IController {
        protected crudService: IService<T>;
        protected $location: angular.ILocationService;
        protected $attrs: angular.IAttributes;
        protected uiHelpers: UiHelpers;
        /**
         * Bindings for the component which uses CRUD controller.
         */
        static crudBindings: {
            /**
             * Function that is called when user selects an item in the list.
             * The selected item is the parameter of the function call.
             * @example <salaxy-payroll-list on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
             */
            onListSelect: string;
            /**
             * Function that is called when an item is deleted.
             * The event is intended for user interface logic after delete (promise resolve)
             * and potentially when waiting for server to respond (from function call until promise resolve).
             * It is not meant for delete validation and/or for preventing deletion.
             * If onDelete is not specified, the browser is redirected to listUrl if specified.
             * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
             */
            onDelete: string;
            /**
             * Function that is called after a new item has been created.
             * At this point, the item has been created, but not yet selected as current.
             * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
             * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc"></salaxy-payroll-list>
             */
            onCreateNew: string;
            /**
             * URL to which the component navigates when an item is clicked.
             * The "id" or "rowIndex" property of the selected item is added to the URL.
             * URL is ignored if onListSelect is set. In this case, you may navigate yourself in that method.
             * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
             */
            detailsUrl: string;
            /**
             * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
             * @example
             * <!-- Main worker list is in the front page in this case -->
             * <salaxy-worker-details list-url="/home/index"></salaxy-worker-details>
             */
            listUrl: string;
        };
        /**
         * For list-controls, this is the URL for item select event
         * as well as the URL where a new item is edited. Basically showing the Details view.
         * For more control, use onListSelect or onCreateNew events.
         * @example <salaxy-payroll-list details-url="/myCustomRoute/"></salaxy-payroll-list>
         */
        detailsUrl: string;
        /**
         * URL for the list view. At the moment, if specified, the browser is redirected here after delete.
         * @example
         * <!-- Main worker list is in the front page in this case -->
         * <salaxy-worker-details list-url="/home/index"></salaxy-worker-details>
         */
        listUrl: string;
        /**
         * Angular ng-model **if** the controller is bound to model using ng-model attribute.
         */
        model: angular.INgModelController;
        /**
         * Function that is called when an item is deleted.
         * The event is intended for user interface logic after delete (promise resolve)
         * and potentially when waiting for server to respond (from function call until promise resolve).
         * It is not meant for delete validation and/or for preventing deletion.
         * If onDelete is not specified, the browser is redirected to listUrl if specified.
         * NOTE: the deleteResult should basically always be  Promise<true> or promise failure. Promise<false> does not really make sense here.
         * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc(deleteResult)"></salaxy-payroll-list>
         */
        onDelete: (eventData: {
            /** The deleteResult should basically always be  Promise<true> or promise failure. Promise<false> does not really make sense here. */
            deleteResult: Promise<boolean>;
        }) => Promise<boolean>;
        /**
         * Function that is called after a new item has been created.
         * At this point, the item has been created, but not yet selected as current.
         * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
         * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc(item)"></salaxy-payroll-list>
         */
        onCreateNew: (eventData: {
            /** New item which was created. */
            item: T;
        }) => void;
        /**
         * For list-controls, this is the function of on-list-select event.
         * If onListSelect is not specified, the browser is redirected to detailsUrl if specified.
         * If detailsUrl is not specified, only current item is set.
         * @example <salaxy-payroll-list on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
         */
        onListSelect: (eventData: {
            /** The selected item. */
            item: T;
        }) => void;
        /**
         * Creates a new CrudControllerBase.
         * @param crudService The the BaseService instance that is used for communicating to server.
         * @param $location Angular.js Location service that is used for navigation. Especially the list views.
         * @param $attrs Angular.js Attisbutes for determining whether events have been bound to.
         * @param uiHelpers - Salaxy ui helpers service.
         */
        constructor(crudService: IService<T>, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
        /**
         * Implement IController by providing onInit method.
         * We currently do nothing here, but if you override this function,
         * you should call this method in base class for future compatibility.
         */
        $onInit(): void;
        /** Gets the Current selected item. */
        readonly current: T;
        /** Gets the list of all CRUD objects listed. */
        readonly list: T[];
        /**
         * Creates a new item. The item is not saved in this process yet.
         * Item is set as current unless onCreateNew is specified => Then you are responsible for doing it yourself.
         * If detailsUrl is specified, the browser is redirectedted there.
         * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
         * and passing it in current item is the only way using router.
         * Use onCreateNew event or crudService.getBlank() if you do not want to set the current item.
         * @param newItem Specify the new item if you want to initialize it with specific values.
         * In most cases, you should let the system create it with defaults.
         */
        createNew(newItem?: T): T;
        /** Copies the current item and sets it as the new current */
        copyCurrent(): void;
        /**
         * Creates a copy of a given item and sets it as current
         * unless onCreateNew is specified => Then you are responsible for doing it yourself.
         * If detailsUrl is specified, the browser is redirectedted there.
         * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
         * and passing it in current item is the only way using router.
         * Use onCreateNew event or crudService.copyAsNew() if you do not want to set the current item.
         * @param copySource The item to copy as new.
         * @returns The new item that is created.
         */
        copyAsNew(copySource: T): T;
        /**
         * Mode for data binding is "singleton" by default.
         * This means that this.current is bound to crudService.current.
         * If ng-model is defined for the component (this.ngModelController),
         * this.current is bound to ngModelController view value (as with form controls).
         */
        getBindingMode(): "model" | "singleton";
        /** Called by the view when the item is clicked. */
        listSelect: (selectedItem: T) => void;
        /** Saves the current item. */
        saveCurrent(): Promise<T>;
        /**
         * Sets the current item: Either to ng-model view value or crudService.setCurrent().
         * @param item The item to set as Current.
         */
        setCurrent(item: T): void;
        /**
         * Shows the "Are you sure?" dialog and if user clicks OK, deletes the item.
         * Cancels the started payment for the payroll too.
         *
         * @param id: Identifier of the item to be deleted.
         * @param confirmMessage: Optional custom message for the confirm dialog.
         * If not specified, a generic message is shown.
         * If set to boolean false, the confirm message is not shown at all.
         *
         * @returns Promise that resolves to true if the item is deleted.
         * False, if user cancels and fails if the deletion fails.
         */
        delete(id: string, confirmMessage?: string): Promise<boolean>;
        /**
         * Deletes an item without showing the confirm dialog.
         * The method shows the "Please wait..." loader, but does not call onDelete
         * or move the browser to listUrl. The caller should take care
         * of the UX actions after delete if necessary.
         *
         * @param id: Identifier of the item to be deleted.
         * @returns Promise that resolves to true (never false). Fails if the deletion fails.
         */
        deleteNoConfirm(id: string): Promise<boolean>;
    }
}
declare module "services/ui/UiHelpers" {
    import * as angular from "angular";
    import { EditDialogResult } from "services/ui/EditDialogResult";
    /**
     * Provides misc. user interface helper methods that are related to AngularJS.
     */
    export class UiHelpers {
        private $uibModal;
        private $location;
        private $rootScope;
        private $window;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * @param $uibModal - Modal service.
         * @param $location - Angular location service.
         * @param $rootScope - Angular root scope service.
         * @param $window - Angular window service.
         */
        constructor($uibModal: angular.ui.bootstrap.IModalService, $location: angular.ILocationService, $rootScope: angular.IRootScopeService, $window: angular.IWindowService);
        /**
         * Shows an edit dialog - typically for a list item, but potentially for some other editable item.
         * Alternatively, you may use showDialog() if you wish to specify your own controller (with more logic)
         *
         * @param templateUrl - Template url.
         * @param item - The item that is being edited. The logic will support change tracking and reset on this item.
         * @param logic - Additional logic: Helper functions, metadata etc. that view can use to contruct the UI.
         * @param size - Optional size parameter: sm, lg or ''. Default is empty string (normal size).
         *
         * @returns A promise that resolves to the result from the dialog.
         *
         * @example
         *  this.uiHelpers.showDialog("salaxy-components/modals/account/AuthorizedAccountAdd.html", "AccountAuthorizationController")
         *
         */
        openEditDialog<TItem>(templateUrl: string, item: TItem, logic: any, size?: "sm" | "lg" | ""): Promise<EditDialogResult<TItem>>;
        /**
         * Shows a custom dialog using the given controller and templateUrl.
         * Optionally injects the given data into the controller as 'data'.
         * NOTE: There is an alternative - more simple - modal dialog pattern openEditDialog() if you do not need a custom controller or windowTemplateUrl.
         *
         * @param templateUrl - Template url.
         * @param controller  - Controller for the dialog.
         * @param data - Optional data to inject into the controller.
         * @param windowTemplateUrl - Optional template url for the window. Defaults to 'salaxy-components/modals/ui/DialogWindow.html'.
         * @param size - Optional size parameter: sm, lg or ''.
         *
         * @returns A promise that resolves to the result from the dialog.
         *
         * @example
         *  this.uiHelpers.showDialog("salaxy-components/modals/account/AuthorizedAccountAdd.html", "AccountAuthorizationController")
         *
         */
        showDialog(templateUrl: string, controller?: string | any, data?: any, windowTemplateUrl?: string, size?: "sm" | "lg" | ""): Promise<any>;
        /**
         * Shows a customized loading component (spinner) with an optional message.
         * Returns an object with a dismiss function to call for closing the dialog.
         * Note that there is a separate generic directive 'salaxy-loader' for indicating
         * http-traffic.
         *
         * @param heading Translation key for heading text for the loader
         * @param text Optional translation key for additional text paragraph describing what is happening.
         * @returns An object with dismiss function to close the dialog.
         *
         * @example
         * const loader = this.uiHelper.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
         * setTimeout(() => {
         *   loader.dismiss();
         * }, 2000);
         */
        showLoading(heading: string, text?: string): {
            /** Closes the dialog */
            dismiss: () => void;
        };
        /**
         * Shows a confirm dialog. Like window.confirm() in JavaScript.
         * Returns true if a user clicks OK -button. Otherwise (cancel or close) returns false.
         *
         * @param heading Heading text of the confirm question as translation key.
         * @param text Optional additional text paragraph describing what is happening.
         * @param okText Possibility to override the OK button text. The given text is run through translation.
         * @param cancelText Possibility to override the Cancel button text. The given text is run through translation.
         *
         * @returns A promise that resolves to true if user clicks OK, otherwise false.
         *
         * @example
         * this.uiHelpers
         *     .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.UI_TERMS.sureToDeleteRecord")
         *     .then((result) => { this.uiHelpers.showAlert("result", result); })
         */
        showConfirm(heading: string, text?: string, okText?: string, cancelText?: string): Promise<boolean>;
        /**
         * Shows an alert dialog. Like window.alert() in JavaScript.
         *
         * @param heading Heading text as translation key
         * @param text Optional additional text paragraph describing what is happening.
         * @param okText Possibility to override the button text. The given text is run through translation.
         *
         * @returns A promise with boolean: True if OK button was clicked, false if the dialog is closed / dismissed in another way.
         *
         * @example this.uiHelpers.showAlert("Please note", "There is this thing that we want to tell you.");
         */
        showAlert(heading: string, text?: string, okText?: string): Promise<boolean>;
        /**
         * Shows a dialog based on returnUrl parameter from the current url.
         */
        private showReturnUrlDialog;
        private getReturnUrlParameters;
        private init;
    }
}
declare module "services/ui/WizardService" {
    import * as angular from "angular";
    /**
     * Defines a step in wizard
     */
    export interface WizardStep {
        /** A number for the step, typically set by the counter */
        number?: number;
        /** Short title that is displayed with the Wizard buttons */
        title: string;
        /** If true, the step is active/selected. Typically, there is only one step active at any given time. */
        active?: boolean;
        /** If true, the step cannot be clicked/activated */
        disabled?: boolean;
        /** Path to the view that is shown in the wizard */
        view?: string;
        /** Path to the buttons view that is shown in the footer of the wizard */
        buttonsView?: string;
        /** Heading for the step */
        heading?: string;
        /** Intro text at the top of the wizard */
        intro?: string;
    }
    /**
     * Manages the state and pages of a Wizard that potentially has multiple
     * controllers views etc. in it.
     */
    export class WizardService {
        private $rootScope;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private steps;
        private activeStep;
        /**
         * Creates a new instance of WizardService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         */
        constructor($rootScope: angular.IRootScopeService, newSteps: WizardStep[]);
        /** Gets the Wizard steps as collection */
        getSteps(): WizardStep[];
        /**
         * Sets the Wizard steps
         *
         * @param newSteps - the new collection of steps for the wizard.
         * @param {boolean} skipNotify - If true will not send the the Notify event to Subscribed controllers.
         */
        setSteps(newSteps?: WizardStep[], skipNotify?: boolean): void;
        /** Gets the number of the currently active step */
        /** Sets the number of the currently active step */
        activeStepNumber: number;
        /**
         * Gets the current step object
         */
        getCurrentStepObject(): WizardStep;
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        private notify;
    }
}
declare module "services/ui/index" {
    export * from "services/ui/AlertService";
    export * from "services/ui/EditDialogKnownActions";
    export * from "services/ui/EditDialogResult";
    export * from "services/ui/NaviService";
    export * from "services/ui/Ng1Translations";
    export * from "services/ui/UiHelpers";
    export * from "services/ui/WizardService";
}
declare module "services/AbsencesService" {
    import { Absences, WorkerAbsences } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /** Service for holiday years. */
    export class AbsencesService extends BaseService<WorkerAbsences> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, absencesApi: Absences);
        /**
         * Gets a WorkerAbsences for the given worker ID.
         * If the object does not exist, but the initial data is already loaded from the server,
         * a new one is created. Note that it is not yet added to the list or set as current.
         */
        getForWorker(workerId: string): WorkerAbsences;
    }
}
declare module "services/AccountService" {
    import * as angular from "angular";
    import { AccountProducts, Accounts, Product } from "@salaxy/core";
    import { SessionService } from "services/SessionService";
    import { AlertService } from "services/ui/index";
    /**
     * Helps in managing the login process and provides information of the current session
     */
    export class AccountService {
        private $rootScope;
        private alertService;
        private sessionService;
        private accountApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Currently selected product in the user interface
         */
        currentProduct: any;
        /**
         * Product information/edit is typically shown in a modal dialog (UI Bootstrap).
         * This is the place to store reference to it if one is open (should be away when navigating to info page).
         */
        modalDialog: any;
        private currentProductKey;
        private productData;
        /**
         * List of available shops.
         */
        private shops;
        /**
         * Creates a new instance of AccountService.
         *
         * @param $rootScope - Angular root scope. Used for event routing.
         * @param alertService - Alert service for pop-up dialogs.
         * @param sessionService - Session service notifies when the user is known to be authenticated.
         * @param accountApi - The Accounts API that is used to communicating with the server.
         */
        constructor($rootScope: angular.IRootScopeService, alertService: AlertService, sessionService: SessionService, accountApi: Accounts);
        /**
         * Refreshes the set of products from the server.
         *
         * @returns A Promise with result data (AccountProducts)
         */
        refreshProductsFromServer(): Promise<AccountProducts>;
        /**
         * Gets a specific product or null if not available for the current user
         *
         * @param id - Identifier of the product
         * @returns The product with given id
         */
        getProduct(id: string): Product;
        /**
         * Sets the currentProduct to a product specified by id.
         *
         * @param key - Identifier for the product
         */
        setCurrentProduct(key: string): void;
        /**
         * Use setCurrentProduct instead
         * @deprecated
         */
        setCurrent(key: string): void;
        /**
         * Save all products
         *
         * @returns A Promise with result data (AccountProducts)
         */
        saveProducts(products: any): Promise<AccountProducts>;
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        /**
         * Get list of available products, or an empty object
         */
        getAvailableProducts(): AccountProducts;
        /**
         * List of Shop-in-Shop modals
         */
        getShops(): {
            /** Title for the shop. */
            title: string;
            /** Id for the shop. */
            id: string;
            /** Image url. */
            img: string;
            /** Description for the shop. */
            description: string;
            /** Current status for the shop. */
            status: "pending" | "new" | "ready";
            /** Type of the shop. */
            type: "product" | "shop" | "external";
            /** Template url for the html. */
            templateUrl?: string;
            /** Controller for the shop. */
            controller?: string;
            /** External url. */
            url?: string;
            /** Template url for the dialog window. */
            windowTemplateUrl?: string;
        }[];
        private notify;
        private resetUnvisitedProducts;
    }
}
declare module "services/AuthorizedAccountService" {
    import * as angular from "angular";
    import { AuthorizedAccounts, Avatar, CompanyAccount, PersonAccount } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD functionality for the authorized accounts. Additionally listing of authorizing accounts.
     */
    export class AuthorizedAccountService extends BaseService<Avatar> {
        private authorizedAccountsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        private authorizingAccounts;
        private currentPartnerSite;
        /**
         * List of all possible partner sites
         *
         * Partner sites are services that have Salaxy or Palkkaus functionality in them. Others are services that have a different sign-in method,
         * and these services need an authorization to access user's Salaxy/Palkkaus account. Others use the Salaxy/Palkkaus sign-in method, so they don't
         * need separate authorization. Due to some services needing an authorization and others not needing it, all partner services need to be stored in a
         * separate property, simply listing authorizations does not suffice.
         *
         * Partner services that need an authorization have a property accountId, that is the IBAN for their Salaxy acccount
         */
        private partnerSiteList;
        /**
         * List of Accountant partner sites
         */
        private partnerAccountantSiteList;
        /**
         * List of Software/Integration partner sites
         */
        private partnerIntegrationSiteList;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, authorizedAccountsApi: AuthorizedAccounts);
        /**
         * Refreshes existing authorizing accounts.
         *
         * @returns A Promise with result data (PersonAccount|CompanyAccount)[]
         */
        reloadAuthorizingAccounts(): Promise<Array<(PersonAccount | CompanyAccount)>>;
        /**
         * Lists all accounts on behalf of which this account has been authorized to act.
         *
         * @returns A Promise with result data (PersonAccount|CompanyAccount)[]
         */
        getAuthorizingAccounts(): Array<(PersonAccount | CompanyAccount)>;
        /**
         * Sets the current partner site
         * @param partnerSite A partner site to be set as current
         */
        setCurrentPartnerSite(partnerSite: any): void;
        /**
         * Returns the current partner site
         */
        getCurrentPartnerSite(): any;
        /**
         * Return a list of all possible partner sites
         *
         * Partner sites are services that have Salaxy or Palkkaus functionality in them. Others are services that have a different sign-in method,
         * and these services need an authorization to access user's Salaxy/Palkkaus account. Others use the Salaxy/Palkkaus sign-in method, so they don't
         * need separate authorization. Due to some services needing an authorization and others not needing it, all partner services need to be stored in a
         * separate property, simply listing authorizations does not suffice.
         *
         * Partner services that need an authorization have a property accountId, that is the IBAN for their Salaxy acccount
         *
         * @returns List of partner site objects.
         */
        getPartnerSiteList(): any;
        /**
         * Return a list of all possible Accountant partner sites
         *
         * Partner sites are services that have Salaxy or Palkkaus functionality in them. Others are services that have a different sign-in method,
         * and these services need an authorization to access user's Salaxy/Palkkaus account. Others use the Salaxy/Palkkaus sign-in method, so they don't
         * need separate authorization. Due to some services needing an authorization and others not needing it, all partner services need to be stored in a
         * separate property, simply listing authorizations does not suffice.
         *
         * Partner services that need an authorization have a property accountId, that is the IBAN for their Salaxy acccount
         *
         * @returns List of partner site objects.
         */
        getPartnerAccountantSiteList(): any;
        /**
         * Return a list of all possible Integration partner sites
         *
         * Partner sites are services that have Salaxy or Palkkaus functionality in them. Others are services that have a different sign-in method,
         * and these services need an authorization to access user's Salaxy/Palkkaus account. Others use the Salaxy/Palkkaus sign-in method, so they don't
         * need separate authorization. Due to some services needing an authorization and others not needing it, all partner services need to be stored in a
         * separate property, simply listing authorizations does not suffice.
         *
         * Partner services that need an authorization have a property accountId, that is the IBAN for their Salaxy acccount
         *
         * @returns List of partner site objects.
         */
        getPartnerIntegrationSiteList(): any;
        /**
         * Returns the partner site with given id
         *
         * @param id The name of the partner site
         *
         * @returns Partner site object with the given id, or undefined.
         */
        getPartnerSite(id: any): any;
    }
}
declare module "services/HolidayYearsService" {
    import { HolidayYear, HolidayYears } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /** Service for holiday years. */
    export class HolidayYearsService extends BaseService<HolidayYear> {
        private holidayYearsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, holidayYearsApi: HolidayYears);
        /**
         * Initializes (or re-creates) the holiday years for a Worker.
         * @param workerId Identifier of the Worker to initialize
         * @returns All the holiday years (for other Workers as well) as saved into the database.
         */
        initWorker(workerId: string): Promise<HolidayYear[]>;
        /**
         * Gets the previous year from the current year for the same Worker.
         * Null if there is no previous year.
         * @param year Current year for which the previous year is fetched.
         */
        getPreviousYear(year: HolidayYear): HolidayYear;
        /**
         * Gets a Holiday year for the given worker ID and year.
         */
        getForWorker(workerId: string, year: number): HolidayYear;
    }
}
declare module "services/ReportsService" {
    import { AccountingReportTableType, calcReportType, Calculation, PeriodType, Report, reportPartial, Reports, ReportType } from "@salaxy/core";
    import { SessionService } from "services/SessionService";
    import { UiHelpers } from "services/ui/index";
    /**
     * Methods for viewing and later also generating different reports.
     */
    export class ReportsService {
        private $rootScope;
        private sessionService;
        private uiHelpers;
        private $sce;
        private reportsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Convenience property for navigation controls to set a current report type.
         * Checked, when getReports is called with null or "current"
         */
        currentReportType: ReportType;
        /** Cache for reports-by-type as generated on the server */
        private reportMetadataCache;
        /**
         * Creates a new instance of ReportsService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param sessionService - Session service notifies when the user is known to be authenticated.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param $sce - $sce is a service that provides Strict Contextual Escaping services to AngularJS
         * @param reportsApi - The Reports API that is used to communicating with the server.
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, uiHelpers: UiHelpers, $sce: angular.ISCEService, reportsApi: Reports);
        /**
         * Gets a list of reports (metadata only) filtered by a report type.
         *
         * @param type - Type of report. See type (string enumeration) for possible values.
         *          Also supports value "current" for showing reports defined by currentReportType.
         *
         * @returns A Promise with result data (<Report[]) if dataset not fetched, else the fetched dataset
         */
        getReports(type: ReportType | "current"): Promise<Report[]> | Report[];
        /**
         * Gets a link URL for a yearly report. This is a full link with token and absolute URL.
         *
         * @param type - Type of the report must be one of the yearly reports
         * @param year - Year for the report
         * @param id - Worker ID for those reports that are specific to one Worker.
         * @param id2 - Second Worker ID for those reports that have two Workers in one report
         *
         * @returns Yearly report URL string
         */
        getYearlyReportUrl(type: ReportType, year: number, id?: string, id2?: string): string;
        /**
         * Gets a URL for a calculation report.
         * If report has not been saved (ID is null), returns null.
         *
         * @param {calcReportType} reportType - Type of report
         * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
         *
         * @returns Url for specified report
         */
        getReportUrlForCalc(reportType: calcReportType, calcId: string): string;
        /**
         * Show a report modal (a preview modal dialog) for a given calculation.
         * The calculation may not be stored to back end. I.e. this reporting method is available also to
         * non-authenticated users unlike the more resource intensive methods that generate PDF-files and require CRUD rights.
         */
        showReportModalForCalc(reportType: calcReportType, calculation: any): void;
        /**
         * Returns html content for a report by calculation id.
         *
         * @param reportType - Type of report.
         * @param page - Page number of the report, optional.
         *
         * @returns A Promise with result data (report HTML string)
         */
        getReportHtmlForCalculationId(reportType: reportPartial, calculationId: string): Promise<string>;
        /**
         * Returns html content for a posted calculation.
         */
        getReportHtmlForCalculation(reportType: calcReportType, calculation: any, callback: (html: string, slot: number) => void): void;
        /**
         * Gets the monthly / quarterly/ yearly accounting data for the current account.
         *
         * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
         * @param tableType  - Accounting table type.
         * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
         * @param endDate - End date for the period. Required only for the custom period.
         * @returns A Promise with result data (Raw data for accounting purposes).
         */
        getAccountingReportTableForPeriod(refDate: string, tableType?: AccountingReportTableType, periodType?: PeriodType, endDate?: string): Promise<any>;
        /**
         * Gets the accounting report based on given set of calculations.
         *
         * @param calculationIds - Calculations that are the bases for the report.
         * @param tableType  - Accounting table type.
         * @returns Account report based on the calculations.
         * @ignore - experimental
         */
        getAccountingReportTableForCalculationIds(calculationIds: string[], tableType?: AccountingReportTableType): Promise<any>;
        /**
         * Gets the accounting report based on given set of calculations.
         *
         * @param calculations - Calculations that are the bases for the report.
         * @param tableType  - Accounting table type.
         * @returns Account report based on the calculations.
         * @ignore - experimental
         */
        getAccountingReportTableForCalculations(calculations: Calculation[], tableType?: AccountingReportTableType): Promise<any>;
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        /**
         * Shows the report modal dialog
         *
         * @param html1 - The report HTML to show in the dialog
         * @param html2 - Optional second page
         */
        private modalDialogContentDone;
        private notify;
    }
}
declare module "services/CalculationsService" {
    import * as angular from "angular";
    import { calcReportType, Calculation, CalculationRowType, Calculations, Calculator, CalcWorktime, HolidayYear, SalaryKind, UnionPaymentType, WorkerAbsences } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { ReportsService } from "services/ReportsService";
    import { SessionService } from "services/SessionService";
    /**
     * Handles operations on calculations owned by the current user.
     */
    export class CalculationsService extends BaseService<Calculation> {
        private calculationsApi;
        private calculatorApi;
        private reportsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        private reportCache;
        /** Validation object for the current calculation */
        private currentValidation;
        /**
         * Creates a new instance of CalculationsService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param sessionService - Session service notifies when the user is known to be authenticated.
         * @param calculationsApi - The Calculations API that is used for CRUD operations (authenticated methods)
         * @param calculatorApi - The Calculator API that is used for salary calculation (anonymous methods)
         * @param reportsService - Reports service.
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, calculationsApi: Calculations, calculatorApi: Calculator, reportsService: ReportsService);
        /**
         * Get the report of given type from reports cache.
         *
         * @param reportType - Type of report. See type (string enumeration) for possible values.
         *
         * @returns Report HTML string
         */
        getReportHtml(reportType: calcReportType): string;
        /**
         * Sets the given calculation as current.
         *
         * @param calculation - Calculation to be set as current.
         */
        setCurrent(calculation: Calculation): void;
        /**
         * Saves the current calculation to database.
         *
         * @returns A Promise with result data (Calculation)
         */
        saveCurrent(): Promise<Calculation>;
        /**
         * Copies the current calculation as a new draft.
         */
        copyAsNew(calculation: Calculation): Calculation;
        /** copyAsNew implemented in a way that it waits for recalculation. */
        copyAsNewWithResult(calculation: Calculation): Promise<Calculation>;
        /**
         * Reloads the current calculation from the server.
         *
         * @returns A Promise with result data
         */
        reloadCurrent(): Promise<Calculation>;
        /**
         * Recalculates the current calculation
         * @returns A Promise with result data (Calculation)
         */
        recalculate(): Promise<Calculation>;
        /**
         * Recalculates the holidays and absences for the current calculation.
         * This is stand-alone anonymous method (comparable to recalculated) - Does not read / write from database.
         * @param worktime Worktime object that is set to the calculation that is tehn recalculated.
         * @returns A Promise with result data (Calculation)
         */
        recalculateWorktime(worktime: CalcWorktime): Promise<Calculation>;
        /**
         * Gets necessary parameters for Period recalculation from the current  calc, holidays andabsences data.
         * This is an anonymous method that can be used for unit testing: Does not read / write from database.
         * @param holidayYear Holiday year with holidays that should be added as salary rows. If not provided, holidays are not calculated.
         * @param absences Absences that should be deducted from monthly salary. If not provided, absences are not calculated.
         *
         * @returns A Promise with the recalculation parameters.
         */
        getWorktimeData(holidayYear?: HolidayYear, absences?: WorkerAbsences): Promise<any>;
        /**
         * Set a household deduction (on/off) to a calculation
         *
         * @param {boolean} isHouseholdDeduction
         *
         * @returns A Promise with result data (Calculation)
         */
        setHouseholdDeduction(isHouseholdDeduction: boolean): Promise<Calculation>;
        /**
         * Sets a unionPayment to a calculation
         *
         * @param {UnionPaymentType} paymentType
         * @param {number} unionPaymentPercent
         *
         * @returns A Promise with result data (Calculation)
         */
        setUnionPayment(paymentType: UnionPaymentType, unionPaymentPercent: number): Promise<Calculation>;
        /**
         * Updates the salary of the current calculation and recalculates
         *
         * @param kind - The type of the salary.
         * @param amount - The amount of typically hours (could be months etc.). Defaults to 1 if left as null.
         * @param price - The price of one unit of amount.
         *
         * @returns A Promise with result data (Calculation)
         */
        calculateSalary(kind: SalaryKind, amount: number, price: number, message: string): Promise<Calculation>;
        /**
         * Adds a calculation row: An expenses, benefits or other such row and recalculates.
         *
         * @param rowType - Type of the calculation row that should be added.
         * @param message - Message text for end user - used in salary calculation
         * @param price - Price for the row (e.g. €/h, €/km, €/month. Default is 0.
         * @param count - Count of the items - default is 1.
         *
         * @returns A Promise with result data (Calculation)
         */
        addRow(rowType: CalculationRowType, message: string, price: number, count: number): Promise<Calculation>;
        /**
         * Deletes a calculation row: Expenses, benefits etc. item and recalculates.
         *
         * @param rowIndex - Zero based row index of the row that should be deleted.
         *
         * @returns A Promise with result data (Calculation)
         */
        deleteRow(rowIndex: number): Promise<Calculation>;
        /**
         * Updates the current calculation with the Worker as a specified Person
         *
         * @param profileId - The Identifier of the Profile that should be set as Worker.
         *
         * @returns A Promise with result data (Calculation)
         */
        workerSet(profileId: string): Promise<Calculation>;
        /**
         * Refreshes current calculation report from server.
         * @param reportType - Type of calc report. See type (string enumeration) for possible values.
         * @returns A Promise with result data (preview HTML string) TODO void
         */
        refreshReport(reportType: calcReportType): void;
        /**
         * Gets a calculation from the local storage
         *
         * @returns Salary calculations from local storage or null.
         */
        private getFromLocalStorage;
        /**
         * Saves a calculation to local storage
         *
         * @param {any} calc - Calculation to store
         */
        private setToLocalStorage;
        /**
         * Deletes a calculation in local storage
         */
        private deleteFromLocalStorage;
        private resetReportHtml;
    }
}
declare module "services/CalcRowsService" {
    import { Calculation, UserDefinedRow } from "@salaxy/core";
    import { CalculationsService } from "services/CalculationsService";
    import { IService } from "services/IService";
    /**
     * Implements an IService for collection of user defined rows in the
     * current calculation singleton (in Calculation service).
     */
    export class CalcRowsService implements IService<UserDefinedRow> {
        private calculationsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Current row. */
        readonly current: UserDefinedRow;
        private _current;
        constructor(calculationsService: CalculationsService);
        /** Returns the current list of rows. */
        readonly list: UserDefinedRow[];
        /** Returns a new blank row. */
        getBlank?(): UserDefinedRow;
        /** Copies the given row and returns a copy. */
        copyAsNew(copySource: UserDefinedRow): UserDefinedRow;
        /** Recalculates the underlying calculation and returns the list of rows. */
        reloadList(): Promise<UserDefinedRow[]>;
        /**
         * Sets the given id as current row.
         *
         * @param id Id for the row.
         */
        setCurrentId(id: string): void;
        /**
         * Sets the given row as current row.
         *
         * @param item The item to be set as current.
         */
        setCurrent(item: UserDefinedRow): void;
        /** Creates a new item and sets it as current. */
        newCurrent?(): void;
        /** Saves the current row. */
        saveCurrent(): Promise<UserDefinedRow>;
        /** Saves the given row.
         *
         * @param item The item to be saved.
         * @returns Promise for the saved item.
         */
        save(item: UserDefinedRow): Promise<UserDefinedRow>;
        /**
         * Deletes the given row.
         *
         * @param id The id for the row.
         * @returns Promise for the deletion message.
         */
        delete?(id: any): Promise<string>;
        /**
         * Called when the current row changes. Currently not supported.
         */
        onChange(scope: any, callback: any): void;
        /** Gets the underlying Calculation - contains the listed rows. */
        getCalc(): Calculation;
    }
}
declare module "services/CertificateService" {
    import * as angular from "angular";
    import { Certificate, Certificates } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD functionality for the Payroll objects and functionality for payroll in general.
     */
    export class CertificateService extends BaseService<Certificate> {
        private certificateApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, certificateApi: Certificates);
    }
}
declare module "services/CmsService" {
    import { AbcSection, Article, Cms } from "@salaxy/core";
    import * as angular from "angular";
    /**
     * Loads Articles and metadata related to them from Palkkaus.fi Content Management System (CMS).
     */
    export class CmsService {
        private $rootScope;
        private $sce;
        private cmsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private articleListCache;
        private loadedArticlesCache;
        private currentArticleId;
        /**
         * Creates a new instance of CmsService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param $sce - $sce is a service that provides Strict Contextual Escaping services to AngularJS
         * @param cmsApi - The CMS API that is used to communicating with the server.
         */
        constructor($rootScope: angular.IRootScopeService, $sce: angular.ISCEService, cmsApi: Cms);
        /**
         * Sets the current article ID - typically based on routing.
         *
         * @param articleId - Identifier for the new current article
         */
        setCurrent(articleId: string): void;
        /**
         * Gets the current article if it has been loaded from the server.
         *
         * @param articleLinkPattern - Optional pattern for CMS links from one article to another.
         *          The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         *          The caller can change this to direct internal links to appropriate page template.
         *
         * @returns A Promise with result data (Article) if not fetched yet, else the fetched Article
         */
        getCurrentArticle(articleLinkPattern?: string): Promise<Article> | Article;
        /**
         * Gets an article or starts the loading for an article (watch for subscribe event to actually get the article).
         *
         * @param articleId - Identifier for the article
         * @param articleLinkPattern - Optional pattern for CMS links from one article to another.
         *          The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         *          The caller can change this to direct internal links to appropriate page template.
         *
         * @returns A Promise with result data (Article) if not fetched yet, else the fetched Article
         */
        getArticle(articleId: string, articleLinkPattern?: string): Promise<Article> | Article;
        /**
         * Gets a list of articles from the server.
         * This list does not contain the article contents, just the metadata.
         *
         * @param section - Section to filter by. If null, all sections are returned.
         *
         * @returns filtered or full list of articles
         */
        getArticleList(section: AbcSection): Article[];
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        private notify;
    }
}
declare module "services/ContactService" {
    import { Contacts, ContractParty } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * Service for contact management.
     */
    export class ContactService extends BaseService<ContractParty> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, contactsApi: Contacts);
    }
}
declare module "services/ContractService" {
    import * as angular from "angular";
    import { Contracts, EmploymentContract } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * Service for contracts management.
     */
    export class ContractService extends BaseService<EmploymentContract> {
        private contractsApi;
        private $sce;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        private currentPreview;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, contractsApi: Contracts, $sce: angular.ISCEService);
        /** Returns the preview for the current contract. */
        getCurrentPreview(): string;
        /**
         * Get current contract from server and (re)load in current preview
         */
        refreshPreview(): void;
        /**
         * Creates a new contract for the current user and sets it as current.
         * Takes into account the role and sets the current account aither as a Worker or Employer automatically.
         */
        newCurrent(): void;
    }
}
declare module "services/CredentialService" {
    import * as angular from "angular";
    import { Credentials, SessionUserCredential } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD functionality for the Credentials objects and functionality for Credentials in general.
     */
    export class CredentialService extends BaseService<SessionUserCredential> {
        private credentialsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, credentialsApi: Credentials);
    }
}
declare module "services/EarningsPaymentService" {
    import * as angular from "angular";
    import { Calculation, EarningsPayments, nir } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD functionality for the EarningsPayment objects and functionality for EarningsPayment in general.
     */
    export class EarningsPaymentService extends BaseService<nir.EarningsPayment> {
        private earningsPaymentApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Calculation for the Earnings Payment */
        calc: Calculation | "current";
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, earningsPaymentApi: EarningsPayments);
        /**
         * Validates a EarningsPayment object for delivery.
         * @param earningsPaymentToValidate EarningsPayment or null for current EarningsPayment that should be validated.
         * @returns The validated EarningsPayment object.
         */
        validate(earningsPaymentToValidate?: nir.EarningsPayment): Promise<nir.EarningsPayment>;
    }
}
declare module "services/EmailMessageService" {
    import { EmailMessage, EmailMessages } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /** Service for email messages and sending emails. */
    export class EmailMessageService extends BaseService<EmailMessage> {
        private emailMessagesApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, emailMessagesApi: EmailMessages);
    }
}
declare module "services/ESalaryPaymentService" {
    import * as angular from "angular";
    import { ESalaryPayment, ESalaryPayments } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD operations and other actions on ESalaryPayments.
     */
    export class ESalaryPaymentService extends BaseService<ESalaryPayment> {
        private eSalaryPaymentsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        /**
         * @param $rootScope - Angular root scope. Used for event routing
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, eSalaryPaymentsApi: ESalaryPayments);
        /**
         * Gets the payment address for the E-salary payment.
         * @param eSalary The E-salary for which to fetch the Payment address.
         */
        getPaymentAddress(eSalary: ESalaryPayment): string;
    }
}
declare module "services/IfInsuranceService" {
    import * as angular from "angular";
    import { IfInsuranceOrder, PartnerServices } from "@salaxy/core";
    import { AccountService } from "services/AccountService";
    import { SessionService } from "services/SessionService";
    /**
     * If insurance service. Contains methods for getting order template, validating the order
     * and sending the order.
     */
    export class IfInsuranceService {
        private $rootScope;
        private sessionService;
        private partnerServicesApi;
        private accountService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Current order  */
        private current;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, partnerServicesApi: PartnerServices, accountService: AccountService);
        /**
         * Sends order for processing.
         */
        send(order?: IfInsuranceOrder): Promise<IfInsuranceOrder>;
        /** Lazy loading of current pension order template */
        getCurrent(): IfInsuranceOrder;
        /**
         * Resets the current order.
         */
        reset(): void;
    }
}
declare module "services/OnboardingService" {
    import * as angular from "angular";
    import { Ajax, Onboarding, Onboardings } from "@salaxy/core";
    import { SessionService } from "services/SessionService";
    import { UiHelpers, WizardService } from "services/ui/index";
    /**
     * Manages the onboarding process where an account is created.
     */
    export class OnboardingService {
        private $rootScope;
        private $sce;
        private onboardingsApi;
        private sessionService;
        private ajax;
        private wizardService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** The current onboarding model */
        model: Onboarding;
        /** Unique identifier of the onboarding data in storage */
        id: string;
        /** Server address for onboarding pages etc. */
        serverAddress: string;
        /**
         * Creates a new instance of AccountService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param $sce - Strict Contextual Escaping service.
         * @param onboardingsApi - The Onboardings API that is used to communicating with the server.
         * @param sessionService - Session service notifies when the user is known to be authenticated.
         * @param ajax - Ajax service.
         * @param wizardService - Wizard service.
         * @param uiHelpers - Salaxy ui helpers service.
         */
        constructor($rootScope: angular.IRootScopeService, $sce: angular.ISCEService, onboardingsApi: Onboardings, sessionService: SessionService, ajax: Ajax, wizardService: WizardService, uiHelpers: UiHelpers);
        /** Gets / refreshes the onboarding data from the server  */
        getOnboardingData(id?: string): Promise<Onboarding>;
        /**
         * Sets the personName in signature object
         * @todo check if this method is really needed.
         *
         * @param personName - Name (first and last name) that is set to signature object in onboarding data
         */
        setSignatureName(personName: string): void;
        /**
         * Sets the email in signature object
         * * @todo check if this method is really needed.
         *
         * @param email - Email that is set to signature object in onboarding data
         */
        setSignatureEmail(email: string): void;
        /**
         * Sets the telephone number in signature object
         * * @todo check if this method is really needed.
         *
         * @param telephone - Telephone number that is set to signature object in onboarding data
         */
        setSignaturePhone(telephone: string): void;
        /**
         * Sets the isPensionSelfHandling property in pension product to given boolean parameter
         *
         * @param choice - Boolean parameter to which isPensionSelfHandling in pension product is set
         */
        setPensionSelfHandlingTo(choice: boolean): void;
        /**
         * Saves the current onboarding to database.
         *
         * @returns A Promise with result data (Onboarding)
         */
        save(): Promise<Onboarding>;
        /**
         * Sends a new pin code for verifying the telephone number in onboarding.
         * Uses onboarding model stored in this service.
         *
         * @returns A Promise with result data (true if sending succeeded).
         */
        sendSmsVerificationPin(): Promise<boolean>;
        /**
         * Checks the pin code.
         *
         * @returns A Promise with result data (true if sending succeeded).
         */
        checkSmsVerificationPin(): Promise<boolean>;
        /**
         * Opens company wizard as modal dialog.
         * @param accountId - Optional accountId for existing company.
         */
        launchCompanyWizard(accountId?: string): Promise<any>;
        /**
         * Opens worker wizard as modal dialog.
         * @param id - Optional onboarding id for existing worker.
         * @param isModelLoaded  - If true, the method does not load the model from backend any more.
         */
        launchWorkerWizard(id?: string, isModelLoaded?: boolean): Promise<any>;
        /**
         * Opens generic account wizard as modal dialog.
         * @param id - Optioanl onboarding id.
         * @param isModelLoaded  - If true, the method does not load the model from backend any more.
         */
        launchAccountWizard(id?: string, isModelLoaded?: boolean, resetAccountType?: boolean): Promise<any>;
        /** Updates account wizard steps. */
        setAccountWizardSteps(): void;
        /**
         * Return Visma sign url for current model.
         * @param authService Select the signature service (bank / mobile auth).
         */
        getVismaSignUrl(authService: string): string;
        /**
         * Commits current onboarding  and creates/changes the account.
         *
         * @returns A Promise with result data (Onboarding)
         */
        commit(): Promise<Onboarding>;
        /**
         * Creates a new company onboarding object and sets it as current.
         * @param accountId - Optional account id for existing account.
         */
        getCompanyOnboardingData(accountId?: string): Promise<Onboarding>;
        /**
         * Gets the PDF preview address taking into account potential changes in the model.
         *
         * @returns Url address
         */
        getPdfPreviewAddress(): string;
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        private notify;
        private getServerAddress;
        /**
         * Opens generic account wizard as modal dialog.
         * @param id - Optioanl onboarding id.
         */
        private launchGenericWizard;
    }
}
declare module "services/PayrollService" {
    import * as angular from "angular";
    import { Payroll, Payrolls } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /**
     * CRUD functionality for the Payroll objects and functionality for payroll in general.
     */
    export class PayrollService extends BaseService<Payroll> {
        private payrollApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Payroll that is being validated.
         * This is null when validation is in progress or if validation has not been started.
         */
        validatedPayroll: Payroll;
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, payrollApi: Payrolls);
        /**
         * Creates a copy of a given payroll and sets it as current.
         * @param payroll Payroll to copy as new.
         */
        copyAsNew(payroll: Payroll): Payroll;
        /**
         * Validates the current Payroll object and sets it as validatedPayroll.
         */
        validateCurrent(): void;
        /**
         * Validates a Payroll object for payment.
         * @param payrollToValidate Payroll that should be validated.
         * @returns The Payroll object with Calculations added to the Result object and Validated.
         */
        validate(payrollToValidate: Payroll): Promise<Payroll>;
    }
}
declare module "services/PaymentService" {
    import { Ajax, BankPaymentType, Calculation, Calculations, Invoice, Payment, Payments, Payroll, Payrolls, PeriodType } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { CalculationsService } from "services/CalculationsService";
    import { PayrollService } from "services/PayrollService";
    import { SessionService } from "services/SessionService";
    /**
     * Service for payment processing.
     */
    export class PaymentService extends BaseService<Payment> {
        private paymentsApi;
        private calculationsApi;
        private payrollApi;
        private calculationsService;
        private payrollService;
        private $location;
        private ajax;
        /**
         * For NG-dependency injection.
         * @ignore
         */
        static $inject: string[];
        /** Business object for the payment. */
        businessObject: Payroll | Calculation;
        /** Indicates if the payment is for payroll. */
        isPayrollPayment: boolean;
        /** Event prefix */
        protected eventPrefix: string;
        /**
         * Creates a new instance of the PaymentsService.
         * @param $rootScope - Angular root scope.
         * @param sessionService - Session service.
         * @param paymentsApi - The Payments API for creating and paying payments.
         * @param calculationsApi - The Calculations API for fetching calculations.
         * @param payrollApi - The Payroll API for fetching payrolls.
         * @param calculationsService - Calculations service.
         * @param payrollService - Payroll service.
         * @param $location - angular $location service.
         * @param ajax - Ajax service.
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, paymentsApi: Payments, calculationsApi: Calculations, payrollApi: Payrolls, calculationsService: CalculationsService, payrollService: PayrollService, $location: angular.ILocationService, ajax: Ajax);
        /** Total payment for the business object. */
        getTotalPayment(): number;
        /**
         * Returns paytrail payment url.
         * @param returnUrl - Return url after the payment.
         */
        getPaytrailPaymentUrl(returnUrl?: string): string;
        /**
         * Sets the business object for the payment.
         * By default the current calculation is set as business object.
         * @param isPayrollPayment - If true, sets the current payroll object as business object.
         */
        setBusinessObject(isPayrollPayment: boolean): void;
        /**
         * Saves the business object.
         * @returns - Saved business object.
         */
        saveBusinessObject(): Promise<Payroll | Calculation>;
        /**
         * Starts the payment by updating/creating the payment for given business object and sets the payment as current payment.
         * @returns Created/updated payment.
         */
        startPayment(): Promise<Payment>;
        /**
         * Starts the payment by updating/creating the payment for given business object and sets the payment as current payment.
         * Generates and sends a Finvoice.
         * @returns  Finvoice which was sent.
         */
        sendFinvoice(): Promise<Invoice>;
        /**
         * Lists the monthly / quarterly/ yearly employer payments for the current account.
         *
         * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
         * @param paymentTypes - Payment types to list.
         * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
         * @param endDate - End date for the period. Required only for the custom period.
         * @returns A Promise containing payments.
         */
        getEmployerPayments(refDate: string, paymentTypes?: BankPaymentType[], periodType?: PeriodType, endDate?: string): Promise<Payment[]>;
    }
}
declare module "services/ProfilesService" {
    import * as angular from "angular";
    import { City, Contact, Profile, ProfileJob, ProfileJobCategory, Profiles } from "@salaxy/core";
    /**
     * Stateful services that use PalkkausApi methods to communicate to Palkkaus.fi API and manage the resulting state.
     * Designed to be injected into Angular 1 projects.
     */
    /** Search parameters for public Profiles searches */
    export interface ProfileSearchParameters {
        /** Job category - e.g. 'raksa'  */
        category?: string;
        /** City or community in Finland */
        city?: string;
        /** Job */
        job?: string;
        /** Free seach text */
        searchText?: string;
        /** If true, the search is the initial filter for the service */
        isInitial?: boolean;
    }
    /** The searches and search parameters for Public profiles (Osaajaprofiilit) */
    export class ProfilesService {
        private $rootScope;
        private profilesApi;
        private initialFilter?;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private currentProfileId;
        private currentProfile;
        private currentSearch;
        private nextSearch;
        private profiles;
        private cities;
        private jobs;
        private categories;
        private count;
        private allCities;
        private citiesForEdit;
        private allJobs;
        private jobsForEdit;
        private allCategories;
        private allCount;
        private myProfile;
        private contact;
        private initialFilterHasCategory;
        /**
         * Creates a new instance of ProfilesService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param profileApi - The Profiles API that is used to communicating with the server.
         * @param initialFilter - The initial set of filters. In the beginning the service fetches the cities, jobs and categories based on this initial filter.
         */
        constructor($rootScope: angular.IRootScopeService, profilesApi: Profiles, initialFilter?: ProfileSearchParameters);
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param scope - Controller scope for the subscribing controller (or directive etc.)
         * @param listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: any, callback: any): void;
        /** Returns profiles. */
        getProfiles(): Profile[];
        /** Returns current profile. */
        getCurrentProfile(): Profile;
        /** Returns current profile id. */
        getCurrentProfileId(): string;
        /** Returns current search parameters. */
        getCurrentSearch(): ProfileSearchParameters;
        /** Returns next search parameters. */
        getNextSearch(): ProfileSearchParameters;
        /** Returns all cities. */
        getAllCities(): City[];
        /** Returns cities for edit. */
        getCitiesForEdit(): Promise<City[]> | City[];
        /** Returns all jobs. */
        getAllJobs(): ProfileJob[];
        /** Returns jobs for edit. */
        getJobsForEdit(): ProfileJob[];
        /** Returns all categories. */
        getAllCategories(): ProfileJobCategory[];
        /** Returns all profile count. */
        getAllCount(): number;
        /** Returns cities. */
        getCities(): City[];
        /** Returns jobs. */
        getJobs(): ProfileJob[];
        /** Returns categories. */
        getCategories(): ProfileJobCategory[];
        /** Returns count. */
        getCount(): number;
        /** Returns own profile. */
        getMyProfile(): Profile;
        /** Returns contact information. */
        getContact(): Contact;
        /**
         * Set intial search with profile search params
         */
        setInitialSearch(filter: ProfileSearchParameters): void;
        /**
         * Request profile with the given id from server and set as current.
         */
        setCurrent(profileId: string): void;
        /**
         * Makes a new search with given profile search params filter.
         */
        search(filter: ProfileSearchParameters): void;
        /**
         * Makes a new api call and sets the user's profile
         *
         * @returns A Promise with result data (Profile)
         */
        setMyProfile(): Promise<Profile>;
        /**
         * Saves current users profile
         *
         * @returns A Promise with result data (Profile)
         */
        saveMyProfile(): Promise<Profile>;
        /**
         * Returns the url where to post the avatar image file
         *
         * @returns Avatar image upload url string
         */
        getAvatarImageUploadUrl(): string;
        /**
         * Makes a new API call and sets a contact for a single profile.
         *
         * @param profileId - The identifier of the profile.
         *
         * @returns A Promise with result data (Contact)
         */
        setContact(profileId: string): Promise<Contact>;
        private notify;
    }
}
declare module "services/SignatureService" {
    import * as angular from "angular";
    /**
     * Defines a supported signature method.
     */
    export class SignatureMethod {
        /** Title for the signing method. */
        title?: string;
        /** Name for the signing method. */
        name: string;
        /** Id for the signing method. */
        value: string;
        /** Image url for the signing method. */
        img: string;
        /** Boolean to indicate that the method is popular and should be shown among first methods. */
        isPopular?: boolean;
    }
    /**
     * Handles Digital Signature scenarios to Onnistuu.fi-service.
     */
    export class SignatureService {
        private $rootScope;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private methods;
        /**
         * Creates a new instance of SignatureService
         *
         * @param $rootScope - Angular root scope. Used for event routing
         */
        constructor($rootScope: angular.IRootScopeService);
        /**
         * Gets a list of available signature methods. Use img-property to show
         * Note that the methods are only available after they have been loaded from the Onnistuu.fi server.
         */
        getMethods(): SignatureMethod[];
        /**
         * Controllers can subscribe to changes in service data using this method.
         * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
         *
         * @param {angular.IScope} scope - Controller scope for the subscribing controller (or directive etc.)
         * @param {function(event, ...args)} listener - The event listener function. See $on documentation for details
         */
        subscribe(scope: angular.IScope, listener: (event: angular.IAngularEvent, ...args: any[]) => any): void;
        private notify;
    }
}
declare module "services/SmsMessageService" {
    import { SmsMessage, SmsMessages } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    /** Service for SMS messages and sending SMS. */
    export class SmsMessageService extends BaseService<SmsMessage> {
        private smsMessagesApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, smsMessagesApi: SmsMessages);
    }
}
declare module "services/TaxCardService" {
    import * as angular from "angular";
    import { Ajax, PersonAccount, Taxcard, TaxcardKind, Taxcards, WorkerAccount } from "@salaxy/core";
    import { SessionService } from "services/SessionService";
    import { BaseService } from "services/BaseService";
    /**
     * CRUD functionality for the Taxcard objects and functionality for tax cards in general.
     */
    export class TaxCardService extends BaseService<Taxcard> {
        private taxCardsApi;
        private ajax;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        /**
         * Creates a new instance of TaxCardService.
         *
         * @param $rootScope - Angular root scope. Used for event routing
         * @param accountsApi - The Accounts API that is used to communicating with the server
         * @param ajax - The ajax object that has the token for this user and session. Token is needed in methods getTaxCardDownloadUrl() and getTaxCardPreviewUrl()
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, taxCardsApi: Taxcards, ajax: Ajax);
        /**
         * Returns tax cards for the current worker.
         * Note that this is only the new type of tax cards, not the old ones (before 2019) for backward compatibility.
         */
        getWorkerTaxCards(personalId: string): Taxcard[];
        /** Gets the latest valid tax card for the current Worker */
        getLatestCard(worker?: WorkerAccount | PersonAccount): Taxcard;
        /**
         * Gets the best available tax card for the Worker account
         * @param worker Worker account for which to fetch the cards.
         * If null, current Worker account is used.
         */
        getBestCard(worker?: WorkerAccount | PersonAccount): Taxcard;
        /**
         * Gets the personal id (HETU) regardless of whether the given account is Worker account or Personal account.
         */
        getPersonalId(account: WorkerAccount | PersonAccount): string;
        /**
         * Gets the latest tax card based on personal ID.
         * @param personalId Official person id of the person, not the Palkkaus.fi ID.
         */
        getLatestForPersonalId(personalId: string): Taxcard;
        /**
         * Resets editable tax card.
         * @param type Type for the new reset tax card.
         */
        resetTaxCard(worker?: WorkerAccount | PersonAccount, type?: TaxcardKind): void;
        /**
         * Returns the url for uploading the tax card file to the database
         *
         * @returns Tax card upload url string
         */
        getTaxCardUploadUrl(): string;
        /**
         * Returns the url where to get the tax card file from database
         *
         * @param taxCard - Tax card which download url is to be returned
         *
         * @returns Tax card download url string
         */
        getTaxCardDownloadUrl(taxCard: Taxcard): string;
        /**
         * Returns the preview url of the tax card file from database
         *
         * @param taxCard - Tax card which preview is to be returned
         *
         * @returns Tax card preview url string
         */
        getTaxCardPreviewUrl(taxCard: Taxcard): string;
    }
}
declare module "services/TempCalcGroupService" {
    import * as angular from "angular";
    import { Ajax, PayrollDetails } from "@salaxy/core";
    import { CalculationsService } from "services/index";
    import { UiHelpers } from "services/ui/index";
    /**
     * CRUD functionality for the Payroll objects and functionality for payroll in general.
     */
    export class TempCalcGroupService {
        private calculationsService;
        private $location;
        private ajax;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Payroll header */
        current: PayrollDetails;
        constructor($rootScope: angular.IRootScopeService, calculationsService: CalculationsService, $location: angular.ILocationService, ajax: Ajax, uiHelpers: UiHelpers);
        /** Creates a dummy current from calculations. */
        createNewDummyCurrent(): PayrollDetails;
        /**
         * TODO Creates a current from employment rows of all workers.
         */
        /** Dummy method for refreshing the Payroll data: Calculation array, Info object etc. */
        refreshAndValidate(payroll: PayrollDetails): void;
    }
}
declare module "services/VarmaPensionService" {
    import * as angular from "angular";
    import { PartnerServices, VarmaPensionOrder } from "@salaxy/core";
    import { AccountService } from "services/AccountService";
    import { SessionService } from "services/SessionService";
    /**
     * Varma pension service. Contains methods for getting order template, validating the order
     * and sending the order.
     */
    export class VarmaPensionService {
        private $rootScope;
        private sessionService;
        private partnerServicesApi;
        private accountService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Current order  */
        private current;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, partnerServicesApi: PartnerServices, accountService: AccountService);
        /** Lazy loading of current pension order template */
        getCurrent(): VarmaPensionOrder;
        /**
         * Sends order for processing.
         */
        send(): Promise<VarmaPensionOrder>;
        /**
         * Validates the order.
         */
        validate(): Promise<VarmaPensionOrder>;
        /**
         * Resets the current order.
         */
        reset(): void;
        /**
         * Reloads pension order template from server.
         */
        private init;
    }
}
declare module "services/VerificationService" {
    import { Accounts, MessageType } from "@salaxy/core";
    /**
     * Handles email/telephone verification
     */
    export class VerificationService {
        private accountApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new instance of VerificationService
         */
        constructor(accountApi: Accounts);
        /**
         * Sends a verification
         *
         * @param {MessageType} type
         * @param address
         *
         * @returns A Promise with result data (boolean) as the success state of the request.
         */
        sendVerification(type: MessageType, address: string): Promise<boolean>;
        /**
         * Confirms a verification
         *
         * @param {MessageType} type
         * @param pin
         *
         * @returns A Promise with result data (boolean) as the success state of the request.
         */
        confirmVerification(type: MessageType, pin: string): Promise<boolean>;
    }
}
declare module "controllers/WizardController" {
    import * as angular from "angular";
    import { WizardService, WizardStep } from "services/index";
    /**
     * A generic Wizard controller for rendering the Wizard UI
     */
    export class WizardController implements angular.IController {
        protected $scope: angular.IScope;
        protected wizardService: WizardService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Styling classes for columns. */
        colWidthClasses: string[];
        /** Path for the view that is shown in the wizard */
        viewPath: string;
        /** Path to the buttons view that is shown in the footer of the wizard */
        buttonsView?: string;
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param steps - The steps to start with
         *
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Gets all the steps in the Wizard */
        readonly steps: WizardStep[];
        /** Gets the number of the currently active step */
        /** Sets the number of the currently active step */
        step: number;
        /** The currently selected step object */
        readonly currentStep: WizardStep;
        /** Active styling properties */
        readonly style: {
            colWidthClass: string;
        };
        /** Returns true if goNext is enabled  */
        readonly canGoNext: boolean;
        /** Returns true if goNext is enabled  */
        readonly canGoPrevious: boolean;
        /**
         * Navigates to the next step if possible
         */
        goNext(): void;
        /** Navigates to the previous step if possible */
        goPrevious(): void;
    }
}
declare module "controllers/worker/WorkerWizardController" {
    import * as angular from "angular";
    import { EmploymentRelationType, UserDefinedRow } from "@salaxy/core";
    import { WizardController } from "controllers/WizardController";
    import { TaxCardService, UiHelpers, WizardService, WizardStep, WorkersService } from "services/index";
    /**
     * Wizard for creating a new worker account.
     */
    export class WorkerWizardController extends WizardController {
        private workersService;
        private taxcardsService;
        private uiHelpers;
        /** Worker wizard configuration */
        static wizardSteps: WizardStep[];
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Form data validity */
        formDataValidity: boolean;
        /** True if the new tax card has been saved. */
        isTaxCardSaved: boolean;
        /** If true, step is proceeding */
        isStepProceeding: boolean;
        /** Pension insurance type options for binding. */
        private pensionCalculationOptions;
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param workersService - Workers service.
         * @param uiHelpers - Salaxy UI helpers service.
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, workersService: WorkersService, taxcardsService: TaxCardService, uiHelpers: UiHelpers);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * The worker model.
         */
        readonly model: any;
        /** Gets the best taxcard (typically the only one or none) for the current Worker of Wizard (after save)  */
        readonly currentTaxCard: import("../../../../node_modules/@salaxy/core/model/v01").Taxcard;
        /**
         * The current worker id
         */
        readonly currentWorkerId: string;
        /** Returns true if user can go forward in  the wizard  */
        readonly canGoNext: boolean;
        /**
         * Navigates to the next step if possible and saves the data.
         *
         * @param skipSave - If true, proceeds to the next step without saving the model.
         */
        goNext(skipSave?: boolean): void;
        /**
         * Navigates to the previous step, does not save the data.
         */
        goPrevious(): void;
        /** Sets the proposed employment values for the worker */
        setProposedValues(): void;
        /** Returns available pension insurance type options for employment relation type. */
        getPensionCalculationOptions(employmentRelationType: EmploymentRelationType): any[];
        /** Populates worker with test data. */
        populateWithTestData(): void;
        /** Adds a new calculation row for the current worker. */
        addSalaryDefaultsRow(): UserDefinedRow;
        /**
         * Deletes a calculation row.
         * @param {number} rowIndex - Zero based row index of the row that should be deleted.
         */
        deleteSalaryDefaultsRow(rowIndex: number): void;
        /**
         * Saves the data to server
         */
        private save;
        private addEmployment;
        private modifyOptions;
    }
}
declare module "services/WorkersService" {
    import { Accounts, Ajax, Calculation, WorkerAccount, Workers } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { CalculationsService } from "services/CalculationsService";
    import { SessionService } from "services/SessionService";
    import { TaxCardService } from "services/TaxCardService";
    import { AlertService, Ng1Translations, UiHelpers, WizardService } from "services/ui/index";
    /**
     * Fetches the workers for the current user from the server and performs operations on this list.
     */
    export class WorkersService extends BaseService<WorkerAccount> {
        private alertService;
        private accountsApi;
        private ajax;
        private calculationsService;
        private wizardService;
        private taxCardService;
        private uiHelpers;
        private translate;
        private $routeParams;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        private rootScope;
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, workersApi: Workers, alertService: AlertService, accountsApi: Accounts, ajax: Ajax, calculationsService: CalculationsService, wizardService: WizardService, taxCardService: TaxCardService, uiHelpers: UiHelpers, translate: Ng1Translations, $routeParams: any);
        /**
         * Reloads the list from the server - called in init and e.g. after Delete and Add new
         *
         * @returns A Promise with result data
         */
        reloadList(): Promise<WorkerAccount[]>;
        /**
         * Saves the current editable worker account to database.
         *
         * @returns A Promise with result data (WorkerAccount)
         */
        saveCurrent(): Promise<WorkerAccount>;
        /**
         * Saves the given item to database.
         *
         * @param item - Item to be saved
         * @returns A Promise with the item after the round-trip to server.
         */
        save(workerAccount: WorkerAccount): Promise<WorkerAccount>;
        /**
         * Creates a new calculation and sets it as the current calculation
         *
         * @param workerId - Optional Worker ID for the new calculation.
         * If not set, uses the current Worker.
         *
         * @returns A Promise with result data (Calculation)
         */
        createNewCalc(workerId?: string): Promise<Calculation>;
        /**
         * Gets the calculations count for the specified worker.
         * @param workerId - The worker ID to filter for - default is the current worker.
         */
        getCalcCount(workerId: string): number;
        /**
         * Gets the calculations for the specified worker.
         * @param workerId - The worker ID to filter for - default is the current worker.
         */
        getCalculations(workerId: string): Calculation[];
        /**
         * Returns the url where to post the tax card file
         *
         * @returns Tax card post url string
         */
        getCurrentTaxCardUploadUrl(): string;
        /**
         * Returns the url where to get the tax card file
         *
         * @returns Tax card download url string
         */
        getCurrentTaxCardDownloadUrl(): string;
        /**
         * Override for the default setCurrentId. Launches wizard for 'new'.
         *
         * @param id - The Identifier for the item to set as current
         * @param noWizard - If true, does not launch the worker wizard for 'new' user. The default is false.
         */
        setCurrentId(id: "new" | string, noWizard?: boolean): void;
        /**
         * Opens wizard as a modal dialog
         *
         * @param workerId - Id of the worker to edit, or 'new' for creating a new account. If null, the current worker account will be edited.
         */
        launchWorkerWizard(workerId?: string): Promise<any>;
        /**
         * Shows a dialog for adding a new worker or editing an existing worker.
         * @param workerId - Worker id or null for new.
         * @param initialTab - Tab to open.
         */
        launchWorkerEditDialog(workerId?: string, initialTab?: "default" | "calculations" | "taxcards" | "employment" | "calcRows" | "holidays" | "absences"): Promise<any>;
        /**
         *
         * Event for any callback before worker wizard launch.
         *
         * @param scope - Scope for the subscribing controller/service
         * @param {function(event, ...args)} listener - The event listener function.
         * @ignore
         */
        onWorkerWizardLaunch(scope: any, callback: any): void;
        /**
         * Populates given user with test data.
         * Sets values only if the firstName and lastName have been given.
         * @param worker - Worker to populate with test data.
         */
        populateWorkerWithTestData(worker: WorkerAccount): void;
        private notifyWorkerWizardLaunch;
        private cleanup;
    }
}
declare module "services/YearEndService" {
    import { YearEnd, YearlyFeedback } from "@salaxy/core";
    import { BaseService } from "services/BaseService";
    import { SessionService } from "services/SessionService";
    import { UiHelpers } from "services/ui/index";
    /**
     * Methods for checking yearly calculations and sending corrections.
     */
    export class YearEndService extends BaseService<YearlyFeedback> {
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String that identifies the service event (onChange/notify).
         * Must be unique for the service class.
         */
        protected eventPrefix: string;
        /**
         * Creates a new instance of YearEndService.
         *
         * @param $rootScope - Angular root scope. Used for event routing.
         * @param sessionService - Session service notifies when the user is known to be authenticated.
         * @param yearEndApi - The Yearly Feedback API that is used to communicating with the server.
         * @param uiHelpers - Salaxy UI Helpers service.
         */
        constructor($rootScope: angular.IRootScopeService, sessionService: SessionService, yearEndApi: YearEnd, uiHelpers: UiHelpers);
        /**
         * Saves changes (submit) to the current feedback
         */
        saveCurrent(): Promise<YearlyFeedback>;
        /**
         * Reloads the list from the server - called in init and e.g. after Delete and Add new
         *
         * @returns A Promise with result data
         */
        reloadList(): Promise<YearlyFeedback[]>;
    }
}
declare module "services/index" {
    export * from "services/ui/index";
    export * from "services/AbsencesService";
    export * from "services/AccountService";
    export * from "services/AuthorizedAccountService";
    export * from "services/CalcRowsService";
    export * from "services/CalculationsService";
    export * from "services/CertificateService";
    export * from "services/CmsService";
    export * from "services/ContactService";
    export * from "services/ContractService";
    export * from "services/CredentialService";
    export * from "services/EarningsPaymentService";
    export * from "services/EmailMessageService";
    export * from "services/ESalaryPaymentService";
    export * from "services/HolidayYearsService";
    export * from "services/IfInsuranceService";
    export * from "services/IService";
    export * from "services/OnboardingService";
    export * from "services/PaymentService";
    export * from "services/PayrollService";
    export * from "services/ProfilesService";
    export * from "services/ReportsService";
    export * from "services/SessionService";
    export * from "services/SignatureService";
    export * from "services/SmsMessageService";
    export * from "services/TaxCardService";
    export * from "services/TempCalcGroupService";
    export * from "services/VarmaPensionService";
    export * from "services/VerificationService";
    export * from "services/WorkersService";
    export * from "services/YearEndService";
}
declare module "controllers/account/worker/AccountInfoWorkerController" {
    import { Avatar, Contact, PersonAccount } from "@salaxy/core";
    import { TaxCardService, WorkersService } from "services/index";
    /**
     * TODO: Just a starting point template.
     */
    export class AccountInfoWorkerController implements angular.IController {
        private personAccount;
        private workersService;
        private taxCardService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * View type for the worker-info component.
         *
         * - details is a large view with full information
         * - overview shows only basic info - this is the one shown as overview in the calculator.
         */
        viewType: "details" | "overview";
        /**
         * Text (translated) that is shown if now worker is selected.
         * Default is "SALAXY.NG1.WorkerInfo.noSelection".
         */
        noSelectionText: string;
        /**
         * Angular ng-model **if** the controller is bound to model using ng-model attribute.
         */
        model: angular.INgModelController;
        constructor(personAccount: PersonAccount, workersService: WorkersService, taxCardService: TaxCardService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Returns the contact info for the account.
         * You should only expect email and telephone in this contact (often no address).
         */
        readonly contact: Contact;
        /** Get the avatar for the account. */
        readonly avatar: Avatar;
    }
}
declare module "controllers/account/worker/index" {
    export * from "controllers/account/worker/AccountInfoWorkerController";
}
declare module "controllers/account/AccountAuthorizationController" {
    import { Ajax, Avatar, CompanyAccount, PersonAccount } from "@salaxy/core";
    import { AuthorizedAccountService, SessionService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user interaction for viewing and modifying authorized and authorizing accounts.
     */
    export class AccountAuthorizationController extends CrudControllerBase<Avatar> {
        private authorizedAccountService;
        private sessionService;
        private ajax;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(authorizedAccountService: AuthorizedAccountService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, sessionService: SessionService, ajax: Ajax);
        /**
         * Returns the list of partner sites available and the status of each partner.
         */
        readonly partnerSites: any;
        /**
         * Returns the list of Accountant partner sites available and the status of each partner.
         */
        readonly partnerAccountantSites: any;
        /**
         * Returns the list of Integration partner sites available and the status of each partner.
         */
        readonly partnerIntegrationSites: any;
        /**
         * Gets a specific partner site for the current user
         * @param id Identifier of the site
         */
        getPartnerSite(id: string): any;
        /**
         * Returns the current partner site
         */
        readonly currentPartnerSite: any;
        /**
         * Shows a dialog for enabling/disabling a product and editing its properties.
         * @param id - The id of the partner site for which the dialog is shown.
         */
        showPartnerSiteDialog(id: string): void;
        /**
         * Returns the authorizing accounts for the current account
         */
        readonly authorizingAccounts: Array<(PersonAccount | CompanyAccount)>;
        /** Deletes the account. Only in test. */
        deleteAccount(accountId: string): void;
        /**
         * Shows a dialog for adding any new authorized account.
         */
        showAuthorizedAccountAddDialog(): void;
        /** Adds authorized account */
        saveCurrent(callback?: (avatar: Avatar) => void): Promise<Avatar>;
        /**
         * Adds access token to url.
         * @param accountId - Account id to login as.
         */
        getLoginAsUrl(accountId: any): string;
        /**
         * Checks whether the user is in a given role
         * @param role - One of the known roles
         */
        isInRole(role: any): boolean;
    }
}
declare module "controllers/account/AccountController" {
    import * as angular from "angular";
    import { AccountProducts, IfInsuranceOrder, Product } from "@salaxy/core";
    import { AccountService, CmsService, IfInsuranceService, Ng1Translations, UiHelpers } from "services/index";
    /**
     * Handles user interaction for viewing and modifying the current account data
     * including the products that are enabled for the current account and their properties.
     */
    export class AccountController implements angular.IController {
        private $location;
        private accountService;
        private cmsService;
        private uiHelpers;
        private ifInsuranceService;
        private translate;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * current Shop ID
         */
        shopId: string;
        /** Min date for new insurance */
        insuranceDateOptions: {
            minDate: Date;
        };
        /**
         * Creates a new AccountController
         * @param $location - The Angular $location service
         * @param accountService - Service that handles the communication to the server
         * @param cmsService - CMS service for fetching a describing article for a product.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param ifInsuranceService - If insurance service.
         * @param data - data for dialog.
         * @ignore
         */
        constructor($location: angular.ILocationService, accountService: AccountService, cmsService: CmsService, uiHelpers: UiHelpers, ifInsuranceService: IfInsuranceService, translate: Ng1Translations);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** A map of available products for the current account */
        readonly products: AccountProducts;
        /**
         * Gets a specific product or null if not available for the current user
         * @param id Identifier of the product
         */
        getProduct: (id: string) => Product;
        /**
         * Sets the currentProduct to a product specified by id.
         * @param key Identifier for the product
         */
        setCurrentProduct: (key: string) => void;
        /**
         * Currently selected product in the user interface
         */
        readonly currentProduct: any;
        /**
         * Gets an article for a product: Current product or product specified by id
         * @param productId The productId. Leave this null to fetch the article for the current product.
         * @param articleLinkPattern Optional pattern for CMS links from one article to another.
         * The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         * The caller can change this to direct internal links to appropriate page template.
         */
        getArticleForProduct(productId?: string, articleLinkPattern?: string): any;
        /**
         * Saves any changes made to the products selection
         */
        saveCurrentProductAndClose(close?: (result: any) => void): void;
        /**
         * Saves products and enables given product id.
         */
        saveProduct(productId?: string): Promise<AccountProducts>;
        /**
         * Shows a dialog for enabling/disabling a product and editing its properties.
         * @param  key - The key / property name of the Product for which the dialog is shown.
         *  TODO: CAN THIS BE DELETED
         */
        showProductDialog(key: string): void;
        /** Shows product info. */
        showProductInfoPage(key: string): void;
        /** Sets pension fields (defaults) after user input. */
        checkPensionCompanySelection(): void;
        /** Sets insurance fields (defaults) after user input. */
        checkInsuranceCompanySelection(): void;
        /**
         * Send if insurance order
         * TODO: CAN THIS BE DELETED
         */
        sendIfInsuranceOrder(close?: (ifInsuranceOrder: IfInsuranceOrder) => void): void;
        /**
         * List of Shop-in-Shop modals
         */
        readonly shops: {
            title: string;
            id: string;
            img: string;
            description: string;
            status: "pending" | "new" | "ready";
            type: "product" | "shop" | "external";
            templateUrl?: string;
            controller?: string;
            url?: string;
            windowTemplateUrl?: string;
        }[];
        /**
         * Gets a specific shop for the current user
         * @param id Identifier of the shop
         */
        getShop(id: string): {
            title: string;
            id: string;
            img: string;
            description: string;
            status: "pending" | "new" | "ready";
            type: "product" | "shop" | "external";
            templateUrl?: string;
            controller?: string;
            url?: string;
            windowTemplateUrl?: string;
        };
        /**
         * Opens shop/product
         * @param id Id of the shop/product
         */
        openShop(id: string): void;
        /** Returns pension number for test account
         * @param id Id of the pension company
         */
        getPensionNumberForTest(id: string): void;
    }
}
declare module "controllers/account/AccountTestController" {
    import * as angular from "angular";
    import { Ajax, Test } from "@salaxy/core";
    import { UiHelpers } from "services/index";
    /**
     * Controller for resetting account data in the test environment.
     */
    export class AccountTestController implements angular.IController {
        private testApi;
        private uiHelpers;
        private ajax;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new AccountTestController.
         * @param testApi - Api methods for resetting account data.
         * @param uiHelpers - Salaxy UI Helpers.
         * @param ajax - Salaxy ajax component.
         */
        constructor(testApi: Test, uiHelpers: UiHelpers, ajax: Ajax);
        /**
         * Delete the current account data and credentials (including Auth0 user).
         * Can be called only in a test environment.
         */
        deleteCurrent(): void;
        /**
         * Remove all calculations, workers etc. user objects except products and signature from the account.
         * Can be called only in a test environment.
         */
        deleteData(): void;
        /**
         * Remove all calculations, payrolls and payments from the account.
         * Can be called only in a test environment.
         */
        deleteCalculations(): void;
        /**
         * Remove workers including calculations, employment contracts and tax cards from the account.
         * Can be called only in a test environment.
         */
        deleteWorkers(): void;
        /**
         * Remove all holiday year from all workers. Does not touch the default values of holidays in Worker Employment relation.
         * Can be called only in a test environment.
         */
        deleteHolidays(): void;
        /**
         * Delete all empty accounts (company or worker) created by this account.
         * Can be called only in a test environment.
         */
        deleteAuthorizingAccounts(): void;
        /**
         * Remove pension and insurance from the account.
         * Can be called only in a test environment.
         */
        deletePensionAndInsurance(): void;
        /**
         * Remove all products from the account.
         * Can be called only in a test environment.
         */
        deleteProducts(): void;
        /**
         * Remove the signature from the account.
         * Can be called only in a test environment.
         */
        deleteSignature(): void;
        private logout;
    }
}
declare module "controllers/account/AuthorizationSwitchController" {
    import * as angular from "angular";
    import { AuthorizedAccountService } from "services/index";
    /**
     * Controller for Salaxy authorization switch (a graphic checkbox for giving authorization to your Salaxy account data for a certain partner site)
     */
    export class AuthorizationSwitchController implements angular.IController {
        private authorizedAccountService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Switch name */
        name: string;
        /** Id for the switch */
        id: string;
        /** The model that is bound to the switch */
        model: any;
        /** The value of the on text */
        onText: string;
        /** The value of the off text */
        offText: string;
        /** If true, the switch will be disabled and its value cannot be changed by clicking/toggling it */
        disabled: boolean;
        /** partnerSite object that this switch is tied to */
        private partnerSite;
        /**
         * Creates a new AuthorizationSwitchController
         * @ignore
         */
        constructor(authorizedAccountService: AuthorizedAccountService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Toggles the authorization option and makes the corresponding api call. */
        switchAuthorization(): void;
    }
}
declare module "controllers/account/CertificateController" {
    import { Certificate } from "@salaxy/core";
    import { CertificateService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /** Certificate controller for listing certificate, revoking and requesting a new certificate. */
    export class CertificateController extends CrudControllerBase<Certificate> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(certificateService: CertificateService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
        /** Creates a new certificate */
        createNew(): Certificate;
        /**
         * Shows a dialog for adding nw certificate.
         */
        showCertificateAddDialog(): void;
        /** Creates a new certificate */
        saveCurrent(): Promise<Certificate>;
    }
}
declare module "controllers/account/CredentialController" {
    import { SessionUserCredential } from "@salaxy/core";
    import { CredentialService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /** Credential controller for listing credentials and removing existing credentials. */
    export class CredentialController extends CrudControllerBase<SessionUserCredential> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(credentialService: CredentialService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
    }
}
declare module "controllers/account/SessionController" {
    import * as angular from "angular";
    import { Ajax, SystemRole } from "@salaxy/core";
    import { Ng1Translations, SessionService, UiHelpers } from "services/index";
    /**
     * User interaction with the current session: UserCredentials, Current Account(s) and Login/Logout.
     */
    export class SessionController implements angular.IController {
        private sessionService;
        private translate;
        private $location;
        private ajax;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Default redirect URL for signIn and register if not specified by the method */
        redirectUrl: string;
        /**
         * Creates a new SessionController
         * @param sessionService - Service that handles the communication to the server
         * @param translate - Translation service.
         * @param $location - Angular location service,
         * @param AjaxNg1 - Ajax angular component
         * @param uiHelpers- Salaxy ui helpers.
         * @ignore
         */
        constructor(sessionService: SessionService, translate: Ng1Translations, $location: angular.ILocationService, ajax: Ajax, uiHelpers: UiHelpers);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** If true, the session has been checked from theserver - i.e. isAuthenticated = false means that the user really cannot be authenticated. */
        readonly isSessionChecked: boolean;
        /** If true, the session check call is progressing. */
        readonly isSessionChecking: boolean;
        /** If true, the user is authenticated */
        readonly isAuthenticated: boolean;
        /** Avatar to show in the login screen */
        readonly avatar: any;
        /** The full session objcet */
        readonly session: any;
        /** The company account if the current account is company - and if the full account info is fetched */
        readonly company: any;
        /** The person account if the current account is person - and if the full account info is fetched */
        readonly person: any;
        /**
         * Opens the login form for the current user
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login.
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         * @param partnerSite - Optional partner site
         */
        signIn(redirectUrl?: string, role?: string, partnerSite?: any): void;
        /**
         * Opens the login dialog with signUp / register functionality
         *
         * @param  redirectUrl - The URL where the user is taken after a successfull login
         * @param  role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        register(redirectUrl?: string, role?: string): void;
        /** If false, the current authenticated user has not signed the contract. */
        readonly isAccountVerified: boolean;
        /**
         * Returns onboarding wizard url (server side).
         * @deprecated
         */
        readonly accountWizardUrl: string;
        /**
         * Sends the user to the Sign-out page
         * @param redirectUrl - URL where user is redirected after log out.
         * Must be absolute URL. Default is the root of the current server.
         */
        signOut(redirectUrl?: string): void;
        /**
         * Switches the current web site usage role.
         * @param  role - household or worker.
         * @returns A Promise with result data (new role as string)
         */
        switchRole(role: "worker" | "household", callbackOrRedirect?: any): Promise<"household" | "worker">;
        /**
         * When called, will show the login screen if the user is not logged in
         */
        checkAuthenticated(): void;
        /**
         * Checks whether the user is in a given role
         * @param  role - One of the known roles
         */
        isInRole(role: SystemRole): boolean;
        /**
         * Set the language for UI in the current session.
         * @param lang - Language to select: fi, en, sv.
         */
        setLanguage(lang: string): void;
        /**
         * Get the current language for UI in the current session.
         */
        getLanguage(): void;
        /** Sign in error */
        readonly signInError: string;
        /** Sign in error description */
        readonly signInErrorDescription: string;
        /** Sign in error page url */
        readonly signInErrorPageUrl: string;
    }
}
declare module "controllers/account/index" {
    export * from "controllers/account/worker/index";
    export * from "controllers/account/AccountAuthorizationController";
    export * from "controllers/account/AccountController";
    export * from "controllers/account/AccountTestController";
    export * from "controllers/account/AuthorizationSwitchController";
    export * from "controllers/account/CertificateController";
    export * from "controllers/account/CredentialController";
    export * from "controllers/account/SessionController";
}
declare module "controllers/bases/ListControllerBase" {
    import { IService, UiHelpers } from "services/index";
    /**
     * Base class for list controllers that edit a list of items within a parent object (parent property).
     * Abstract class implementation defines the location of list with items of type TListItem.
     * Typically, the list items do not have ID's, they and all operations are synchronous.
     * Save operation is currently done to the parent, it may be later connected also to this controller.
     */
    export abstract class ListControllerBase<TParent, TListItem> implements angular.IController {
        protected crudService: IService<TParent>;
        protected uiHelpers: UiHelpers;
        /** Parent object to which the component is bound.  */
        private _parent;
        /**
         * Creates a new ListControllerBase.
         * @param crudService The the BaseService instance that is used for saving the parent object to server.
         * @param uiHelpers - Salaxy ui helpers service for dialogs etc.
         */
        constructor(crudService: IService<TParent>, uiHelpers: UiHelpers);
        /**
         * Implement IController by providing onInit method.
         * We currently do nothing here, but if you override this function,
         * you should call this method in base class for future compatibility.
         */
        $onInit(): void;
        /**
         * Gets or sets the parent object that contains the list that is being edited.
         * Typically this parent object is an ApiCrudObject: You must implement IService<IParent> for it.
         */
        parent: TParent;
        /**
         * When overriding the abstract class, you should provide the list within the parent that is edited / viewed.
         * If the parent object is not provided or list is not otherwise available, you should provide null.
         */
        abstract readonly list: TListItem[];
        /** The template URL for the edit dialog. Set to null if you do not want an edit dialog */
        abstract getEditDialogTemplateUrl(): string;
        /**
         * Possiblity to define additional logic that is passed to the edit dialog as $ctrl.logic.
         * This may contain additional metadata as well as functions.
         */
        getEditDialogLogic(): any;
        /** When overriding the abstract class, provide her the factory method that creates a new list item. */
        abstract getBlank(): TListItem;
        /**
         * Saves the parent object.
         * Shows a loader / spinner because typically the view is not changing in the save operation like in CRUD views (Detail => List).
         */
        saveParent(): Promise<TParent>;
        /**
         * Deletes an item from list (no confirm etc.)
         * The method shows the "Please wait..." loader, but does not call onDelete
         * or move the browser to listUrl. The caller should take care
         * of the UX actions after delete if necessary.
         *
         * @param id: Identifier of the item to be deleted.
         * @returns Promise that resolves to true (never false). Fails if the deletion fails.
         */
        delete(item: TListItem): TListItem[];
        /**
         * Shows the edit dialog.
         * @param item Item to edit or string "new" for creating a new one.
         */
        showEditDialog(item: TListItem | "new"): void;
    }
}
declare module "controllers/bases/ListControllerBaseBindings" {
    /**
     * Bindings for the component which uses ListControllerBase.
     */
    export class ListControllerBaseBindings {
        /**
         * Parent object that contains the list that is being edited.
         * Typically this parent object is an ApiCrudObject: You must implement IService<IParent> for it.
         */
        parent: string;
        /**
         * Function that is called when user selects an item in the list.
         * The selected item is the parameter of the function call.
         * @example <salaxy-payroll-list on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
         */
        onListSelect: string;
        /**
         * TODO: Check and document this
         * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc"></salaxy-payroll-list>
         */
        onCreateNew: string;
    }
}
declare module "controllers/bases/index" {
    export * from "controllers/bases/ListControllerBase";
    export * from "controllers/bases/ListControllerBaseBindings";
}
declare module "controllers/calc/CalcChartController" {
    import * as angular from "angular";
    import { CalculationsService } from "services/index";
    /**
     * Controller logic that generates the employer and worker charts using angular-chart
     */
    export class CalcChartController implements angular.IController {
        private $scope;
        private calculationsService;
        private $filter;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Role for which the chart is rendered: employer or worker */
        role: "employer" | "worker";
        /** General chart properties */
        htmlId: any;
        /** Label */
        chartLabel: string;
        /** Label */
        chartLabelTable: any;
        /** Y axis scale */
        scaleYAxis: any;
        /** Chart type */
        chartType: any;
        /** Properties used in chart generation */
        seriesData: any;
        /** Series labels */
        seriesLabels: any;
        /** Pie chart data */
        pieData: any;
        /** Should a bar or a pie chart be shown, default is bar chart */
        showBarChart: boolean;
        /** Bar chart options */
        chartOptions: {
            tooltips: {
                enabled: boolean;
                bodySpacing: number;
                callbacks: {
                    label: (tooltipItem: any, data: any) => any;
                };
            };
            scales: {
                xAxes: {
                    stacked: boolean;
                }[];
                yAxes: {
                    stacked: boolean;
                    ticks: {
                        min: number;
                        max: number;
                    };
                }[];
            };
            events: string[];
            title: {
                fullWidth: boolean;
            };
        };
        /** Pie chart options */
        pieChartOptions: {
            tooltips: {
                enabled: boolean;
                mode: string;
                bodySpacing: number;
                callbacks: {
                    label: (tooltipItem: any, data: any) => any;
                };
            };
            events: string[];
        };
        /** Default chart colors */
        chartColors: {
            backgroundColor: string;
            pointBackgroundColor: string;
            pointHoverBackgroundColor: string;
            borderColor: string;
            pointBorderColor: string;
            pointHoverBorderColor: string;
        }[];
        private colors;
        private calc;
        private result;
        private totalExpenses;
        private totalGrossSalary;
        private salaryAdditions;
        private allSideCosts;
        private employerChartScale;
        private finalCost;
        private salaryPayment;
        private tax;
        private workerSideCosts;
        private benefits;
        private totalWorkerPayment;
        private workerChartScale;
        /**
         * Creates a new CalcChartController
         * @param $scope - The Angular scope
         * @param calculationsService - Service that handles the communication to the server
         * @param $filter - Angular $filter service
         * @ignore
         */
        constructor($scope: angular.IScope, calculationsService: CalculationsService, $filter: angular.IFilterService);
        /** Initializes the controller. */
        $onInit(): void;
        /**
         * Executes everything needed for chart generation:
         * fetched the current calculation, stores needed numbers from that calculation,
         * checks colors, sets labels and sets chart data
         */
        private doChart;
        /**
         * Checks if any overriding colors have been given as an attribute
         * Checks if colors are valid hex colors, and then replaces default colors
         */
        private checkColors;
        /**
         * Calculates and sets a proper Y axis max value for a given value
         * @param value The number of the highest bar in chart
         */
        private setChartYAxisLimit;
        /**
         * Fetches all relevant numbers from the current calculation
         */
        private calculateAllNumbers;
        /**
         * Sets the relevant employer data as chart data
         */
        private doChartEmployer;
        /**
         * Sets the relevant worker data as chart data
         */
        private doChartWorker;
    }
}
declare module "controllers/calc/CalcIrRowsController" {
    import * as angular from "angular";
    import { Calculation, IncomeTypeMetadata, IrRow } from "@salaxy/core";
    import { CalculationsService } from "services/index";
    /**
     * IR-rows based editor for calculation
     */
    export class CalcIrRowsController implements angular.IController {
        private calculationsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Active tab of editor
         */
        protected activeTab: number;
        private _currentRow;
        /**
         * Creates a new instance of the controller with dependency injection.
         * @ignore
         */
        constructor(calculationsService: CalculationsService);
        /**
         * Implement IController
         */
        $onInit(): void;
        /** The current row that is being viewed / edited. */
        readonly currentRow: IrRow;
        /** The metadata that describes currentRow */
        readonly currentRowType: IncomeTypeMetadata;
        /** IR-rows that are viewed / edited by the controller */
        readonly irRows: IrRow[];
        /** Sets the current edited / viewd row */
        setCurrentIrRow(row: IrRow): void;
        /** Deletes a row */
        deleteIrRow(row: any): void;
        /** Returns true if the row is read-only: Cannot be directly edited. */
        readonly isIrRowReadOnly: boolean;
        /** Updates the current row type. */
        updateCurrentRowType(): void;
        /** Gets the income type (the metadata) object for the currently selected row. */
        getIncomeType(row: any): IncomeTypeMetadata;
        /** Adds a new IR row to the collection. */
        addIrRow(): void;
        /** Current calculation object */
        readonly currentCalc: Calculation;
        /**
         * Recalculates the current calculation using the API.
         */
        recalculate(): void;
    }
}
declare module "controllers/calc/CalculatorSection" {
    /** Logical section / area of the calculator ui. */
    export class CalculatorSection {
        isActive: boolean;
        isSelectionDone: boolean;
        title: string;
        /** Creates a new CalculatorSection
         * @param isActive - If true, the section is currently active / selected in te user interface.
         * @param isSelectionDone - If true, user has selected some values for the section
         * @param title - Short text title for the section
         */
        constructor(isActive: boolean, isSelectionDone: boolean, title: string);
        /**
         * Shortcut for isSelectionDone or isActive.
         * This is when the overview window should be shown colored
         */
        readonly isOverviewActive: boolean;
    }
}
declare module "controllers/calc/CalculatorSections" {
    import { Calculation } from "@salaxy/core";
    import { CalculatorSection } from "controllers/calc/CalculatorSection";
    /** Sections defines areas of the calculator: worker, work, salary, expenses and results. */
    export class CalculatorSections {
        calc: Calculation;
        /**
         * Creates a new CalculatorSections helper for a calculation
         * @param calculation - The Calculation object for which the helper operates
         */
        constructor(calc: Calculation);
        /** Stores the UI properties to the calculation until a roundtrip to server. Then the property is set to null */
        readonly ui: any;
        /**
         * Activates a section in the calculator (opens the detail view).
         * @param section - Name of the section
         */
        setActive(section: string): void;
        /**
         * Toggles the section
         */
        toggleActive(section: string): void;
        /**
         * Toggles the section active / non-active.
         */
        toggle(section: string): void;
        /** Gets a section */
        get(section: string): CalculatorSection;
    }
}
declare module "controllers/calc/CalcWorktimeController" {
    import * as angular from "angular";
    import { Calculation, CalcWorktime, DateRange, HolidayYear, MonthlyHolidayAccrual, WorkerAbsences } from "@salaxy/core";
    import { AbsencesService, CalculationsService, HolidayYearsService, UiHelpers, WorkersService } from "services/index";
    /**
     * Handles the user interaction of Worktime within the Calculator.
     * The Worktime contains the logic for fetching holidays and absences
     * for that particular period and adding calculation rows for them if necessary.
     */
    export class CalcWorktimeController implements angular.IController {
        private calculationsService;
        private uiHelpers;
        private workersService;
        private absencesService;
        private holidayYearsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Contains the holidays data for the current worker for the currently selected period */
        workerPeriodData: {
            /** Holiday year. */
            holidayYear: HolidayYear;
            /** Absences of the worker. */
            absences: WorkerAbsences;
        };
        /** User interface shortcuts for period selection */
        periodShortcuts: {
            /** Text for current month. */
            monthCurrent: string;
            /** Text for last month. */
            monthLast: string;
            /** Text for other month. */
            monthOther: string;
            /** Text for two weeks. */
            weeks2: string;
            /** Text for half a month */
            monthHalf: string;
            /** Text for other period. */
            other: string;
        };
        /** The calculation that the controller edits (shows in read-only mode). */
        calc: Calculation;
        /** Worktime data for the period fetched from the server. */
        worktime: CalcWorktime;
        /** If there is a validation error in period start, it is added here. */
        periodStartDateError: any;
        private _periodStartDate;
        private holidaysCacheKey;
        private _dateRange;
        /**
         * Creates a new Calculator2016Controller
         * @ignore
         */
        constructor(calculationsService: CalculationsService, uiHelpers: UiHelpers, workersService: WorkersService, absencesService: AbsencesService, holidayYearsService: HolidayYearsService);
        /**
         * Implement IController
         */
        $onInit(): void;
        /** Gets the current calculation object */
        readonly currentCalc: Calculation;
        /** Getter and setter for Calculation daterange in compatible format for new DateRange component. */
        dateRange: DateRange;
        /**
         * Called when the date range changes.
         * @param noStartDateChange If true, the _periodStartDate is not updated.
         * Should be true if the change is triggered by that input to avoid UI flickering.
         */
        dateRangeChange(noPeriodStartDateUpdate?: boolean): void;
        /** Gets the Worker ID if set */
        readonly workerId: string;
        /** Gets or sets the period start date */
        periodStartDate: string;
        /** Gets the user interface shortcut for the period. */
        periodShortcut: string;
        /**
         * Submit the data in the Worktime (period) dialog.
         */
        submitWorktime(): void;
        /** Returns true if the current worktime data seems to have holidays or absences to add as rows in the salary calculation.  */
        hasHolidaysOrAbsences(): boolean;
        /** Shows the pro-dialog */
        showPeriodDetails(): void;
        /**
         * Gets the status for the current holidays and absences fetching.
         * These are fetched from the server when the period and / or worker changes.
         */
        getHolidaysStatus(): "noWorker" | "noPeriod" | "loading" | "ok";
        /** Gets the monthly text for calendar month. */
        getAccrualMonthText(type: "header" | "description", month: MonthlyHolidayAccrual): string;
        /** Gets the accrual explanation text. */
        getHolidayAccrualText(monthlyAccrual: MonthlyHolidayAccrual): string;
        /** Gets the holidays type for the purposes of the user interface. */
        getUiType(uiType: "holidays"): boolean;
        /**
         * Reloads the holidays from server.
         */
        reloadHolidays(): void;
        /** Cache key that defines whether the Period should be recalculated on the server. */
        private getCacheKey;
    }
}
declare module "usecases/UsecaseValidationMessage" {
    /** Client-side validation message for a usecase input. */
    export interface UsecaseValidationMessage {
        /** Message text */
        msg: string;
        /** Type of the message. */
        type: "default" | "error";
    }
}
declare module "usecases/MealBenefitUsecaseLogic" {
    import { MealBenefitKind, UserDefinedRow } from "@salaxy/core";
    import { UsecaseValidationMessage } from "usecases/UsecaseValidationMessage";
    /** Client-side logic for MealBenefitUsecase  */
    export class MealBenefitUsecaseLogic {
        /**
         * Updates the usecase values based onuser input without going to the server.
         * @param row Row that contains the usecase data.
         */
        update(row: UserDefinedRow): void;
        /** Gets validation message for MealBenefitUsecase  */
        getValidation(row: UserDefinedRow): UsecaseValidationMessage;
        /**
         * Gets the taxable price for the row.
         * @param kind meal benefit type
         * @param price Price of the row.
         */
        getTaxablePrice(kind: MealBenefitKind, price: number): number;
    }
}
declare module "usecases/UsecaseValidations" {
    import { Calculation, CalculationRowType, UserDefinedRow } from "@salaxy/core";
    import { UsecaseValidationMessage } from "usecases/UsecaseValidationMessage";
    /**
     * Provides logic for usecase user interfaces.
     * PRELIMINARY: This class will be moved to @salaxy/core project once first implementation is complete. Will probably be merged with UsecasesLogic.
     */
    export class UsecaseValidations {
        /**
         * Returns a validation message for a row or null if none is required.
         * @param row: The row that is validated.
         */
        static getValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage;
        /**
         * Returns true if the row type supports negative values.
         * Typically, this means that the sum of all rows must still be zero or positive.
         * @param rowType Row type to check
         */
        static canHaveNegativeRow(rowType: CalculationRowType): boolean;
        /** Gets validation message for DailyAllowanceUsecase  */
        static getDailyAllowanceValidation(row: UserDefinedRow): UsecaseValidationMessage;
        /** Gets validation message for NonProfitOrgUsecase  */
        static getNonProfitOrgValidation(row: UserDefinedRow): UsecaseValidationMessage;
        /** Gets validation message for SubsidisedCommuteUsecase  */
        static getSubsidisedCommuteValidation(row: UserDefinedRow): UsecaseValidationMessage;
        /** Gets validation message for CarBenefitUsecase  */
        static getCarBenefitValidation(row: UserDefinedRow): UsecaseValidationMessage;
        /** Gets validation message for UnionPaymentUsecase  */
        static getUnionPaymentValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage;
    }
}
declare module "usecases/UsecaseLogic" {
    import { Calculation, UserDefinedRow } from "@salaxy/core";
    import { UsecaseValidationMessage } from "usecases/UsecaseValidationMessage";
    /**
     * Provides logic for usecase user interfaces.
     * PRELIMINARY: This class will be moved to @salaxy/core project once first implementation is complete. Will probably be merged with UsecasesLogic.
     */
    export class UsecaseLogic {
        /**
         * Returns a validation message for a row or null if none is required.
         * @param row: The row that is validated.
         */
        static getValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage;
        /**
         * Updates the usecase property values without going to the server.
         * @param row Row to update.
         */
        static updateUsecase(row: UserDefinedRow): void;
        /**
         * Updates the usecase values based onuser input without going to the server.
         * @param row Row that contains the usecase data.
         */
        static updateMonthlySalaryUsecase(row: UserDefinedRow): void;
        /**
         * Updates the usecase values based onuser input without going to the server.
         * @param row Row that contains the usecase data.
         */
        static updateDailyAllowanceUsecase(row: UserDefinedRow): void;
        /**
         * Updates the usecase values based onuser input without going to the server.
         * @param row Row that contains the usecase data.
         */
        static updateCarBenefitUsecase(row: UserDefinedRow): void;
        /**
         * Updates the usecase values based onuser input without going to the server.
         * @param row Row that contains the usecase data.
         */
        static updateNonProfitOrgUsecase(row: UserDefinedRow): void;
    }
}
declare module "controllers/calc/CalcRowsController" {
    import { CalcRowConfig, CalculationRowCategory, CalculationRowType, UserDefinedRow } from "@salaxy/core";
    import { CalcRowsService, CalculationsService, SessionService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user messages to the customer support and potentially other messaging functionality in the future.
     */
    export class CalcRowsController extends CrudControllerBase<UserDefinedRow> {
        private calcRowsService;
        private session;
        private $timeout;
        private workersService;
        private calculationsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If set, filters the rows based on categories (plus rowTypes if set) */
        categories: CalculationRowCategory[];
        /** If set, shows only these types (plus categories if set) */
        rowTypes: CalculationRowType[];
        /** Grid table title */
        title: string;
        /**
         * Possiblity to set the collection of rows that is being edited.
         * If not set, the rows of current calculation is being edited.
         */
        rows: UserDefinedRow[];
        /**
         * CalcRow-component calls this even when user closes the dialog (either cancel or OK).
         * Users of the dialog must handle the event to hide the dialog.
         * @example
         * <div class="salaxy-calc-details-dialog inner">
         *              <salaxy-calc-row on-close="$ctrl.closeDetail()"></salaxy-calc-row>
         * </div>
         */
        onClose: ({ isOk: bool, item: T }: {
            isOk: any;
            item: any;
        }) => void;
        /** Temp field for isInEditMode checking. */
        _isInEditMode: boolean;
        /**
         * Creates a new CalcRowsController
         * @param calcRowsService - CRUD Service for the rows of the current calculation.
         * @param $location - Angular location service.
         * @param $attrs - Angular attributes service.
         * @param uiHelpers - Salaxy UI helpers service.
         * @ignore
         */
        constructor(calcRowsService: CalcRowsService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, session: SessionService, $timeout: angular.ITimeoutService, workersService: WorkersService, calculationsService: CalculationsService);
        /** If true, the component is in edit mode, i.e. the list of rows is the list in the current calculation. */
        readonly isInEditMode: boolean;
        /** If true, the list may be edited. This is false with paid calculations. */
        readonly isEditable: boolean | UserDefinedRow[];
        /** sets isEditable to true. This may require creating a calculation from list and setting that as current calculation. */
        edit(): void;
        /** Gets the rows to edit filtered by rowTypes and categories. */
        readonly filteredList: UserDefinedRow[];
        /** Returns true if the filtered list has any rows. */
        readonly hasRows: boolean;
        /** Combines together categories and rowTypes properties as one list of row types. */
        readonly rowTypesToShow: CalculationRowType[];
        /**
         * Gets the UX configuration for a row.
         * @param row Row for which the config is fetched.
         * If not set, gets the config for current.
         */
        getConfig(row?: UserDefinedRow): CalcRowConfig;
        /**
         * Returns a validation message for a row or null if none is required.
         * @param row: The row that is validated.
         */
        getValidation(row: UserDefinedRow): {
            /** Validation message. */
            msg: string;
            /** Validation result type. */
            type: "default" | "error";
        };
        /** Gets the placeholder text for an input */
        getPlaceholderText(row: UserDefinedRow, field: "amount" | "price"): string;
        /** Updates the usecase calculation based on new user input. */
        updateUsecase(): void;
        /**
         * Return the total for the row
         */
        getRowTotal(row: UserDefinedRow): number;
        /** Gets the total for the current rows. */
        getTotal(): number;
        /**
         * Returns true, if the row is disabled. The row is interpreted as disabled if it has no rowType
         * or it has been set as hidden in the configuration.
         * @param row Row to check
         * @param field Type of the input / field.
         */
        isDisabled(row: UserDefinedRow, field: "amount" | "price"): boolean;
        /** Row type changes on a row */
        rowTypeChanged(row: UserDefinedRow): void;
        /**
         * Closes the dialog.
         * @param isOk If true, OK is clicked. If false, cancel is clicked.
         */
        closeDetail(isOk: boolean): void;
        /** Deletes the current row. */
        deleteCurrent(): void;
    }
}
declare module "controllers/calc/CalculationGroupController" {
    import { Calculation, DateRange, PayrollDetails, WorkerAccount } from "@salaxy/core";
    import { CalculationsService, TempCalcGroupService, UiHelpers, WorkersService } from "services/index";
    /**
     * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
     * Typical usecase is that a company has e.g. a monthly salary list that is paid
     * at the end of month. For next month, a copy is then made from the latest list and
     * the copy is potentially modified with the changes of that particular month.
     * Payroll can also be started from scratch either by just writing salaries from
     * e.g. an e-mail or by uploading an Excel sheet.
     */
    export class CalculationGroupController {
        private uiHelpers;
        private calculationsService;
        tempCalcGroupService: TempCalcGroupService;
        private workersService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Work period date range for binding */
        dateRange: DateRange;
        constructor($location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, calculationsService: CalculationsService, tempCalcGroupService: TempCalcGroupService, workersService: WorkersService);
        /**
         * Initialize controller.
         */
        $onInit(): void;
        /** Currently editable PayrollDetails object. */
        readonly current: PayrollDetails;
        /**
         * Returns draft calculations.
         */
        readonly drafts: Calculation[];
        /**
         * Returns paid calculations.
         */
        readonly paid: Calculation[];
        /**
         * Returns workers.
         */
        readonly workers: WorkerAccount[];
        /** Returns true if the calculation is editable */
        isEditable(calc: Calculation): boolean;
        /** Starts editing the given calculation */
        edit(calc: Calculation): void;
        /**
         * Demo hack. Remove
         */
        update(): void;
        /**
         * TODO: Opens calculator in modal.
         * @param id is the id of the calculation
         */
        showEditCalcDialog(id: string): void;
        /**
         * Shows a dialog for editing an existing worker.
         * @param workerAccountId - Worker id or null for new.
         */
        showEditWorkerDialog(calculation: Calculation, workerAccountId?: string, initialTab?: string): void;
        /**
         * Opens DateRange component in modal dialog
         * Edits Payroll
         */
        showDateRange(): void;
        /**
         * TODO: make it work
         * Opens DateRange component in modal dialog
         * Edits calculation
         */
        showDateRangeForCalculation(calc: Calculation): void;
        /**
         * Opens the list of draft calculations into a dialog window.
         *  @param placement defines where at the calculations array new calculation is added.
         * 'top' adds the new calculation in the beginning of the array (first on the calculations list), 'bottom' adds it at the end of the array (last on the calculations list).
         */
        showDraftList(placement: "top" | "bottom"): void;
        /** Opens the list of paid calculations into a dialog window.
         *  When selected (onClick), a copy of the calculation is made.
         *  @param placement defines where at the calculations array  new calculation is added.
         * 'top' adds the new calculation in the beginning of the array (first on the calculations list), 'bottom' adds it at the end of the array (last on the calculations list).
         */
        showPaidList(placement: "top" | "bottom"): void;
        /**
         * Opens a list of workers in to a dialog window.
         * Creates a new calculation for the selected worker and current payroll.
         * @param placement defines where at the calculations array  new calculation is added.
         * 'top' adds the new calculation in the beginning of the array (first on the calculations list), 'bottom' adds it at the end of the array (last on the calculations list).
         */
        addNewCalc(placement: "top" | "bottom"): void;
        /**
         * Shows a confirm dialog if payroll has any calculations.
         * Returns true if a user clicks OK -button. Otherwise (cancel or close) returns false.
         */
        getAllWorkerDefaultRows(): void;
        /**
         * Adds default rows (SalaryDefault) from employment information to calculation.
         * @param workerId Identifier of the Worker in the Workers collection.
         * @param calcId Identifier of the Calculation in the Calculations collection.
         *
         */
        getWorkerDefaultRows(calc: Calculation): void;
        /**
         * Removes the calculation from payroll
         */
        removeCalc(calc: Calculation): void;
        /**
         * Deletes the calculation
         */
        deleteCalc(calc: Calculation): void;
    }
}
declare module "controllers/calc/CalculationsController" {
    import { Calculation, Reports } from "@salaxy/core";
    import { CalculationsService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user interaction for listing the calculations for the current user.
     * selecting one of those calculations as a current calculation
     * and performing operations on that calculation.
     */
    export class CalculationsController extends CrudControllerBase<Calculation> {
        private calculationsService;
        private workersService;
        private reportsApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * You may bind a user interface for adding a new row to this property.
         * Then use the addRow function without a parameter to add this row to the calculation.
         */
        newRow: any;
        /** Max count of calculations to show in the list. */
        limitTo: number;
        /**
         * Bindable workerId for listing calculations
         */
        _workerId: string;
        /** If set, filters the list to these statuses. */
        statusList: string;
        /**
         * Type of view of list.
         * "default" is the default calculation list where buttons take to "/calculations/details"
         * and there is delete and separate edit / copy buttons.
         * "standard" is a simpler calculation selection that behaves like a standard list.
         * Future implementations may include dropdowns "select" etc.
         */
        listType: "default" | "standard";
        /**
         * Creates a new CalculationsController
         * @param calculationsService - Service that handles the communication to the server
         * @param $location - Angular location service.
         * @param $attrs - Angular attributes service.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param workersService - Gets the current worker for getCalculationsForWorker if necessary.
         * @param reportsApi - Service for fetching the reports
         * @ignore
         */
        constructor(calculationsService: CalculationsService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, workersService: WorkersService, reportsApi: Reports);
        /** Worker ID can be set for the controller. If not set, it returns the curretn worker id. */
        workerId: string;
        /**
         * Recalculates the current calculation
         */
        recalculate(): void;
        /**
         * Gets the calculations for the specified worker ID.
         * @param workerId - The worker ID to filter for - default is the current worker.
         */
        getCalculationsForWorker(workerId: string): Calculation[];
        /**
         * Adds the row defined by the newRow property to the calculation.
         * Calls the recalculate method to refresh the calculation.
         */
        addRow(): void;
        /**
         * Deletes a calculation row: Expenses, benefits etc. item and recalculates.
         * @param {number} rowIndex - Zero based row index of the row that should be deleted.
         */
        deleteRow(rowIndex: number): void;
        /**
         * List filter for components which list calculations. Uses this.statusList for filtering calculations.
         */
        workflowStatusFilter: (item: any) => boolean;
        /**
         *   - `function(value, index, array)`: A predicate function can be used to write arbitrary filters.
         *     The function is called for each element of the array, with the element, its index, and
         *     the entire array itself as arguments.
         */
        listFilter: (item: Calculation, index: any, array: Calculation[]) => boolean;
        /** View type with default set as "default" */
        protected readonly viewType: "default" | "standard";
    }
}
declare module "controllers/calc/CalculatorPanels" {
    import { Calculation } from "@salaxy/core";
    /**
     * Defines the panels logic in the Calculator
     */
    export class CalculatorPanels {
        calc: Calculation;
        /**
         * Creates a new CalculatorPanels helper for a calculation
         * @param calculation - The Calculation object for which the helper operates
         */
        constructor(calc: Calculation);
        /** Stores the UI properties to the calculation until a roundtrip to server. Then the property is set to null */
        readonly ui: any;
        /**
         * Toggles the user interface panels active (open/close) in a hierarchical way.
         * The functionality mimics a hierarchical panel/menu structure in HTML / components that are not hierarchical.
         * See the example to better understand the logic.
         * Hierarchy logic is supported to 2 levels only - it is not tested on deeper hierarchy though it may function.
         * @param panels - Identifiers of panel or two panels (panel and its child) that should be toggled.
         *
         * @example
         * Assume we have these two panels open: "panel1", "panel2"
         *
         * - togglePanel("panel1", "panel2") => ["panel1"]      // Normal 2nd level toggle opens and closes the last level.
         * - togglePanel("panel2") => ["panel1"]                // Alternative explicit last level close
         * - togglePanel("panel1") => []                        // Normal 1st level toggle opens and closes the first level
         * - togglePanel("panel3") => ["panel3"]                // Opening another "path" of the hierarchy
         * - togglePanel("panel3", "panel4") => ["panel3", "panel4"]
         * - togglePanel("panel1", "panel4") => ["panel1", "panel4"]    // Being a separate "path" is determined by last element only
         * - togglePanel("last") => ["panel1"]                  // Special keyword for closing the last panel
         * - togglePanel() => []                                // Closes all panels
         */
        toggleActivePanel(...panels: any[]): void;
        /**
         * Returns true if the panel is active (open).
         * @param panel - identifier of the panel.
         */
        isPanelActive(panel: string): boolean;
    }
}
declare module "controllers/calc/Calculator2016Controller" {
    import * as angular from "angular";
    import { calcReportType, Calculation, CalculationRowType, ResultRow } from "@salaxy/core";
    import { CalculationsService, Ng1Translations, ReportsService, SessionService, TaxCardService, UiHelpers, WorkersService } from "services/index";
    import { CalculatorPanels } from "controllers/calc/CalculatorPanels";
    import { CalculatorSections } from "controllers/calc/CalculatorSections";
    /**
     * Year 2016 implementation for the full salary calculator.
     * This implementation replaces the original 2015 implementation in Palkkaus.NgCalculator.Controllers namespace.
     */
    export class Calculator2016Controller implements angular.IController {
        private $scope;
        private calculationsService;
        private sessionService;
        private reportsService;
        private $sce;
        private uiHelpers;
        private translate;
        private $location;
        private workersService;
        private taxCardService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Type of the chart to show by default. */
        chartType: string;
        /** Active selected tab */
        protected activeTab: number;
        private _currentTab;
        private _dateRange;
        /**
         * Creates a new Calculator2016Controller
         * @param $scope - The Angular scope
         * @param calculationsService - Service that handles the communication to the server
         * @param sessionService - Service that gives information about the current session.
         * @param reportsService - Service that provides reporting capabilities.
         * @param $sce - $sce service.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param translate - Salaxy translation service.
         * @param $location - $location service
         * @param workersService - Service for worker managmenet.
         * @param taxCardService - Service for tax card management.
         * @ignore
         */
        constructor($scope: angular.IScope, calculationsService: CalculationsService, sessionService: SessionService, reportsService: ReportsService, $sce: angular.ISCEService, uiHelpers: UiHelpers, translate: Ng1Translations, $location: angular.ILocationService, workersService: WorkersService, taxCardService: TaxCardService);
        /** Current calculation object */
        readonly currentCalc: Calculation;
        /**
         * For the tab UI control, returns the current tab, if defined in the path.
         * Defaults to "default".
         */
        currentTab: "default" | "pro";
        /** Getter and setter for Calculation daternge in compatible format for new DateRange component. */
        dateRange: any;
        /**
         * Gets the Panels object for the current calculation.
         * It keeps track of which panel is currently shown.
         */
        readonly calcPanels: CalculatorPanels;
        /**
         * Gets the Sections object for the current calculation.
         * It keeps track of which section is currently shown and what text etc. is shown in that section.
         */
        readonly calcSections: CalculatorSections;
        /**
         * Gets the report-part HTML from server.
         * @param reportType Type of the report part.
         */
        getReportHtml(reportType: calcReportType): any;
        /**
         * Get calc result rows by category as defined in CalculationRowCategories class in @salaxy/core.
         */
        getResultRowsByCategory(category: any): ResultRow[];
        /**
         * Get calc result rows by a list of CalculationRowType.
         */
        getResultRowsByType(...types: CalculationRowType[]): ResultRow[];
        /**
         * Get calc result rows total by category as defined in CalculationRowCategories class in @salaxy/core.
         */
        getTotalForCategory(category: any): number;
        /**
         * Get calc result rows total by a list of CalculationRowType.
         */
        getTotalForRowsByType(...types: CalculationRowType[]): number;
        /**
         * Implement IController
         */
        $onInit(): void;
        /**
         * Returns true if the calculator is used as anonymous.
         * Meaning that the calculations cannot be stored to server (local storage saving possible).
         */
        isAnonymous(): boolean;
        /**
         * Sets the currentCalculationId to null and currentCalculation to an empty calc object.
         */
        resetCurrentCalculation(): void;
        /**
         * Checks whether the user is in a given role
         * @param {any} role - One of the known roles
         */
        isInRole(role: any): boolean;
        /**
         * Opens the login form for the current user
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login.
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        signIn(redirectUrl?: string, role?: string): void;
        /**
         * Opens the login dialog with signUp / register functionality
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        register(redirectUrl?: string, role?: string): void;
        /**
         * Shows a report for the current calcualtion using a modal dialog.
         * @param reportType - Type of report to show
         */
        showReportDialog(reportType: calcReportType): false;
        /**
         * Gets a URL for a printable report.
         * For paid calculations, this is PDF. For drafts etc. it is a HTML.
         * If report has not been saved (ID is null), returns null.
         * @param reportType - Type of report to show
         */
        getReportUrl(reportType: calcReportType): string;
        /** Takes the current calculation and creates a copy of it in draft state (typically, when a calculation is in Paid state). */
        copyAsNew(): void;
        /** Saves the current caltulation to server */
        saveCalcToServer(): void;
        /**
         * Deletes the currently selected calculation
         */
        deleteCalc(): void;
        /** Redirects using unique fraction */
        uniqueRedirect(url: string): void;
        /**
         * Deletes a row from the calculation
         * @param userRowIndex - A zero-based index of the row that should be deleted. Note that this is USER row: Input, not result row index.
         */
        deleteRow(userRowIndex: any): void;
        /**
         * Gets the detail info of the row as text. Designed to be displayed inside PRE-tag.
         * @param row - The row for which the detail info is concatenated.
         */
        getRowDetail(row: any): string;
        /**
         * Recalculates the current calculation using the API.
         */
        recalculate(): void;
        /**
         * Sets the household deduction flag to either true or false and does the recalculation.
         * This method should be set to household employers only.
         * @param isHouseholdDeduction If true, the amount should be calculated and reported to household deduction.
         */
        setHouseholdDeduction(isHouseholdDeduction: boolean): void;
        /**
         * Sets the worker to a given Worker ID.
         * @param id Identifier of the Worker in the Workers collection.
         * For anonymous testing, this may be one of the known example users: "example-default", "example-17" or "example-pensioner"
         */
        setWorker(id: string): void;
        /**
         * Reset worker in the calculations.
         */
        resetWorker(): void;
        /**
         * Intercepts a Form submit and takes the form data to the main salary object.
         * Expexted form fields are: kind, amount, price and message.
         *
         * @param $event Submit event: The data is gathered from the event and preventDefault() is called to preent normal form submit.
         */
        submitSalary($event: any): void;
        /**
         * Intercepts a Form submit and adds the form data to the rows collection (+recalculate).
         * Expexted form fields are: rowType, count, price and message.
         *
         * @param $event Submit event: The data is gathered from the event and preventDefault() is called to preent normal form submit.
         */
        submitRow($event: any): void;
        /** TODO: Will be rewritten */
        submitWorkTemp(): void;
        /** TODO: Waiting for rewrite  */
        submitWork(e: any, type: any): void;
        /**
         * Intercepts a Form submit and takes the form data to the main salary object.
         * Expexted form fields are: unionPayment and unionPaymentPercent.
         *
         * @deprecated Legacy method for setting the union payment.
         * Method should be replaced either with recalculate-based client side implementation or with separate micro service.
         *
         * @param $event Submit event: The data is gathered from the event and preventDefault() is called to preent normal form submit.
         *
         * @ignore
         */
        setUnionPayment($event: any): void;
        /** TODO: Waiting for rewrite  */
        changeConstructionTesParameters(tesToolAndTravelExpensesSelection: string): void;
        /** TODO: Waiting for rewrite  */
        getAgeRange(): "" | "Tuntematon" | "15v" | "16v" | "17v" | "18v - 52v" | "53v - 62v" | "63v - 64v" | "65v - 67v" | "68v tai yli";
        /** TODO: Waiting for rewrite  */
        setChartType(chartType: string): void;
        /**
         * The company account if the current account is company - and if the full account info is fetched
         */
        readonly company: any;
        /**
         * Checks if the salary rows kind/type is compensation. This is useful for customizing texts and other UI elements
         */
        hasCompensation(): boolean;
        /**
         * Adds a new row to the calculation
         */
        addCalculationRow(): void;
        /** Event handler for row message change */
        changeRowMessage(row: any, rowType: any): void;
        /**
         * Return the total for the row
         * @param rowIndex Index of row which total sum is asked
         */
        getRowTotal(rowIndex: any): number;
        /**
         * Returns the salary row's total
         */
        getSalaryTotal(): number;
        /**
         * Shows a dialog for adding a new worker.
         */
        showNewWorkerDialog(): void;
        /**
         * Shows a dialog for adding a new worker.
         */
        showWorkerEditDialog(initialTab: string): void;
        /** Returns validation for current calculation. */
        readonly validation: any;
        /** Returns true if the payment is possible when the employer pays all.  */
        readonly isForcePayAllAllowed: boolean;
        /**
         * Tab selection method.
         * @param activeTab - selected tab.
         */
        protected selectTab(activeTab: number): void;
        /** TODO: Waiting for rewrite  */
        protected refreshReports(): void;
        private setSantaClausDateAndDescription;
        private serializeObject;
    }
}
declare module "controllers/calc/Calculator2018Controller" {
    import * as angular from "angular";
    import { calcReportType, Calculation, CalculationRowType, DateRange, ResultRow, UserDefinedRow, WorkerAccount } from "@salaxy/core";
    import { CalcRowsService, CalculationsService, ReportsService, SessionService, UiHelpers, WorkersService } from "services/index";
    import { CalculatorSections } from "controllers/calc/CalculatorSections";
    /**
     * Year 2018 rewrite of the salary calculator.
     * This version prepares for 2019 Income registry, but is not necessarily 100% compatible.
     */
    export class Calculator2018Controller implements angular.IController {
        private calculationsService;
        private sessionService;
        private reportsService;
        private $sce;
        private uiHelpers;
        private workersService;
        private calcRowsService;
        private $location;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Type of the chart to show by default. */
        chartType: string;
        /** Active selected tab is currently used by CalcProResults component.
         * => To be removed as part of new Reporting logic rewrite (use simple tabs component instead).
         */
        protected activeTab: number;
        private _dateRange;
        /**
         * Creates a new Calculator2016Controller
         * @ignore
         */
        constructor(calculationsService: CalculationsService, sessionService: SessionService, reportsService: ReportsService, $sce: angular.ISCEService, uiHelpers: UiHelpers, workersService: WorkersService, calcRowsService: CalcRowsService, $location: angular.ILocationService);
        /**
         * Implement IController
         */
        $onInit(): void;
        /** Current calculation object */
        readonly currentCalc: Calculation;
        /** Getter and setter for Calculation daterange in compatible format for new DateRange component. */
        dateRange: DateRange;
        /**
         * Gets the Sections object for the current calculation.
         * It keeps track of which section is currently shown and what text etc. is shown in that section.
         */
        readonly calcSections: CalculatorSections;
        /** Gets read-only from the UI point-of-view: Whether the inputs should generally beeditable.  */
        readonly isReadOnly: boolean;
        /**
         * Gets the report-part HTML from server.
         * @param reportType Type of the report part.
         */
        getReportHtml(reportType: calcReportType): any;
        /**
         * Get calc result rows by category as defined in CalculationRowCategories class in @salaxy/core.
         */
        getResultRowsByCategory(category: any): ResultRow[];
        /**
         * Get calc result rows by a list of CalculationRowType.
         */
        getResultRowsByType(...types: CalculationRowType[]): ResultRow[];
        /**
         * Get calc result rows total by category as defined in CalculationRowCategories class in @salaxy/core.
         */
        getTotalForCategory(category: any): number;
        /**
         * Opens the login form for the current user
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login.
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        signIn(redirectUrl?: string, role?: string): void;
        /**
         * Opens the login dialog with signUp / register functionality
         *
         * @param redirectUrl - The URL where the user is taken after a successfull login
         * @param role - Optional role (household, worker or company) for the situations where it is known - mainly for new users
         */
        register(redirectUrl?: string, role?: string): void;
        /**
         * Shows a report for the current calculation using a modal dialog.
         * @param reportType - Type of report to show
         */
        showReportDialog(reportType: calcReportType): false;
        /**
         * Gets a URL for a printable report.
         * For paid calculations, this is PDF. For drafts etc. it is a HTML.
         * If report has not been saved (ID is null), returns null.
         * @param reportType - Type of report to show
         */
        getReportUrl(reportType: calcReportType): string;
        /** Takes the current calculation and creates a copy of it in draft state (typically, when a calculation is in Paid state). */
        copyAsNew(): void;
        /**
         * Deletes the currently selected calculation
         */
        deleteCalc(): void;
        /** Redirects using unique fraction */
        uniqueRedirect(url: string): void;
        /**
         * Deletes a row from the calculation
         * @param userRowIndex - A zero-based index of the row that should be deleted. Note that this is USER row: Input, not result row index.
         */
        deleteRow(userRowIndex: any): void;
        /**
         * Gets the detail info of the row as text. Designed to be displayed inside PRE-tag.
         * @param row - The row for which the detail info is concatenated.
         */
        getRowDetail(row: any): string;
        /**
         * Recalculates the current calculation using the API.
         * @returns A Promise with result data (Calculation)
         */
        recalculate(): Promise<Calculation>;
        /** Saves the current calculation to server */
        saveCalcToServer(): void;
        /**
         * Sets the household deduction flag to either true or false and does the recalculation.
         * This method should be set to household employers only.
         * @param isHouseholdDeduction If true, the amount should be calculated and reported to household deduction.
         */
        setHouseholdDeduction(): void;
        /**
         * Gets a description text for a WorkerAccount.
         * @param worker Worker account to describe.
         */
        getWorkerDescription(worker: WorkerAccount): any;
        /**
         * Sets the worker to a given Worker ID.
         * @param id Identifier of the Worker in the Workers collection.
         * For anonymous testing, this may be one of the known example users: "example-default", "example-17" or "example-pensioner"
         */
        setWorker(id: string): void;
        /**
         * Submit the data for the Worker dialog.
         * Note: Set / reset of worker ID is a separate method.
         * This submit is only for editing the employment data that is in the same dialog.
         */
        submitWorker(): void;
        /**
         * Reset worker in the calculations.
         */
        resetWorker(): void;
        /**
         * Sets the type of the chart
         * @param chartType Supported cahrt types are bar (default) and pie.
         */
        setChartType(chartType: "bar" | "pie"): void;
        /**
         * Checks if the salary rows kind/type is compensation. This is useful for customizing texts and other UI elements
         */
        hasCompensation(): boolean;
        /**
         * Adds a new row to the calculation.
         * Takes into account the empty rows that aree temporarily added by the UI.
         * Adds defaults etc. based on config.
         * @param type Type of the row.
         * @param closefunction Optional close function that is called before calling showEdit()
         */
        addNewRow(type: CalculationRowType, closefunction: () => any): void;
        /**
         * Shows the Edit dialog for calculation row.
         * @param row Row to edit in the dialog.
         */
        showEdit(row: UserDefinedRow): void;
        /** Show a selection list for calculation items. */
        showDialog(dialog?: "list-expenses" | "list-benefits" | "list-deductions" | "list-salaries" | "details"): void;
        /**
         * Returns the salary row's total
         */
        getSalaryTotal(): number;
        /**
         * Shows a dialog for adding a new worker.
         */
        showNewWorkerDialog(): void;
        /**
         * Shows a dialog for adding a new worker.
         */
        showWorkerEditDialog(initialTab?: string): void;
        /** Gets the occupation text for the current worker / calculation */
        getOccupationText(): string;
        /** Returns date range text. */
        getDatesRangeText(): string;
        /** Returns validation for current calculation. */
        readonly validation: any;
        /** Returns true if the payment is possible when the employer pays all.  */
        readonly isForcePayAllAllowed: boolean;
    }
}
declare module "controllers/calc/PayrollController" {
    import { ApiListItemValidation, Avatar, calcReportType, Calculation, Payroll, PayrollRow, WorkerAccount } from "@salaxy/core";
    import { PayrollService, ReportsService, TaxCardService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
     * Typical usecase is that a a company has e.g. a monthly salary list that is paid
     * at the end of month. For next month, a copy is then made from the latest list and
     * the copy is potentially modified with the changes of that particular month.
     * Payroll can also be started from scratch either by just writing salaries from
     * e.g. an e-mail or by uploading an Excel sheet.
     */
    export class PayrollController extends CrudControllerBase<Payroll> {
        private payrollService;
        private taxCardService;
        private workersService;
        private reportsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Index of the active tab in the combined edit view. */
        activeTab: number;
        /** Gets a new row for data binding. */
        newRow: PayrollRow;
        /** Import data */
        tsv: string;
        /** Bound worker addition function  */
        onAddWorker: any;
        /**
         * Payroll that is being validated.
         * This is null when validation is in progress or if validation has not been started.
         */
        readonly validatedPayroll: Payroll;
        constructor(payrollService: PayrollService, taxCardService: TaxCardService, workersService: WorkersService, reportsService: ReportsService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
        /** List of all templates (Payroll items with status "template"). */
        readonly templates: Payroll[];
        /** List of all Payroll instances (not templates). */
        readonly instances: Payroll[];
        /** Returns the validation object for the validated payroll. */
        getValidation(index: number): ApiListItemValidation;
        /** Imports tab separated values to current row collection. */
        importRows(): void;
        /**
         * Sets the Payroll as template or switches it back to Draft.
         * @param payroll Payroll to modify
         * @param isTemplate If true (default) sets a Draft Payroll as Template.
         * If false, swithces it back to normal Payroll with status Draft.
         */
        setAsTemplate(payroll: Payroll, isTemplate?: boolean): void;
        /** Checks that the taxcard is OK for a given user. */
        isTaxCardOk(row: PayrollRow): boolean;
        /** Gets a Worker object for a PayrollRow */
        getWorker(row: PayrollRow): WorkerAccount;
        /** Returns true if the row has worker selected. False if not. */
        hasWorker(row: PayrollRow): boolean;
        /** Gets a Worker Avatar object for a PayrollRow */
        getWorkerAvatar(row: PayrollRow): Avatar;
        /**
         * Creates a new item with default title. The item is not saved in this process yet.
         * Item is set as current unless onCreateNew is specified => Then you are responsible for doing it yourself.
         * If detailsUrl is specified, the browser is redirectedted there.
         * Current item is set also in this case (as opposed to listSelect) because the new item does not yet have an id
         * and passing it in current item is the only way using router.
         * Use onCreateNew event or crudService.getBlank() if you do not want to set the current item.
         * @param newItem Specify the new item if you want to initialize it with specific values.
         * In most cases, you should let the system create it with defaults.
         */
        createNew(newItem?: Payroll): Payroll;
        /**
         * Creates a new salary list as template
         */
        createNewTemplate(): Payroll;
        /**
         * Adds a new row to the payroll
         */
        addRow(row: PayrollRow): void;
        /** Called when the Worker is changed on the row. Updates the defaults. */
        workerChanged(row: PayrollRow): void;
        /** Removes selected rows */
        removeRows(): void;
        /**
         * Validates the current Payroll object and sets it as validatedPayroll.
         */
        validateCurrent(): void;
        /**
         * Shows a dialog for adding missing tax card for the worker.
         * @param workerAccountId - Worker id.
         */
        showTaxCardAddDialogForPayrollRow(row: PayrollRow, workerAccountId: string): void;
        /**
         * Shows a dialog for adding missing tax card for the worker.
         * @param workerAccountId - Worker id.
         */
        showTaxCardAddDialogForPayrollCalculation(calculation: Calculation, workerAccountId: string): void;
        /**
         * Shows a dialog for adding a new worker or editing an existing worker.
         * @param workerAccountId - Worker id or null for new.
         */
        showWorkerEditDialogForPayrollRow(row: PayrollRow, workerAccountId?: string): void;
        /**
         * Shows a dialog for adding a new worker or editing an existing worker.
         * @param workerAccountId - Worker id or null for new.
         */
        showWorkerEditDialogForPayrollCalculation(calculation: Calculation, workerAccountId?: string): void;
        /** Start adding worker in validation view */
        addWorkerInValidation(): void;
        /**
         * Shows the "Are you sure?" dialog and if user clicks OK, deletes the item.
         * Cancels the started payment for the payroll too.
         *
         * @returns Promise that resolves to true if the item is deleted.
         * False, if user cancels and fails if the deletion fails.
         */
        delete(id: string): Promise<boolean>;
        /**
         * Shows a report for the current payroll calculation using a modal dialog.
         * @param reportType - Type of report to show
         * @param calculation - Calculation for the report
         */
        showReportDialog(reportType: calcReportType, calculation: Calculation): void;
    }
}
declare module "controllers/calc/PaymentController" {
    import * as angular from "angular";
    import { Calculation, CalculationResult, Payment, Payroll } from "@salaxy/core";
    import { OnboardingService } from "services/OnboardingService";
    import { PaymentService } from "services/PaymentService";
    import { ReportsService } from "services/ReportsService";
    import { SessionService } from "services/SessionService";
    import { UiHelpers } from "services/ui/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Contains functionality for executing payments for payrolls or calculations.
     */
    export class PaymentController extends CrudControllerBase<Payment> {
        protected $scope: angular.IScope;
        protected paymentService: PaymentService;
        protected $sce: angular.ISCEService;
        protected sessionService: SessionService;
        protected onboardingService: OnboardingService;
        protected reportsService: ReportsService;
        private $window;
        protected data: any;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         *  Title for the payment dialog.
         *  This is typically customized in custom payment scenarios.
         */
        paymentDialogTitle: string;
        /**
         * Payment instructions in the payment dialog.
         * This is typically customized in custom payment scenarios.
         */
        paymentInstructions: string;
        /**
         * Text for the payment button
         * This is typically customized in custom payment scenarios.
         */
        paymentButtonText: string;
        /** Current payment view. */
        paymentView: string;
        /** Class for printing */
        printClass: "sxyFullPrint" | "sxySecurePrint";
        /** Shows only valid salary dates. */
        salaryDateOptions: {
            dateDisabled: (data: any) => boolean;
            minDate: any;
        };
        /** Shows salary date dialog */
        showSalaryDate: boolean;
        /** Salary date for the date control */
        requestedSalaryDate: any;
        /** Paytrail payment url. */
        paytrailUrl: string;
        /** Flag for pending save. */
        protected isPendingSave: boolean;
        /** All calculations in the business object. */
        private _calculations;
        /** All calculation results summed together */
        private _totalResult;
        /** Contains rendered calcs */
        private _paymentReportsCalcs;
        /** Payment reports. */
        private _paymentReportRefreshing;
        private _paymentReports;
        /** Contains rendered results */
        private _totalPaymentReportResult;
        private _totalPaymentReportRefreshing;
        /** Total payment report html */
        private _totalPaymentReport;
        /**
         * Creates a new PaymentController.
         * @param $scope - Angular scope.
         * @param paymentService - Service for payment processing.
         * @param $location - Angular $location service.
         * @param $attrs - Angular @attrs service.
         * @param uiHelpers - Ui helpers service.
         * @param $sce - Angular $sce service.
         * @param sessionService - Session service.
         * @param onboardingService - Onboarding service.
         * @param reportsService - Reports service.
         * @param data - Dialog control parmeters
         * @ignore
         */
        constructor($scope: angular.IScope, paymentService: PaymentService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, $sce: angular.ISCEService, sessionService: SessionService, onboardingService: OnboardingService, reportsService: ReportsService, $window: angular.IWindowService, data: any);
        /**
         * Shows the payment dialog for current business object.
         * @param isPayrollPayment - If true, the business object is the current payroll. By default the business object is the current calculation.
         * @param returnUrl - Return url after the payment.
         */
        showPaymentDialog(isPayrollPayment?: boolean, returnUrl?: string): void;
        /** Shows the payment view. Commits pending saves before opening the view. */
        setPaymentView(paymentView: string): void;
        /**
         * The actual payment launch action. In the default implementation this shows the payment selection view.
         * In the custom payment scenarios, this should save the pending business objects, start the payment and
         * do the custom action. Finally it should show a success/failure dialog.
         * @param close - function for closing the dialog.
         */
        pay(close: () => void): void;
        /**
         * Starts the payment and generates a reference number for the credit transfer.
         * Shows the view for credit transfer.
         */
        startTransferPayment(): void;
        /** Shows print preview. Timeout for setting a css class before print preview. */
        print(printClass?: "sxyFullPrint" | "sxySecurePrint"): void;
        /**
         * If the total payment is 0 (the employer handles the payments),
         * starting the payment also updates the payrolls or calculations to paymentSucceeded state.
         *
         * @returns Promise: none = no null payment possible, cancel = possible but canceled, paid = possible and paid.
         */
        tryNullPayment(): Promise<("none" | "cancel" | "paid")>;
        /**
         * Saves the current calculations in status paymentSucceeded.
         */
        saveCalculationAsPaymentSucceeded(close: () => void): void;
        /**
         * Saves pending business objects.
         */
        savePendingBusinessObjects(): Promise<boolean>;
        /**
         * Shows an alert if the signature is missing.
         */
        showMissingSignatureAlert(): void;
        /** Business objects for the payment */
        readonly businessObject: Payroll | Calculation;
        /** Returns true if the payment is for payroll */
        readonly isPayrollPayment: boolean;
        /**
         * Checks if the worker has a social security number, a bank account number, tax percent and tax card type
         * Now a minimal implementation, in the future this method show just check this from the calculation object's isReadyForPaymentProperty
         * which requires also a phone number and an email to be ready for payment
         */
        readonly isReadyForPayment: boolean;
        /** Returns the payment page for the dialog. */
        readonly paymentPage: "salaxy-components/modals/payment/PaymentPages/Selection.html" | "salaxy-components/modals/payment/PaymentPages/CreditTransfer.html" | "salaxy-components/modals/payment/PaymentPages/Info.html";
        /**
         * Returns the paytrail payment url.
         */
        readonly paytrailPaymentUrl: string;
        /** Sum of total payment in business objects. */
        readonly totalPayment: number;
        /** Returns true if the requested salary date has been set. */
        readonly isRequestedSalaryDate: boolean;
        /** Shows control for setting salary date. */
        showSalaryDateControl(): void;
        /** Change salary date. */
        changeRequestedSalaryDate(): void;
        /** Reset salary date. */
        resetRequestedSalaryDate(): void;
        /** Returns the salary date */
        getSalaryDate(): string;
        /** Returns disabled salary dates for the date picker. */
        getDisabledSalaryDates(data: any): boolean;
        /** Returns minimum salary date. */
        getMinSalaryDate(): Date;
        /** Trust helper for views. */
        trust(html: any): any;
        /** Returns all calculations in the business object. */
        readonly calculations: Calculation[];
        /** Returns CalculationResult for all calculations in the business object. */
        readonly totalResult: CalculationResult;
        /** returns total palkkaus */
        readonly totalPalkkaus: number;
        /** returns total gross salaries  */
        readonly totalGrossSalaries: number;
        /** Returns paymentReports for all calculations in the business object. */
        readonly paymentReports: Array<{
            /** Html string for the report. */
            html: string;
            /** Index number in the array. */
            index: number;
            /** Worker's name. */
            worker: string;
            /** Flag for readiness */
            ready: boolean;
        }>;
        /** Returns total paymentReports html for all calculations in the business object. */
        readonly totalPaymentReport: string;
        private getPayableBusinessObject;
    }
}
declare module "controllers/calc/index" {
    export * from "controllers/calc/CalcChartController";
    export * from "controllers/calc/CalcIrRowsController";
    export * from "controllers/calc/CalcWorktimeController";
    export * from "controllers/calc/CalcRowsController";
    export * from "controllers/calc/CalculationGroupController";
    export * from "controllers/calc/CalculationsController";
    export * from "controllers/calc/Calculator2016Controller";
    export * from "controllers/calc/Calculator2018Controller";
    export * from "controllers/calc/CalculatorPanels";
    export * from "controllers/calc/CalculatorSection";
    export * from "controllers/calc/CalculatorSections";
    export * from "controllers/calc/PayrollController";
    export * from "controllers/calc/PaymentController";
}
declare module "controllers/helpers/AlertController" {
    import * as angular from "angular";
    /**
     * Controller rendering an Alert box in the page area.
     */
    export class AlertController implements angular.IController {
        private $transclude;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Type of the alert is the Bootstrap style: Note that also "primary" and "default" are supported. */
        type: "default" | "primary" | "success" | "info" | "warning" | "danger";
        /**
         * Alert main content as simple text.
         * You can alternatively provide html as main element.
         * NOTE: If necessary we could provide a html property to bind from JavaScript and to allow HTML in the binded content.
         */
        text: string;
        /**
         * Possibility to speicfy a font-awesome icon.
         * Setting "none", will show no icon.
         * If not set, it is determined by type.
         */
        icon: "none" | string;
        /** If true (and hasMoreInfo is true), the more info html is shown. */
        showMoreInfo: boolean;
        /**
         * Creates a new AlertController
         * @ignore
         */
        constructor($transclude: angular.ITranscludeFunction);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Returns true if details part (aside element) is filled / "More info" button should be shown. */
        readonly hasMoreInfo: boolean;
        /** Returns the icon based on the type. */
        getIcon(): string;
    }
}
declare module "controllers/helpers/AvatarController" {
    import * as angular from "angular";
    /**
     * Controller rendering a Salaxy account Avatar (image for the person / company).
     */
    export class AvatarController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** The Avatar object which the controller is rendering */
        avatar: any;
        /**
         * Creates a new AvatarController
         * @ignore
         */
        constructor();
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Returns true if the avatar is an image url (as opposed to icon rendering) */
        readonly isImage: boolean;
        /** Returns true if the avatar should be rendered as a person icon */
        readonly isPersonIcon: boolean;
        /** Returns true if the avatar should be rendered as a company icon */
        readonly isCompanyIcon: boolean;
        /** Returns the avatar color or 'gray' if nothing is defined. */
        readonly color: string;
        /** Returns the initials for rendering the avatar */
        readonly initials: string;
    }
}
declare module "controllers/helpers/CalendarController" {
    import * as angular from "angular";
    import { DateRange } from "@salaxy/core";
    /**
     * Renders a calendar control for visualisation of holidays.
     */
    export class CalendarController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** NOT IMPLEMENTED: Specifies how the weekdays are aligned. TODO: "align-left" */
        mode: "align-weekdays" | "align-left";
        /**
         * Start date for the calendar as ISO date yyyy-MM-dd. Renders from the beginning of this month.
         */
        start: string;
        /**
         * End date of the calendar as ISO date yyyy-MM-dd. Renders until the end of this month.
         */
        end: string;
        /** Periods / date ranges plotted on the chart. */
        periods: Array<{
            /** Period to plot. */
            range: DateRange;
            /** Title for the period. */
            title: string;
            /** Description for the period. */
            description: string;
        }>;
        /** Single dates that are plotted on the chart. */
        days: Array<{
            /** Date to plot. */
            date: string;
            /** Title for the date. */
            title: string;
            /** Description for the date. */
            description?: string;
        }>;
        /**
         * Function that is called when user selects an item in the calendar.
         * Function can have the following locals: type: "period" | "day" | "emptyDay",  date: string, selectedItem: number
         * @example <salaxy-calendar on-list-select="$ctrl.myCustomSelectFunc(type, date, selectedItem)"></salaxy-calendar>
         */
        onListSelect: (params: {
            /** Selection type. */
            type: "period" | "day" | "emptyDay";
            /** Seleted date. */
            date: string;
            /** Index in the date range. */
            selectedItem: number;
        }) => void;
        /** Weekedays: Currently this is Finnish texts. May later be something translateable: Eithe numbers or strings. */
        weekdays: string[];
        /** Gets repeatable weekdays list for the total length of . */
        weekdaysForMonth: Array<{
            /** Weekday index number in month. */
            ix: number;
            /** Week day number in week. */
            weekday: number;
            /** Text for the week day. */
            text: string;
        }>;
        /** Cache for calendar data */
        cache: any;
        /** Cache key for calendar data. */
        cacheKey: any;
        constructor();
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** The collection of months that the calendar renders. */
        readonly months: any;
        /**
         * Gets the short text "ma", "ti", "ke", "to", "pe", "la", "su".
         * @param date Date as ISO string or date
         */
        getShortWeekdayText(date: any): string;
        /**
         * A range was clicked
         * @param rangeIndex Index of the range that was clicked.
         */
        rangeClicked(rangeIndex: number, day: any): void;
        /**
         * A date was clicked
         * @param day Day that was clicked.
         */
        dateClicked(day: any): void;
        /** The actual object with potentially Moment objects etc. is too complex to be used as cache key */
        private getCacheKey;
    }
}
declare module "controllers/helpers/ChartController" {
    import * as angular from "angular";
    /**
     * Controller rendering a angular-chart.js (http://jtblin.github.io/angular-chart.js) with Salaxy defaults, caching, colors etc.
     */
    export class ChartController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /**
         * Function that is called for chart data, options and other properties of chart.
         * The caching makes sure that the function is not called more often than once a second.
         * Binding to Angular UI is only done if the datra property changes.
         * @example <salaxy-chart on-get-chart="$ctrl.getChart()"></salaxy-chart>
         */
        onGetChart: () => {
            /** Type of the chart */
            type: "line" | "bar" | "doughnut" | "radar" | "pie" | "polarArea" | "horizontalBar" | "bubble";
            /** series data */
            data: any[];
            /** x axis labels */
            labels: any;
            /** Chart.js options */
            options?: any;
            /** series labels */
            series?: any;
            /** onclick event handler */
            click?: any;
            /** onmousemove event handler */
            hover?: any;
            /** colors for the chart */
            colors: any;
            /** override datasets individually */
            datasetOverride: any;
        };
        /** Type of the chart. */
        type: "line" | "bar" | "doughnut" | "radar" | "pie" | "polarArea" | "horizontalBar" | "bubble";
        private chartCache;
        private chartCacheUpdated;
        constructor();
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Force refresh of cache */
        refresh(): void;
        /**
         * Gets the cached version of chart data and options.
         * Cache refreshes if the type or data changes.
         * However, the check for data is done only every 1 second.
         */
        getChartCached(): any;
    }
}
declare module "controllers/helpers/ImportController" {
    import { ImportResult, WorkerAccount, Workers } from "@salaxy/core";
    import { WorkersService } from "services/index";
    /**
     * UNDERCON: Provides functionality to import Workers to the Salaxy system.
     */
    export class ImportController implements angular.IController {
        private workersApi;
        private workersService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Data as tab-separated values (tsv) */
        tsv: string;
        /** Results of the import. */
        result: ImportResult;
        /** Worker accounts as a result of the import */
        workerAccounts: WorkerAccount[];
        constructor(workersApi: Workers, workersService: WorkersService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Does the import process from Excel copy-paste until
         * the list of to-be-created Worker accounts.
         */
        previewWorkerAccountImport(): void;
        /**
         * Creates the worker accounts in-memory.
         * This method does not yet commit to database.
         */
        createWorkerAccounts(importedData: ImportResult): void;
        /** Validates the worker data */
        workerDataValidation(importedData: ImportResult): void;
        /** Creates all the workers in to the database. */
        commitWorkerAccountsToServer(): void;
        /**
         * Workers are created one-by-one as an asyncronous process.
         * This method creates one worker and when confirmation is received from the server,
         * calls itself again to continue the process untill all workers have been created.
         */
        protected createNextWorker(): void;
        private checkRequired;
        private addValidationError;
    }
}
declare module "controllers/helpers/NaviController" {
    import * as angular from "angular";
    import { NaviService, SalaxySitemapNode, SalaxySitemapSection } from "services/index";
    /**
     * Helper controller to generate navigation components and views:
     * Top- and side-menus, paths and controls that show the current title.
     * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
     *
     * NOTE: This is just an optional helper to make creating simple demo sites easier.
     * There is no need to use NaviService, NaviController or components in your custom site!
     * You can use something completely different.
     */
    export class NaviController implements angular.IController {
        private naviService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Display mode of the sitemap / navi: "default" or "accordion"
         * default: Shows full two levels of the tree. Typically used as sitemap in the content area.
         * accordion: Shows first level of the navi tree and second level only it is in the current path.
         *            Typically used in left menu navigation.
         */
        mode: "default" | "accordion";
        /**
         * Creates a new NaviController
         * @param naviService - Service that stores the navigation state.
         * @ignore
         */
        constructor(naviService: NaviService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Returns the main sitemap object that is used in rendering the navigation.
         * Set the sitemap by setting the angular constant or object "SITEMAP"
         */
        readonly sitemap: SalaxySitemapSection[];
        /**
         * Retuns the identifier of the currently selected section.
         * If nothing else is selected, it will return the first section, by convention called 'home'.
         */
        getSectionId(): string;
        /** Toggles the isNaviOpen property */
        toggleNavi(): void;
        /**
         * True if the navigation (e.g. left sidebar) is open.
         * Typically, this property is used only in narrow (mobile) view, otherwise the sidebar is always shown.
         */
        isNaviOpen: boolean;
        /** Toggles the isSidebarOpen property */
        toggleSidebar(): void;
        /**
         * True if the secondary sidebar (e.g. right sidebar) is open.
         */
        isSidebarOpen: boolean;
        /**
         * Retuns the currently selected section (the entire object).
         * If nothing else is selected, it will return the first section.
         */
        getSection(): SalaxySitemapSection;
        /**
         * Returns true if the given node is the currently selected node.
         */
        isCurrent(siteMapNode: SalaxySitemapNode): boolean;
        /**
         * Returns true if the given node is the currently selected node.
         */
        isCurrentSection(section: SalaxySitemapSection): boolean;
        /** Returns the page / node title */
        readonly title: string;
        /** Gets the currently selected node. */
        readonly current: SalaxySitemapNode;
    }
}
declare module "controllers/helpers/SpinnerController" {
    import * as angular from "angular";
    /**
     * Controller rendering a Spinner (please wait) user interfaces.
     */
    export class SpinnerController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** If true, the spinner is shown full screen. */
        fullScreen: boolean;
        /**
         * Heading is the larger text under the spinner.
         * The text is translated.
         */
        heading: string;
        /**
         * Small text - use pre for line breaks.
         * The text is translated.
         */
        text: string;
        /**
         * Creates a new SpinnerController
         * @ignore
         */
        constructor();
        /**
         * Implement IController
         */
        $onInit: () => void;
    }
}
declare module "controllers/helpers/SwitchController" {
    import * as angular from "angular";
    /**
     * Controller for Salaxy switch (a graphic checkbox)
     */
    export class SwitchController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Switch name */
        name: string;
        /** Id for the switch */
        id: string;
        /** The model that is bound to the switch */
        model: any;
        /** The value of the on text */
        onText: string;
        /** The value of the off text */
        offText: string;
        /** If true, the switch will be disabled and its value cannot be changed by clicking/toggling it */
        disabled: boolean;
        /**
         * Creates a new SwitchController
         * @ignore
         */
        constructor();
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Event handler for switch change. */
        onChange(): void;
    }
}
declare module "controllers/helpers/index" {
    export * from "controllers/helpers/AlertController";
    export * from "controllers/helpers/AvatarController";
    export * from "controllers/helpers/CalendarController";
    export * from "controllers/helpers/ChartController";
    export * from "controllers/helpers/ImportController";
    export * from "controllers/helpers/NaviController";
    export * from "controllers/helpers/SpinnerController";
    export * from "controllers/helpers/SwitchController";
}
declare module "controllers/communications/CmsController" {
    import * as angular from "angular";
    import { AbcSection, Article } from "@salaxy/core";
    import { CmsService } from "services/index";
    /**
     * Handles user interaction for fetrching and showing Articles and related metadata from
     * Palkkaus.fi Content Management System (CMS)
     */
    export class CmsController implements angular.IController {
        private cmsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(cmsService: CmsService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Gets an article form the server. Note that the process is ASYNC
         * so first you will get an empty article object.
         * Bind the article to ui and angular will update the object after retrieval automatically.
         * @param articleId Identifier for the article
         * @param articleLinkPattern Optional pattern for CMS links from one article to another.
         * The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         * The caller can change this to direct internal links to appropriate page template.
         */
        getArticle(articleId: string, articleLinkPattern?: string): any;
        /**
         * Gets the current article form the server. Note that the process is ASYNC
         * so first you will get an empty article object.
         * Bind the article to ui and angular will update the object after retrieval automatically.
         * @param articleLinkPattern Optional pattern for CMS links from one article to another.
         * The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
         * The caller can change this to direct internal links to appropriate page template.
         */
        getCurrentArticle(articleLinkPattern?: string): any;
        /**
         * Gets a list of articles from the server.
         * This list does not contain the article contents, just the metadata.
         * @param section - Section to filter by. If null, all sections are returned.
         */
        getArticleList(section: AbcSection): Article[];
    }
}
declare module "controllers/communications/MessageController" {
    import * as angular from "angular";
    import { Ajax } from "@salaxy/core";
    import { AlertService } from "services/index";
    /**
     * Handles user messages to the customer support and potentially other messaging functionality in the future.
     */
    export class MessageController implements angular.IController {
        private alertService;
        private ajax;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new MessageController
         * @param alertService - Palkkaus alert service
         * @param ajax - The ajax object that is used for error handling if connection to the Onnistuu.fi-service cannot be made
         * @ignore
         */
        constructor(alertService: AlertService, ajax: Ajax);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Sends an e-mail to support from an authenticated current user.
         * @param title - Title for the e-mail
         * @param message - E-mail body
         * @param successMessage - Message that is shown through the Alert service after the message has been successfully sent. Leave this null to not show any message
         */
        sendEmailAuthenticated(title: string, message: string, successMessage: string): Promise<any>;
    }
}
declare module "controllers/communications/EmailMessageController" {
    import { EmailMessage } from "@salaxy/core";
    import { ContactService, EmailMessageService, SessionService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user messages to the customer support and potentially other messaging functionality in the future.
     */
    export class EmailMessageController extends CrudControllerBase<EmailMessage> {
        private emailMessageService;
        private contactService;
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Send to address from the binding. */
        mailTo: string;
        /**
         * Creates a new MessageController
         * @param emailMessageService - Email message service
         * @param $location - Angular location service.
         * @param $attrs - Angular attributes service.
         * @param uiHelpers - Salaxy UI helpers service.
         * @param contactService - Contact service
         * @param sessionService - Service that gives information about the current session.
         * @ignore
         */
        constructor(emailMessageService: EmailMessageService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, contactService: ContactService, sessionService: SessionService);
        /** List of receiver candidates. */
        readonly receiverList: any[];
        /**
         * Sets the current receiver.
         * @param id - id of the receiver.
         */
        setCurrentReceiver(id: string): void;
        /**
         * Checks whether the user is in a given role
         * @param  role - One of the known roles
         */
        isInRole(role: any): boolean;
    }
}
declare module "controllers/communications/SmsMessageController" {
    import { SmsMessage } from "@salaxy/core";
    import { ContactService, SmsMessageService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user messages to the customer support and potentially other messaging functionality in the future.
     */
    export class SmsMessageController extends CrudControllerBase<SmsMessage> {
        private smsMessageService;
        private contactService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new MessageController
         * @param smsMessageService - Sms message service
         * @param $location - Angular location service.
         * @param $attrs - Angular attributes service.
         * @param uiHelpers - Salaxy UI helpers service.
         * @param contactService - Contact service
         * @ignore
         */
        constructor(smsMessageService: SmsMessageService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, contactService: ContactService);
        /** Return list of receivers */
        readonly receiverList: any[];
        /** Sets selected receiver's address */
        setCurrenReceiver(id: string): void;
    }
}
declare module "controllers/communications/WelcomeController" {
    import * as angular from "angular";
    import { InsuranceProduct, PensionProduct } from "@salaxy/core";
    import { AccountService, CalculationsService, PayrollService, SessionService, WorkersService } from "services/index";
    /**
     * Provides methods for profiled Welcome messages to the end user.
     */
    export class WelcomeController implements angular.IController {
        private session;
        private calculations;
        private workers;
        private payrolls;
        private account;
        private $location;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * If set to true, the settings are handled by proxy:
         * we do not ask the user to change the settings in
         * the Welcome screen (mainly Pension or Insurance).
         */
        settingsByProxy: any;
        /**
         * Creates a new WelcomeController
         * @ignore
         */
        constructor(session: SessionService, calculations: CalculationsService, workers: WorkersService, payrolls: PayrollService, account: AccountService, $location: angular.ILocationService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Moves the browser to create-new calculation screen
         * @param workerId Worker ID for which to create the calculation.
         */
        createNewCalculationFor(workerId: string): void;
        /** Gets the current pension product or empty object */
        getPension(): PensionProduct;
        /** Gets the current insurance product or empty object */
        getInsurance(): InsuranceProduct;
        /** Gets the flags that indicate which tutorials to show. */
        readonly tutorials: {
            /** If true, shows info box that the site is runnin in anonymous mode. */
            showAnon: boolean;
            /** If true, should direct the user to create the first Worker */
            showAddWorker: boolean;
            /** If true, should direct user to create a new calculation.  */
            showAddCalculation: boolean;
            /** If true, should ask the user for digital signature. */
            showSign: boolean;
            /** If true, should ask the user to fill in the Pension contract information. */
            showPension: boolean;
            /** If true, should ask the user to fill in the Accident Insurance contract information. */
            showInsurance: boolean;
            /** If true, should promote Payroll feature. */
            showTryPayroll: boolean;
            /** If true, should promote new features. */
            showNewFeatures: boolean;
        };
        /**
         * Returns system alert html.
         */
        readonly systemAlertHtml: string;
        /**
         * Returns true if system alert exists.
         */
        readonly hasSystemAlert: boolean;
    }
}
declare module "controllers/communications/index" {
    export * from "controllers/communications/CmsController";
    export * from "controllers/communications/MessageController";
    export * from "controllers/communications/EmailMessageController";
    export * from "controllers/communications/SmsMessageController";
    export * from "controllers/communications/WelcomeController";
}
declare module "controllers/form-controls/FormGroupLabelType" {
    /**
     * Defines how the label is positioned in relation to input.
     * Supported values are:
     *
     * - "horizontal" (default): Bootstrap form-horizontal form-control (https://getbootstrap.com/docs/3.3/css/#forms-horizontal)
     * - "basic": Basic Bootstrap form-group with label on top of input (https://getbootstrap.com/docs/3.3/css/#forms-example)
     * - "plain": Just the payload (typically input) with span class="salaxy-form-group-plain" around it.
     * - "no-label": Form control of width "col-sm-12" without label.
     *
     * Default is currently horizontal, but it may later be made configurable.
     */
    export type FormGroupLabelType = "horizontal" | "no-label" | "plain" | "basic" | "empty-label";
}
declare module "controllers/form-controls/_InputBase" {
    import * as angular from "angular";
    import { FormGroupLabelType } from "controllers/form-controls/FormGroupLabelType";
    /**
     * Base controller for form control groups (label, input, validation errors etc.).
     */
    export class InputBase<T> implements angular.IController {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        static crudBindings: {
            /** Name of the input - also used as id. */
            name: string;
            /** Label for the control */
            label: string;
            /** If true the field is required (form validation) */
            required: string;
            /**
             * Expression for read-only display of the input control
             * This is visualized in a different way than readonly: The input is not shown and display is more compact.
             */
            readOnly: string;
            /** Expression for ng-readonly of the input */
            readonly: string;
            /** Expression for ng-disabled of the input */
            disabled: string;
            /**
             * Positioning of the label of form-control.
             * Supported values are "horizontal" (default), "no-label", "plain" and "basic".
             * See FormGourpLabelType for details.
             */
            labelType: string;
            /**
             * Label columns expressed as Bootstrap grid columns. Supports multiple classes (e.g. "col-xs-4 col-sm-2").
             * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
             * Other label-types do not have column classes at the moment.
             */
            labelCols: string;
            /** Placeholder text */
            placeholder: string;
            /**
             * TooltipHtml adds an info button after the input which opens a uib-popover-html.
             */
            tooltipHtml: string;
            /** Options for ui.bootstrap.popover */
            tooltipPlacement: string;
            /**
             * Disables the default validation error message.
             * Use this if you want to create custom error messages in the UI.
             */
            disableValidationErrors: string;
        };
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Label for the control. If not set, it is derived from name. */
        label: string;
        /**
         * Positioning of the label of form-control.
         * Supported values are "horizontal" (default), "no-label", "plain" and "basic".
         * See FormGourpLabelType for details.
         */
        labelType: FormGroupLabelType;
        /** The model that is bound to the input */
        model: angular.INgModelController;
        /** The value of the input */
        value: T;
        /** Form control name */
        name: string;
        /** Validation error text */
        validationErrors: string[];
        /** Placeholder text */
        placeholder: string;
        /** If true, the input value is required */
        required: boolean;
        /** If true, readonly is set on the input with ng-readonly */
        readonly: boolean;
        /** If true, disabled is set on the input with ng-disabled */
        disabled: boolean;
        /** Options for ui.bootstrap.datepicker */
        tooltipPlacement: string;
        /**
         * Creates a new InputController
         * @ignore
         */
        constructor();
        /**
         * Set the values and defaults on init.
         * When overridding at derived classes, do your own stuff first and then call this method
         * because this method calls validate() at the end.
         */
        $onInit(): void;
        /** Returns the placeholder text: We will probably add stuff about validation here if not explicitly set. */
        protected getPlaceholder(): string;
        /** On change of the value, do preventive operations and set value to model. */
        protected onChange(): void;
        /** Do validations. Automatically called on onInit() and onChange() */
        protected validate(): boolean;
        private modifyValidationErrors;
    }
}
declare module "controllers/form-controls/DatepickerController" {
    import * as angular from "angular";
    /**
     * Controller for datepicker control.
     */
    export class DatepickerController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Label for the control */
        label: string;
        /** The model that is bound to the input */
        model: any;
        /** The value of the ui.bootstrap.datepicker */
        dtValue: Date;
        /** Gets or sets the value as ISO formatted date string. */
        value: string;
        /** Form control name */
        name: string;
        /** Validation error text */
        validationError: string;
        /** Options for ui.bootstrap.datepicker */
        datepickerOptions: any;
        /** If true, the input value is required */
        required: boolean;
        constructor();
        /** Set the values and defaults on init */
        $onInit(): void;
        /** On change of the value, do preventive operations and set value to model. */
        onChange(): void;
        /** Do validations. Automatically called on onInit() and onChange() */
        validate(): boolean;
    }
}
declare module "controllers/form-controls/DatepickerPopupController" {
    import { DatepickerController } from "controllers/form-controls/DatepickerController";
    /**
     * Controller for datepicker popup control.
     */
    export class DatepickerPopupController extends DatepickerController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Options for ui.bootstrap.datepicker */
        popupPlacement: string;
        /** Placeholder text to show in the text input. Default is 'p.k.vvvv' (TODO: Language versioning) */
        placeholder: string;
        /** Date format in text input. Default is d.M.yyyy */
        format: string;
        /**
         * If set to true, sets datepicker-append-to-body to false.
         * By default we set it to true in Salaxy framework,
         * but you may want to set it back to false in e.g. Modal dialogs.
         */
        appendInline: boolean;
        /** Popup control state. */
        popup: {
            opened: boolean;
        };
        constructor();
        /** Open popup. */
        open(): void;
    }
}
declare module "controllers/form-controls/DateRangeController" {
    import * as angular from "angular";
    import { DateRange } from "@salaxy/core";
    /**
     * Provides a user interface for picking up a date range
     * and optionally specifying also the number of working days within that range.
     */
    export class DateRangeController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Label for the control */
        label: string;
        /** Label for the scondary input. Translation is attempted. Default is SALAXY.NG1.DateRange.labelDaysCount.  */
        labelDaysCount: string;
        /** The model that is bound to the input */
        model: angular.INgModelController;
        /** The selection mode is either "range" (default) or "multiple" */
        mode: "range" | "multiple";
        /**
         * Minimum available date.
         * Bindable and ISO string version of the datepicker-options.minDate.
         */
        minDate: string;
        /**
         * Maximum available date.
         * Bindable and ISO string version of the datepicker-options.maxDate.
         */
        maxDate: string;
        /**
         * Fires an event when the model is changing: Any of the values specific to the model are changing: start, end or daysCount.
         * This should typically used instead of ng-change because ng-change will only fire if the object reference changes.
         * On-change fires when dates or day count changes and this is typically what you are looking for.
         * @example <salaxy-date-range ng-model="$ctrl.dateRange" on-change="$ctrl.dateRangeChange(value)"></salaxy-date-range>
         */
        onChange: (eventData: {
            /** The changed date range. */
            value: DateRange;
        }) => void;
        /**
         * Gets or sets the internal value of the complex object.
         */
        value: {
            /** Dately value for start date. */
            start?: string;
            /** Dately value for end date. */
            end?: string;
            /** Boolean indicating whether the end date has been explictly set: end date has been selected and is not the same as the start date. */
            isEndExplicit?: boolean;
            /** Number of days in the range. */
            daysCount?: number;
            /** Array of selected days in the range. */
            days?: string[];
            /** Latest selected date. */
            latest?: string;
            /** Formatted presentation of  range. */
            formattedRange?: string;
        };
        /** Form control name */
        name: string;
        /** If true, the input value is required */
        required: boolean;
        /**
         * Options for ui.bootstrap.datepicker
         * Note that if you use customClass, you need to handle the formatting of the selected range.
         * See getDayClass().
         * Also note that this value is not watched, it is read only once when the options are first needed-
         */
        datepickerOptions: any;
        private internalOptions;
        /** When only first selection is done in Range, we set the minimum value here. */
        private tempMinDateForRangeSelection;
        private _value;
        constructor();
        /** Set the values and defaults on init */
        $onInit(): void;
        /**
         * This is the latest selection in the Datepicker component.
         * When in mode=range: First it sets the value start, then end and then again start (and sets end to null).
         * When in mode=multiple: Sets the values on or off in the array.
         */
        latestDateSelection: string;
        /**
         * Sets the ng-model value from this.value;
         * Called by daysCount input directly and the datepicker through latestDateSelection.
         * Also called by value setter, but that probably not really used by any code at the moment.
         */
        setModelValue(): void;
        /**
         * Gets the options for datepicker.
         * Uses datepickerOptions, but adds customClass for range formatting.
         * Also converts minDate and maxDate to Date() object if necessary.
         */
        protected getOptions(): any;
        private getDayClass;
    }
}
declare module "controllers/form-controls/InputController" {
    import { InputBase } from "controllers/form-controls/_InputBase";
    /**
     * Base controller for form control groups (label, input, validation errors etc.).
     */
    export class InputController extends InputBase<string> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Minimum length, 0 is the default */
        minlength: number;
        /** Maximum length, 1024 is the default */
        maxlength: number;
        /** Regular expression pattern for validation */
        pattern: string;
        /** If true, sets the readonly attribute of the input with ng-readonly. */
        readonly: boolean;
        /**
         * If true, displays the control as read-only div instead of the input control.
         * This (read-only) is visualized in a different way than readonly (ng-readonly): The input is not shown and display is more compact.
         */
        readOnly: boolean;
        /**
         * Creates a new InputController
         * @ignore
         */
        constructor();
        /** Set the values and defaults on init */
        $onInit(): void;
        /** On change of the value, do preventive operations and set value to model. */
        onChange(): void;
        /** Do validations. Automatically called on onInit() and onChange() */
        validate(): boolean;
    }
}
declare module "controllers/form-controls/InputBooleanController" {
    import { InputBase } from "controllers/form-controls/_InputBase";
    /**
     * Controller behind boolean form controls (checkbox, radio, switch)
     */
    export class InputBooleanController extends InputBase<boolean> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Type of the input element. Default is checkbox. */
        type: "checkbox" | "radio" | "select" | "switch";
        /** Text to show as a label for input with value FALSE
         * Supported by types radio and dropdown
         */
        labelFalse: string;
        /** Text to show as a label for input with value TRUE
         * Supported by types radio, checkbox and dropdown
         */
        labelTrue: string;
        /**
         * TODO: Is this needed when there's support for 'empty-label' ?
         * BS class for offsetting the input (no-label)
         */
        offsetCols: string;
        /**
         * Creates a new InputController
         * @ignore
         */
        constructor();
        /** Toggles boolean value. */
        toggle(): void;
    }
}
declare module "controllers/form-controls/InputEnumController" {
    import { InputController } from "controllers/form-controls/InputController";
    /**
     * Controller behind form controls that select an occupation type.
     */
    export class InputEnumController extends InputController {
        /**
         * Binds to an enumeration defined by the Salaxy API.
         * Set the name of the enumeration.
         */
        enum: string;
        /** Type of the input element. Options are typeahead, select (default) and radio */
        type: "typeahead" | "select" | "radio";
        /** Options of the select control as a key-value object. */
        options: any;
        /**
         * Array or comma separated string to filter the option values to just the given ones.
         * Also sets the order to this order, so works for ordering a given set of values.
         * Note that hiddenOptions is applied first, so if you want e.g. "undefined" to appear if selected,
         * you may add it here and it behaves as expected (undefined is still hidden if a value is selevted).
         */
        filter: string[];
        /** If set to true, does not cache enums. */
        disableCache: boolean;
        /**
         * These values are visible only if they are selected in the data.
         *  Default is ["undefined"], set to empty array to show all in every state.
         * I.e. after something else is selected, hidden value cannot be selected back.
         * Use for not-selected values ("Please choose...") when you do not want selection reversed
         * or legacy data that is not selectable, but may still exist on the server.
         */
        hiddenOptions: string[];
        private enumCache;
        /** Gets the list of all enumerations */
        /** Sets the default values in init. */
        $onInit(): void;
        /**
         * Gets the label for an enumeration.
         * @param value Type of the enum.
         */
        getEnumerationLabel(value: string): string;
        /**
         * Returns items for current enumeration.
         */
        protected getEnumerations(): {
            /** Enum value. */
            value: any;
            /** Label for the value. */
            text: any;
            /** Description for the value. */
            title?: any;
        }[];
        private modifyOptions;
    }
}
declare module "controllers/form-controls/InputIncomeTypeController" {
    import { SessionService } from "services/index";
    import { InputController } from "controllers/form-controls/InputController";
    /**
     * Controller behind form controls that selects Income type code for transaction in Income Registry (Tulorekisteri).
     */
    export class InputIncomeTypeController extends InputController {
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Type of the input element. Default is typeahead. */
        type: "typehead" | "list";
        /**
         * Creates a new InputIncomeTypeController
         * @ignore
         */
        constructor(sessionService: SessionService);
        /** Sets the default values in init. */
        $onInit(): void;
        /** Gets the list of all income types. */
        getIncomeTypes(searchString: string): import("../../../../node_modules/@salaxy/core/logic/model/IncomeTypeMetadata").IncomeTypeMetadata[];
        /**
         * Returns the label for given occupation id.
         * @param occupationId - Id of the occupation.
         */
        getRowLabel(code: string): string;
        /** Returns the placeholder text - this control has a default text. */
        getPlaceholder(): string;
        /** Called by the view, when a value is selected. */
        protected selectionClicked(value: number): void;
    }
}
declare module "controllers/form-controls/InputNumberController" {
    import { CalculationRowUnit } from "@salaxy/core";
    import { InputBase } from "controllers/form-controls/_InputBase";
    /**
     * Controller behind form controls that select an occupation type.
     */
    export class InputNumberController extends InputBase<number> {
        /**
         * Unit for the number.
         * If set, shows a visual clue of the unit.
         * For 'percent' editor value is multiplied by 100.
         */
        unit: CalculationRowUnit;
        /** Sets the default values in init. */
        $onInit(): void;
        /**
         * Gets or sets the string value that is used in the view.
         * For percent, this is multiplied by 100.
         */
        viewValue: string;
        /**
         * Gets an indicator string (1-2 characters) for the unit.
         */
        getUnitIndicator(): "h" | "km" | "€" | "%" | "pv" | "kpl" | "vko" | "jakso";
    }
}
declare module "controllers/form-controls/InputCalcRowTypeController" {
    import { CalcRowConfig, CalculationRowCategory, CalculationRowType } from "@salaxy/core";
    import { SessionService } from "services/index";
    import { InputController } from "controllers/form-controls/InputController";
    /**
     * Controller behind form controls that select a calculation row type.
     */
    export class InputCalcRowTypeController extends InputController {
        private session;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Type of the input element. Default is typeahead. */
        type: "typehead" | "list";
        /** If set, filters the rows based on categories (plus rowTypes if set) */
        categories: CalculationRowCategory[];
        /** If set, shows only these types (plus categories if set) */
        rowTypes: CalculationRowType[];
        private listCache;
        /** */
        constructor(session: SessionService);
        /** Sets the default values in init. */
        $onInit(): void;
        /** Gets the list of row types. */
        getRowTypes(): CalcRowConfig[];
        /** Gets a label for given row type (string). */
        getRowLabel(rowTypeKey: string): string;
        /** Called by the view, when a value is selected. */
        protected selectionClicked(value: CalculationRowType): void;
    }
}
declare module "controllers/form-controls/InputOccupationTypeController" {
    import { Occupation } from "@salaxy/core";
    import { SessionService } from "services/index";
    import { InputController } from "controllers/form-controls/InputController";
    /**
     * Controller behind form controls that select an occupation type.
     */
    export class InputOccupationTypeController extends InputController {
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * One or a list of occupation IDs or a known keyword to define which occupations are shown
         * Supported keywords: 'household' and 'company'
         */
        defaultList: string;
        /**
         * Creates a new InputOccupationTypeController
         * @ignore
         */
        constructor(sessionService: SessionService);
        /** Gets the list of all occupations. */
        getOccupations(searchString: string): Occupation[];
        /**
         * Returns the label for given occupation id.
         * @param occupationId - Id of the occupation.
         */
        getOccupationLabel(occupationId: string): string;
        /** Returns the placeholder text - this control has a default text. */
        getPlaceholder(): string;
    }
}
declare module "controllers/form-controls/InputWorkerController" {
    import { Avatar } from "@salaxy/core";
    import { SessionService, TaxCardService, UiHelpers, WorkersService } from "services/index";
    import { InputController } from "controllers/form-controls/InputController";
    /**
     * Controller behind worker selection inputs
     */
    export class InputWorkerController extends InputController {
        private workersService;
        private taxCardService;
        private sessionService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, will add the option to add a new Worker directly from the selection. */
        addNew: boolean;
        constructor(workersService: WorkersService, taxCardService: TaxCardService, sessionService: SessionService, uiHelpers: UiHelpers);
        /** Gets the list of row types. */
        getWorkers(searchString: string): Avatar[];
        /** Gets the display name for the selected worker. */
        getWorkerName(avatarId: string): string;
        /** On change of the value, do preventive operations and set value to model. */
        onChange(): void;
        /**
         * Shows a dialog for adding a new worker.
         */
        showWorkerAddDialog(): void;
        private getCreateNewAvatar;
        private getAvatar;
    }
}
declare module "controllers/form-controls/FormGroupController" {
    import * as angular from "angular";
    import { FormGroupLabelType } from "controllers/form-controls/FormGroupLabelType";
    /**
     * Helper for rendering the HTML for FormGroup:
     * This component renders only the label - input html.
     * It does not do any of the real form-control logic like ng-model, validations etc.
     * The "input" part of the form group may be a non form control - e.g. just a text.
     * Also the label may be hidden.
     */
    export class FormGroupController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Label for the control */
        label: string;
        /** Form control name */
        name: string;
        /** If true, shows the asterix in the label. */
        required: boolean;
        /**
         * Positioning of the label of form-control.
         * Supported values are "horizontal" (default), "no-label", "plain", "basic" and "empty-label".
         * See FormGourpLabelType for details.
         */
        labelType: FormGroupLabelType;
        /**
         * Label columns expressed as Bootstrap grid columns.
         * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
         * Other label-types do not have column classes at the moment.
         */
        labelCols: string;
        constructor();
        /**
         * Checks the existence of name property.
         */
        $onInit(): void;
        /**
         * Gets the class (or classes space separated) for label or value element.
         */
        getLabelCols(forElement: "label" | "value" | "no-label"): string;
        /** Gets the label type with "horizontal" as default / unknown value. */
        protected getLabelType(): "basic" | "horizontal" | "no-label" | "plain" | "empty-label";
    }
}
declare module "controllers/form-controls/TabsController" {
    import * as angular from "angular";
    import { TabController } from "controllers/form-controls/TabController";
    /**
     * Controller for the tabs control.
     */
    export class TabsController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Tabs array. Contains all registered tabs. */
        tabs: TabController[];
        /**
         * Creates a new TabsController.
         * @ignore
         */
        constructor();
        /** Set the values and defaults on init */
        $onInit(): void;
        /**
         * Registers a single tab to this controller.
         * @param tab - Tab to register.
         */
        register(tab: TabController): any;
        /**
         * De-registers a single tab from this controller.
         * @param tab - Tab to de-register.
         */
        deregister(tab: TabController): void;
        /**
         * Sets the selected tab as selected.
         * @param selectedTab - Selected tab.
         */
        select(selectedTab: TabController, evt?: angular.IAngularEvent): void;
        /** Get the index of the active tab. */
        /** Set the index of the active tab. */
        active: any;
        private findIndex;
        private setSortOrder;
    }
}
declare module "controllers/form-controls/TabController" {
    import * as angular from "angular";
    import { TabsController } from "controllers/form-controls/TabsController";
    /**
     * Controller for a single tab pane in the tabs control.
     */
    export class TabController implements angular.IController {
        private $element;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Tabs controller */
        tabsCtrl: TabsController;
        /** Selected tab flag. */
        selected: boolean;
        /** Flag to disable tab. */
        disable: boolean;
        /** Unique name/id for the tab. */
        index: any;
        /** Text heading for the tab */
        heading: string;
        /** Selection event handler */
        onSelect: (args: {
            /** Selection event. */
            $event: angular.IAngularEvent;
        }) => void;
        /** Heading DOM node for the tab. */
        headingElement: any;
        /** Content DOM node for the tab. */
        contentElement: any;
        /** Sorting field */
        sort: number;
        /**
         * Creates a new TabController.
         * @ignore
         */
        constructor($element: any);
        /**
         * Change event handler. Re-sets the one-way bound fields.
         * This is needed, because the parent scope cannot detect the changes without the re-set.
         */
        $onChanges(changes: any): void;
        /**
         * Registers this tab to the tabs controller.
         * Passes the heading DOM element to be displayed in the header.
         */
        $onInit(): void;
        /**
         * De-registers this tab from the tabs controller.
         */
        $onDestroy(): void;
        /**
         * Tries to find the element with ng-transclude='heading' attribute.
         * @param el - Element to check.
         */
        private findTranscludeElement;
        private getPosition;
        private findParentTag;
        private findChildren;
    }
}
declare module "controllers/form-controls/ValidationSummaryController" {
    import * as angular from "angular";
    import { ApiValidation, ApiValidationError, ApiValidationErrorType } from "@salaxy/core";
    import { Ng1Translations, UiHelpers } from "services/index";
    /**
     * Handles user interaction for viewing and modifying the current account data
     * including the products that are enabled for the current account and their properties.
     */
    export class ValidationSummaryController implements angular.IController {
        private translate;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** The server-side API-validation object that is displayed by this controller. */
        apiValidation: ApiValidation;
        /** HTML that should be shown when the bound validation data is null */
        loadingHtml: "Ladataan validointi-tietoa...";
        constructor(translate: Ng1Translations, uiHelpers: UiHelpers);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** If false, there is no validation object bound to the component. */
        readonly hasValidation: boolean;
        /** If true, the validation has no errors. */
        readonly isValid: boolean;
        /** If false, has empty required fields. These are shown separately from other errors. */
        readonly hasAllRequiredFields: boolean;
        /** If true, has other errors than missing required fiels. */
        readonly hasValidationOrGeneralErrors: boolean;
        /** Gets the errors, optionally filtered by a type.
         * @param type Type of the error: "general", "required", "invalid" or null.
         * If null, all errors are returned.
         * Also supports negation: "!general", "!required" or "!invalid".
         */
        getErrors(type?: ApiValidationErrorType | string): ApiValidationError[];
        /**
         * Returns the label for the validation result.
         * The label is translated using the key: `SALAXY.VALIDATION.[error.code].label`
         * If the translation does not exist the `error.msg` is returned.
         * @param error - Error in validation.
         */
        getLabel(error: ApiValidationError): any;
        /**
         * Returns the description for the validation result.
         * The label is translated using the key: `SALAXY.VALIDATION.[error.code].description`
         * If the translation does not exist the `null` is returned.
         * @param error - Error in validation.
         */
        getDescription(error: ApiValidationError): boolean;
        /**
         * Returns true if the translation for the key `SALAXY.VALIDATION.[error.code].description` exists.
         * @param error - Error in validation.
         */
        hasDescription(error: ApiValidationError): boolean;
        /** Shows details of the errors. */
        showDetails(): void;
        private getValidation;
    }
}
declare module "controllers/form-controls/index" {
    export * from "controllers/form-controls/_InputBase";
    export * from "controllers/form-controls/DatepickerController";
    export * from "controllers/form-controls/DatepickerPopupController";
    export * from "controllers/form-controls/DateRangeController";
    export * from "controllers/form-controls/InputController";
    export * from "controllers/form-controls/InputBooleanController";
    export * from "controllers/form-controls/InputEnumController";
    export * from "controllers/form-controls/InputIncomeTypeController";
    export * from "controllers/form-controls/InputNumberController";
    export * from "controllers/form-controls/InputCalcRowTypeController";
    export * from "controllers/form-controls/InputOccupationTypeController";
    export * from "controllers/form-controls/InputWorkerController";
    export * from "controllers/form-controls/FormGroupController";
    export * from "controllers/form-controls/FormGroupLabelType";
    export * from "controllers/form-controls/TabController";
    export * from "controllers/form-controls/TabsController";
    export * from "controllers/form-controls/ValidationSummaryController";
}
declare module "controllers/modals/EditDialogController" {
    /**
     * Controller for Item edit dialogs.
     */
    export class EditDialogController {
        editDialogParams: any;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * The item that is being edited.
         * This a copy of the original item to allow cancel / reset and other cahnge tracking.
         */
        current: any;
        /** Additional logic: Helper functions, metadata etc. that view can use to contruct the UI. */
        logic: any;
        constructor(editDialogParams: any);
    }
}
declare module "controllers/modals/ModalGenericDialogController" {
    /**
     * Controller for alert, confirm and loading dialogs.
     */
    export class ModalGenericDialogController {
        data: any;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(data: any);
    }
}
declare module "controllers/modals/ModalDocumentPreviewController" {
    /**
     * Represents a modal window (instance) dependency and is used by UI Bootstrap modal.
     * All modal instances that uses ModalInstanceController need to resolve 'data'.
     */
    export class ModalDocumentPreviewController implements angular.IController {
        private $sce;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private contentCache0;
        private contentCache1;
        /**
         * Creates a new ModalInstance
         * @param reportData
         * @ignore
         */
        constructor(reportData: any, $sce: angular.ISCEService);
        /** Returns data for the report. */
        protected readonly data: string;
    }
}
declare module "controllers/modals/index" {
    export * from "controllers/modals/EditDialogController";
    export * from "controllers/modals/ModalGenericDialogController";
    export * from "controllers/modals/ModalDocumentPreviewController";
}
declare module "controllers/worker/AbsencePeriodsController" {
    import { AbsencePeriod, WorkerAbsences } from "@salaxy/core";
    import { AbsencesService, UiHelpers } from "services/index";
    import { ListControllerBase } from "controllers/bases/index";
    /**
     * Controls the absences list (poissaolokirjanpito) for a selected holiday year.
     */
    export class AbsencePeriodsController extends ListControllerBase<WorkerAbsences, AbsencePeriod> {
        private absencesService;
        /** Bindings for components that use this controller */
        static bindings: {
            /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
            condensed: string;
            /** Date filter start value. Will be compared to period end date. */
            filterStart: string;
            /** Date filter end value. Will be compared to period start date. */
            filterEnd: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
        condensed: boolean;
        /** Date filter start value. Will be applied to period start dates. */
        filterStart: string;
        /** Date filter end value. Will be applied to period end dates. */
        filterEnd: string;
        constructor(absencesService: AbsencesService, uiHelpers: UiHelpers);
        /** List of items */
        readonly list: AbsencePeriod[];
        /**
         * Filter that is applied to the list
         * @param value Item in the list
         * @param index Index of the item
         * @param array The entire array
         */
        filter: (value: AbsencePeriod) => boolean;
        /** Creating of a new item. */
        getBlank(): AbsencePeriod;
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
        /** Gets the needed logic for Edit dialog */
        getEditDialogLogic(): {
            /**
             * Returns true if the select component should be shown for absence isPaid.
             */
            canSelectIsPaid: (current: AbsencePeriod) => boolean;
            /** Updates the IsPaid according to absence type. */
            updateIsPaid: (current: AbsencePeriod) => void;
            updateIsHolidayAccrual: (current: AbsencePeriod) => void;
            /** Updates the workdays count */
            updatePeriodDays: (current: AbsencePeriod) => void;
            /** Sets the days array in period to enable days selection UI. */
            setDaysSelection: (period: AbsencePeriod) => void;
        };
        /** Gets a total days calculation for different types. */
        getTotalDays(type?: "all" | "absencesPaid" | "absencesUnpaid" | "absencesHolidayAccrual" | "absencesNoHolidayAccrual"): number;
    }
}
declare module "controllers/worker/HolidayYearAccrualController" {
    import { HolidayAccrualEntry, HolidayYear } from "@salaxy/core";
    import { HolidayYearsService, UiHelpers } from "services/index";
    import { ListControllerBase, ListControllerBaseBindings } from "controllers/bases/index";
    /**
     * Controls holiday accrual (lomapäivien kertymä) of the annual leave for a selected holiday period.
     */
    export class HolidayYearAccrualController extends ListControllerBase<HolidayYear, HolidayAccrualEntry> {
        private holidayYearsService;
        /** Bindings for components that use this controller */
        static bindings: ListControllerBaseBindings;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(holidayYearsService: HolidayYearsService, uiHelpers: UiHelpers);
        /** List of items */
        readonly list: HolidayAccrualEntry[];
        /** Creating of a new item. */
        getBlank(): HolidayAccrualEntry;
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
        /** Chart for eccrual overview. */
        getChart(list: HolidayAccrualEntry[]): {
            labels: any[];
            series: string[];
            data: any[][];
            options: {
                scales: {
                    yAxes: {
                        ticks: {
                            beginAtZero: boolean;
                        };
                    }[];
                };
            };
        };
        /** Gets the color for the row avatar */
        getRowColor(row: HolidayAccrualEntry | "start" | "total"): string;
        /** Gets this month: The first date of this month */
        getThisMonth(): string;
        /**
         * Returns the notes if available or, if not, description text based on the source.
         */
        getAccrualNotes(row: HolidayAccrualEntry): string;
        /** Gets calculations related to holiday accruals */
        getAccrualCalculations(): {
            accrual: number;
            accrualToday: number;
            total: number;
            totalToday: number;
        };
    }
}
declare module "controllers/worker/HolidayYearHolidaysController" {
    import { AnnualLeave, HolidayYear } from "@salaxy/core";
    import { HolidayYearsService, UiHelpers } from "services/index";
    import { ListControllerBase } from "controllers/bases/index";
    /**
     * Controls the planned holidays list (lomakirjanpito / lomakalenteri) for a selected holiday year.
     */
    export class HolidayYearHolidaysController extends ListControllerBase<HolidayYear, AnnualLeave> {
        /** Bindings for components that use this controller */
        static bindings: {
            /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
            condensed: string;
            /** Date filter start value. Will be compared to period end date. */
            filterStart: string;
            /** Date filter end value. Will be compared to period start date. */
            filterEnd: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
        condensed: boolean;
        /** Date filter start value. Will be applied to period start dates. */
        filterStart: string;
        /** Date filter end value. Will be applied to period end dates. */
        filterEnd: string;
        constructor(holidayYearsService: HolidayYearsService, uiHelpers: UiHelpers);
        /** List of items */
        readonly list: AnnualLeave[];
        /**
         * Filter that is applied to the list when displayed.
         * This is done in view: Does not apply to list property, but does affect getTotalDays().
         * @param value Item in the list
         * @param index Index of the item
         * @param array The entire array
         */
        filter: (value: AnnualLeave) => boolean;
        /** Creating of a new item. */
        getBlank(): AnnualLeave;
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
        /** Logic for edit dialog. */
        getEditDialogLogic(): {
            /** Updates the workdays count */
            updatePeriodDays: (current: AnnualLeave) => void;
            /** Sets the days array in period to enable days selection UI. */
            setDaysSelection: (period: AnnualLeave) => void;
        };
        /** Gets the description text for the planned holiday. */
        getDescription(row: AnnualLeave): string;
        /** Gets a total days calculation for different types. */
        getTotalDays(type?: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart" | "filtered"): number;
    }
}
declare module "controllers/worker/HolidaysModel" {
    /** TODO: Future interface for listing the hourly salaries for annual leave payments. */
    export interface HourlySalaryEntry {
        /** TODO. */
        todo?: string;
    }
}
declare module "controllers/worker/HolidayYearHourlySalariesController" {
    import { HolidayYear } from "@salaxy/core";
    import { HolidayYearsService, UiHelpers } from "services/index";
    import { ListControllerBase, ListControllerBaseBindings } from "controllers/bases/index";
    import { HourlySalaryEntry } from "controllers/worker/HolidaysModel";
    /**
     * Controls the hourly entries that are the bases for annual leave payments.
     * Based on this, system makes a suggestion for daily mean salary that is the bases for HolidaySalary.
     */
    export class HolidayYearHourlySalariesController extends ListControllerBase<HolidayYear, HourlySalaryEntry> {
        private holidayYearsService;
        /** Bindings for components that use this controller */
        static bindings: ListControllerBaseBindings;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(holidayYearsService: HolidayYearsService, uiHelpers: UiHelpers);
        /** List of items */
        readonly list: HourlySalaryEntry[];
        /** Creating of a new item. */
        getBlank(): HourlySalaryEntry;
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
    }
}
declare module "controllers/worker/HolidayYearPaidController" {
    import { AnnualLeavePayment, Avatar, Calculation, HolidayYear } from "@salaxy/core";
    import { CalculationsService, HolidayYearsService, UiHelpers } from "services/index";
    import { ListControllerBase, ListControllerBaseBindings } from "controllers/bases/index";
    /**
     * Controls the annual leave payments: HolidayCompensation, HolidayBonus and HolidaySalary.
     * These payments are typically fetched from paid calculations automatically,
     * but may also be marked paid manually. Also, in client-side logic, payments are fetched
     * optionally from Draft calculations.
     */
    export class HolidayYearPaidController extends ListControllerBase<HolidayYear, AnnualLeavePayment> {
        private holidayYearsService;
        private calculationsService;
        /** Bindings for components that use this controller */
        static bindings: ListControllerBaseBindings;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Total calculations */
        totals: {
            /** Total calculation  */
            total: AnnualLeavePayment;
            /** Start saldo of the payments */
            startSaldo: AnnualLeavePayment;
            /** End saldo of the payments. */
            endSaldo: AnnualLeavePayment;
        };
        private _list;
        private _listCacheKey;
        constructor(holidayYearsService: HolidayYearsService, uiHelpers: UiHelpers, calculationsService: CalculationsService);
        /** Gets the worker ID for the current holiday year. */
        readonly workerId: string;
        /** Refreshes the cache */
        refresh(): void;
        /**
         * List of items fetched from the calculations,
         * cached to the controller level, except when workerId changes.
         */
        readonly list: AnnualLeavePayment[];
        /** Gets an avatar for a table row. */
        getAvatar(type: "row" | "startSaldo" | "total" | "endSaldo", row: AnnualLeavePayment): Avatar;
        /**
         * Gets the payments fromcalculations with given filters.
         * This method has no caching.
         */
        getPaymentsFromCalculations(calcs: Calculation[]): AnnualLeavePayment[];
        /** Creating of a new item. */
        getBlank(): any;
        /** Logic for edit dialog. */
        getEditDialogLogic(): {
            /** Updates the workdays count */
            updatePeriodDays: (current: AnnualLeavePayment) => void;
        };
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
        private getCurrentCacheKey;
    }
}
declare module "controllers/worker/HolidayYearsController" {
    import { HolidaySpecification, HolidayYear } from "@salaxy/core";
    import { CalculationsService, HolidayYearsService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Shows information about the holidays of a Worker.
     * Also allows editing the holidays.
     */
    export class HolidayYearsController extends CrudControllerBase<HolidayYear> {
        private holidayYearsService;
        private workersService;
        private calculationsService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * View type for the worker-info component.
         *
         * - details is a large view with full information
         * - overview shows only basic info - this is the one shown as overview in the calculator.
         */
        viewType: "overview" | "accrual" | "holidays" | "absenses";
        constructor(holidayYearsService: HolidayYearsService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, workersService: WorkersService, calculationsService: CalculationsService);
        /** Gets or sets the current holiday year filtering it to only years of the currently selected Worker. */
        selectedYear: HolidayYear;
        /** Gets the current worker */
        readonly currentWorker: import("../../../../node_modules/@salaxy/core/model/v01").WorkerAccount;
        /** Gets all the holiday years for the current worker */
        readonly currentWorkerYears: HolidayYear[];
        /** Gets the current holiday specification for the worker */
        readonly spec: HolidaySpecification;
        /** If true, the holidays is initialized / specified for the current worker. */
        readonly isInitialized: boolean;
        /** Add new holiday year */
        addNewYear(): void;
        /** Saves the currently selected holiday year. */
        save(): void;
        /** Initializes the holidays for the current Worker */
        initHolidays(spec: HolidaySpecification): void;
        /** Shows the Init holidays dialog for initializing the holiday years for the given worker */
        showInitHolidays(): void;
        /** Gets the visualisation data for the selected holiday year. */
        getYearVisualisation(): any;
        /** Gets a total days calculation for different types. */
        getTotalDays(type?: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart"): number;
        /**
         * Gets items visibility based on properties of the selected holiday year.
         * @param elements Logical name of group of items in teh view.
         */
        getVisibility(elements: "accrual" | "compensation" | "hourly"): boolean;
    }
}
declare module "controllers/worker/WorkerAbsencesController" {
    import { WorkerAbsences } from "@salaxy/core";
    import { AbsencesService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Allows viewing and editing of Absences.
     */
    export class WorkerAbsencesController extends CrudControllerBase<WorkerAbsences> {
        private absencesService;
        private workersService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** UI view that is shown */
        viewType: "overview" | "list";
        constructor(absencesService: AbsencesService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, workersService: WorkersService);
        /**
         * Gets the absences for the current worker in WorkersService.
         * Getting this property will also set the current item accordingly.
         */
        readonly currentWorkerAbsences: WorkerAbsences;
        /**
         * Shows the edit dialog.
         * @param item Either pass the index of the selected item or date of the new item as string.
         * Pass keyword "new" for new item with default date (currently today).
         */
        showEditDialog(selectedItem: number | "new" | string): void;
        /** Calendar is clicked */
        calendarClick(type: "period" | "day" | "emptyDay", date: string, selectedItem: number): void;
        /** Saves the absences with a loader dialog. */
        save(): void;
    }
}
declare module "controllers/worker/WorkerCalcRowsController" {
    import { CalcRowConfig, CalculationRowType, UserDefinedRow, WorkerAccount } from "@salaxy/core";
    import { UiHelpers, WorkersService } from "services/index";
    import { ListControllerBase } from "controllers/bases/index";
    /**
     * TODO
     */
    export class WorkerCalcRowsController extends ListControllerBase<WorkerAccount, UserDefinedRow> {
        private $timeout;
        /** Bindings for components that use this controller */
        static bindings: {
            /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
            condensed: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, will format the table with class table-condensed. Later may add some other condensed formatting. */
        condensed: boolean;
        /** Type of the new row that is added. */
        newRowType: CalculationRowType;
        /**
         * Creates a new WorkerCalcRowsController
         * @param workersService - TODO WorkersService,
         * @param uiHelpers - Salaxy UI helpers service.
         * @ignore
         */
        constructor(workersService: WorkersService, uiHelpers: UiHelpers, $timeout: angular.ITimeoutService);
        /** List of rows  */
        readonly list: UserDefinedRow[];
        /** Creating of a new item. */
        getBlank(): UserDefinedRow;
        /**
         * Returns a validation message for a row or null if none is required.
         * @param row: The row that is validated.
         */
        getValidation(row: UserDefinedRow): {
            /** Validation message */
            msg: string;
            /** Type of validation message */
            type: "default" | "error";
        };
        /** Template for edit UI that is shown in a modal dialog. */
        getEditDialogTemplateUrl(): string;
        /**
         * Gets the UX configuration for a row.
         * @param row Row for which the config is fetched.
         * If not set, gets the config for current.
         */
        getConfig(row: UserDefinedRow): CalcRowConfig;
        /** Gets the placeholder text for an input
         * TODO check calcrowscontoller
         */
        getPlaceholderText(row: UserDefinedRow, field: "amount" | "price"): string;
        /**
         * Return the total for the row
         */
        getRowTotal(row: UserDefinedRow): number;
        /**
         * Returns true, if the row is disabled. The row is interpreted as disabled if it has no rowType
         * or it has been set as hidden in the configuration.
         * @param row Row to check
         * @param field Type of the input / field.
         */
        isDisabled(row: UserDefinedRow, field: "amount" | "price"): boolean;
        /** Row type changes on a row */
        rowTypeChanged(row: UserDefinedRow): void;
        /** Gets a new row for the type specified in newRowType */
        newRowForType(): UserDefinedRow;
        /** Shows the selection dialog for the row type. */
        showRowTypeSelectionDialog(): void;
        /** Gets the needed logic for Edit dialog */
        getEditDialogLogic(): {
            getConfig: (current: UserDefinedRow) => CalcRowConfig;
            isDisabled: (current: UserDefinedRow, field: "amount" | "price") => boolean;
            getRowTotal: (current: UserDefinedRow) => number;
        };
        /** Returns true if the filtered list has any rows. */
        readonly hasRows: boolean;
        /** Gets the total for the current rows. */
        getTotal(): number;
        private getCalc;
    }
}
declare module "controllers/worker/WorkerInfoController" {
    import { Avatar, Calculation, CalcWorker, Contact, TaxcardKind, WorkerAccount } from "@salaxy/core";
    import { TaxCardService, WorkersService } from "services/index";
    /**
     * Shows information about a Worker either based on a WorkerAccount (from WorkersService)
     * or Calculation (and CalcWorker within that calculation).
     */
    export class WorkerInfoController implements angular.IController {
        private workersService;
        private taxCardService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * View type for the worker-info component.
         *
         * - details is a large view with full information
         * - overview shows only basic info - this is the one shown as overview in the calculator.
         */
        viewType: "details" | "overview";
        /**
         * Text (translated) that is shown if now worker is selected.
         * Default is "SALAXY.NG1.WorkerInfo.noSelection".
         */
        noSelectionText: string;
        /**
         * Angular ng-model **if** the controller is bound to model using ng-model attribute.
         */
        model: angular.INgModelController;
        constructor(workersService: WorkersService, taxCardService: TaxCardService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Returns true if there is a Worker account bound / selected.
         * This may be either Calculation or WorkerAccount
         */
        readonly hasValue: CalcWorker | WorkerAccount;
        /** Gets the Calculation IF the controller is bount to a Calculation (not WorkerAccount). */
        readonly calc: Calculation;
        /** Gets the CalcWorker object if the ng-model is bound to to a Calculation (not WorkerAccount). */
        readonly calcWorker: CalcWorker;
        /**
         * Gets the WorkerAccount object if the ng-model is bound to a WorkerAccount.
         */
        readonly workerAccount: WorkerAccount;
        /**
         * Gets the tax card info text.
         * @param short If true, provides short text (currently no type info included - just OK/Not + percent).
         */
        getTaxCardInfo(short: boolean): {
            kind: TaxcardKind;
            percent: number;
            text: string;
        };
        /** Gets the text for pension. */
        getPensionText(): "Työeläkesopimus: TyEL, osaomistaja" | "Työeläkesopimus: YEL" | "Työeläkesopimus: MyEL" | "Työeläkesopimus: Urheilijan eläke" | "Työeläkesopimus: Ei (työkorvaus)" | "Työeläkesopimus: Ei (hallituspalkkio)" | "Työeläkesopimus: TyEL";
        /**
         * Returns the contact info for the worker.
         * You should only expect email and telephone in this contact (often no address).
         */
        readonly contact: Contact;
        /** Get the avatar for the account. */
        readonly avatar: Avatar;
        /** Returns the bank account IBAN number if available. */
        readonly ibanNumber: string;
        /** Returns the personal ID. */
        readonly personalId: string;
        /** Returns the occupation text translated or "Ei työn tyyppiä!". */
        readonly occupation: string;
    }
}
declare module "controllers/worker/WorkersController" {
    import * as angular from "angular";
    import { Ajax, EmploymentRelationType, Taxcard, UserDefinedRow, WorkerAccount } from "@salaxy/core";
    import { CalcRowsService, CalculationsService, SessionService, TaxCardService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user interaction for listing the workers for the current user
     * selecting one of those workers as a current worker to edit etc.
     * and performing operations on that worker.
     */
    export class WorkersController extends CrudControllerBase<WorkerAccount> {
        private workersService;
        private calcRowsService;
        private calculationsService;
        private taxCardService;
        private ajax;
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Data binding field for submitAssureWorkerAccount() method  */
        assureWorkerAccountReq: {
            officialId: any;
            firstName: any;
            lastName: any;
            email: any;
            telephone: any;
            bankAccountIban: any;
            startDate: any;
            endDate: any;
        };
        /** Initial tab to open in launch. */
        initialTab: any;
        /** Pension insurance type options for binding. */
        private pensionCalculationOptions;
        private _currentTab;
        /**
         * Creates a new WorkersController
         * @param workersService - Worker service.
         * @param $location - Angular $location service.
         * @param $attrs - Angular @attrs service.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param calcRowsService - CalcRows Service
         * @param calculationsService - Calculations service.
         * @param taxCardService - Tax card service.
         * @param ajax - Salaxy ajax service.
         * @param sessionServicee - Session service.
         * @ignore
         */
        constructor(workersService: WorkersService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, calcRowsService: CalcRowsService, calculationsService: CalculationsService, taxCardService: TaxCardService, ajax: Ajax, sessionService: SessionService);
        /** Controller initialization */
        $onInit(): void;
        /**
         * Saves changes (submit) to the current worker
         */
        saveCurrent(callback?: (workerAccount: WorkerAccount) => void): Promise<WorkerAccount>;
        /**
         * Shows the Edit dialog for calculation row.
         * @param row Row to edit in the dialog.
         */
        showEdit(row: UserDefinedRow): void;
        /**
         * Gets a description text for a WorkerAccount.
         * @param worker Worker account to describe.
         */
        getDescription(worker: WorkerAccount): any;
        /**
         * For the tab UI control, returns the current tab, if defined in the path.
         * Defaults to "default".
         */
        currentTab: "default" | "calculations" | "taxcards" | "employment" | "contracts";
        /**
         * Gets the calculations count for the specified worker.
         * @param workerId - The worker ID to filter for - default is the current worker.
         */
        getCalcCount(workerId: string): number;
        /**
         * Gets the current tax card for a worker if one is added in the TaxCards collection.
         */
        getCurrentTaxCard(): Taxcard;
        /**
         * Gets the best tax card for a worker.
         */
        getBestTaxCard(worker: WorkerAccount): Taxcard;
        /** Adds a new calculation row for the current worker. */
        addSalaryDefaultsRow(): UserDefinedRow;
        /**
         * Deletes a calculation row.
         * @param {number} rowIndex - Zero based row index of the row that should be deleted.
         */
        deleteSalaryDefaultsRow(rowIndex: number): void;
        /**
         * Checks if worked can be deleted. A worker can be deleted if they have any calculations that are in other states (paymentStarted, paymentCancelled, etc)
         * than draft
         */
        canWorkerBeDeleted(): boolean;
        /**
         * Calls the AssureWorkerAccount method in the API with the data defined in assureWorkerAccountReq
         * @param callback - callback for using eg. resetting the form.
         */
        submitAssureWorkerAccount(callback?: () => void): Promise<WorkerAccount>;
        /**
         * Resets assure worker account model.
         */
        resetAssureWorkerAccount(): void;
        /**
         * Checks whether the user is in a given role
         * @param  role - One of the known roles
         */
        isInRole(role: any): boolean;
        /**
         * Returns the url where to post the current tax card
         */
        getCurrentTaxCardUploadUrl(): string;
        /**
         * Returns the url where to download the current tax card
         */
        getCurrentTaxCardDownloadUrl(): string;
        /**
         * Opens wizard to create a new Worker account.
         */
        launchWorkerWizard(workerId?: string): Promise<any>;
        /** Sets the proposed employment values for the worker */
        setProposedValues(): void;
        /** Returns available pension insurance type options for employment relation type. */
        getPensionCalculationOptions(employmentRelationType: EmploymentRelationType): any[];
        /**
         *   - `function(value, index, array)`: A predicate function can be used to write arbitrary filters.
         *     The function is called for each element of the array, with the element, its index, and
         *     the entire array itself as arguments.
         */
        listFilter: (item: WorkerAccount, index: any, array: WorkerAccount[]) => boolean;
        /** Gets an empty assureWorkerAccountReq - used for reset. */
        private getEmptyAssureWorkerAccountReq;
        private modifyOptions;
    }
}
declare module "controllers/worker/index" {
    export * from "controllers/worker/AbsencePeriodsController";
    export * from "controllers/worker/HolidayYearAccrualController";
    export * from "controllers/worker/HolidayYearHolidaysController";
    export * from "controllers/worker/HolidayYearHourlySalariesController";
    export * from "controllers/worker/HolidayYearPaidController";
    export * from "controllers/worker/HolidayYearsController";
    export * from "controllers/worker/WorkerAbsencesController";
    export * from "controllers/worker/WorkerCalcRowsController";
    export * from "controllers/worker/WorkerInfoController";
    export * from "controllers/worker/WorkersController";
    export * from "controllers/worker/WorkerWizardController";
}
declare module "controllers/year-end/AddCorrectionDialogController" {
    import { CalculationSummary, Calculator, WorkerSummary, YearEndFeedbackCalculationType } from "@salaxy/core";
    import { YearEndService } from "services/index";
    /**
     * Controller for Add Correction dialog in Yearend checks and reports.
     */
    export class AddCorrectionDialogController implements angular.IController {
        private yearendService;
        dialogConfig: any;
        private calcApi;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If true, the item is a new item to be inserted (otherwise edit/delete) */
        isNew: boolean;
        /** The worker summry object to which the row is added (from which it is removed) */
        worker: WorkerSummary;
        /** The calculation summary that is being added / edited */
        current: CalculationSummary;
        /**
         * Creates a new DialogController
         * @param yearendService - Service that handles the communication to the server (for year end data)
         * @param dialogConfig - Configuration from the calling Controller
         * @param calcApi - Calculations API for calculating the side costs.
         * @ignore
         */
        constructor(yearendService: YearEndService, dialogConfig: any, calcApi: Calculator);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Type of row to be added / edited */
        readonly type: YearEndFeedbackCalculationType;
        /** Dialog title */
        readonly title: "Lisää muualla maksettu palkka" | "Lisää palkkalaskelman korjaus" | "Muokkaa muualla maksettua palkkaa" | "Muokkaa palkkalaskelman korjausta";
        /** Saves the changes to the current item. */
        submit(): void;
        /** Deletes the current item */
        delete(): void;
        /** Calculates the side costs based on the salary */
        calculate(): void;
        private roundTo2Decimal;
    }
}
declare module "controllers/year-end/YearEndControllerBase" {
    import { YearEndUserFeedback, YearlyFeedback } from "@salaxy/core";
    import { AccountService, ReportsService, UiHelpers, YearEndService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles user interaction for viewing and modifying the current yearly feedback data.
     */
    export class YearEndControllerBase extends CrudControllerBase<YearlyFeedback> {
        protected yearEndService: YearEndService;
        protected $location: angular.ILocationService;
        protected $attrs: angular.IAttributes;
        protected uiHelpers: UiHelpers;
        protected accountService: AccountService;
        protected reportsService: ReportsService;
        protected $scope: angular.IScope;
        protected $uibModal: angular.ui.bootstrap.IModalService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * If set to true, shows the submit buttons even if the feedback for the given year has already been sent
         * At the time of writing this is used only in Admin and Protype UI's.
         */
        alwaysShowSubmit: boolean;
        /**
         * Creates a new YearlyFeedbackController
         * @param yearendService - Service that handles the communication to the server for
         * @param $location - Angular $location service.
         * @param $attrs - Angular @attrs service.
         * @param uiHelpers - Salaxy ui helpers service.
         * @param accountService - Service that provides information about the current account.
         * @param reportsService - Service for fetching reports and building report links.
         * @param $scope - Angular scope.
         * @param $uibModal - Bootstrap modal service.
         * @ignore
         */
        constructor(yearEndService: YearEndService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, accountService: AccountService, reportsService: ReportsService, $scope: angular.IScope, $uibModal: angular.ui.bootstrap.IModalService);
        /** Returns true if the feedback has been sent */
        readonly hasBeenSent: boolean;
        /** Show the submit buttons if not sent or alwaysShowSubmit is true. */
        readonly showSubmit: boolean;
        /**
         * Gets a URL for a calculation report
         * @param {calcReportType} reportType - Type of report
         * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
         */
        getCalcReport(reportType: salaxy.api.calcReportType, calcId: string): string;
        /**
         * Gets a link URL for a yearly report. This is a full link with token and absolute URL.
         * @param type - Type of the report must be one of the yearly reports
         * @param year - Year for the report
         * @param id - Worker ID for those reports that are specific to one Worker.
         * @param id2 - Second Worker ID for those reports that have two Workers in one report
         */
        getYearlyReport(type: salaxy.model.ReportType, year: number, id?: string, id2?: string): string;
        /**
         * Gets the default configuration for submit modal dialog
         * @param userStatus User status for the feedback
         */
        protected getModalConfig(userStatus: YearEndUserFeedback): angular.ui.bootstrap.IModalSettings;
        /**
         * Shows the submit modal dialog
         * @param config Modal configuration
         */
        protected showModal(config: any): Promise<any>;
    }
}
declare module "controllers/year-end/YearEndCompanyController" {
    import { CalculationSummary, WorkerSummary, YearEndUserFeedback } from "@salaxy/core";
    import { YearEndControllerBase } from "controllers/year-end/YearEndControllerBase";
    /**
     * Implements the YearendControllerBase for Company accounts.
     */
    export class YearEndCompanyController extends YearEndControllerBase {
        /**
         * Open a dialog that adds a new correction row to the Worker or edits an existing one.
         * @param rowType Type of the new row:
         * "external" for salaries paid outside Salaxy, "correction" to changes in salaries paid in Salaxy
         * @param worker Worker summary into which the row is added. Required even in edit for Delete.
         * @param row Optional row to edit. If null adds a new row.
         */
        editRow(rowType: "external" | "correction", worker: WorkerSummary, row?: CalculationSummary): Promise<any>;
        /** Returns true if the account has indicated they have salaries that were paid external to Salaxy system */
        readonly hasExternalSalaries: boolean;
        /**
         * Helper method for summing array properties
         */
        sumOf(array: any[], property: string): number;
        /** TODO: from core */
        replaceComma(value: string): string;
        /**
         * Shows the confirm dialog depending on which button is clicked
         * @param userStatus - Status corresponds to the button that user clicks.
         */
        showConfirmDialog(userStatus: YearEndUserFeedback): void;
    }
}
declare module "controllers/year-end/YearEndHouseholdController" {
    import { YearEndUserFeedback } from "@salaxy/core";
    import { YearEndControllerBase } from "controllers/year-end/YearEndControllerBase";
    /**
     * Implements the YearendControllerBase for Household accounts.
     */
    export class YearEndHouseholdController extends YearEndControllerBase {
        /**
         * Household tax deduction: Percentage from the total salary
         * This number is updated by getTaxDeductionTotal().
         */
        percentDeductible: number;
        /** Calculates the household tax deduction total */
        getTaxDeductionTotal(): number;
        /**
         * The insurance sum is either the insurance sum that the user has input
         * or the last known sum from the employer profile.
         */
        getInsuranceSum(): any;
        /** Returns true if there is more than 2500€ of household deduction meaning that it should be split between spouses */
        canSplitWithSpouse(): boolean;
        /** Returns true if the spouse information is provided */
        isSpouseInfoProvided(): string;
        /** Gets the insurance product for the current user */
        readonly insurance: salaxy.model.InsuranceProduct;
        /**
         * Shows the confirm dialog depending on which button is clicked
         * @param userStatus - Status corresponds to the button that user clicks.
         */
        showConfirmDialog(userStatus: YearEndUserFeedback): void;
    }
}
declare module "controllers/year-end/index" {
    export * from "controllers/year-end/AddCorrectionDialogController";
    export * from "controllers/year-end/YearEndControllerBase";
    export * from "controllers/year-end/YearEndCompanyController";
    export * from "controllers/year-end/YearEndHouseholdController";
}
declare module "controllers/CompanyOnboardingWizardController" {
    import * as angular from "angular";
    import { ApiValidationError, Onboarding } from "@salaxy/core";
    import { WizardController } from "controllers/WizardController";
    import { AuthorizedAccountService, OnboardingService, SessionService, WizardService, WizardStep } from "services/index";
    /**
     * Wizard for Creating a new Palkkaus.fi company account.
     */
    export class CompanyOnboardingWizardController extends WizardController {
        private onboardingService;
        private sessionService;
        private authorizedAccountService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Company wizard configuration */
        static getWizardSteps(session: SessionService): WizardStep[];
        /** Flag indicating if e-authorization check will be performed. */
        eAuth: boolean;
        /** Form data validity */
        formDataValidity: boolean;
        /** If true, step is proceeding */
        isStepProceeding: boolean;
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param onboardingService - Maintains the state of onboarding data
         * @param sessionService - Salaxy session service.
         * @param authorizedAccountService - Salaxy account service.
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, onboardingService: OnboardingService, sessionService: SessionService, authorizedAccountService: AuthorizedAccountService);
        /**
         * The onboarding model is provided by the onboarding service.
         *
         */
        readonly model: Onboarding;
        /** Returns true if user can go forward in wizard  */
        readonly canGoNext: boolean;
        /** Sharing link for authorization mail. */
        readonly sharingLink: string;
        /** Mailto-link with sharing link. */
        readonly mailtoLink: string;
        /** Returns true, if the site is for testing. */
        readonly isTestData: boolean;
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Navigates to the next step if possible and saves the data.
         */
        goNext(): void;
        /**
         * Navigates to the previous step if possible and saves the data.
         */
        goPrevious(): void;
        /**
         * Returns validation error for key if exists.
         * @param key - Validation error key.
         */
        getValidationError(key: string): ApiValidationError;
        /**
         * Proceeds to company selection in e-authorization service.
         */
        goToCompanySelection(): void;
        /**
         * Flag for year change.
         */
        is2018(): boolean;
        /** Commit onboarding */
        commit(): void;
        /** Opens wizard as modal dialog */
        launchCompanyWizard(accountId?: string): Promise<any>;
        /** Sets pension fields (defaults) after user input. */
        checkPensionCompanySelection(): void;
        /** Sets insurance fields (defaults) after user input. */
        checkInsuranceCompanySelection(): void;
        /** Returns pension number for test account
         * @param id Id of the pension company
         */
        getPensionNumberForTest(id: string): void;
        /**
         * Saves the data to server
         */
        private save;
    }
}
declare module "controllers/ContactController" {
    import { ContractParty } from "@salaxy/core";
    import { ContactService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles Contacts
     */
    export class ContactController extends CrudControllerBase<ContractParty> {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(contactService: ContactService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
    }
}
declare module "controllers/ContractController" {
    import { EmploymentContract } from "@salaxy/core";
    import { ContactService, ContractService, SessionService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Handles employment contracts
     */
    export class ContractController extends CrudControllerBase<EmploymentContract> {
        private contractService;
        private contactService;
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Active selected tab */
        protected activeTab: number;
        constructor(contractService: ContractService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, contactService: ContactService, sessionService: SessionService);
        /** Get preview */
        readonly currentPreview: string;
        /** Returns list of all workers (for employer role). */
        readonly workerList: any[];
        /** Returns list of all employers (for worker role). */
        readonly employerList: any[];
        /**
         * Checks whether the user is in a given role
         * @param role - One of the known roles
         */
        isInRole(role: any): boolean;
        /**
         * Tab selection method.
         * @param activeTab - selected tab.
         */
        protected selectTab(activeTab: number): void;
    }
}
declare module "controllers/CreateAccountWizardController" {
    import * as angular from "angular";
    import { ApiValidationError, Onboarding, WebSiteUserRole } from "@salaxy/core";
    import { OnboardingService, UiHelpers, WizardService, WizardStep } from "services/index";
    import { WizardController } from "controllers/WizardController";
    /**
     * Wizard for Creating a new Palkkaus.fi-account
     */
    export class CreateAccountWizardController extends WizardController {
        private onboardingService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Base wizard is shown in the first screen - when the Account type to be created is not yet known */
        static baseWizard: WizardStep[];
        /** Household wizard configuration */
        static householdWizard: WizardStep[];
        /** Company wizard configuration */
        static companyWizard: WizardStep[];
        /** Worker wizard configuration */
        static workerWizard: WizardStep[];
        /** Worker fetching salary slip wizard configuration */
        static workerFetchingSalarySlipWizard: WizardStep[];
        /**
         * Option to skip telepehone sms verification in the test enviroment.
         */
        skipSmsVerification: boolean;
        /**
         * Has the user agreed to the EULA of the service
         */
        isTermsOk: boolean;
        /**
         * Has the user got signature rights in the company / organization
         */
        isProcura: boolean;
        /**
         * Field to expose forms validity state.
         */
        formDataValidity: boolean;
        /** If true, step is proceeding */
        isStepProceeding: boolean;
        /** if true, verification process is proceeding */
        isVerificationProceeding: boolean;
        /**
         * A list of company types, with individual label and value
         */
        companyTypes: {
            label: string;
            value: string;
        }[];
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param onboardingService - Maintains the state of onboarding data
         * @param uiHelpers - Salaxy ui helpers.
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, onboardingService: OnboardingService, uiHelpers: UiHelpers);
        /**
         * Navigates to the next step if possible and saves the data.
         */
        goNext(): Promise<boolean>;
        /** Runs a check function before going forward */
        goNextIf(checkFunction: (goNext: () => Promise<boolean>) => void): void;
        /**
         * Navigates to the previous step if possible and saves the data.
         */
        goPrevious(): void;
        /**
         * Sets the company's business id, clears the YTJ search results fetches other company info's via YTJ search and saves
         *
         * @param businessId The business id that is set as the company's business id
         */
        chooseBusinessId(businessId: any): void;
        /**
         * Sets the DoYtjUpdate flag to true and saves.
         *
         * @param searchType - If true, sets the business ID to null
         * Use this option to force a YTJ name search, even if there already is something written in business ID field.
         */
        searchYtj(searchType?: "businessId" | "name"): void;
        /**
         * Saves the data to server
         */
        save(): Promise<Onboarding>;
        /**
         * Returns signing url.
         */
        readonly vismaSignUrl: string;
        /**
         * The onboarding model is provided by the onboarding service.
         */
        readonly model: Onboarding;
        /**
         * Sets the personName in signature object
         * @param personName Name (first and last name) that is set to signature object in onboarding data
         */
        setSignatureName(personName: string): void;
        /**
         * Sets the email in signature object
         * @param email Email that is set to signature object in onboarding data
         */
        setSignatureEmail(email: string): void;
        /**
         * Sets the telephone number in signature object
         * @param telephone Telephone number that is set to signature object in onboarding data
         */
        setSignaturePhone(telephone: string): void;
        /**
         * Checks should the isPensionSelfHandling parameter be forced to true based on pension company selection.
         * If the company equals Veritas or other, the system forces user to self-handle their pension payments and notifications
         * Note! If the company is Varma, Elo or Ilmarinen, this function does not change the isPensionSelfHandling back to false
         * @param pensionCompany
         */
        checkPensionSelfHandlingBasedOnCompany(pensionCompany: string): void;
        /**
         * Sets the pension company and pension contract number to null if there is no pension contract
         */
        checkPensionCompanyBasedOnContractStatus(): void;
        /**
         * Gets / sets the type in a way that the wizard steps are updated (checkWizardSteps)
         */
        type: WebSiteUserRole;
        /** Returns the PDF preview address for the authorization pdf. */
        getPdfPreviewAddress(): string;
        /**
         * Returns validation error for key if exists.
         * @param key - Validation error key.
         */
        getValidationError(key: string): ApiValidationError;
        /**
         * Resets validation error for key if exists.
         * @param key - Validation error key.
         */
        removeValidationError(key: string): void;
        /** Returns the wizard title. */
        readonly pageTitle: string;
        /** Sends a new pin code for verifying the telephone number in onboarding. */
        sendSmsVerificationPin: (callback: () => Promise<boolean>) => void;
        /** Checks the verfication pin and calls callback if success */
        checkSmsVerificationPin: (callback: () => Promise<boolean>) => void;
        /** Returns true if user can go forward in wizard  */
        readonly canGoNext: boolean;
        /** Returns true if user can go backward in wizard */
        readonly canGoPrevious: boolean;
        /** Opens wizard as modal dialog */
        launchAccountWizard(id?: string, isModelLoaded?: boolean, resetAccountType?: boolean): Promise<any>;
    }
}
declare module "controllers/CreditTransferController" {
    import * as angular from "angular";
    import { Payer, Payment, Recipient } from "@salaxy/core";
    /**
     * Contains functionality for showing credit transfer.
     */
    export class CreditTransferController implements angular.IController {
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: any[];
        /** Payment for the credit transfer. */
        payment: Payment;
        /** https://github.com/isonet/angular-barcode */
        bcOptions: {
            width: number;
            height: number;
            displayValue: boolean;
            marginTop: number;
            format: string;
        };
        /**
         * Creates a new CreditTransferController.
         * @ignore
         */
        constructor();
        /** Barcode value */
        readonly bcValue: string;
        /** Bank caccount IBAN for the recimpient. */
        readonly recipientIban: string;
        /** Payer for the credit transfer. */
        readonly payer: Payer;
        /** Recipient of the payment - typically Palkkaus.fi */
        readonly recipient: Recipient;
        /** Reference number based on which the payment is routed. */
        readonly referenceNumber: string;
        /** Due date for the payment */
        readonly dueDate: string;
        /** Amount to be paid. */
        readonly amount: number | string;
    }
}
declare module "controllers/EmployerPaymentController" {
    import * as angular from "angular";
    import { BankPaymentType, Payment, PeriodType } from "@salaxy/core";
    import { PaymentService, UiHelpers } from "services/index";
    /**
     * Lists payments to be paid by the employer.
     */
    export class EmployerPaymentController implements angular.IController {
        private paymentService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        private static getDefaultRefDate;
        private static getDefaultEndDate;
        /** List of employer payments. */
        list: Payment[];
        /** Summaruy of the list */
        summary: Array<{
            /** Payment type */
            type: BankPaymentType;
            /** Sum of payments */
            sum: number;
        }>;
        /** Reference date for binding. */
        refDate: string;
        /** End date for binding. */
        endDate: string;
        /**
         * Creates a new EmployerPaymentController.
         * @param  paymentService - Service for payments.
         * @ignore
         */
        constructor(paymentService: PaymentService, uiHelpers: UiHelpers);
        /**
         * Implement IController by providing onInit method.
         * We currently do nothing here, but if you override this function,
         * you should call this method in base class for future compatibility.
         */
        $onInit(): void;
        /**
         * Lists the monthly / quarterly/ yearly employer payments for the current account.
         *
         * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
         * @param paymentTypes - Payment types to list.
         * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
         * @param endDate - End date for the period. Required only for the custom period.
         * @returns A Promise containing payments.
         */
        getEmployerPayments(refDate: string, paymentTypes?: BankPaymentType[], periodType?: PeriodType, endDate?: string): void;
        /**
         * Show details of the selected payment.
         * @param payment - Selected payment.
         */
        show(payment: Payment): void;
    }
}
declare module "controllers/ESalaryPaymentController" {
    import { Calculation, ESalaryPayment } from "@salaxy/core";
    import { CalculationsService, ESalaryPaymentService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Controller for CRUD opoerations and other functions on ESalaryPayments.
     */
    export class ESalaryPaymentController extends CrudControllerBase<ESalaryPayment> {
        private eSalaryService;
        private calcSrv;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        constructor(eSalaryService: ESalaryPaymentService, calcSrv: CalculationsService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers);
        /**
         * Returns the current calculation.
         * Returns null if there is no current E-salary.
         * Sets the current.calc if that is null.
         */
        readonly currentCalc: Calculation;
        /**
         * Creates a new item. The item is not saved in this process yet.
         * The current location url is set to the success/cancel url.
         */
        createNew(): ESalaryPayment;
        /**
         * Gets the payment address for the E-salary payment.
         * @param eSalary The E-salary for which to fetch the Payment address.
         */
        getPaymentAddress(eSalary: ESalaryPayment): string;
        /** Gets the list info for Employer */
        getEmployerInfo(eSalary: ESalaryPayment): string;
        /** Gets the list info for Worker */
        getWorkerInfo(eSalary: ESalaryPayment): string;
    }
}
declare module "controllers/FinvoiceTesterController" {
    import { Calculator, Payment } from "@salaxy/core";
    import { CalculationsService, PaymentService, ReportsService } from "services/index";
    /**
     * Experimental controller for accounting reports and finvoice.
     */
    export class FinvoiceTesterController implements angular.IController {
        private calculationsService;
        private reportsService;
        private paymentService;
        private calculator;
        private $sce;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** The finvoice data for preview */
        finvoice: any;
        /** The invoice pdf for preview */
        pdfUrl: any;
        /** The accounting report */
        report: any;
        /** The payment */
        payment: Payment;
        /** Accounting report taable type */
        tableType: any;
        /**
         * Constructor for Dependency Injection.
         * @param calculations - Calculations service.
         * @param reportsService - Reports service.
         * @param PaymentService - Payment service.
         * @param calculator - to be removed.
         * @param $sce - $sce service.
         */
        constructor(calculationsService: CalculationsService, reportsService: ReportsService, paymentService: PaymentService, calculator: Calculator, $sce: angular.ISCEService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Gets the payment, finvoice and accounting report if they do not exist.
         */
        refresh(force?: boolean): void;
        /** Returns report rows of given row type */
        getRows(...rowTypes: string[]): any;
        /** Resets payment, accounting report and finvoice. */
        reset(): void;
    }
}
declare module "controllers/IfInsuranceWizardController" {
    import * as angular from "angular";
    import { IfInsuranceOrder } from "@salaxy/core";
    import { IfInsuranceService, UiHelpers, WizardService } from "services/index";
    import { WizardController } from "controllers/WizardController";
    /**
     * Wizard for choosing If insurance
     */
    export class IfInsuranceWizardController extends WizardController {
        private insuranceService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** If Insurance wizard configuration */
        private static wizardSteps;
        private static moveSteps;
        private static buySteps;
        /** Form data validity */
        formDataValidity: boolean;
        /** Indicates if the order has been sent. */
        isSent: boolean;
        /** Indicates if order is just sending */
        isSending: boolean;
        /** Insurance action */
        action: "move" | "buy";
        /** Min date for new insurance */
        insuranceDateOptions: {
            minDate: Date;
        };
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param insuranceService - Insurance service
         * @param UiHelpers - Salaxy UI helpers
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, insuranceService: IfInsuranceService, uiHelpers: UiHelpers);
        /**
         * Sets the current wizard action and wizard steps
         * @param insuranceAction - Action to performs: order new or move existing insurance.
         */
        setInsuranceAction(insuranceAction: "move" | "buy"): void;
        /**
         * The insurance order.
         */
        readonly model: IfInsuranceOrder;
        /** Send the current order. */
        send(): void;
        /** Returns true if user can go forward in wizard  */
        readonly canGoNext: boolean;
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Navigates to the next step if possible and saves the data.
         */
        goNext(): void;
        /**
         * Navigates to the previous step if possible and saves the data.
         */
        goPrevious(): void;
        /** Opens the insurance dialog. */
        openModal(): void;
    }
}
declare module "controllers/OnboardingController" {
    import * as angular from "angular";
    import { OnboardingService } from "services/index";
    /**
     * Simple onboarding controller:
     * Provides simple functionality for plain onboarding.
     * No wizards and other bells and whistles.
     * Also no product gathering - just the necessary stuff to create an account.
     */
    export class OnboardingController implements angular.IController {
        private onboardingService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new OnboardingController
         * @param $scope - The Angular scope
         * @param onboardingService - Maintains the state of onboarding data
         * @ignore
         */
        constructor($scope: angular.IScope, onboardingService: OnboardingService);
        /**
         * Implement IController
         */
        $onInit: () => void;
    }
}
declare module "controllers/PricingController" {
    import * as angular from "angular";
    import { AccountService } from "services/index";
    /**
     * Controller for the pricing partner settings.
     */
    export class PricingController implements angular.IController {
        private accountService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Current bound pricing product. */
        product: any;
        /**
         * Creates a new PricingController
         *
         * @param  accountService - Account service containing the products of the current user.
         */
        constructor(accountService: AccountService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Returns the current pricing product.
         * If the control is bound, returns the bound product.
         * Otherwise returns the product of the current user.
         */
        readonly current: any;
        /**
         * Returns the current Palkkaus fee.
         */
        readonly palkkausFee: number | null;
        /** Set default fixed fee */
        setDefaultFixedFee(): void;
    }
}
declare module "controllers/ProfilesController" {
    import * as angular from "angular";
    import { City, Profile, ProfileJob } from "@salaxy/core";
    import { ProfileSearchParameters, ProfilesService } from "services/index";
    /**
     * User interaction with Public profiles: Searches and showing a detail view.
     */
    export class ProfilesController implements angular.IController {
        private profilesService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new ProfilesController
         * @param profilesService - Service that handles the communication to the server
         * @ignore
         */
        constructor(profilesService: ProfilesService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** The profiles returned by the search (initial or custom */
        readonly profiles: any[];
        /** Currently selected profile */
        readonly currentProfile: any;
        /** ID of the currently selected profile. Null if no profile is selected. */
        readonly currentProfileId: string;
        /** Search parameters that are in sync with the results */
        readonly currentSearch: ProfileSearchParameters;
        /** Search parameters that are in sync with search input field - not necessarily yet with the results */
        readonly nextSearch: ProfileSearchParameters;
        /** All cities - based on the initial search */
        readonly allCities: any[];
        /** All cities */
        readonly citiesForEdit: City[] | Promise<City[]>;
        /** All jobs - based on the initial search */
        readonly allJobs: any[];
        /** All jobs */
        readonly jobsForEdit: any[];
        /** All categories - based on the initial search */
        readonly allCategories: any[];
        /** Count of all items in the database - based on the initial search */
        readonly allCount: number;
        /** Cities in the current search - note that the counts are also within the current search */
        readonly cities: any[];
        /** Jobs in the current search - note that the counts are also within the current search */
        readonly jobs: any[];
        /** Categories in the current search - note that the counts are also within the current search */
        readonly categories: any[];
        /**
         * User's own profile
         */
        readonly myProfile: any;
        /**
         * Current contact
         */
        readonly contact: any;
        /**
         * Count of the items in the current search.
         * Note that this may be more than profiles.length because the number of profiles returned from the server is limited (to 100 at the time of writing).
         */
        readonly count: number;
        /**
         * Sets the current profile id - this will fetch the profile detail data from the server
         *
         * @param profileId - identifier of the profile
         */
        setCurrent(profileId: string): void;
        /**
         * Sets the current user's profile  - this will fetch the profile detail data from the server
         *
         */
        setMyProfile(): void;
        /**
         * Sets the contact for single profile  - this will fetch the profile detail data from the server
         *
         * @param { string } profileId - The identifier of the profile.
         */
        setContact(profileId: string): void;
        /**
         * Invokes the search to the server (using the Profiles service)
         *
         * @param {ProfileSearchParameters} searchParameters - The search parameters. If null, the nextSearch property is used.
         */
        doSearch(searchParameters?: ProfileSearchParameters): void;
        /**
         * Gets the full category data based on category id
         * returns an empty object if ID is not found for safe binding to child properties
         *
         * @param  categoryId - Identifier of the category
         */
        getCategory(categoryId: any): any;
        /**
         * Gets the full job data based on job id
         * returns an empty object if ID is not found for safe binding to child properties
         *
         * @param  jobId - Identifier of the job
         */
        getJob: (jobId: string) => any;
        /**
         * Gets the full city data based on city key
         * returns an empty object if ID is not found for safe binding to child properties
         *
         * @param  cityId - Identifier of the city
         */
        getCity: (cityId: string) => any;
        /**
         * Returns the name property of the object.
         * @param {any} obj - any object.
         */
        getObjName(obj: any): string;
        /**
         * Saves user's profile to the database.
         */
        saveMyProfile(): Promise<Profile>;
        /**
         * Returns the url where to post the avatar image file
         */
        getAvatarImageUploadUrl(): string;
        /** Returns job list for edit UI. */
        getJobsForEdit(): ProfileJob[];
        /** Returns city list for edit UI. */
        getCitiesForEdit(): City[] | Promise<City[]>;
    }
}
declare module "controllers/ReportsController" {
    import * as angular from "angular";
    import { Report, ReportType } from "@salaxy/core";
    import { ReportsService, SessionService } from "services/index";
    /**
     * Handles user interaction for fetching, showing and later generating Reports
     */
    export class ReportsController implements angular.IController {
        private reportsService;
        private sessionService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Current report type for this controller. Default for the getReports() method.
         */
        reportType: ReportType;
        /**
         * Static instance of getReportTypes() to avoid a $digest problem that occured when calling getReportTypes() directly from the view.
         * HACK: This may not work in a scenario where Controller is created before the Session.
         */
        types: {
            id: string;
            title: string;
            description: string;
            roles: string;
            category: string;
        }[];
        constructor(reportsService: ReportsService, sessionService: SessionService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Gets a list of reports (metadata only) filtered by a report type.
         * @param type - Type of report. See type (string enumeration) for possible values. Default is the property reportType.
         */
        getReports(type?: ReportType): Report[] | Promise<Report[]>;
        /**
         * Gets a list of available report types for the current user.
         * TODO: This method is still under construction.
         */
        getReportTypes(): {
            id: string;
            title: string;
            description: string;
            roles: string;
            category: string;
        }[];
    }
}
declare module "controllers/SignatureController" {
    import * as angular from "angular";
    import { SignatureMethod, SignatureService } from "services/index";
    /**
     * Handles user interaction for Digital Signature
     */
    export class SignatureController implements angular.IController {
        private signatureService;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Creates a new SignatureController
         * @param signatureService - Service that handles the communication to the server
         * @ignore
         */
        constructor(signatureService: SignatureService);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** List of supported Signature methods */
        readonly methods: SignatureMethod[];
        /**
         * Returns the SignatureMethod for given value.
         * @param value - value for SignatureMethod, e.g. tupas-nordea.
         */
        getMethod(value: string): SignatureMethod;
    }
}
declare module "controllers/SigningController" {
    import * as angular from "angular";
    import { Onboarding } from "@salaxy/core";
    import { OnboardingService, SessionService, SignatureMethod, SignatureService, UiHelpers } from "services/index";
    /**
     * Controller for showing the signing user interface for unsigned account.
     */
    export class SigningController implements angular.IController {
        private $rootScope;
        private onboardingService;
        private sessionService;
        private signatureService;
        private $sce;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Boolean for indicating if the onboarding object fetching is in progress. */
        isLoading: boolean;
        private pdfTimestamp;
        /**
         * Creates a new SigningController
         * @param $rootScope - Angular root scope.
         * @param onboardingService - Onboarding service.
         * @param sessionService - Session service.
         * @param signatureService - Signature service.
         * @param $sce - Angular $sce service.
         * @ignore
         */
        constructor($rootScope: angular.IRootScopeService, onboardingService: OnboardingService, sessionService: SessionService, signatureService: SignatureService, $sce: angular.ISCEService, uiHelpers: UiHelpers);
        /**
         * Implement IController
         */
        $onInit: () => void;
        /** Saves the current onboarding model. */
        save(): Promise<Onboarding>;
        /**
         * The onboarding model is provided by the onboarding service.
         */
        readonly model: Onboarding;
        /** Returns the PDF preview address for the authorization pdf. */
        getPdfPreviewAddress(): any;
        /**
         * Saves the model and starts the signature.
         * @param authMethod Method of authentication (bank selection).
         */
        startVismaSign(authMethod: string): void;
        /** Launches the account wizard for the current account. */
        launchAccountWizard(): Promise<any>;
        /** List of supported Signature methods */
        readonly methods: SignatureMethod[];
        /**
         * Returns the SignatureMethod for given value.
         * @param value - value for SignatureMethod, e.g. tupas-nordea.
         */
        getMethod(value: string): SignatureMethod;
        /** Returns true if the signing is required. */
        readonly isSigningRequired: boolean;
        private init;
    }
}
declare module "controllers/TaxCard2019Controller" {
    import * as angular from "angular";
    import { Ajax, PersonAccount, Taxcard, TaxcardKind, WorkerAccount } from "@salaxy/core";
    import { SessionService, TaxCardService, UiHelpers, WorkersService } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Base controller for salaxyCurrentTaxCard and salaxyWorkerTaxCards components
     */
    export class TaxCard2019Controller extends CrudControllerBase<Taxcard> {
        private taxCardService;
        private workersService;
        private fileUpload;
        private ajax;
        private sessionSrv;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * String to show upload progress in UI
         */
        uploadProgress: string;
        /**
         * Defines to which worker the functionality is bound to:
         *
         * - "currentWorker" (default): binds to the currently selected worker
         * - "self": binds to current account - users own tax cards.
         */
        binding: "currentWorker" | "self";
        /**
         * Today for data binding in datetime controls.
         */
        today: string;
        /**
         * Last day of current year - used in limiting date picker to this year.
         */
        yearsLastDay: Date;
        /** Max date for muutosverokortti 2019 */
        yearEndDate: {
            maxDate: Date;
        };
        private chartCache;
        /**
         * Creates a new TaxCard2019Controller
         *
         * @param  taxCard2019service - Gets the all owned tax cards or latest tax card for the current user from the database.
         * @param  $location - Angular $location service.
         * @param  $attrs - Angular @attrs service.
         * @param  uiHelpers - Salaxy ui helpers service.
         * @param  workersService - Gets the current worker for methods allTaxCardsForCurrentWorker() and getLatestTaxCardForCurrentWorker()
         * @param  fileupload - ng-file-upload upload service, used in uploading an attachment file when inputting a new tax card
         * @param  ajax - Salaxy ajax service, holds the token that is needed in method uploadTaxCard(), when inputting a new tax card
         */
        constructor(taxCardService: TaxCardService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, workersService: WorkersService, fileUpload: any, ajax: Ajax, sessionSrv: SessionService);
        /**
         * If true, the UI is in edit (add new) mode.
         * If false, current tax card status / properties is shown in read-only mode.
         */
        readonly isInEditMode: boolean;
        /**
         * Current tax card: Shown in read only / info view.
         * Editable UI is bound to newTaxCard.
         */
        readonly workerTaxCard: Taxcard;
        /**
         * Gets the current worker for which the taxcards are shown / modified.
         * Note that this may be either WorkerAccount or PersonAccount.
         */
        readonly currentWorker: WorkerAccount | PersonAccount;
        /** Gets the personal id for the currently selected worker / account. Null, if none selected. */
        readonly currentPersonalId: string;
        /** Gets the chart data, labels, colors and other settings. */
        readonly chartData: {
            limits: {
                type: TaxcardKind;
                percent: number;
                hasLimit: boolean;
                fractionOfLimit: number;
            };
            data: any[];
            labels: any[];
            datasets: {};
            options: any;
        };
        /** Gets the cached version of the chart data. */
        readonly pieChartData: {
            limits: {
                type: TaxcardKind;
                percent: number;
                hasLimit: boolean;
                fractionOfLimit: number;
            };
            data: any[];
            labels: any[];
            datasets: {};
            options: any;
        };
        /**
         * Opens tax card edit view
         */
        openEditView(type?: TaxcardKind): void;
        /**
         * Closes tax card edit view
         */
        closeEditView(): void;
        /**
         * Method that fills the blank tax card with the options for 'no tax card' option and saves the blank tax card as latest/current tax card
         */
        chooseNoTaxCard(): void;
        /**
         * Saves a new tax card
         */
        saveCurrent(callback?: (taxcard: Taxcard) => void): Promise<Taxcard>;
        /**
         * Returns all tax cards for current worker
         */
        readonly workerTaxCards: Taxcard[];
        /**
         * Deletes a tax card with given id
         * @param id - Id of the tax card to be deleted
         */
        deleteTaxCard(id: string): void;
        /**
         * Counts total income for given tax card
         * @param taxCard - Tax card object that's total income is calculated
         */
        countTotalIncome(taxCard: Taxcard): number;
        /**
         * Checks if the current tax card is about to expire, for showing an alert in the UI
         * If it is January and the tax card's year is not current year, the tax card is about to expire
         */
        isTaxCardAboutToExpire(taxCard: Taxcard): boolean;
        /**
         * Checks if the tax card is expired
         * A tax card still valid in the next year's January
         */
        isTaxCardExpired(taxCard: Taxcard): boolean;
        /**
         * Returns the url where to download the tax card
         */
        getTaxCardDownloadUrl(taxCard: any): string;
        /**
         * Returns the url where to preview the tax card
         */
        getTaxCardPreviewUrl(taxCard: Taxcard): string;
        /**
         * Uploads tax card to the server.
         * @param {any} file - selected file
         */
        uploadTaxCard(file: any): void;
        /**
         * Defines which fields to show/hide, require etc. in the view
         *
         */
        protected readonly taxCardUISettings: {
            showTaxPercent: boolean;
            showTaxPercent2: boolean;
            showIncomeLimit: boolean;
            showTaxYear: boolean;
            showTaxCardStartDate: boolean;
            showTaxCardEndDate: boolean;
            showPreviousSalariesPaid: boolean;
            showExampleTaxCard: boolean;
            showFileUpload: boolean;
            requireTaxPercent: boolean;
            requireTaxPercent2: boolean;
            requireIncomeLimit: boolean;
            requireTaxYear: boolean;
            requireTaxCardStartDate: boolean;
            requireTaxCardEndDate: boolean;
            requirePreviousSalariesPaid: boolean;
            nonChangeable: boolean;
        };
    }
}
declare module "controllers/TaxCardsListController" {
    import * as angular from "angular";
    import { Taxcard, WorkerAccount } from "@salaxy/core";
    import { TaxCardService, UiHelpers, WorkersService } from "services/index";
    /**
     * Controller for tax card list. Displays a list of tax cards for all workers.
     */
    export class TaxCardsListController implements angular.IController {
        private $scope;
        private workersService;
        private taxCardService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * List of metadata of workers' taxcards
         */
        list: Array<{
            /** Worker account. */
            worker: WorkerAccount;
            /** Tax card for the worker. */
            taxCard: Taxcard;
            /** Unused.  */
            hasPersonalId: boolean;
            /** Typing for the tax card. */
            type: "noPersonalId" | "noTaxCard" | "expired" | "future" | "ok";
            /** Amount of paid salaries in this tax card. */
            paidSalaries: number;
            /** Current tax card percent for visualization. */
            currentTaxPercent: number;
            /** Boolean indicating whether that type of this tax card is 'ok'. */
            isOk: boolean;
        }>;
        /**
         * Creates a new TaxCardsListController
         * @param $scope - Controllers scope.
         * @param workersService - Worker service.
         * @param taxCardService - Tax card service.
         * @param uiHelpers - Salaxy ui helpers service.
         * @ignore
         */
        constructor($scope: angular.IScope, workersService: WorkersService, taxCardService: TaxCardService, uiHelpers: UiHelpers);
        /**
         * Refresh taxcard list
         */
        $onInit: () => void;
        /**
         * Shows a dialog for adding tax card for the worker.
         * @param workerAccountId - Worker id.
         */
        showTaxCardAddDialogForTaxCardsList(workerAccountId: string): void;
        /**
         * Refresh taxcard list.
         */
        refreshList(): void;
    }
}
declare module "controllers/VarmaPensionWizardController" {
    import * as angular from "angular";
    import { VarmaPensionOrder, VarmaPensionOrderAction } from "@salaxy/core";
    import { UiHelpers, VarmaPensionService, WizardService } from "services/index";
    import { WizardController } from "controllers/WizardController";
    /**
     * Wizard for choosing Varma pension
     */
    export class VarmaPensionWizardController extends WizardController {
        private pensionService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Company wizard configuration */
        private static wizardSteps;
        private static tyelSteps;
        private static yelSteps;
        private static mvmtSteps;
        /** Form data validity */
        formDataValidity: boolean;
        /** Indicates if the order has been sent. */
        isSent: boolean;
        /** Indicates if order is just sending */
        isSending: boolean;
        /** Pension action */
        action: VarmaPensionOrderAction;
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param PensionService - Pension service
         * @param UiHelpers - Salaxy UI helpers
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, pensionService: VarmaPensionService, uiHelpers: UiHelpers);
        /**
         * Sets the current wizard action and wizard steps
         * @param pensionAction - Action to performs: order new TyEL, YEL or move pension.
         */
        setPensionAction(pensionAction: VarmaPensionOrderAction): void;
        /**
         * Sets steps in the move action.
         */
        setMvmtSteps(): void;
        /**
         * The pension order.
         */
        readonly model: VarmaPensionOrder;
        /** Send the current order. */
        send(): void;
        /** Returns true if user can go forward in wizard  */
        readonly canGoNext: boolean;
        /**
         * Implement IController
         */
        $onInit: () => void;
        /**
         * Navigates to the next step if possible and saves the data.
         */
        goNext(): void;
        /**
         * Navigates to the previous step if possible and saves the data.
         */
        goPrevious(): void;
        /** Navigates to specified step */
        goToStep(step: any): void;
        /** Opens the pension dialog. */
        openModal(): void;
    }
}
declare module "controllers/EarningsPaymentController" {
    import * as angular from "angular";
    import { Calculation, Calculator, nir } from "@salaxy/core";
    import { CalculationsService, EarningsPaymentService, UiHelpers } from "services/index";
    import { CrudControllerBase } from "controllers/CrudControllerBase";
    /**
     * Controller for user interface of Earnings Payment Report ("Tulorekisteri-ilmoitus").
     */
    export class EarningsPaymentController extends CrudControllerBase<nir.EarningsPayment> {
        private earningsPaymentService;
        private calculationsService;
        private calculator;
        private $sce;
        private $scope;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Calculation for which the component shows the report.
         * If set, monitors changes and updates the report accordingly.
         * The object may be a calculation or string "current" for the current calculation.
         */
        calc: Calculation | "current";
        /**
         * Setter for EarningsPaymentReport object to which the component is bound.
         * NOTE: This is meant for HTML binding. Typically, you should use fullReport getter for fetching the report.
         */
        report: nir.EarningsPayment;
        /** Field indicating that loading from the server is in progress. */
        isLoading: boolean;
        private trustedHtmlCache;
        /** Constructor for Dependency Injection. */
        constructor(earningsPaymentService: EarningsPaymentService, $location: angular.ILocationService, $attrs: angular.IAttributes, uiHelpers: UiHelpers, calculationsService: CalculationsService, calculator: Calculator, $sce: angular.ISCEService, $scope: angular.IScope);
        /**
         * Implement IController
         */
        $onInit(): void;
        /** Changes event handler */
        $onChanges: (changesObj: any) => void;
        /** Getter for the full Earnings Payment Report object (including all metadata) */
        readonly fullReport: nir.EarningsPayment;
        /**
         * The actual payload report of the Earnings Payment report.
         * NOTE: In the Income Registry schema, there may be several payload reports,
         * but in the current Salaxy implementation, there is always only one.
         */
        readonly payloadReport: nir.Report;
        /** Delivery data part of the report */
        readonly deliveryData: nir.DeliveryData;
        /**
         * Refreshes the report based on calculation
         */
        refresh(): void;
        /** Resets wage report. */
        clear(): void;
        /** Gets a JavaScript object as formatted string. */
        getJsonHtml(obj: any): any;
        /**
         * Gets the details in a transaction row as simple texts that are
         * easy to bind to the user interface (read-only).
         */
        getRowDisplay(row: nir.Transaction): {
            details: {
                unitWages: any;
                carBenefit: any;
                mealBenefit: any;
                otherBenefit: any;
                dailyAllowance: any;
                kmAllowance: any;
            };
            exceptions: {
                noMoney: any;
                oneOff: any;
                unjustEnrichment: any;
                recovery: any;
                insuranceData: any;
                earningPeriods: any;
                recoveryData: any;
            };
        };
        /**
         * Returns income type code meta data.
         * TODO: move to Core LOGIC
         */
        getIncomeTypeCode(code: number): nir.IncomeTypeCode;
        /** Get income for report */
        getIncome(report: nir.Report): number;
        /**
         * Show details of the Earnings Payment.
         * @param earningsPayment - Earnings payment.
         */
        select(earningsPayment: nir.EarningsPayment): void;
        /**
         * Shows income type code editor.
         * @param row - Row to edit.
         */
        showTransactionCodeEditor(row: any): void;
        /** Deletes a row */
        removeRow(rowIndex: number): void;
        /** Adds a new row to report */
        addRow(): void;
        /**
         * Returns available transaction codes
         */
        private getTransactionCodeOptions;
    }
}
declare module "controllers/WorkerOnboardingWizardController" {
    import * as angular from "angular";
    import { ApiValidationError, Onboarding } from "@salaxy/core";
    import { OnboardingService, UiHelpers, WizardService, WizardStep } from "services/index";
    import { WizardController } from "controllers/WizardController";
    /**
     * Wizard for Creating a new Palkkaus.fi-account (worker)
     */
    export class WorkerOnboardingWizardController extends WizardController {
        private onboardingService;
        private uiHelpers;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /** Worker wizard configuration */
        static wizardSteps: WizardStep[];
        /**
         * Field to expose forms validity state.
         */
        formDataValidity: boolean;
        /** If true, step is proceeding */
        isStepProceeding: boolean;
        /** if true, verification process is proceeding */
        isVerificationProceeding: boolean;
        /**
         * Creates a new WizardController
         * @param $scope - The Angular scope
         * @param wizardService - Maintains the state of the wizard
         * @param onboardingService - Maintains the state of onboarding data
         * @param uiHelpers - Salaxy ui helpers.
         * @ignore
         */
        constructor($scope: angular.IScope, wizardService: WizardService, onboardingService: OnboardingService, uiHelpers: UiHelpers);
        /**
         * Navigates to the next step if possible and saves the data.
         */
        goNext(): Promise<boolean>;
        /** Runs a check function before going forward */
        goNextIf(checkFunction: (goNext: () => Promise<boolean>) => void): void;
        /**
         * Navigates to the previous step if possible and saves the data.
         */
        goPrevious(): void;
        /**
         * Saves the data to server
         */
        save(): Promise<Onboarding>;
        /**
         * Returns signing url.
         */
        readonly vismaSignUrl: string;
        /**
         * Option to skip telepehone sms verification in the test enviroment.
         */
        readonly skipSmsVerification: boolean;
        /**
         * The onboarding model is provided by the onboarding service.
         */
        readonly model: Onboarding;
        /**
         * Sets the personName in signature object
         * @param personName Name (first and last name) that is set to signature object in onboarding data
         */
        setSignatureName(personName: string): void;
        /**
         * Sets the email in signature object
         * @param email Email that is set to signature object in onboarding data
         */
        setSignatureEmail(email: string): void;
        /**
         * Sets the telephone number in signature object
         * @param telephone Telephone number that is set to signature object in onboarding data
         */
        setSignaturePhone(telephone: string): void;
        /** Returns the PDF preview address for the authorization pdf. */
        getPdfPreviewAddress(): string;
        /**
         * Returns validation error for key if exists.
         * @param key - Validation error key.
         */
        getValidationError(key: string): ApiValidationError;
        /**
         * Resets validation error for key if exists.
         * @param key - Validation error key.
         */
        removeValidationError(key: string): void;
        /** Sends a new pin code for verifying the telephone number in onboarding. */
        sendSmsVerificationPin: (callback: () => Promise<boolean>) => void;
        /** Checks the verfication pin and calls callback if success */
        checkSmsVerificationPin: (callback: () => Promise<boolean>) => void;
        /** Returns true if user can go forward in wizard  */
        readonly canGoNext: boolean;
        /** Returns true if user can go backward in wizard */
        readonly canGoPrevious: boolean;
        /** Opens wizard as modal dialog */
        launchWorkerOnboardingWizard(id?: string): Promise<any>;
    }
}
declare module "controllers/index" {
    export * from "controllers/account/index";
    export * from "controllers/bases/index";
    export * from "controllers/calc/index";
    export * from "controllers/helpers/index";
    export * from "controllers/communications/index";
    export * from "controllers/form-controls/index";
    export * from "controllers/modals/index";
    export * from "controllers/worker/index";
    export * from "controllers/year-end/index";
    export * from "controllers/CompanyOnboardingWizardController";
    export * from "controllers/ContactController";
    export * from "controllers/ContractController";
    export * from "controllers/CreateAccountWizardController";
    export * from "controllers/CreditTransferController";
    export * from "controllers/EmployerPaymentController";
    export * from "controllers/ESalaryPaymentController";
    export * from "controllers/FinvoiceTesterController";
    export * from "controllers/IfInsuranceWizardController";
    export * from "controllers/OnboardingController";
    export * from "controllers/PricingController";
    export * from "controllers/ProfilesController";
    export * from "controllers/ReportsController";
    export * from "controllers/SignatureController";
    export * from "controllers/SigningController";
    export * from "controllers/TaxCard2019Controller";
    export * from "controllers/TaxCardsListController";
    export * from "controllers/VarmaPensionWizardController";
    export * from "controllers/EarningsPaymentController";
    export * from "controllers/WizardController";
    export * from "controllers/WorkerOnboardingWizardController";
}
declare module "components/_ComponentBase" {
    import * as angular from "angular";
    /** Abstract Base class for salaxy components */
    export abstract class ComponentBase implements angular.IComponentOptions {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        abstract bindings: any;
        /**
         * Controller has the actual implementation of the component.
         * Same Controller may be reused between different components with different views and binding.
         */
        abstract controller: any;
        /** The default template for the component. */
        abstract defaultTemplate: any;
        /**
         * Set the template-url attribute to specify your custom template.
         * @ignore
         */
        templateUrl: ($element: any, $attrs: any) => any;
    }
}
declare module "components/calc/classic/Calc" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * The **salaxy.ng1.components.Calc** is the generic Angular-based salary calculator implementation.
     * It shows the basic features required for a salary calculator user interfaces:
     * 1. Setting the Worker for the calculation
     * 2. Defining the Work Framework and other information about the work
     * 3. Defining the Salary and the benefits
     * 4. Defining Expenses, benefits, household subsidies etc.
     * 5. Showing the results as charts and reports.
     *
     * Controllers are defined in **salaxy.ng1.controllers**. Most of the functionality comes from Calculator2016Controller.
     * The views can be modified separately for each component using the templateUrl. See documentation for details.
     *
     * @componenttag salaxy-calc
     * @example
     * ```html
     * <salaxy-calc></salaxy-calc>
     * ```
     */
    export class Calc extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcChart" {
    import { CalcChartController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Angular-chart based chart.
     *
     * @componenttag salaxy-calc-chart
     * @example
     * ```html
     * <salaxy-calc-chart role="worker"></salaxy-calc-chart>
     * ```
     */
    export class CalcChart extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** - role: Role for which the chart is rendered. Currently, 'worker' and 'employer' are supported. Employer is the default. */
            role: string;
            /** - scaleYAxis: Boolean that specifies should the Y axis of this chart be scaled so that if worker and employer charts are side to side, their Y axis are the same */
            scaleYAxis: string;
            /** - ChartType. */
            chartType: string;
            /** - colors: Custom colors for the chart as a string containing hex values of the colors, separated by a delimeter */
            colors: string;
        };
        /** Uses the CalcChartController */
        controller: typeof CalcChartController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcOverviewPanels" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the overview panels for the Calculators
     *
     * @componenttag salaxy-calc-overview-panels
     * @example
     * ```html
     * <salaxy-calc-overview-panels></salaxy-calc-overview-panels>
     * ```
     */
    export class CalcOverviewPanels extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcTopIcons" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the top icons above the overview panels for the Calculators
     *
     * @componenttag salaxy-calc-top-icons
     * @example
     * ```html
     * <salaxy-calc-top-icons></salaxy-calc-top-icons>
     * ```
     */
    export class CalcTopIcons extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcDetailsExpenses" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the expenses detail view for the Calculator
     *
     * @componenttag salaxy-calc-details-expenses
     * @example
     * ```html
     * <salaxy-calc-details-expenses></salaxy-calc-details-expenses>
     * ```
     */
    export class CalcDetailsExpenses extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcDetailsSalary" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the salary detail view for the Calculator
     *
     * @componenttag salaxy-calc-details-salary
     * @example
     * ```html
     * <salaxy-calc-details-salary></salaxy-calc-details-salary>
     * ```
     */
    export class CalcDetailsSalary extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcDetailsWork" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the Work/Info/Framework detail view for the Calculator
     *
     * @componenttag salaxy-calc-details-work
     * @example
     * ```html
     * <salaxy-calc-details-work></salaxy-calc-details-work>
     * ```
     */
    export class CalcDetailsWork extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcDetailsWorker" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the worker detail view for the Calculator
     *
     * @componenttag salaxy-calc-details-worker
     * @example
     * ´´´html
     * <salaxy-calc-details-worker></salaxy-calc-details-worker>
     * ´´´
     */
    export class CalcDetailsWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcNewIntro" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows an introduction text (instructions) for the user when user starts a new calculation.
     *
     * @componenttag salaxy-calc-new-intro
     * @example
     * ```html
     * <salaxy-calc-new-intro></salaxy-calc-new-intro>
     * ```
     */
    export class CalcNewIntro extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcPayButton" {
    import { PaymentController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a Payment -button for the current Calculation.
     *
     * @componenttag salaxy-calc-pay-button
     * @example
     * ```html
     * <salaxy-calc-pay-button></salaxy-calc-pay-button>
     * ```
     */
    export class CalcPayButton extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Expression for ng-disabled of the input */
            disabled: string;
        };
        /** Uses the PaymentController */
        controller: typeof PaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/CalcResults" {
    import { Calculator2018Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the results view for the Calculator
     *
     * @componenttag salaxy-calc-results
     * @example
     * ```html
     * <salaxy-calc-results></salaxy-calc-results>
     * ```
     */
    export class CalcResults extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2018Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/classic/index" {
    export * from "components/calc/classic/Calc";
    export * from "components/calc/classic/CalcChart";
    export * from "components/calc/classic/CalcOverviewPanels";
    export * from "components/calc/classic/CalcTopIcons";
    export * from "components/calc/classic/CalcDetailsExpenses";
    export * from "components/calc/classic/CalcDetailsSalary";
    export * from "components/calc/classic/CalcDetailsWork";
    export * from "components/calc/classic/CalcDetailsWorker";
    export * from "components/calc/classic/CalcNewIntro";
    export * from "components/calc/classic/CalcPayButton";
    export * from "components/calc/classic/CalcResults";
}
declare module "components/calc/payroll/PayrollEdit" {
    import { PayrollController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
     * Typical usecase is that a a company has e.g. a monthly salary list that is paid
     * at the end of month. For next month, a copy is then made from the latest list and
     * the copy is potentially modified with the changes of that particular month.
     * Payroll can also be started from scratch either by just writing salaries from
     * e.g. an e-mail or by uploading an Excel sheet.
     *
     * @componenttag salaxy-payroll-edit
     * @example
     * ```html
     * <salaxy-payroll-edit></salaxy-payroll-edit>
     * ```
     */
    export class PayrollEdit extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the PayrollController */
        controller: typeof PayrollController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/PayrollEditGrid" {
    import { PayrollController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For PayrollEdit component, implements the fast / simple grid input.
     *
     * @componenttag salaxy-payroll-edit-grid
     * @example
     * ```html
     * <salaxy-payroll-edit-grid></salaxy-payroll-edit-grid>
     * ```
     */
    export class PayrollEditGrid extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the PayrollController */
        controller: typeof PayrollController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/PayrollEditProperties" {
    import { PayrollController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For PayrollEdit component, implements the general properties / metadata editor.
     *
     * @componenttag salaxy-payroll-edit-properties
     * @example
     * ```html
     * <salaxy-payroll-edit-properties></salaxy-payroll-edit-properties>
     * ```
     */
    export class PayrollEditProperties extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the PayrollController */
        controller: typeof PayrollController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/PayrollEditValidate" {
    import { PayrollController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For PayrollEdit component, implements the Validator view / Structured editor.
     *
     * @componenttag salaxy-payroll-edit-validate
     * @example
     * ```html
     * <salaxy-payroll-edit-validate on-add-worker="$ctrl.activeTab = 1"></salaxy-payroll-edit-validate>
     * ```
     */
    export class PayrollEditValidate extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Function to call when adding a new worker is requested in validation view */
            onAddWorker: string;
        };
        /** Uses the PayrollController */
        controller: typeof PayrollController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/PayrollList" {
    import { PayrollController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Payroll (Palkkalista) is a list of employees who receive salary or wages from a particular organization.
     * Typical usecase is that a a company has e.g. a monthly salary list that is paid
     * at the end of month. For next month, a copy is then made from the latest list and
     * the copy is potentially modified with the changes of that particular month.
     * Payroll can also be started from scratch either by just writing salaries from
     * e.g. an e-mail or by uploading an Excel sheet.
     *
     * @componenttag salaxy-payroll-list
     * @example
     * ```html
     * <salaxy-payroll-list></salaxy-payroll-list>
     * ```
     */
    export class PayrollList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the PayrollController */
        controller: typeof PayrollController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/PayrollPayButton" {
    import { PaymentController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a Payment -button for the current Payroll.
     *
     * @componenttag salaxy-payroll-pay-button
     * @example
     * ```html
     * <salaxy-payroll-pay-button></salaxy-payroll-pay-button>
     * ```
     */
    export class PayrollPayButton extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Expression for ng-disabled of the input */
            disabled: string;
        };
        /** Uses the PaymentController */
        controller: typeof PaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/payroll/index" {
    export * from "components/calc/payroll/PayrollEdit";
    export * from "components/calc/payroll/PayrollEditGrid";
    export * from "components/calc/payroll/PayrollEditProperties";
    export * from "components/calc/payroll/PayrollEditValidate";
    export * from "components/calc/payroll/PayrollList";
    export * from "components/calc/payroll/PayrollPayButton";
}
declare module "components/calc/pro/CalcPro" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * The **salaxy.ng1.components.CalcPro** is the generic simplified Angular-based salary calculator implementation for advanced users.
     * It shows the basic features required for a salary calculator user interfaces:
     * 1. Setting the Worker for the calculation
     * 2. Defining the Work Framework and other information about the work
     * 3. Defining the Salary and the benefits etc.
     * 5. Showing the results as reports for employer, worker and society.
     *
     * Controllers are defined in **salaxy.ng1.controllers**. Most of the functionality comes from Calculator2016Controller.
     * The views can be modified separately for each component using the templateUrl. See documentation for details.
     *
     * @componenttag salaxy-calc-pro
     * @example
     * ```html
     * <salaxy-calc-pro></salaxy-calc-pro>
     * ```
     */
    export class CalcPro extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/CalcProDetailsSalary" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the salary detail view for the Pro Calculator
     *
     * @componenttag salaxy-calc-pro-details-salary
     * @example
     * ```html
     * <salaxy-calc-pro-details-salary></salaxy-calc-pro-details-salary>
     * ```
     */
    export class CalcProDetailsSalary extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/CalcProDetailsWork" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the Work/Info/Framework detail view for the Pro Calculator
     *
     * @componenttag salaxy-calc-pro-details-work
     * @example
     * ```html
     * <salaxy-calc-pro-details-work></salaxy-calc-pro-details-work>
     * ```
     */
    export class CalcProDetailsWork extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/CalcProDetailsWorker" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the worker detail view for the Pro Calculator
     *
     * @componenttag salaxy-calc-pro-details-worker
     * @example
     * ```html
     * <salaxy-calc-pro-details-worker></salaxy-calc-pro-details-worker>
     * ```
     */
    export class CalcProDetailsWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/CalcProPaidSalary" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the worker and payment information when the selected payment is paid
     *
     * @componenttag salaxy-calc-paid-salary
     * @example
     * ```html
     * <salaxy-calc-paid-salary></salaxy-calc-paid-salary>
     * ```
     */
    export class CalcProPaidSalary extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/CalcProResults" {
    import { Calculator2016Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the simplified results view for the Calculator and the Pro Calculator
     *
     * @componenttag salaxy-calc-pro-results
     * @example
     * ```html
     * <salaxy-calc-pro-results></salaxy-calc-pro-results>
     * ```
     */
    export class CalcProResults extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2016Controller */
        controller: typeof Calculator2016Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/pro/index" {
    export * from "components/calc/pro/CalcPro";
    export * from "components/calc/pro/CalcProDetailsSalary";
    export * from "components/calc/pro/CalcProDetailsWork";
    export * from "components/calc/pro/CalcProDetailsWorker";
    export * from "components/calc/pro/CalcProPaidSalary";
    export * from "components/calc/pro/CalcProResults";
}
declare module "components/calc/Calc2018" {
    import { Calculator2018Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * The **salaxy.ng1.components.Calc** is the generic Angular-based salary calculator implementation.
     * It shows the basic features required for a salary calculator user interfaces:
     * 1. Setting the Worker for the calculation
     * 2. Defining the Work Framework and other information about the work
     * 3. Defining the Salary and the benefits
     * 4. Defining Expenses, benefits, household subsidies etc.
     * 5. Showing the results as charts and reports.
     *
     * Controllers are defined in **salaxy.ng1.controllers**. Most of the functionality comes from Calculator2018Controller.
     * The views can be modified separately for each component using the templateUrl. See documentation for details.
     *
     * @componenttag salaxy-calc-2018
     * @example
     * ```html
     * <salaxy-calc-2018></salaxy-calc-2018>
     * ```
     */
    export class Calc2018 extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the Calculator2018Controller */
        controller: typeof Calculator2018Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcDetails" {
    import { Calculator2018Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a details view of a single Calculation: Component has both the Classic and Pro calculators.
     *
     * @componenttag salaxy-calc-details
     * @example
     * ```html
     * <salaxy-calc-details></salaxy-calc-details>
     * ```
     */
    export class CalcDetails extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * By default it includes Add new and Delete buttons in Bootstrap .pull-right div element
         */
        transclude: {
            header: string;
        };
        /** Uses the Calculator2018Controller */
        controller: typeof Calculator2018Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/calculation-group/CalcGroupEdit" {
    import { CalculationGroupController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For CalcGroupEdit component, implements the fast / simple grid input.
     *
     * @componenttag salaxy-calc-group-edit
     * @example
     * ```html
     * <salaxy-calc-group-edit></salaxy-calc-group-edit>
     * ```
     */
    export class CalcGroupEdit extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the CalculationGroupController */
        controller: typeof CalculationGroupController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/calculation-group/CalcGroupEditHeader" {
    import { CalculationGroupController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For CalcGroupEdit component, implements the fast / simple grid input.
     *
     * @componenttag salaxy-calc-group-edit-header
     * @example
     * ```html
     * <salaxy-calc-group-edit-header></salaxy-calc-group-edit-header>
     * ```
     */
    export class CalcGroupEditHeader extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the CalculationGroupController */
        controller: typeof CalculationGroupController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/calculation-group/CalcGroupEditProperties" {
    import { CalculationGroupController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * For CalcGroupEdit component, implements the general properties / metadata editor.
     *
     * @componenttag salaxy-calc-group-edit-properties
     * @example
     * ```html
     * <salaxy-calc-group-edit-properties></salaxy-calc-group-edit-properties>
     * ```
     */
    export class CalcGroupEditProperties extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the CalculationGroupController */
        controller: typeof CalculationGroupController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/calculation-group/index" {
    export * from "components/calc/calculation-group/CalcGroupEdit";
    export * from "components/calc/calculation-group/CalcGroupEditHeader";
    export * from "components/calc/calculation-group/CalcGroupEditProperties";
}
declare module "components/calc/CalcIrRows" {
    import { CalcIrRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Editor for Incomes Register (Tulorekisteri) rows from Calculation.
     *
     * @componenttag salaxy-calc-ir-rows
     * @example
     * ```html
     * <salaxy-calc-ir-rows></salaxy-calc-ir-rows>
     * ```
     */
    export class CalcIrRows extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the CalcIrRowsController */
        controller: typeof CalcIrRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcList" {
    import { CalculationsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of calculations
     *
     * @componenttag salaxy-calc-list
     * @example
     * ```html
     * <salaxy-calc-list status-list="draft,paymentSucceeded" limit-to="5"></salaxy-calc-list>
     * ```
     */
    export class CalcList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * Default filter text search.
         * These are positioned to the right side of the table header (thead).
         * Mobile header and filter are outside of the table.
         */
        transclude: {
            header: string;
            filter: string;
            mobileheader: string;
            mobilefilter: string;
        };
        /** Uses the CalculationsController */
        controller: typeof CalculationsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcWorktime" {
    import { CalcWorktimeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for setting the Worktime properties within the calculator
     * The process starts by selecting the period based on which relevatn worktime
     * data is fetched: Holidays, Absences and later Hours.
     * Calculation rows are then added based on this worktime.
     *
     * @componenttag salaxy-calc-worktime
     * @example
     * ```html
     * <salaxy-calc-worktime calc="$ctrl.currentCalc"></salaxy-calc-worktime>
     * ```
     */
    export class CalcWorktime extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** The calculation that the component edits (shows in read-only mode). */
            calc: string;
        };
        /** Uses the CalcWorktimeController */
        controller: typeof CalcWorktimeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcRow" {
    import { CalcRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Detail / Editor view for a single row in a salary calculation.
     *
     * @componenttag salaxy-calc-row
     * @example
     * ```html
     * <salaxy-calc-row ng-model="$ctrl.currentCalc.rows[0]"></salaxy-calc-row>
     * ```
     */
    export class CalcRow extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Function that is called when an item is deleted.
             * The event is intended for user interface logic after delete (promise resolve)
             * and potentially when waiting for server to respond (from function call until promise resolve).
             * It is not meant for delete validation and/or for preventing deletion.
             * If onDelete is not specified, the browser is redirected to listUrl if specified.
             * NOTE: the deleteResult should basically always be Promise<true> or promise failure. Promise<false> does not really make sense here.
             * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
             */
            onDelete: string;
            /**
             * CalcRow-component calls this even whent user closes the dialog (either cancel or OK).
             * Users of the dialog must handle the event to hide the dialog.
             */
            onClose: string;
        };
        /** Uses the CalcRowsController */
        controller: typeof CalcRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcRow2019" {
    import { WorkerCalcRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Detail / Editor view for a single row in a salary calculation.
     *
     * @componenttag salaxy-calc-row-2019
     * @example
     * ```html
     * <salaxy-calc-row-2019 ng-model="$ctrl.currentCalc.rows[0]"></salaxy-calc-row-2019>
     * ```
     */
    export class CalcRow2019 extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Function that is called when an item is deleted.
             * The event is intended for user interface logic after delete (promise resolve)
             * and potentially when waiting for server to respond (from function call until promise resolve).
             * It is not meant for delete validation and/or for preventing deletion.
             * If onDelete is not specified, the browser is redirected to listUrl if specified.
             * NOTE: the deleteResult should basically always be Promise<true> or promise failure. Promise<false> does not really make sense here.
             * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
             */
            onDelete: string;
            /**
             * CalcRow-component calls this even whent user closes the dialog (either cancel or OK).
             * Users of the dialog must handle the event to hide the dialog.
             */
            onClose: string;
        };
        /** Uses the WorkerCalcRowsController */
        controller: typeof WorkerCalcRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcRowsEditor" {
    import { CalcRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a grid-type rows editor for addding calculation rows: Salaries, expenses, benefits etc.
     *
     * @componenttag salaxy-calc-rows-editor
     * @example
     * ```html
     * <salaxy-calc-rows-editor categories="['expenses']"
     *     title="Kulukorvaukset"></salaxy-calc-rows-editor>
     * ```
     */
    export class CalcRowsEditor extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Title for the grid table. */
            title: string;
            /** If set, filters the rows based on categories (plus rowTypes if set) */
            categories: string;
            /** If set, shows only these types (plus categories if set) */
            rowTypes: string;
            /**
             * If set, this is the list of rows that is being edited.
             * This means that by default the editor is in read-only mode. When the user clicks edit,
             * a virtual calculation is created (for current worker, this cannot be overriden yet)
             * and the controller is bound to its calculation rows.
             */
            rows: string;
            /**
             * Function that is called when user selects an item in the list.
             * The selected item is the parameter of the function call.
             * @example <salaxy-calc-rows-editor on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
             */
            onListSelect: string;
            /**
             * Function that is called when an item is deleted.
             * The event is intended for user interface logic after delete (promise resolve)
             * and potentially when waiting for server to respond (from function call until promise resolve).
             * It is not meant for delete validation and/or for preventing deletion.
             * If onDelete is not specified, the browser is redirected to listUrl if specified.
             * NOTE: the deleteResult should basically always be Promise<true> or promise failure. Promise<false> does not really make sense here.
             * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
             */
            onDelete: string;
            /**
             * Function that is called after a new item has been created.
             * At this point, the item has been created, but not yet selected as current.
             * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
             * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc"></salaxy-payroll-list>
             */
            onCreateNew: string;
        };
        /** Uses the CalcRowsController */
        controller: typeof CalcRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CalcRows2019Editor" {
    import { WorkerCalcRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a grid-type rows editor for addding calculation rows: Salaries, expenses, benefits etc.
     *
     * @componenttag salaxy-calc-rows-2019-editor
     * @example
     * ```html
     * <salaxy-calc-rows-2019-editor categories="['expenses']"
     *     title="Kulukorvaukset"></salaxy-calc-rows-2019-editor>
     * ```
     */
    export class CalcRows2019Editor extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Title for the grid table. */
            title: string;
            /** If set, filters the rows based on categories (plus rowTypes if set) */
            categories: string;
            /** If set, shows only these types (plus categories if set) */
            rowTypes: string;
            /**
             * If set, this is the list of rows that is being edited.
             * This means that by default the editor is in read-only mode. When the user clicks edit,
             * a virtual calculation is created (for current worker, this cannot be overriden yet)
             * and the controller is bound to its calculation rows.
             */
            rows: string;
            /**
             * Function that is called when user selects an item in the list.
             * The selected item is the parameter of the function call.
             * @example <salaxy-calc-rows-editor on-list-select="$ctrl.myCustomSelectFunc(item)"></salaxy-payroll-list>
             */
            onListSelect: string;
            /**
             * Function that is called when an item is deleted.
             * The event is intended for user interface logic after delete (promise resolve)
             * and potentially when waiting for server to respond (from function call until promise resolve).
             * It is not meant for delete validation and/or for preventing deletion.
             * If onDelete is not specified, the browser is redirected to listUrl if specified.
             * NOTE: the deleteResult should basically always be Promise<true> or promise failure. Promise<false> does not really make sense here.
             * @example <salaxy-payroll-list on-delete="$ctrl.resetUiAfterDeleteFunc"></salaxy-payroll-list>
             */
            onDelete: string;
            /**
             * Function that is called after a new item has been created.
             * At this point, the item has been created, but not yet selected as current.
             * If onCreateNew is not specified the browser is redirected to detailsUrl if specified and if not, only current item is set.
             * @example <salaxy-payroll-list on-create-new="$ctrl.startMyCustomWizardFunc"></salaxy-payroll-list>
             */
            onCreateNew: string;
        };
        /** Uses the WorkerCalcRowsController */
        controller: typeof WorkerCalcRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/CreditTransfer" {
    import { CreditTransferController } from "controllers/CreditTransferController";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Credit Transfer Form for SEPA euro payments.
     *
     * @componenttag salaxy-credit-transfer
     * @example
     * ```html
     * <salaxy-credit-transfer payment="$ctrl.current"></salaxy-credit-transfer>
     * ```
     */
    export class CreditTransfer extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            payment: string;
        };
        /** Uses the CreateCompanyAccountWizardController */
        controller: typeof CreditTransferController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/calc/index" {
    export * from "components/calc/classic/index";
    export * from "components/calc/payroll/index";
    export * from "components/calc/pro/index";
    export * from "components/calc/Calc2018";
    export * from "components/calc/CalcDetails";
    export * from "components/calc/calculation-group/index";
    export * from "components/calc/CalcIrRows";
    export * from "components/calc/CalcList";
    export * from "components/calc/CalcWorktime";
    export * from "components/calc/CalcRow";
    export * from "components/calc/CalcRow2019";
    export * from "components/calc/CalcRowsEditor";
    export * from "components/calc/CalcRows2019Editor";
    export * from "components/calc/CreditTransfer";
}
declare module "components/helpers/Alert" {
    import { AlertController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows an alert box with styling.
     * Based on Bootstrap alert, but has the following additional functionality:
     *
     * - primary style (in addition to success, info, warning and danger) for banner type of advertising
     * - isDismissable for close alert button
     *   - Currently, for session only
     *   - Future implementation is expected to remember dismissed alerts in browser storage.
     * - Icon for better visuality
     * - Optional "Read more" area
     *
     * @componenttag salaxy-avatar
     * @example
     * ```html
     * <salaxy-avatar ></salaxy-avatar>
     * ```
     */
    export class Alert extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Possibility to speicfy a font-awesome icon.
             * Setting "none", will show no icon.
             * If not set, it is determined by type.
             */
            icon: string;
            /** Type of the alert is the Bootstrap style: Note that also "primary" and "default" are supported. */
            type: string;
            /**
             * Alert main content as simple text.
             * You can alternatively provide html as main element.
             */
            text: string;
        };
        /**
         * Transclusion slots
         */
        transclude: {
            /**
             * The main content of the alert.
             * You can alternatively provide a simple text in "text" property.
             */
            main: string;
            /**
             * Optional Details part of the alert.
             * Will automatically show "Read more" button.
             */
            aside: string;
        };
        /** Uses the AvatarController */
        controller: typeof AlertController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/Avatar" {
    import { AvatarController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the Palkkaus Avatar image: This is either an image or a font-icon with a user specific color/initilas combination.
     *
     * @componenttag salaxy-avatar
     * @example
     * ```html
     * <salaxy-avatar avatar="$ctrl.myAvatar"></salaxy-avatar>
     * ```
     */
    export class Avatar extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** The Avatar object that should be rendered */
            avatar: string;
        };
        /** Uses the AvatarController */
        controller: typeof AvatarController;
        /** The default template for the component. */
        defaultTemplate: string;
        /** Rendered HTML replaces the original element */
        replace: true;
    }
}
declare module "components/helpers/Calendar" {
    import { CalendarController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a monthly calendar where one month is one row and you can show ranges and special days (only partial support).
     * Future implementations will support categories / swimlanes of periods (e.g. holidays from several persons)
     * and have better support for special days: different types of markers, pop-up etc.
     *
     * @componenttag salaxy-calendar
     * @example
     * ```html
     * <salaxy-calendar start="'2019-01-5'" end="$ctrl.getToday()" periods="$ctrl.getPeriods()"></salaxy-chart>
     * ```
     */
    export class Calendar extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Start date for the calendar as ISO date yyyy-MM-dd. Renders from the beginning of this month.
             */
            start: string;
            /**
             * End date of the calendar as ISO date yyyy-MM-dd. Renders until the end of this month.
             */
            end: string;
            /** Periods / date ranges plotted on the chart. */
            periods: string;
            /** Single dates that are plotted on the chart. */
            days: string;
            /** Type of chart: "align-weekdays" or "align-left" (not yet implemented) */
            model: string;
            /**
             * Function that is called when user selects an item in the calendar.
             * Function can have the following locals: type: "period" | "day" | "emptyDay",  date: string, selectedItem: number
             * @example <salaxy-calendar on-list-select="$ctrl.myCustomSelectFunc(type, date, selectedItem)"></salaxy-calendar>
             */
            onListSelect: string;
        };
        /** Uses the CalendarController */
        controller: typeof CalendarController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/Chart" {
    import { ChartController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a angular-chart.js with Salaxy defaults, caching, colors etc.
     * Basic use is to define a type and getChart() function that returns object with data and labels (plus other options and proiperties as neeed).
     * Caching watches for changes in type and data property returned by getChart()
     * so if you make changes to other properties after first rendering, make sure to call $ctrl.refresh().
     * However, the getChart() method is called only every 1 second.
     *
     * @componenttag salaxy-chart
     * @example
     * ```html
     * <salaxy-chart type="pie" on-get-chart="$ctrl.getChart()"></salaxy-chart>
     * ```
     */
    export class Chart extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Function that returns an object with data, options and all the other properties for the chart.
             * The method call is cached first for 1 second and then data is bound to Angular UI only if the data property changes (to avoid flickering etc.)
             * If you change the other properties without changing the data, call the refresh() method to force cache refresh.
             */
            onGetChart: string;
            /** Type of chart: "line" (default), "bar", "doughnut", "radar", "pie", "polarArea", "horizontalBar" or "bubble" */
            type: string;
        };
        /** Uses the ChartController */
        controller: typeof ChartController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/Import" {
    import { ImportController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UNDERCON: This is a PROTOTYPE implementation, not ready for production use.
     *
     * Generic import data component.
     * At the moment, it is unclear whether we will have one generic component or case specific import components for Workers, Calculations, Payroll etc.
     *
     * @componenttag salaxy-import
     * @example
     * ```html
     * <salaxy-import></salaxy-import>
     * ```
     */
    export class Import extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the ImportController */
        controller: typeof ImportController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/NaviSitemap" {
    import { NaviController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Helper component to generate navigation components and views:
     * Top- and side-menus, paths and controls that show the current title.
     * These controls take the navigation logic from an object (sitemap) and are aware of current node / open page on that sitemap.
     *
     *
     * NOTE: This is just an optional helper to make creating simple demo sites easier.
     * There is no need to use NaviService, NaviController or components in your custom site!
     * You can use something completely different.
     *
     * @componenttag salaxy-login-button
     * @example
     * ```html
     * <salaxy-navi-sitemap></salaxy-navi-sitemap>
     * ```
     */
    export class NaviSitemap extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * - mode: "default" or "accordion"
             * - default: shows full two levels of the tree. Typically used as sitemap in the content area.
             * - accordion: shows first level of the navi tree and second level only if is in the current path.
             * Typically used in left menu navigation.
             */
            mode: string;
        };
        /** Uses the NaviController */
        controller: typeof NaviController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/NaviStarterLayout" {
    import { NaviController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Helper component to easily create a web site that is responsive and usable.
     * The starter contains:
     *
     * - Customizable Header with title
     * - Left menu with logo, authentication info / login button and sitemap
     * - Mobile version with simple header: Title + Hamburger button that shows the menu, which is hidden by default.
     * - Loader message / animation
     * - Content area as ng-view
     * - Alert container
     *
     *
     * NOTE: This is just an optional helper to make creating simple (demo) sites easier.
     * There is no need to use NaviStarterLayout, NaviService, NaviController or components in your custom site!
     * You can use something completely different.
     *
     * @componenttag salaxy-navi-starter-layout
     * @example
     * ```html
     * <salaxy-navi-starter-layout>
     *   <header ng-controller="NaviController as navi">
     *     Your html here: {{ navi.title }}
     *   </header>
     *   <main ng-controller="NaviController as navi">
     *   	<div ng-class="navi.current.isFullWidth ? 'container-fluid' : 'container'" ng-view autoscroll="true"></div>
     *   </main>
     * </salaxy-navi-starter-layout>
     * ```
     */
    export class NaviStarterLayout extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * - mode: "default" or "accordion"
             * - default: shows full two levels of the tree. Typically used as sitemap in the content area.
             * - accordion: shows first level of the navi tree and second level only if is in the current path.
             * Typically used in left menu navigation.
             */
            mode: string;
        };
        /**
         * Component may contain header and main tags to
         * override the default rendering of the header (non-mobile) and main content area respectively.
         */
        transclude: {
            header: string;
            main: string;
        };
        /** Uses the NaviController */
        controller: typeof NaviController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/Spinner" {
    import { AvatarController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Spinner as a UI component
     *
     * @componenttag salaxy-spinner
     * @example
     * ```html
     * <salaxy-spinner salaxy-if-role="init" full-screen="true" heading="Loading..." text="Please wait."></salaxy-spinner>
     * ```
     */
    export class Spinner extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** If true, the spinner is shown full screen. */
            fullScreen: string;
            /**
             * Heading is the larger text under the spinner.
             * The text is translated.
             */
            heading: string;
            /**
             * Small text - use pre for line breaks.
             * The text is translated.
             */
            text: string;
        };
        /** Uses the AvatarController */
        controller: typeof AvatarController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/helpers/index" {
    export * from "components/helpers/Alert";
    export * from "components/helpers/Avatar";
    export * from "components/helpers/Calendar";
    export * from "components/helpers/Chart";
    export * from "components/helpers/Import";
    export * from "components/helpers/NaviSitemap";
    export * from "components/helpers/NaviStarterLayout";
    export * from "components/helpers/Spinner";
}
declare module "components/communications/CmsArticle" {
    import { CmsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders an article from the Salaxy Content Management System
     * Also render the Heading and the author information and other metadata.
     *
     * @componenttag salaxy-cms-article
     * @example
     * ```html
     * <salaxy-cms-article></salaxy-cms-article>
     * ```
     */
    export class CmsArticle extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Id that specifies which article to be shown */
            id: string;
        };
        /** Uses the CmsController */
        controller: typeof CmsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/CmsList" {
    import { CmsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a list of articles from a specific category from the Salaxy Content Management System
     *
     * @componenttag salaxy-cms-list
     * @example
     * ```html
     * <salaxy-cms-list section="palkkausInstructions"></salaxy-cms-list>
     * ```
     */
    export class CmsList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Name of the category that specifies which articles to be shown */
            section: string;
        };
        /** Uses the CmsController */
        controller: typeof CmsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/EmailMessage" {
    import { EmailMessageController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current email
     *
     * - mailTo: forced email To address, also disables drop down selection box.
     *
     * @componenttag salaxy-email-message
     * @example
     * ```html
     * <salaxy-email-message></salaxy-email-message>
     * ```
     */
    export class EmailMessage extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /** Uses the EmailMessageController */
        controller: typeof EmailMessageController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/EmailMessageList" {
    import { EmailMessageController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of emails
     *
     * @componenttag salaxy-email-list
     * @example
     * ```html
     * <salaxy-email-list></salaxy-email-list>
     * ```
     */
    export class EmailMessageList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the EmailMessageController */
        controller: typeof EmailMessageController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/FeedbackForm" {
    import { MessageController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a feedback form that can be used by an authenticated user to send a question to Salaxy customer service.
     *
     * @componenttag salaxy-feedback-form
     * @example
     * ```html
     * <salaxy-feedback-form email-title="Feedback from customer support pages" button-text="Send feedback"></salaxy-feedback-form>
     * ```
     */
    export class FeedbackForm extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** - emailTitle: - a title to the email, for example "Feedback from customer support pages" */
            emailTitle: string;
            /** - buttonText: - text in the submit button of the form */
            buttonText: string;
            /** - alertText: - Message that is shown through the Alert service after something goes wrong with the form */
            alertText: string;
            /** - onSuccess: - Message that is shown through the Alert service after the message has been successfully sent. Leave this null to not show any message */
            onSuccess: string;
        };
        /** Uses the MessageController */
        controller: typeof MessageController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/SmsMessage" {
    import { SmsMessageController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Send a SMS
     *
     * @componenttag salaxy-sms-message
     * @example
     * ```html
     * <salaxy-sms-message></salaxy-sms-message>
     * ```
     */
    export class SmsMessage extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the SmsMessageController */
        controller: typeof SmsMessageController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/SmsMessageList" {
    import { SmsMessageController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of sms messages.
     *
     * @componenttag salaxy-sms-message-list
     * @example
     * ```html
     * <salaxy-sms-message-list></salaxy-sms-message-list>
     * ```
     */
    export class SmsMessageList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the SmsMessageController */
        controller: typeof SmsMessageController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/Tutorials" {
    import { CmsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows links to Tutorials and Helpers => Main entry point to Palkkaus.fi ABC
     * when not shown as a separate page.
     *
     * @componenttag salaxy-tutorials
     * @example
     * ```html
     * <salaxy-tutorials></salaxy-tutorials>
     * ```
     */
    export class Tutorials extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the WelcomeController */
        controller: typeof CmsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/Welcome" {
    import { WelcomeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail a Welcome message depending on the user profile.
     * Typically the first component on the front page.
     *
     * @componenttag salaxy-welcome
     * @example
     * ```html
     * <salaxy-welcome></salaxy-welcome>
     * ```
     */
    export class Welcome extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * If set to true, the settings are handled by proxy:
             * we do not ask the user to change the settings in
             * the Welcome screen (mainly Pension or Insurance).
             */
            settingsByProxy: string;
        };
        /** Uses the WelcomeController */
        controller: typeof WelcomeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/communications/index" {
    export * from "components/communications/CmsArticle";
    export * from "components/communications/CmsList";
    export * from "components/communications/EmailMessage";
    export * from "components/communications/EmailMessageList";
    export * from "components/communications/FeedbackForm";
    export * from "components/communications/SmsMessage";
    export * from "components/communications/SmsMessageList";
    export * from "components/communications/Tutorials";
    export * from "components/communications/Welcome";
}
declare module "components/contact/Contact" {
    import { ContactController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current employment contact
     *
     * @componenttag salaxy-contact
     * @example
     * ```html
     * <salaxy-contact></salaxy-contact>
     * ```
     */
    export class Contact extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the ContracController */
        controller: typeof ContactController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/contact/ContactList" {
    import { ContactController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of contacts
     *
     * @componenttag salaxy-contact-list
     * @example
     * ```html
     * <salaxy-contact-list></salaxy-contact-list>
     * ```
     */
    export class ContactList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the ContactController */
        controller: typeof ContactController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/contact/index" {
    export * from "components/contact/Contact";
    export * from "components/contact/ContactList";
}
declare module "components/contract/Contract" {
    import { ContractController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current employment contract
     *
     * @componenttag salaxy-contract
     * @example
     * ```html
     * <salaxy-contract></salaxy-contract>
     * ```
     */
    export class Contract extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the ContracController */
        controller: typeof ContractController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/contract/ContractList" {
    import { ContractController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of contracts
     *
     * @componenttag salaxy-contract-list
     * @example
     * ```html
     * <salaxy-contract-list></salaxy-contract-list>
     * ```
     */
    export class ContractList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the ContractController */
        controller: typeof ContractController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/contract/index" {
    export * from "components/contract/Contract";
    export * from "components/contract/ContractList";
}
declare module "components/form-controls/Datepicker" {
    import { DatepickerController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Modified ui.bootstrap.datepicker.
     * The component can be bound to a string typed variable. The standard ui.bootstrap.datepicker can be bound successfully only to a Date variable.
     *
     * @componenttag salaxy-datepicker
     * @example
     * ```html
     * <salaxy-datepicker name="myDatepicker" ng-model="temp" label="Basic datepicker"></salaxy-datepicker>
     * ```
     */
    export class Datepicker extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            /**
             * The following component properties (attributes in HTML) are bound to the Controller.
             * For detailed functionality, refer to [controller](#controller) implementation.
             */
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** options for ui.bootstrap.datepicker */
            datepickerOptions: string;
        };
        /** Uses the DatepickerController */
        controller: typeof DatepickerController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/DatepickerPopup" {
    import { DatepickerPopupController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Modified ui.bootstrap.datepickerPopup.
     * The component can be bound to a string typed variable. The standard ui.bootstrap.datepickerPopup can be bound successfully only to a Date variable.
     *
     * @componenttag salaxy-datepicker-popup
     * @example
     * ```html
     *
     * <salaxy-datepicker-popup name="myDatepickerPopup" ng-model="temp" label="Basic datepicker popup" label-placement="top"></salaxy-datepicker-popup>
     * ```
     */
    export class DatepickerPopup extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** options for ui.bootstrap.datepicker */
            datepickerOptions: string;
            /** popup placement:  */
            popupPlacement: string;
            /** If 'top', the label is placed on it's own row before input */
            labelPlacement: string;
            /** Format for the date, default is d.M.yyyy */
            format: string;
            /**
             * If set to true, sets datepicker-append-to-body to false.
             * By default we set it to true in Salaxy framework,
             * but you may want to set it back to false in e.g. Modal dialogs.
             */
            appendInline: string;
        };
        /** Uses the DatepickerController */
        controller: typeof DatepickerPopupController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/DateRange" {
    import { DateRangeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Provides a user interface for picking up a date range
     * and optionally specifying also the number of working days within that range.
     *
     * @componenttag salaxy-date-range
     * @example
     * ```html
     * <salaxy-date-range name="myDateRange" ng-model="temp" label="Sample date range"></salaxy-date-range>
     * ```
     */
    export class DateRange extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string; /** ngModel is required */
            label: string;
            required: string;
            readOnly: string;
            /** Options for ui.bootstrap.datepicker */
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string; /**
             * Fires an event when the model is changing: Any of the values specific to the model are changing: start, end or daysCount.
             * This should typically used instead of ng-change because ng-change will only fire if the object reference changes.
             * On-change fires when dates or day count changes and this is typically what you are looking for.
             * @example <salaxy-date-range ng-model="$ctrl.dateRange" on-change="$ctrl.dateRangeChange(value)"></salaxy-date-range>
             */
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** Options for ui.bootstrap.datepicker */
            datepickerOptions: string;
            /**
             * Label for the scondary input (Default: SALAXY.NG1.DateRange.labelDaysCount).
             * Translation is attempted.
             */
            labelDaysCount: string;
            /**
             * Minimum available date.
             * Bindable and ISO string version of the datepicker-options.minDate.
             */
            minDate: string;
            /**
             * Maximum available date.
             * Bindable and ISO string version of the datepicker-options.maxDate.
             */
            maxDate: string;
            /** Supported modes are "range" (default) and "multiple" for selecting individual days. */
            mode: string;
            /**
             * Fires an event when the model is changing: Any of the values specific to the model are changing: start, end or daysCount.
             * This should typically used instead of ng-change because ng-change will only fire if the object reference changes.
             * On-change fires when dates or day count changes and this is typically what you are looking for.
             * @example <salaxy-date-range ng-model="$ctrl.dateRange" on-change="$ctrl.dateRangeChange(value)"></salaxy-date-range>
             */
            onChange: string;
        };
        /** Uses the DateRangeController */
        controller: typeof DateRangeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/FormGroup" {
    import { FormGroupController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Helper for rendering the HTML for FormGroup:
     * This component renders only the label - input html.
     * It does not do any of the real form-control logic like ng-model, validations etc. (see salaxy-input for that).
     * The "input" part of the form group may be a non form control - e.g. just a text.
     * Also the label may be hidden.
     *
     * @componenttag salaxy-form-group
     * @example
     * ```html
     * <salaxy-form-group label="Some label"><p>Just text here</p></salaxy-form-group>
     * ```
     */
    export class FormGroup extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Name of the input - also used as id. */
            name: string;
            /** Label for the control */
            label: string;
            /** If true the field is required (shows an asterix) */
            required: string;
            /**
             * Positioning of the label of form-control.
             * Supported values are "horizontal" (default), "no-label", "plain", "basic" and "empty-label".
             * See FormGourpLabelType for details.
             */
            labelType: string;
            /**
             * Label columns expressed as Bootstrap grid columns.
             * Default is 'col-sm-4' for label-type: 'horizontal' and 'col-sm-12' for label-type: 'no-label'.
             * Other label-types do not have column classes at the moment.
             */
            labelCols: string;
        };
        /** Contents of the tag is the input element. */
        transclude: boolean;
        /** Uses the InputController */
        controller: typeof FormGroupController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/Input" {
    import { InputController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows an input control with label, validation error, info etc.
     *
     * Basic label-input control for forms.
     *
     * @componenttag salaxy-input
     * @example
     * ```html
     * <salaxy-input name="myInput" ng-model="temp" label="Basic example"></salaxy-input>
     * ```
     */
    export class Input extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** Minimum length for the text value, default is 0 (form validation) */
            minlength: string;
            /** Maximum length for the text value, default is 1024 (form validation) */
            maxlength: string;
            /** Regular expression pattern for validation (form validation) */
            pattern: string;
        };
        /** Uses the InputController */
        controller: typeof InputController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputBoolean" {
    import { InputBooleanController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a boolean input control with label, validation error, info etc.
     *
     * @componenttag salaxy-input-boolean
     * @example
     * ```html
     * <salaxy-input-boolean name="myInput" ng-model="temp" label="Basic example" type="radio"></salaxy-input>
     * ```
     */
    export class InputBoolean extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** Type of the input element. Options are checkbox, radio, select and switch */
            type: string;
            /** Text to show as a label for input with value FALSE
             * Supported by types radio and select
             */
            labelFalse: string;
            /** Text to show as a label for input with value TRUE
             * Supported by types radio, checkbox and select
             */
            labelTrue: string;
            /** TODO: Is this needed when there's support for 'empty-label' ?
             *  BS class for offsetting the input (no-label)
             */
            offsetCols: string;
        };
        /** Uses the InputEnumController */
        controller: typeof InputBooleanController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputEnum" {
    import { InputEnumController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Select component for enumerations.
     * @componenttag salaxy-input-enum
     * @example
     * ```html
     * <salaxy-input-enum type="select"  options="{FiOy:'Osakeyhtiö', FiTm:'Toiminimi'}"  name="CompanyType" enum="CompanyType" label="Company"></salaxy-input-enum>
     * <salaxy-input-enum type="radio"  name="CompanyType" enum="CompanyType" label="Company" hidden-options="unknown"></salaxy-input-enum>
     * <salaxy-input-enum type="typeahead"  name="CompanyType" enum="CompanyType" label="Company" hidden-options="unknown"></salaxy-input-enum>
     * ```
     */
    export class InputEnum extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            /**
             * These values are visible only if they are selected in the data.
             * I.e. after something else is selected, hidden value cannot be selected back.
             * Use for not-selected values ("Please choose...") when you do not want selection reversed
             * or legacy data that is not selectable, but may still exist on the server.
             */
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /**
             * Binds to an enumeration defined by the Salaxy API.
             * Set the name of the enumeration.
             */
            enum: string;
            /** TODO: not working well with Typeahead-element
             * Options of the select control as a key-value object.
             */
            options: string;
            /** Type of the input element. Options are typeahead, select and radio */
            type: string;
            /**
             * These values are visible only if they are selected in the data.
             * I.e. after something else is selected, hidden value cannot be selected back.
             * Use for not-selected values ("Please choose...") when you do not want selection reversed
             * or legacy data that is not selectable, but may still exist on the server.
             */
            hiddenOptions: string;
            /**
             * Array or comma separated string to filter the option values to just the given ones.
             * Also sets the order to this order, so works for ordering a given set of values.
             * Note that hiddenOptions is applied first, so if you want e.g. "undefined" to appear if selected,
             * you may add it here and it behaves as expected (undefined is still hidden if a value is selevted).
             */
            filter: string;
            /** If true, the control is not caching values. */
            disableCache: string;
        };
        /** Uses the InputEnumController */
        controller: typeof InputEnumController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputCalcRowType" {
    import { InputCalcRowTypeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Select component for Calculations row types.
     * @componenttag salaxy-calc-row-type-select
     * @example
     * ```html
     * <salaxy-calc-row-type-select name="rowType" ng-model="row.type" label="Basic datepicker"></salaxy-calc-row-type-select>
     * ```
     */
    export class InputCalcRowType extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** Type of the input element. Options are typeahead and list (select, search-list and radio may be supported later). */
            type: string;
            /** If set, filters the rows based on categories (plus rowTypes if set) */
            categories: string;
            /** If set, shows only these types (plus categories if set) */
            rowTypes: string;
        };
        /** Uses the InputCalcRowTypeController */
        controller: typeof InputCalcRowTypeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputIncomeType" {
    import { InputIncomeTypeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Visual selection for income types (and deductibles) resulting to number code in Incomes register.
     * @componenttag salaxy-input-income-type
     * @example
     * ```html
     * <salaxy-input-income-type name="code" ng-model="row.code" label="income type"></salaxy-input-income-type>
     * ```
     */
    export class InputIncomeType extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string; /** Uses the InputIncomeTypeController */
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** Type of the input element is either typehead or list. Default is typeahead. */
            type: string;
        };
        /** Uses the InputIncomeTypeController */
        controller: typeof InputIncomeTypeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputNumber" {
    import { InputNumberController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Input control for numbers.
     * @componenttag salaxy-input-number
     * @example
     * ```html
     * <salaxy-input-number name="count" ng-model="row.count" label="Number of items"></salaxy-input-number>
     * ```
     */
    export class InputNumber extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string; /** Uses the InputCalcRowTypeController */
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /**
             * Expression for Unit for the number.
             * If set, shows a visual clue of the unit.
             * For 'percent' editor value is multiplied by 100.
             */
            unit: string;
        };
        /** Uses the InputCalcRowTypeController */
        controller: typeof InputNumberController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputOccupationType" {
    import { InputOccupationTypeController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Select component for Occupation types.
     * @componenttag salaxy-occupation-type-select
     * @example
     * ```html
     * <salaxy-input-occupation-type default-list="household" name="occupationType" ng-model="row.occupation" label="occupationType"></salaxy-input-occupation-type>
     * <salaxy-input-occupation-type default-list="21490,21510,21532" name="occupationType" ng-model="row.occupation" label="occupationType"></salaxy-input-occupation-type>
     * ```
     */
    export class InputOccupationType extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /**
             * One or comma-separated list of occupation IDs, or a known keyword to define which occupations are shown
             * Supported keywords: 'household' and 'company'
             * If empty, shows all
             */
            defaultList: string;
        };
        /** Uses the InputOccupationTypeController */
        controller: typeof InputOccupationTypeController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/InputWorker" {
    import { InputWorkerController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Select component for Workers
     *
     * @componenttag salaxy-input-worker
     * @example
     * ```html
     * <salaxy-input-worker name="rowType" ng-model="row.type" label="Basic datepicker"></salaxy-input-worker>
     * ```
     */
    export class InputWorker extends ComponentBase {
        /** ngModel is required */
        require: {
            model: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            name: string;
            label: string;
            required: string;
            readOnly: string;
            readonly: string;
            disabled: string;
            labelType: string;
            labelCols: string;
            placeholder: string;
            tooltipHtml: string;
            tooltipPlacement: string;
            disableValidationErrors: string;
        } & {
            /** If true, will add the option to add a new Worker directly from the selection. */
            addNew: string;
        };
        /** Uses the InputWorkerController */
        controller: typeof InputWorkerController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/Switch" {
    import { SwitchController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a salaxy switch icon
     *
     * Please note that also the ng-model directive is required.
     *
     * @componenttag salaxy-switch
     * @example
     * ```html
     * <salaxy-switch xxx="tax"></salaxy-switch>
     * ```
     */
    export class Switch extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Name of the input - also used as id. */
            name: string;
            /** - id: */
            id: string;
            /** - onText: */
            onText: string;
            /** - offText: */
            offText: string;
            /** - disabled: */
            disabled: string;
        };
        /** ngModel is required */
        require: {
            model: string;
        };
        /** Uses the SwitchController */
        controller: typeof SwitchController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/Tab" {
    import { TabController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Component for a single tab pane in the tabs control.
     * Shows a navigation header and a tab pane.
     * Contains salaxy-tab-heading element for the tab header.
     * Contains salaxy-tab-content element for the tab pane content.
     * The disable attribute can be used to disable the tab.
     * The index attribute can be used to name the tab. The index
     * is used in setting the active tab in the salaxy-tabs element using the active attribute.
     *
     * @componenttag salaxy-tab
     * @example
     * ```html
     * <salaxy-tabs active="active">
     *   <salaxy-tab>
     *      <salaxy-tab-heading>Tab number one</salaxy-tab-heading>
     *      <salaxy-tab-content>
     *          <p>Tab one content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     *  <salaxy-tab index="'kolme'">
     *      <salaxy-tab-heading><i>Tab number two</i></salaxy-tab-heading>
     *      <salaxy-tab-content>
     *          <p>Tab two content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     *  <salaxy-tab disable="true" heading="Tab text number three">
     *      <salaxy-tab-content>
     *          <p>Tab three content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     * </salaxy-tabs>
     * ```
     */
    export class Tab extends ComponentBase {
        /** Require salaxy-tabs */
        require: {
            tabsCtrl: string;
        };
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Index/name for the tab. */
            index: string;
            /** Disable tab attribute. */
            disable: string;
            /** Text heading. */
            heading: string;
            /** Selection event handler */
            onSelect: string;
        };
        /** Uses the TabController */
        controller: typeof TabController;
        /** The default template for the component. */
        defaultTemplate: string;
        /** Transclusion */
        transclude: {
            content: string;
            heading: string;
        };
    }
}
declare module "components/form-controls/Tabs" {
    import { TabsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Component for a tabs control.
     * Shows navigation headers and tab panes.
     * Contains one or more salaxy-tab elements, each for one tab.
     * The active-attribute shows/sets the current active tab.
     *
     * @componenttag salaxy-tabs
     * @example
     * ```html
     * <salaxy-tabs active="active">
     *   <salaxy-tab>
     *      <salaxy-tab-heading>Tab number one</salaxy-tab-heading>
     *      <salaxy-tab-content>
     *          <p>Tab one content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     *  <salaxy-tab index="'kolme'">
     *      <salaxy-tab-heading><i>Tab number two</i></salaxy-tab-heading>
     *      <salaxy-tab-content>
     *          <p>Tab two content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     *  <salaxy-tab disable="true" heading="Tab text number three">
     *      <salaxy-tab-content>
     *          <p>Tab three content</p>
     *      </salaxy-tab-content>
     *  </salaxy-tab>
     * </salaxy-tabs>
     * ```
     */
    export class Tabs extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Expression for active tab in the tabset */
            active: string;
        };
        /** Uses the TabsController */
        controller: typeof TabsController;
        /** The default template for the component. */
        defaultTemplate: string;
        /** Transclusion */
        transclude: boolean;
    }
}
declare module "components/form-controls/ValidationSummary" {
    import { ValidationSummaryController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a validation summary user interface.
     * Currently, this component is used for showing the status of server-side validation,
     * but the idea is that later implementation may enable it for using also with client-side validation.
     *
     * @componenttag salaxy-validation-summary
     * @example
     * ```html
     * <salaxy-validation-summary api-validation="$ctrl.validationData">
     *       <span>Everything is OK!</span>
     * </salaxy-validation-summary>
     * ```
     */
    export class ValidationSummary extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** The server-side API-validation object that is displayed by this component. */
            apiValidation: string;
            /** HTML that should be shown when the bound validation data is null. */
            loadingHtml: string;
        };
        /** Inner HTML is shown in case object is valid. */
        transclude: boolean;
        /** Uses the ValidationSummaryController */
        controller: typeof ValidationSummaryController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/form-controls/index" {
    export * from "components/form-controls/Datepicker";
    export * from "components/form-controls/DatepickerPopup";
    export * from "components/form-controls/DateRange";
    export * from "components/form-controls/FormGroup";
    export * from "components/form-controls/Input";
    export * from "components/form-controls/InputBoolean";
    export * from "components/form-controls/InputEnum";
    export * from "components/form-controls/InputCalcRowType";
    export * from "components/form-controls/InputIncomeType";
    export * from "components/form-controls/InputNumber";
    export * from "components/form-controls/InputOccupationType";
    export * from "components/form-controls/InputWorker";
    export * from "components/form-controls/Switch";
    export * from "components/form-controls/Tab";
    export * from "components/form-controls/Tabs";
    export * from "components/form-controls/ValidationSummary";
}
declare module "components/settings/Insurance" {
    import { IfInsuranceWizardController } from "controllers/IfInsuranceWizardController";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shop in shop modal for getting Insurance
     *
     * @componenttag salaxy-insurance
     * @example
     * ```html
     * <salaxy-insurance></salaxy-insurance>
     * ```
     */
    export class Insurance extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the IfInsuranceWizardController */
        controller: typeof IfInsuranceWizardController;
        /** Uses the controller as aliasing */
        controllerAs: string;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/settings/PaymentSettings" {
    import { AccountController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Payment Settings
     *
     * @componenttag salaxy-payment
     * @example
     * ```html
     * <salaxy-payment-settings></salaxy-payment-settings>
     * ```
     */
    export class PaymentSettings extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the AccountController */
        controller: typeof AccountController;
        /** Default template is the view  */
        defaultTemplate: string;
    }
}
declare module "components/settings/Pension" {
    import { VarmaPensionWizardController } from "controllers/VarmaPensionWizardController";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shop in shop modal for getting Tyel or Yel.
     *
     * @componenttag salaxy-pension
     * @example
     * ```html
     * <salaxy-pension></salaxy-pension>
     * ```
     */
    export class Pension extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the PensionWizardController */
        controller: typeof VarmaPensionWizardController;
        /** Uses the controller as aliasing */
        controllerAs: string;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/settings/Pricing" {
    import { PricingController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Defines the partner, service and pricing model for the customer.
     *
     * @componenttag salaxy-pricing
     * @example
     * ```html
     * <salaxy-pricing></salaxy-pricing>
     * ```
     */
    export class Pricing extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Optional model for pricing model. If not set, the settings of the current account are used.
             */
            product: string;
        };
        /** Uses the PricingController */
        controller: typeof PricingController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/settings/ProductInfoHeaders" {
    import { AccountController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows infor headers for a product: short description and price
     *
     * @componenttag salaxy-product-info-headers
     * @example
     * ```html
     * <salaxy-product-info-headers></salaxy-product-info-headers>
     * ```
     */
    export class ProductInfoHeaders extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            showMoreInfoButton: string;
            close: string;
        };
        /** Uses the AccountController */
        controller: typeof AccountController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/settings/index" {
    export { Insurance } from "components/settings/Insurance";
    export { PaymentSettings } from "components/settings/PaymentSettings";
    export { Pension } from "components/settings/Pension";
    export { Pricing } from "components/settings/Pricing";
    export { ProductInfoHeaders } from "components/settings/ProductInfoHeaders";
}
declare module "components/worker/AbsencePeriods" {
    import { AbsencePeriodsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for the absences list (poissaolokirjanpito).
     *
     * @componenttag salaxy-absence-periods
     * @example
     * ```html
     * <salaxy-absence-periods></salaxy-absence-periods>
     * ```
     */
    export class AbsencePeriods extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            condensed: string;
            filterStart: string;
            filterEnd: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /** Uses the HolidayYearsController */
        controller: typeof AbsencePeriodsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerAssure" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Alternative component for adding Worker accounts.
     * Uses the AssureWorkerAccount API service that is currently reserved for partner access.
     *
     * @componenttag salaxy-worker-assure
     * @example
     * ```html
     * <salaxy-worker-assure></salaxy-worker-assure>
     * ```
     */
    export class WorkerAssure extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the WorkersController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerAbsences" {
    import { WorkerAbsencesController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for absences of a single worker.
     *
     * @componenttag salaxy-holiday-year-absences
     * @example
     * ```html
     * <salaxy-worker-absences></salaxy-worker-absences>
     * ```
     */
    export class WorkerAbsences extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the WorkerAbsencesController */
        controller: typeof WorkerAbsencesController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerCalculations" {
    import { CalculationsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's all salary calculations: drafts and paid.
     *
     * @componenttag salaxy-worker-calculations
     * @example
     * ```html
     * <salaxy-worker-calculations></salaxy-worker-calculations>
     * ```
     */
    export class WorkerCalculations extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the CalculationsController */
        controller: typeof CalculationsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerCalcRows" {
    import { WorkerCalcRowsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for the worker calc rows.
     *
     * @componenttag salaxy-worker-calc-rows
     * @example
     * ```html
     * <salaxy-worker-calc-rows></salaxy-worker-calc-rows>
     * ```
     */
    export class WorkerCalcRows extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            condensed: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /** Uses the HolidayYearsController */
        controller: typeof WorkerCalcRowsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerDetails" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's contact and payment information, for example phone number and bank account number.
     *
     * @componenttag salaxy-worker-details
     * @example
     * ```html
     * <salaxy-worker-details></salaxy-worker-details>
     * ```
     */
    export class WorkerDetails extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /** Uses the MessageController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerDetailsEdit" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's contact and payment information, for example phone number and bank account number.
     *
     * @componenttag salaxy-worker-details
     * @example
     * ```html
     * <salaxy-worker-details></salaxy-worker-details>
     * ```
     */
    export class WorkerDetailsEdit extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the MessageController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerInfo" {
    import { WorkerInfoController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows information about the Worker.
     * The component may be bound with ng-model to either WorkerAccount or Calculation.
     * If ng-model binding or if omitteds binds to WorkerService.current singleton.
     *
     * @componenttag salaxy-worker-info
     * @example
     * ```html
     * <salaxy-worker-info view-type="details" ng-model=""></salaxy-worker-info>
     * ```
     */
    export class WorkerInfo extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Type of info box */
            viewType: string;
            /**
             * Text (translated) that is shown if now worker is selected.
             * Default is "SALAXY.NG1.WorkerInfo.noSelection".
             */
            noSelectionText: string;
        };
        /** ngModel may be used */
        require: {
            model: string;
        };
        /** Uses the WorkerInfoController */
        controller: typeof WorkerInfoController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerList" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of workers for a particular employer
     *
     * @componenttag salaxy-worker-list
     * @example
     * ```html
     * <salaxy-worker-list limit-to="5"></salaxy-worker-list>
     * ```
     */
    export class WorkerList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * Default filter includes dropdown options and text search.
         * These are positioned to the right side of the table header (thead).
         * Mobile header and filter are outside of the table.
         */
        transclude: {
            header: string;
            filter: string;
            mobileheader: string;
            mobilefilter: string;
        };
        /** Uses the WorkersController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerSelect" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of workers for a particular employer and allows selecting one of them.
     *
     * @componenttag salaxy-worker-select
     * @example
     * ```html
     * <salaxy-worker-select></salaxy-worker-select>
     * ```
     */
    export class WorkerSelect extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Required controller. */
        require: {
            model: string;
        };
        /** Uses the WorkerSelectionController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerTaxCards2019" {
    import { TaxCard2019Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of current worker's all tax cards (latest and previous ones), that the current account owns.
     * Note that this listing is always complete for workers whose enityType is personCreatedByEmployer (in this case all the tax cards for this person have been added and are owned by the employer)
     * but with workers whose entityType is "person", the worker themselves might have added/uploaded tax cards, and these tax cards are owned by the worker, not the employer,
     * and therefore they are not visible in this list
     *
     * @componenttag salaxy-worker-tax-card-2019
     * @example
     * ```html
     * <salaxy-worker-tax-cards-2019 binding="self"></salaxy-worker-tax-cards-2019>
     * ```
     */
    export class WorkerTaxCards2019 extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** "self" for current account "currentWorker" (default) for selected worker account of the employer  */
            binding: string;
        };
        /** Uses the TaxCard2019Controller */
        controller: typeof TaxCard2019Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/WorkerTaxCard2019" {
    import { TaxCard2019Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current worker's latest tax card, and provides an edit component for a new tax card.
     * Set binding="self" to bind to current account (users own taxcards).
     *
     * @componenttag salaxy-worker-tax-card-2019
     * @example
     * ```html
     * <salaxy-worker-tax-card-2019 binding="self" is-in-edit-mode="true"></salaxy-worker-tax-card-2019>
     * ```
     */
    export class WorkerTaxCard2019 extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** "self" for current account "currentWorker" (default) for selected worker account of the employer  */
            binding: string;
        };
        /** Uses the TaxCard2019Controller */
        controller: typeof TaxCard2019Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/taxcard/WorkerTaxCard2019Info" {
    import { TaxCard2019Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current worker's latest tax card.
     * Set binding="self" to bind to current account (users own taxcards).
     *
     * @componenttag salaxy-worker-tax-card-2019-info
     * @example
     * ```html
     * <salaxy-worker-tax-card-2019-info></salaxy-worker-tax-card-2019-info>
     * ```
     */
    export class WorkerTaxCard2019Info extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** "self" for current account "currentWorker" (default) for selected worker account of the employer  */
            binding: string;
        };
        /** Uses the TaxCard2019Controller */
        controller: typeof TaxCard2019Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/taxcard/WorkerTaxCard2019Editor" {
    import { TaxCard2019Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the editor for worker's tax card
     * Set binding="self" to bind to current account (users own taxcards).
     *
     * @componenttag salaxy-worker-tax-card-2019-editor
     * @example
     * ```html
     * <salaxy-worker-tax-card-2019-editor  binding="self"></salaxy-worker-tax-card-2019-editor>
     * ```
     */
    export class WorkerTaxCard2019Editor extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** "self" for current account "currentWorker" (default) for selected worker account of the employer  */
            binding: string;
            /** Form validity binding as output. */
            form: string;
        };
        /** Uses the TaxCard2019Controller */
        controller: typeof TaxCard2019Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/holidays/HolidayYearAccrual" {
    import { HolidayYearAccrualController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for holiday accrual (lomapäivien kertymä) of the annual leave for a selected holiday period.
     *
     * @componenttag salaxy-holiday-year-accrual
     * @example
     * ```html
     * <salaxy-holiday-year-accrual parent="$ctrl.selectedYear"></salaxy-holiday-year-accrual>
     * ```
     */
    export class HolidayYearAccrual extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: import("../../../controllers/bases/ListControllerBaseBindings").ListControllerBaseBindings;
        /** Uses the HolidayYearsController */
        controller: typeof HolidayYearAccrualController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/holidays/HolidayYearHolidays" {
    import { HolidayYearHolidaysController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * UI for the planned holidays list (lomakirjanpito / lomakalenteri) for a selected holiday year.
     *
     * @componenttag salaxy-holiday-year-holidays
     * @example
     * ```html
     * <salaxy-holiday-year-holidays></salaxy-holiday-year-holidays>
     * ```
     */
    export class HolidayYearHolidays extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            condensed: string;
            filterStart: string;
            filterEnd: string;
            parent: string;
            onListSelect: string;
            onCreateNew: string;
        };
        /** Uses the HolidayYearHolidaysController */
        controller: typeof HolidayYearHolidaysController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/holidays/HolidayYearHourlySalaries" {
    import { HolidayYearHourlySalariesController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * User interface for the hourly entries that are the bases for annual leave payments.
     * Based on this, system makes a suggestion for daily mean salary that is the bases for HolidaySalary.
     *
     * @componenttag salaxy-holiday-year-hourly-salaries
     * @example
     * ```html
     * <salaxy-holiday-year-hourly-salaries parent="$ctrl.selectedYear"></salaxy-holiday-year-hourly-salaries>
     * ```
     */
    export class HolidayYearHourlySalaries extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: import("../../../controllers/bases/ListControllerBaseBindings").ListControllerBaseBindings;
        /** Uses the HolidayYearsController */
        controller: typeof HolidayYearHourlySalariesController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/holidays/HolidayYearPaid" {
    import { HolidayYearPaidController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * User interface for the annual leave payments: HolidayCompensation, HolidayBonus and HolidaySalary.
     * These payments are typically fetched from paid calculations automatically,
     * but may also be marked paid manually. Also, in client-side logic, payments are fetched
     * optionally from Draft calculations.
     *
     * @componenttag salaxy-holiday-year-paid
     * @example
     * ```html
     * <salaxy-holiday-year-paid parent="$ctrl.selectedYear"></salaxy-holiday-year-paid>
     * ```
     */
    export class HolidayYearPaid extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: import("../../../controllers/bases/ListControllerBaseBindings").ListControllerBaseBindings;
        /** Uses the HolidayYearsController */
        controller: typeof HolidayYearPaidController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/holidays/WorkerHolidays" {
    import { HolidayYearsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Viewing and modifying of Worker holidays (holiday years and related settings).
     *
     * @componenttag salaxy-worker-holidays
     * @example
     * ```html
     * <salaxy-worker-holidays></salaxy-worker-holidays>
     * ```
     */
    export class WorkerHolidays extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the HolidayYearsController */
        controller: typeof HolidayYearsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/worker/index" {
    export * from "components/worker/AbsencePeriods";
    export * from "components/worker/WorkerAssure";
    export * from "components/worker/WorkerAbsences";
    export * from "components/worker/WorkerCalculations";
    export * from "components/worker/WorkerCalcRows";
    export * from "components/worker/WorkerDetails";
    export * from "components/worker/WorkerDetailsEdit";
    export * from "components/worker/WorkerInfo";
    export * from "components/worker/WorkerList";
    export * from "components/worker/WorkerSelect";
    export * from "components/worker/WorkerTaxCards2019";
    export * from "components/worker/WorkerTaxCard2019";
    export * from "components/worker/taxcard/WorkerTaxCard2019Info";
    export * from "components/worker/taxcard/WorkerTaxCard2019Editor";
    export * from "components/worker/holidays/HolidayYearAccrual";
    export * from "components/worker/holidays/HolidayYearHolidays";
    export * from "components/worker/holidays/HolidayYearHourlySalaries";
    export * from "components/worker/holidays/HolidayYearPaid";
    export * from "components/worker/holidays/WorkerHolidays";
}
declare module "components/account/worker/AccountAuthorizationWorker" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Simple presentation of Salaxy account info: employer and/or worker data
     *
     * @componenttag salaxy-authorization-worker
     * @example
     * ```html
     * <salaxy-account-authorization-worker></salaxy-authorization-worker>
     * ```
     */
    export class AccountAuthorizationWorker extends ComponentBase {
        /** The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to controller implementation
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountAvatarEditWorker" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's own avatar information.
     *
     * @componenttag salaxy-account-avatar-edit-worker
     * @example
     * ```html
     * <salaxy-account-avatar-edit-worker></salaxy-account-avatar-worker>
     * ```
     */
    export class AccountAvatarEditWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountContactWorker" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's own contact information.
     *
     * @componenttag salaxy-account-contact-worker
     * @example
     * ```html
     * <salaxy-account-contact-worker></salaxy-account-contact-worker>
     * ```
     */
    export class AccountContactWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountDetailsWorker" {
    import { WorkersController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's contact and payment information, for example phone number and bank account number.
     *
     * @componenttag salaxy-account-details-worker
     * @example
     * ```html
     * <salaxy-account-details-worker></salaxy-account-details-worker>
     * ```
     */
    export class AccountDetailsWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /** Uses the MessageController */
        controller: typeof WorkersController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountDetailsEditWorker" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a view of worker's contact and payment information, for example phone number and bank account number.
     *
     * @componenttag salaxy-account-details-edit-worker
     * @example
     * ```html
     * <salaxy-account-details-edit-worker></salaxy-account-details-edit-worker>
     * ```
     */
    export class AccountDetailsEditWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the MessageController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountInfoWorker" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows information about the Worker account.
     * The component may be bound with ng-model or if omitteds binds to crudService.current singleton.
     *
     * @componenttag salaxy-account-info-worker
     * @example
     * ```html
     * <salaxy-account-info-worker view-type="details" ng-model=""></salaxy-account-info-worker>
     * ```
     */
    export class AccountInfoWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** ngModel may be used */
        require: {
            model: string;
        };
        /** Uses the WorkersController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/AccountTaxCardWorker" {
    import { TaxCard2019Controller } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a detail view about current worker's latest tax card, and provides an edit component for a new tax card
     *
     * @componenttag salaxy-account-tax-card-worker
     * @example
     * ```html
     * <salaxy-account-tax-card-worker binding="self"></salaxy-account-tax-card-worker>
     * ```
     */
    export class AccountTaxCardWorker extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** "self" for current account "currentWorker" (default) for selected worker account of the employer  */
            binding: string;
        };
        /** Uses the TaxCard2019Controller */
        controller: typeof TaxCard2019Controller;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/worker/index" {
    export * from "components/account/worker/AccountAuthorizationWorker";
    export * from "components/account/worker/AccountAvatarEditWorker";
    export * from "components/account/worker/AccountContactWorker";
    export * from "components/account/worker/AccountDetailsWorker";
    export * from "components/account/worker/AccountDetailsEditWorker";
    export * from "components/account/worker/AccountInfoWorker";
    export * from "components/account/worker/AccountTaxCardWorker";
}
declare module "components/account/AccountInfo" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Simple presentation of Salaxy account info: employer and/or worker data
     *
     * @componenttag salaxy-account-info
     * @example
     * ```html
     * <salaxy-account-info></salaxy-account-info>
     * ```
     */
    export class AccountInfo extends ComponentBase {
        /** The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to controller implementation
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/AuthCard" {
    import { AccountAuthorizationController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a card for granting authorization
     *
     * @componenttag salaxy-auth-card
     * @example
     * ```html
     * <salaxy-auth-card auth-id="tax"></salaxy-auth-card>
     * ```
     */
    export class AuthCard extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Id that specifies what auth card to be shown */
            authId: string;
            /** If set to true, the switch is not shown on the card. */
            hideSwitch: string;
        };
        /** Uses the AccountAuthorizationController */
        controller: typeof AccountAuthorizationController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/AuthInfoHeaders" {
    import { AccountAuthorizationController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows infor headers for an authorization party
     *
     * @componenttag salaxy-auth-card
     * @example
     * ```html
     * <salaxy-auth-info-headers></salaxy-auth-info-headers>
     * ```
     */
    export class AuthInfoHeaders extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            showMoreInfoButton: string;
        };
        /** Uses the AccountAuthorizationController */
        controller: typeof AccountAuthorizationController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/AuthSwitch" {
    import { AuthorizationSwitchController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a salaxy switch icon
     *
     *
     * Please note that also the ng-model directive is required.
     *
     * @componenttag salaxy-auth-switch
     * @example
     * ```html
     * <salaxy-auth-switch xxx="tax"><salaxy-auth-switch>
     * ```
     */
    export class AuthSwitch extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** - name: Name of the input - also used as id. */
            name: string;
            /** - id: */
            id: string;
            /** - onText: */
            onText: string;
            /** - offText: */
            offText: string;
            /** - disabled: */
            disabled: string;
        };
        /** Uses the AuthorizationSwitchController */
        controller: typeof AuthorizationSwitchController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/AuthorizedAccounts" {
    import { AccountAuthorizationController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of all authorized accounts which can act on behalf of this account.
     *
     * @componenttag salaxy-authorized-accounts
     * @example
     * ```html
     * <salaxy-authorized-accounts></salaxy-authorized-accounts>
     * ```
     */
    export class AuthorizedAccounts extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the AccountAuthorizationController */
        controller: typeof AccountAuthorizationController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/AuthorizingAccounts" {
    import { AccountAuthorizationController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of all authorizing accounts on behalf of which this account can act.
     *
     * @componenttag salaxy-authorizing-accounts
     * @example
     * ```html
     * <salaxy-authorizing-accounts></salaxy-authorizing-accounts>
     * ```
     */
    export class AuthorizingAccounts extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the AccountAuthorizationController */
        controller: typeof AccountAuthorizationController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/Certificates" {
    import { CertificateController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of all certificates which has been generated for this account.
     *
     * @componenttag salaxy-certificates
     * @example
     * ```html
     * <salaxy-certificates></salaxy-certificates>
     * ```
     */
    export class Certificates extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the CertificateController */
        controller: typeof CertificateController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/Credentials" {
    import { CredentialController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of all login credentials which can access this account.
     *
     * @componenttag salaxy-credentials
     * @example
     * ```html
     * <salaxy-credentials></salaxy-credentials>
     * ```
     */
    export class Credentials extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the CredentialController */
        controller: typeof CredentialController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/LanguageSelector" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a language selection control.
     *
     * @componenttag salaxy-language-selector
     * @example
     * ```html
     * <salaxy-language-selector></salaxy-language-selector>
     * ```
     */
    export class LanguageSelector extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/LoginButton" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a Login / Logout -button.
     * When anonymous, the button shows the login button.
     * When loggen in shows: Logout and links to Palkkaus Account and User Account pages.
     *
     * @componenttag salaxy-login-button
     * @example
     * ```html
     * <salaxy-login-button redirect-url="'/my-page'"></salaxy-login-button>
     * ```
     */
    export class LoginButton extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** URL to which the user should be redirected. When you specify string, use quotes. */
            redirectUrl: string;
        };
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/ShopCard" {
    import { AccountController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a card for granting authorization
     *
     * @componenttag salaxy-shop-card
     * @example
     * ```html
     * <salaxy-shop-card shop-id="'varma'"></salaxy-auth-card>
     * ```
     */
    export class ShopCard extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** Id that specifies which Shop card to be shown */
            shopId: string;
        };
        /** Uses the AccountController */
        controller: typeof AccountController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/ShopCards" {
    import { AccountController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of shop cards
     *
     * @componenttag salaxy-shop-cards
     * @example
     * ```html
     * <salaxy-shop-cards grid-column-class="col-md-6 col-lg-3"></salaxy-shop-card>
     * ```
     */
    export class ShopCards extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** - grid-column-class: Class that is set on the column div of the card. Typically to set the Bootstrap column classes. Default is 'col-sm-6 col-md-4'. */
            gridColumnClass: string;
        };
        /** Uses the AccountController */
        controller: typeof AccountController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/Signing" {
    import { SigningController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows the signature for the current account for signing.
     *
     * @componenttag salaxy-signing
     * @example
     * ```html
     * <salaxy-signing></salaxy-signing>
     * ```
     */
    export class Signing extends ComponentBase {
        /** The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to controller implementation
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SigningController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/UserInfo" {
    import { SessionController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Simple presentation of Salaxy user info: employer and/or worker data
     *
     * @componenttag salaxy-user-info
     * @example
     * ```html
     * <salaxy-user-info></salaxy-user-info>
     * ```
     */
    export class UserInfo extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the SessionController */
        controller: typeof SessionController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/account/index" {
    export * from "components/account/worker/index";
    export * from "components/account/AccountInfo";
    export * from "components/account/AuthCard";
    export * from "components/account/AuthInfoHeaders";
    export * from "components/account/AuthSwitch";
    export * from "components/account/AuthorizedAccounts";
    export * from "components/account/AuthorizingAccounts";
    export * from "components/account/Certificates";
    export * from "components/account/Credentials";
    export * from "components/account/LanguageSelector";
    export * from "components/account/LoginButton";
    export * from "components/account/ShopCard";
    export * from "components/account/ShopCards";
    export * from "components/account/Signing";
    export * from "components/account/UserInfo";
}
declare module "components/report/EarningsPayment" {
    import { EarningsPaymentController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a user interface for Earnings Payment Report ("Tulorekisteri-ilmoitus").
     * Currently, the UI is read-only, but it may later be extended to be editable
     * for purposes of corrections.
     *
     * @componenttag salaxy-earnings-payment
     * @example
     * ```html
     * <salaxy-earnings-payment calc="current"></salaxy-earnings-payment>
     * ```
     */
    export class EarningsPayment extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /**
             * Calculation for which the component shows the report.
             * If set, monitors changes and updates the report accordingly.
             * The object may be a calculation or string "current" for the current calculation.
             */
            calc: string;
            /** EarningsPayment object to which the component is bound. */
            report: string;
        };
        /** Uses the EarningsPaymentReportController */
        controller: typeof EarningsPaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/report/EarningsPaymentList" {
    import { EarningsPaymentController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of Earnings Payment reports sent to National Incomes Register.
     *
     * @componenttag salaxy-earnings-payment-list
     * @example
     * ```html
     * <salaxy-earnings-payment-list calc="current"></salaxy-earnings-payment-list>
     * ```
     */
    export class EarningsPaymentList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: any;
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * Default filter includes dropdown options and text search.
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
            filter: string;
        };
        /** Uses the WorkersController */
        controller: typeof EarningsPaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/report/ReportList" {
    import { ReportsController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of reports
     *
     *
     * @componenttag salaxy-report-list
     * @example
     * ```html
     * <salaxy-report-list report-type="unemployment"></salaxy-report-list>
     * ```
     */
    export class ReportList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** - report-type: Type of the report (string, see reportType enumeration) that is shown in the list.
             * Also supports value null/"current" for showing reports defined by reportsService.currentReportType.
             */
            reportType: string;
        };
        /** Uses the ReportsController */
        controller: typeof ReportsController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/report/EmployerPaymentList" {
    import { EmployerPaymentController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of payments to be paid by the employer.
     *
     * @componenttag salaxy-employer-payment-list
     * @example
     * ```html
     * <salaxy-employer-payment-list></salaxy-employer-payment-list>
     * ```
     */
    export class EmployerPaymentList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the WorkersController */
        controller: typeof EmployerPaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/report/index" {
    export * from "components/report/EarningsPayment";
    export * from "components/report/EarningsPaymentList";
    export * from "components/report/ReportList";
    export * from "components/report/EmployerPaymentList";
}
declare module "components/year-end/YearEndFeedbackCompany" {
    import { YearEndCompanyController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a yearly feedback form that can be used by an authenticated user to check the calculations of the year and post fixing requests to Salaxy customer service.
     *
     * @componenttag salaxy-year-end-feedback-company
     * @example
     * ```html
     * <salaxy-year-end-feedback-company></salaxy-year-end-feedback-company>
     * ```
     */
    export class YearEndFeedbackCompany extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** If true, shows the feedback form even if it has already been submitted */
            alwaysShowSubmit: string;
        };
        /** Uses the YearEndCompanyController */
        controller: typeof YearEndCompanyController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/year-end/YearEndFeedbackHousehold" {
    import { YearEndHouseholdController } from "controllers/index";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Renders a yearly feedback form that can be used by an authenticated user to check the calculations of the year and post fixing requests to Salaxy customer service.
     *
     * @componenttag salaxy-year-end-feedback-household
     * @example
     * ```html
     * <salaxy-year-end-feedback-household></salaxy-year-end-feedback-household>
     * ```
     */
    export class YearEndFeedbackHousehold extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {
            /** If true, shows the feedback form even if it has already been submitted */
            alwaysShowSubmit: string;
        };
        /** Uses the YearEndCompanyController */
        controller: typeof YearEndHouseholdController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/year-end/index" {
    export * from "components/year-end/YearEndFeedbackCompany";
    export * from "components/year-end/YearEndFeedbackHousehold";
}
declare module "components/ESalaryAdmin" {
    import { ESalaryPaymentController } from "controllers/ESalaryPaymentController";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * EXPERIMENTAL: E-salaries admin component for Partner internal purposes.
     */
    export class ESalaryAdmin extends ComponentBase {
        /** See the component description for available bindings */
        bindings: {
            onListSelect: string;
            onDelete: string;
            onCreateNew: string;
            detailsUrl: string;
            listUrl: string;
        };
        /**
         * Header is mainly meant for buttons (Usually at least "Add new").
         * These are positioned to the right side of the table header (thead).
         */
        transclude: {
            header: string;
        };
        /** Uses the ESalaryController */
        controller: typeof ESalaryPaymentController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/TaxCardsList" {
    import { TaxCardsListController } from "controllers/TaxCardsListController";
    import { ComponentBase } from "components/_ComponentBase";
    /**
     * Shows a list of workers and their taxcards
     *
     * @componenttag salaxy-tax-cards-list
     * @example
     * ```html
     * <salaxy-tax-cards-list></salaxy-tax-cards-list>
     * ```
     */
    export class TaxCardsList extends ComponentBase {
        /**
         * The following component properties (attributes in HTML) are bound to the Controller.
         * For detailed functionality, refer to [controller](#controller) implementation.
         */
        bindings: {};
        /** Uses the WorkersController */
        controller: typeof TaxCardsListController;
        /** The default template for the component. */
        defaultTemplate: string;
    }
}
declare module "components/index_components" {
    export * from "components/calc/index";
    export * from "components/helpers/index";
    export * from "components/communications/index";
    export * from "components/contact/index";
    export * from "components/contract/index";
    export * from "components/form-controls/index";
    export * from "components/settings/index";
    export * from "components/worker/index";
    export * from "components/account/index";
    export * from "components/report/index";
    export * from "components/year-end/index";
    export * from "components/ESalaryAdmin";
    export * from "components/TaxCardsList";
}
declare module "controllers/_ControllersRegistration" {
    import { AbsencePeriodsController, AccountAuthorizationController, AccountController, AccountTestController, AddCorrectionDialogController, AlertController, AuthorizationSwitchController, AvatarController, CalcChartController, CalcIrRowsController, CalcRowsController, CalculationGroupController, CalculationsController, Calculator2016Controller, Calculator2018Controller, CalcWorktimeController, CalendarController, CertificateController, ChartController, CmsController, CompanyOnboardingWizardController, ContactController, ContractController, CreateAccountWizardController, CredentialController, CreditTransferController, DatepickerController, DatepickerPopupController, DateRangeController, EarningsPaymentController, EditDialogController, EmailMessageController, EmployerPaymentController, ESalaryPaymentController, FinvoiceTesterController, HolidayYearAccrualController, HolidayYearHolidaysController, HolidayYearHourlySalariesController, HolidayYearPaidController, HolidayYearsController, IfInsuranceWizardController, ImportController, InputBooleanController, InputController, InputIncomeTypeController, InputNumberController, MessageController, ModalDocumentPreviewController, ModalGenericDialogController, NaviController, OnboardingController, PaymentController, PayrollController, PricingController, ProfilesController, ReportsController, SessionController, SignatureController, SigningController, SmsMessageController, SpinnerController, SwitchController, TabController, TabsController, TaxCard2019Controller, TaxCardsListController, VarmaPensionWizardController, WelcomeController, WizardController, WorkerAbsencesController, WorkerCalcRowsController, WorkerInfoController, WorkerOnboardingWizardController, WorkersController, WorkerWizardController, YearEndCompanyController, YearEndHouseholdController } from "controllers/index";
    /** Provides methods for registering the Controllers to module
     * (and other related metadata in the future).
     */
    export class ControllersRegistration {
        /** Gets the controllers for Module registration. */
        static getControllers(): {
            AbsencePeriodsController: typeof AbsencePeriodsController;
            AccountAuthorizationController: typeof AccountAuthorizationController;
            AccountController: typeof AccountController;
            AccountTestController: typeof AccountTestController;
            AddCorrectionDialogController: typeof AddCorrectionDialogController;
            AlertController: typeof AlertController;
            AuthorizationSwitchController: typeof AuthorizationSwitchController;
            AvatarController: typeof AvatarController;
            CalcIrRowsController: typeof CalcIrRowsController;
            CalcWorktimeController: typeof CalcWorktimeController;
            CalcRowsController: typeof CalcRowsController;
            CalcChartController: typeof CalcChartController;
            CalculationGroupController: typeof CalculationGroupController;
            CalculationsController: typeof CalculationsController;
            Calculator2016Controller: typeof Calculator2016Controller;
            Calculator2018Controller: typeof Calculator2018Controller;
            CalendarController: typeof CalendarController;
            CertificateController: typeof CertificateController;
            ChartController: typeof ChartController;
            CmsController: typeof CmsController;
            CompanyOnboardingWizardController: typeof CompanyOnboardingWizardController;
            ContactController: typeof ContactController;
            ContractController: typeof ContractController;
            CredentialController: typeof CredentialController;
            CreditTransferController: typeof CreditTransferController;
            CreateAccountWizardController: typeof CreateAccountWizardController;
            DatepickerController: typeof DatepickerController;
            DatepickerPopupController: typeof DatepickerPopupController;
            DateRangeController: typeof DateRangeController;
            EditDialogController: typeof EditDialogController;
            EmailMessageController: typeof EmailMessageController;
            EmployerPaymentController: typeof EmployerPaymentController;
            ESalaryPaymentController: typeof ESalaryPaymentController;
            FinvoiceTesterController: typeof FinvoiceTesterController;
            HolidayYearAccrualController: typeof HolidayYearAccrualController;
            HolidayYearHolidaysController: typeof HolidayYearHolidaysController;
            HolidayYearHourlySalariesController: typeof HolidayYearHourlySalariesController;
            HolidayYearPaidController: typeof HolidayYearPaidController;
            HolidayYearsController: typeof HolidayYearsController;
            IfInsuranceWizardController: typeof IfInsuranceWizardController;
            ImportController: typeof ImportController;
            InputBooleanController: typeof InputBooleanController;
            InputController: typeof InputController;
            InputIncomeTypeController: typeof InputIncomeTypeController;
            InputNumberController: typeof InputNumberController;
            MessageController: typeof MessageController;
            ModalDocumentPreviewController: typeof ModalDocumentPreviewController;
            ModalGenericDialogController: typeof ModalGenericDialogController;
            NaviController: typeof NaviController;
            OnboardingController: typeof OnboardingController;
            PaymentController: typeof PaymentController;
            PayrollController: typeof PayrollController;
            PricingController: typeof PricingController;
            ProfilesController: typeof ProfilesController;
            ReportsController: typeof ReportsController;
            SessionController: typeof SessionController;
            SignatureController: typeof SignatureController;
            SigningController: typeof SigningController;
            SmsMessageController: typeof SmsMessageController;
            SpinnerController: typeof SpinnerController;
            SwitchController: typeof SwitchController;
            TabController: typeof TabController;
            TabsController: typeof TabsController;
            TaxCard2019Controller: typeof TaxCard2019Controller;
            TaxCardsListController: typeof TaxCardsListController;
            VarmaPensionWizardController: typeof VarmaPensionWizardController;
            EarningsPaymentController: typeof EarningsPaymentController;
            WelcomeController: typeof WelcomeController;
            WizardController: typeof WizardController;
            WorkerAbsencesController: typeof WorkerAbsencesController;
            WorkerCalcRowsController: typeof WorkerCalcRowsController;
            WorkerInfoController: typeof WorkerInfoController;
            WorkerOnboardingWizardController: typeof WorkerOnboardingWizardController;
            WorkersController: typeof WorkersController;
            WorkerWizardController: typeof WorkerWizardController;
            YearEndCompanyController: typeof YearEndCompanyController;
            YearEndHouseholdController: typeof YearEndHouseholdController;
        };
    }
}
declare module "directives/AppendNodeDirective" {
    import * as angular from "angular";
    /**
     * Appends the given element to the current element.
     * The existing element will be removed from the source parent element.
     * The attribute value should resolve to an existing DOM node or string.
     *
     * @example
     * <div salaxy-append-node="tab.headingElement"></div>
     */
    export class AppendNodeDirective implements angular.IDirective {
        /**
         * Factory for directive registration.
         * @ignore
         */
        static factory(): () => AppendNodeDirective;
        /**
         * Applies to attributes only.
         * @ignore
         */
        restrict: string;
        /**
         * Creates a new instance of the directive.
         */
        constructor();
        /**
         * Evaluates the attribute value (DOM node or default string), and appends it to the current element.
         * @ignore
         */
        link(scope: any, element: any, attrs: any): void;
    }
}
declare module "directives/EnumParserFunctions" {
    /**
     * Parser and formatter functions for different enum inputs.
     * This is required for AngularJS versions less than 1.6
     * @example
     * ```html
     * sxyEnumAsBoolean
     * <select ng-model="model.value" sxy-enum-as-boolean>
     *  <option value="null">Null</option>
     *  <option value="true">True</option>
     *  <option value="false">False</option>
     * </select>
     * ```
     */
    export class EnumParserFunctions {
        /**
         * Boolean conversion.
         */
        static sxyEnumAsBoolean(): () => any;
        private static directive;
        private static getLink;
        private static booleanParser;
        private static defaultFormatter;
    }
}
declare module "directives/IfRoleDirective" {
    import * as angular from "angular";
    import { SessionService } from "services/index";
    /**
     * Shows the element if the user is in the given role / in one of the given roles.
     * The attribute value should resolve to either role string or array of role strings.
     *
     * @example
     * <div *salaxy-if-role="anon">You are not logged in.</div>
     * <div *salaxy-if-role="household,worker">This text is shown to Household or Worker roles.</div>
     */
    export class IfRoleDirective implements angular.IDirective {
        private sessionService;
        /**
         * Factory method for the directive creation.
         * @ignore
         */
        static factory(): (sessionService: SessionService, ngIfDirective: any) => IfRoleDirective;
        /**
         * Directive restrictions.
         * @ignore
         */
        restrict: any;
        /**
         * Transclusion.
         * @ignore
         */
        transclude: any;
        /**
         * Priority parameter.
         * @ignore
         */
        priority: any;
        /**
         * Terminal parameter.
         * @ignore
         */
        terminal: any;
        /** Uses the original ngIf class as bases for the functionality. */
        private ngIf;
        /**
         * Creates a new instance of the directive.
         */
        constructor(sessionService: SessionService, ngIfDirective: any);
        /**
         * Link function for the directive.
         * @ignore
         */
        link(scope: any, element: any, attrs: any): void;
    }
}
declare module "directives/LoaderDirective" {
    import * as angular from "angular";
    /**
     * Loading indicator directive.
     * Toggles class "salaxy-loading" on an element. Toggling is based on  "salaxy-loader-show" and "salaxy-loader-hide" events from the
     * LoaderInterceptor which intercepts http calls.
     * @example
     * <div salaxy-loader class="spinner">
     *          <div class="bounce1"></div>
     *          <div class="bounce2"></div>
     *          <div class="bounce3"></div>
     * </div>
     */
    export class LoaderDirective implements angular.IDirective {
        /**
         * Factory for the directive creation.
         * @ignore
         */
        static factory(): () => LoaderDirective;
        /**
         * Directive restrictions.
         * @ignore
         */
        restrict: string;
        /**
         * Creates a new instance of the directive.
         */
        constructor();
        /**
         * Link function for the directive.
         * @ignore
         */
        link(scope: any, element: any, attrs: any): void;
    }
}
declare module "directives/ValidatorFunctions" {
    /**
     * Validator directives for different form inputs.
     * @example
     * ```html
     * sxyIban
     * <input name="iban" type="text" ng-model="iban" sxy-iban>
     *
     * sxyEmail
     * <input name="email" type="text" ng-model="email" sxy-email>
     *
     * sxyMobilePhone
     * <input name="mobilePhone" type="text" ng-model="mobilePhone" sxy-mobile-phone>
     *
     * sxyPersonalIdFi
     * <input name="personalIdFi" type="text" ng-model="personalIdFi" sxy-personal-id-fi>
     *
     * sxyCompanyIdFi
     * <input name="companyIdFi" type="text" ng-model="companyIdFi" sxy-company-id-fi>
     *
     * sxyPostalCodeFi
     * <input name="postalCodeFi" type="text" ng-model="postalCodeFi" sxy-postal-code-fi>
     *
     * sxyCurrency
     * <input name="currency" type="text" ng-model="currency" sxy-currency>
     *
     * sxyTaxPercent
     * <input name="taxPercent" type="text" ng-model="taxPercent" sxy-tax-percent>
     *
     * sxyNumber
     * <input name="number" type="text" ng-model="number" sxy-number>
     *
     * sxyInteger
     * <input name="intger" type="text" ng-model="integer" sxy-integer>
     *
     * sxySmsVerificationCode
     * <input name="smsVerificationCode" type="text" ng-model="smsVerificationCode" sxy-sms-verification-code>
     *
     * sxyPensionContractNumber
     * <input name="pensionContractNumber" type="text" ng-model="pensionContractNumber" sxy-pension-contract-number>
     *
     * sxyPensionContractNumber
     * <input name="temporaryPensionContractNumber" type="text" ng-model="temporaryPensionContractNumber" sxy-temporary-pension-contract-number>
     * ```
     */
    export class ValidatorFunctions {
        /**
         * Tax percent validator.
         */
        static sxyTaxPercent(): () => any;
        /**
         * Currency value validator.
         */
        static sxyCurrency(): () => any;
        /**
         * Iban validator.
         */
        static sxyIban(): () => any;
        /**
         * Email validator.
         */
        static sxyEmail(): () => any;
        /**
         * Mobile phone validator.
         */
        static sxyMobilePhone(): () => any;
        /**
         * Finnish personal id validator.
         */
        static sxyPersonalIdFi(): () => any;
        /**
         * Finnish company id validator.
         */
        static sxyCompanyIdFi(): () => any;
        /**
         * Finnish postal code validator.
         */
        static sxyPostalCodeFi(): () => any;
        /**
         * Sms verification code validator.
         */
        static sxySmsVerificationCode(): () => any;
        /**
         * Number validator.
         */
        static sxyNumber(): () => any;
        /**
         * Pension contract number validator.
         */
        static sxyPensionContractNumber(): () => any;
        /**
         * Integer validator.
         */
        static sxyInteger(): () => any;
        private static directive;
        private static parserAndFormatterDirective;
        private static setValidator;
        private static setParserAndFormatter;
    }
}
declare module "directives/DatepickerInputValidationDirective" {
    import * as angular from "angular";
    /**
     * Validates the datepicker manual input using the same rules
     * as the actual calendar selection control.
     * Currently this directive is used internally in salaxy-datepicker-popup only.
     *
     */
    export class DatepickerInputValidationDirective implements angular.IDirective {
        /**
         * Factory for directive registration.
         * @ignore
         */
        static factory(): () => DatepickerInputValidationDirective;
        /**
         * Applies to attributes only.
         * @ignore
         */
        restrict: string;
        /**
         * Requires model.
         * @ignore
         */
        require: string;
        /**
         * Creates a new instance of the directive.
         */
        constructor();
        /**
         * Validates the datepicker manual input using the same rules
         * as the actual calendar selection control.
         * @ignore
         */
        link(scope: any, element: any, attrs: any, ngModel: any): void;
    }
}
declare module "directives/HtmlDirective" {
    import * as angular from "angular";
    import { Ng1Translations } from "services/ui/index";
    /**
     * Directive for binding html for the element.
     * The attribute sxy-html should contain the translation key for the html content.
     * This directive should be used instead of the AngularJs ng-bind-html element.
     * Interpolation parameters for translation can be given using params -attribute.
     *
     * @example
     * <p class="lead" sxy-html="SALAXY.NG1.UserInfoComponent.description.html" ></p>
     */
    export class HtmlDirective implements angular.IDirective {
        private translate;
        private $sce;
        /**
         * Factory method for the directive creation.
         * @ignore
         */
        static factory(): any;
        /**
         * Directive restrictions.
         * @ignore
         */
        restrict: string;
        /**
         * Directive scope.
         * @ignore
         */
        scope: {
            params: string;
            sxyHtml: string;
        };
        /**
         * Creates a new instance of the directive.
         */
        constructor(translate: Ng1Translations, $sce: angular.ISCEService);
        /**
         * Compile function for the directive.
         * @ignore
         */
        compile(cElement: any, cAttr: any): any;
    }
}
declare module "directives/TextDirective" {
    import * as angular from "angular";
    import { Ng1Translations } from "services/ui/index";
    /**
     * Directive for binding text for the element.
     * The attribute sxy-text should contain the translation key for the text content.
     * This directive should be used to replace AngularJS expression and translation pipe filter.
     * Small additions to the beginning and end can be added with pre and ps attributes.
     * Interpolation parameters for translation can be given using params -attribute.
     * @example
     * <div sxy-text="SALAXY.NG1.UserInfoComponent.authType" ps=":" pre="HUOM: "></div>
     */
    export class TextDirective implements angular.IDirective {
        private translate;
        /**
         * Factory method for the directive creation.
         * @ignore
         */
        static factory(): any;
        /**
         * Directive restrictions.
         * @ignore
         */
        restrict: string;
        /**
         * Directive scope.
         * @ignore
         */
        scope: {
            params: string;
        };
        /**
         * Creates a new instance of the directive.
         */
        constructor(translate: Ng1Translations);
        /**
         * Compile function for the directive.
         * @ignore
         */
        compile(cElement: any, cAttr: any): any;
    }
}
declare module "directives/index" {
    export * from "directives/AppendNodeDirective";
    export * from "directives/EnumParserFunctions";
    export * from "directives/IfRoleDirective";
    export * from "directives/LoaderDirective";
    export * from "directives/ValidatorFunctions";
    export * from "directives/DatepickerInputValidationDirective";
    export * from "directives/HtmlDirective";
    export * from "directives/TextDirective";
}
declare module "directives/_DirectivesRegistration" {
    import { AppendNodeDirective, DatepickerInputValidationDirective, IfRoleDirective, LoaderDirective } from "directives/index";
    /** Provides methods for registering the Directives to module
     * (and other related metadata in the future).
     */
    export class DirectivesRegistration {
        /** Gets the directives for Module registration. */
        static getDirectives(): {
            salaxyLoader: () => LoaderDirective;
            salaxyIfRole: (sessionService: import("../services/SessionService").SessionService, ngIfDirective: any) => IfRoleDirective;
            sxyTaxPercent: () => any;
            sxyCurrency: () => any;
            sxyIban: () => any;
            sxyEmail: () => any;
            sxyEnumAsBoolean: () => any;
            sxyMobilePhone: () => any;
            sxyPersonalIdFi: () => any;
            sxyCompanyIdFi: () => any;
            sxyPostalCodeFi: () => any;
            sxyNumber: () => any;
            sxyInteger: () => any;
            sxySmsVerificationCode: () => any;
            sxyPensionContractNumber: () => any;
            salaxyAppendNode: () => AppendNodeDirective;
            salaxyDatepickerInputValidation: () => DatepickerInputValidationDirective;
            sxyHtml: any;
            sxyText: any;
        };
    }
}
declare module "services/_ServicesRegistration" {
    import { AbsencesService, AccountService, AlertService, AuthorizedAccountService, CalcRowsService, CalculationsService, CertificateService, CmsService, ContactService, ContractService, CredentialService, EarningsPaymentService, EmailMessageService, ESalaryPaymentService, HolidayYearsService, IfInsuranceService, NaviService, Ng1Translations, OnboardingService, PaymentService, PayrollService, ProfilesService, ReportsService, SessionService, SignatureService, SmsMessageService, TaxCardService, TempCalcGroupService, UiHelpers, VarmaPensionService, VerificationService, WizardService, WorkersService, YearEndService } from "services/index";
    import { AjaxNg1 } from "ajax/AjaxNg1";
    import { Absences, Accounts, AuthorizedAccounts, Calculations, Calculator, Certificates, Client, Cms, Contacts, Contracts, Credentials, EarningsPayments, EmailMessages, ESalaryPayments, Files, HolidayYears, OAuth2, Onboardings, PartnerServices, Payments, Payrolls, Profiles, Reports, Session, SmsMessages, Taxcards, Test, Workers, YearEnd } from "@salaxy/core";
    /** Provides methods for registering the Services to module
     * (and other related metadata in the future).
     */
    export class ServicesRegistration {
        /** Gets the services for Module registration. */
        static getServices(): {
            AbsencesService: typeof AbsencesService;
            AjaxNg1: typeof AjaxNg1;
            AccountService: typeof AccountService;
            AlertService: typeof AlertService;
            AuthorizedAccountService: typeof AuthorizedAccountService;
            CalcRowsService: typeof CalcRowsService;
            CalculationsService: typeof CalculationsService;
            CertificateService: typeof CertificateService;
            CmsService: typeof CmsService;
            ContactService: typeof ContactService;
            ContractService: typeof ContractService;
            CredentialService: typeof CredentialService;
            EarningsPaymentService: typeof EarningsPaymentService;
            EmailMessageService: typeof EmailMessageService;
            ESalaryPaymentService: typeof ESalaryPaymentService;
            HolidayYearsService: typeof HolidayYearsService;
            IfInsuranceService: typeof IfInsuranceService;
            NaviService: typeof NaviService;
            Ng1Translations: typeof Ng1Translations;
            OnboardingService: typeof OnboardingService;
            PayrollService: typeof PayrollService;
            PaymentService: typeof PaymentService;
            ProfilesService: typeof ProfilesService;
            ReportsService: typeof ReportsService;
            SessionService: typeof SessionService;
            SignatureService: typeof SignatureService;
            SmsMessageService: typeof SmsMessageService;
            TaxCardService: typeof TaxCardService;
            TempCalcGroupService: typeof TempCalcGroupService;
            UiHelpers: typeof UiHelpers;
            VarmaPensionService: typeof VarmaPensionService;
            VerificationService: typeof VerificationService;
            WizardService: typeof WizardService;
            WorkersService: typeof WorkersService;
            YearEndService: typeof YearEndService;
        };
        /** Gets the services from the @salaxy/core project that need to be registered for NG1 dependency injection. */
        static getCoreServices(): {
            Absences: typeof Absences;
            Accounts: typeof Accounts;
            AuthorizedAccounts: typeof AuthorizedAccounts;
            Calculations: typeof Calculations;
            Calculator: typeof Calculator;
            Certificates: typeof Certificates;
            Client: typeof Client;
            Cms: typeof Cms;
            Contacts: typeof Contacts;
            Contracts: typeof Contracts;
            Credentials: typeof Credentials;
            EarningsPayments: typeof EarningsPayments;
            EmailMessages: typeof EmailMessages;
            ESalaryPayments: typeof ESalaryPayments;
            Files: typeof Files;
            HolidayYears: typeof HolidayYears;
            OAuth2: typeof OAuth2;
            Onboardings: typeof Onboardings;
            PartnerServices: typeof PartnerServices;
            Payments: typeof Payments;
            Payrolls: typeof Payrolls;
            Profiles: typeof Profiles;
            Reports: typeof Reports;
            Session: typeof Session;
            SmsMessages: typeof SmsMessages;
            Taxcards: typeof Taxcards;
            Test: typeof Test;
            Workers: typeof Workers;
            YearEnd: typeof YearEnd;
        };
    }
}
declare module "interceptors/LoaderInterceptor" {
    import * as angular from "angular";
    /**
     * Interceptor for monitoring $http-service calls.
     */
    export class LoaderInterceptor {
        private $q;
        private $rootScope;
        /**
         * For NG-dependency injection
         * @ignore
         */
        static $inject: string[];
        /**
         * Factory method for creating singleton.
         * @param  $q - $q service.
         * @param  $rootScope - Angular root scope.
         */
        static factory($q: angular.IQService, $rootScope: angular.IRootScopeService): LoaderInterceptor;
        /**
         * Singleton
         */
        private static instance;
        private loadingCount;
        /**
         * Constructor creating a new interceptor.
         * @param  $q - $q service.
         * @param  $rootScope - Angular root scope.
         */
        private constructor();
        /**
         * Intercepting method for request.
         * @param config - $http request.
         */
        request: (config: angular.IRequestConfig) => angular.IRequestConfig | angular.IPromise<angular.IRequestConfig>;
        /**
         * Intercepting method for response.
         * @param response - $http response.
         */
        response: (response: angular.IHttpResponse<any>) => angular.IHttpResponse<any> | angular.IPromise<angular.IHttpResponse<any>>;
        /**
         * Intercepting method for response error.
         * @param {any} rejection - $http error.
         */
        responseError: (rejection: any) => angular.IPromise<never>;
    }
}
declare module "interceptors/index" {
    export * from "interceptors/LoaderInterceptor";
}
declare module "filters/TranslateFilter" {
    import * as angular from "angular";
    import { Ng1Translations } from "services/ui/index";
    /**
     * Translate filter with key and interpolation parameter.
     * Converts content with translationId ending with '.html', to safe html.
     * Converts content with translationId ending with '.md', first to html and then to safe html.
     * If the text contains a variable placeholder like: 'Hello {{name}}!', it will be replaced by given parameter.
     *
     * @example
     * <p>{{ 'SALAXY.NG1.test1' | sxyTranslate }}</p>;
     * <p>{{ 'SALAXY.NG1.hello' | sxyTranslate: { name: 'John'} }}</p>;
     */
    export class TranslateFilter {
        private translate;
        private $sce;
        /** Factory for the filter creation. */
        static factory(): (translate: Ng1Translations, $sce: angular.ISCEService) => (translationId: string, interpolateParams: object) => string;
        constructor(translate: Ng1Translations, $sce: angular.ISCEService);
        /** Filter function. */
        filter: (translationId: string, interpolateParams: object) => string;
    }
}
declare module "filters/FilterFunctions" {
    import { DateRange, Unit } from "@salaxy/core";
    /**
     * Contains the simple filter functions where implementation
     * is in some other class (typically @salaxy/core) and thus the filter implementation is very small.
     * This should typically the case, but some filters may contain a lot of NG1 specific
     * implementation (e.g. SalaxyTranslateFilter) and/or dependency injection and thus deserve their own class.
     */
    export class FilterFunctions {
        /**
         * Returns the label for an enumeration.
         *
         * @example
         * <p>{{ 'CalculationStatus.Draft' | sxyEnum }} - The enum full name</p>;
         * <p>{{ 'CalculationStatus.paymentSucceeded' | sxyEnum }} - you may also use JSON value where first letter is lower-case.</p>;
         * <p>{{ 'CalculationStatus.' + calc.status | sxyEnum }} - ...which makes it easy to use with data.</p>;
         */
        static sxyEnum(): (input: string) => any;
        /**
         * Returns the description for an enumeration.
         *
         * @example
         * <p>{{ 'CalculationStatus.Draft' | sxyEnumDescr }} - The enum full name</p>;
         * <p>{{ 'CalculationStatus.paymentSucceeded' | sxyEnumDescr }} - you may also use JSON value where first letter is lower-case.</p>;
         * <p>{{ 'CalculationStatus.' + calc.status | sxyEnumDescr }} - ...which makes it easy to use with data.</p>;
         */
        static sxyEnumDescr(): (input: string) => any;
        /**
         * Formats the date with Dates.getFormattedDate in core project.
         * If the input is not recognized as date or if it is empty or less than year 1900, shows a dash.
         *
         * @example
         * <p>{{ calc.workflow.paidAt | sxyDate }}</p>;
         */
        static sxyDate(): (input: string) => string;
        /**
         * Formats a DateRange with Dates.getFormattedDate in core project.
         * If input is null, shows a dash.
         */
        static sxyDateRange(): (input: DateRange) => string;
        /**
         * Iban formatting filter.
         */
        static sxyIban(): (input: string) => string;
        /**
         * Formats a number as count optionally taking into account the related unit
         * @param input Input to format
         * @param unit Unit that is used in formatting.
         * @param decimals Number of decimals to show.
         * @param nullText Text to show if the value is null / undefined.
         * By default returns null, which can then be cought by another filter or logical or ("||").
         * @param zeroText If specified, will use this text without the Unit formatting if the value is zero.
         * By default zero is formatted as "0 [unitText]".
         */
        static sxyCount(): (input: number, unit?: Unit, decimals?: number, nullText?: string, zeroText?: string) => string;
    }
}
declare module "filters/index" {
    export * from "filters/TranslateFilter";
    export * from "filters/FilterFunctions";
}
declare module "components/_NgComponents" {
    import * as angular from "angular";
    /**
     * Components contain the user interface element (the view) on top of the controller logic.
     * This is the Angular2 compatible alternative to Directives introduced in Angular 1.5.
     *
     * Registers the salaxy.ng1.components.all module that contains the entire
     * Palkkaus.fi Angular stack
     * @example
     * angular.module("myAngularApplication", ["salaxy.ng1.components.all"])
     * @ignore
     */
    export const SalaxyNg1ComponentsModule: angular.IModule;
}
declare module "components/index" {
    export * from "components/index_components";
    export * from "components/_ComponentBase";
    export { SalaxyNg1ComponentsModule } from "components/_NgComponents";
}
declare module "i18n/index" {
    export * from "i18n/dictionary";
}
declare module "templates/bootstrap" {
    import * as angular from "angular";
    /** Bootstrap templates compiled to JavaScript
    @ignore */
    export const SalaxyNg1BootstrapTemplatesModule: angular.IModule;
}
declare module "templates/index" {
    export * from "templates/bootstrap";
}
declare module "namespace/exports" {
    export * from "ajax/index";
    export * from "components/index";
    export * from "controllers/index";
    export * from "directives/index";
    export * from "filters/index";
    export * from "i18n/index";
    export * from "interceptors/index";
    export * from "services/index";
    export * from "templates/index";
}
declare module "namespace/namespace" {
    import * as lib from "namespace/exports";
    global {
        namespace salaxy {
            namespace api {
                export import AjaxNg1 = lib.AjaxNg1;
            }
            namespace ng1 {
                export import dictionary = lib.dictionary;
                namespace controllers {
                    export import AccountAuthorizationController = lib.AccountAuthorizationController;
                    export import AccountController = lib.AccountController;
                    export import AccountInfoWorkerController = lib.AccountInfoWorkerController;
                    export import AccountTestController = lib.AccountTestController;
                    export import AuthorizationSwitchController = lib.AuthorizationSwitchController;
                    export import AvatarController = lib.AvatarController;
                    export import CalcChartController = lib.CalcChartController;
                    export import CalculationsController = lib.CalculationsController;
                    export import Calculator2016Controller = lib.Calculator2016Controller;
                    export import CertificateController = lib.CertificateController;
                    export import CmsController = lib.CmsController;
                    export import ContactController = lib.ContactController;
                    export import ContractController = lib.ContractController;
                    export import CompanyOnboardingWizardController = lib.CompanyOnboardingWizardController;
                    export import CreateAccountWizardController = lib.CreateAccountWizardController;
                    export import CredentialController = lib.CredentialController;
                    export import CreditTransferController = lib.CreditTransferController;
                    export import DatepickerController = lib.DatepickerController;
                    export import DatepickerPopupController = lib.DatepickerPopupController;
                    export import EmailMessageController = lib.EmailMessageController;
                    export import ESalaryPaymentController = lib.ESalaryPaymentController;
                    export import ImportController = lib.ImportController;
                    export import InputController = lib.InputController;
                    export import MessageController = lib.MessageController;
                    export import ModalDocumentPreviewController = lib.ModalDocumentPreviewController;
                    export import ModalGenericDialogController = lib.ModalGenericDialogController;
                    export import NaviController = lib.NaviController;
                    export import OnboardingController = lib.OnboardingController;
                    export import ProfilesController = lib.ProfilesController;
                    export import ReportsController = lib.ReportsController;
                    export import SessionController = lib.SessionController;
                    export import SignatureController = lib.SignatureController;
                    export import SmsMessageController = lib.SmsMessageController;
                    export import SpinnerController = lib.SpinnerController;
                    export import SwitchController = lib.SwitchController;
                    export import TabController = lib.TabController;
                    export import TabsController = lib.TabsController;
                    export import TaxCard2019Controller = lib.TaxCard2019Controller;
                    export import WizardController = lib.WizardController;
                    export import WorkerInfoController = lib.WorkerInfoController;
                    export import WorkerOnboardingWizardController = lib.WorkerOnboardingWizardController;
                    export import WorkersController = lib.WorkersController;
                    export import WorkerWizardController = lib.WorkerWizardController;
                }
                namespace components {
                    namespace formControls {
                        export import Datepicker = lib.Datepicker;
                        export import DatepickerPopup = lib.DatepickerPopup;
                        export import FormGroup = lib.FormGroup;
                        export import Input = lib.Input;
                        export import InputCalcRowType = lib.InputCalcRowType;
                        export import InputOccupationType = lib.InputOccupationType;
                        export import InputWorker = lib.InputWorker;
                        export import Tabs = lib.Tabs;
                        export import Tab = lib.Tab;
                        export import ValidationSummary = lib.ValidationSummary;
                    }
                    export import AccountDetailsWorker = lib.AccountDetailsWorker;
                    export import AccountInfo = lib.AccountInfo;
                    export import AccountInfoWorker = lib.AccountInfoWorker;
                    export import AuthCard = lib.AuthCard;
                    export import AuthInfoHeaders = lib.AuthInfoHeaders;
                    export import AuthSwitch = lib.AuthSwitch;
                    export import AuthorizingAccounts = lib.AuthorizingAccounts;
                    export import Avatar = lib.Avatar;
                    export import Calc = lib.Calc;
                    export import CalcChart = lib.CalcChart;
                    export import CalcDetailsExpenses = lib.CalcDetailsExpenses;
                    export import CalcDetailsSalary = lib.CalcDetailsSalary;
                    export import CalcDetailsWork = lib.CalcDetailsWork;
                    export import CalcDetailsWorker = lib.CalcDetailsWorker;
                    export import CalcList = lib.CalcList;
                    export import CalcNewIntro = lib.CalcNewIntro;
                    export import CalcOverviewPanels = lib.CalcOverviewPanels;
                    export import CalcPayButton = lib.CalcPayButton;
                    export import CalcPro = lib.CalcPro;
                    export import CalcProDetailsSalary = lib.CalcProDetailsSalary;
                    export import CalcProDetailsWork = lib.CalcProDetailsWork;
                    export import CalcProDetailsWorker = lib.CalcProDetailsWorker;
                    export import CalcProResults = lib.CalcProResults;
                    export import CalcResults = lib.CalcResults;
                    export import CalcTopIcons = lib.CalcTopIcons;
                    export import Certificates = lib.Certificates;
                    export import CmsArticle = lib.CmsArticle;
                    export import CmsList = lib.CmsList;
                    export import Contact = lib.Contact;
                    export import ContactList = lib.ContactList;
                    export import Contract = lib.Contract;
                    export import ContractList = lib.ContractList;
                    export import Credentials = lib.Credentials;
                    export import CreditTransfer = lib.CreditTransfer;
                    export import Datepicker = lib.Datepicker;
                    export import DatepickerPopup = lib.DatepickerPopup;
                    export import EmailMessage = lib.EmailMessage;
                    export import EmailMessageList = lib.EmailMessageList;
                    export import ESalaryAdmin = lib.ESalaryAdmin;
                    export import FeedbackForm = lib.FeedbackForm;
                    export import Import = lib.Import;
                    export import LanguageSelector = lib.LanguageSelector;
                    export import LoginButton = lib.LoginButton;
                    export import NaviSitemap = lib.NaviSitemap;
                    export import NaviStarterLayout = lib.NaviStarterLayout;
                    export import ProductInfoHeaders = lib.ProductInfoHeaders;
                    export import ReportList = lib.ReportList;
                    export import ShopCard = lib.ShopCard;
                    export import ShopCards = lib.ShopCards;
                    export import SmsMessage = lib.SmsMessage;
                    export import SmsMessageList = lib.SmsMessageList;
                    export import Spinner = lib.Spinner;
                    export import Switch = lib.Switch;
                    export import UserInfo = lib.UserInfo;
                    export import WorkerCalculations = lib.WorkerCalculations;
                    export import WorkerDetails = lib.WorkerDetails;
                    export import WorkerList = lib.WorkerList;
                    export import WorkerSelect = lib.WorkerSelect;
                }
                namespace directives {
                    export import AppendNodeDirective = lib.AppendNodeDirective;
                    export import IfRoleDirective = lib.IfRoleDirective;
                    export import LoaderDirective = lib.LoaderDirective;
                    export import ValidatorFunctions = lib.ValidatorFunctions;
                }
                namespace filters {
                    export import TranslateFilter = lib.TranslateFilter;
                }
                namespace interceptors {
                    export import LoaderInterceptor = lib.LoaderInterceptor;
                }
                namespace services {
                    export import AccountService = lib.AccountService;
                    export import AlertService = lib.AlertService;
                    export import AuthorizedAccountService = lib.AuthorizedAccountService;
                    export import CalculationsService = lib.CalculationsService;
                    export import CertificateService = lib.CertificateService;
                    export import CmsService = lib.CmsService;
                    export import ContactService = lib.ContactService;
                    export import ContractService = lib.ContractService;
                    export import CredentialService = lib.CredentialService;
                    export import EmailMessageService = lib.EmailMessageService;
                    export import ESalaryPaymentService = lib.ESalaryPaymentService;
                    export import IService = lib.IService;
                    export import NaviService = lib.NaviService;
                    export import Ng1Translations = lib.Ng1Translations;
                    export import OnboardingService = lib.OnboardingService;
                    export import PaymentService = lib.PaymentService;
                    export import PayrollService = lib.PayrollService;
                    export import ProfileSearchParameters = lib.ProfileSearchParameters;
                    export import ProfilesService = lib.ProfilesService;
                    export import ReportsService = lib.ReportsService;
                    export import SalaxySitemapNode = lib.SalaxySitemapNode;
                    export import SalaxySitemapSection = lib.SalaxySitemapSection;
                    export import SessionService = lib.SessionService;
                    export import SignatureMethod = lib.SignatureMethod;
                    export import SignatureService = lib.SignatureService;
                    export import SmsMessageService = lib.SmsMessageService;
                    export import TaxCardService = lib.TaxCardService;
                    export import UiHelpers = lib.UiHelpers;
                    export import VerificationService = lib.VerificationService;
                    export import WizardService = lib.WizardService;
                    export import WizardStep = lib.WizardStep;
                    export import WorkersService = lib.WorkersService;
                }
            }
        }
    }
    export namespace salaxy {
        namespace api {
            export import AjaxNg1 = lib.AjaxNg1;
        }
        namespace ng1 {
            export import dictionary = lib.dictionary;
            namespace controllers {
                export import AccountAuthorizationController = lib.AccountAuthorizationController;
                export import AccountController = lib.AccountController;
                export import AccountTestController = lib.AccountTestController;
                export import AuthorizationSwitchController = lib.AuthorizationSwitchController;
                export import AvatarController = lib.AvatarController;
                export import CalcChartController = lib.CalcChartController;
                export import CalculationsController = lib.CalculationsController;
                export import Calculator2016Controller = lib.Calculator2016Controller;
                export import CertificateController = lib.CertificateController;
                export import CmsController = lib.CmsController;
                export import CompanyOnboardingWizardController = lib.CompanyOnboardingWizardController;
                export import ContactController = lib.ContactController;
                export import ContractController = lib.ContractController;
                export import CreateAccountWizardController = lib.CreateAccountWizardController;
                export import CredentialController = lib.CredentialController;
                export import CreditTransferController = lib.CreditTransferController;
                export import DatepickerController = lib.DatepickerController;
                export import DatepickerPopupController = lib.DatepickerPopupController;
                export import EmailMessageController = lib.EmailMessageController;
                export import ESalaryPaymentController = lib.ESalaryPaymentController;
                export import ImportController = lib.ImportController;
                export import InputController = lib.InputController;
                export import MessageController = lib.MessageController;
                export import ModalGenericDialogController = lib.ModalGenericDialogController;
                export import ModalDocumentPreviewController = lib.ModalDocumentPreviewController;
                export import NaviController = lib.NaviController;
                export import OnboardingController = lib.OnboardingController;
                export import ProfilesController = lib.ProfilesController;
                export import ReportsController = lib.ReportsController;
                export import SessionController = lib.SessionController;
                export import SignatureController = lib.SignatureController;
                export import SmsMessageController = lib.SmsMessageController;
                export import SpinnerController = lib.SpinnerController;
                export import SwitchController = lib.SwitchController;
                export import TabController = lib.TabController;
                export import TabsController = lib.TabsController;
                export import TaxCard2019Controller = lib.TaxCard2019Controller;
                export import WizardController = lib.WizardController;
                export import WorkerInfoController = lib.WorkerInfoController;
                export import WorkerOnboardingWizardController = lib.WorkerOnboardingWizardController;
                export import WorkersController = lib.WorkersController;
                export import WorkerWizardController = lib.WorkerWizardController;
            }
            namespace components {
                namespace formControls {
                    export import Datepicker = lib.Datepicker;
                    export import DatepickerPopup = lib.DatepickerPopup;
                    export import FormGroup = lib.FormGroup;
                    export import Input = lib.Input;
                    export import InputCalcRowType = lib.InputCalcRowType;
                    export import InputOccupationType = lib.InputOccupationType;
                    export import InputWorker = lib.InputWorker;
                    export import Tabs = lib.Tabs;
                    export import Tab = lib.Tab;
                    export import ValidationSummary = lib.ValidationSummary;
                }
                export import AccountInfo = lib.AccountInfo;
                export import AccountInfoWorker = lib.AccountInfoWorker;
                export import AuthCard = lib.AuthCard;
                export import AuthInfoHeaders = lib.AuthInfoHeaders;
                export import AuthSwitch = lib.AuthSwitch;
                export import AuthorizingAccounts = lib.AuthorizingAccounts;
                export import Avatar = lib.Avatar;
                export import Calc = lib.Calc;
                export import CalcChart = lib.CalcChart;
                export import CalcDetailsExpenses = lib.CalcDetailsExpenses;
                export import CalcDetailsSalary = lib.CalcDetailsSalary;
                export import CalcDetailsWork = lib.CalcDetailsWork;
                export import CalcDetailsWorker = lib.CalcDetailsWorker;
                export import CalcList = lib.CalcList;
                export import CalcNewIntro = lib.CalcNewIntro;
                export import CalcOverviewPanels = lib.CalcOverviewPanels;
                export import CalcPayButton = lib.CalcPayButton;
                export import CalcPro = lib.CalcPro;
                export import CalcProDetailsSalary = lib.CalcProDetailsSalary;
                export import CalcProDetailsWork = lib.CalcProDetailsWork;
                export import CalcProDetailsWorker = lib.CalcProDetailsWorker;
                export import CalcProResults = lib.CalcProResults;
                export import CalcResults = lib.CalcResults;
                export import CalcTopIcons = lib.CalcTopIcons;
                export import Certificates = lib.Certificates;
                export import CmsArticle = lib.CmsArticle;
                export import CmsList = lib.CmsList;
                export import Contact = lib.Contact;
                export import ContactList = lib.ContactList;
                export import Contract = lib.Contract;
                export import ContractList = lib.ContractList;
                export import Credentials = lib.Credentials;
                export import CreditTransfer = lib.CreditTransfer;
                export import Datepicker = lib.Datepicker;
                export import DatepickerPopup = lib.DatepickerPopup;
                export import EmailMessage = lib.EmailMessage;
                export import EmailMessageList = lib.EmailMessageList;
                export import ESalaryAdmin = lib.ESalaryAdmin;
                export import FeedbackForm = lib.FeedbackForm;
                export import Import = lib.Import;
                export import LanguageSelector = lib.LanguageSelector;
                export import LoginButton = lib.LoginButton;
                export import NaviSitemap = lib.NaviSitemap;
                export import NaviStarterLayout = lib.NaviStarterLayout;
                export import ProductInfoHeaders = lib.ProductInfoHeaders;
                export import ReportList = lib.ReportList;
                export import ShopCard = lib.ShopCard;
                export import ShopCards = lib.ShopCards;
                export import SmsMessage = lib.SmsMessage;
                export import SmsMessageList = lib.SmsMessageList;
                export import Spinner = lib.Spinner;
                export import Switch = lib.Switch;
                export import UserInfo = lib.UserInfo;
                export import WorkerCalculations = lib.WorkerCalculations;
                export import WorkerDetails = lib.WorkerDetails;
                export import WorkerList = lib.WorkerList;
                export import WorkerSelect = lib.WorkerSelect;
            }
            namespace directives {
                export import AppendNodeDirective = lib.AppendNodeDirective;
                export import IfRoleDirective = lib.IfRoleDirective;
                export import LoaderDirective = lib.LoaderDirective;
                export import ValidatorFunctions = lib.ValidatorFunctions;
            }
            namespace filters {
                export import TranslateFilter = lib.TranslateFilter;
            }
            namespace interceptors {
                export import LoaderInterceptor = lib.LoaderInterceptor;
            }
            namespace services {
                export import AccountService = lib.AccountService;
                export import AlertService = lib.AlertService;
                export import AuthorizedAccountService = lib.AuthorizedAccountService;
                export import CalculationsService = lib.CalculationsService;
                export import CertificateService = lib.CertificateService;
                export import CmsService = lib.CmsService;
                export import ContactService = lib.ContactService;
                export import ContractService = lib.ContractService;
                export import CredentialService = lib.CredentialService;
                export import EmailMessageService = lib.EmailMessageService;
                export import ESalaryPaymentService = lib.ESalaryPaymentService;
                export import IService = lib.IService;
                export import NaviService = lib.NaviService;
                export import Ng1Translations = lib.Ng1Translations;
                export import OnboardingService = lib.OnboardingService;
                export import PaymentService = lib.PaymentService;
                export import PayrollService = lib.PayrollService;
                export import ProfileSearchParameters = lib.ProfileSearchParameters;
                export import ProfilesService = lib.ProfilesService;
                export import ReportsService = lib.ReportsService;
                export import SalaxySitemapNode = lib.SalaxySitemapNode;
                export import SalaxySitemapSection = lib.SalaxySitemapSection;
                export import SessionService = lib.SessionService;
                export import SignatureMethod = lib.SignatureMethod;
                export import SignatureService = lib.SignatureService;
                export import SmsMessageService = lib.SmsMessageService;
                export import TaxCardService = lib.TaxCardService;
                export import UiHelpers = lib.UiHelpers;
                export import VerificationService = lib.VerificationService;
                export import WizardService = lib.WizardService;
                export import WizardStep = lib.WizardStep;
                export import WorkersService = lib.WorkersService;
            }
        }
    }
}
declare module "namespace/index" {
    export * from "namespace/namespace";
}
declare module "index" {
    export * from "ajax/index";
    export * from "components/index";
    export * from "controllers/index";
    export * from "directives/index";
    export * from "filters/index";
    export * from "i18n/index";
    export * from "interceptors/index";
    export * from "services/index";
    export * from "templates/index";
    export * from "namespace/index";
}
