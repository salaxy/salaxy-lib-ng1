Swagger and Schema sources
==========================

To update Swagger and Schema files, run: `grunt fetch-test-api`. 
This will fetch the API files from https://test-api.salaxy.com.

Alternatively, you can run `grunt fetch-local-api`to fetch the files from the localhost.
The correct files will be fetched from test by deployment scripts.


Swagger customization
=====================

Use this guildeline when you update to the latest version of Swagger.
Please update teh version numbers below.

- Download Swagger UI from from https://github.com/swagger-api/swagger-ui
- Current version: 2.2.8
- Copy the dist folder to this swagger folder
- Open index.html
 - "http://petstore.swagger.io/v2/swagger.json" => "/docs/swagger/salaxy-02.json"
 - docExpansion: "none" => "list"
