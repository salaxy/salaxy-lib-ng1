Environment specific files
==========================

TO GET RID OF THE 404 ERROR IN CONSOLE, 
copy file /env/test/settings.js to this folder (assuming you want to use test environment).

This folder is excluded from GIT so that it can be different in different environments (e.g. localhost, real server).

At the moment, the only environment specific file needed is settings.js.
Even if you do not have it, the library will default to using https://test-api.salaxy.com, 
which is probably the right choice for you.
