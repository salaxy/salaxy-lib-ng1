<h2 class="page-header">
  Input attributes
</h2>
<p class="lead">
  The basic attributes in the <code>sxy-form</code> controls are
  model, label, type, readOnly, disable and require.
</p>
<table class="table">
  <col width="15%">
  <col width="40%">
  <col width="45%">
  <thead>
    <tr>
      <th>Attribute</th>
      <th>Description</th>
      <th>Default</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>model</th>
      <td>
        Model in the form. Currently always starts with <code>form.</code> to bind to a property of the 
        main data object of the form (later there may be other data objects in form - e.g. metadata).
        For fieldset, this may also be just <code>form</code> to bind to main model in form (not a property).
      </td>
      <td>
        This property is required.
      </td>
    </tr>
    <tr>
      <th>label</th>
      <td>
        Possibility to override the label. Translated by default, if translation not found, shown as is.
        If starts with dot, will be interpreted as just the last part of the key,
        e.g. <code>label=".specialLabel"</code>  will fetch key <code>"SALAXY.MODEL.[TypeName].[propName].specialLabel"</code>.
      </td>
      <td>
        Translation of <code>"SALAXY.MODEL.[TypeName].[propName].title"</code>.
        If not found, <code>"#[propName]"</code>.
      </td>
    </tr>
    <tr>
      <th>type</th>
      <td>
        Input type.
        Type is validated or set based on the JSON Schema type of the model:
        Not all types are valid for all data types.
      </td>
      <td>
        Suitable type determined based on JSON Schema type and format.
      </td>
    </tr>
    <tr>
      <th>read-only</th>
      <td>
        True or false forces the read-only mode on the input regardless of the default value.
        The read-only mode does not look like a form control, but is plain text (or other visualization).
      </td>
      <td>
        The default value comes from the API metadata (readOnly in JSON schema) or 
        it may also come from the form or from the business logic, UI logic or user roles (e.g. read-only access role).
      </td>
    </tr>
    <tr>
      <th>disable</th>
      <td>
        True or false forces the input being disabled: It is still visibly as form element,
        but it is not editable and it is grayed out.
      </td>
      <td>Currently, always false. May be configurable in metadata (API model) later.</td>
    </tr>
    <tr>
      <th>require</th>
      <td>True or false forces the required mode on the input.</td>
      <td>The default value comes from the API metadata (required in JSON schema).</td>
    </tr>
  </tbody>
</table>
<h3>Examples</h3>
<div>
  <div ng-init="$ctrl.testModelReadOnly = { default: 'Default text value', readOnlyText: 'Read-only text value' }">
    <form name="formReadOnly" class="form-condensed">
      <sxy-form open-api="/assets/schemas/demo.schema.json" type="Attributes" auto-gen="false" debugger="true" model="$ctrl.testModelReadOnly">
        <div>
          <fieldset>
            <legend>Read-only</legend>
            <p>Read-only values set one-by-one. The form is also <code>condensened</code>.</p>
            <sxy-input model="form.default" label="Default: Not read-only"></sxy-input>
            <sxy-input model="form.default" label="Read-only attribute" read-only="true"></sxy-input>
            <sxy-input model="form.readOnlyText" label="Read-only in metadata"></sxy-input>
            <sxy-input model="form.readOnlyText" label="Read-only in metadata, but overriden" read-only="false"></sxy-input>
          </fieldset>
        </div>
      </sxy-form>
    </form>
  </div>
  <div ng-init="$ctrl.testModelReadOnlyForm = { default: 'Default text value', readOnlyText: 'Read-only text value' }">
    <form name="formReadOnlyForm" class="form-condensed">
      <sxy-form open-api="/assets/schemas/demo.schema.json" read-only="true" type="Attributes" auto-gen="false" debugger="true" model="$ctrl.testModelReadOnlyForm">
        <div>
          <fieldset>
            <legend>Read-only, entire form</legend>
            <p>
              Read-only -attribute may also be set on the entire form.
              Input-specific <code>read-only=false</code> attribute overrides the form-level sepcification.
              The form is also <code>condensened</code>.
            </p>
            <sxy-input model="form.default" label="Not read-only in metadata"></sxy-input>
            <sxy-input model="form.overrideInHtml" label="Another non read-only, but overriden" read-only="false"></sxy-input>
            <sxy-input model="form.readOnlyText" label="Read-only in metadata"></sxy-input>
            <sxy-input model="form.readOnlyText" label="Read-only in metadata, but overriden" read-only="false"></sxy-input>
          </fieldset>
        </div>
      </sxy-form>
    </form>
  </div>
  <div ng-init="$ctrl.testModelDisabled = { default: 'Default text value', readOnlyText: 'Read-only text value' }">
    <form name="formDisabled">
      <sxy-form open-api="/assets/schemas/demo.schema.json" type="Attributes" auto-gen="false" debugger="true" model="$ctrl.testModelReadOnly">
        <div>
          <fieldset>
            <legend>Disabled</legend>
            <p>
              A disabled field is conceptually a field, that the user cannot edit at the moment,
              but should be editable after user does some other action or under some other circumstances.
              As such, disabled can only be marked in UI, it does not make sense in JSON schema.
              This example form has normal padding (not condensed).
            </p>
            <sxy-input model="form.default" label="Default: Not disabled"></sxy-input>
            <sxy-input model="form.default" label="Disabled" disable="true"></sxy-input>
            <sxy-input model="form.readOnlyText" label="Read-only (schema) AND disabled" disable="true"></sxy-input>
            <sxy-input model="form.default" label="Read-only (attr) AND disabled" read-only="true" disable="true"></sxy-input>
          </fieldset>
        </div>
      </sxy-form>
    </form>
  </div>
  <div ng-init="$ctrl.testModelRequired = {}">
    <form name="formRequired" class="form-condensed">
      <sxy-form open-api="/assets/schemas/demo.schema.json" type="Attributes" auto-gen="false" debugger="true" model="$ctrl.testModelRequired">
        <div>
          <fieldset>
            <legend>Required</legend>
            <p>
              A <code>sxy-input</code> can be marked required or a set of properties can be marked required in the JSON schema.
              Note that in JSON schema, the required is an array of properties, not a "property of a property".
              This example form is also <code>condensened</code>.
            </p>
            <sxy-input model="form.default" label="Default: Not required"></sxy-input>
            <sxy-input model="form.overrideInHtml" label="Require in HTML" require="true"></sxy-input>
            <sxy-input model="form.requiredInJson" label="Required in JSON Schema"></sxy-input>
            <sxy-input model="form.requiredInJsonOverrideInHtml" label=" ...overriden in HTML" require="false"></sxy-input>
            <salaxy-validation-summary form="formRequired"></salaxy-validation-summary>
          </fieldset>
        </div>
      </sxy-form>
    </form>
  </div>
</div>
<h3>Differences to HTML INPUT attributes</h3>
<p>
  Please note, that some attributes are similar to HTML attributes, but they behave in a differently in details.
  Because of this, they are also named differently than their HTML counterparts.
</p>
<table class="table">
  <thead>
    <tr>
      <th>SXY-IPT attribute</th>
      <th>HTML attribute</th>
      <th>Differences</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        read-only<br />
        <small><code style="white-space: nowrap;">&lt;sxy-ipt read-only="true"...</code></small>
      </td>
      <td>
        readonly<br />
        <small><code style="white-space: nowrap;">&lt;input readonly type=...</code></small>
      </td>
      <td>
        The value is boolean (true/1 or false/0). 
        Non-existent attribute or empty/null uses the default value from metadata (API model)
        or from the form. Also, the input will become fully read-only view without any form control.
        In HTML, the attribute has no value, it either exists or not (=false).
      </td>
    </tr>
    <tr>
      <td>
        disable<br />
        <small><code style="white-space: nowrap;">&lt;sxy-ipt disable="true"...</code></small>
      </td>
      <td>
        disabled<br />
        <small><code style="white-space: nowrap;">&lt;input disabled type=...</code></small>
      </td>
      <td>
        The SXY-IPT value is boolean (true/1 or false/0).
        Non-existent attribute or empty/null uses the default value: Currently always false, but may later come from metadata.
        In HTML, the attribute has no value, it either exists or not (=false).
      </td>
    </tr>
    <tr>
      <td>
        require<br />
        <small><code style="white-space: nowrap;">&lt;sxy-ipt require="true"...</code></small>
      </td>
      <td>
        required<br />
        <small><code style="white-space: nowrap;">&lt;input required type=...</code></small>
      </td>
      <td>
        The SXY-IPT value is boolean (true/1 or false/0). 
        Non existent attribute or empty/null uses the default value from metadata (API model).
        In HTML, the attribute has no value, it either exists or not (=false).
      </td>
    </tr>
  </tbody>
</table>
<h2>Comparison to JSON Schema</h2>
<p>
  Also, in JSON schema, the the above attributes have quite a different meaning and syntax:
</p>
<ul>
  <li>
    Marking a property <code>readOnly</code> in JSON schema will mark also the property <code>read-only</code> in the UI.
    You can override that in the SXY-IPT attribute.
  </li>
  <li>
    In JSON schema, the <code>required</code> is very different than in HTML:
    Instead of marking a property as required, you provide an array of required properties in the schema.
    This is array is reflected in a SXY-IPT input and you can override that in the SXY-IPT attribute.
  </li>
  <li><code>Disabled</code> is not a concept in JSON schema - it would not make sense there.</li>
</ul>
