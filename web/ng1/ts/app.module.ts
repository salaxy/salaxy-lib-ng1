import * as angular from "angular";

import { RouteHelperProvider, SitemapHelper, SalaxySitemapNode, SessionService, Ng1Translations, OnboardingService } from "@salaxy/ng1";

import { AssureBo, Example } from "./components";
import { DemoDataController, DocsComponentsController, ScheduleController, UiHelperDemoController } from "./controllers";
import { DocsDirectives, DocsFilters, DocsService } from "./other";
import sitemap from "./sitemap.json";

/**
 * AngularJS (NG1) demo site Angular application.
 */
export const module = angular.module("Salaxy.ng1", ["ngRoute", "salaxy.ng1.components.all", "color.picker"])
  .config(["RouteHelperProvider",
    function (routeHelperProvider: RouteHelperProvider) {
      routeHelperProvider
        .setCustomSectionRoot("/ng1")
        .homeDefault("about")
        .customSection("about")
        .customSection("docs")
        .customSection("forms")
        .customSection("demos")
        .customSection("service-model")
        .defaultSection("taxcards") // This not enabled on a default web site.
        .addPersonRoutes()
        // CRUD edge cases: These crud pages are not on normal web pages, just in this ng1 demo site.
        .when("/crud/:type/:viewName?/:crudItemId*?", {
          templateUrl: function (path) {
            return "/ng1/demos/crud/" + (path.viewName ? (path.type + "-" + path.viewName) : path.type) + ".html";
          }
        }, "crud")
        .commit()
        ;
    }
  ])
  .constant<SalaxySitemapNode[]>("SITEMAP", sitemap)
  .config(["SITEMAP", function(sitemap: SalaxySitemapNode[]) {
    const standard = SitemapHelper.getAllRolesSiteMap();
    standard[0].url = "#/home";
    /*
    // NOTE: The service models concept is under development
    standard.push({
      title: "Palvelumallit ja asetukset",
      url: "#/service-model",
      id: "serviceModel",
      roles: "test",
      children: [
        {
          title: "Luo uusi palvelumalli",
          url: "#/service-model/create",
          id: "create"
        },
        {
          title: "Palvalumallin sivusto",
          url: "#/service-model/sitemap",
          id: "sitemap",
          hidden: true,
        },
        {
          title: "Palvalumallin ulkoasu",
          url: "#/service-model/ui",
          id: "sitemap",
          hidden: true,
        },
      ]
    });
    */
    sitemap.find((x) => x.id === "company-site").children.push(...standard);
  }])
  .run(["SessionService",
    function(sessionService: SessionService) {
      sessionService.signInErrorUrl = "/home/error-oauth";
    }
  ])
  // Just for location change tracking:
  .run(["OnboardingService", function(onboardingService: OnboardingService) {}])
  .run(["Ng1Translations", function(translate: Ng1Translations) {
    translate.setLanguage("fi");
    translate.addDictionary("fi", (window as any).translations);
  }])
  .controller("DemoDataController", DemoDataController)
  .controller("DocsComponentsController", DocsComponentsController)
  .controller("ScheduleController", ScheduleController)
  .controller("UiHelperDemoController", UiHelperDemoController)
  .component("sxydocExample", new Example())
  .component("sxydocAssureBo", new AssureBo())
  .filter("sxydevCode", DocsFilters.sxydevCode)
  .filter("sxydevMarkdown", DocsFilters.sxydevMarkdown)
  .filter("sxydevType", DocsFilters.sxydevType)
  .service("DocsService", DocsService)
  .directive("sxydevInclude", DocsDirectives.sxydevInclude)
  ;
