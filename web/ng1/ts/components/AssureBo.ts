import * as angular from "angular";

import { DemoDataController } from "../controllers";

/** Renders a Salaxy cpde example within a HTML documnet */
export class AssureBo implements angular.IComponentOptions {

    /** Component bindings */
    public bindings = {

      /** Mode defines Which view to show. "calc" is default. */
      mode: "@",
    };

    /** Main example html code is in the "main" element. */
    public transclude = {
      /** Main example that is shown in the current item has been selected. */
      "main": "main",
      // Add other slots (anon, auth etc. if necessary)
    };

    /** Add controller if new functionality is required. */
    public controller = DemoDataController;

    /** Template of the component. */
    public templateUrl = "/ng1/templates/AssureBo.html";
}
