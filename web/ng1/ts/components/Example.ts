import * as angular from "angular";

/** Renders a Salaxy code example within a HTML documnet */
export class Example implements angular.IComponentOptions {

    /** Component bindings */
    public bindings = {};

    /** Content is the example code */
    public transclude = true;

    /** Add controller if new functionality is required. */
    public controller = null;

    /** Template of the component. */
    public template = `<small class="sxydoc-example">Code example</small><div class="sxydoc-example"><ng-transclude>No content in the example</ng-transclude></div>`;
}
