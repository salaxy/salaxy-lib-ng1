import * as angular from "angular";

import { Calculation, Calculations, EmploymentListItem } from "@salaxy/core";
import { AlertService, EditDialogKnownActions, UiHelpers } from "@salaxy/ng1";



/** Provides misc. methods for generating demo data fro examples. */
export class DemoDataController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers", "AlertService", "Calculations"];

  /** In AssureBo component defines the mode: Which view to show. "calc" is default. */
  public mode: "calc" | "employment";

  /** AssureBo may select a current calculation here. */
  public currentCalc: Calculation;

  /** AssureBo may select an employment relation (list item) here. */
  public currentEmployment: EmploymentListItem;

  constructor(private uiHelpers: UiHelpers, private alertService: AlertService, private calculations: Calculations) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
  }

  /** Adds a non-obtrusive alert message (top-right corner) */
  public addAlert(alertHtml: string, type: "danger" | "warning" | "success" | "info") {
    switch (type) {
      case "danger":
        this.alertService.addError(alertHtml);
        break;
      case "warning":
        this.alertService.addWarning(alertHtml);
        break;
      case "success":
        this.alertService.addSuccess(alertHtml);
        break;
      case "info":
        this.alertService.addInfo(alertHtml);
        break;
    }
  }

  /** Opens a dialog for selecting a calculation */
  public selectCalculation(type: "paid" | "draft" | "new") {
    if (type == "new") {
      this.currentCalc = this.calculations.getBlank();
    } else {
      this.uiHelpers.openSelectCalcs(type, "Select a calculation for demo").then((result) => {
        if (result.action == EditDialogKnownActions.Ok && result.item.length > 0) {
          const loader = this.uiHelpers.showLoading();
          this.calculations.getSingle(result.item[0].id).then((calc) => {
            this.currentCalc = calc;
            loader.dismiss();
          });
        }
      });
    }
  }

}
