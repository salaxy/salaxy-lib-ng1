import * as angular from "angular";

import { ClassDoc, ClassMemberDoc, ComponentDoc, EnumDoc, RootDeclarationReflection } from "@salaxy/core";
import { I18nTerm, InputEnumOption, NaviService, Ng1Translations, SitemapHelper, UiHelpers } from "@salaxy/ng1";

import { DocsService } from "../other/DocsService";


/**
 * Documentation controller that helps displaying the Components of the library from Typedoc documentation.
 * NOTE: This controller is only for library documentation purposes.
 * It may be removed from the @salaxy/ng1-library without that being a breaking change.
 */
export class DocsComponentsController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["DocsService", "$scope", "$location", "$routeParams", "Ng1Translations", "UiHelpers", "NaviService"];

  /**
   * Currently selected controller or component if any.
   * You should be able to determine the type based on currentType and it should have currentName as name.
   */
  public current: ClassDoc | ComponentDoc | EnumDoc;

  /** Type of the current item identified as a collection / the view name. */
  public currentType: "components" | "classes" | "enums" | "controllers" | "filters" | "directives";

  /** Name of the current documentation item (item in path). */
  public currentName: string;

  /** List filter: applies to components and controllers. */
  public filter: {
    /** Component or controller group. */
    group?: string,
    /** Class kind */
    kind?: string,
    /** Salaxy library name (e.g. "@salaxy/core" or "@salaxy/ng1") */
    lib?: string,
    /** Does a search to the class/interface name, */
    name?: string,
  } = {};

  /**
   * Creates a new DocsComponentsController
   * @ignore
   */
  constructor(
    private docsService: DocsService,
    private $scope: angular.IScope,
    private $location: angular.ILocationService,
    private $routeParams: any,
    private translations: Ng1Translations,
    private uiHelpers: UiHelpers,
    private naviService: NaviService,
  ) {
  }

  /**
   * Implement IController
   */
  public $onInit = () => {
    if (this.docsService.isReady) {
      this.init();
    }
    this.docsService.subscribe(this.$scope, () => { this.init(); });
  }

  /** Gets or sets the active tab - stored in DocsService */
  public get currentTab(): string {
    return this.docsService.currentTab;
  }
  public set currentTab(value: string) {
    this.docsService.currentTab = value;
  }

  /** All classes and interfaces in NG1 and Core projects. */
  public get classes(): ClassDoc[] {
    return this.docsService.classes;
  }

  /** All enumerations in NG1 and Core projects. */
  public get enums(): EnumDoc[] {
    return this.docsService.enums;
  }

  /** Components in the NG1-project. */
  public get components(): ComponentDoc[] {
    return this.docsService.components;
  }

  /** Controllers in the NG1-project. */
  public get controllers(): ClassDoc[] {
    return this.docsService.controllers;
  }

  /** Filters in the NG1-project. */
  public get filters(): ClassMemberDoc[] {
    return this.docsService.filters;
  }

  /** Filters in the NG1-project. */
  public get directives(): (ClassDoc | ClassMemberDoc)[] {
    return this.docsService.directives;
  }

  /** Grouping for controllers and components */
  public get groups(): { [key: string]: string } {
    if (this.filter.lib) {
      return this.docsService.groups[this.filter.lib];
    }
    return this.docsService.groups.all;
  }

  /** Gets the component groups based on navigation for listing. */
  public get componentGroups(): InputEnumOption[] {
    var isComponentsLoaded = !!this.components;
    return this.uiHelpers.cache(this, "componentGroups", () => {
      let totalCount = 0;
      const result: InputEnumOption[] = SitemapHelper.getSection(this.naviService.getSitemap(), "components")
        .children.filter((x) => !x.hidden).map((x) => {
          const url = x.url.replace("/*", "");
          const value = url.substr(url.lastIndexOf("/") + 1);
          const count = this.components?.filter((x) => x.group === value).length || 0;
          totalCount += count;
          return {
            text: x.title,
            value,
            ui: {
              url,
              count,
            },
          };
        })
      result.push({
        text: "Other",
        value: null,
        ui: {
          url: null,
          count: isComponentsLoaded ? this.components.length - totalCount : 0,
        }
      });
      return result;
    }, () => isComponentsLoaded)
  }

  /**
   * Moves to another location in the documentation.
   * @param item That should be linked to
   */
  public goto(item: RootDeclarationReflection) {
    let viewName = "classes";
    switch (item.kind) {
      case "enum":
        viewName = "enums";
        break;
      case "class":
      case "interface":
        viewName = "classes";
        break;
      case "controller":
      case "controllerBase":
        viewName = "controllers";
        break;
      case "component":
        viewName = "components";
        break;
      default:
        throw new Error("Item kind not supported: " + item.kind);
    }
    this.$location.url(`/docs/${viewName}/${item.group}/${item.name}`);
  }

  /** Gets a link URL to component template in GitLab. */
  public getTemplateUrl(): string {
    return this.currentType === "components" ?
      (this.current as ComponentDoc).defaultTemplate.replace("salaxy-components/", "https://gitlab.com/salaxy/salaxy-lib-ng1/-/tree/master/src/templates/")
      : null;
  }

  /** Gets a link URL to source file in GitLab. */
  public getGitUrl(source: string): string {
    if (!source) {
      return null;
    }
    // In some cases the path is "salaxy-lib-ng1/src/ts/components/personal/AccountAuthorizationPerson.ts" and sometimes "components/personal/AccountAuthorizationPerson.ts"
    // TODO: Check whether this was TypeScript version or something else and harmonize so that the format is always correct.
    return "https://gitlab.com/salaxy/salaxy-lib-ng1/-/tree/master/src/ts/" + source.replace("salaxy-lib-ng1/src/ts/", "");
  }

  /** Gets the data transmitted in the route / url */
  public getRouteData(): {
    /** Name of the item to show */
    name: string,

    /** Grouping of the item */
    group: string,

    /** ViewName parameter in the route. */
    viewName: "components" | "classes" | "enums" | "example",
  } {
    if (!this.$routeParams) {
      return {} as any;
    }
    const fullId = (this.$routeParams.crudItemId || "").split("/") as string[];
    return {
      name: fullId[1] || null,
      viewName: this.$routeParams.viewName || null,
      group: fullId[0] || null,
    };
  }

  /** Gets the translation texts for the current component. */
  public getTranslations(): { [key: string]: I18nTerm } {
    return this.uiHelpers.cache(this, "translations", () => {
      switch (this.current?.kind) {
        case "component":
          return this.translations.getComponentTranslations(this.current.name + "Component");
        case "interface":
          return this.translations.getModelTranslations(this.current.name);
        default:
          return {
            error: {
              key: "error",
              fi: "Virhe: Vain komponentit tuettu tässä käännösmenetelmässä.",
              en: "Error: Only component tranlations supported",
              sv: "",
            }
          }
      }
    }, () => {
      return this.current.name;
    })
      ;
  }

  /** Fired when include loaded: Catch 404 */
  public exampleLoaded(angularEvent: angular.IAngularEvent, src: string) {
    console.debug("Example loaded:", angularEvent);
  }

  private init(): void {
    const routeData = this.getRouteData();
    this.currentType = this.currentType || (routeData.viewName === "example" ? "components" : routeData.viewName);
    this.currentName = this.currentName || routeData.name;

    if (this.currentName) {
      switch (this.currentType) {
        case "classes":
        case "controllers":
          this.current = this.classes.find((x) => x.name === this.currentName);
          this.filter.group = this.current?.group;
          this.filter.kind = this.current?.kind;
          if (this.filter.kind === "controllerBase") {
            // Use more generic form in filters.
            this.filter.kind = "controller";
          }
          break;
        case "components":
          this.current = this.components.find((x) => x.name === this.currentName);
          this.filter.group = this.current?.group;
          this.filter.kind = this.current?.kind;
          break;
        case "enums":
          this.current = this.enums.find((x) => x.name === this.currentName);
          break;
        default:
          throw new Error(`Viewname ${this.currentType} currently not supported.`);
      }
      if (!this.current) {
        throw new Error(`Object with name ${this.currentName} not found.`);
      }
      this.filter.lib = this.current.lib;
    } else {
      this.current = null;
      switch (this.currentType) {
        case "classes":
          if (routeData.group === "ng1" || routeData.group === "core") {
            this.filter.lib = "@salaxy/" + routeData.group;
          } else if (routeData.group) {
            this.filter.group = routeData.group;
          }
          break;
        case "components":
          this.filter.kind = "component";
          this.filter.lib = "@salaxy/ng1";
          this.filter.group = routeData.group;
          break;
        case "controllers":
          this.filter.kind = "controller";
          this.filter.lib = "@salaxy/ng1";
          this.filter.group = routeData.group;
          break;
        case "enums":
          if (routeData.group === "core") {
            this.filter.lib = "@salaxy/core";
          } else {
            this.filter.lib = "@salaxy/ng1";
          }
          break;
        case "filters":
        case "directives":
          break;
        default:
          throw new Error(`Viewname ${this.currentType} currently not supported.`);
      }
    }
  }
}
