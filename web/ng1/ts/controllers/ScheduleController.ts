import * as angular from "angular";
import { UiHelpers } from "@salaxy/ng1";
import { DateRange, Dates, PayrollDetails } from "@salaxy/core";

/** Prototype for future scheduling functionality. */
export class ScheduleController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["UiHelpers"];

  /** TODO: This should be the schedule combined with createNewModel */
  public current: {
    salaryDate: string,
    period: DateRange,
  } = {
    salaryDate: Dates.getToday(),
    period: {} as any,
  };

  public todo = {
    /** Should probably be an enumeration. Probably in the PayrollTemplate. */
    periodicityOptions: {
      monthly: "Kuukausi",
      halfMonthly: "Kaksi kertaa kuussa",
      weekly: "Kahden viikon välein",
      manual: "Muu periodi",
    },
  };

  /**
   * Model for creating a new Payroll or Payroll template.
   * The same model also applies for copy-as-new.
   */
   public createNewModel: {
    /** Input for creating the new tre */
    input: {

      /** One-off instance or recurring template */
      periodicity?: "monthly" | "halfMonthly" | "weekly" | "manual",

      /** Start period for recurrence */
      periodStart?: string,

      /** Count of weeks or months in recurrence */
      periodicityCount?: number,

      /** Number of work days before salaryDate the payroll instance will be created  */
      payrollCreateDays?: number,

      /** If true, the notification is enabled.  */
      notificationEnabled?: boolean,

      /** Number of work days before dueDate the payroll instance will be created  */
      notificationDays?: number,

      /** Custom text for notification */
      notificationText?: string,

      /** E-mail address for the notification */
      notificationEmail?: string,

      /** If true, updates teh calculations from employment */
      updateFromEmployment: boolean,
    },

    /** Preview of the instances created based on this model. Typically 3 insances. */
    preview: {
      /** Salary date */
      salaryDate: string,
      /** Salary period */
      period: DateRange,
      /** When the instance will be created. */
      createDate: string,
      /** When the due date for the payment will be */
      dueDate: string,
      /** When the notification will be. */
      notificationDate: string,
    }[],
  } = {
      input: {
        periodicity: "manual",
        updateFromEmployment: true,
      },
      preview: [],
    };

  constructor(uiHelpers: UiHelpers) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
  }

  /**
   * Called when the periodicity changes: Sets the defaults for each case.
   */
   public onPeriodicityChanged(): void {
    const initData = this.createNewModel.input;
    const current = this.current;
    switch (initData.periodicity) {
      case "monthly":
        initData.periodicityCount = 1;
        current.salaryDate = Dates.asDate(Dates.getTodayMoment().startOf("month").add(1, "month"));
        current.period.start = Dates.asDate(Dates.getTodayMoment().startOf("month"));
        current.period.end = Dates.asDate(Dates.getMoment(current.period.start).add(initData.periodicityCount, "month").subtract(1, "day"));
        initData.payrollCreateDays = 20;
        initData.notificationDays = 5;
        break;
      case "halfMonthly":
        current.salaryDate = Dates.asDate(Dates.getTodayMoment().startOf("month").add(1, "month").add(15, "day"));
        current.period.start = Dates.asDate(Dates.getTodayMoment().startOf("month").add(1, "month"));
        current.period.end = Dates.asDate(Dates.getMoment(current.period.start).add(14, "day"));
        initData.payrollCreateDays = 10;
        initData.notificationDays = 5;
        break;
      case "weekly":
        initData.periodicityCount = 2;
        current.salaryDate = Dates.asDate(Dates.getTodayMoment().startOf("week").add(1, "week").add(1, "day"));
        current.period.start = Dates.asDate(Dates.getTodayMoment().startOf("week").add(1, "day"));
        current.period.end = Dates.asDate(Dates.getMoment(current.period.start).add(initData.periodicityCount, "week").subtract(1, "day"));
        initData.payrollCreateDays = 5;
        initData.notificationDays = 5;
        break;
      case "manual":
        break;
    }
    this.periodDatesChanged();
  }

  /** Called when the dates of periodicity changes */
  public periodDatesChanged(): void {
    this.createNewModel.preview = this.getInstancesWorkflowPreview(1);
    // End date for the case where user first clicks monthly, weekly etc. and then "manual".
    this.current.period.end = this.createNewModel.preview[0].period.end;
  }

  /** Gets the preview of the next instances based on the current parameters. */
  public get instancesPreview() {
    return this.createNewModel.preview;
  }

  /**
   * Gets the preview of the Payroll instances that will be created.
   * @param instanceCount Number of payroll instances that should be previewed.
   * @param beforeDate Instances are only created for create dates that are before this.
   * If you want to use this INSTEAD of instanceCount, just set instanceCount as a very large number.
   */
   public getInstancesWorkflowPreview(instanceCount: number, beforeDate: string = null): any[] {
    const initData = this.createNewModel.input;
    const current = this.current;
    const result = [];
    const loopCount = (initData.periodicity === "manual" ? 1 : instanceCount);
    for (let i = 0; i < loopCount; i++) {
      const workflow = {
        salaryDate: null as string,
        period: {} as DateRange,
        createDate: null as string,
        dueDate: null as string,
        notificationDate: null as string,
      };
      switch (initData.periodicity) {
        case "monthly":
          workflow.salaryDate = Dates.asDate(Dates.getMoment(current.salaryDate).add(initData.periodicityCount * i, "month"));
          workflow.period.start = Dates.asDate(Dates.getMoment(current.period.start).add(initData.periodicityCount * i, "month"));
          workflow.period.end = Dates.asDate(Dates.getMoment(workflow.period.start).add(initData.periodicityCount, "month").subtract(1, "day"));
          break;
        case "halfMonthly":
          if (i % 2 === 0) {
            // Even (including 0=first): Same logic as monthly, except divide i by 2 and period is 15 days (1+14).
            workflow.salaryDate = Dates.asDate(Dates.getMoment(current.salaryDate).add(i / 2, "month"));
            const startMoment = Dates.getMoment(current.period.start).add(i / 2, "month");
            workflow.period.start = Dates.asDate(startMoment);
            workflow.period.end = Dates.asDate(startMoment.add(14, "day"));
          } else {
            // Always add 15 days (1st and 16th etc.). TODO: Add unit test to edge cases here.
            workflow.salaryDate = Dates.asDate(Dates.getMoment(result[i - 1].salaryDate).add(15 * i, "day"));
            // Derive start from previous end day
            workflow.period.start = Dates.asDate(Dates.getMoment(result[i - 1].period.end).add(1, "day"));
            // Derive end from next start day
            workflow.period.end = Dates.asDate(Dates.getMoment(current.period.start).add((i + 1) / 2, "month").subtract(1, "day"));
          }
          break;
        case "weekly":
          workflow.salaryDate = Dates.asDate(Dates.getMoment(current.salaryDate).add(initData.periodicityCount * i, "week"));
          workflow.period.start = Dates.asDate(Dates.getMoment(current.period.start).add(initData.periodicityCount * i, "week"));
          workflow.period.end = Dates.asDate(Dates.getMoment(workflow.period.start).add(initData.periodicityCount, "week").subtract(1, "day"));
          break;
        case "manual":
          workflow.salaryDate = current.salaryDate;
          workflow.period = current.period;
          workflow.createDate = Dates.getToday();
          break;
        default:
          break;
      }
      workflow.createDate = workflow.createDate || Dates.addWorkdays(workflow.salaryDate, -initData.payrollCreateDays);
      workflow.dueDate = Dates.addWorkdays(workflow.salaryDate, -3);
      if (workflow.createDate > workflow.dueDate) {
        workflow.createDate = workflow.dueDate;
      }
      if (workflow.createDate < Dates.getToday()) {
        workflow.createDate = Dates.getToday();
      }
      workflow.notificationDate = initData.notificationEnabled ? Dates.addWorkdays(workflow.dueDate, -initData.notificationDays) : null;
      if (workflow.notificationDate <= Dates.getToday()) {
        workflow.notificationDate = null;
      }
      if (!beforeDate || workflow.createDate < beforeDate) {
        result.push(workflow);
      } else {
        return result;
      }
    }
    return result;
  }

  /**
   * Returns true if the date is overdue (smaller than today).
   * @param date Date to compare to today.
   */
   public isOverdue(date: string): boolean {
    date = Dates.asDate(date);
    return date < Dates.getToday();
  }

}
