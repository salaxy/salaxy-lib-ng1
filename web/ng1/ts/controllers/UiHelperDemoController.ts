import * as angular from "angular";
import { UiHelpers } from "@salaxy/ng1";
import { Numeric } from "@salaxy/core";

/** Helps creating a user interface for Payment Channels. */
export class UiHelperDemoController implements angular.IController {

  /**
   * Identifies the services for dependency injection.
   */
  public static $inject = ["UiHelpers"];

  /** Popup demonstration Timeout either as number or string */
  public timeout: number | string = "5";

  /** Target URL for the popup demo */
  public popupUrl = "https://example.com";

  constructor(private uiHelpers: UiHelpers) {
    // UiHelpers is injected into controller using AngularJS dependency injection. 
    // Injected service is identified in runtime by static field "$inject".
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    // Init here
  }

  /**
   * Demonstration method that fetches the entire UiHelpers class and exposes it to view 
   * for easier demonstration.
   *
   * PLEASE NOTE: Exposing the entire service to view like IS AN ANTI-PATTERN!
   * You should typically only expose the methods that you really need in your view code:
   * This makes sure that code breaks at build time if the service changes (in upgrading)
   * instead of things breaking silently in view.
   *
   * @returns Instance of the UI helper service.
   */
  public getHelper(): UiHelpers {
    return this.uiHelpers;
  }

  /** 
   * Test method for launching different types of popups
   * @param type Type of the popup to launch
   */
  public launchPopup(type: "default" | "timeout") {
    // Using the window timeout in purpose so that $timeout service does not interfere
    switch (type) {
      case "timeout":
        const timeout = (Numeric.parseNumber(this.timeout) || 0) * 1000;
        window.setTimeout(() => {
          this.uiHelpers.showExternalDialog("type", this.popupUrl, null, null);
        }, timeout);
        return;
      default: 
        this.uiHelpers.showExternalDialog("type", this.popupUrl, null, null);
        return;
    }
  }

}
