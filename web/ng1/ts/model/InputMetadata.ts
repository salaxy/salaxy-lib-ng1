import { InputType } from "./InputType";

/**
 * Contains metadata about the default input that is determined
 * based on reflection model.
 */
export interface InputMetadata {

  /** Property name. Used in model path and sorting. */
  name: string,

  /**
   * Sort order for the component:
   * Number 10-99 to give the logical sorting and then the name of the property lower case.
   */
  sort: string;

  /** Type of the input */
  input: InputType;

  /** The base data type */
  type: string,

  /** Grouping of the type */
  typeGroup: "number" | "boolean" | "string" | "date" | "enum" // Standard
  | "file"    // TODO
  | "salaxy"  // Salaxy well known / commonly reused
  | "ref"     // Reference to another Salaxy object
  | "unknown" // Unknown / uresolved type => sxy-alert
  | "array"   // Array of other types

  /** In data binding scenarios, the path from the current object. */
  path: string,

  /** Path to the data model */
  model?: string,

  /** Error message if the resolving fails: May be some other content in the future. */
  content?: string,
}
