/**
 * Available input types in SxyInput component.
 * NOTE: Enumeration is designed for HTML attributes, so multi-word values
 * are separated by dash. E.g. MultiSelect = "multi-select".
 */
export enum InputType {
  Text = "text",
  Multiline = "multiline",
  Email = "email",
  Password = "password",
  Tel = "tel",
  Search = "search",
  Checkbox = "checkbox",
  Radio = "radio",
  Select = "select",
  Switch = "switch",
  Typeahead = "typeahead",
  List = "list",
  MultiSelect = "multi-select",
  Numeric = "numeric",
  Range = "range",
  Date = "date",
  Popup = "popup",
  Month = "month",
  Year = "year",
  DateRange = "date-range",
  Upload = "upload",
  Img = "img",
  Avatar = "avatar",
  Unknown = "unknown",
  Ref = "ref",

  PensionContractNumber = "pension-contract-number",
  Array = "array",
}
