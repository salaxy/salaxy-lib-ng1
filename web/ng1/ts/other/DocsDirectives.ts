import angular from "angular";

/** Directives used in documentation. */
export class DocsDirectives {

  /** 
   * Directive that does ng-inlude with 404 error failover.
   * Note that this implementation has its own scope, i.e. it does not have the same scope as parent like ng-include does.
   * This could probably be fixed by following instructions in https://stackoverflow.com/questions/25391279/angularjs-ng-include-failover
   */
  public static sxydevInclude = (): angular.IDirective => {
    return {
      restrict: 'CAE',
      scope: {
        src: '=',
        myInclude: '='
      },
      transclude: true,
      link: function(scope: any, iElement: JQLite, iAttrs: angular.IAttributes, controller: angular.IController) {
        scope.$on("$includeContentError", function(event, args){
          scope.loadFailed = true;
         });
        scope.$on("$includeContentLoaded", function(event, args){
          scope.loadFailed = false;
        });
      },
      template: `<div ng-include="myInclude || src"></div><div ng-show="loadFailed" ng-transclude></div>`,
    };
  }

}