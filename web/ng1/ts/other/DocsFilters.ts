import marked from "marked";

import { TypedocLogic } from "@salaxy/core";

import { DocsService } from "./DocsService";


/** HACK: Temporary import for Higlight.js */
const hljs = (window as any).hljs;

// Set global options for highlight.
marked.setOptions({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  highlight: (code, lang, callback) => {
    if (!lang) {
      return hljs.highlightAuto(code).value;
    } else {
      return hljs.highlight(lang, code).value;
    }
  },
});

/*
import hljs from "highlight.js/lib/core";
import javascript from "highlight.js/lib/languages/javascript";
import json from "highlight.js/lib/languages/json";
import less from "highlight.js/lib/languages/less";
import markdown from "highlight.js/lib/languages/markdown";
import plaintext from "highlight.js/lib/languages/plaintext";
import typescript from "highlight.js/lib/languages/typescript";
import xml from "highlight.js/lib/languages/xml";

hljs.registerLanguage("javascript", javascript);
hljs.registerLanguage("json", json);
hljs.registerLanguage("less", less);
hljs.registerLanguage("markdown", markdown);
hljs.registerLanguage("plaintext", plaintext);
hljs.registerLanguage("typescript", typescript);
hljs.registerLanguage("xml", xml);
*/

/*
Consider adding later if necessary

import diff from "highlight.js/lib/languages/diff";
import scss from "highlight.js/lib/languages/scss";
import yaml from "highlight.js/lib/languages/yaml";

hljs.registerLanguage("diff", diff);
hljs.registerLanguage("scss", scss);
hljs.registerLanguage("yaml", yaml);
*/

/**
 * Provides functions for documentation and DEV support purposes.
 * Mostly used as filters on https://developers.salaxy.com
 */
export class DocsFilters {

  /**
   * Formats a string using markdown and highlighting (color coding) code blocks.
   *
   * - markdown: The markdown that should be formatted.
   * - returns: The formatted markdown as trusted HTML.
   *
   * @example
   * ```html
   * <div ng-bind-html="$ctrl.current.example | sxydevMarkdown"></div>
   * ```
   */
  public static sxydevMarkdown = ["$sce", ($sce: any) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.
    return (markdown: string): string => {
      if (markdown == null) {
        return null;
      }
      return $sce.getTrustedHtml(marked(markdown));
    };
  }];

  /**
   * Syntax highlighting for code.
   *
   * - code: Code that should be highlighted.
   * - lang: Language of the code as in html, typescript, javascript etc. If null, auto detection is used.
   * - returns: The code as formatted trusted HTML.
   *
   * @example
   * ```html
   * <pre class="hljs"><code ng-bind-html="'<p class=&quot;foo&quot;>huuhaa</p>' | sxydevCode"></code></pre>
   * <code ng-bind-html="'<p class=&quot;foo&quot;>huuhaa</p>' | sxydevCode"></code>
   * ```
   */
  public static sxydevCode = ["$sce", ($sce: any) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.
    return (code: string, lang: string): string => {
      if (code == null) {
        return null;
      }
      let html;
      if (!lang) {
        html = hljs.highlightAuto(code).value;
      } else {
        html = hljs.highlight(lang, code).value;
      }
      return $sce.getTrustedHtml(html);
    };
  }];

  /**
   * Formats a typedoc type as html.
   *
   * - type: Typedeoc type that should be visualized.
   * - returns: The type as formatted trusted HTML.
   *
   * @example
   * ```html
   * <span ng-bind-html="prop.type | sxydevType"></span>
   * ```
   */
  public static sxydevType = ["$sce", "DocsService", ($sce: any, docsService: DocsService) => {
    // In tsc v2.9.2 ts:tds breaks if $sce is returned strongly typed (angular.ISCEService). Check if this works after TypeScript upgrade.

    return (type: any): string => {
      return $sce.getTrustedHtml(TypedocLogic.getTypeHtml(type, docsService.typeLinker));
    };
  }];
}
