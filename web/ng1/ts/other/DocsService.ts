import * as angular from "angular";

import { TypedocLogic, AttributeDoc, ClassDoc, ClassMemberDoc, ComponentDoc, EnumDoc, TypeDocType } from "@salaxy/core";
import { ComponentsRegistration, ControllersRegistration } from "@salaxy/ng1";

/**
 * Service that fetches the documentation using NG1 project reflection and typedoc JSON files.
 */
export class DocsService {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$rootScope", "$http", "$q"];

  /**
   * If true, the documentation has been loaded from the server.
   * If false, subscribe to Service event before reload.
   */
  public isReady: boolean;

  /** Collection of all classes and interfaces in the Core and NG1-libraries.  */
  public classes: ClassDoc[] = [];

  /** Enumerations in the Core and NG1-libraries.  */
  public enums: EnumDoc[] = [];

  /** Components in the NG1-project. */
  public components: ComponentDoc[];

  /** Controllers in the NG1-project. */
  public controllers: ClassDoc[];

  /** Registered NG1 filters. */
  public filters: ClassMemberDoc[];

  /** Registered NG1 directives. */
  public directives: (ClassDoc | ClassMemberDoc)[];

  /** Current tab in the UI. Currently used only in the Components view. */
  public currentTab: string;

  /**
   * Grouping for different libraries.
   * You can add defaults with labels, additional groups will be added after library loading.
   */
  public groups: {
    [key: string]: { [key: string]: string },
  } = {
      "all": {},
      "@salaxy/core": {
        api: "API wrappers",
        util: "Core utilities",
        logic: "Core business logic",
        model: "Core model",
      },
      "@salaxy/ng1": {
        "account": "Account edit and view",
        "calc": "Salary Calculation",
        "communications": "Communications",
        "form-controls": "Form controls",
        "helpers": "Misc. helpers and tools",
        "odata": "OData lists and querying",
        "invoices": "Payments and invoices",
        "report": "Reports and accounting",
        "worker": "Worker and employment",
        "obsolete": "Obsolete and phasing out",
        "personal": "Personal: Worker/Household webs",
        "bases": "Base classes",
        "undercon": "Functionality under construction",
      },
    };

  private loadStartTime: Date;

  /**
   * Creates a new DocsComponentsController
   * @ignore
   */
  constructor(
    private $rootScope: angular.IRootScopeService,
    private $http: angular.IHttpService,
    private $q: angular.IQService,
  ) {
    this.reloadDocs();
  }

  /** Loads the ng1-documentation from the generated JSON file and parses it to component documentation. */
  public reloadDocs(): void {
    this.loadStartTime = new Date();
    this.isReady = false;

    // Core typedoc object model
    const coreLoading = this.$http.get("/docs/ts/@salaxy/core.json", {
      responseType: "json",
    }).then((response) => {
      const typeDocJson = response.data as any;
      if (!typeDocJson || typeDocJson.children.length < 10) {
        throw new Error("Invalid generated typedoc: Core.");
      }
      this.enums = this.enums.concat(TypedocLogic.getEnums(typeDocJson, "@salaxy/core"));
      this.classes = this.classes.concat(TypedocLogic.getClasses(typeDocJson, "@salaxy/core"));
    });

    // Ng1 typedoc object model
    const ng1Loading = this.$http.get("/docs/ts/@salaxy/ng1.json", {
      responseType: "json",
    }).then((response) => {
      const typeDocJson = response.data as any;
      if (!typeDocJson || typeDocJson.children.length < 10) {
        throw new Error("Invalid generated typedoc: NG1.");
      }
      this.enums = this.enums.concat(TypedocLogic.getEnums(typeDocJson, "@salaxy/ng1"));
      const ng1Classes = TypedocLogic.getClasses(typeDocJson, "@salaxy/ng1");
      this.classes = this.classes.concat(ng1Classes);
      this.controllers = this.getControllers(ng1Classes);
      this.components = this.getComponents(typeDocJson, ng1Classes);
      this.filters = this.getFilters(ng1Classes);
      this.directives = this.getDirectives(ng1Classes);
    });

    this.$q.all([ng1Loading, coreLoading])
      .then(() => {
        this.isReady = true;
        this.classes.forEach((x) => {
          if (!this.groups[x.lib]) {
            this.groups[x.lib] = {};
          }
          if (!this.groups[x.lib][x.group]) {
            this.groups[x.lib][x.group] = x.group;
          }
        });
        Object.keys(this.groups).slice(1).forEach((key) => this.groups.all = { ...this.groups.all, ...this.groups[key] });
        console.info(`Load and parse ${(new Date().getTime() - this.loadStartTime.getTime()) / 1000} secs.`);
        this.notify();
      })
      .catch((reason) => {
        console.error(reason);
      });
  }

  /**
   * Provides a link for a given type´
   * @param type Type as typedoc object model.
   */
  public typeLinker = (type: TypeDocType): string => {
    // First try by ID.
    if (type.id) {
      // The searching could be made with better accuracy if we knew the source library.
      const enumById = this.enums.find((x) => x.id === type.id && x.name === type.name);
      if (enumById) {
        return `#/docs/enums/${enumById.group}/${enumById.name}`;
      }
      const classById = this.classes.find((x) => x.id === type.id && x.name === type.name);
      if (classById) {
        if (classById.kind === "controller" || classById.kind === "controllerBase") {
          return `#/docs/controllers/${classById.group}/${classById.name}`;
        }
        return `#/docs/classes/${classById.group}/${classById.name}`;
      }
    }
    // Fallback to name-only search
    const enumType = this.enums.find((x) => x.name === type.name);
    if (enumType) {
      return `#/docs/enums/${enumType.group}/${enumType.name}`;
    }
    const classOrInterface = this.classes.find((x) => x.name === type.name);
    if (classOrInterface) {
      if (classOrInterface.kind === "controller" || classOrInterface.kind === "controllerBase") {
        return `#/docs/constollers/${classOrInterface.group}/${classOrInterface.name}`;
      }
      return `#/docs/classes/${classOrInterface.group}/${classOrInterface.name}`;
    }
    return null;
  }

  /**
   * Gets the class documentation based on name
   * @param name Class name to fetch. Name will be trimmed.
   */
  public findClass(name: string): ClassDoc {
    if (!name) {
      return null;
    }
    name = name.trim();
    return this.classes.find((x) => x.name === name);
  }

  /**
   * Gets the class member documentation based on class and member names.
   * Both names are trimmed for spaces (in default value).
   * @param className Name of the class
   * @param memberName Name of the member
   */
  public findMember(className: string, memberName: string): ClassMemberDoc {
    const classDoc = this.findClass(className);
    if (!classDoc) {
      return null;
    }
    memberName = memberName.trim();
    return classDoc.members.find((x) => x.name === memberName);
  }

  /**
   * Controllers can subscribe to changes in service data using this method.
   * Read more about the pattern in: http://www.codelord.net/2015/05/04/angularjs-notifying-about-changes-from-services-to-controllers/
   *
   * @param scope - Controller scope for the subscribing controller (or directive etc.)
   * @param callback - The event listener function. See $on documentation for details
   */
  public subscribe(scope: angular.IScope, callback: (event: angular.IAngularEvent, ...args: any[]) => any): void {
    const handler = this.$rootScope.$on("typedoc-service-event", callback);
    scope.$on("$destroy", handler);
  }

  private notify(): void {
    this.$rootScope.$emit("typedoc-service-event");
  }

  private getDirectives(allClasses: ClassDoc[]): (ClassDoc | ClassMemberDoc)[] {
    const registeredFiltersType = allClasses.find((x) => x.name === "DirectivesRegistration")
      .members.find((x) => x.name === "getDirectives").type;

    const directives: ClassMemberDoc[] = registeredFiltersType.declaration.children.map((filterRegTypedoc) => {
      const defaultValueArr = filterRegTypedoc.defaultValue.split(".");
      const classDoc = this.findClass(defaultValueArr[0]);
      if (!classDoc) {
        // HACK: The defaultValueArr is no longer reliable for some reason.
        return null;
    }
      if (classDoc.implements.find((x) => x.name === "IDirective")) {
        const result = angular.copy(classDoc); // There may be more than one registration for a function.
        result.name = filterRegTypedoc.name; // Typically the same, but may be different in edge cases.
        (result as any).directiveName = this.snakeCase(result.name);
        return result;
      }
      const member = this.findMember(defaultValueArr[0], defaultValueArr[1].replace("()", ""));
      if (!member) {
        // HACK: The directive functions are no longer found for some reason.
        return null;
      }
      const result = angular.copy(member); // There may be more than one registration for a function.
      result.name = filterRegTypedoc.name; // Typically the same, but may be different in edge cases.
      (result as any).directiveName = this.snakeCase(result.name);
      return result;
    });
    return directives.filter(x => x != null);
  }

  private getFilters(allClasses: ClassDoc[]): ClassMemberDoc[] {
    const registeredFiltersType = allClasses.find((x) => x.name === "FiltersRegistration")
      .members.find((x) => x.name === "getFilters").type;

    const filters: ClassMemberDoc[] = registeredFiltersType.declaration.children.map((filterRegTypedoc) => {
      const defaultValueArr = filterRegTypedoc.defaultValue.split(".");
      const member = this.findMember(defaultValueArr[0], defaultValueArr[1]);
      if (!member) {
        // HACK: The filter functions are no longer found for some reason.
        return null;
      }
      const result = angular.copy(member); // There may be more than one registration for a function.
      result.name = filterRegTypedoc.name; // Typically the same, but may be different in edge cases.
      return result;
    });
    return filters.filter(x => x != null);
  }

  /**
   * Sets the kind to "controller" (as opposed to "controllerBase")
   * for registered controllers in all classes and returns all the controllers.
   * TODO: Copying of the array is probably obosolete, we could just set the property.
   */
  private getControllers(allClasses: ClassDoc[]): ClassDoc[] {
    const registeredControllers = ControllersRegistration.getControllers();
    const controllers = allClasses.filter((x) => x.kind === "controllerBase");
    controllers.forEach((ctrl) => {
      if (registeredControllers[ctrl.name]) {
        ctrl.kind = "controller";
      }
    });
    return controllers;
  }

  /**
   * Gets the components from the ComponentsRegistration and typedoc json.
   * @param typedoc The typedoc serialized JSON desrialized as object.
   * @returns The components collection.
   */
  private getComponents(typedoc: any, classes: ClassDoc[]): ComponentDoc[] {
    const registeredComponents = ComponentsRegistration.getComponents();
    const components: ComponentDoc[] = Object.keys(registeredComponents).map((key) => {
      const ng1Component = registeredComponents[key];
      const componentName = ng1Component.constructor.name;
      const controllerName = ng1Component.controller.name;
      const controller = classes.find((x) => x.name === controllerName);
      const componentClass = classes.find((x) => x.name === componentName);
      if (!componentClass){
        console.error("Component not found:" + componentName);
        return null;
      }

      componentClass.kind = "component";
      if (controller){
        controller.usages = controller.usages || [];
        controller.usages.push(componentName);
      }
      const result: ComponentDoc = {
        name: componentName,
        kind: "component",
        elem: this.snakeCase(key),
        controller: controllerName,
        transclude: ng1Component.transclude || null,
        require: ng1Component.require || null,
        defaultTemplate: ng1Component.defaultTemplate,
        attributes: [],
        // Copy from class definition
        id: componentClass.id,
        lib: componentClass.lib,
        shortText: componentClass.shortText,
        group: componentClass.group,
        descr: componentClass.descr,
        example: componentClass.example,
        exampleUrls: componentClass.exampleUrls,
        rawKind: componentClass.rawKind,
        source: componentClass.source,
        screenShots: componentClass.screenShots,
      };
      if (!result.exampleUrls?.length) {
        result.exampleUrls = [`/ng1/examples/${result.group}/${result.name}.html`];
      }
      if (!result.screenShots?.length) {
        result.screenShots = [`/ng1/screen-shots/${result.group}/${result.name}.png`];
      }
      result.attributes = Object.keys(ng1Component.bindings).map((bindingKey) => {
        const prop = controller ? controller.members.find((x) => x.name === bindingKey) : null;
        const attrResult: AttributeDoc = {
          name: bindingKey,
          attr: this.snakeCase(bindingKey),
          binding: ng1Component.bindings[bindingKey],
          shortText: "ERR: Documentation not found",
          descr: "ERR: Documentation not found",
          type: {
            type: "intrinsic",
            name: "any",
          },
          controller: controller ? controller.name : null,
          example: prop ? prop.example : null,
        };
        if (prop) {
          attrResult.rawKind = prop.rawKind;
          attrResult.shortText = prop.shortText;
          attrResult.descr = prop.descr;
          attrResult.type = prop.type;
          attrResult.example = prop.example;
        } else {
          // Binding documentation is only used if property not found in controller.
          let bindingTypedoc = null;
          const componentTypedoc = typedoc.children.find((x) => x.name === componentName);
          if (componentTypedoc && componentTypedoc.children.find((x) => x.name === "bindings") && componentTypedoc.children.find((x) => x.name === "bindings").children) {
            bindingTypedoc = componentTypedoc.children.find((x) => x.name === "bindings").children.find((x) => x.name === bindingKey);
            TypedocLogic.setDeclrProperties(attrResult, bindingTypedoc);
          }
          attrResult.descr += "\n\nThe attribute is only a component binding - not available in the controller.";
          if (attrResult.binding === "@") {
            attrResult.type = {
              type: "intrinsic",
              name: "string",
            };
          }
        }
        return attrResult;
      });
      if (result.transclude) {
        if (result.transclude === true) {
          result.transclude = "single";
        } else {
          Object.keys(result.transclude).forEach((key) => {
            const val: string = result.transclude[key];
            result.transclude[key] = {
              original: val,
              isOptional: val.startsWith("?"),
              elem: this.snakeCase(val.replace("?", "")),
            };
          });
        }
      }
      if (result.require) {
        Object.keys(result.require).forEach((key) => {
          const val: string = result.require[key];
          const isOptional = val.indexOf("?") >= 0;
          const name = val.replace("?", "").replace("^", "").replace("^", "");
          const locate: "current" | "currentAndParents" | "parents" =
            val.indexOf("^^") >= 0 ? "parents" : (val.indexOf("^") >= 0 ? "currentAndParents" : "current");
          const attr = this.snakeCase(name);
          let shortText;
          if (locate === "current") {
            shortText = `The component ${isOptional ? "MAY" : "MUST"} have directive ${attr} (controller ${name}).`;
            result.attributes.splice(0, 0, {
              attr,
              binding: "<",
              example: `${"```"}html\n<${result.elem} ${attr}="$ctrl.current.someValue"></${result.elem}>\n${"```"}`,
              controller: name,
              name,
              shortText,
              type: { type: "intrinsic", name: "any" },
              descr: shortText,
              rawKind: null,
            });
          } else {
            shortText =
              (locate === "currentAndParents" ? "The component or one its parents" : "One of the parent elements of the component")
              + ` ${isOptional ? "MAY" : "MUST"} have controller "${name}" (directive or component "${attr}").`;
          }
          result.require[key] = {
            original: val,
            name,
            isOptional,
            locate,
            elem: attr,
            shortText,
          };
        });
      }
      return result;
    }).filter((x) => x != null);
    return components;
  }

  private snakeCase(name) {
    const SNAKE_CASE_REGEXP = /[A-Z]/g;
    return name.replace(SNAKE_CASE_REGEXP, (letter, pos) => {
      return (pos ? "-" : "") + letter.toLowerCase();
    });
  }
}
