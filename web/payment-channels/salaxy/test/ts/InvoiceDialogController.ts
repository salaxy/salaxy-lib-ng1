import * as angular from "angular";

import { ExternalDialogData, PaymentChannelSettingsInfo } from "@salaxy/core";

import { UiHelpers } from "@salaxy/ng1";

/** Invoice dialog controller for Test payment channel. */
export class InvoiceDialogController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$location", "$window", "$http", "$q", "UiHelpers"];

  /** Access token for the request. */
  public accessToken: string;

  /** Invoice ids */
  public invoiceIds: string;

  /** Flag indicating if the authentication has been done. */
  public isAuthenticated: boolean;

  /** Current dialog action. */
  public action: "ok" | "cancel";

  /** Method to execute in init. */
  public method: "invoice" | "verify";

  /** User option to use authentication in testing. */
  public useAuthentication: boolean;
  /** Indicates if the current channel is enabled. */

  /** Verification result. */
  public verificationResult: any;

  /** Current error. */
  public error = null;

  /** */
  public success = null;

  /** Loaders. */
  public loaders = {};

  constructor(private $location: angular.ILocationService, private $window: angular.IWindowService, private $http: angular.IHttpService, private $q: angular.IQService, private uiHelpers: UiHelpers) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    this.accessToken = this.$location.search().token;
    this.invoiceIds = this.$location.search().invoiceIds;
    this.isAuthenticated = !!this.$location.search().isAuthenticated;
    this.action = this.$location.search().action as any;
    this.method = this.$location.search().method as any;
   
    this.useAuthentication = this.isAuthenticated || false;

    if (this.method) {
        this.close(this.action, this.method);
    };
  }

  /** Closes the dialog with given action */
  public close(action: "ok" | "cancel", method: "invoice" | "verify" = null) {
  
    this.action = action;

    if (this.action === "cancel") {
      this.method = null;
    } else {
      this.method = method;
    }
   
    if (this.action !== "cancel" && this.useAuthentication && !this.isAuthenticated) {
      this.authenticate(this.method, this.action);
    } else {
      const end = () => {
        const result: ExternalDialogData<PaymentChannelSettingsInfo> = {
          action,
          item: null,
        };
        window.opener.postMessage(result, "*");
        window.close();
      }
  
      switch (this.method) {
        case "invoice":
          return this.invoice().then((result) => {
            if (result) {
              end();
            }
          });
        case "verify":
          return this.verify().then((result) => {
            if (result) {
            }
          });
        default:
          end();
      }
    }
  }

  /**
   * Returns true if any loader is active.
   */
  public get isLoading(): boolean {
    for (const prop of Object.keys(this.loaders)) {
      return true;
    }
    return false;
  }

  /**
   * Sets the current loader.
   * @param method Name for the loader.
   */
  private setLoader(method: "invoice" | "verify" = null) {
    this.loaders = {};
    if (method) {
      this.loaders[method] = true;
    }
  };

  /**
   * Makes a GET request using given url and returns the result.
   * @param url Url for fetching data.
   */
  private getJson(url: string): angular.IPromise<any> {
    return this.$http.get(url, { responseType: "json", withCredentials: false }).then(
      (response) => response.data,
      (error) => {
        return this.$q.reject(error);
      });
  };

 
  /** Create invoice */
  private invoice(): angular.IPromise<boolean> {
    this.error = null;
    this.success = null;
    this.verificationResult = null;
    this.setLoader(this.method);
    const url = "test/invoice?accessToken=" + this.accessToken + "&invoiceIds=" + this.invoiceIds;
    return this.getJson(url).then(
            () => {
                this.setLoader();
                this.success = this.method;
                return true;
            },
            (error) => {
                this.setLoader();
                this.error = error;
                return false;
            }
        );
    } 


 /** Verify the invoice immediately */
 private verify(): angular.IPromise<boolean> {
  this.error = null;
  this.success = null;
  this.verificationResult = null;
  this.setLoader(this.method);
  const url = "test/verification?accessToken=" + this.accessToken + "&invoiceIds=" + this.invoiceIds;
  return this.getJson(url).then(
          (data) => {
              this.setLoader();
              this.success = this.method;
              this.verificationResult = data;
              return true;
          },
          (error) => {
              this.setLoader();
              this.error = error;
              return false;
          }
      );
  } 

  /**
   * Authenticates the current user.
   * @param method Pending method.
   * @param action Pending action.
   * @param data Pending data.
   */
  private authenticate(method: "invoice" | "verify", action: "ok" | "cancel") {
    const searchParams = new URLSearchParams(this.$location.search());
    searchParams.set("isAuthenticated", `${true}`);
    searchParams.set("method", method);
    searchParams.set("action", action);
   
    const returnUrl = encodeURIComponent(this.$window.location.href.substr(0, this.$window.location.href.indexOf("?")) + "?" + searchParams.toString());
    this.$window.location.href = "test/oauth/authenticate?accessToken=" + this.accessToken + "&returnUrl=" + returnUrl;
  };
}
