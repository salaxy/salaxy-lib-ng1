import * as angular from "angular";

import { ExternalDialogData, PaymentChannelSettingsInfo } from "@salaxy/core";

import { UiHelpers } from "@salaxy/ng1";

/** Settings dialog controller for Test payment channel. */
export class SettingsDialogController implements angular.IController {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["$location", "$window", "$http", "$q", "UiHelpers"];

  /** The existing Settings and info data for this payment channel. */
  public current: PaymentChannelSettingsInfo;

  /** Access token for the request. */
  public accessToken: string;

  /** Flag indicating if the authentication has been done. */
  public isAuthenticated: boolean;

  /** Current dialog action. */
  public action: "ok" | "cancel" | "delete" | "set-default";

  /** Method to execute in init. */
  public method: "coa" | "none";

  /** User option to use authentication in testing. */
  public useAuthentication: boolean;
  /** Indicates if the current channel is enabled. */
  public isDefault: boolean;
  /** Indicates if the chart of accounts should be fetched. */
  public configureCoa: boolean;

  /** Current error. */
  public error = null;

  /** Loaders. */
  public loaders = {};

  constructor(private $location: angular.ILocationService, private $window: angular.IWindowService, private $http: angular.IHttpService, private $q: angular.IQService, private uiHelpers: UiHelpers) {
  }

  /**
   * Controller initialization
   */
  public $onInit() {
    this.accessToken = this.$location.search().token;
    this.isAuthenticated = !!this.$location.search().isAuthenticated;
    this.action = this.$location.search().action as any;
    this.method = this.$location.search().method as any;
    const data = this.$location.search().data;
    if (data) {
      this.current = JSON.parse(atob(decodeURIComponent(data)));
    }

    this.isDefault = this.current?.isDefault || false;
    this.useAuthentication = this.isAuthenticated || false;

    switch (this.method) {
      case "coa":
      case "none":
        this.close(this.action);
        break;
    };
  }

  /** Closes the dialog with given action */
  public close(action: "ok" | "cancel" | "delete" | "set-default") {
  
    this.action = action;

    if (this.action === "delete" || this.action === "cancel") {
      this.method = "none";
    } else {
      this.method = this.method || this.configureCoa ? "coa" : "none";
    }
   

    if (this.action !== "cancel" && this.useAuthentication && !this.isAuthenticated) {
      this.authenticate(this.method, this.action, encodeURIComponent(btoa(JSON.stringify(this.current))));
    } else {

      const end = () => {
        const result: ExternalDialogData<PaymentChannelSettingsInfo> = {
          action,
          item: this.current,
        };
        window.opener.postMessage(result, "*");
        window.close();
      }
  
      switch (this.method) {
        case "coa":
          return this.coa().then((result) => {
            if (result) {
              end();
            }
          });
        default:
          end();
      }
    }
  }

  /**
   * Returns true if any loader is active.
   */
  public get isLoading(): boolean {
    for (const prop of Object.keys(this.loaders)) {
      return true;
    }
    return false;
  }

  /**
   * Sets the current loader.
   * @param action Name for the loader.
   */
  private setLoader(action: "ok" | "cancel" | "delete" | "set-default" = null) {
    this.loaders = {};
    if (action) {
      this.loaders[action] = true;
    }
  };

  /**
   * Makes a GET request using given url and returns the result.
   * @param url Url for fetching data.
   */
  private getJson(url: string): angular.IPromise<any> {
    return this.$http.get(url, { responseType: "json", withCredentials: false }).then(
      (response) => response.data,
      (error) => {
        return this.$q.reject(error);
      });
  };

  /**
   * Retrieves the chart of accounts.
   */
  private coa(): angular.IPromise<boolean> {
    this.error = null;
    this.setLoader(this.action);
    const url = "test/coa?accessToken=" + this.accessToken;
    return this.getJson(url).then(
      (data) => {
        this.setLoader();
        (this.current as any).coa = data;
        return true;
      },
      (error) => {
        this.setLoader();
        this.error = error;
        return false;
      }
    );
  };

  /**
   * Authenticates the current user.
   * @param method Pending method.
   * @param action Pending action.
   * @param data Pending data.
   */
  private authenticate(method: "none" | "coa", action: "ok" | "cancel" | "delete" | "set-default", data: any) {
    const searchParams = new URLSearchParams(this.$location.search());
    searchParams.set("isAuthenticated", `${true}`);
    searchParams.set("method", method);
    searchParams.set("action", action);
    searchParams.set("data", data);
   
    const returnUrl = encodeURIComponent(this.$window.location.href.substr(0, this.$window.location.href.indexOf("?")) + "?" + searchParams.toString());
    this.$window.location.href = "test/oauth/authenticate?accessToken=" + this.accessToken + "&returnUrl=" + returnUrl;
  };
}
