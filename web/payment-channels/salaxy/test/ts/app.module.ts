import * as angular from "angular";

import { Configs } from "@salaxy/core";
import { RouteHelperProvider } from "@salaxy/ng1";
import { SettingsDialogController } from "./SettingsDialogController";
import { PaymentDialogController } from "./PaymentDialogController";
import { InvoiceDialogController } from "./InvoiceDialogController";

/**
 * Main Angular application for the Payment Channel dialog pages.
 */
export const module = angular.module("Salaxy.Payment", ["ngRoute", "salaxy.ng1.components.all"])
  .config(["RouteHelperProvider",
    function (routeHelperProvider: RouteHelperProvider) {
      routeHelperProvider
        .setCustomSectionRoot(((Configs.current as any || {}).rootDirectory || "") + "/payment-channels/salaxy/test")
        //.homeDefault("home")
        .customSection("dialogs")
        .commit();
    }
  ])
  .controller("SettingsDialogController", SettingsDialogController)
  .controller("PaymentDialogController", PaymentDialogController)
  .controller("InvoiceDialogController", InvoiceDialogController)
  .run(["Ng1Translations", function (translate) {
    translate.setLanguage("fi");
  }]);
  